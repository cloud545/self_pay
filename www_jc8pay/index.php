<?php
date_default_timezone_set('Asia/Shanghai');
error_reporting(E_ALL^E_NOTICE);

//ini_set("display_errors", "On");
//error_reporting(E_ALL);

$sDir = dirname(__DIR__);

define("PATH_BOOPHP", "{$sDir}/BooPHP/lib");

define("PATH_ROOT", "{$sDir}/");
define("PATH_CLASS", "{$sDir}/class");
define("PATH_CONFIG", "{$sDir}/config");
define("PATH_LOG", "{$sDir}/logs");
define("PATH_TPL", "{$sDir}/tpl");
define("PATH_DATA", "{$sDir}/data");
define("PATH_WEB", "{$sDir}/www_jc8pay");

$_SERVER["APP_ENV"] = 'jc8pay';

require_once PATH_BOOPHP . "/Autoload.php";

$oApp = new BooApp();
$oApp->run("Api");