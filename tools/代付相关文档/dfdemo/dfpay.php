<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap.min.css"  crossorigin="anonymous">
    <title>代付demo</title>
  </head>
  <body>
     <form class="needs-validation" method="post" action="./withdrawal.php" target="_blank">
        <h1 style="width:800px;margin:10px auto;">代付demo</h1>
        <div class="row" style="width:800px;margin:0 auto;">
              <div class="col-md-8 mb-3">
                <label for="firstName">代付订单金额（10000~49999）</label>
                <input type="text" class="form-control" name="amount"  value="10001" >
              </div>
              <div class="col-md-8 mb-3">
                <label for="firstName">订单号</label>
                <input type="text" class="form-control" name="orderId"  value="dy4225<?=time() ?>" >
              </div>
              <div class="col-md-8 mb-3">
                <label for="firstName">支付方式</label>
                <select class="custom-select d-block w-100" name="payType" required="">
					<option value="7">代付</option>
                </select>
              </div>
              <div class="col-md-8 mb-3">
                <label for="firstName">创建时间</label>
                <input type="text" class="form-control" name="time"  value="<?=date('Y-m-d H:i:s',time())?>" >
              </div>
              <button class="btn btn-primary btn-lg btn-block" type="submit">立即请求</button>
        </div>
     </form>
</body>
</html>