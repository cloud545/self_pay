<?php

function createSign($params, $appKey) {

	ksort($params);

    $checkString = '';
    foreach ($params as $key =>$value) {
        if (!$checkString) {
            $checkString = "{$key}={$value}";
        } else {
            $checkString .= "&{$key}={$value}";
        }
    }

    $checkString .= $appKey;
    $sign = md5($checkString);

    return $sign;
}

function sendPost($url, $postData) {

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // post数据
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 信任任何证书
    // post的变量
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);

    $outPut = curl_exec($ch);
    curl_close($ch);
    //返回数据
    return $outPut;
}

