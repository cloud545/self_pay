package com.rest;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rest.DateUtil;
import com.rest.MD5Util;

/**
 * 控制器
 * @author 
 */
@RestController
@Validated
public class PayController {

	/**
	 * 下单接口
	 * @param request
	 * @param response
	 * @param id
	 * @throws IOException
	 * @throws ServletException 
	 */
	@RequestMapping("/payment")
	public void payment(HttpServletRequest request, 
			HttpServletResponse response) {
		String amount = request.getParameter("amount");
		String payType = request.getParameter("payType");
		
        String pay_notifyurl = "http://<host>/notifyurl";
        String pay_callbackurl = "http://<host>/";
        
        String time = DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss");
        String appid = "0001";
        String appKey = "***************************";
		String orderId = "000000001";
		String remark = "iphone11";
		
        String param = "amount=" + amount + "&appId=" + appid + "&clientUrl=" + pay_callbackurl +
        		"&notifyUrl=" + pay_notifyurl + "&orderId=" + orderId + 
        		"&payType=" + payType + "&remark=" + remark + "&time=" + time;
        System.out.println("发送的报文md5sign明文：" + param + appKey);
        String md5sign = MD5Util.EncodeByMD5(param + appKey);
        System.out.println("发送的报文md5sign：" + md5sign);

		try {	
	        String url = "https://<server>/app/pay?" + "amount=" + amount + "&appId=" + appid + "&clientUrl=" + URLEncoder.encode(pay_callbackurl,"utf-8") +
	        		"&notifyUrl=" + URLEncoder.encode(pay_notifyurl,"utf-8") + "&orderId=" + orderId + 
	        		"&payType=" + payType + "&remark=" + URLEncoder.encode(remark,"utf-8") + "&time=" + URLEncoder.encode(time,"utf-8") + "&sign=" + md5sign;
	        System.out.println("发送的报文url：" + url);

			response.sendRedirect(url);
		} catch (Exception e) {
			System.out.println("第一支付发送的报文url报错");
			e.printStackTrace();
			//进行错误提示处理
		}
	}
	
    /**
     * 下单异步通知
     */
	@RequestMapping("/notifyurl")
	public void notifyurl(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String platOrderId = request.getParameter("platOrderId");
		String orderId = request.getParameter("orderId");
		String amount = request.getParameter("amount");
		String realAmount = request.getParameter("realAmount");
		String remark = request.getParameter("remark");
		String time = request.getParameter("time");
		String sign = request.getParameter("sign");
		String appKey = "***************************";
		String md5Sign = "amount=" + amount + "&orderId=" + orderId + "&platOrderId=" + platOrderId + "&realAmount="
				+ realAmount + "&remark=" + remark + "&time=" + time + appKey;

		System.out.println("接收到的报文：" + md5Sign);
		md5Sign = MD5Util.EncodeByMD5(md5Sign);

		if (!md5Sign.equals(sign)) {
			System.out.println("支付异步回调=>签名验证失败：" + sign);
			return;
		}
		System.out.println("支付异步回调=>签名验证成功：" + orderId);

		//TODO 支付成功业务逻辑代码
		
		//TODO END

		BufferedOutputStream out = new BufferedOutputStream(response.getOutputStream());
		out.write("success".getBytes());
		out.flush();
		out.close();
	}

}
