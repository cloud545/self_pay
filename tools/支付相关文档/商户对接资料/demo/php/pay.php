<?php
require_once 'lib.php';

$appId = 123456;
$appKey = 'abcdefg';
$serverUrl = 'http://www.xxx.com/app/pay';

$params = array();
$params['appId'] = $appId; // 商户id
$params['orderId'] = 'dfhdhdnd'; // 商户订单id
$params['amount'] = 100.00; // 充值金额，单位：元
$params['payType'] = 2; // 支付类型：1网银，2支付宝wap，3微信wap，4 QQ钱包，5银联扫码，6京东，7代付，8 银联快捷，9支付宝扫码，10微信扫码
$params['remark'] = ''; // 商户备注信息，通知过程中会原样返回。若该值包含中文，请注意编码,编码方式为：UTF-8
$params['clientUrl'] = 'http://www.xxxx.com'; // 在支付完成后将会跳转到的商户页面的地址
$params['notifyUrl'] = 'http://www.xxxx.com/payCallback'; // 异步通知的地址，需要以http://或https://开头且没有任何参数(如存在特殊字符请转码,注:不支持参数)
$params['time'] = date('Y-m-d H:i:s');
$params['sign'] = createSign($params, $appKey);

header("Location: {$serverUrl}?" . http_build_query($params));
exit;

