<?php
require_once 'lib.php';

$appId = 123456;
$appKey = 'abcdefg';
$serverUrl = 'http://www.xxx.com/app/getBalance';

$params = array();
$params['appId'] = $appId; // 商户id
$params['time'] = date('Y-m-d H:i:s');
$params['sign'] = createSign($params, $appKey);

$rs = sendPost($serverUrl, $params);
echo $rs;
exit;
