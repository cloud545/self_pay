<?php
require_once 'lib.php';

$appId = 123456;
$appKey = 'abcdefg';
$serverUrl = 'http://www.xxx.com/app/withdrawal';

$params = array();
$params['appId'] = $appId; // 商户id
$params['orderId'] = 'dfhdhdnd'; // 商户订单id
$params['amount'] = 100.00; // 提款金额，单位：元
$params['payType'] = 7; // 支付类型：1网银，2支付宝，3微信，4 QQ钱包，5银联扫码，6京东，7代付
$params['cardNo'] = '6546859879757668'; // 提款银行卡号
$params['accountName'] = '张三'; // 开户人
$params['bankCode'] = 'ABC'; // 银行代号
$params['bankName'] = '中国农业银行泉州支行'; // 开户行
$params['notifyUrl'] = 'http://www.xxxx.com/withdrawalCallback'; // 异步通知的地址，需要以http://或https://开头且没有任何参数(如存在特殊字符请转码,注:不支持参数)
$params['time'] = date('Y-m-d H:i:s');
$params['sign'] = createSign($params, $appKey);

$rs = sendPost($serverUrl, $params);
echo $rs;
exit;

