window.SysCookie = {
    set: function (key, value, iHours) {

        var cookieString = key + "=" + value;

        if (iHours) {
            var date = new Date();
            date.setTime(date.getTime + iHours * 3600 * 1000);
            cookieString = cookieString + ";expires=" + date.toGMTString();
        }
        document.cookie = cookieString;
    },

    get: function (key) {
        var arr = document.cookie.split("; ");
        for(var i = 0; i < arr.length; i++) {
            var arr2 = arr[i].split("=");
            if(key == arr2[0]) {
                return arr2[1];
            }
        }
        return null;
    },

    remove: function (key) {
        Sys.Cookie.set(key, "", -1);
    }
};

window.SysData = {
    isObj: function (data) {
        if (typeof data === "object") {
            return true;
        } else {
            return false;
        }
    },
    isArray: function (data) {
        return Object.prototype.toString.call(data) === '[object Array]';
    },
    isNull: function (data) {
        if (typeof data == "undefined" || data == null || data == '' || (typeof data === "object" && (data instanceof Array) && data.length == 0)) {
            return true;
        } else {
            if (typeof data === "object" && !(data instanceof Array)){
                var hasProp = false;
                for (var prop in data){
                    hasProp = true;
                    break;
                }
                if (!hasProp){
                    return true;
                }
            }
            return false;
        }
    },
    isString: function (data) {
        var patrn=/^([a-z]|[A-Z])+$/;
        return patrn.exec(data);
    },
    isFloat: function (data) {
        var patrn=/^\d+\.\d+$/;
        return patrn.exec(data);
    },
    isInt: function (data) {
        var patrn=/^-?\d+$/;
        return patrn.exec(data);
    },
    isUint: function (data) {
        var patrn=/^\d+$/;
        return patrn.exec(data);
    },
    //修剪两端空白字符和换行符
    trim: function (data) {
        return data.replace(/(^\s*)|(\s*$)|(\n)/g, "");
    },
    //修剪左端空白字符和换行符
    leftTrim: function (data) {
        return data.replace(/(^\s*)|(^\n)/g, "");
    },
    //修剪右端空白字符和换行符
    rightTrim: function (data) {
        return data.replace(/(\s*$)|(\n$)/g, "");
    },
    // 是否是URL
    isUrl: function (data) {
        var p = /^((https|http|ftp|rtsp|mms)?:\/\/)[A-Za-z0-9-]+\.[A-Za-z0-9-]+[\/=\?%\-&_~`@[\]\':+!]*([^<>\"\"])*$/;
        data = data.toLowerCase();
        return p.test(data);
    },
    //格式化数字
    numberFormat: function (data, len) {
        if (!len || len < 1) len = 3;
        data=String(data).split(".");
        data[0]=data[0].replace(new RegExp('(\\d)(?=(\\d{'+len+'})+$)','ig'),"$1,");
        return data.join(".");
    },
    //星号字节
    asteriskByte: function (data, start, end) {
        var startStr = start?data.substr(0,start):"";
        var endStr = end?data.substr(end+1):"";
        var star = "",l;
        l = !start && !end?data.length:(start && !end?data.length - start:end);
        while(star.length < l)star += "*";
        return startStr + star + endStr;
    },
    //四舍五入保留n位小数(默认保留两位小数)，不足用0补齐
    roundDecimalPlaces: function (data, l) {
        if (isNaN(parseFloat(data))||data==0) return "0.00";
        var bit = !l?100:Math.pow(10,l);
        var str = String(Math.round(data * bit) / bit);
        while (str.indexOf(".") != -1 && str.length <= str.indexOf(".") + l)str += '0';
        return str;
    },
    //四舍五入保留n位小数
    roundDecimal: function (data, l) {
        if (isNaN(parseFloat(data))||data==0) return "0.00";
        var bit = !l?100:Math.pow(10,l);
        var str = String(Math.round(data * bit) / bit);
        return str;
    },
    // 验证手机号码
    checkPhone: function (phone) {
        if(!(/^1[34578]\d{9}$/.test(phone))){
            return false;
        }
        return true;
    },
    //密码验证(6－16位数字和字母，不能只是数字，或者只是字母，不能连续三位相同)
    validateUserPss: function (str) {

        var patrn = /^[0-9a-zA-Z]{6,16}$/;
        if (!patrn.exec(str)) {
            return false;
        }

        patrn = /^\d+$/;
        if (patrn.exec(str)) {
            return false;
        }

        patrn = /^[a-zA-Z]+$/;
        if (patrn.exec(str)) {
            return false;
        }

        patrn = /(.)\1{2,}/;
        if (patrn.exec(str)) {
            return false;
        }
        return true;
    }
};

window.SysHttpRqSetTimeoutId = 0;
window.SysHttpResponse = true;
window.SysHttp = {
    post: function (url, jsonParam, callback, isLimitReq = false) {

        if (!SysData.isObj(jsonParam)) {
            console.log("参数格式错误，参数格式是 json对象数据");
            return;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        if (window.SysHttpResponse === false) {
            alert("1 秒钟内只能提交一次");
            return;
        }

        if (isLimitReq) {
            window.SysHttpRqSetTimeoutId = setTimeout(window.SysHttp.timer, 1000);
            window.SysHttpResponse = false;
        }

        $.post(url, jsonParam, function (rs, status, xhr) {
            $('meta[name="csrf-token"]').attr('content', xhr.getResponseHeader("X-CSRF-TOKEN"));
            callback(rs);

            //window.SysHttpResponse = true;
            //clearTimeout(window.SysHttpRqSetTimeoutId);
        });
    },
    get: function (url, jsonParam, callback) {

        if (!SysData.isObj(jsonParam)) {
            console.log("参数格式错误，参数格式是 json对象数据");
            return;
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.get(url, jsonParam, function (rs, status, xhr) {
            $('meta[name="csrf-token"]').attr('content', xhr.getResponseHeader("X-CSRF-TOKEN"));
            callback(rs);
        });
    },
    timer: function () {
        window.SysHttpResponse = true;
    }

};


window.SysDialog = {
    error: function (text) {
        layer.alert(text, {title:'<i class="red fa fa-exclamation-triangle bigger-140"></i>', btn:false});
    },
    success: function (text) {
        layer.alert(text, {title:'<i class="green fa fa-check-circle bigger-140"></i>'});
    },
    msg: function (text) {
        layer.msg(text);
    },
    alert: function (text) {
        layer.alert(text, {title:"&nbsp;"});
    },
    confirm: function (text, callback) {
        layer.confirm(text, {
            btn: ['确定','取消']
        }, function(index, layero){
            callback(true);
            layer.close(index);

        }, function(){
            callback(false);
        });
    }
};
