<?php
/**
 * BooPHP
 * Controller
 */
class BooController {
	private static $_classes = array();

	public static function get($classMethod, $args = null) {
		if (!$classMethod) throw new ErrorException("class is empty!", 1002);

		if ($args !== null) {
			$noArges = false;
			$funcNum = func_num_args();
			if ($funcNum > 2) {
				$args = func_get_args();
				unset($args[0]);
			} else {
				if (!is_array($args)) {
					$args = (array)$args;
				} else {
					$args = array($args);
				}
			}
		} else {
			$args = array();
			$noArges = true;
		}

		$classMethod = explode('.', $classMethod);
		$class = &$classMethod[0];
		if (!isset(self::$_classes[$class])) {
			self::$_classes[$class] = new $class;
		}
		if (!empty($classMethod[1])) {
			$method = &$classMethod[1];
			return call_user_func_array(array(self::$_classes[$class], $method), $args);
		} else {
			if (!$noArges) {
				return call_user_func_array(array(self::$_classes[$class], "run"), $args);
			} else {
				return self::$_classes[$class];
			}
		}
	}
}
