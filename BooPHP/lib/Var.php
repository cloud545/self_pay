<?php
/**
 * BooPHP
 * Var
 */
class BooVar {
	public static function get($key, $default = null) {
		return self::_get("get", $key, $default);
	}

	public static function getx() {
		$keys = func_get_args();
		return self::_getx('get', $keys);
	}

	public static function post($key, $default = null){
		return self::_get('post', $key, $default);
	}

	public static function postx() {
		$keys = func_get_args();
		return self::_getx('post', $keys);
	}

	public static function request($key, $default = null){
		return self::_get('request', $key, $default);
	}

	public static function requestx() {
		$keys = func_get_args();
		return self::_getx('request', $keys);
	}

	public static function server($key, $default = null){
		return self::_get('server', $key, $default);
	}

	public static function serverx() {
		$keys = func_get_args();
		return self::_getx('server', $keys);
	}

	public static function cookie($key, $default = null){
		return self::_get('cookie', $key, $default);
	}

	public static function cookiex() {
		$keys = func_get_args();
		return self::_getx('cookie', $keys);
	}

	public static function session($key, $default = null){
		return self::_get('session', $key, $default);
	}

	public static function sessionx() {
		$keys = func_get_args();
		return self::_getx('session', $keys);
	}

	public static function file($key) {
		return self::_get('file', $key, null);
	}

	public static function filex() {
		$keys = func_get_args();
		return self::_getx('file', $keys);
	}

	private static function _get($type, $key, $default) {
		$data = self::_getValue($type);
		if (!isset($data[$key]) && $default) return $default;
		if (isset($data[$key])) return $data[$key];
		return null;
	}

	private static function _getx($type, $keys) {
		$data = self::_getValue($type);
		if ($keys && is_array($keys)) {
			$values = array();
			foreach ($keys as $key) {
				if (isset($data[$key])) {
					$values[$key] = $data[$key];
				}
			}
			return $values;
		}
		return $data;
	}

	private static function _getValue($type) {
		$data = array();
		if ($type == 'get') $data = $_GET;
		if ($type == 'post') $data = $_POST;
		if ($type == 'request') $data = $_REQUEST;
		if ($type == 'server') $data = $_SERVER;
		if ($type == 'cookie') $data = $_COOKIE;
		if ($type == 'session') $data = $_SESSION;
		if ($type == 'file') $data = $_FILES;
		// 如果是提交的数据做html字符处理
		if ($type == "get" || $type == "post" || $type == "request") {
			if ($data) {
				self::_resetData($data);
			}
		}
		return $data;
	}

	private static function _resetData(&$data) {
		if ($data) {
			foreach($data as &$val) {
				if (!is_array($val)) {
					$val = trim($val);
					if ($val) $val = htmlspecialchars($val);
				} else {
					self::_resetData($val);
				}
			}
		}
	}
}
