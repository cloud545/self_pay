<?php
/**
 * BooPHP
 * Util
 */
class BooUtil {

	/**
	 * 获取真实ip
	 *
	 * @return string
	 */
	static public function realIp() {
		static $realip = NULL;
		if ($realip !== NULL) {
			return $realip;
		}

		if (isset($_SERVER)) {
			if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				$arr = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);

				//取X-Forwarded-for中第一个非unknown的有效Ip字符串
				foreach ($arr as $ip) {
					$ip = trim($ip);
					if ($ip != 'unknown') {
						$realip = $ip;
						break;
					}
				}
			}elseif (isset($_SERVER['HTTP_CF_CONNECTING_IP'])) {
				$realip = $_SERVER['HTTP_CF_CONNECTING_IP'];
			} elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
				$realip = $_SERVER['HTTP_CLIENT_IP'];
			} else {
				if (isset($_SERVER['REMOTE_ADDR'])) {
					$realip = $_SERVER['REMOTE_ADDR'];
				} else {
					$realip = '0.0.0.0';
				}
			}
		} else {
			if (getenv('HTTP_X_FORWARDED_FOR')) {
				$realip = getenv('HTTP_X_FORWARDED_FOR');
			} elseif (getenv('HTTP_CLIENT_IP')) {
				$realip = getenv('HTTP_CLIENT_IP');
			} else {
				$realip = getenv('REMOTE_ADDR');
			}
		}
                
		$onlineip = null;
		preg_match("/[\d\.]{7,15}/", $realip, $onlineip);
                if(!empty($onlineip[0])){
                    $realip = $onlineip[0];
                }elseif(self::isIpv6($realip)){
                    $realip = substr($realip,0,20); //数据库ip长度只有20
                }else{
                    $realip = '0.0.0.0';
                }
		return $realip;
	}

    /**
     * 判断当前地址是否为https
     * @return bool
     */
	static public function isHttps() {

        if (!empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off') {
            return true;
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
            return true;
        } elseif ( ! empty($_SERVER['HTTP_FRONT_END_HTTPS']) && strtolower($_SERVER['HTTP_FRONT_END_HTTPS']) !== 'off') {
            return true;
        } elseif (! empty($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], 'https') == true) {
            return true;
        } elseif ($_SERVER['SERVER_PORT'] == 443) {
            return true;
        }

        return false;
    }
    
    static public function isIpv6($ip){
        $is_ipv6 = false;
        try{
            $is_ipv6 = filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) !== false;
        } catch (Exception $ex) {
            $is_ipv6 = false;
        }
        return $is_ipv6;
    }
}
