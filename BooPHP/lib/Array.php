<?php
class BooArray {
	public static function multisort(&$arr) {
		if (!is_array($arr)) return $arr;
		if (!$arr) return array();
		$args = func_get_args();
		array_shift($args);
		if ($args) {
			$params = $fields = $sortType = array();
			foreach($args as $arg) {
				$fields[] = $arg[0];
				$sortType[] = isset($arg[1]) ? $arg[1] : SORT_ASC;
			}
			foreach($arr as $key => $val) {
				foreach($fields as $k => $field) {
					$data[$k][] = isset($val[$field]) ? $val[$field] : 0;
				}
			}
			foreach($sortType as $key => $sort) {
				$params[] = $data[$key];
				$params[] = $sort;
			}
			$params[] = &$arr;
			call_user_func_array("array_multisort", $params);
		}
	}

	public static function resetIndex($array, $index) {
		$tmpArray = array();
		if ($array) {
			foreach($array as $val) {
				$v = $val[$index];
				$tmpArray[$v] = $val;
			}
		}
		return $tmpArray;
	}
}
