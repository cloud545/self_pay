<?php
/**
 * BooPHP
 * autoload
 */
class BooAutoload {
	private static $_classes = array(); // 类

	/**
	 * 加载类文件
	 */
	public static function load($class) {
		$dirSeparator = DIRECTORY_SEPARATOR;
		$dir = str_replace(array('_', '/', '\\'), $dirSeparator, $class);
		if (isset(self::$_classes[$class])) {
			$file = self::$_classes[$class];
		} else {
			$isBooPHPClass = false;
			$isProject = self::_isProject($class);
			$Boophp = stripos($class, "Boo");
			if ($Boophp !== false) {
				$isBooPHPClass = true;
				$dir = str_replace("Boo", $dirSeparator, $dir);
				$file = PATH_BOOPHP . "{$dir}.php";
			} else if ($isProject){
				$isBooPHPClass = true;
				/*
				$position = strrpos($dir, $dirSeparator);
				if ($position) $position += 1;
				$endDir = substr($dir, $position);
				 */
				// 更改类目录大小写
				$dirArr = explode($dirSeparator, $dir);
				$endDir = end($dirArr);
				foreach($dirArr as &$v) {
					if ($v != $endDir) $v = ucfirst($v);
				}
				$dir = join($dirSeparator, $dirArr);
				$dir = PATH_CLASS . "{$dirSeparator}{$dir}";
				// 向下找
				$file = "{$dir}{$dirSeparator}{$endDir}.php";
				// 当前目录找
				if (!file_exists($file)) $file = "{$dir}.php";
				// 向上找
				if (!file_exists($file)) {
					$dirArr = explode($dirSeparator, $dir);
					$position = count($dirArr) - 2;
					unset($dirArr[$position]);
					$file = join($dirSeparator, $dirArr) . ".php";
				}
				// 大写字母开头的文件
				if (!file_exists($file)) {
					$ucFirstEnddir = ucfirst($endDir);
					$dir = str_replace($endDir, $ucFirstEnddir, $dir);
					$file = "{$dir}{$dirSeparator}" . $ucFirstEnddir . ".php";
				}
			}
		}

		if (isset($file)) {
			if (!file_exists($file) && $isBooPHPClass) {
				// header("Location: /404");
				throw new ErrorException("class '{$class}' not exists!", 1001);
				return;
			}
			require_once $file;
		}
	}

	/**
	 * 是否是项目类
	 */
	private static function _isProject($class) {
		$classPrefix = array("Smarty_Internal_", "PHPExcel");
		$isTrue = true;
		foreach($classPrefix as $prefix) {
			if (stripos($class, $prefix) !== false) {
				$isTrue = false;
				break;
			}
		}
		return $isTrue;
	}
}

spl_autoload_register(array("BooAutoload", "load"));
