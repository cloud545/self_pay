<?php
/**
 * BooPHP
 * Upload
 */
class BooUpload {
	const FILE_IS_NULL = 1;
	const FILE_IS_EXCEED_SIZE = 2;
	const FILE_IS_NOT_ALLOWED = 3;
	const FILE_IS_NOT_XY = 4;

	private static $_single = array();
	private static $_allowableFileTypes = array(
		"image" => array("image/gif", "image/png", "image/jpeg", "image/pjpeg", "image/bmp"),
		"zip" => array("application/zip", "application/x-tar", "application/octet-stream"),
		"doc" => array("application/pdf", "application/msword"),
		"txt" => array("text/plain", "text/html", "text/css", "text/javascript", "text/php"),
		"audio" => array(),
		"video" => array(),
	);
	private $_fileTypes = array();
	private $_fileSize = 0;
	private $_xy = '';


	public static function init($fileTypes, $size = 0, $xy='') {
		$key = $fileTypes . $size . $xy;
		if (!isset(self::$_single[$key])) {
			self::$_single[$key] = new self($fileTypes, $size, $xy);
		}
		return self::$_single[$key];
	}

	public function upload($files, $toDir, $toFileName = null) {
		if (($res = $this->_checkFile($files)) !== true) return $res;
		$prefix = date("Ymd_Hi", BooApp::$now);
		if (isset($files["name"])) {
			if (!$toFileName) {
				$now = microtime(true);
				$toFileName = "{$now}_{$files["name"]}";
			} else {
				$position = strripos($files["name"], ".");
				$suffix = substr($files["name"], $position);
				$toFileName .= $suffix;
			}
			$toFile = "{$toDir}/{$toFileName}";
			BooFile::cp($files["tmp_name"], $toFile);
			return $toFile;
		} else {
			$toFiles = array();
			foreach($files as $key => $file) {
				if (!$toFileName) {
					$fileName = "{$prefix}_{$file["name"]}";
				} else {
					$fileName = $toFileName[$key];
				}
				$toFile = "{$toDir}/{$fileName}";
				$toFiles[] = $toFile;
				BooFile::cp($file["tmp_name"], $toFile);
			}
			return $toFiles;
		}
	}

	public function checkFile($files) {
		return $this->_checkFile($files);
	}

	private function __construct($fileTypes, $size, $xy) {
		$fileTypes = explode(",", $fileTypes);
		if ($fileTypes[0] != "*") {
			foreach($fileTypes as $fileType) {
				$this->_fileTypes = array_merge($this->_fileTypes, self::$_allowableFileTypes[$fileType]);
			}
		} else {
			foreach(self::$_allowableFileTypes as $fileTypes) {
				$this->_fileTypes = array_merge($this->_fileTypes, $fileTypes);
			}
		}

		if ($size) {
			$this->_fileSize = $size;
		} else {
			$maxFileSize = ini_get("upload_max_filesize");
			$fileSize = substr($maxFileSize, 0, -1);
			$fileSizeUnit = strtolower(substr($maxFileSize, -1));
			$sizes = array("k" => 1024, "m" => 1048576, "g" => 1073741824);
			$fileSize = $fileSize * $sizes[$fileSizeUnit];
			$this->_fileSize = $fileSize;
		}
		$this->_xy = $xy;
	}

	private function _checkFile($files) {
		if (!$files && !is_array($files)) return self::FILE_IS_NULL;
		$isCanUpload = true;
		if ($files) {
			
			if (isset($files["name"])) {
				if( ($auth = $this->_auth($files)) ) {
					return $auth;
				}
			} else {
				foreach($files as $file) {
					if( ($auth = $this->_auth($file)) ) {
						return $auth;
					}
				}
			}
		}
		return $isCanUpload;
	}

	//判断文件 是否可以上传
	private function _auth($file) {
		if (!in_array($file["type"], $this->_fileTypes)) return self::FILE_IS_NOT_ALLOWED;
		if ($file["size"] > $this->_fileSize) return self::FILE_IS_EXCEED_SIZE;
		if($this->_xy) {
			$xy = explode('x', $this->_xy);
			$wh = getimagesize($file['tmp_name']);
			if($wh[0] != $xy[0] || $wh[1] != $xy[1]) {
				return self::FILE_IS_NOT_XY;
			}
		}
	}
}
