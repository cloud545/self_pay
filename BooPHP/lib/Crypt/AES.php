<?php
/**
 * AES
 */
namespace BooCrypt;
use BooCrypt\AES;

class AES {
	private $_cipher; // 算法名称 MCRYPT_RIJNDAEL_128 MCRYPT_RIJNDAEL_192 MCRYPT_RIJNDAEL_256
	private $_key; // 私钥
	private $_mode; // 模式 "ecb"，"cbc"，"cfb"，"ofb"，"nofb", "stream"

	public function __construct($key, $cipher = MCRYPT_RIJNDAEL_128, $mode = "cbc") {
		$this->_key = $key;
		$this->_cipher = $cipher;
		$this->_mode = $mode;
	}

	public function encode($data) {
		$data = str_pad($data, (16*(floor(strlen($data) / 16)+(strlen($data) % 16==0?2:1))), chr(16-(strlen($data) % 16)));
		$ciphertext = \mcrypt_encrypt($this->_cipher, $this->_key, $data, $this->_mode);
		return \base64_encode($ciphertext);
	}

	public function decode($data) {
		$data = \base64_decode($data);
		return \mcrypt_decrypt($this->_cipher, $this->_key, $data, $this->_mode);
	}
}
