<?php
/**
 * BooPHP
 * MobileDetect
 */
$dir = dirname(__DIR__);
require_once $dir . "/GoogleAuthor/gauthor.php";

class BooGoogleAuthor {
    private static $_detect = null;

    public static function getInstance(){
        if (!self::$_detect) {
            self::$_detect = new gauthor();
        }

        return self::$_detect;
    }

}
