<?php
/**
 * BooPHP
 * Session
 */
class BooSession {
    private static $_sessionType = '';

	public static function start($saveType = "file", $sessionFilePathOrSessionDao = '') {

		if (!self::$_sessionType) {
			$saveType = ucfirst($saveType);
            self::$_sessionType = $saveType;
            $class = "BooSession\\" . $saveType;
            $class::start($sessionFilePathOrSessionDao);
		}
	}

    public static function getSessionId() {

        if (!self::$_sessionType) {
            throw new ErrorException("session is not start! call BooSession::start()", "3001");
        }
        $class = "BooSession\\" . self::$_sessionType;
        return $class::getSessionId();
    }

	public static function get($key) {
		if (!self::$_sessionType) {
			throw new ErrorException("session is not start! call BooSession::start()", "3001");
		}
        $class = "BooSession\\" . self::$_sessionType;
		return $class::get($key);
	}

	public static function set($key, $val = null) {
        if (!self::$_sessionType) {
            throw new ErrorException("session is not start! call BooSession::start()", "3001");
        }

        $class = "BooSession\\" . self::$_sessionType;
        return $class::set($key, $val);
	}

	public static function del($key) {
        if (!self::$_sessionType) {
            throw new ErrorException("session is not start! call BooSession::start()", "3001");
        }

        $class = "BooSession\\" . self::$_sessionType;
        return $class::del($key);
	}

    public static function destroy() {
        if (!self::$_sessionType) {
            throw new ErrorException("session is not start! call BooSession::start()", "3001");
        }

        $class = "BooSession\\" . self::$_sessionType;
        return $class::destroy();
    }
}
