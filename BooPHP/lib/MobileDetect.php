<?php
/**
 * BooPHP
 * MobileDetect
 */
$dir = dirname(__DIR__);
require_once $dir . "/Mobile-Detect-2.8.31/Mobile_Detect.php";

class BooMobileDetect {
    private static $_detect = null;

    public static function getInstance(){
        if (!self::$_detect) {
            self::$_detect = new Mobile_Detect;
        }

        return self::$_detect;
    }

}
