<?php
/**
 * BooPHP
 * Redis
 */
abstract class BooRedis {
	private $_redids;

	public function __call($method, $params){
		if ($this->_redids) {
			return call_user_func_array(array($this->_redids, $method), $params);
		} else {
			return false;
		}
	}

	protected function _connect($redisServer, $pconnect = false) {
		$redis = new Redis();
		if (!$pconnect) {
			$res = $redis->connect($redisServer['host'], $redisServer['port'], $redisServer['timeout']);
		} else {
			$res = $redis->pconnect($redisServer['host'], $redisServer['port'], $redisServer['timeout']);
		}
		if (isset($redisServer['password']) && $redisServer['password']) {
			$redis->auth($redisServer['password']);
		}
		
		if ($res) $this->_redids = $redis;
	}
}
