<?php
/**
 * 文件形式存储session
 */
namespace BooSession;
use BooSession\File;

class File {
	private static $_session = null;
	private static $_sessionPath;

    public static function start($sessionFilePath) {
        if (!$sessionFilePath) {
            throw new \ErrorException('$sessionFilePath can not be empty!', 2001);
        }

        session_start();

        if ($_REQUEST['sessionId']) {
            $sessionId = $_REQUEST['sessionId'];
        } else {
            $sessionId = session_id();
        }

        self::$_sessionPath = $sessionFilePath;
        $dir = substr(md5($sessionId), 10, 3);
        self::$_sessionPath .= "/{$dir}";

        $booSessionId = "sess_{$sessionId}";
        $sessionData = self::read($booSessionId);
        if (!$sessionData) {
            $sessionData = json_encode(array('BooPHPSessionId' => $sessionId));
        }

        self::write($booSessionId, $sessionData);
    }

    public static function getSessionId() {

        if (isset($_REQUEST['sessionId']) && $_REQUEST['sessionId']) {
            $sessionId = $_REQUEST['sessionId'];
        } else {
            $sessionId = session_id();
        }

        return $sessionId;
    }

	public static function get($key, $val = null) {

        if (!self::$_sessionPath) throw new \ErrorException('you must call BooSession\File::start($sessionFilePath) first!', 2001);

        if ($_REQUEST['sessionId']) {
            $sessionId = $_REQUEST['sessionId'];
        } else {
            $sessionId = session_id();
        }

        $session = null;
        $booSessionId = "sess_{$sessionId}";
        $sessionData = self::read($booSessionId);
        if ($sessionData) {
            $sessionData = json_decode($sessionData, true);

            if (isset($sessionData[$key])) {
                $session = $sessionData[$key];
            }
            if (!$session && $val) {
                $session = $val;
            }
        }

        return $session;
	}

	public static function set($key, $val = null) {

        if (!self::$_sessionPath) throw new \ErrorException('you must call BooSession\File::start($sessionFilePath) first!', 2001);

        if ($_REQUEST['sessionId']) {
            $sessionId = $_REQUEST['sessionId'];
        } else {
            $sessionId = session_id();
        }

        $booSessionId = "sess_{$sessionId}";
        $sessionData = self::read($booSessionId);
        if (!$sessionData) {
            $sessionData = array();
        } else {
            $sessionData = json_decode($sessionData, true);
        }

        if (is_array($key)) {
            foreach ($key as $key1 => $value1) {
                if ($value1 !== null) {
                    $sessionData[$key1] = $value1;
                } else {
                    unset($sessionData[$key1]);
                }
            }
        } else {
            if ($val !== null) {
                $sessionData[$key] = $val;
            } else {
                unset($sessionData[$key]);
            }

        }

        self::write($booSessionId, json_encode($sessionData));

        return true;
	}

	public static function del($key) {

        if (!self::$_sessionPath) throw new \ErrorException('you must call BooSession\File::start($sessionFilePath) first!', 2001);

        if ($_REQUEST['sessionId']) {
            $sessionId = $_REQUEST['sessionId'];
        } else {
            $sessionId = session_id();
        }

        $booSessionId = "sess_{$sessionId}";
        $sessionData = self::read($booSessionId);
        if (!$sessionData) {
            return true;
        }
        $sessionData = json_decode($sessionData, true);

        if (!$sessionData[$key]) {
            return true;
        }

        unset($sessionData[$key]);

        self::write($booSessionId, json_encode($sessionData));

        return true;
	}

    public function destroy() {
        if (!self::$_sessionPath) throw new \ErrorException('you must call BooSession\File::start($sessionFilePath) first!', 2001);

        if ($_REQUEST['sessionId']) {
            $sessionId = $_REQUEST['sessionId'];
        } else {
            $sessionId = session_id();
        }

        $booSessionId = "sess_{$sessionId}";
        $file = self::$_sessionPath . "/{$booSessionId}";
        \BooFile::del($file);

        session_destroy();
    }

	private static function read($id) {

        $lifeTime = ini_get("session.gc_maxlifetime");

        $now = \BooApp::$now;
        $files = \BooFile::read(self::$_sessionPath);
        if ($files) {
            foreach($files as $file) {
                $fileTime = filemtime($file);
                if ($fileTime + $lifeTime < $now) {
                    \BooFile::del($file);
                }
            }
        }

		$data = "";
		$file = self::$_sessionPath . "/{$id}";
		if (file_exists($file)) {
			$fileTime = filemtime($file);
			if ($fileTime + $lifeTime > \BooApp::$now) {
				touch($file);
				$data = \BooFile::read($file);
			}
		}
		return $data;
	}

    private static function write($id, $data) {
		\BooFile::write(self::$_sessionPath . "/{$id}", $data);
		return true;
	}

}
