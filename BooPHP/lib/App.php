<?php
/**
 * BooPHP
 * App
 */
class BooApp {
	public static $now = 0;

	private $_target = "target"; // 默认路由名称
	private $_index = "Index"; // 默认类

	public function __construct($target = null, $index = null) {
		self::$now = $_SERVER["REQUEST_TIME"];
		if ($target) $this->_target = $target;
		if ($index) $this->_index = $index;
	}

	public function run($classType, $classMethod = '', $param = array()) {
		try {
			$this->_checkDefined(); // 检查必须要定义的常量
			$classType .= "_";
			if (!$classMethod) {
                $classMethod = BooVar::request($this->_target);
            }

			$argv = array();
			if (!$classMethod) {
				if (PHP_SAPI == "cli") {
					$argv = BooVar::server("argv");
					if (!empty($argv[1])) {
						$classMethod = $classType . $argv[1];
						unset($argv[1]);
					}
					unset($argv[0]);
				} else {
					$classMethod = "{$classType}{$this->_index}";
				}
			} else {
				$classMethod = $classType . $classMethod;
				if ($param) {
                    $argv = $param;
                }
			}
			if (!$classMethod) throw new ErrorException("target is null!", 101);
			$classMethod = trim($classMethod, "/");
            $classMethod = str_replace("/", "_", $classMethod);
			BooController::get($classMethod, $argv);
		} catch (Exception $e) {
			$this->_output($e);
		}
	}

	public function __destruct() {
		try {
			BooDebug::writeDebugInfo();
			BooLog::write();
		} catch (Exception $e) {
			$this->_output($e);
		}
	}

	private function _output($e) {
		$code = $e->getCode();
		$data["msg"] = $e->getMessage();
		$data["trace"] = $e->getTraceAsString();
		$output = array("code" => $code, "data" => $data);
		if (PHP_SAPI != "cli") {
			if (BooVar::server("APP_ENV") == "dev") {
				header("Content-type: application/json;charset=utf-8");
				echo json_encode($output);
				return;
			} else {
				BooLog::fatal($data["msg"]);
			}
		} else {
			echo json_encode($output) . "\n";
			return;
		}
	}

	private function _checkDefined() {
		if (!defined('PATH_BOOPHP')) throw new ErrorException("PATH_BOOPHP is not defined!", 2001);
		if (!defined('PATH_CLASS')) throw new ErrorException("PATH_CLASS is not defined!", 2001);
		if (!defined("PATH_LOG")) throw new ErrorException("PATH_LOG is not defined!", 2001);
	}
}
