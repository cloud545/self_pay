<?php
/**
 * BooPHP
 * View
 */
$dir = dirname(__DIR__);
require_once $dir . "/PHPQrcode/phpqrcode.php";
require_once $dir . "/PHPQrcode/reader/QrReader.php";

class BooQrcode {
	
	public static function getImg($url, $outfile = false, $errorCorrectionLevel='L', $matrixPointSize = '4') {
		
		//$errorCorrectionLevel = "L";
		//$matrixPointSize = "4";
		QRcode::png($url, $outfile, $errorCorrectionLevel, $matrixPointSize);
	}

	public static function reader($url) {

        $qrcode = new QrReader($url);
        $text = $qrcode->text();

        return $text;
    }
}
