<?php
date_default_timezone_set('Asia/Shanghai');
set_time_limit(0);
error_reporting(E_ALL^E_NOTICE);

$sDir = dirname(__DIR__);

define("PATH_BOOPHP", "{$sDir}/BooPHP/lib");

define("PATH_ROOT", "{$sDir}/");
define("PATH_CLASS", "{$sDir}/class");
define("PATH_CONFIG", "{$sDir}/config");
define("PATH_LOG", "{$sDir}/logs");
define("PATH_TPL", "{$sDir}/tpl");
define("PATH_DATA", "{$sDir}/data");
define("PATH_WEB", "{$sDir}/www_zhkj2019");

$_SERVER["APP_ENV"] = 'zhkj2019';

require_once PATH_BOOPHP . "/Autoload.php";

$oApp = new BooApp();
$oApp->run("Crontab");