<?php
$config = array(
    'webCdnUrl' => '/misc',
    //'webCdnUrl' => 'https://www.best1pay.com/misc',
    //'webCdnUrl' => 'https://ice.lalajj.com:444/payapril/misc',
    'bankList' => array(
        'ABC' => '中国农业银行',
        'BCCB' => '北京银行',
        'BCM' => '交通银行',
        'BJRCB' => '北京农商银行',
        'BOC' => '中国银行',
        'BOS' => '上海银行',
        'CCB' => '中国建设银行',
        'CEB' => '中国光大银行',
        'CIB' => '兴业银行',
        'CITIC' => '中信银行',
        'CMB' => '招商银行',
        'CMBC' => '中国民生银行',
        'GDB' => '广东发展银行',
        'HXB' => '华夏银行',
        'ICBC' => '中国工商银行',
        'PAB' => '平安银行',
        'PSBC' => '中国邮政储蓄银行',
        'SPDB' => '浦发银行',
        'HSB' => '徽商银行',
        'GHB' => '广东华兴银行',
    ),
    'noLogMenu' => array(
        '/admin/successRateList',
        '/admin/qrCode/successRateList',
        '/admin/logs/list',
        '/admin/logs/appList',
        '/admin/logs/proxyList',
        '/admin/logs/info',
        '/admin/website/adminMenu',
        '/admin/website/proxyMenu',
        '/admin/website/appMenu',
        '/appAdmin/logs/list',
        '/appAdmin/logs/info',
        '/proxyAdmin/logs/list',
        '/proxyAdmin/logs/info',
        '/qrCodeAdmin/logs/list',
        '/qrCodeAdmin/logs/info',
        '/qrCodeAdmin/index',
    ),
);

if ($_SERVER['HTTP_HOST'] == 'www.best1pay.com') {
    $config['webCdnUrl'] = '/misc';
}

return $config;
