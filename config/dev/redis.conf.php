<?php

$redisHost = '127.0.0.1';
$redisPort = 6379;
$redisPass = '';

return array(
    'default' => array(
        'host' => $redisHost,
        'port' => $redisPort,
        'timeout' => 0.5,
        'password' => $redisPass,
    ),
    'session' => array(
        'host' => $redisHost,
        'port' => $redisPort,
        'timeout' => 0.5,
        'password' => $redisPass,
    ),
    'appDataTable' => array(
        'host' => $redisHost,
        'port' => $redisPort,
        'timeout' => 0.5,
        'password' => $redisPass,
    ),
);