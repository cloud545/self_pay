!
    function(a) {
        "use strict";
        "function" == typeof define && define.amd ? define(["jquery", "../grid.base"], a) : a(jQuery)
    } (function(a) {
        a.jgrid = a.jgrid || {},
        a.jgrid.hasOwnProperty("regional") || (a.jgrid.regional = []),
            a.jgrid.regional.en = {
                defaults: {
                    recordtext: "第 {0} - {1} 条记录，共 {2} 条记录",
                    emptyrecords: "查无数据",
                    loadtext: "加载中...",
                    savetext: "保存中...",
                    pgtext: "当前页 {0} ，共 {1} 页",
                    pgfirst: "首页",
                    pglast: "尾页",
                    pgnext: "下一页",
                    pgprev: "上一页",
                    pgrecs: "每页显示记录数",
                    showhide: "收起/展开",
                    pagerCaption: "Grid::Page Settings",
                    pageText: "Page:",
                    recordPage: "每页显示记录数",
                    nomorerecs: "没有更多的数据...",
                    scrollPullup: "Pull up to load more...",
                    scrollPulldown: "Pull down to refresh...",
                    scrollRefresh: "Release to refresh..."
                },
                search: {
                    caption: "搜索",
                    Find: "查找",
                    Reset: "重置",
                    odata: [{
                        oper: "eq",
                        text: "等于"
                    },
                        {
                            oper: "ne",
                            text: "不等于"
                        },
                        {
                            oper: "lt",
                            text: "小于"
                        },
                        {
                            oper: "le",
                            text: "小于等于"
                        },
                        {
                            oper: "gt",
                            text: "大于"
                        },
                        {
                            oper: "ge",
                            text: "大于等于"
                        },
                        {
                            oper: "bw",
                            text: "begins with"
                        },
                        {
                            oper: "bn",
                            text: "does not begin with"
                        },
                        {
                            oper: "in",
                            text: "is in"
                        },
                        {
                            oper: "ni",
                            text: "is not in"
                        },
                        {
                            oper: "ew",
                            text: "ends with"
                        },
                        {
                            oper: "en",
                            text: "does not end with"
                        },
                        {
                            oper: "cn",
                            text: "contains"
                        },
                        {
                            oper: "nc",
                            text: "does not contain"
                        },
                        {
                            oper: "nu",
                            text: "is null"
                        },
                        {
                            oper: "nn",
                            text: "is not null"
                        }],
                    groupOps: [{
                        op: "AND",
                        text: "all"
                    },
                        {
                            op: "OR",
                            text: "any"
                        }],
                    operandTitle: "Click to select search operation.",
                    resetTitle: "Reset Search Value"
                },
                edit: {
                    addCaption: "添加数据",
                    editCaption: "编辑数据",
                    bSubmit: "确定",
                    bCancel: "撤销",
                    bClose: "关闭",
                    saveData: "数据有变更! 是否保存?",
                    bYes: "是",
                    bNo: "否",
                    bExit: "撤销",
                    msg: {
                        required: "Field is required",
                        number: "Please, enter valid number",
                        minValue: "value must be greater than or equal to ",
                        maxValue: "value must be less than or equal to",
                        email: "is not a valid e-mail",
                        integer: "Please, enter valid integer value",
                        date: "Please, enter valid date value",
                        url: "is not a valid URL. Prefix required ('http://' or 'https://')",
                        nodefined: " is not defined!",
                        novalue: " return value is required!",
                        customarray: "Custom function should return array!",
                        customfcheck: "Custom function should be present in case of custom checking!"
                    }
                },
                view: {
                    caption: "数据详情",
                    bClose: "关闭"
                },
                del: {
                    caption: "删除",
                    msg: "确定删除选中的数据?",
                    bSubmit: "删除",
                    bCancel: "撤销"
                },
                nav: {
                    edittext: "",
                    edittitle: "编辑",
                    addtext: "",
                    addtitle: "添加",
                    deltext: "",
                    deltitle: "删除",
                    searchtext: "",
                    searchtitle: "查找数据",
                    refreshtext: "",
                    refreshtitle: "刷新数据",
                    alertcap: "警告",
                    alerttext: "请先选择数据",
                    viewtext: "",
                    viewtitle: "查看选中数据",
                    savetext: "",
                    savetitle: "保存",
                    canceltext: "",
                    canceltitle: "撤销",
                    selectcaption: "进行中..."
                },
                col: {
                    caption: "Select columns",
                    bSubmit: "Ok",
                    bCancel: "Cancel"
                },
                errors: {
                    errcap: "Error",
                    nourl: "No url is set",
                    norecords: "No records to process",
                    model: "Length of colNames <> colModel!"
                },
                formatter: {
                    integer: {
                        thousandsSeparator: ",",
                        defaultValue: "0"
                    },
                    number: {
                        decimalSeparator: ".",
                        thousandsSeparator: ",",
                        decimalPlaces: 2,
                        defaultValue: "0.00"
                    },
                    currency: {
                        decimalSeparator: ".",
                        thousandsSeparator: ",",
                        decimalPlaces: 2,
                        prefix: "",
                        suffix: "",
                        defaultValue: "0.00"
                    },
                    date: {
                        dayNames: ["Sun", "Mon", "Tue", "Wed", "Thr", "Fri", "Sat", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                        monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                        AmPm: ["am", "pm", "AM", "PM"],
                        S: function(a) {
                            return 11 > a || a > 13 ? ["st", "nd", "rd", "th"][Math.min((a - 1) % 10, 3)] : "th"
                        },
                        srcformat: "Y-m-d",
                        newformat: "n/j/Y",
                        parseRe: /[#%\\\/:_;.,\t\s-]/,
                        masks: {
                            ISO8601Long: "Y-m-d H:i:s",
                            ISO8601Short: "Y-m-d",
                            ShortDate: "n/j/Y",
                            LongDate: "l, F d, Y",
                            FullDateTime: "l, F d, Y g:i:s A",
                            MonthDay: "F d",
                            ShortTime: "g:i A",
                            LongTime: "g:i:s A",
                            SortableDateTime: "Y-m-d\\TH:i:s",
                            UniversalSortableDateTime: "Y-m-d H:i:sO",
                            YearMonth: "F, Y"
                        },
                        reformatAfterEdit: !1,
                        userLocalTime: !1
                    },
                    baseLinkUrl: "",
                    showAction: "",
                    target: "",
                    checkbox: {
                        disabled: !0
                    },
                    idName: "id"
                }
            }
    });