<?php
class Crontab_report extends Crontab {

	protected function _do() {

        $crontabLogFileName = PATH_WEB . '/crontablog/report.log';
        BooFile::write($crontabLogFileName, date('Y-m-d H:i:s') . "\n", 'a');

        // 检查是否已有相同CLI在运行中
        $sLocksFileName = PATH_WEB . '/run/report.locks';
        if (BooFile::isExist($sLocksFileName)) {
            $stat = stat($sLocksFileName);
            if ((time() - $stat['mtime']) > 180) {
                echo "文件被锁超过180秒，被强制删除，文件为 report.locks \n";
                @unlink($sLocksFileName);
            } else {
                echo "[d] [" . date('Y-m-d H:i:s') . "] CLI report 正在运行 \n";
                exit();
            }
        }

        BooFile::write($sLocksFileName, time(), 'a'); // CLI 独占锁


        $argv = BooVar::server("argv");
        $reportDate = $argv[2] ? $argv[2] : date('Y-m-d', strtotime('-1 day'));
        $this->systemReport($reportDate);
        $this->proxyReport($reportDate);
        $this->appReport($reportDate);

        @unlink($sLocksFileName);
        echo "统计报表完成，执行操作为 report \n";
        exit();
	}

	private function systemReport($reportDate) {

        $startTime = $reportDate . " 00:00:00";
        $endTime = date('Y-m-d 00:00:00', strtotime($startTime) + 86400);
        $intNowDate = date('Ymd', strtotime($startTime));

        $yestodayDate = date('Y-m-d', strtotime($startTime) - 86400);
        $nowDateTime = date('Y-m-d H:i:s');
        $payObj = BooController::get('Obj_Admin_Pay');
        $reportObj = BooController::get('Obj_Admin_Report');
        $withdrawalObj = BooController::get('Obj_Admin_Withdrawal');
        $orderObj = BooController::get('Obj_Admin_Orders');

        // 日报表
        $paySumInfo = $payObj->getSum(0, 0, 0, 0, $startTime, $endTime);
        $orderNumber = $payObj->getCount(0, 0, 0, 0, $startTime, $endTime);
        $withdrawalSumInfo = $withdrawalObj->getSum(2, $startTime, $endTime);

        // 管理员添加资金
        $orderSumInfo4 = $orderObj->getSum(4, $startTime, $endTime);
        // 管理员扣减资金
        $orderSumInfo5 = $orderObj->getSum(5, $startTime, $endTime);
        // 管理员冻结资金
        //$orderSumInfo6 = $orderObj->getSum(6, $startTime, $endTime);
        // 管理员解冻资金
        $orderSumInfo7 = $orderObj->getSum(7, $startTime, $endTime);

        $delBalance = $orderSumInfo4['sumAmount'] + abs($orderSumInfo7['sumAmount']) + $orderSumInfo5['sumAmount'];// - $orderSumInfo6['sumAmount'];

        $yestodayRsInfo = $reportObj->getInfo( $yestodayDate);
        $balance = $yestodayRsInfo['balance'] + $paySumInfo['sumFee'] - $withdrawalSumInfo['sumAmount'] - $withdrawalSumInfo['sumFee'] + $delBalance;

        $tmpRsInfo = $reportObj->getInfo($reportDate);
        if (!$tmpRsInfo) {
            $insertData = array();
            $insertData['order_number'] = $orderNumber;
            $insertData['real_amount'] = $paySumInfo['sumRealAmount'];
            $insertData['fee'] = $paySumInfo['sumFee'];
            $insertData['balance'] = $balance;
            $insertData['report_date'] = $reportDate;
            $insertData['create_time'] = $nowDateTime;
            $reportObj->insert($insertData);
        } else {
            $updateData = array();
            $updateData['order_number'] = $orderNumber;
            $updateData['real_amount'] = $paySumInfo['sumRealAmount'];
            $updateData['fee'] = $paySumInfo['sumFee'];
            $updateData['balance'] = $balance;
            $reportObj->update($tmpRsInfo['id'], $updateData);
        }

        $orderInfo = $orderObj->getInfoByDate(1, $intNowDate);
        if (!$orderInfo) {
            // 计算记录写入帐变表
            $insertData = array();
            $insertData['amount'] = $paySumInfo['sumFee'];
            $insertData['type'] = 1;
            $insertData['start_time'] = $startTime;
            $insertData['end_time'] = $endTime;
            $insertData['create_time'] = $endTime;
            $insertData['create_date'] = $intNowDate;
            $insertData['current_balance'] = $balance;
            $orderObj->insert($insertData);
        } else {
            $updata = array();
            $updata['amount'] = $paySumInfo['sumFee'];
            $updata['end_time'] = $endTime;
            $updata['current_balance'] = $balance;
            $orderObj->update($orderInfo['id'], $updata);
        }

        $nowTime = time();
        $nowMonth = date('Y-m', strtotime($startTime));
        $nowMonthStartTime = $nowMonth . "-01 00:00:00";
        $nowMonthEndTime = date('Y-m-d H:i:s', strtotime("$nowMonthStartTime +1 month")-1);
        if(strtotime($nowMonthEndTime)> strtotime($nowDateTime)){
            $nowMonthEndTime = $nowDateTime;
        }
        $nowMonthReportLastMonthTime = strtotime($nowMonth . "-04 00:00:00");
        $lastMonth = date('Y-m', strtotime($nowMonth . "-01 00:00:00") - 86400);
        $lastMonthStartTime = $lastMonth . "-01 00:00:00";
        $lastMonthEndTime = date('Y-m-d H:i:s', strtotime($nowMonth . "-01 00:00:00") - 1);
        $reportMonthObj = BooController::get('Obj_Admin_ReportMonth');

        // 上个月的报表
        if ($nowTime <= $nowMonthReportLastMonthTime) {
            $reportSumInfo = $reportObj->getSum($lastMonthStartTime, $lastMonthEndTime);
            $tmpRsInfo = $reportMonthObj->getInfo($lastMonth);
            if (!$tmpRsInfo) {
                $insertData = array();
                $insertData['order_number'] = $reportSumInfo['orderNum'];
                $insertData['real_amount'] = $reportSumInfo['sumRealAmount'];
                $insertData['fee'] = $reportSumInfo['sumFee'];
                $insertData['report_month'] = $lastMonth;
                $insertData['create_time'] = $nowDateTime;
                $reportMonthObj->insert($insertData);
            } else {
                $updateData = array();
                $updateData['order_number'] = $reportSumInfo['orderNum'];
                $updateData['real_amount'] = $reportSumInfo['sumRealAmount'];
                $updateData['fee'] = $reportSumInfo['sumFee'];
                $reportMonthObj->update($tmpRsInfo['id'], $updateData);
            }
        }

        // 本月报表
        $reportSumInfo = $reportObj->getSum($nowMonthStartTime, $nowMonthEndTime);
        $tmpRsInfo = $reportMonthObj->getInfo($nowMonth);
        if (!$tmpRsInfo) {
            $insertData = array();
            $insertData['order_number'] = $reportSumInfo['orderNum'];
            $insertData['real_amount'] = $reportSumInfo['sumRealAmount'];
            $insertData['fee'] = $reportSumInfo['sumFee'];
            $insertData['report_month'] = $nowMonth;
            $insertData['create_time'] = $nowDateTime;
            $reportMonthObj->insert($insertData);
        } else {
            $updateData = array();
            $updateData['order_number'] = $reportSumInfo['orderNum'];
            $updateData['real_amount'] = $reportSumInfo['sumRealAmount'];
            $updateData['fee'] = $reportSumInfo['sumFee'];
            $reportMonthObj->update($tmpRsInfo['id'], $updateData);
        }

        // 修正隔几天补单的数据
        $nowMonthReportstartTime = strtotime($nowMonth . "-02 00:00:00");
        if ($nowTime > $nowMonthReportstartTime) {

            for ($i = strtotime($nowMonthStartTime); $i < strtotime($endTime); $i += 86400) {

                // 日报表
                $paySumInfo = $payObj->getSum(0, 0, 0, 0, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));
                $orderNumber = $payObj->getCount(0, 0, 0, 0, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));
                $withdrawalSumInfo = $withdrawalObj->getSum(2, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));

                // 管理员添加资金
                $orderSumInfo4 = $orderObj->getSum(4, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));
                // 管理员扣减资金
                $orderSumInfo5 = $orderObj->getSum(5, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));
                // 管理员解冻资金
                $orderSumInfo7 = $orderObj->getSum(7, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));

                $delBalance = $orderSumInfo4['sumAmount'] + abs($orderSumInfo7['sumAmount']) + $orderSumInfo5['sumAmount'];

                $tmpYestodayDate = date('Y-m-d', $i - 86400);
                $yestodayRsInfo = $reportObj->getInfo($tmpYestodayDate);

                $balance = $yestodayRsInfo['balance'] + $paySumInfo['sumFee'] - $withdrawalSumInfo['sumAmount'] - $withdrawalSumInfo['sumFee'] + $delBalance;

                $tmpReportDate = date('Y-m-d', $i);
                $tmpRsInfo = $reportObj->getInfo($tmpReportDate);
                if ($tmpRsInfo && (
                    $tmpRsInfo['order_number'] != $orderNumber ||
                    $tmpRsInfo['real_amount'] != $paySumInfo['sumRealAmount'] ||
                    $tmpRsInfo['fee'] != $paySumInfo['sumFee'] ||
                    $tmpRsInfo['balance'] != $balance
                )) {
                    $updateData = array();
                    $updateData['order_number'] = $orderNumber;
                    $updateData['real_amount'] = $paySumInfo['sumRealAmount'];
                    $updateData['fee'] = $paySumInfo['sumFee'];
                    $updateData['balance'] = $balance;
                    $reportObj->update($tmpRsInfo['id'], $updateData);
                }
            }
        }

        return true;
    }

    private function proxyReport($reportDate) {

        $startTime = $reportDate . " 00:00:00";
        $endTime = date('Y-m-d 00:00:00', strtotime($startTime) + 86400);
        $intNowDate = date('Ymd', strtotime($startTime));

        $proxyObj = BooController::get('Obj_Proxy_Info');
        $proxyList = $proxyObj->getAllList();

        $yestodayDate = date('Y-m-d', strtotime($startTime) - 86400);
        $nowDateTime = date('Y-m-d H:i:s');
        $payObj = BooController::get('Obj_Proxy_Pay');
        $reportObj = BooController::get('Obj_Proxy_Report');
        $withdrawalObj = BooController::get('Obj_Proxy_Withdrawal');
        $orderObj = BooController::get('Obj_Proxy_Orders');

        $nowTime = time();
        $nowMonth = date('Y-m', strtotime($startTime));
        $nowMonthStartTime = $nowMonth . "-01 00:00:00";
        $nowMonthEndTime = date('Y-m-d H:i:s', strtotime("$nowMonthStartTime +1 month")-1);
        if(strtotime($nowMonthEndTime)> strtotime($nowDateTime)){
            $nowMonthEndTime = $nowDateTime;
        }
        $nowMonthReportLastMonthTime = strtotime($nowMonth . "-04 00:00:00");
        $lastMonth = date('Y-m', strtotime($nowMonth . "-01 00:00:00") - 86400);
        $lastMonthStartTime = $lastMonth . "-01 00:00:00";
        $lastMonthEndTime = date('Y-m-d H:i:s', strtotime($nowMonth . "-01 00:00:00") - 1);
        $reportMonthObj = BooController::get('Obj_Proxy_ReportMonth');

        foreach ($proxyList as $tmpInfo) {

            if (!$tmpInfo['proxy_id']) {
                continue;
            }

            // 日报表
            $paySumInfo = $payObj->getSum($tmpInfo['proxy_id'], 0, 0, 0, $startTime, $endTime);
            $orderNumber = $payObj->getCount($tmpInfo['proxy_id'], 0, 0, 0, $startTime, $endTime);
            $withdrawalSumInfo = $withdrawalObj->getSum($tmpInfo['proxy_id'], 2, $startTime, $endTime);

            // 管理员添加资金
            $orderSumInfo4 = $orderObj->getSum($tmpInfo['proxy_id'], 4, $startTime, $endTime);
            // 管理员扣减资金
            $orderSumInfo5 = $orderObj->getSum($tmpInfo['proxy_id'], 5, $startTime, $endTime);
            // 管理员冻结资金
            //$orderSumInfo6 = $orderObj->getSum($tmpInfo['proxy_id'], 6, $startTime, $endTime);
            // 管理员解冻资金
            $orderSumInfo7 = $orderObj->getSum($tmpInfo['proxy_id'], 7, $startTime, $endTime);

            $delBalance = $orderSumInfo4['sumAmount'] + abs($orderSumInfo7['sumAmount']) + $orderSumInfo5['sumAmount'];// - $orderSumInfo6['sumAmount'];

            $yestodayRsInfo = $reportObj->getInfo($tmpInfo['proxy_id'], $yestodayDate);
            $balance = $yestodayRsInfo['balance'] + $paySumInfo['sumFee'] - $withdrawalSumInfo['sumAmount'] - $withdrawalSumInfo['sumFee'] + $delBalance;

            $tmpRsInfo = $reportObj->getInfo($tmpInfo['proxy_id'], $reportDate);
            if (!$tmpRsInfo) {
                $insertData = array();
                $insertData['proxy_id'] = $tmpInfo['proxy_id'];
                $insertData['proxy_name'] = $tmpInfo['proxy_name'];
                $insertData['order_number'] = $orderNumber;
                $insertData['real_amount'] = $paySumInfo['sumRealAmount'];
                $insertData['fee'] = $paySumInfo['sumFee'];
                $insertData['balance'] = $balance;
                $insertData['report_date'] = $reportDate;
                $insertData['create_time'] = $nowDateTime;
                $reportObj->insert($insertData);
            } else {
                $updateData = array();
                $updateData['order_number'] = $orderNumber;
                $updateData['real_amount'] = $paySumInfo['sumRealAmount'];
                $updateData['fee'] = $paySumInfo['sumFee'];
                $updateData['balance'] = $balance;
                $reportObj->update($tmpRsInfo['id'], $updateData);
            }

            $orderInfo = $orderObj->getInfoByDate($tmpInfo['proxy_id'], 1, $intNowDate);
            if (!$orderInfo) {
                // 计算记录写入帐变表
                $insertData = array();
                $insertData['proxy_id'] = $tmpInfo['proxy_id'];
                $insertData['proxy_name'] = $tmpInfo['proxy_name'];
                $insertData['amount'] = $paySumInfo['sumFee'];
                $insertData['type'] = 1;
                $insertData['start_time'] = $startTime;
                $insertData['end_time'] = $endTime;
                $insertData['create_time'] = $endTime;
                $insertData['create_date'] = $intNowDate;
                $insertData['current_balance'] = $balance;
                $orderObj->insert($insertData);
            } else {
                $updata = array();
                $updata['amount'] = $paySumInfo['sumFee'];
                $updata['end_time'] = $endTime;
                $updata['current_balance'] = $balance;
                $orderObj->update($orderInfo['id'], $updata);
            }

            // 上个月的报表
            if ($nowTime <= $nowMonthReportLastMonthTime) {
                $paySumInfo = $payObj->getSum($tmpInfo['proxy_id'], 0, 0, 0, $lastMonthStartTime, $lastMonthEndTime);
                $orderNumber = $payObj->getCount($tmpInfo['proxy_id'], 0, 0, 0, $lastMonthStartTime, $lastMonthEndTime);

                $tmpRsInfo = $reportMonthObj->getInfo($tmpInfo['proxy_id'], $lastMonth);
                if (!$tmpRsInfo) {
                    $insertData = array();
                    $insertData['proxy_id'] = $tmpInfo['proxy_id'];
                    $insertData['proxy_name'] = $tmpInfo['proxy_name'];
                    $insertData['order_number'] = $orderNumber;
                    $insertData['real_amount'] = $paySumInfo['sumRealAmount'];
                    $insertData['fee'] = $paySumInfo['sumFee'];
                    $insertData['report_month'] = $lastMonth;
                    $insertData['create_time'] = $nowDateTime;
                    $reportMonthObj->insert($insertData);
                } else {
                    $updateData = array();
                    $updateData['order_number'] = $orderNumber;
                    $updateData['real_amount'] = $paySumInfo['sumRealAmount'];
                    $updateData['fee'] = $paySumInfo['sumFee'];
                    $reportMonthObj->update($tmpRsInfo['id'], $updateData);
                }
            }

            // 本月报表
            $paySumInfo = $payObj->getSum($tmpInfo['proxy_id'], 0, 0, 0, $nowMonthStartTime, $nowMonthEndTime);
            $orderNumber = $payObj->getCount($tmpInfo['proxy_id'], 0, 0, 0, $nowMonthStartTime, $nowMonthEndTime);

            $tmpRsInfo = $reportMonthObj->getInfo($tmpInfo['proxy_id'], $nowMonth);
            if (!$tmpRsInfo) {
                $insertData = array();
                $insertData['proxy_id'] = $tmpInfo['proxy_id'];
                $insertData['proxy_name'] = $tmpInfo['proxy_name'];
                $insertData['order_number'] = $orderNumber;
                $insertData['real_amount'] = $paySumInfo['sumRealAmount'];
                $insertData['fee'] = $paySumInfo['sumFee'];
                $insertData['report_month'] = $nowMonth;
                $insertData['create_time'] = $nowDateTime;
                $reportMonthObj->insert($insertData);
            } else {
                $updateData = array();
                $updateData['order_number'] = $orderNumber;
                $updateData['real_amount'] = $paySumInfo['sumRealAmount'];
                $updateData['fee'] = $paySumInfo['sumFee'];
                $reportMonthObj->update($tmpRsInfo['id'], $updateData);
            }

        }

        // 修正隔几天补单的数据
        $nowMonthReportstartTime = strtotime($nowMonth . "-02 00:00:00");
        if ($nowTime > $nowMonthReportstartTime) {

            for ($i = strtotime($nowMonthStartTime); $i < strtotime($endTime); $i += 86400) {

                foreach ($proxyList as $tmpInfo) {

                    if (!$tmpInfo['proxy_id']) {
                        continue;
                    }

                    // 日报表
                    $paySumInfo = $payObj->getSum($tmpInfo['proxy_id'], 0, 0, 0, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));
                    $orderNumber = $payObj->getCount($tmpInfo['proxy_id'], 0, 0, 0, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));
                    $withdrawalSumInfo = $withdrawalObj->getSum($tmpInfo['proxy_id'], 2, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));

                    // 管理员添加资金
                    $orderSumInfo4 = $orderObj->getSum($tmpInfo['proxy_id'], 4, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));
                    // 管理员扣减资金
                    $orderSumInfo5 = $orderObj->getSum($tmpInfo['proxy_id'], 5, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));
                    // 管理员解冻资金
                    $orderSumInfo7 = $orderObj->getSum($tmpInfo['proxy_id'], 7, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));

                    $delBalance = $orderSumInfo4['sumAmount'] + abs($orderSumInfo7['sumAmount']) + $orderSumInfo5['sumAmount'];

                    $tmpYestodayDate = date('Y-m-d', $i - 86400);
                    $yestodayRsInfo = $reportObj->getInfo($tmpInfo['proxy_id'], $tmpYestodayDate);
                    $balance = $yestodayRsInfo['balance'] + $paySumInfo['sumFee'] - $withdrawalSumInfo['sumAmount'] - $withdrawalSumInfo['sumFee'] + $delBalance;

                    $tmpReportDate = date('Y-m-d', $i);
                    $tmpRsInfo = $reportObj->getInfo($tmpInfo['proxy_id'], $tmpReportDate);
                    if ($tmpRsInfo && (
                            $tmpRsInfo['order_number'] != $orderNumber ||
                            $tmpRsInfo['real_amount'] != $paySumInfo['sumRealAmount'] ||
                            $tmpRsInfo['fee'] != $paySumInfo['sumFee'] ||
                            $tmpRsInfo['balance'] != $balance
                        )) {
                        $updateData = array();
                        $updateData['order_number'] = $orderNumber;
                        $updateData['real_amount'] = $paySumInfo['sumRealAmount'];
                        $updateData['fee'] = $paySumInfo['sumFee'];
                        $updateData['balance'] = $balance;
                        $reportObj->update($tmpRsInfo['id'], $updateData);
                    }
                }
            }
        }

        return true;
    }

    private function appReport($reportDate) {

        $startTime = $reportDate . " 00:00:00";
        $endTime = date('Y-m-d 00:00:00', strtotime($startTime) + 86400);
        $intNowDate = date('Ymd', strtotime($startTime));

        $appObj = BooController::get('Obj_App_Info');
        $appList = $appObj->getList();

        $yestodayDate = date('Y-m-d', strtotime($startTime) - 86400);
        $nowDateTime = date('Y-m-d H:i:s');
        $payObj = BooController::get('Obj_App_Pay');
        $reportObj = BooController::get('Obj_App_Report');
        $withdrawalObj = BooController::get('Obj_App_Withdrawal');
        $orderObj = BooController::get('Obj_App_Orders');

        $nowTime = time();
        $nowMonth = date('Y-m', strtotime($startTime));
        $nowMonthStartTime = $nowMonth . "-01 00:00:00";
        $nowMonthEndTime = date('Y-m-d H:i:s', strtotime("$nowMonthStartTime +1 month")-1);
        if(strtotime($nowMonthEndTime)> strtotime($nowDateTime)){
            $nowMonthEndTime = $nowDateTime;
        }
        $nowMonthReportLastMonthTime = strtotime($nowMonth . "-04 00:00:00");
        $lastMonth = date('Y-m', strtotime($nowMonth . "-01 00:00:00") - 86400);
        $lastMonthStartTime = $lastMonth . "-01 00:00:00";
        $lastMonthEndTime = date('Y-m-d H:i:s', strtotime($nowMonth . "-01 00:00:00") - 1);
        $reportMonthObj = BooController::get('Obj_App_ReportMonth');
        foreach ($appList as $tmpInfo) {

            if (!$tmpInfo['app_id']) {
                continue;
            }
            // 日报表
            $paySumInfo = $payObj->getSum($tmpInfo['app_id'], 0, 0, 0, $startTime, $endTime);
            $totalOrderNumber = $payObj->getCount($tmpInfo['app_id'], 0, 0, 0, $startTime, $endTime, 'all');
            $orderNumber = $payObj->getCount($tmpInfo['app_id'], 0, 0, 0, $startTime, $endTime);
            $withdrawalSumInfo = $withdrawalObj->getSum($tmpInfo['app_id'], 2, $startTime, $endTime);

            // 管理员添加资金
            $orderSumInfo4 = $orderObj->getSum($tmpInfo['app_id'], 4, $startTime, $endTime);
            // 管理员扣减资金
            $orderSumInfo5 = $orderObj->getSum($tmpInfo['app_id'], 5, $startTime, $endTime);
            // 管理员冻结资金
            //$orderSumInfo6 = $orderObj->getSum($tmpInfo['app_id'], 6, $startTime, $endTime);
            // 管理员解冻资金
            $orderSumInfo7 = $orderObj->getSum($tmpInfo['app_id'], 7, $startTime, $endTime);

            $delBalance = $orderSumInfo4['sumAmount'] + abs($orderSumInfo7['sumAmount']) + $orderSumInfo5['sumAmount'];// - $orderSumInfo6['sumAmount'];

            $yestodayRsInfo = $reportObj->getInfo($tmpInfo['app_id'], $yestodayDate);
            $balance = $yestodayRsInfo['balance'] + $paySumInfo['sumFee'] - $withdrawalSumInfo['sumAmount'] - $withdrawalSumInfo['sumFee'] + $delBalance;

            $tmpRsInfo = $reportObj->getInfo($tmpInfo['app_id'], $reportDate);
            if (!$tmpRsInfo) {
                $insertData = array();
                $insertData['app_id'] = $tmpInfo['app_id'];
                $insertData['app_name'] = $tmpInfo['name'];
                $insertData['all_order_number'] = $totalOrderNumber;
                $insertData['order_number'] = $orderNumber;
                $insertData['real_amount'] = $paySumInfo['sumRealAmount'];
                $insertData['fee'] = $paySumInfo['sumFee'];
                $insertData['withdrawal_amount'] = $withdrawalSumInfo['sumAmount'];
                $insertData['withdrawal_fee'] = $withdrawalSumInfo['sumFee'];
                $insertData['balance'] = $balance;
                $insertData['report_date'] = $reportDate;
                $insertData['create_time'] = $nowDateTime;
                $reportObj->insert($insertData);
            } else {
                $updateData = array();
                $updateData['all_order_number'] = $totalOrderNumber;
                $updateData['order_number'] = $orderNumber;
                $updateData['real_amount'] = $paySumInfo['sumRealAmount'];
                $updateData['fee'] = $paySumInfo['sumFee'];
                $updateData['withdrawal_amount'] = $withdrawalSumInfo['sumAmount'];
                $updateData['withdrawal_fee'] = $withdrawalSumInfo['sumFee'];
                $updateData['balance'] = $balance;
                $reportObj->update($tmpRsInfo['id'], $updateData);
            }

            $orderInfo = $orderObj->getInfoByDate($tmpInfo['app_id'], 1, $intNowDate);
            if (!$orderInfo) {
                // 计算记录写入帐变表
                $insertData = array();
                $insertData['app_id'] = $tmpInfo['app_id'];
                $insertData['app_name'] = $tmpInfo['name'];
                $insertData['amount'] = $paySumInfo['sumFee'];
                $insertData['type'] = 1;
                $insertData['start_time'] = $startTime;
                $insertData['end_time'] = $endTime;
                $insertData['create_time'] = $endTime;
                $insertData['create_date'] = $intNowDate;
                $insertData['current_balance'] = $balance;
                $orderObj->insert($insertData);
            } else {
                $updata = array();
                $updata['amount'] = $paySumInfo['sumFee'];
                $updata['end_time'] = $endTime;
                $updata['current_balance'] = $balance;
                $orderObj->update($orderInfo['id'], $updata);
            }

            // 上个月的报表
            if ($nowTime <= $nowMonthReportLastMonthTime) {
                $paySumInfo = $payObj->getSum($tmpInfo['app_id'], 0, 0, 0, $lastMonthStartTime, $lastMonthEndTime);
                $totalOrderNumber = $payObj->getCount($tmpInfo['app_id'], 0, 0, 0, $lastMonthStartTime, $lastMonthEndTime, 'all');
                $orderNumber = $payObj->getCount($tmpInfo['app_id'], 0, 0, 0, $lastMonthStartTime, $lastMonthEndTime);
                $withdrawalSumInfo = $withdrawalObj->getSum($tmpInfo['app_id'], 2, $lastMonthStartTime, $lastMonthEndTime);

                $tmpRsInfo = $reportMonthObj->getInfo($tmpInfo['app_id'], $lastMonth);
                if (!$tmpRsInfo) {
                    $insertData = array();
                    $insertData['app_id'] = $tmpInfo['app_id'];
                    $insertData['app_name'] = $tmpInfo['name'];
                    $insertData['all_order_number'] = $totalOrderNumber;
                    $insertData['order_number'] = $orderNumber;
                    $insertData['real_amount'] = $paySumInfo['sumRealAmount'];
                    $insertData['fee'] = $paySumInfo['sumFee'];
                    $insertData['withdrawal_amount'] = $withdrawalSumInfo['sumAmount'];
                    $insertData['withdrawal_fee'] = $withdrawalSumInfo['sumFee'];
                    $insertData['report_month'] = $lastMonth;
                    $insertData['create_time'] = $nowDateTime;
                    $reportMonthObj->insert($insertData);
                } else {
                    $updateData = array();
                    $updateData['all_order_number'] = $totalOrderNumber;
                    $updateData['order_number'] = $orderNumber;
                    $updateData['real_amount'] = $paySumInfo['sumRealAmount'];
                    $updateData['fee'] = $paySumInfo['sumFee'];
                    $updateData['withdrawal_amount'] = $withdrawalSumInfo['sumAmount'];
                    $updateData['withdrawal_fee'] = $withdrawalSumInfo['sumFee'];
                    $reportMonthObj->update($tmpRsInfo['id'], $updateData);
                }
            }

            // 本月报表
            $paySumInfo = $payObj->getSum($tmpInfo['app_id'], 0, 0, 0, $nowMonthStartTime, $nowMonthEndTime);
            $totalOrderNumber = $payObj->getCount($tmpInfo['app_id'], 0, 0, 0, $nowMonthStartTime, $nowMonthEndTime, 'all');
            $orderNumber = $payObj->getCount($tmpInfo['app_id'], 0, 0, 0, $nowMonthStartTime, $nowMonthEndTime);
            $withdrawalSumInfo = $withdrawalObj->getSum($tmpInfo['app_id'], 2, $nowMonthStartTime, $nowMonthEndTime);

            $tmpRsInfo = $reportMonthObj->getInfo($tmpInfo['app_id'], $nowMonth);
            if (!$tmpRsInfo) {
                $insertData = array();
                $insertData['app_id'] = $tmpInfo['app_id'];
                $insertData['app_name'] = $tmpInfo['name'];
                $insertData['all_order_number'] = $totalOrderNumber;
                $insertData['order_number'] = $orderNumber;
                $insertData['real_amount'] = $paySumInfo['sumRealAmount'];
                $insertData['fee'] = $paySumInfo['sumFee'];
                $insertData['withdrawal_amount'] = $withdrawalSumInfo['sumAmount'];
                $insertData['withdrawal_fee'] = $withdrawalSumInfo['sumFee'];
                $insertData['report_month'] = $nowMonth;
                $insertData['create_time'] = $nowDateTime;
                $reportMonthObj->insert($insertData);
            } else {
                $updateData = array();
                $updateData['all_order_number'] = $totalOrderNumber;
                $updateData['order_number'] = $orderNumber;
                $updateData['real_amount'] = $paySumInfo['sumRealAmount'];
                $updateData['fee'] = $paySumInfo['sumFee'];
                $updateData['withdrawal_amount'] = $withdrawalSumInfo['sumAmount'];
                $updateData['withdrawal_fee'] = $withdrawalSumInfo['sumFee'];
                $reportMonthObj->update($tmpRsInfo['id'], $updateData);
            }
        }

        // 修正隔几天补单的数据
        $nowMonthReportstartTime = strtotime($nowMonth . "-02 00:00:00");
        if ($nowTime > $nowMonthReportstartTime) {

            for ($i = strtotime($nowMonthStartTime); $i < strtotime($endTime); $i += 86400) {

                foreach ($appList as $tmpInfo) {

                    if (!$tmpInfo['app_id']) {
                        continue;
                    }

                    // 日报表
                    $paySumInfo = $payObj->getSum($tmpInfo['app_id'], 0, 0, 0, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));
                    $totalOrderNumber = $payObj->getCount($tmpInfo['app_id'], 0, 0, 0, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400), 'all');
                    $orderNumber = $payObj->getCount($tmpInfo['app_id'], 0, 0, 0, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));
                    $withdrawalSumInfo = $withdrawalObj->getSum($tmpInfo['app_id'], 2, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));

                    // 管理员添加资金
                    $orderSumInfo4 = $orderObj->getSum($tmpInfo['app_id'], 4, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));
                    // 管理员扣减资金
                    $orderSumInfo5 = $orderObj->getSum($tmpInfo['app_id'], 5, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));
                    // 管理员解冻资金
                    $orderSumInfo7 = $orderObj->getSum($tmpInfo['app_id'], 7, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));

                    $delBalance = $orderSumInfo4['sumAmount'] + abs($orderSumInfo7['sumAmount']) + $orderSumInfo5['sumAmount'];

                    $tmpYestodayDate = date('Y-m-d', $i - 86400);
                    $yestodayRsInfo = $reportObj->getInfo($tmpInfo['app_id'], $tmpYestodayDate);

                    $balance = $yestodayRsInfo['balance'] + $paySumInfo['sumFee'] - $withdrawalSumInfo['sumAmount'] - $withdrawalSumInfo['sumFee'] + $delBalance;

                    $tmpReportDate = date('Y-m-d', $i);
                    $tmpRsInfo = $reportObj->getInfo($tmpInfo['app_id'], $tmpReportDate);
                    if ($tmpRsInfo && (
                            $tmpRsInfo['all_order_number'] != $totalOrderNumber ||
                            $tmpRsInfo['order_number'] != $orderNumber ||
                            $tmpRsInfo['real_amount'] != $paySumInfo['sumRealAmount'] ||
                            $tmpRsInfo['fee'] != $paySumInfo['sumFee'] ||
                            $tmpRsInfo['withdrawal_amount'] != $withdrawalSumInfo['sumAmount'] ||
                            $tmpRsInfo['withdrawal_fee'] != $withdrawalSumInfo['sumFee'] ||
                            $tmpRsInfo['balance'] != $balance
                        )) {
                        $updateData = array();
                        $updateData['all_order_number'] = $totalOrderNumber;
                        $updateData['order_number'] = $orderNumber;
                        $updateData['real_amount'] = $paySumInfo['sumRealAmount'];
                        $updateData['fee'] = $paySumInfo['sumFee'];
                        $updateData['withdrawal_amount'] = $withdrawalSumInfo['sumAmount'];
                        $updateData['withdrawal_fee'] = $withdrawalSumInfo['sumFee'];
                        $updateData['balance'] = $balance;
                        $reportObj->update($tmpRsInfo['id'], $updateData);
                    }

                }
            }
        }

        return true;
    }

}