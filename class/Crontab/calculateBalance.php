<?php
class Crontab_calculateBalance extends Crontab {

	protected function _do() {

        $crontabLogFileName = PATH_WEB . '/crontablog/calculateBalance.log';
        BooFile::write($crontabLogFileName, date('Y-m-d H:i:s') . "\n", 'a');

        // 检查是否已有相同CLI在运行中
        $sLocksFileName = PATH_WEB . '/run/calculateBalance.locks';
        if (BooFile::isExist($sLocksFileName)) {
            $stat = stat($sLocksFileName);
            if ((time() - $stat['mtime']) > 180) {
                echo "文件被锁超过180秒，被强制删除，文件为 calculateBalance.locks \n";
                @unlink($sLocksFileName);
            } else {
                echo "[d] [" . date('Y-m-d H:i:s') . "] CLI calculateBalance 正在运行 \n";
                exit();
            }
        }

        BooFile::write($sLocksFileName, time(), 'a'); // CLI 独占锁

        /*------------------处理商户 start----------------*/

        $orderObj = BooController::get('Obj_App_Orders');
        $obj = BooController::get('Obj_App_Info');
        $payObj = BooController::get('Obj_App_Pay');
        $withdrawalObj = BooController::get('Obj_App_Withdrawal');
        $reportObj = BooController::get('Obj_App_Report');
        $balanceObj = BooController::get('Obj_App_Balance');

        $intNowDate = date('Ymd');
        $startTime = date('Y-m-d') . " 00:00:00";
        $endTime = date('Y-m-d H:i:s');

        // 00 点暂不结算，等日汇总结算后再进行结算，否则会导致商户余额在5分钟内全部显示为0
        if (time() < (strtotime($startTime) + 300)) {
            @unlink($sLocksFileName);
            echo "统计报表完成，执行操作为 calculateBalance \n";
            exit();
        }

        $yestodayDate = date('Y-m-d', strtotime($startTime) - 86400);
        $appList = $obj->getList();
        foreach ($appList as $appInfo) {

            $appId = $appInfo['app_id'];

            if (!$appId) {
                continue;
            }

            $cacheKey = BooConfig::get('cacheKey.appWithdrawalLock') . $appId;
            if (Common_cacheLock::get($cacheKey)) {
                continue;
            }
            $rs = Common_cacheLock::set($cacheKey, 1, 3);
            if (!$rs) {
                continue;
            }

            $yestodayRsInfo = $reportObj->getInfo($appId, $yestodayDate);
            if (!$yestodayRsInfo) {
                //continue;
            }

            $paySumInfo = $payObj->getSum($appId, 0, 0, 0, $startTime, $endTime);
            $withdrawalSumInfo = $withdrawalObj->getSum($appId, 2, $startTime, $endTime);

            // 管理员添加资金
            $orderSumInfo4 = $orderObj->getSum($appId, 4, $startTime, $endTime);
            // 管理员扣减资金
            $orderSumInfo5 = $orderObj->getSum($appId, 5, $startTime, $endTime);
            // 管理员冻结资金
            //$orderSumInfo6 = $orderObj->getSum($appId, 6, $startTime, $endTime);
            // 管理员解冻资金
            $orderSumInfo7 = $orderObj->getSum($appId, 7, $startTime, $endTime);

            $delBalance = $orderSumInfo4['sumAmount'] + abs($orderSumInfo7['sumAmount']) + $orderSumInfo5['sumAmount'];// - $orderSumInfo6['sumAmount'];

            $appBalanceInfo = $balanceObj->getInfoByAppId($appId);
            $balance = $yestodayRsInfo['balance'] + $paySumInfo['sumFee'] - $withdrawalSumInfo['sumAmount'] - $withdrawalSumInfo['sumFee'] + $delBalance - $appBalanceInfo['frozen_amount'] - $appBalanceInfo['frozen_balance'];

            if ($balance != $appBalanceInfo['available_balance']) {
                $updateData = array();
                $updateData['available_balance'] = $balance;
                $updateData['cash_balance'] = $balance + $appBalanceInfo['frozen_amount'] + $appBalanceInfo['frozen_balance'];
                $updateData['last_modify_time'] = $endTime;
                $balanceObj->update($appBalanceInfo['id'], $updateData);
            }

            $orderInfo = $orderObj->getInfoByDate($appId, 1, $intNowDate);
            if (!$orderInfo) {
                // 计算记录写入帐变表
                $insertData = array();
                $insertData['app_id'] = $appId;
                $insertData['app_name'] = $appBalanceInfo['app_name'];
                $insertData['amount'] = $paySumInfo['sumFee'];
                $insertData['type'] = 1;
                $insertData['start_time'] = $startTime;
                $insertData['end_time'] = $endTime;
                $insertData['create_time'] = $endTime;
                $insertData['create_date'] = $intNowDate;
                $insertData['current_balance'] = $balance;
                $orderObj->insert($insertData);
            } else {
                $updata = array();
                $updata['amount'] = $paySumInfo['sumFee'];
                $updata['end_time'] = $endTime;
                $updata['create_time'] = $endTime;
                $updata['current_balance'] = $balance;
                $orderObj->update($orderInfo['id'], $updata);
            }

            // 删除锁
            Common_cacheLock::del($cacheKey);
        }

        /*------------------处理商户 end----------------*/

        /*------------------处理系统 start----------------*/
        
        /**
         * @var $balanceObj Obj_Admin_Info
         */
        $balanceObj = BooController::get('Obj_Admin_Info');
        $payObj = BooController::get('Obj_Admin_Pay');
        $orderObj = BooController::get('Obj_Admin_Orders');
        $reportObj = BooController::get('Obj_Admin_Report');
        $withdrawalObj = BooController::get('Obj_Admin_Withdrawal');
        $paySumInfo = $payObj->getSum(0, 0, 0, 0, $startTime, $endTime);
        $withdrawalSumInfo = $withdrawalObj->getSum(2, $startTime, $endTime);

        $yestodayRsInfo = $reportObj->getInfo($yestodayDate);
        //if ($yestodayRsInfo) {
            // 管理员添加资金
            $orderSumInfo4 = $orderObj->getSum(4, $startTime, $endTime);
            // 管理员扣减资金
            $orderSumInfo5 = $orderObj->getSum(5, $startTime, $endTime);
            // 管理员冻结资金
            //$orderSumInfo6 = $orderObj->getSum(6, $startTime, $endTime);
            // 管理员解冻资金
            $orderSumInfo7 = $orderObj->getSum(7, $startTime, $endTime);

            $delBalance = $orderSumInfo4['sumAmount'] + abs($orderSumInfo7['sumAmount']) + $orderSumInfo5['sumAmount'];// - $orderSumInfo6['sumAmount'];
            $balance = $yestodayRsInfo['balance'] + $paySumInfo['sumFee'] - $withdrawalSumInfo['sumAmount'] - $withdrawalSumInfo['sumFee'] + $delBalance - $info['frozen_amount'] - $info['frozen_balance'];
        
            /**
             * 更新所有管理员账号中的余额
             */
            $info = $balanceObj->getList();
            if(!empty($info)){
                foreach ($info as $key=>$admin_user) {
                    $updateData                      = array();
                    $updateData['available_balance'] = $balance;
                    $updateData['cash_balance']      = $balance + $admin_user['frozen_amount'] + $admin_user['frozen_balance'];
                    $balanceObj->update($updateData, $admin_user['admin_id']);
                }
            }

            $orderInfo = $orderObj->getInfoByDate(1, $intNowDate);
            if (!$orderInfo) {
                // 计算记录写入帐变表
                $insertData = array();
                $insertData['amount'] = $paySumInfo['sumFee'];
                $insertData['type'] = 1;
                $insertData['start_time'] = $startTime;
                $insertData['end_time'] = $endTime;
                $insertData['create_time'] = $endTime;
                $insertData['create_date'] = $intNowDate;
                $insertData['current_balance'] = $balance;
                $orderObj->insert($insertData);
            } else {
                $updata = array();
                $updata['amount'] = $paySumInfo['sumFee'];
                $updata['end_time'] = $endTime;
                $updata['create_time'] = $endTime;
                $updata['current_balance'] = $balance;
                $orderObj->update($orderInfo['id'], $updata);
            }
        //}
        // */

        /*------------------处理系统 end----------------*/

        /*------------------处理代理 start----------------*/

        //*
        $orderObj = BooController::get('Obj_Proxy_Orders');
        $obj = BooController::get('Obj_Proxy_Info');
        $payObj = BooController::get('Obj_Proxy_Pay');
        $withdrawalObj = BooController::get('Obj_Proxy_Withdrawal');
        $reportObj = BooController::get('Obj_Proxy_Report');
        /**
         * @var $balanceObj Obj_Proxy_Balance
         */
        $balanceObj = BooController::get('Obj_Proxy_Balance');

        $proxyList = $obj->getAllList();
        foreach ($proxyList as $proxyInfo) {

            $proxyId = $proxyInfo['proxy_id'];

            $cacheKey = BooConfig::get('cacheKey.proxyWithdrawalLock') . $proxyId;
            if (Common_cacheLock::get($cacheKey)) {
                continue;
            }
            $rs = Common_cacheLock::set($cacheKey, 1, 3);
            if (!$rs) {
                continue;
            }

            $yestodayRsInfo = $reportObj->getInfo($proxyId, $yestodayDate);
            if (!$yestodayRsInfo) {
                //continue;
            }

            $paySumInfo = $payObj->getSum($proxyId, 0, 0, 0, $startTime, $endTime);
            $withdrawalSumInfo = $withdrawalObj->getSum($proxyId, 2, $startTime, $endTime);

            // 管理员添加资金
            $orderSumInfo4 = $orderObj->getSum($proxyId, 4, $startTime, $endTime);
            // 管理员扣减资金
            $orderSumInfo5 = $orderObj->getSum($proxyId, 5, $startTime, $endTime);
            // 管理员冻结资金
            //$orderSumInfo6 = $orderObj->getSum($proxyId, 6, $startTime, $endTime);
            // 管理员解冻资金
            $orderSumInfo7 = $orderObj->getSum($proxyId, 7, $startTime, $endTime);

            $delBalance = $orderSumInfo4['sumAmount'] + abs($orderSumInfo7['sumAmount']) + $orderSumInfo5['sumAmount'];// - $orderSumInfo6['sumAmount'];

            $proxyBalanceInfo = $balanceObj->getInfoByProxyId($proxyId);
            $balance = $yestodayRsInfo['balance'] + $paySumInfo['sumFee'] - $withdrawalSumInfo['sumAmount'] - $withdrawalSumInfo['sumFee'] + $delBalance - $proxyBalanceInfo['frozen_amount'] - $proxyBalanceInfo['frozen_balance'];

            if ($balance != $proxyBalanceInfo['available_balance']) {
                $updateData = array();
                $updateData['available_balance'] = $balance;
                $updateData['cash_balance'] = $balance + $proxyBalanceInfo['frozen_amount'] + $proxyBalanceInfo['frozen_balance'];
                $updateData['last_modify_time'] = $endTime;
                $balanceObj->update($proxyBalanceInfo['id'], $updateData);
            }

            $orderInfo = $orderObj->getInfoByDate($proxyId, 1, $intNowDate);
            if (!$orderInfo) {
                // 计算记录写入帐变表
                $insertData = array();
                $insertData['proxy_id'] = $proxyId;
                $insertData['proxy_name'] = $proxyBalanceInfo['proxy_name'];
                $insertData['amount'] = $paySumInfo['sumFee'];
                $insertData['type'] = 1;
                $insertData['start_time'] = $startTime;
                $insertData['end_time'] = $endTime;
                $insertData['create_time'] = $endTime;
                $insertData['create_date'] = $intNowDate;
                $insertData['current_balance'] = $balance;
                $orderObj->insert($insertData);
            } else {
                $updata = array();
                $updata['amount'] = $paySumInfo['sumFee'];
                $updata['end_time'] = $endTime;
                $updata['create_time'] = $endTime;
                $updata['current_balance'] = $balance;
                $orderObj->update($orderInfo['id'], $updata);
            }

            // 删除锁
            Common_cacheLock::del($cacheKey);
        }
        // */

        /*------------------处理代理 end----------------*/


        @unlink($sLocksFileName);
        echo "统计报表完成，执行操作为 calculateBalance \n";
        exit();
	}

}
