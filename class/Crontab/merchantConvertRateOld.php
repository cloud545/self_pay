<?php

/**
 * 统计商户转化率
 *
 * Class Crontab_merchantConvertRateOld
 */
class Crontab_merchantConvertRateOld extends Crontab
{
    
    protected function _do()
    {
        
        date_default_timezone_set('PRC');
        
        $start_time = date('Y-m-d 00:00:00', strtotime('-3 day'));
        $end_time   = date('Y-m-d 00:00:00', time());
        echo "开始时间:" . $start_time . "\n";
        echo "结束时间:" . $end_time . "\n";
        
        try {
            
            $start = strtotime($start_time);
            $end   = strtotime($end_time);
            
            /**
             * 获取所有用户
             *
             * @var $appObj Obj_App_Info
             */
            $appObj     = BooController::get('Obj_App_Info');
            $tmpAppList = $appObj->getList();
            foreach ($tmpAppList as $info) {
                for ($i = $start; $i < $end; $i += 3600) {
                    $this->exec($info['app_id'], $i, $i + 3600);
                    echo "完成商户编号" . $info['app_id'] . "\n";
                }
            }
            
        } catch (\Exception $exception) {
            echo $exception->getMessage() . "\n";
        }
    }
    
    public function exec($merchant_id, $start_time, $end_time)
    {
        try {
            /**
             * @var $apps Obj_App_Pay
             */
            $start = date('Y-m-d H:00:00', $start_time);
            $end   = date('Y-m-d H:00:00', $end_time);
            $apps  = BooController::get('Obj_App_Pay');
            
            /**
             * @var $obj Obj_App_ConvertRate
             */
            $obj = BooController::get('Obj_App_ConvertRate');
            
            /**
             * 统计成单
             */
            $settled_data = $apps->getConvertRateCrontab(
                $merchant_id,
                $start,
                $end,
                true
            );
            echo '开始日期' . $start . "\n";
            if (!empty($settled_data)) {
                foreach ($settled_data as $key => $item) {
                    try {
                        $info = $obj->getInfo($item['create_hour'], $item['app_id'], $item['pt_id'], $item['p_id']);
                        if (!empty($info)) {
                            $obj->update(
                                $info['id'],
                                [
                                    'amount'       => $item['total_amount'],
                                    'num'          => $item['total_num'],
                                    'updated_time' => time(),
                                ]
                            );
                        }
                        else {
                            $obj->insert(
                                [
                                    'create_date'  => substr($item['create_hour'], 0, 8),
                                    'create_hour'  => $item['create_hour'],
                                    'app_id'       => $item['app_id'],
                                    'pt_id'        => $item['pt_id'],
                                    'p_id'         => $item['p_id'],
                                    'total_amount' => 0,
                                    'total_num'    => 0,
                                    'amount'       => $item['total_amount'],
                                    'num'          => $item['total_num'],
                                    'created_time' => time(),
                                ]
                            );
                        }
                    } catch (\Exception $exception) {
                        echo $exception->getMessage() . "\n";
                    }
                }
            }
            echo '结束日期:' . $end . "\n";
            
        } catch (\Exception $exception) {
            echo $exception->getMessage() . "\n";
        }
    }
    
}
