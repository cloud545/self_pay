<?php

/**
 * 检查订单
 *
 * Class Crontab_checkBill
 */
class Crontab_checkBill extends Crontab
{
    protected $_dbDao;
    
    protected function _do()
    {
        ini_set("display_errors", "On");
        //error_reporting(E_ALL);
        
        header("content-type:text/html;charset=utf-8");
        $this->_dbDao = Dao_Mysql_Default::getInstance();
        
        
        /******补数据时启用******/
        $this->writeDropData();die;
        
        
        $time       = time();
        $start_time = "2020-01-04 00:00:00";
        $end_time   = '2020-01-06 00:00:00';
        
        $this->log("开始执行……");
        
        $re = $this->createTmpTable();
        if(!$re){
            $this->log("创建临时表失败");
            exit();
        }
        $this->log("创建临时表 tmp_app、tmp_bill 完成");
        
        $reCnt = $this->_dbDao->get("select count(*) cnt from tmp_app");
        if($reCnt['cnt']==0){
            $this->log("检查参与的商户……");
            $list = $this->_dbDao->mget("select distinct app_id from self_pay_zf2860.system_pay a3 where a3.create_time>='{$start_time}' and a3.create_time<'{$end_time}'");
            foreach ($list as $row) {
                $insertData = array('app_id'=>$row['app_id'],'create_time'=>date('Y-m-d H:i:s'));
                $this->_dbDao->set('`tmp_app`', $insertData);
            }
            $appcnt = count($list);
            $this->log("检查到{$appcnt}个参与商户");
        }
        
        $reCnt = $this->_dbDao->get("select count(*) cnt from tmp_pay01");
        if(!$reCnt||$reCnt['cnt']==0){
            
            $this->_dbDao->query("
            create table tmp_pay01 as select distinct order_id,app_id from self_pay_zf2860.system_pay a3 where a3.create_time>='{$start_time}' and a3.create_time<'{$end_time}'
		");
            $this->log("01月份临时数据表tmp_pay01创建完成");
            $this->_dbDao->query("
            ALTER TABLE `tmp_pay01`
ADD PRIMARY KEY (`order_id`),
ADD INDEX `idx_order_id` (`order_id`) USING BTREE
		");
            $this->log("01月份临时数据表tmp_pay01创建索引");
        }
        
        $this->log("-----------------------------begin-----------------------------");
        
        $list = $this->_dbDao->mget("select * from tmp_app where checkstatus=0");
        foreach ($list as $row) {
            $this->log("检测商户ID={$row['app_id']} 漏掉的记录……");
            $sql = "select * from self_app_pay_zf2860.pay_{$row['app_id']} a1 where a1.create_time>='{$start_time}' and a1.create_time<'{$end_time}' and a1.result=1 and
not EXISTS(select 1 from self_pay_zf2860.tmp_pay01 a3 where a1.order_id=a3.order_id and a3.app_id={$row['app_id']} )";
            $list2 = $this->_dbDao->mget($sql);
            
            if(!$list2){
                $updateData = array('cnt'=>0,'checkstatus'=>1);
                $this->_dbDao->set('`tmp_app`', $updateData, '`app_id` = :id', array(':id' => $row['app_id']));
                $cnt = 0;
            }else{
                $cnt = count($list2);
                $updateData = array('cnt'=>$cnt,'checkstatus'=>1);
                $this->_dbDao->set('`tmp_app`', $updateData, '`app_id` = :id', array(':id' => $row['app_id']));
                if(!empty($list2)){
                    foreach ($list2 as $row2) {
                        $insertData = array('app_id'=>$row2['app_id'],'pay_id'=>$row2['id'],'order_id'=>$row2['order_id'], 'create_time'=>date('Y-m-d H:i:s'));
                        $this->_dbDao->set('`tmp_bill`', $insertData);
                    }
                }
            }
            $this->log("商户ID={$row['app_id']} 漏掉{$cnt}条记录");
        }
        
        
        $this->log("----------------------------- end -----------------------------");
        die;
    }

    private function log($msg){
        echo date('Y-m-d H:i:s')."\t".$msg."\n";
    }

    
    
    
    private function writeDropData(){
        $this->log("开始补数据……");
        
        $list = $this->_dbDao->mget("select * from tmp_bill where writestatus=0");
        foreach ($list as $row) {
            $appId = $row['app_id'];
            $payId = $row['pay_id'];
            $appPayObj = BooController::get('Obj_App_Pay');
            $payInfo = $appPayObj->getInfoById($appId, $payId);
            if ($payInfo) {
                 $this->sendMoney($payInfo,$payInfo['real_amount']);
                 $updateData = array('writestatus'=>1);
                $this->_dbDao->set('`tmp_bill`', $updateData, '`id` = :id', array(':id' => $row['id']));
                $this->log("订单号={$row['order_id']} 补数据完成");
            }
            else{
                $this->log("订单号={$row['order_id']} 商户数据不存在");
            }
            
        }
        $this->log("-----------------------------end-----------------------------");
        die;
    }

    private function createTmpTable(){
        
        $this->_dbDao->query("
            CREATE TABLE `tmp_app` (
                `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
                `app_id` int(6) unsigned NOT NULL COMMENT '应用id',
                `cnt` int(6) unsigned NOT NULL DEFAULT '0' COMMENT '漏掉记录数',
                `checkstatus` int(4) unsigned NOT NULL DEFAULT '0' COMMENT '检测状态',
                `writestatus` int(4) unsigned NOT NULL DEFAULT '0' COMMENT '补数据状态',
                `create_time` datetime NOT NULL COMMENT '创建时间',
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='临时补漏商户表';
		");
        
        $this->_dbDao->query("
            CREATE TABLE `tmp_bill` (
              `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增id',
              `app_id` int(6) UNSIGNED NOT NULL COMMENT '应用id',
              `pay_id` int(10) UNSIGNED NOT NULL COMMENT '充值id',
              `order_id` varchar(60) NOT NULL COMMENT '订单编号',
              `writestatus` int(4) UNSIGNED NOT NULL DEFAULT '0' COMMENT '补数据状态',
              `create_time` datetime NOT NULL COMMENT '创建时间',
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='临时补漏订单表';
		");
        return true;
    }
    
    
    private function sendMoney($payInfo, $realMoney) {

        $start_time = "2020-01-04 00:00:00";
        $end_time   = '2020-01-06 00:00:00';
        $lastLevelPoint = $payInfo['current_point'];
	if (!$payInfo['proxy_id']) {

            
            $point = 0;
            $obj = BooController::get('Obj_Plat_Info');
            $list = $obj->getList();
            foreach ($list as $key => $tmpInfo) {
                if ($tmpInfo['p_id'] == $payInfo['p_id']) {
                    $point = $tmpInfo['p_point'];
                    break;
                }
            }

            if (!$point) {
                $this->log("\t\t上游通道ID={$payInfo['p_id']} 找不到");
            }
            
            $dxpoint = $lastLevelPoint - $point;

            if ($dxpoint < 0) {
                $dxpoint = 0;
                $this->log("\t\t充值费率={$lastLevelPoint}<上游费率={$point}");
            }
            $reCnt = $this->_dbDao->get("select count(*) cnt from system_pay where order_id='{$payInfo['order_id']}' and create_time>='{$start_time}' and create_time<'{$end_time}'");
            if($reCnt['cnt']>0){
                return true;
            }
            $systemMoney = $realMoney * $dxpoint;
            $insertData = array();
            $insertData['app_id'] = $payInfo['app_id'];
            $insertData['app_name'] = $payInfo['app_name'];
            $insertData['p_id'] = $payInfo['p_id'];
            $insertData['order_id'] = $payInfo['order_id'];
            $insertData['pay_id'] = $payInfo['id'];
            $insertData['amount'] = $payInfo['amount'];
            $insertData['real_amount'] = $realMoney;
            $insertData['pt_id'] = $payInfo['pt_id'];
            $insertData['create_time'] = $payInfo['create_time'];
            $insertData['end_time'] = date('Y-m-d H:i:s');
            $insertData['create_date'] = $payInfo['create_date'];
            $insertData['current_point'] = $point;
            $insertData['current_fee_rate'] = $dxpoint;
            $insertData['current_fee'] = $systemMoney;

            $systemPayObj = BooController::get('Obj_Admin_Pay');
            $systemPayObj->insert($insertData);

            return true;
        }

        $obj = BooController::get('Obj_Proxy_Info');
        $info = $obj->getInfoById($payInfo['proxy_id']);
        $platAppInfo = json_decode($info['plat_app_info'], true);
        $pointInfo = $platAppInfo[$payInfo['p_id']];
        foreach ($platAppInfo as $tmpInfo) {
            if ($tmpInfo['payType'] == $payInfo['pt_id']) {
                $pointInfo = $tmpInfo;
                break;
            }
        }
        
        //代理通道查找不到，这里当作不返点处理
        if($pointInfo['point']>0){
            

            $dxpoint = $payInfo['current_point'] - $pointInfo['point'];
            if ($dxpoint < 0) {
                $dxpoint = 0;
                $this->log("\t\t充值费率={$lastLevelPoint}<上游费率={$point}");
            }
            $proxyMoney = $realMoney * $dxpoint;

            $nowDateTime = date('Y-m-d H:i:s');
            $insertData = array();
            $insertData['app_id'] = $payInfo['app_id'];
            $insertData['app_name'] = $payInfo['app_name'];
            $insertData['proxy_id'] = $info['proxy_id'];
            $insertData['proxy_name'] = $info['proxy_name'];
            $insertData['p_id'] = $payInfo['p_id'];
            $insertData['order_id'] = $payInfo['order_id'];
            $insertData['amount'] = $payInfo['amount'];
            $insertData['real_amount'] = $realMoney;
            $insertData['pt_id'] = $payInfo['pt_id'];
            $insertData['pay_id'] = $payInfo['id'];
            $insertData['create_time'] = $payInfo['create_time'];
            $insertData['end_time'] = $nowDateTime;
            $insertData['create_date'] = $payInfo['create_date'];
            $insertData['current_point'] = $pointInfo['point'];
            $insertData['current_fee_rate'] = $dxpoint;
            $insertData['current_fee'] = $proxyMoney;

            $proxyPayObj = BooController::get('Obj_Proxy_Pay');
            
            $dbDao = Dao_Mysql_AppPay::getInstance();
            $reCnt = $dbDao->get("select count(*) cnt from proxy_pay_{$info['proxy_id']} where order_id='{$payInfo['order_id']}' and proxy_id='{$info['proxy_id']}' and create_time>='{$start_time}' and create_time<'{$end_time}'");
            if($reCnt['cnt']==0){
                $proxyPayObj->insert($payInfo['proxy_id'], $insertData);
            }
            

            $lastLevelPoint = $pointInfo['point'];
        
            if ($info['user_tree']) {
                $treeInfo = explode(',', $info['user_tree']);
                krsort($treeInfo);

                foreach ($treeInfo as $tmpProxyId) {

                    if ($lastLevelPoint - $pointInfo['point'] < 0) {
                        continue;
                    }

                    $info = $obj->getInfoById($tmpProxyId);
                    $platAppInfo = json_decode($info['plat_app_info'], true);
                    $pointInfo = $platAppInfo[$payInfo['p_id']];
                    foreach ($platAppInfo as $tmpInfo) {
                        if ($tmpInfo['payType'] == $payInfo['pt_id']) {
                            $pointInfo = $tmpInfo;
                            break;
                        }
                    }
                    $proxyMoney = $realMoney * ($lastLevelPoint - $pointInfo['point']);
                    $lastLevelPoint = $pointInfo['point'];

                    $insertData = array();
                    $insertData['app_id'] = $payInfo['app_id'];
                    $insertData['app_name'] = $payInfo['app_name'];
                    $insertData['proxy_id'] = $info['proxy_id'];
                    $insertData['proxy_name'] = $info['proxy_name'];
                    $insertData['p_id'] = $payInfo['p_id'];
                    $insertData['order_id'] = $payInfo['order_id'];
                    $insertData['amount'] = $payInfo['amount'];
                    $insertData['real_amount'] = $realMoney;
                    $insertData['pt_id'] = $payInfo['pt_id'];
                    $insertData['pay_id'] = $payInfo['id'];
                    $insertData['create_time'] = $payInfo['create_time'];
                    $insertData['end_time'] = $nowDateTime;
                    $insertData['create_date'] = $payInfo['create_date'];
                    $insertData['current_point'] = $pointInfo['point'];
                    $insertData['current_fee_rate'] = $lastLevelPoint - $pointInfo['point'];
                    $insertData['current_fee'] = $proxyMoney;

                    $reCnt = $dbDao->get("select count(*) cnt from proxy_pay_{$tmpProxyId} where order_id='{$payInfo['order_id']}' and proxy_id='{$info['proxy_id']}' and create_time>='{$start_time}' and create_time<'{$end_time}'");
                    if($reCnt['cnt']==0){
                        $proxyPayObj->insert($tmpProxyId, $insertData);
                    }
                    
                }

            }
        }

        $point = 0;
        $obj = BooController::get('Obj_Plat_Info');
        $list = $obj->getList();
        foreach ($list as $key => $tmpInfo) {
            if ($tmpInfo['p_id'] == $payInfo['p_id']) {
                $point = $tmpInfo['p_point'];
                break;
            }
        }

        if (!$point) {
            $this->log("\t\t上游通道ID={$payInfo['p_id']} 找不到");
        }

        $dxpoint = $lastLevelPoint - $point;

        if ($dxpoint < 0) {
            $dxpoint = 0;
            $this->log("\t\t充值费率={$lastLevelPoint}<上游费率={$point}");
        }
        
        $reCnt = $this->_dbDao->get("select count(*) cnt from system_pay where order_id='{$payInfo['order_id']}' AND app_name='{$payInfo['app_name']}'");
        if($reCnt['cnt']>0){
            return true;
        }
        $systemMoney = $realMoney * $dxpoint;
        $insertData = array();
        $insertData['app_id'] = $payInfo['app_id'];
        $insertData['app_name'] = $payInfo['app_name'];
        $insertData['proxy_id'] = $payInfo['proxy_id'];
        $insertData['proxy_name'] = $payInfo['proxy_name'];
        $insertData['p_id'] = $payInfo['p_id'];
        $insertData['order_id'] = $payInfo['order_id'];
        $insertData['pay_id'] = $payInfo['id'];
        $insertData['amount'] = $payInfo['amount'];
        $insertData['real_amount'] = $realMoney;
        $insertData['pt_id'] = $payInfo['pt_id'];
        $insertData['create_time'] = $payInfo['create_time'];
        $insertData['end_time'] = $nowDateTime;
        $insertData['create_date'] = $payInfo['create_date'];
        $insertData['current_point'] = $point;
        $insertData['current_fee_rate'] = $dxpoint;
        $insertData['current_fee'] = $systemMoney;

        $systemPayObj = BooController::get('Obj_Admin_Pay');
        $systemPayObj->insert($insertData);

        return true;
    }
    
}
