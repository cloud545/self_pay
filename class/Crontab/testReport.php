<?php
class Crontab_testReport extends Crontab {

	protected function _do() {


        $argv = BooVar::server("argv");
        $reportDate = $argv[2] ? $argv[2] : date('Y-m-d', strtotime('-1 day'));

        //$this->systemReport($reportDate);
        //$this->proxyReport($reportDate);
        //$this->appReport($reportDate);

        echo "统计报表完成，执行操作为 report \n";
        exit();
	}

	private function systemReport($reportDate) {

        $startTime = $reportDate . " 00:00:00";
        $endTime = date('Y-m-d 00:00:00', strtotime($startTime) + 86400);
        $intNowDate = date('Ymd', strtotime($startTime));

        $yestodayDate = date('Y-m-d', strtotime($startTime) - 86400);
        $nowDateTime = date('Y-m-d H:i:s');
        $payObj = BooController::get('Obj_Admin_Pay');
        $reportObj = BooController::get('Obj_Admin_Report');
        $withdrawalObj = BooController::get('Obj_Admin_Withdrawal');
        $orderObj = BooController::get('Obj_Admin_Orders');


        $nowTime = time();
        $nowMonth = date('Y-m', strtotime($startTime));
        $nowMonthStartTime = $nowMonth . "-01 00:00:00";
        $nowMonthReportstartTime = strtotime($nowMonth . "-02 00:00:00");

        if ($nowTime > $nowMonthReportstartTime) {

            for ($i = strtotime($nowMonthStartTime); $i < strtotime($endTime); $i += 86400) {

                // 日报表
                $paySumInfo = $payObj->getSum(0, 0, 0, 0, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));
                $orderNumber = $payObj->getCount(0, 0, 0, 0, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));
                $withdrawalSumInfo = $withdrawalObj->getSum(2, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));

                // 管理员添加资金
                $orderSumInfo4 = $orderObj->getSum(4, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));
                // 管理员扣减资金
                $orderSumInfo5 = $orderObj->getSum(5, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));
                // 管理员解冻资金
                $orderSumInfo7 = $orderObj->getSum(7, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));

                $delBalance = $orderSumInfo4['sumAmount'] + abs($orderSumInfo7['sumAmount']) + $orderSumInfo5['sumAmount'];

                $tmpYestodayDate = date('Y-m-d', $i - 86400);
                $yestodayRsInfo = $reportObj->getInfo($tmpYestodayDate);

                $balance = $yestodayRsInfo['balance'] + $paySumInfo['sumFee'] - $withdrawalSumInfo['sumAmount'] - $withdrawalSumInfo['sumFee'] + $delBalance;

                $tmpReportDate = date('Y-m-d', $i);
                $tmpRsInfo = $reportObj->getInfo($tmpReportDate);
                if ($tmpRsInfo && (
                    $tmpRsInfo['order_number'] != $orderNumber ||
                    $tmpRsInfo['real_amount'] != $paySumInfo['sumRealAmount'] ||
                    $tmpRsInfo['fee'] != $paySumInfo['sumFee'] ||
                    $tmpRsInfo['balance'] != $balance
                )) {
                    $updateData = array();
                    $updateData['order_number'] = $orderNumber;
                    $updateData['real_amount'] = $paySumInfo['sumRealAmount'];
                    $updateData['fee'] = $paySumInfo['sumFee'];
                    $updateData['balance'] = $balance;
                    //$reportObj->update($tmpRsInfo['id'], $updateData);
                }
            }
        }

        return true;
    }

    private function proxyReport($reportDate) {

        $startTime = $reportDate . " 00:00:00";
        $endTime = date('Y-m-d 00:00:00', strtotime($startTime) + 86400);
        $intNowDate = date('Ymd', strtotime($startTime));

        $proxyObj = BooController::get('Obj_Proxy_Info');
        $proxyList = $proxyObj->getAllList();

        $yestodayDate = date('Y-m-d', strtotime($startTime) - 86400);
        $nowDateTime = date('Y-m-d H:i:s');
        $payObj = BooController::get('Obj_Proxy_Pay');
        $reportObj = BooController::get('Obj_Proxy_Report');
        $withdrawalObj = BooController::get('Obj_Proxy_Withdrawal');
        $orderObj = BooController::get('Obj_Proxy_Orders');

        $nowTime = time();
        $nowMonth = date('Y-m', strtotime($startTime));
        $nowMonthStartTime = $nowMonth . "-01 00:00:00";
        $nowMonthReportstartTime = strtotime($nowMonth . "-02 00:00:00");

        if ($nowTime > $nowMonthReportstartTime) {

            for ($i = strtotime($nowMonthStartTime); $i < strtotime($endTime); $i += 86400) {

                foreach ($proxyList as $tmpInfo) {
                    // 日报表
                    $paySumInfo = $payObj->getSum($tmpInfo['proxy_id'], 0, 0, 0, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));
                    $orderNumber = $payObj->getCount($tmpInfo['proxy_id'], 0, 0, 0, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));
                    $withdrawalSumInfo = $withdrawalObj->getSum($tmpInfo['app_id'], 2, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));

                    // 管理员添加资金
                    $orderSumInfo4 = $orderObj->getSum($tmpInfo['app_id'], 4, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));
                    // 管理员扣减资金
                    $orderSumInfo5 = $orderObj->getSum($tmpInfo['app_id'], 5, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));
                    // 管理员解冻资金
                    $orderSumInfo7 = $orderObj->getSum($tmpInfo['app_id'], 7, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));

                    $delBalance = $orderSumInfo4['sumAmount'] + abs($orderSumInfo7['sumAmount']) + $orderSumInfo5['sumAmount'];

                    $tmpYestodayDate = date('Y-m-d', $i - 86400);
                    $yestodayRsInfo = $reportObj->getInfo($tmpInfo['app_id'], $tmpYestodayDate);
                    $balance = $yestodayRsInfo['balance'] + $paySumInfo['sumFee'] - $withdrawalSumInfo['sumAmount'] - $withdrawalSumInfo['sumFee'] + $delBalance;

                    $tmpReportDate = date('Y-m-d', $i);
                    $tmpRsInfo = $reportObj->getInfo($tmpInfo['proxy_id'], $tmpReportDate);
                    if ($tmpRsInfo && (
                        $tmpRsInfo['order_number'] != $orderNumber ||
                        $tmpRsInfo['real_amount'] != $paySumInfo['sumRealAmount'] ||
                        $tmpRsInfo['fee'] != $paySumInfo['sumFee'] ||
                        $tmpRsInfo['balance'] != $balance
                    )) {
                        $updateData = array();
                        $updateData['order_number'] = $orderNumber;
                        $updateData['real_amount'] = $paySumInfo['sumRealAmount'];
                        $updateData['fee'] = $paySumInfo['sumFee'];
                        $updateData['balance'] = $balance;
                        //$reportObj->update($tmpRsInfo['id'], $updateData);
                    }
                }
            }
        }

        return true;
    }

    private function appReport($reportDate) {

        $startTime = $reportDate . " 00:00:00";
        $endTime = date('Y-m-d 00:00:00', strtotime($startTime) + 86400);
        $intNowDate = date('Ymd', strtotime($startTime));

        $appObj = BooController::get('Obj_App_Info');
        $appList = $appObj->getList();

        $yestodayDate = date('Y-m-d', strtotime($startTime) - 86400);
        $nowDateTime = date('Y-m-d H:i:s');
        $payObj = BooController::get('Obj_App_Pay');
        $reportObj = BooController::get('Obj_App_Report');
        $withdrawalObj = BooController::get('Obj_App_Withdrawal');
        $orderObj = BooController::get('Obj_App_Orders');


        $nowTime = time();
        $nowMonth = date('Y-m', strtotime($startTime));
        $nowMonthStartTime = $nowMonth . "-01 00:00:00";
        $nowMonthReportstartTime = strtotime($nowMonth . "-02 00:00:00");

        $list = $feeList = array();
        $total = 0;
        if ($nowTime > $nowMonthReportstartTime) {

            for ($i = strtotime($nowMonthStartTime); $i < strtotime($endTime); $i += 86400) {

                foreach ($appList as $tmpInfo) {
                    // 日报表
                    $paySumInfo = $payObj->getSum($tmpInfo['app_id'], 0, 0, 0, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));
                    $totalOrderNumber = $payObj->getCount($tmpInfo['app_id'], 0, 0, 0, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400), 'all');
                    $orderNumber = $payObj->getCount($tmpInfo['app_id'], 0, 0, 0, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));
                    $withdrawalSumInfo = $withdrawalObj->getSum($tmpInfo['app_id'], 2, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));

                    // 管理员添加资金
                    $orderSumInfo4 = $orderObj->getSum($tmpInfo['app_id'], 4, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));
                    // 管理员扣减资金
                    $orderSumInfo5 = $orderObj->getSum($tmpInfo['app_id'], 5, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));
                    // 管理员解冻资金
                    $orderSumInfo7 = $orderObj->getSum($tmpInfo['app_id'], 7, date('Y-m-d H:i:s', $i), date('Y-m-d H:i:s', $i + 86400));

                    $delBalance = $orderSumInfo4['sumAmount'] + abs($orderSumInfo7['sumAmount']) + $orderSumInfo5['sumAmount'];

                    $tmpYestodayDate = date('Y-m-d', $i - 86400);
                    //$yestodayRsInfo = $reportObj->getInfo($tmpInfo['app_id'], $tmpYestodayDate);
                    if (!$list[$tmpInfo['app_id']][$tmpYestodayDate]) {
                        $yestodayRsInfo = $reportObj->getInfo($tmpInfo['app_id'], $tmpYestodayDate);
                    } else {
                        $yestodayRsInfo['balance'] = $list[$tmpInfo['app_id']][$tmpYestodayDate];
                    }

                    $balance = $yestodayRsInfo['balance'] + $paySumInfo['sumFee'] - $withdrawalSumInfo['sumAmount'] - $withdrawalSumInfo['sumFee'] + $delBalance;

                    $tmpReportDate = date('Y-m-d', $i);
                    $tmpRsInfo = $reportObj->getInfo($tmpInfo['app_id'], $tmpReportDate);
                    if ($tmpRsInfo && (
                        $tmpRsInfo['all_order_number'] != $totalOrderNumber ||
                        $tmpRsInfo['order_number'] != $orderNumber ||
                        $tmpRsInfo['real_amount'] != $paySumInfo['sumRealAmount'] ||
                        $tmpRsInfo['fee'] != $paySumInfo['sumFee'] ||
                        $tmpRsInfo['withdrawal_amount'] != $withdrawalSumInfo['sumAmount'] ||
                        $tmpRsInfo['withdrawal_fee'] != $withdrawalSumInfo['sumFee'] ||
                        $tmpRsInfo['balance'] != $balance
                    )) {
                        $updateData = array();
                        $updateData['all_order_number'] = $totalOrderNumber;
                        $updateData['order_number'] = $orderNumber;
                        $updateData['real_amount'] = $paySumInfo['sumRealAmount'];
                        $updateData['fee'] = $paySumInfo['sumFee'];
                        $updateData['withdrawal_amount'] = $withdrawalSumInfo['sumAmount'];
                        $updateData['withdrawal_fee'] = $withdrawalSumInfo['sumFee'];
                        $updateData['balance'] = $balance;
                        //$reportObj->update($tmpRsInfo['id'], $updateData);
                    }

                }
            }
        }

        return true;
    }

}