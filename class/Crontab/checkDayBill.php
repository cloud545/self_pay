<?php

/**
 * 检查订单
 *
 * Class Crontab_checkBill
 */
class Crontab_checkDayBill extends Crontab
{
    protected $_dbDao;
    private  $isWirte = false;//是否写入


    protected function _do()
    {
        //ini_set("display_errors", "On");
        //error_reporting(E_ALL);
        
        header("content-type:text/html;charset=utf-8");
        $this->_dbDao = Dao_Mysql_Default::getInstance();
        
        $argv = BooVar::server("argv");
        if(!$argv[2]){
            $this->log("请填写 检测日期");
            return;
        }
        $reportDate = strtotime($argv[2]);
        
        if($argv[3] && strtolower($argv[3])=="true"){
            $this->isWirte = true;
        }
        
        $start_time = date("Y-m-d 00:00:00",$reportDate);
        $end_time   = date("Y-m-d 00:00:00",$reportDate + 86400);
        
        $this->log("开始执行……");
        
        $tmp_table_name = "tmp_tmppay_".date("Ymd",$reportDate);
        
        $reCnt = $this->_dbDao->get("select count(*) cnt from information_schema.tables where table_name ='{$tmp_table_name}' and TABLE_SCHEMA='self_pay_zf2860';");
        if($reCnt['cnt']>0){
            $this->_dbDao->query("drop table {$tmp_table_name}");
            $this->log("检测到上次的临时表，已删除");
        }
        $reCnt = $this->_dbDao->get("select count(*) cnt from information_schema.tables where table_name ='{$tmp_table_name}' and TABLE_SCHEMA='self_pay_zf2860';");
        if(!$reCnt||$reCnt['cnt']==0){
            
            $this->_dbDao->query("
            create table {$tmp_table_name} as select order_id,app_id,pay_id,real_amount,create_time from self_pay_zf2860.system_pay a3 where a3.create_time>='{$start_time}' and a3.create_time<'{$end_time}'
		");
            $this->log("【".date("Y-m-d",$reportDate)."】临时数据表创建完成");
            $this->_dbDao->query("
            ALTER TABLE `{$tmp_table_name}`
ADD INDEX `idx_app_id` (`app_id`) USING BTREE,
ADD INDEX `idx_app_pay_id` (`pay_id`) USING BTREE
		");
            $this->log("【".date("Y-m-d",$reportDate)."】临时数据表创建索引");
        }
        
        $this->log("-----------------------------begin-----------------------------");
        
        $list = $this->_dbDao->mget("select * from app_report where report_date>='{$start_time}' and report_date<'{$end_time}'");
        if(count($list)==0){
            $this->log("商户日统计表无数据");
            $this->_dbDao->query("drop table {$tmp_table_name}");
            $this->log("删除【".date("Y-m-d",$reportDate)."】临时数据表");
            $this->log("----------------------------- end -----------------------------");
            return;
        }
        
        $num_app = 0;
        $num_pay = 0;
        foreach ($list as $row) {
            $this->log("检测商户ID={$row['app_id']} {$row['app_name']}的记录……");
            $reCnt = $this->_dbDao->get("select count(*) cnt from {$tmp_table_name} where app_id={$row['app_id']}");
            if($reCnt['cnt']!=$row['order_number']){
                $dx = $row['order_number'] - $reCnt['cnt'];
                $this->log("商户ID={$row['app_id']} {$row['app_name']}，商户表充值数：{$row['order_number']},系统表充值数：{$reCnt['cnt']},漏单数：{$dx}");
                $num_app++;
                $num_pay = $num_pay + $dx;
                if($dx>0){ //漏单明细
                    $sql = "select * from self_app_pay_zf2860.pay_{$row['app_id']} a1 where a1.create_time>='{$start_time}' and a1.create_time<'{$end_time}' and a1.result=1 and
    not EXISTS(select 1 from self_pay_zf2860.{$tmp_table_name} a3 where a1.id=a3.pay_id and a3.app_id={$row['app_id']} )";
                    $list2 = $this->_dbDao->mget($sql);
                    if(!empty($list2)){
                        foreach ($list2 as $idx2=>$row2) {
                            $wirteStr = "";
                            if($this->isWirte){
                                $wirteStr = " -----> 写入完成";
                                $this->sendMoney($row2,$row2['real_amount']);
                            }
                            $position = $idx2+1;
                            $this->log("{$position}. 订单表ID={$row2['id']} order_id={$row2['order_id']} real_amount={$row2['real_amount']} create_time={$row2['create_time']}{$wirteStr}");
                        }
                    }
                }else{
                    $sql = "select pay_id,order_id,count(*) cnt from self_pay_zf2860.{$tmp_table_name} a3 where a3.app_id={$row['app_id']} group by pay_id,order_id having count(*)>1";
                    $list2 = $this->_dbDao->mget($sql);
                    if(!empty($list2)){
                        foreach ($list2 as $idx2=>$row2) {
                            $position = $idx2+1;
                            $this->log("{$position}. 订单表ID={$row2['pay_id']} order_id={$row2['order_id']} 重复次数:{$row2['cnt']}");
                        }
                    }
                }
            }
        }
        if($num_app>0){
            $this->log("合计：统计异常商户{$num_app}个，漏掉总数：{$num_pay}");
        }else{
            $this->log("合计：无差异");
        }
        $this->_dbDao->query("drop table {$tmp_table_name}");
        $this->log("删除【".date("Y-m-d",$reportDate)."】临时数据表");
        $this->log("----------------------------- end -----------------------------");
        die;
    }
    
    
    private function sendMoney($payInfo, $realMoney) {

        $lastLevelPoint = $payInfo['current_point'];
	if (!$payInfo['proxy_id']) {

            
            $point = 0;
            $obj = BooController::get('Obj_Plat_Info');
            $list = $obj->getList();
            foreach ($list as $key => $tmpInfo) {
                if ($tmpInfo['p_id'] == $payInfo['p_id']) {
                    $point = $tmpInfo['p_point'];
                    break;
                }
            }

            if (!$point) {
                $this->log("\t\t上游通道ID={$payInfo['p_id']} 找不到");
            }
            
            $dxpoint = $lastLevelPoint - $point;

            if ($dxpoint < 0) {
                $dxpoint = 0;
                $this->log("\t\t充值费率={$lastLevelPoint}<上游费率={$point}");
            }
            $reCnt = $this->_dbDao->get("select count(*) cnt from system_pay where app_id={$payInfo['app_id']} and pay_id='{$payInfo['id']}'");
            if($reCnt['cnt']>0){
                return true;
            }
            $systemMoney = $realMoney * $dxpoint;
            $insertData = array();
            $insertData['app_id'] = $payInfo['app_id'];
            $insertData['app_name'] = $payInfo['app_name'];
            $insertData['p_id'] = $payInfo['p_id'];
            $insertData['order_id'] = $payInfo['order_id'];
            $insertData['pay_id'] = $payInfo['id'];
            $insertData['amount'] = $payInfo['amount'];
            $insertData['real_amount'] = $realMoney;
            $insertData['pt_id'] = $payInfo['pt_id'];
            $insertData['create_time'] = $payInfo['create_time'];
            $insertData['end_time'] = date('Y-m-d H:i:s');
            $insertData['create_date'] = $payInfo['create_date'];
            $insertData['current_point'] = $point;
            $insertData['current_fee_rate'] = $dxpoint;
            $insertData['current_fee'] = $systemMoney;

            $systemPayObj = BooController::get('Obj_Admin_Pay');
            $systemPayObj->insert($insertData);

            return true;
        }

        $obj = BooController::get('Obj_Proxy_Info');
        $info = $obj->getInfoById($payInfo['proxy_id']);
        $platAppInfo = json_decode($info['plat_app_info'], true);
        $pointInfo = $platAppInfo[$payInfo['p_id']];
        foreach ($platAppInfo as $tmpInfo) {
            if ($tmpInfo['payType'] == $payInfo['pt_id']) {
                $pointInfo = $tmpInfo;
                break;
            }
        }
        
        //代理通道查找不到，这里当作不返点处理
        if($pointInfo['point']>0){
            

            $dxpoint = $payInfo['current_point'] - $pointInfo['point'];
            if ($dxpoint < 0) {
                $dxpoint = 0;
                $this->log("\t\t充值费率={$lastLevelPoint}<上游费率={$point}");
            }
            $proxyMoney = $realMoney * $dxpoint;

            $nowDateTime = date('Y-m-d H:i:s');
            $insertData = array();
            $insertData['app_id'] = $payInfo['app_id'];
            $insertData['app_name'] = $payInfo['app_name'];
            $insertData['proxy_id'] = $info['proxy_id'];
            $insertData['proxy_name'] = $info['proxy_name'];
            $insertData['p_id'] = $payInfo['p_id'];
            $insertData['order_id'] = $payInfo['order_id'];
            $insertData['amount'] = $payInfo['amount'];
            $insertData['real_amount'] = $realMoney;
            $insertData['pt_id'] = $payInfo['pt_id'];
            $insertData['pay_id'] = $payInfo['id'];
            $insertData['create_time'] = $payInfo['create_time'];
            $insertData['end_time'] = $nowDateTime;
            $insertData['create_date'] = $payInfo['create_date'];
            $insertData['current_point'] = $pointInfo['point'];
            $insertData['current_fee_rate'] = $dxpoint;
            $insertData['current_fee'] = $proxyMoney;

            $proxyPayObj = BooController::get('Obj_Proxy_Pay');
            
            $dbDao = Dao_Mysql_AppPay::getInstance();
            $reCnt = $dbDao->get("select count(*) cnt from proxy_pay_{$info['proxy_id']} where app_id={$payInfo['app_id']} and pay_id='{$payInfo['id']}'");
            if($reCnt['cnt']==0){
                $proxyPayObj->insert($payInfo['proxy_id'], $insertData);
            }
            

            $lastLevelPoint = $pointInfo['point'];
        
            if ($info['user_tree']) {
                $treeInfo = explode(',', $info['user_tree']);
                krsort($treeInfo);

                foreach ($treeInfo as $tmpProxyId) {

                    if ($lastLevelPoint - $pointInfo['point'] < 0) {
                        continue;
                    }

                    $info = $obj->getInfoById($tmpProxyId);
                    $platAppInfo = json_decode($info['plat_app_info'], true);
                    $pointInfo = $platAppInfo[$payInfo['p_id']];
                    foreach ($platAppInfo as $tmpInfo) {
                        if ($tmpInfo['payType'] == $payInfo['pt_id']) {
                            $pointInfo = $tmpInfo;
                            break;
                        }
                    }
                    $proxyMoney = $realMoney * ($lastLevelPoint - $pointInfo['point']);
                    $lastLevelPoint = $pointInfo['point'];

                    $insertData = array();
                    $insertData['app_id'] = $payInfo['app_id'];
                    $insertData['app_name'] = $payInfo['app_name'];
                    $insertData['proxy_id'] = $info['proxy_id'];
                    $insertData['proxy_name'] = $info['proxy_name'];
                    $insertData['p_id'] = $payInfo['p_id'];
                    $insertData['order_id'] = $payInfo['order_id'];
                    $insertData['amount'] = $payInfo['amount'];
                    $insertData['real_amount'] = $realMoney;
                    $insertData['pt_id'] = $payInfo['pt_id'];
                    $insertData['pay_id'] = $payInfo['id'];
                    $insertData['create_time'] = $payInfo['create_time'];
                    $insertData['end_time'] = $nowDateTime;
                    $insertData['create_date'] = $payInfo['create_date'];
                    $insertData['current_point'] = $pointInfo['point'];
                    $insertData['current_fee_rate'] = $lastLevelPoint - $pointInfo['point'];
                    $insertData['current_fee'] = $proxyMoney;

                    $reCnt = $dbDao->get("select count(*) cnt from proxy_pay_{$tmpProxyId} where app_id={$payInfo['app_id']} and pay_id='{$payInfo['id']}'");
                    if($reCnt['cnt']==0){
                        $proxyPayObj->insert($tmpProxyId, $insertData);
                    }
                    
                }

            }
        }

        $point = 0;
        $obj = BooController::get('Obj_Plat_Info');
        $list = $obj->getList();
        foreach ($list as $key => $tmpInfo) {
            if ($tmpInfo['p_id'] == $payInfo['p_id']) {
                $point = $tmpInfo['p_point'];
                break;
            }
        }

        if (!$point) {
            $this->log("\t\t上游通道ID={$payInfo['p_id']} 找不到");
        }

        $dxpoint = $lastLevelPoint - $point;

        if ($dxpoint < 0) {
            $dxpoint = 0;
            $this->log("\t\t充值费率={$lastLevelPoint}<上游费率={$point}");
        }
        
        $reCnt = $this->_dbDao->get("select count(*) cnt from system_pay where app_id={$payInfo['app_id']} and pay_id='{$payInfo['id']}'");
        if($reCnt['cnt']>0){
            return true;
        }
        $systemMoney = $realMoney * $dxpoint;
        $insertData = array();
        $insertData['app_id'] = $payInfo['app_id'];
        $insertData['app_name'] = $payInfo['app_name'];
        $insertData['proxy_id'] = $payInfo['proxy_id'];
        $insertData['proxy_name'] = $payInfo['proxy_name'];
        $insertData['p_id'] = $payInfo['p_id'];
        $insertData['order_id'] = $payInfo['order_id'];
        $insertData['pay_id'] = $payInfo['id'];
        $insertData['amount'] = $payInfo['amount'];
        $insertData['real_amount'] = $realMoney;
        $insertData['pt_id'] = $payInfo['pt_id'];
        $insertData['create_time'] = $payInfo['create_time'];
        $insertData['end_time'] = $nowDateTime;
        $insertData['create_date'] = $payInfo['create_date'];
        $insertData['current_point'] = $point;
        $insertData['current_fee_rate'] = $dxpoint;
        $insertData['current_fee'] = $systemMoney;

        $systemPayObj = BooController::get('Obj_Admin_Pay');
        $systemPayObj->insert($insertData);

        return true;
    }

    private function log($msg){
        echo date('Y-m-d H:i:s')."\t".$msg."\n";
    }
}
