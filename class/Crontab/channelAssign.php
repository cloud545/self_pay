<?php

/**
 * 更新商户配置
 *
 * Class Crontab_channelAssign
 */
class Crontab_channelAssign extends Crontab
{
    
    protected function _do()
    {
        
        ////商户列表
        $appObj     = BooController::get('Obj_App_Info');
        $tmpAppList = $appObj->getList();
        
        /**
         * @var $obj Obj_App_ChannelAssign
         */
        $obj = BooController::get('Obj_App_ChannelAssign');
        foreach ($tmpAppList as $key => $item) {
            $channels = json_decode($item['plat_app_info'], true);
            if (!empty($channels)) {
                foreach ($channels as $k => $v) {
                    $obj->insert(
                        [
                            'app_id'       => $item['app_id'],
                            'pt_id'        => $v['payType'],
                            'p_id'         => $k,
                            'fee'          => $v['point'],
                            'status'       => 'N',
                            'created_time' => time(),
                            'updated_time' => time(),
                            'operator'     => 'admin',
                        ]
                    );
                }
            }
        }
    }
    
}
