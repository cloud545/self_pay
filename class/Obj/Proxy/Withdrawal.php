<?php
class Obj_Proxy_Withdrawal extends Obj_Proxy {

    public function __construct() {
        $this->_dbDao = Dao_Mysql_Default::getInstance();
        $this->_cacheDao = Dao_Redis_Default::getInstance();
    }

    public function getInfoById($id) {

        if(!$id){
            return false;
        }

        $sql = "SELECT * FROM `proxy_withdrawal` WHERE `id` = :id";
        $info = $this->_dbDao->get($sql, array(':id' => $id));
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getCount($proxyId = 0, $status = 'all', $startTime = '', $endTime = '') {

        $sql = "select count(`id`) as countNum from `proxy_withdrawal`";

        $whereArr = array();
        if ($proxyId) {
            $whereArr[] = "`proxy_id` = {$proxyId}";
        }

        if ($status != 'all') {
            if (is_array($status)) {
                $status = implode(',', $status);
                $whereArr[] = "`status` IN({$status})";
            } else {
                $whereArr[] = "`status` = {$status}";
            }
        }

        if ($startTime && $endTime) {
            $whereArr[] = "`create_time` >= '{$startTime}' AND `create_time` < '{$endTime}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `create_time` DESC";

        $info = $this->_dbDao->get($sql);
        return $info['countNum'];
    }

    public function getSum($proxyId = 0, $status = 'all', $startTime = '', $endTime = '') {

        $sql = "select sum(`amount`) as sumAmount, sum(`fee`) as sumFee from `proxy_withdrawal`";

        $whereArr = array();
        if ($proxyId) {
            $whereArr[] = "`proxy_id` = {$proxyId}";
        }

        if ($status != 'all') {
            $whereArr[] = "`status` = {$status}";
        }

        if ($startTime && $endTime) {
            $whereArr[] = "`create_time` >= '{$startTime}' AND `create_time` < '{$endTime}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `create_time` DESC";

        $info = $this->_dbDao->get($sql);
        return $info;
    }

    public function getPageList($proxyId = 0, $status = 'all', $startTime = '', $endTime = '', $id = 0) {

        $sql = "select * from `proxy_withdrawal`";

        $whereArr = array();
        if ($id) {
            $whereArr[] = "`id` = {$id}";
        }

        if ($proxyId) {
            $whereArr[] = "`proxy_id` = {$proxyId}";
        }

        if ($status != 'all') {
            $whereArr[] = "`status` = {$status}";
        }

        if ($startTime && $endTime) {
            $whereArr[] = "`create_time` >= '{$startTime}' AND `create_time` < '{$endTime}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `create_time` DESC";
        $pageList = $this->_dbDao->pget($sql);

        return $pageList;
    }

    public function update($id, $updateData) {

        if(!$id || !$updateData){
            return false;
        }

        $rs = $this->_dbDao->set('`proxy_withdrawal`', $updateData, '`id` = :id', array(':id' => $id));
        return $rs;
    }

    public function insert($insertData) {

        if(!$insertData){
            return false;
        }

        $rs = $this->_dbDao->set('`proxy_withdrawal`', $insertData);
        return $rs;
    }

    public function delete($id) {

        if(!$id){
            return false;
        }

        $rs = $this->_dbDao->del('`proxy_withdrawal`', '`id` = :id', array(':id' => $id));
        return $rs;
    }

}
