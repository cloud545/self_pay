<?php
class Obj_Proxy_Menu extends Obj_Proxy {

    public function __construct() {
        $this->_dbDao = Dao_Mysql_Default::getInstance();
        $this->_cacheDao = Dao_Redis_Default::getInstance();
    }
	
	public function getInfo($id) {
		
		if(!$id){
			return false;
		}
		
		$sql = "SELECT * FROM `proxy_menu` WHERE `menu_id` = :id";
		$info = $this->_dbDao->get($sql, array(':id' => $id));
		if (!$info) {
			return false;
		}
		
		return $info;
	}

    public function getInfoByTarget($target) {

        if(!$target){
            return false;
        }

        $sql = "SELECT * FROM `proxy_menu` WHERE `menu_target` = :target";
        $info = $this->_dbDao->get($sql, array(':target' => $target));
        if (!$info) {
            return false;
        }

        return $info;
    }

	public function getList($fields = '', $conditionStr = '') {

        if (!$fields) {
            $fields = '*';
        }

	    if ($conditionStr) {
            $sql = "SELECT {$fields} FROM `proxy_menu` WHERE {$conditionStr} ORDER BY `menu_sort` ASC";
        } else {
            $sql = "SELECT {$fields} FROM `proxy_menu` ORDER BY `menu_sort` ASC";
        }
		$list = $this->_dbDao->mget($sql);
		if (!$list) {
			return array();
		}

		return $list;
	}
	
	public function update($id, $updateData) {
		
		if(!$id || !$updateData){
			return false;
		}
		
		$rs = $this->_dbDao->set('`proxy_menu`', $updateData, '`menu_id` = :id', array(':id' => $id));
		return $rs;
	}
	
	public function insert($insertData) {
		
		if(!$insertData){
			return false;
		}
		
		$rs = $this->_dbDao->set('`proxy_menu`', $insertData);
		return $rs;
	}

    public function delete($id) {

        if(!$id){
            return false;
        }

        $rs = $this->_dbDao->del('`proxy_menu`', '`menu_id` = :id', array(':id' => $id));
        return $rs;
    }

    /**
     * 更新父级菜单和排序
     *
     * @param $parentNodes
     * @param $sortNodes
     * @return bool|int
     */
    public function updateParentAndSort($parentNodes, $sortNodes) {

        if(!$parentNodes && !$sortNodes){
            return false;
        }

        $idArr = array();
        $sql = "UPDATE `proxy_menu` SET ";
        if ($parentNodes) {
            $sql .= "`menu_parent_id` = CASE `menu_id` ";
            foreach ($parentNodes as $id => $ordinal) {
                $sql .= sprintf("WHEN %d THEN %d ", $id, $ordinal);
                $idArr[$id] = $id;
            }
        }

        if ($parentNodes && $sortNodes) {
            $sql .= "END, `menu_sort` = CASE `menu_id` ";
        } elseif (!$parentNodes && $sortNodes) {
            $sql .= "`menu_sort` = CASE `menu_id` ";
        }

        if ($sortNodes) {
            foreach ($sortNodes as $id => $ordinal) {
                $sql .= sprintf("WHEN %d THEN %d ", $id, $ordinal);
                $idArr[$id] = $id;
            }
        }

        $ids = implode(',', $idArr);
        $sql .= "END WHERE `menu_id` IN ($ids)";
        $rs = $this->_dbDao->query($sql);
        return $rs;
    }
}