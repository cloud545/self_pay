<?php
class Obj_Proxy_Balance extends Obj_Proxy {

    public function __construct() {
        $this->_dbDao = Dao_Mysql_Default::getInstance();
        $this->_cacheDao = Dao_Redis_Default::getInstance();
    }

    public function getInfoById($id) {

        if(!$id){
            return false;
        }

        $sql = "SELECT * FROM `proxy_balance` WHERE `id` = :id";
        $info = $this->_dbDao->get($sql, array(':id' => $id));
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getInfoByProxyId($proxyId) {

        if(!$proxyId){
            return false;
        }

        $sql = "SELECT * FROM `proxy_balance` WHERE `proxy_id` = :id";
        $info = $this->_dbDao->get($sql, array(':id' => $proxyId));
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getPageList($proxyId = 0, $status = 'all') {

        $sql = "select * from `proxy_balance`";

        $whereArr = array();
        if ($proxyId) {
            $whereArr[] = "`proxy_id` = {$proxyId}";
        }

        if ($status != 'all') {
            $whereArr[] = "`lock_status` = {$status}";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `id` DESC";
        $pageList = $this->_dbDao->pget($sql);

        return $pageList;
    }

    public function update($id, $updateData) {

        if(!$id || !$updateData){
            return false;
        }

        $rs = $this->_dbDao->set('`proxy_balance`', $updateData, '`id` = :id', array(':id' => $id));
        return $rs;
    }

    public function insert($insertData) {

        if(!$insertData){
            return false;
        }

        $rs = $this->_dbDao->set('`proxy_balance`', $insertData);
        return $rs;
    }

    public function delete($id) {

        if(!$id){
            return false;
        }

        $info = $this->getInfoById($id);
        if (!$info) {
            return false;
        }

        $rs = $this->_dbDao->del('`proxy_balance`', '`id` = :id', array(':id' => $id));
        return $rs;
    }
}