<?php
class Obj_Proxy_ReportMonth extends Obj_Proxy {

    public function __construct() {
        $this->_dbDao = Dao_Mysql_Default::getInstance();
        $this->_cacheDao = Dao_Redis_Default::getInstance();
    }

    public function getInfoById($id) {

        if(!$id){
            return false;
        }

        $sql = "SELECT * FROM `proxy_report_month` WHERE `id` = :id";
        $info = $this->_dbDao->get($sql, array(':id' => $id));
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getInfo($proxyId, $date = '') {

        $sql = "select * from `proxy_report_month`";

        if(!$proxyId){
            return false;
        }

        $whereArr = array();
        if ($proxyId) {
            $whereArr[] = "`proxy_id` = {$proxyId}";
        }

        if ($date) {
            $whereArr[] = "`report_month` = '{$date}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $info = $this->_dbDao->get($sql);
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getPageList($proxyId = 0, $startTime = '', $endTime = '') {

        $sql = "select * from `proxy_report_month`";

        $whereArr = array();
        if ($proxyId) {
            $whereArr[] = "`proxy_id` = {$proxyId}";
        }

        if ($startTime && $endTime) {
            $whereArr[] = "`report_month` >= '{$startTime}' AND `report_month` < '{$endTime}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `report_month` DESC";
        $pageList = $this->_dbDao->pget($sql);

        return $pageList;
    }

    public function getSum($proxyId = 0, $startTime = '', $endTime = '') {

        $sql = "select sum(`order_number`) as orderNum, sum(`real_amount`) as sumRealAmount, sum(`fee`) as sumFee from `proxy_report_month`";

        $whereArr = array();
        if ($proxyId) {
            $whereArr[] = "`proxy_id` = {$proxyId}";
        }

        if ($startTime && $endTime) {
            $whereArr[] = "`report_month` >= '{$startTime}' AND `report_month` < '{$endTime}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `report_month` DESC";
        $info = $this->_dbDao->get($sql);

        return $info;
    }

    public function update($id, $updateData) {

        if(!$id || !$updateData){
            return false;
        }

        $rs = $this->_dbDao->set('`proxy_report_month`', $updateData, '`id` = :id', array(':id' => $id));
        return $rs;
    }

    public function insert($insertData) {

        if(!$insertData){
            return false;
        }

        $rs = $this->_dbDao->set('`proxy_report_month`', $insertData);
        return $rs;
    }

    public function delete($id) {

        if(!$id){
            return false;
        }

        $rs = $this->_dbDao->del('`proxy_report_month`', '`id` = :id', array(':id' => $id));
        return $rs;
    }

}
