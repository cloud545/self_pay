<?php
class Obj_Proxy_Pay extends Obj_Proxy {

    public function __construct() {
        $this->_dbDao = Dao_Mysql_AppPay::getInstance();
        $this->_cacheDao = Dao_Redis_AppDataTable::getInstance();
    }

    public function getInfoById($proxyId, $id) {

        if (!$proxyId || !$id) {
            return false;
        }

        $tableName = $this->prepareTable($proxyId);
        if (!$tableName) {
            return false;
        }

        $sql = "SELECT * FROM `{$tableName}` WHERE `id` = {$id}";
        $info = $this->_dbDao->get($sql);
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getList($proxyId, $platId = 0, $type = 0, $startDate = '', $endDate = '') {

        if (!$proxyId) {
            return false;
        }

        $tableName = $this->prepareTable($proxyId);
        if (!$tableName) {
            return false;
        }

        $sql = "select * from `{$tableName}`";

        $whereArr = array();
        if ($platId) {
            $whereArr[] = "`p_id` = {$platId}";
        }

        if ($type) {
            $whereArr[] = "`pt_id` = {$type}";
        }

        if ($startDate && $endDate) {
            $whereArr[] = "`create_time` >= '{$startDate}' AND `create_time` < '{$endDate}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `create_time` DESC";
        $pageList = $this->_dbDao->pget($sql);

        return $pageList;
    }

    public function getPageList($proxyId, $platId = 0, $id = 0, $type = 0, $startDate = '', $endDate = '', $appId = 0) {

        if (!$proxyId) {
            return false;
        }

        $tableName = $this->prepareTable($proxyId);
        if (!$tableName) {
            return false;
        }

        $sql = "select * from `{$tableName}`";

        $whereArr = array();
        if ($platId) {
            $whereArr[] = "`p_id` = {$platId}";
        }

        if ($appId) {
            $whereArr[] = "`app_id` = {$appId}";
        }

        if ($id) {
            $whereArr[] = "`id` = {$id}";
        }

        if ($type) {
            $whereArr[] = "`pt_id` = {$type}";
        }

        if ($startDate && $endDate) {
            $whereArr[] = "`create_time` >= '{$startDate}' AND `create_time` < '{$endDate}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `create_time` DESC";
        $pageList = $this->_dbDao->pget($sql);

        return $pageList;
    }

    public function getCount($proxyId, $platId = 0, $id = 0, $type = 0, $startDate = '', $endDate = '', $appId = 0) {

        if (!$proxyId) {
            return false;
        }

        $tableName = $this->prepareTable($proxyId);
        if (!$tableName) {
            return false;
        }

        $sql = "select count(`id`) as countNum from `{$tableName}`";

        $whereArr = array();
        if ($platId) {
            $whereArr[] = "`p_id` = {$platId}";
        }

        if ($appId) {
            $whereArr[] = "`app_id` = {$appId}";
        }

        if ($id) {
            $whereArr[] = "`id` = {$id}";
        }

        if ($type) {
            $whereArr[] = "`pt_id` = {$type}";
        }

        if ($startDate && $endDate) {
            $whereArr[] = "`create_time` >= '{$startDate}' AND `create_time` < '{$endDate}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $info = $this->_dbDao->get($sql);
        return $info['countNum'];
    }

    public function getSum($proxyId, $platId = 0, $id = 0, $type = 0, $startDate = '', $endDate = '', $appId = 0) {

        if (!$proxyId) {
            return false;
        }

        $tableName = $this->prepareTable($proxyId);
        if (!$tableName) {
            return false;
        }

        $sql = "select sum(`amount`) as sumAmount, sum(`real_amount`) as sumRealAmount, sum(`current_fee`) as sumFee from `{$tableName}`";

        $whereArr = array();
        if ($platId) {
            $whereArr[] = "`p_id` = {$platId}";
        }

        if ($appId) {
            $whereArr[] = "`app_id` = {$appId}";
        }

        if ($id) {
            $whereArr[] = "`id` = {$id}";
        }

        if ($type) {
            $whereArr[] = "`pt_id` = {$type}";
        }

        if ($startDate && $endDate) {
            $whereArr[] = "`create_time` >= '{$startDate}' AND `create_time` < '{$endDate}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $info = $this->_dbDao->get($sql);

        if ($info['sumRealAmount'] > 0) {
            $sql = "SELECT * FROM `{$tableName}`";
            if ($whereArr) {
                $sql .= " WHERE " . implode(' AND ', $whereArr);
            }
            $sql .= " ORDER BY `create_time` DESC LIMIT 1";

            $lastInfo = $this->_dbDao->get($sql);
            $info['last_create_time'] = $lastInfo['create_time'];
        }

        return $info;
    }
    public function getCountByAppPayId($proxyId,$appId,$payId) {

        if (!$proxyId || !$appId || !$payId) {
            return false;
        }
        
        $tableName = $this->prepareTable($proxyId);
        if (!$tableName) {
            return false;
        }

        $sql = "select count(`id`) as countNum from `{$tableName}` where `app_id` = {$appId} and `pay_id` = {$payId}";
        $info = $this->_dbDao->get($sql);
        return $info['countNum'];
    }

    public function update($proxyId, $id, $updateData) {

        if(!$proxyId || !$id || !$updateData){
            return false;
        }

        $tableName = $this->prepareTable($proxyId);
        if (!$tableName) {
            return false;
        }

        $rs = $this->_dbDao->set($tableName, $updateData, '`id` = :id', array(':id' => $id));
        return $rs;
    }

    public function insert($proxyId, $insertData) {

        if(!$proxyId || !$insertData){
            return false;
        }

        $tableName = $this->prepareTable($proxyId);
        if (!$tableName) {
            return false;
        }

        $rs = $this->_dbDao->set($tableName, $insertData);
        return $rs;
    }

    /**
     * 返回分表后的表名
     *
     * @param $proxyId
     * @return bool|string
     * @throws ErrorException
     */
    protected function prepareTable($proxyId) {

        // 分表方法 按app分表
        $tableName = "proxy_pay_" . $proxyId;

        // 检查表是否存在
        if ($this->checkTableExists($tableName) === false) {
            return false;
        }

        return $tableName;
    }

    /**
     * 检查是否有相应的数据库
     *
     * @param $tableName
     * @return bool|int
     * @throws ErrorException
     */
    private function checkTableExists($tableName) {

        $cacheKey = BooConfig::get('cacheKey.proxyPayTable') . $tableName;

        // 已经生成过这个表了
        if ($this->_cacheDao->get($cacheKey)) {
            return true;
        }

        // 缓存失效后执行一次查询看是否已经生成过这个表
        if ($this->_dbDao->checkTable($tableName)) {
            $this->_cacheDao->set($cacheKey, 1);
            return true;
        }

        $rs = $this->_dbDao->query("
            CREATE TABLE `{$tableName}` (
              `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增id',
              `app_id` int(6) UNSIGNED NOT NULL COMMENT '应用id',
              `app_name` varchar(30) NOT NULL COMMENT '商户名称',
              `proxy_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '代理id',
              `proxy_name` varchar(30) NOT NULL COMMENT '代理名称',
              `p_id` int(6) UNSIGNED NOT NULL COMMENT '平台id',
              `order_id` varchar(60) NOT NULL COMMENT '充值订单id',
              `pay_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '商户用户充值表id',
              `amount` decimal(14,4) UNSIGNED NOT NULL DEFAULT '0.0000' COMMENT '充值金额',
              `real_amount` decimal(14,4) UNSIGNED NOT NULL DEFAULT '0.0000' COMMENT '真实充值金额',
              `pt_id` int(6) UNSIGNED NOT NULL DEFAULT '0' COMMENT '支付类型',
              `create_time` datetime NOT NULL COMMENT '充值时间',
              `end_time` datetime NOT NULL COMMENT '充值完成时间',
              `create_date` date NOT NULL COMMENT '创建日期',
              `current_point` decimal(14,4) UNSIGNED NOT NULL DEFAULT '0.0000' COMMENT '当前点数',
              `current_fee_rate` decimal(14,4) UNSIGNED NOT NULL DEFAULT '0.0000' COMMENT '本次支付的手续费率',
              `current_fee` decimal(14,4) UNSIGNED NOT NULL DEFAULT '0.0000' COMMENT '本次支付的手续费',
              PRIMARY KEY (`id`),
              KEY `app_id` (`app_id`,`order_id`),
              KEY `create_time` (`create_time`),
              KEY `end_time` (`end_time`),
              KEY `pt_id` (`pt_id`),
              KEY `app_name` (`app_name`),
              KEY `p_id` (`p_id`),
              KEY `create_date` (`create_date`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='支付表';
		");

        // 生成表成功缓存标记
        if ($rs) {
            $this->_cacheDao->set($cacheKey, 1);
        }

        return $rs;
    }
}
