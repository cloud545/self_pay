<?php
class Obj_Proxy_Info extends Obj_Proxy {

    public function __construct() {
        $this->_dbDao = Dao_Mysql_Default::getInstance();
        $this->_cacheDao = Dao_Redis_Default::getInstance();
    }
	
    public function getInfoById($id) {
        if(!$id){
             return false;
        }
        $cacheKey = BooConfig::get('cacheKey.proxyById') . $id;
        $cacheInfo = $this->_cacheDao->get($cacheKey);
        if ($cacheInfo) {
            return json_decode($cacheInfo, true);
        }
        $sql = "SELECT * FROM `proxys` WHERE `proxy_id` = :id";
        $info = $this->_dbDao->get($sql, array(':id' => $id));
        if (!$info) {
            return false;
        }
        $this->_cacheDao->set($cacheKey, json_encode($info), 86400);
        return $info;
    }

    public function getInfoByName($name) {

        if(!$name){
            return false;
        }

        $sql = "SELECT * FROM `proxys` WHERE `proxy_name` = :userName";
        $info = $this->_dbDao->get($sql, array(':userName' => $name));
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getAllList() {

        $sql = "SELECT * FROM `proxys`";
        $list = $this->_dbDao->mget($sql);
        if (!$list) {
            return false;
        }

        return $list;
    }

    public function getListByIds($ids) {

        if(!$ids){
            return false;
        }

        $ids = implode(',', $ids);
        $sql = "SELECT * FROM `proxys` WHERE `proxy_id` IN({$ids})";
        $list = $this->_dbDao->mget($sql);
        if (!$list) {
            return false;
        }

        return $list;
    }

    public function getList($parentId = 0) {

        $sql = "select * from `proxys`";

        $whereArr = array();

        if ($parentId) {
            $whereArr[] = "`parent_id` = {$parentId}";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `create_time` DESC";
        $list = $this->_dbDao->mget($sql);

        return $list;
    }

    public function getByFieldsList($fields = '') {

        if ($fields) {
            $sql = "SELECT {$fields} FROM `proxys` ORDER BY `proxy_id` DESC ";
        } else {
            $sql = "SELECT * FROM `proxys` ORDER BY `proxy_id` DESC ";
        }
        $list = $this->_dbDao->mget($sql);
        if (!$list) {
            return array();
        }

        return $list;
    }

    public function getPageList($proxyId = 0, $proxyName = '', $parentId = 0, $adminId = 0) {

        $sql = "select * from `proxys`";

        $whereArr = array();
        if ($proxyId) {
            $whereArr[] = "`proxy_id` = {$proxyId}";
        }

        if ($proxyName) {
            $whereArr[] = "`proxy_name` = '{$proxyName}'";
        }

        if ($parentId) {
            $whereArr[] = "`parent_id` = {$parentId}";
        }

        if ($adminId) {
            $whereArr[] = "`add_admin_id` = {$adminId}";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `create_time` DESC";
        $pageList = $this->_dbDao->pget($sql);

        return $pageList;
    }
	
    public function update($id, $updateData) {
        if(!$id || !$updateData){
             return false;
        }
        $cacheKey = BooConfig::get('cacheKey.proxyById') . $id;
        $this->_cacheDao->del($cacheKey);
        $rs = $this->_dbDao->set('`proxys`', $updateData, '`proxy_id` = :id', array(':id' => $id));
        return $rs;
    }
	
	public function insert($insertData) {
		
		if(!$insertData){
			return false;
		}
		
		$rs = $this->_dbDao->set('`proxys`', $insertData);
		return $rs;
	}

    public function delete($id) {

        if(!$id){
            return false;
        }

        $cacheKey = BooConfig::get('cacheKey.proxyById') . $id;
        $this->_cacheDao->del($cacheKey);

        $rs = $this->_dbDao->del('`proxys`', '`	proxy_id` = :id', array(':id' => $id));
        return $rs;
    }
}