<?php
class Obj_Plat_Info extends Obj_Plat {
	
	public function getInfoById($id) {
		
		if(!$id){
			return false;
		}

        $cacheKey = BooConfig::get('cacheKey.platInfo') . $id;
        $cacheInfo = $this->_cacheDao->get($cacheKey);
        if ($cacheInfo) {
            return json_decode($cacheInfo, true);
        }
		
		$sql = "SELECT * FROM `plat` WHERE `p_id` = :id";
		$info = $this->_dbDao->get($sql, array(':id' => $id));
		if (!$info) {
			return false;
		}

        $this->_cacheDao->set($cacheKey, json_encode($info), 86400);
		
		return $info;
	}

	public function getList($fields = '') {

        $cacheKey = BooConfig::get('cacheKey.dbDataPlatList');
        $cacheInfo = $this->_cacheDao->get($cacheKey);
        if ($cacheInfo) {
            return json_decode($cacheInfo, true);
        }

        if ($fields) {
            $sql = "SELECT {$fields} FROM `plat` ORDER BY `p_id` DESC ";
        } else {
            $sql = "SELECT * FROM `plat` ORDER BY `p_id` DESC ";
        }
		$list = $this->_dbDao->mget($sql);
		if (!$list) {
			return array();
		}

        $this->_cacheDao->set($cacheKey, json_encode($list), 86400);

		return $list;
	}
	
	public function update($id, $updateData) {
		
		if(!$id || !$updateData){
			return false;
		}

        $cacheKey = BooConfig::get('cacheKey.platInfo') . $id;
        $this->_cacheDao->del($cacheKey);

        $cacheKey = BooConfig::get('cacheKey.dbDataPlatList');
        $this->_cacheDao->del($cacheKey);
		
		$rs = $this->_dbDao->set('`plat`', $updateData, '`p_id` = :id', array(':id' => $id));
		return $rs;
	}
	
	public function insert($insertData) {
		
		if(!$insertData){
			return false;
		}

        $cacheKey = BooConfig::get('cacheKey.dbDataPlatList');
        $this->_cacheDao->del($cacheKey);
		
		$rs = $this->_dbDao->set('`plat`', $insertData);
		return $rs;
	}

    public function delete($id) {

        if(!$id){
            return false;
        }

        $cacheKey = BooConfig::get('cacheKey.platInfo') . $id;
        $this->_cacheDao->del($cacheKey);

        $cacheKey = BooConfig::get('cacheKey.dbDataPlatList');
        $this->_cacheDao->del($cacheKey);

        $rs = $this->_dbDao->del('`plat`', '`p_id` = :id', array(':id' => $id));
        return $rs;
    }
}