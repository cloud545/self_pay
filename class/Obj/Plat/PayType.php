<?php
class Obj_Plat_PayType extends Obj_Plat {
	
	public function getInfoById($id) {
		
		if(!$id){
			return false;
		}
		
		$sql = "SELECT * FROM `pay_type` WHERE `pt_id` = :id";
		$info = $this->_dbDao->get($sql, array(':id' => $id));
		if (!$info) {
			return false;
		}
		
		return $info;
	}

	public function getAll() {
        $list = $this->_dbDao->mget("select * from `pay_type`");
        return $list;
    }

	public function getList($fields = '') {

        if ($fields) {
            $sql = "SELECT {$fields} FROM `pay_type` ORDER BY `pt_id` DESC ";
        } else {
            $sql = "SELECT * FROM `pay_type` ORDER BY `pt_id` DESC ";
        }
		$list = $this->_dbDao->mget($sql);
		if (!$list) {
			return array();
		}

		return $list;
	}
	
	public function update($id, $updateData) {
		
		if(!$id || !$updateData){
			return false;
		}
		
		$rs = $this->_dbDao->set('`pay_type`', $updateData, '`pt_id` = :id', array(':id' => $id));
		return $rs;
	}
	
	public function insert($insertData) {
		
		if(!$insertData){
			return false;
		}
		
		$rs = $this->_dbDao->set('`pay_type`', $insertData);
		return $rs;
	}

    public function delete($id) {

        if(!$id){
            return false;
        }

        $rs = $this->_dbDao->del('`pay_type`', '`pt_id` = :id', array(':id' => $id));
        return $rs;
    }
}