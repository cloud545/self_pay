<?php
class Obj_App_Info extends Obj_App {

    public function __construct() {
        $this->_dbDao = Dao_Mysql_Default::getInstance();
        $this->_cacheDao = Dao_Redis_Default::getInstance();
    }
	
	public function getInfoById($id) {

		if(!$id){
			return false;
		}

        $cacheKey = BooConfig::get('cacheKey.appById') . $id;
        $cacheInfo = $this->_cacheDao->get($cacheKey);
        if ($cacheInfo) {
            return json_decode($cacheInfo, true);
        }
		
		$sql = "SELECT * FROM `app` WHERE `id` = :id";
		$info = $this->_dbDao->get($sql, array(':id' => $id));
		if (!$info) {
			return false;
		}

        $this->_cacheDao->set($cacheKey, json_encode($info), 86400);
		
		return $info;
	}

    public function getInfoByName($name) {

        if(!$name){
            return false;
        }

        $sql = "SELECT * FROM `app` WHERE `name` = :userName";
        $info = $this->_dbDao->get($sql, array(':userName' => $name));
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getInfoByAppId($appId) {

        if(!$appId){
            return false;
        }

        $cacheKey = BooConfig::get('cacheKey.appByAppId') . $appId;
        $cacheInfo = $this->_cacheDao->get($cacheKey);
        if ($cacheInfo) {
            return json_decode($cacheInfo, true);
        }

        $sql = "SELECT * FROM `app` WHERE `app_id` = :id";
        $info = $this->_dbDao->get($sql, array(':id' => $appId));
        if (!$info) {
            return false;
        }

        $this->_cacheDao->set($cacheKey, json_encode($info), 86400);

        return $info;
    }

    //获取商户的第三方使用信息

    public function getPlatAppInfoByAppId($appId) {

        if(!$appId){
            return false;
        }

        $cacheKey = BooConfig::get('cacheKey.appByAppId') . $appId;
        $cacheInfo = $this->_cacheDao->get($cacheKey);
        if ($cacheInfo) {
            return json_decode($cacheInfo, true);
        }

        $sql = "SELECT `plat_app_info` FROM `app` WHERE `app_id` = :id";
        $info = $this->_dbDao->get($sql, array(':id' => $appId));
        if (!$info) {
            return false;
        }

        $this->_cacheDao->set($cacheKey, json_encode($info), 86400);

        return $info;
    }


	public function getList($fields = '') {

        $cacheKey = BooConfig::get('cacheKey.dbDataAppList');
        $cacheInfo = $this->_cacheDao->get($cacheKey);
        if ($cacheInfo) {
            return json_decode($cacheInfo, true);
        }

        if ($fields) {
            $sql = "SELECT {$fields} FROM `app` ORDER BY `id` DESC ";
        } else {
            $sql = "SELECT * FROM `app` ORDER BY `id` DESC ";
        }
		$list = $this->_dbDao->mget($sql);
		if (!$list) {
			return array();
		}

        $this->_cacheDao->set($cacheKey, json_encode($list), 86400);

		return $list;
	}

    public function getListByProxyId($proxyId) {

        if(!$proxyId){
            return array();
        }

        $sql = "select * from `app`";

        $whereArr = array();
        $whereArr[] = "`proxy_id` = {$proxyId}";

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `create_time` DESC";
        $list = $this->_dbDao->mget($sql);

        return $list;
    }

    public function getPageList($appId = 0, $appName = '', $proxyId = 0, $adminId = 0) {

        $sql = "select * from `app`";

        $whereArr = array();
        if ($appId) {
            $whereArr[] = "`app_id` = {$appId}";
        }

        if ($appName) {
            $whereArr[] = "`name` = '{$appName}' or `app_id` = '{$appName}'";
        }

        if ($proxyId) {
            $whereArr[] = "`proxy_id` = {$proxyId}";
        }

        if ($adminId) {
            $whereArr[] = "`add_admin_id` = {$adminId}";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `create_time` DESC";
        $pageList = $this->_dbDao->pget($sql);

        return $pageList;
    }

    public function update($id, $updateData) {

        if(!$id || !$updateData){
                            return false;
        }

        $info = $this->getInfoById($id);
        if (!$info) {
            return false;
        }

        $cacheKey = BooConfig::get('cacheKey.appByAppId') . $info['app_id'];
        $this->_cacheDao->del($cacheKey);

        $cacheKey = BooConfig::get('cacheKey.appById') . $id;
        $this->_cacheDao->del($cacheKey);

        $cacheKey = BooConfig::get('cacheKey.dbDataAppList');
        $this->_cacheDao->del($cacheKey);
		
        $rs = $this->_dbDao->set('`app`', $updateData, '`id` = :id', array(':id' => $id));
        return $rs;
    }

    //这个是一键分配渠道功能所添加的，凭直觉改的
    public function updateByAppId($appId, $updateData) {

        if(!$appId || !$updateData){
            return false;
        }

        $info = $this->getInfoByAppId($appId);
        if (!$info) {
            return false;
        }

        $cacheKey = BooConfig::get('cacheKey.appByAppId') . $appId;
        $this->_cacheDao->del($cacheKey);

        $cacheKey = BooConfig::get('cacheKey.appById') . $info['id'];
        $this->_cacheDao->del($cacheKey);

        $this->_dbDao->set('`app`', $updateData, '`app_id` = :id', array(':id' => $appId));

		$cacheKey = BooConfig::get('cacheKey.dbDataAppList');
        $this->_cacheDao->del($cacheKey);

        return true;
    }

    public function updateByAppIds($appIds, $updateData) {

        if(!$appIds || !$updateData){
            return false;
        }

        foreach ($appIds as $appId) {
            $info = $this->getInfoByAppId($appId);
            if (!$info) {
                return false;
            }

            $cacheKey = BooConfig::get('cacheKey.appByAppId') . $info['app_id'];
            $this->_cacheDao->del($cacheKey);

            $cacheKey = BooConfig::get('cacheKey.appById') . $info['id'];
            $this->_cacheDao->del($cacheKey);

            $this->_dbDao->set('`app`', $updateData, '`id` = :id', array(':id' => $info['id']));
        }

        $cacheKey = BooConfig::get('cacheKey.dbDataAppList');
        $this->_cacheDao->del($cacheKey);

        return true;
    }

	public function insert($insertData) {

		if(!$insertData){
			return false;
		}

        $cacheKey = BooConfig::get('cacheKey.dbDataAppList');
        $this->_cacheDao->del($cacheKey);

		$rs = $this->_dbDao->set('`app`', $insertData);
		return $rs;
	}

    public function delete($id) {

        if(!$id){
            return false;
        }

        $info = $this->getInfoById($id);
        if (!$info) {
            return false;
        }

        $cacheKey = BooConfig::get('cacheKey.appByAppId') . $info['app_id'];
        $this->_cacheDao->del($cacheKey);

        $cacheKey = BooConfig::get('cacheKey.appById') . $id;
        $this->_cacheDao->del($cacheKey);

        $cacheKey = BooConfig::get('cacheKey.dbDataAppList');
        $this->_cacheDao->del($cacheKey);

        $rs = $this->_dbDao->del('`app`', '`id` = :id', array(':id' => $id));
        return $rs;
    }
}