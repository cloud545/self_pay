<?php
class Obj_App_Orders extends Obj_App {

    public function __construct() {
        $this->_dbDao = Dao_Mysql_Default::getInstance();
        $this->_cacheDao = Dao_Redis_Default::getInstance();
    }

    public function getInfoById($id) {

        if(!$id){
            return false;
        }

        $sql = "SELECT * FROM `app_order` WHERE `id` = :id";
        $info = $this->_dbDao->get($sql, array(':id' => $id));
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getInfoByDate($appId, $type = 0, $date = 0) {

        if(!$appId){
            return false;
        }

        $sql = "SELECT * FROM `app_order` WHERE `app_id` = {$appId} AND `type` = {$type}  AND `create_date` = {$date} order by `create_date` desc";
        $info = $this->_dbDao->get($sql);
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getLastInfo($appId, $type = 0) {

        if(!$appId){
            return false;
        }

        if ($type) {
            $sql = "SELECT * FROM `app_order` WHERE `app_id` = {$appId} AND `type` = {$type} ORDER BY `create_time` DESC LIMIT 1";
        } else {
            $sql = "SELECT * FROM `app_order` WHERE `app_id` = {$appId} ORDER BY `create_time` DESC LIMIT 1";
        }

        $info = $this->_dbDao->get($sql);
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getLimitList($appId, $type = 0, $limitNum = 1) {

        if(!$appId){
            return false;
        }

        if ($type) {
            $sql = "SELECT * FROM `app_order` WHERE `app_id` = {$appId} AND `type` = {$type} ORDER BY `create_time` DESC LIMIT {$limitNum}";
        } else {
            $sql = "SELECT * FROM `app_order` WHERE `app_id` = {$appId} ORDER BY `create_time` DESC LIMIT {$limitNum}";
        }

        $list = $this->_dbDao->mget($sql);
        if (!$list) {
            return false;
        }

        return $list;
    }

    public function getList($appId = 0, $type = 'all', $startTime = '', $endTime = '') {

        $sql = "select * from `app_order`";

        $whereArr = array();
        if ($appId) {
            $whereArr[] = "`app_id` = {$appId}";
        }

        if ($type != 'all') {
            $whereArr[] = "`type` = {$type}";
        }

        if ($startTime && $endTime) {
            $whereArr[] = "`create_time` >= '{$startTime}' AND `create_time` < '{$endTime}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `id` DESC";
        $list = $this->_dbDao->mget($sql);

        return $list;
    }

    public function getSum($appId = 0, $type = 'all', $startTime = '', $endTime = '') {

        $sql = "select sum(`amount`) as sumAmount from `app_order`";

        $whereArr = array();
        if ($appId) {
            $whereArr[] = "`app_id` = {$appId}";
        }

        if ($type != 'all') {
            $whereArr[] = "`type` = {$type}";
        }

        if ($startTime && $endTime) {
            $whereArr[] = "`create_time` >= '{$startTime}' AND `create_time` < '{$endTime}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `id` DESC";
        $info = $this->_dbDao->get($sql);

        return $info;
    }

    public function getPageList($appId = 0, $type = 'all', $startTime = '', $endTime = '') {

        $sql = "select * from `app_order`";

        $whereArr = array();
        if ($appId) {
            $whereArr[] = "`app_id` = {$appId}";
        }

        if ($type != 'all') {
            $whereArr[] = "`type` = {$type}";
        }

        if ($startTime && $endTime) {
            $whereArr[] = "`create_time` >= '{$startTime}' AND `create_time` < '{$endTime}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `id` DESC";
        $pageList = $this->_dbDao->pget($sql);

        return $pageList;
    }

    public function update($id, $updateData) {

        if(!$id || !$updateData){
            return false;
        }

        $rs = $this->_dbDao->set('`app_order`', $updateData, '`id` = :id', array(':id' => $id));
        return $rs;
    }

    public function insert($insertData) {

        if(!$insertData){
            return false;
        }

        $rs = $this->_dbDao->set('`app_order`', $insertData);
        return $rs;
    }

    public function delete($id) {

        if(!$id){
            return false;
        }

        $rs = $this->_dbDao->del('`app_order`', '`id` = :id', array(':id' => $id));
        return $rs;
    }

}
