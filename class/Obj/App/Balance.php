<?php
class Obj_App_Balance extends Obj_App {

    public function __construct() {
        $this->_dbDao = Dao_Mysql_Default::getInstance();
        $this->_cacheDao = Dao_Redis_Default::getInstance();
    }
	
	public function getInfoById($id) {
		
		if(!$id){
			return false;
		}
		
		$sql = "SELECT * FROM `app_balance` WHERE `id` = :id";
		$info = $this->_dbDao->get($sql, array(':id' => $id));
		if (!$info) {
			return false;
		}
		
		return $info;
	}

    public function getInfoByAppId($appId) {

        if(!$appId){
            return false;
        }

        $sql = "SELECT * FROM `app_balance` WHERE `app_id` = :id";
        $info = $this->_dbDao->get($sql, array(':id' => $appId));
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getPageList($appId = 0, $status = 'all') {

        $sql = "select * from `app_balance`";

        $whereArr = array();
        if ($appId) {
            $whereArr[] = "`app_id` = {$appId}";
        }

        if ($status != 'all') {
            $whereArr[] = "`lock_status` = {$status}";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `id` DESC";
        $pageList = $this->_dbDao->pget($sql);

        return $pageList;
    }

    public function getList($appId = 0, $type = 'all') {
        $sql = "select * from `app_balance`";
        $whereArr = array();
        if ($appId) {
            $whereArr[] = "`app_id` = {$appId}";
        }

        if ($type != 'all') {
            $whereArr[] = "`lock_status` = {$type}";
        }


        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `id` DESC";

        $data = $this->_dbDao->mget($sql);

        return $data;
    }

	public function update($id, $updateData) {
		
		if(!$id || !$updateData){
			return false;
		}
		
		$rs = $this->_dbDao->set('`app_balance`', $updateData, '`id` = :id', array(':id' => $id));
		return $rs;
	}
	
	public function insert($insertData) {
		
		if(!$insertData){
			return false;
		}
		
		$rs = $this->_dbDao->set('`app_balance`', $insertData);
		return $rs;
	}

    public function delete($id) {

        if(!$id){
            return false;
        }

        $info = $this->getInfoById($id);
        if (!$info) {
            return false;
        }

        $rs = $this->_dbDao->del('`app_balance`', '`id` = :id', array(':id' => $id));
        return $rs;
    }
}