<?php
class Obj_App_Report extends Obj_App {

    public function __construct() {
        $this->_dbDao = Dao_Mysql_Default::getInstance();
        $this->_cacheDao = Dao_Redis_Default::getInstance();
    }

    /**
     * ��ȡ��������
     *
     * @param integer $app_id     �̻�ID
     * @param string  $start_time ��ʼʱ��
     * @param string  $end_time   ����ʱ��
     * @return bool
     */
    public function getList($app_id, $start_time = '', $end_time= '') {
        $sql = "select * from `app_report`";
        $whereArr = array();
        if ($app_id) {
            $whereArr[] = "`app_id` = {$app_id}";
        }

        if ($start_time && $end_time) {
            $whereArr[] = "`report_date` >= '{$start_time}' AND `report_date` < '{$end_time}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `report_date` DESC , `id` DESC";
        $data = $this->_dbDao->mget($sql);

        return $data;
    }

    public function getInfoById($id) {

        if(!$id){
            return false;
        }

        $sql = "SELECT * FROM `app_report` WHERE `id` = :id";
        $info = $this->_dbDao->get($sql, array(':id' => $id));
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getInfo($appId, $date = '') {

        $sql = "select * from `app_report`";

        if(!$appId){
            return false;
        }

        $whereArr = array();
        if ($appId) {
            $whereArr[] = "`app_id` = {$appId}";
        }

        if ($date) {
            $whereArr[] = "`report_date` = '{$date}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $info = $this->_dbDao->get($sql);
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getPageList($appId = 0, $startTime = '', $endTime = '') {

        $sql = "select * from `app_report`";

        $whereArr = array();
        if ($appId) {
            $whereArr[] = "`app_id` = {$appId}";
        }

        if ($startTime && $endTime) {
            $whereArr[] = "`report_date` >= '{$startTime}' AND `report_date` < '{$endTime}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `report_date` DESC, `id` DESC";
        $pageList = $this->_dbDao->pget($sql);

        return $pageList;
    }

    public function getSum($appId = 0, $startTime = '', $endTime = '') {

        $sql = "select sum(`all_order_number`) as allOrderNum, sum(`order_number`) as orderNum, sum(`real_amount`) as sumRealAmount, sum(`fee`) as sumFee, sum(`withdrawal_amount`) as sumWithdrawal, sum(`withdrawal_fee`) as sumWithdrawalFee, sum(`balance`) as sumBalance from `app_report`";

        $whereArr = array();
        if ($appId) {
            $whereArr[] = "`app_id` = {$appId}";
        }

        if ($startTime && $endTime) {
            $whereArr[] = "`report_date` >= '{$startTime}' AND `report_date` < '{$endTime}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `report_date` DESC, `id` DESC";
        $info = $this->_dbDao->get($sql);

        return $info;
    }

    public function update($id, $updateData) {

        if(!$id || !$updateData){
            return false;
        }

        $rs = $this->_dbDao->set('`app_report`', $updateData, '`id` = :id', array(':id' => $id));
        return $rs;
    }

    public function insert($insertData) {

        if(!$insertData){
            return false;
        }

        $rs = $this->_dbDao->set('`app_report`', $insertData);
        return $rs;
    }

    public function delete($id) {

        if(!$id){
            return false;
        }

        $rs = $this->_dbDao->del('`app_report`', '`id` = :id', array(':id' => $id));
        return $rs;
    }

}
