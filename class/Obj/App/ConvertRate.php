<?php

class Obj_App_ConvertRate extends Obj_App
{
    
    public function __construct()
    {
        $this->_dbDao    = Dao_Mysql_Default::getInstance();
        $this->_cacheDao = Dao_Redis_Default::getInstance();
    }
    
    public function getInfo($create_hour, $app_id, $pt_id, $p_id)
    {
        $sql  = "select * from `convert_rate` where create_hour={$create_hour} and app_id={$app_id} and pt_id={$pt_id} and p_id={$p_id}";
        $info = $this->_dbDao->get($sql);
        if (!$info) {
            return false;
        }
        
        return $info;
    }
    
    public function update($id, $updateData)
    {
        
        if (!$id || !$updateData) {
            return false;
        }
        
        $rs = $this->_dbDao->set('`convert_rate`', $updateData, '`id` = :id', array(':id' => $id));
        
        return $rs;
    }
    
    public function insert($insertData)
    {
        
        if (!$insertData) {
            return false;
        }
        
        $rs = $this->_dbDao->set('`convert_rate`', $insertData);
        
        return $rs;
    }
    
    public function getConvertRateList(
        $appId = 0,
        $type = 0,
        $platId = 0,
        $startDate = '',
        $endDate = '',
        $min_amount = 0,
        $max_amount = 0,
        $limit_start = 0,
        $limit_nums = 10
    )
    {
        
        $tableName = 'convert_rate';
        $whereArr  = [];
        $field     = "create_date";
        $group_by  = 'create_date';
        
        if ($appId) {
            $whereArr[] = "`app_id` = {$appId}";
            $group_by   .= ',app_id';
            $field      .= ',app_id';
        }
        
        if ($type) {
            $whereArr[] = "`pt_id` = {$type}";
            $group_by   .= ',pt_id';
            $field      .= ',pt_id';
        }
        
        if ($platId) {
            $whereArr[] = "`p_id` = {$platId}";
            $group_by   .= ',p_id';
            $field      .= ',p_id';
        }
        
        if ($startDate) {
            $startDate  = date('YmdH', strtotime($startDate));
            $whereArr[] = "`create_hour` >= '{$startDate}'";
        }
        
        if ($endDate) {
            $endDate    = date('YmdH', strtotime($endDate));
            $whereArr[] = "`create_hour` < '{$endDate}'";
        }
        
        if ($min_amount) {
            $whereArr[] = " `amount` >= {$min_amount}";
        }
        if ($max_amount) {
            $whereArr[] = " `amount` < {$max_amount}";
        }
        
        $where = '';
        if ($whereArr) {
            $where .= implode(' AND ', $whereArr);
        }
        if (empty($where)) {
            $where = '1';
        }
        
        /**
         * 分页条数
         */
        $sql      = "select {$field},sum(total_amount) as total_amount,sum(amount) as amount,sum(total_num) as total_num,sum(num) as num from `{$tableName}` ";
        $sql      .= " where {$where}  group by {$group_by} ORDER BY create_date DESC limit {$limit_start},{$limit_nums}";
        $pageList = $this->_dbDao->mget($sql);
        
        /**
         * 统计汇总
         */
        $sql      = "select sum(total_amount) as total_amount,sum(amount) as amount,sum(total_num) as total_nums,sum(num) as nums from `{$tableName}` ";
        $sql      .= " where {$where}";
        $totalSum = $this->_dbDao->mget($sql);
        
        /**
         * 总数
         */
        $sql   = "select count(*) as nums from (select count(*) as nu  from `{$tableName}` ";
        $sql   .= " where {$where} group by {$group_by}) as aa";
        $total = $this->_dbDao->mget($sql);
        
        return [
            'totalRows' => !empty($total[0]['nums']) ? $total[0]['nums'] : 0,
            'totalSum'  => !empty($totalSum[0]) ? $totalSum[0] : [],
            'data'      => $pageList,
        ];
        
    }
}
