<?php

class Obj_App_Pay extends Obj_App
{
    
    public function __construct()
    {
        $this->_dbDao    = Dao_Mysql_AppPay::getInstance();
        $this->_cacheDao = Dao_Redis_AppDataTable::getInstance();
    }
    public function getInfoByContent() {


        $sql = "SELECT content FROM `pay_6329` WHERE `p_id` = 131 and result =1 AND create_date = '2019-09-30'";
        $info = $this->_dbDao->mget($sql);
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getInfoByAll() {


        $sql = "SELECT * FROM `pay_6329` WHERE `p_id` = 131 and result =1 AND create_date = '2019-09-30'";
        $info = $this->_dbDao->mget($sql);
        if (!$info) {
            return false;
        }

        return $info;
    }
    
    public function getInfoById($appId, $id)
    {
        
        if (!$appId || !$id) {
            return false;
        }
        
        $tableName = $this->prepareTable($appId);
        if (!$tableName) {
            return false;
        }
        
        $sql  = "SELECT * FROM `{$tableName}` WHERE `id` = {$id}";
        $info = $this->_dbDao->get($sql);
        if (!$info) {
            return false;
        }
        
        return $info;
    }
    
    public function getInfoByOrderId($appId, $orderId)
    {
        
        if (!$appId || !$orderId) {
            return false;
        }
        
        $tableName = $this->prepareTable($appId);
        if (!$tableName) {
            return false;
        }
        
        $sql  = "SELECT * FROM `{$tableName}` WHERE `order_id` = '{$orderId}'";
        $info = $this->_dbDao->get($sql);
        if (!$info) {
            return false;
        }
        
        return $info;
    }
    
    public function getList($appId, $platId = 0, $id = 0, $type = 0, $startDate = '', $endDate = '', $result = 1)
    {
        
        if (!$appId) {
            return false;
        }
        
        $tableName = $this->prepareTable($appId);
        if (!$tableName) {
            return false;
        }
        
        $sql = "select * from `{$tableName}`";
        
        $whereArr = array();
        if ($platId) {
            $whereArr[] = "`p_id` = {$platId}";
        }
        
        if ($id) {
            $whereArr[] = "`id` = {$id}";
        }
        
        if ($type) {
            $whereArr[] = "`pt_id` = {$type}";
        }
        
        if ($startDate && $endDate) {
            $whereArr[] = "`create_time` >= '{$startDate}' AND `create_time` < '{$endDate}'";
        }
        
        if ($result != 'all') {
            $whereArr[] = "`result` = {$result}";
        }
        
        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }
        
        $sql  .= " ORDER BY `create_time` DESC";
        $list = $this->_dbDao->mget($sql);
        
        return $list;
    }
    
    public function getConvertRateCrontab(
        $appId,
        $startDate = '',
        $endDate = '',
        $success = false
    )
    {
        
        if (!$appId) {
            return false;
        }
        
        $tableName = $this->prepareTable($appId);
        if (!$tableName) {
            return false;
        }
        
        $whereArr = $whereArr2 = array();
        if ($startDate) {
            $whereArr[]  = "a.`create_time` >= '{$startDate}'";
            $whereArr2[] = "`create_time` >= '{$startDate}'";
        }
        
        if ($endDate) {
            $whereArr[]  = " a.`create_time` < '{$endDate}'";
            $whereArr2[] = " `create_time` < '{$endDate}'";
        }
        
        if ($success) {
            $whereArr[]  = " a.`result`='1'";
            $whereArr2[] = " `result` = '1'";
        }
        
        $where = $where2 = '';
        if ($whereArr) {
            $where .= implode(' AND ', $whereArr);
        }
        if (empty($where)) {
            $where = '1';
        }
        
        /**
         * 分页条数
         */
        $sql = "select DATE_FORMAT(a.create_time,'%Y%m%d%H') as create_hour,a.app_id,a.pt_id,a.p_id,sum(a.amount) as total_amount,count(a.create_date) as total_num from `{$tableName}` ";
        $sql .= " as a where {$where} group by DATE_FORMAT(a.create_time,'%Y%m%d%H'),a.pt_id,a.p_id ORDER BY DATE_FORMAT(a.create_time,'%Y%m%d%H') DESC";
        
        $pageList = $this->_dbDao->mget($sql);
        
        return $pageList;
        
    }
    
    public function getPageList($appId,
                                $platId = 0,
                                $id = 0,
                                $type = 0,
                                $startDate = '',
                                $endDate = '',
                                $result = 1,
                                $orderId = '',
                                $payId = 0)
    {
        
        if (!$appId) {
            return false;
        }
        
        $tableName = $this->prepareTable($appId);
        if (!$tableName) {
            return false;
        }
        
        $sql = "select * from `{$tableName}`";
        
        $whereArr = array();
        if ($payId) {
            $whereArr[] = "`id` = {$payId}";
        }
        
        if ($platId) {
            $whereArr[] = "`p_id` = {$platId}";
        }
        
        if ($id) {
            $whereArr[] = "`id` = {$id}";
        }
        
        if ($type) {
            $whereArr[] = "`pt_id` = {$type}";
        }
        
        if ($startDate && $endDate) {
            $whereArr[] = "`create_time` >= '{$startDate}' AND `create_time` < '{$endDate}'";
        }
        
        if ($result != 'all') {
            $whereArr[] = "`result` = {$result}";
        }
        
        if ($orderId) {
            $whereArr[] = "`order_id` = '{$orderId}'";
        }
        
        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }
        
        $sql .= " ORDER BY `create_time` DESC";
        
        $pageList = $this->_dbDao->pget($sql);
        
        return $pageList;
    }
    
    public function getSum($appId, $platId = 0, $id = 0, $type = 0, $startDate = '', $endDate = '', $result = 1)
    {
        
        if (!$appId) {
            return false;
        }
        
        $tableName = $this->prepareTable($appId);
        if (!$tableName) {
            return false;
        }
        
        $sql = "select sum(`amount`) as sumAmount, sum(`real_amount`) as sumRealAmount, sum(`current_fee`) as sumFee from `{$tableName}`";
        
        $whereArr = array();
        if ($platId) {
            $whereArr[] = "`p_id` = {$platId}";
        }
        
        if ($id) {
            $whereArr[] = "`id` = {$id}";
        }
        
        if ($type) {
            $whereArr[] = "`pt_id` = {$type}";
        }
        
        if ($startDate && $endDate) {
            $whereArr[] = "`create_time` >= '{$startDate}' AND `create_time` < '{$endDate}'";
        }
        
        if ($result != 'all') {
            $whereArr[] = "`result` = {$result}";
        }
        
        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }
        
        $sql .= " ORDER BY `create_time` DESC";
        
        $info = $this->_dbDao->get($sql);
        
        if ($info['sumRealAmount'] > 0) {
            $sql = "SELECT * FROM `{$tableName}`";
            if ($whereArr) {
                $sql .= " WHERE " . implode(' AND ', $whereArr);
            }
            $sql .= " ORDER BY `create_time` DESC LIMIT 1";
            
            $lastInfo                 = $this->_dbDao->get($sql);
            $info['last_create_time'] = $lastInfo['create_time'];
        }
        
        return $info;
    }
    
    public function getCount($appId, $platId = 0, $id = 0, $type = 0, $startDate = '', $endDate = '', $result = 1)
    {
        
        if (!$appId) {
            return false;
        }
        
        $tableName = $this->prepareTable($appId);
        if (!$tableName) {
            return false;
        }
        
        $sql = "select count(`id`) as countNum from `{$tableName}`";
        
        $whereArr = array();
        if ($platId) {
            $whereArr[] = "`p_id` = {$platId}";
        }
        
        if ($id) {
            $whereArr[] = "`id` = {$id}";
        }
        
        if ($type) {
            $whereArr[] = "`pt_id` = {$type}";
        }
        
        if ($startDate && $endDate) {
            $whereArr[] = "`create_time` >= '{$startDate}' AND `create_time` < '{$endDate}'";
        }
        
        if ($result != 'all') {
            $whereArr[] = "`result` = {$result}";
        }
        
        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }
        
        $sql  .= " ORDER BY `create_time` DESC";
        $info = $this->_dbDao->get($sql);
        
        return $info['countNum'];
    }
    
    public function update($appId, $id, $updateData)
    {
        
        if (!$appId || !$id || !$updateData) {
            return false;
        }
        
        $tableName = $this->prepareTable($appId);
        if (!$tableName) {
            return false;
        }
        
        $rs = $this->_dbDao->set($tableName, $updateData, '`id` = :id', array(':id' => $id));
        
        return $rs;
    }
    
    public function insert($appId, $insertData)
    {
        
        if (!$appId || !$insertData) {
            return false;
        }
        
        $tableName = $this->prepareTable($appId);
        if (!$tableName) {
            return false;
        }
        
        $rs = $this->_dbDao->set($tableName, $insertData);
        
        return $rs;
    }
    
    /**
     * 返回分表后的表名
     *
     * @param $appId
     *
     * @return bool|string
     */
    protected function prepareTable($appId)
    {
        try {
            // 分表方法 按app分表
            $tableName = "pay_" . $appId;
            
            // 检查表是否存在
            if ($this->checkTableExists($tableName) === false) {
                return false;
            }
            
            return $tableName;
        } catch (\Exception $exception) {
            return false;
        }
        
    }
    
    /**
     * 检查是否有相应的数据库
     *
     * @param $tableName
     *
     * @return bool|int
     * @throws ErrorException
     */
    private function checkTableExists($tableName)
    {
        
        $cacheKey = BooConfig::get('cacheKey.appPayTable') . $tableName;
        
        // 已经生成过这个表了
        if ($this->_cacheDao->get($cacheKey)) {
            return true;
        }
        
        // 缓存失效后执行一次查询看是否已经生成过这个表
        if ($this->_dbDao->checkTable($tableName)) {
            $this->_cacheDao->set($cacheKey, 1);
            
            return true;
        }
        
        $rs = $this->_dbDao->query(
            "
            CREATE TABLE `{$tableName}` (
              `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增id',
              `app_id` int(6) UNSIGNED NOT NULL COMMENT '应用id',
			  `app_name` varchar(30) NOT NULL COMMENT '应用名称',
			  `proxy_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '代理id',
			  `proxy_name` varchar(30) NOT NULL COMMENT '代理名称',
			  `p_id` int(6) UNSIGNED NOT NULL COMMENT '平台id',
			  `order_id` varchar(60) NOT NULL COMMENT '充值订单id',
			  `amount` decimal(14,4) UNSIGNED NOT NULL DEFAULT '0.0000' COMMENT '充值金额',
			  `real_amount` decimal(14,4) UNSIGNED NOT NULL DEFAULT '0.0000' COMMENT '真实充值金额',
			  `pt_id` int(6) UNSIGNED NOT NULL DEFAULT '0' COMMENT '支付类型',
			  `remark` varchar(60) NOT NULL COMMENT '商户订单备注',
			  `result` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '是否充值成功，0未成功，1成功',
			  `plat_order_id` varchar(60) NOT NULL COMMENT '平台充值订单id',
			  `content` varchar(500) NOT NULL COMMENT '平台响应数据',
			  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '充值时间',
			  `create_date` date NOT NULL COMMENT '创建日期',
			  `end_time` datetime NOT NULL COMMENT '充值完成时间',
			  `client_url` varchar(250) NOT NULL COMMENT '商户提供的返回客户端地址',
			  `notify_url` varchar(250) NOT NULL COMMENT '商户提供的通知地址',
			  `notify_data` varchar(500) NOT NULL COMMENT '通知的数据',
			  `notify_status` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT ' 支付成功后通知商户时，商户是否确认收到通知 0 未确认 1 已确认',
			  `notify_times` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '支付成功后通知商户的次数',
			  `last_notify_time` datetime NOT NULL COMMENT '上一次通知的时间',
			  `notify_res_data` varchar(200) NOT NULL COMMENT '商户响应数据',
			  `referer_ip` varchar(20) NOT NULL COMMENT '来路ip',
			  `current_point` decimal(14,4) UNSIGNED NOT NULL DEFAULT '0.0000' COMMENT '当前点数',
			  `current_fee_rate` decimal(14,4) UNSIGNED NOT NULL DEFAULT '0.0000' COMMENT '本次支付的手续费率',
			  `current_fee` decimal(14,4) UNSIGNED NOT NULL DEFAULT '0.0000' COMMENT '本次支付的手续费',
              PRIMARY KEY (`id`),
              UNIQUE KEY `order_id` (`order_id`),
              KEY `app_id` (`app_id`,`p_id`),
              KEY `result` (`result`),
              KEY `plat_order_id` (`plat_order_id`),
              KEY `create_time` (`create_time`),
              KEY `end_time` (`end_time`),
              KEY `pt_id` (`pt_id`),
              KEY `notify_status` (`notify_status`),
              KEY `create_date` (`create_date`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='支付表';
		"
        );
        
        // 生成表成功缓存标记
        if ($rs) {
            $this->_cacheDao->set($cacheKey, 1);
        }
        
        return $rs;
    }
}
