<?php

class Obj_App_ChannelAssign extends Obj_App
{
    
    public function __construct()
    {
        parent::__construct();
        
        $this->_dbDao    = Dao_Mysql_Default::getInstance();
        $this->_cacheDao = Dao_Redis_Default::getInstance();
    }
    
    public function getInfoById($id)
    {
        
        if (!$id) {
            return false;
        }
        
        $sql  = "SELECT * FROM `app_channel_assign` WHERE `id` = :id";
        $info = $this->_dbDao->get($sql, array(':id' => $id));
        if (!$info) {
            return false;
        }
        
        return $info;
    }
    
    public function getInfoByAppId($appId)
    {
        
        if (!$appId) {
            return false;
        }
        
        $sql  = "SELECT * FROM `app_channel_assign` WHERE `app_id` = :id";
        $info = $this->_dbDao->get($sql, array(':id' => $appId));
        if (!$info) {
            return false;
        }
        
        return $info;
    }
    
    public function getInfoByAppAndType($app_id, $pt_id, $p_id = '')
    {
        
        if (!$pt_id || !$pt_id) {
            return false;
        }
        if ($p_id) {
            $sql  = "SELECT * FROM `app_channel_assign` WHERE `app_id` = :id and `pt_id`=:pt_id and `p_id`=:p_id";
            $info = $this->_dbDao->mget($sql, array(':id' => $app_id, ':pt_id' => $pt_id, 'p_id' => $p_id));
        }
        else {
            $sql  = "SELECT * FROM `app_channel_assign` WHERE `app_id` = :id and `pt_id`=:pt_id";
            $info = $this->_dbDao->mget($sql, array(':id' => $app_id, ':pt_id' => $pt_id));
        }
        
        if (!$info) {
            return false;
        }
        
        return $info;
    }
    
    public function getPageList(
        $appId = 0,
        $type = 0,
        $platId = 0,
        $startDate = '',
        $endDate = '',
        $limit_start = 0,
        $limit_nums = 10,
        $channel_status = ''
    )
    {
        
        $tableName = 'app_channel_assign';
        $whereArr  = [];
        $group_by  = 'updated_time';
        
        if ($appId) {
            $whereArr[] = "`app_id` = {$appId}";
            $group_by   .= ',app_id';
        }
        
        if ($type) {
            $whereArr[] = "`pt_id` = {$type}";
            $group_by   .= ',pt_id';
        }
        
        if ($platId) {
            $whereArr[] = "`p_id` = {$platId}";
            $group_by   .= ',p_id';
        }
        
        if ($channel_status) {
            $channel_status = $channel_status == 'Y' ? 'Y' : 'N';
            $whereArr[]     = "`status` = '{$channel_status}'";
            $group_by       .= ',status';
        }
        
        if ($startDate) {
            $startDate  = strtotime($startDate);
            $whereArr[] = "`created_time` >= {$startDate}";
        }
        
        if ($endDate) {
            $endDate    = strtotime($endDate);
            $whereArr[] = "`created_time` < '{$endDate}'";
        }
        
        $where = '';
        if ($whereArr) {
            $where .= implode(' AND ', $whereArr);
        }
        if (empty($where)) {
            $where = '1';
        }
        
        /**
         * 分页条数
         */
        $sql      = "select * from `{$tableName}` ";
        $sql      .= " where {$where}  order by {$group_by} DESC limit {$limit_start},{$limit_nums}";
        $pageList = $this->_dbDao->mget($sql);
        
        /**
         * 总数
         */
        $sql   = "select count(*) as nums from `{$tableName}` ";
        $sql   .= " where {$where}";
        $total = $this->_dbDao->mget($sql);
        
        return [
            'totalRows' => !empty($total[0]['nums']) ? $total[0]['nums'] : 0,
            'data'      => $pageList,
        ];
        
    }
    
    public function update($id, $updateData)
    {
        
        if (!$id || !$updateData) {
            return false;
        }
        try {
            
            /**
             * 更新基础信息
             */
            $rs = $this->_dbDao->set('`app_channel_assign`', $updateData, '`id` = :id', array(':id' => $id));
            if (!$rs) {
                return false;
            }
            
            /**
             * 获取更新后的数据
             */
            $channel = $this->getInfoById($id);
            if (empty($channel)) {
                return false;
            }
            
            /**
             * 如果开启当前渠道,则需要做如下操作
             * 1.关闭当前类型下,其他所有渠道
             * 2.更新商户plat_app_info字段,将该类型下的渠道和费率,更新为当前渠道
             */
            if ($channel['status'] == 'Y') {
                /**
                 * 1.关闭当前类型下,其他所有渠道
                 */
                $channels = $this->getInfoByAppAndType($channel['app_id'], $channel['pt_id']);
                foreach ($channels as $key => $item) {
                    if ($item['id'] == $id) {
                        continue;
                    }
                    $this->_dbDao->set(
                        '`app_channel_assign`',
                        [
                            'status'       => 'N',
                            'updated_time' => time(),
                            'operator'     => BooSession::get('adminName') ? : 'system',
                        ],
                        '`id` = :id',
                        array(':id' => $item['id'])
                    );
                }
                
                /**
                 * 更新商户plat_app_info字段,删除当前渠道分类
                 *
                 * @var $appObj Obj_App_Info
                 */
                $appObj        = BooController::get('Obj_App_Info');
                $tmpApp        = $appObj->getInfoByAppId($channel['app_id']);
                $plat_app_info = json_decode($tmpApp['plat_app_info'], true);
                foreach ($plat_app_info as $k_p_id => $v) {
                    if ($v['payType'] == $channel['pt_id']) {
                        unset($plat_app_info[$k_p_id]);
                    }
                }
                
                /**
                 * 更新商户plat_app_info字段,新增当前渠道和分类
                 *
                 * @var $platObj Obj_Plat_Info
                 */
                $platObj                         = BooController::get('Obj_Plat_Info');
                $tmpPlat                         = $platObj->getInfoById($channel['p_id']);
                $plat_app_info[$channel['p_id']] = [
                    'platTag' => $tmpPlat['p_tag'],
                    'payType' => $channel['pt_id'],
                    'point'   => $channel['fee'],
                ];
                $appObj->updateByAppId(
                    $channel['app_id'],
                    [
                        'plat_app_info' => json_encode($plat_app_info),
                    ]
                );
            }
            else {
                /**
                 * 如果关闭当前渠道,则需要从商户信息中去除掉当前渠道
                 *
                 * @var $appObj Obj_App_Info
                 */
                $appObj        = BooController::get('Obj_App_Info');
                $tmpApp        = $appObj->getInfoByAppId($channel['app_id']);
                $plat_app_info = json_decode($tmpApp['plat_app_info'], true);
                foreach ($plat_app_info as $k_p_id => $v) {
                    if ($v['payType'] == $channel['pt_id'] && $k_p_id == $channel['p_id']) {
                        unset($plat_app_info[$k_p_id]);
                    }
                }
                $appObj->updateByAppId(
                    $channel['app_id'],
                    [
                        'plat_app_info' => json_encode($plat_app_info),
                    ]
                );
            }
            
            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }
    
    public function updatePlatAppInfo(&$updateData, $app_id, $reset_point = true)
    {
        
        if (!is_array($updateData)) {
            $updateData = !empty($updateData) ? json_decode($updateData, true) : [];
        }
        
        try {
            /**
             * 关闭所有
             */
            $operator = BooSession::get('adminName') ? : 'system';
            $this->_dbDao->set(
                '`app_channel_assign`',
                [
                    'status'       => 'N',
                    'updated_time' => time(),
                    'operator'     => $operator,
                ],
                '`app_id` = :app_id',
                array(':app_id' => $app_id)
            );
            
            if (!empty($updateData)) {
                foreach ($updateData as $p_id => $item) {
                    $channel = $this->getInfoByAppAndType($app_id, $item['payType'], $p_id);
                    if (!empty($channel[0])) {
                        $channel = $channel[0];
                        if ($reset_point) {
                            $point = $item['point'];
                        }
                        else {
                            $point         = $channel['fee'];
                            $item['point'] = $point;
                        }
                        $this->_dbDao->set(
                            '`app_channel_assign`',
                            [
                                'fee'          => $point,
                                'status'       => 'Y',
                                'updated_time' => time(),
                                'operator'     => $operator,
                            ],
                            '`id` = :id',
                            array(':id' => $channel['id'])
                        );
                        
                    }
                    else {
                        $this->insert(
                            [
                                'app_id'       => $app_id,
                                'pt_id'        => $item['payType'],
                                'p_id'         => $p_id,
                                'fee'          => $item['point'],
                                'status'       => 'Y',
                                'created_time' => time(),
                                'operator'     => $operator,
                            ]
                        );
                    }
                    $updateData[$p_id] = $item;
                }
            }
        } catch (\Exception $exception) {
            var_dump($exception->getTrace());
            die;
        }
    }
    
    public function insert($insertData)
    {
        
        if (!$insertData) {
            return false;
        }
        
        try {
            return $this->_dbDao->set('`app_channel_assign`', $insertData);
        } catch (\Exception $exception) {
            return false;
        }
        
    }
    
    public function delete($id)
    {
        
        if (!$id) {
            return false;
        }
        
        $info = $this->getInfoById($id);
        if (!$info) {
            return false;
        }
        try {
            return $this->_dbDao->del('`app_channel_assign`', '`id` = :id', array(':id' => $id));
        } catch (\Exception $exception) {
            return false;
        }
    }
}
