<?php
class Obj_App_Withdrawal extends Obj_App {

    public function __construct() {
        $this->_dbDao = Dao_Mysql_Default::getInstance();
        $this->_cacheDao = Dao_Redis_Default::getInstance();
    }

    public function getInfoById($id) {

        if(!$id){
            return false;
        }

        $sql = "SELECT * FROM `app_withdrawal` WHERE `id` = :id";
        $info = $this->_dbDao->get($sql, array(':id' => $id));
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getInfoByOrderId($appId, $orderId) {

        if (!$appId || !$orderId) {
            return false;
        }

        $sql = "SELECT * FROM `app_withdrawal` WHERE `app_id` = {$appId} AND `order_id` = '{$orderId}'";
        $info = $this->_dbDao->get($sql);
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getCount($appId = 0, $status = 'all', $startTime = '', $endTime = '') {

        $sql = "select count(`id`) as countNum from `app_withdrawal`";

        $whereArr = array();
        if ($appId) {
            $whereArr[] = "`app_id` = {$appId}";
        }

        if ($status !== 'all') {
            if (is_array($status)) {
                $status = implode(',', $status);
                $whereArr[] = "`status` IN({$status})";
            } else {
                $whereArr[] = "`status` = {$status}";
            }
        }

        if ($startTime && $endTime) {
            $whereArr[] = "`create_time` >= '{$startTime}' AND `create_time` < '{$endTime}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `create_time` DESC";

        $info = $this->_dbDao->get($sql);

        return $info['countNum'];
    }

    public function getSum($appId = 0, $status = 'all', $startTime = '', $endTime = '', $id = 0) {

        $sql = "select sum(`amount`) as sumAmount, sum(`fee`) as sumFee from `app_withdrawal`";

        $whereArr = array();
        if ($id) {
            $whereArr[] = "`id` = {$id}";
        }
        if ($appId) {
            $whereArr[] = "`app_id` = {$appId}";
        }

        if ($status != 'all') {
            $whereArr[] = "`status` = {$status}";
        }

        if ($startTime && $endTime) {
            $whereArr[] = "`create_time` >= '{$startTime}' AND `create_time` < '{$endTime}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `create_time` DESC";

        $info = $this->_dbDao->get($sql);
        return $info;
    }

    public function getList($appId = 0, $status = 'all', $startTime = '', $endTime = '', $id = 0) {

        $sql = "select * from `app_withdrawal`";

        $whereArr = array();
        if ($id) {
            $whereArr[] = "`id` = {$id}";
        }
        if ($appId) {
            $whereArr[] = "`app_id` = {$appId}";
        }

        if ($status != 'all') {
            $whereArr[] = "`status` = {$status}";
        }

        if ($startTime && $endTime) {
            $whereArr[] = "`create_time` >= '{$startTime}' AND `create_time` < '{$endTime}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `create_time` DESC";
        $list = $this->_dbDao->mget($sql);

        return $list;
    }

    public function getPageList($appId = 0, $status = 'all', $startTime = '', $endTime = '', $id = 0) {

        $sql = "select * from `app_withdrawal`";

        $whereArr = array();
        if ($id) {
            $whereArr[] = "`id` = {$id}";
        }

        if ($appId) {
            $whereArr[] = "`app_id` = {$appId}";
        }

        if ($status != 'all') {
            $whereArr[] = "`status` = {$status}";
        }

        if ($startTime && $endTime) {
            $whereArr[] = "`create_time` >= '{$startTime}' AND `create_time` < '{$endTime}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `create_time` DESC";
        $pageList = $this->_dbDao->pget($sql);

        return $pageList;
    }

    public function update($id, $updateData) {

        if(!$id || !$updateData){
            return false;
        }

        $rs = $this->_dbDao->set('`app_withdrawal`', $updateData, '`id` = :id', array(':id' => $id));
        return $rs;
    }

    public function insert($insertData) {

        if(!$insertData){
            return false;
        }

        $rs = $this->_dbDao->set('`app_withdrawal`', $insertData);
        return $rs;
    }

    public function delete($id) {

        if(!$id){
            return false;
        }

        $rs = $this->_dbDao->del('`app_withdrawal`', '`id` = :id', array(':id' => $id));
        return $rs;
    }

}
