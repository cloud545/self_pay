<?php
class Obj_App_ReportMonth extends Obj_App {

    public function __construct() {
        $this->_dbDao = Dao_Mysql_Default::getInstance();
        $this->_cacheDao = Dao_Redis_Default::getInstance();
    }

    public function getInfoById($id) {

        if(!$id){
            return false;
        }

        $sql = "SELECT * FROM `app_report_month` WHERE `id` = :id";
        $info = $this->_dbDao->get($sql, array(':id' => $id));
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getInfo($appId, $date = '') {

        $sql = "select * from `app_report_month`";

        if(!$appId){
            return false;
        }

        $whereArr = array();
        if ($appId) {
            $whereArr[] = "`app_id` = {$appId}";
        }

        if ($date) {
            $whereArr[] = "`report_month` = '{$date}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $info = $this->_dbDao->get($sql);
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getPageList($appId = 0, $startTime = '') {

        $sql = "select * from `app_report_month`";
        $whereArr = array();
        if ($appId) {
            $whereArr[] = "`app_id` = {$appId}";
        }

        if ($startTime) {
            if ($startTime == '1970-01') {
                $whereArr[] = "`report_month` > '{$startTime}'";
            } else {
                $last = date('Y-m-d H:i:s', strtotime("$startTime +1 month +1 hour"));
                $whereArr[] = "`report_month` = '{$startTime}' AND `create_time` < '{$last}'";
            }
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `report_month` DESC, `id` DESC";
        $pageList = $this->_dbDao->pget($sql);

        return $pageList;
    }

    public function getSum($appId = 0, $startTime = '') {

        $sql = "select sum(`all_order_number`) as allOrderNum, sum(`order_number`) as orderNum, sum(`real_amount`) as sumRealAmount, sum(`fee`) as sumFee, sum(`withdrawal_amount`) as sumWithdrawal, sum(`withdrawal_fee`) as sumWithdrawalFee from `app_report_month`";

        $whereArr = array();
        if ($appId) {
            $whereArr[] = "`app_id` = {$appId}";
        }

        if ($startTime) {
            if ($startTime == '1970-01') {
                $whereArr[] = "`report_month` > '{$startTime}'";
            } else {
                $last = date('Y-m-d H:i:s', strtotime("$startTime +1 month +1 hour"));
                $whereArr[] = "`report_month` = '{$startTime}' AND `create_time` < '{$last}'";
            }
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `report_month` DESC, `id` DESC";
        $info = $this->_dbDao->get($sql);

        return $info;
    }

    public function update($id, $updateData) {

        if(!$id || !$updateData){
            return false;
        }

        $rs = $this->_dbDao->set('`app_report_month`', $updateData, '`id` = :id', array(':id' => $id));
        return $rs;
    }

    public function insert($insertData) {

        if(!$insertData){
            return false;
        }

        $rs = $this->_dbDao->set('`app_report_month`', $insertData);
        return $rs;
    }

    public function delete($id) {

        if(!$id){
            return false;
        }

        $rs = $this->_dbDao->del('`app_report_month`', '`id` = :id', array(':id' => $id));
        return $rs;
    }

}
