<?php
class Obj_QrPay_CodeInfo extends Obj_QrPay {

    public function __construct() {
        $this->_dbDao = Dao_Mysql_Default::getInstance();
        $this->_cacheDao = Dao_Redis_Default::getInstance();
    }

    public function getInfoById($id) {

        if(!$id){
            return false;
        }

        $sql = "SELECT * FROM `qr_code` WHERE `id` = :id";
        $info = $this->_dbDao->get($sql, array(':id' => $id));
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getInfo($payType, $userId, $account) {

        if(!$payType || !$userId || !$account){
            return false;
        }

        $sql = "SELECT * FROM `qr_code` WHERE `pt_id` = {$payType} AND `pay_account` = '{$account}' AND `qrcode_user_id` = {$userId}";
        $info = $this->_dbDao->get($sql);
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getList($payType, $userId) {

        $sql = "SELECT * FROM `qr_code`";

        $whereArr = array();
        if ($payType) {
            $whereArr[] = "`pt_id` = {$payType}";
        }

        if ($userId) {
            $whereArr[] = "`qrcode_user_id` = {$userId}";
        }

        $whereArr[] = "`is_open` = 1";
        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `create_time` DESC";

        $list = $this->_dbDao->mget($sql);
        if (!$list) {
            return array();
        }

        return $list;
    }

    public function getListByQrcodeUserIds($payType, $userIds) {

        $sql = "SELECT * FROM `qr_code`";

        $whereArr = array();
        if ($payType) {
            $whereArr[] = "`pt_id` = {$payType}";
        }

        if ($userIds) {
            $userIds = implode(',', $userIds);
            $whereArr[] = "`qrcode_user_id` IN({$userIds})";
        }

        $whereArr[] = "`is_open` = 1";
        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `create_time` DESC";

        $list = $this->_dbDao->mget($sql);
        if (!$list) {
            return array();
        }

        return $list;
    }

    public function getPageList($payType = 'all', $userId = 'all', $isOpen = 'all', $account = 'all') {

        $sql = "select * from `qr_code`";

        $whereArr = array();
        if ($payType != 'all') {
            $whereArr[] = "`pt_id` = {$payType}";
        }

        if ($account != 'all') {
            $whereArr[] = "`pay_account` = '{$account}'";
        }

        if ($userId != 'all') {
            $whereArr[] = "`qrcode_user_id` = {$userId}";
        }

        if ($isOpen != 'all') {
            $whereArr[] = "`is_open` = {$isOpen}";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `create_time` DESC";

        $pageList = $this->_dbDao->pget($sql, array(), array('perPage' => 50));

        return $pageList;
    }

    public function update($id, $updateData) {

        if(!$id || !$updateData){
            return false;
        }

        $rs = $this->_dbDao->set('`qr_code`', $updateData, '`id` = :id', array(':id' => $id));

        return $rs;
    }

    public function insert($insertData) {

        if(!$insertData){
            return false;
        }

        $rs = $this->_dbDao->set('`qr_code`', $insertData);
        return $rs;
    }

    public function delete($id) {

        if(!$id){
            return false;
        }

        $rs = $this->_dbDao->del('`qr_code`', '`id` = :id', array(':id' => $id));
        return $rs;
    }

}