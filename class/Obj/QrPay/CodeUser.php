<?php
class Obj_QrPay_CodeUser extends Obj_QrPay {

    public function __construct() {
        $this->_dbDao = Dao_Mysql_Default::getInstance();
        $this->_cacheDao = Dao_Redis_Default::getInstance();
    }
	
	public function getInfoById($id) {

		if(!$id){
			return false;
		}

        $cacheKey = BooConfig::get('cacheKey.QrPayCodeUserById') . $id;
        $cacheInfo = $this->_cacheDao->get($cacheKey);
        if ($cacheInfo) {
            return json_decode($cacheInfo, true);
        }
		
		$sql = "SELECT * FROM `qr_code_user` WHERE `id` = :id";
		$info = $this->_dbDao->get($sql, array(':id' => $id));
		if (!$info) {
			return false;
		}

        $this->_cacheDao->set($cacheKey, json_encode($info), 86400);
		
		return $info;
	}

    public function getInfoByName($name) {

        if(!$name){
            return false;
        }

        $sql = "SELECT * FROM `qr_code_user` WHERE `name` = :userName";
        $info = $this->_dbDao->get($sql, array(':userName' => $name));
        if (!$info) {
            return false;
        }

        return $info;
    }

	public function getList($fields = '') {

        $cacheKey = BooConfig::get('cacheKey.qrPayCodeUserList');
        $cacheInfo = $this->_cacheDao->get($cacheKey);
        if ($cacheInfo) {
            return json_decode($cacheInfo, true);
        }

        if ($fields) {
            $sql = "SELECT {$fields} FROM `qr_code_user` ORDER BY `id` DESC ";
        } else {
            $sql = "SELECT * FROM `qr_code_user` ORDER BY `id` DESC ";
        }
		$list = $this->_dbDao->mget($sql);
		if (!$list) {
			return array();
		}

		$userList = array();
		foreach ($list as $info) {
            $userList[$info['id']] = $info;
        }

        $this->_cacheDao->set($cacheKey, json_encode($userList), 86400);

		return $userList;
	}

    public function getPageList($name = '') {

        $sql = "select * from `qr_code_user`";

        $whereArr = array();
        if ($name) {
            $whereArr[] = "`name` = '{$name}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `create_time` DESC";
        $pageList = $this->_dbDao->pget($sql);

        return $pageList;
    }
	
	public function update($id, $updateData) {
		
		if(!$id || !$updateData){
			return false;
		}

		$info = $this->getInfoById($id);
		if (!$info) {
            return false;
        }

        $cacheKey = BooConfig::get('cacheKey.QrPayCodeUserById') . $id;
        $this->_cacheDao->del($cacheKey);

        $cacheKey = BooConfig::get('cacheKey.qrPayCodeUserList');
        $this->_cacheDao->del($cacheKey);
		
		$rs = $this->_dbDao->set('`qr_code_user`', $updateData, '`id` = :id', array(':id' => $id));
		return $rs;
	}

	public function insert($insertData) {

		if(!$insertData){
			return false;
		}

        $cacheKey = BooConfig::get('cacheKey.qrPayCodeUserList');
        $this->_cacheDao->del($cacheKey);

		$rs = $this->_dbDao->set('`qr_code_user`', $insertData);
		return $rs;
	}

    public function delete($id) {

        if(!$id){
            return false;
        }

        $info = $this->getInfoById($id);
        if (!$info) {
            return false;
        }

        $cacheKey = BooConfig::get('cacheKey.QrPayCodeUserById') . $id;
        $this->_cacheDao->del($cacheKey);

        $cacheKey = BooConfig::get('cacheKey.qrPayCodeUserList');
        $this->_cacheDao->del($cacheKey);

        $rs = $this->_dbDao->del('`qr_code_user`', '`id` = :id', array(':id' => $id));
        return $rs;
    }
}