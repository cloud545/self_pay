<?php
class Obj_QrPay_Pay extends Obj_QrPay {

    public function __construct() {
        $this->_dbDao = Dao_Mysql_QrPay::getInstance();
        $this->_cacheDao = Dao_Redis_AppDataTable::getInstance();
    }

    public function getInfoById($date, $id) {

        $tableName = $this->prepareTable($date);
        if (!$id || !$tableName) {
            return false;
        }

        $sql = "SELECT * FROM `{$tableName}` WHERE `id` = :id";
        $orderInfo = $this->_dbDao->get($sql, array(':id' => (int)$id));
        if (!$orderInfo) {
            return false;
        }

        return $orderInfo;
    }

    public function getInfoByDateAndPayTypeAndPayNo($date, $payType, $payNo) {

        $tableName = $this->prepareTable($date);
        if (!$payType || !$tableName || !$payNo) {
            return false;
        }

        $sql = "SELECT * FROM `{$tableName}` WHERE `pt_id` = '{$payType}' AND `create_date` = '{$date}' AND `pay_no` = '{$payNo}' ORDER BY `create_time` DESC LIMIT 1";
        $orderInfo = $this->_dbDao->get($sql);
        if (!$orderInfo) {
            return false;
        }

        return $orderInfo;
    }

    public function getFailOrderList($startDate, $endDate) {

        $sm = date('m', strtotime($startDate));
        $em = date('m', strtotime($endDate));

        $whereSql = " `create_time` >= '{$startDate}' and `create_time` <= '{$endDate}' AND `result` = 0";

        if ($sm != $em) {
            $gameOrderList = array();
            $tableName = $this->prepareTable($startDate);
            if (!$tableName) {
                return false;
            }
            $sql = "select * from `{$tableName}` where {$whereSql}";
            $startGameOrderList = $this->_dbDao->mget($sql);

            $tableName = $this->prepareTable($endDate);
            if (!$tableName) {
                return false;
            }
            $sql = "select * from `{$tableName}` where {$whereSql}";
            $endGameOrderList = $this->_dbDao->mget($sql);

            if ($startGameOrderList) {
                $gameOrderList = array_merge($gameOrderList, $startGameOrderList);
            }

            if ($endGameOrderList) {
                $gameOrderList = array_merge($gameOrderList, $endGameOrderList);
            }
        } else {
            $tableName = $this->prepareTable($endDate);
            if (!$tableName) {
                return false;
            }
            $sql = "select * from `{$tableName}` where {$whereSql}";
            $gameOrderList = $this->_dbDao->mget($sql);
        }

        return $gameOrderList;
    }

    public function getPageList($startDate, $endDate, $result = '', $payType = 0, $qrcodeUserId = 'all', $appOrderId = '', $appId = 0, $payNo = '') {

        $sm = date('m', strtotime($startDate));
        $em = date('m', strtotime($endDate));
        if ($sm != $em) {
            return false;
        }

        $whereSql = " `create_time` >= '{$startDate}' AND `create_time` <= '{$endDate}'";
        if ($result !== '') {
            $whereSql .= " AND `result` = {$result}";
        }
        if ($appId) {
            $whereSql .= " AND `app_id` = {$appId}";
        }
        if ($appOrderId) {
            $whereSql .= " AND `app_order_id` = '{$appOrderId}'";
        }
        if ($payNo) {
            $whereSql .= " AND `pay_no` = '{$payNo}'";
        }
        if ($payType) {
            $whereSql .= " AND `pt_id` = {$payType}";
        }
        if ($qrcodeUserId != 'all') {
            $whereSql .= " AND `qrcode_user_id` = {$qrcodeUserId}";
        }
        $whereSql .= " ORDER BY `id` DESC";

        $tableName = $this->prepareTable($endDate);
        if (!$tableName) {
            return false;
        }
        $sql = "select * from `{$tableName}` where {$whereSql}";
        $orderList = $this->_dbDao->pget($sql);

        return $orderList;
    }

    public function getHavePayAndNoOrderList($startDate, $endDate, $payType = 0, $qrcodeUserId = 'all', $realAmount = 0) {

        if ($realAmount <= 0) {
            return false;
        }

        $sm = date('m', strtotime($startDate));
        $em = date('m', strtotime($endDate));
        if ($sm != $em) {
            return false;
        }

        $whereSql = " `create_time` >= '{$startDate}' AND `create_time` <= '{$endDate}'";
        $whereSql .= " AND `result` = 1";
        $whereSql .= " AND `real_amount` = {$realAmount}";
        $whereSql .= " AND `app_id` = 0";

        if ($qrcodeUserId != 'all') {
            $whereSql .= " AND `qrcode_user_id` = {$qrcodeUserId}";
        }
        if ($payType) {
            $whereSql .= " AND `pt_id` = {$payType}";
        }
        $whereSql .= " ORDER BY `create_time` DESC";

        $tableName = $this->prepareTable($endDate);
        if (!$tableName) {
            return false;
        }

        $sql = "select * from `{$tableName}` where {$whereSql}";
        $orderList = $this->_dbDao->mget($sql);
        return $orderList;
    }

    public function getSum($startDate, $endDate, $payType = 'all', $qrcodeUserId = 'all') {

        $sm = date('m', strtotime($startDate));
        $em = date('m', strtotime($endDate));
        if ($sm != $em) {
            return false;
        }

        $whereSql = " `create_time` >= '{$startDate}' AND `create_time` <= '{$endDate}' AND `result` = 1";
        if ($payType) {
            $whereSql .= " AND `pt_id` = {$payType}";
        }
        if ($qrcodeUserId != 'all') {
            $whereSql .= " AND `qrcode_user_id` = {$qrcodeUserId}";
        }

        if ($sm != $em) {
            $tableName = $this->prepareTable($startDate);
            if (!$tableName) {
                return false;
            }
            $sql = "select SUM(`real_amount`) as sum from `{$tableName}` where {$whereSql}";
            $oneSum = $this->_dbDao->get($sql);

            $tableName = $this->prepareTable($endDate);
            if (!$tableName) {
                return false;
            }
            $sql = "select SUM(`real_amount`) as sum from `{$tableName}` where {$whereSql}";
            $twoSum = $this->_dbDao->get($sql);

            $sum = $oneSum['sum'] + $twoSum['sum'];
        } else {
            $tableName = $this->prepareTable($endDate);
            if (!$tableName) {
                return false;
            }
            $sql = "select SUM(`real_amount`) as sum from `{$tableName}` where {$whereSql}";
            $sumData = $this->_dbDao->get($sql);
            $sum = $sumData['sum'];
        }

        return $sum;
    }

    public function update($date, $id, $updateData) {

        if(!$id || !$date || !$updateData){
            return false;
        }

        $tableName = $this->prepareTable($date);
        if (!$tableName) {
            return false;
        }

        $rs = $this->_dbDao->set($tableName, $updateData, '`id` = :id', array(':id' => $id));
        return $rs;
    }

    public function del($date, $id) {

        if(!$id || !$date){
            return false;
        }

        $tableName = $this->prepareTable($date);
        if (!$tableName) {
            return false;
        }

        $rs = $this->_dbDao->del($tableName, '`id` = :id', array(':id' => $id));
        return $rs;
    }

    public function insert($insertData, $date = '') {

        if(!$insertData){
            return false;
        }

        if (!$date) {
            $date = date('Ym');
        }

        $tableName = $this->prepareTable($date);
        if (!$tableName) {
            return false;
        }

        $rs = $this->_dbDao->set($tableName, $insertData);
        return $rs;
    }

    /**
     * 返回分表后的表名
     *
     * @param $dateTime
     * @return bool|string
     * @throws ErrorException
     */
    protected function prepareTable($dateTime) {

        if ($dateTime) {
            $psDate = date('Ym', strtotime($dateTime));
        } else {
            $psDate = date('Ym');
        }

        // 分表方法 按每个月划分
        $tableName = "qrpay_" . $psDate;

        // 准备数据库
        if ($this->checkTableExists($tableName) === false) {
            return false;
        }

        return $tableName;
    }

    /**
     * 检查是否有相应的数据库
     *
     * @param $tableName
     * @return bool|int
     * @throws ErrorException
     */
    private function checkTableExists($tableName) {

        $cacheKey = BooConfig::get('cacheKey.qrPayTable') . $tableName;

        // 已经生成过这个表了
        if ($this->_cacheDao->get($cacheKey)) {
            return true;
        }

        // 缓存失效后执行一次查询看是否已经生成过这个表
        if ($this->_dbDao->checkTable($tableName)) {
            $this->_cacheDao->set($cacheKey, 1);
            return true;
        }

        // 自动生成表
        $rs = $this->_dbDao->query("
            CREATE TABLE `{$tableName}` (
              `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增id',
              `pt_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '支付类型',
              `qrcode_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '收款码id',
              `qrcode_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '收款码所属用户id',
              `qrcode_user_name` varchar(60) NOT NULL COMMENT '所属码商',
              `pay_account` varchar(60) NOT NULL COMMENT '收款码设置的收款账号',
              `account` varchar(60) NOT NULL COMMENT '收到款的账号',
              `amount` decimal(14,4) UNSIGNED NOT NULL DEFAULT '0.0000' COMMENT '充值金额',
              `real_amount` decimal(14,4) UNSIGNED NOT NULL DEFAULT '0.0000' COMMENT '真实到账金额',
              `app_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '商户id',
              `app_order_id` varchar(60) NOT NULL COMMENT '商户订单id',
              `p_id` int(6) UNSIGNED NOT NULL DEFAULT '0' COMMENT '平台id',
              `plat_order_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '对接下游下单系统订单id',
              `pay_no` varchar(60) NOT NULL COMMENT '支付宝、微信支付等第三方订单号',
              `result` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '支付状态，0支付中，1已支付',
              `notify_status` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '通知状态，0未通知，1成功通知，2通知失败',
              `create_time` datetime NOT NULL COMMENT '充值时间',
              `create_date` date NOT NULL COMMENT '创建日期',
              `client_ip` varchar(60) NOT NULL COMMENT '下单ip',
              `page` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '打开页面序号',
              `system_type` varchar(30) NOT NULL COMMENT '系统类型',
              `content` varchar(500) NOT NULL COMMENT '客户端监听充值成功通知的内容',
              `content_ip` varchar(60) NOT NULL COMMENT '客户端监听充值成功通知的ip',
              `client_db_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '客户端监控软件数据库唯一id',
              `end_time` datetime NOT NULL COMMENT '充值到账时间',
              `status` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '前端通知数据验证状态，0 验证成功，1 确实手机编号，2 sign校验失败，3 充值金额异常',
              `notify_time` datetime NOT NULL COMMENT '通知时间',
              PRIMARY KEY (`id`),
              KEY `qrcode_id` (`qrcode_id`),
              KEY `create_time` (`create_time`),
              KEY `end_time` (`end_time`),
              KEY `result` (`result`),
              KEY `app_id` (`app_id`,`app_order_id`) USING BTREE,
              KEY `pt_id` (`pt_id`,`qrcode_user_id`,`pay_no`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='二维码支付表';
		");

        // 生成表成功缓存标记
        if ($rs) {
            $this->_cacheDao->set($cacheKey, 1);
        }

        return $rs;
    }
}
