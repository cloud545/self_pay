<?php
class Obj_Admin_User extends Obj_Admin{
	
	public function getInfoById($adminId) {
		
		if(!$adminId){
			return false;
		}
		
		$sql = "SELECT * FROM `admin_user` WHERE `admin_id` = :adminId";
		$userInfo = $this->_dbDao->get($sql, array(':adminId' => $adminId));
		if (!$userInfo) {
			return false;
		}
		
		return $userInfo;
	}

    public function getInfoByName($adminName) {

        if(!$adminName){
            return false;
        }

        $sql = "SELECT * FROM `admin_user` WHERE `admin_name` = :adminName";
        $userInfo = $this->_dbDao->get($sql, array(':adminName' => $adminName));
        if (!$userInfo) {
            return false;
        }

        return $userInfo;
    }

	public function getList($fields = '') {

        if ($fields) {
            $sql = "SELECT {$fields} FROM `admin_user` ORDER BY `islocked` ,`last_update_time` DESC ";
        } else {
            $sql = "SELECT * FROM `admin_user` ORDER BY `islocked` ,`last_update_time` DESC ";
        }
		$adminList = $this->_dbDao->mget($sql);
		if (!$adminList) {
			return array();
		}

		return $adminList;
	}

    public function getListByPowerId($powerId, $fields = '') {

	    if (!$powerId) {
            return  array();
        }

        if ($fields) {
            $sql = "SELECT {$fields} FROM `admin_user` WHERE `power_id` = :powerId ORDER BY `create_time` DESC ";
        } else {
            $sql = "SELECT * FROM `admin_user` WHERE `power_id` = :powerId ORDER BY `create_time` DESC ";
        }
        $adminList = $this->_dbDao->mget($sql, array(':powerId' => $powerId));
        if (!$adminList) {
            return array();
        }

        return $adminList;
    }
	
	public function update($adminId, $updateData) {
		
		if(!$adminId || !$updateData){
			return false;
		}
		
		$rs = $this->_dbDao->set('`admin_user`', $updateData, '`admin_id` = :adminId', array(':adminId' => $adminId));
		return $rs;
	}
	
	public function insert($insertData) {
		
		if(!$insertData){
			return false;
		}
		
		$rs = $this->_dbDao->set('`admin_user`', $insertData);
		return $rs;
	}

    public function delete($id) {

        if(!$id){
            return false;
        }

        $rs = $this->_dbDao->del('`admin_user`', '`admin_id` = :adminId', array(':adminId' => $id));
        return $rs;
    }
}