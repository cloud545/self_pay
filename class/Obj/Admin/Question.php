<?php
class Obj_Admin_Question extends Obj_Admin {

    public function getList() {

        $sql = "SELECT * FROM `question` ORDER BY `id` DESC ";
        $list = $this->_dbDao->mget($sql);
        if (!$list) {
            return array();
        }

        return $list;
    }

}