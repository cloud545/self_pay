<?php

class Obj_Admin_Info extends Obj_Admin
{
    
    public function getInfo($admin_id)
    {
        
        $sql  = "SELECT * FROM `system_info` WHERE `admin_id` = {$admin_id}";
        $info = $this->_dbDao->get($sql);
        if (!$info) {
            return false;
        }
        
        return $info;
    }
    
    public function getList()
    {
        $sql  = "SELECT * FROM `system_info` WHERE 1 ";
        $info = $this->_dbDao->mget($sql);
        if (!$info) {
            return false;
        }
        
        return $info;
    }
    
    public function update($updateData, $admin_id)
    {
        
        if (!$updateData) {
            return false;
        }
        
        $rs = $this->_dbDao->set('`system_info`', $updateData, '`admin_id` = ' . $admin_id);
        
        return $rs;
    }
    
    public function insert($insertData)
    {
        
        if (!$insertData) {
            return false;
        }
        
        $rs = $this->_dbDao->set('`system_info`', $insertData);
        
        return $rs;
    }
    
}
