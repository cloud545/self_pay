<?php
class Obj_Admin_Pay extends Obj_Admin {

    public function getInfoById($id) {

        if (!$id) {
            return false;
        }

        $sql = "SELECT * FROM `system_pay` WHERE `id` = {$id}";
        $info = $this->_dbDao->get($sql);
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getList($appId = 0, $platId = 0, $id = 0, $type = 0, $startDate = '', $endDate = '') {

        $sql = "select * from `system_pay`";

        $whereArr = array();
        if ($appId) {
            $whereArr[] = "`app_id` = {$appId}";
        }

        if ($platId) {
            if (is_array($platId)) {
                $ids = implode(',', $platId);
                $whereArr[] = "`p_id` IN({$ids})";
            } else {
                $whereArr[] = "`p_id` = {$platId}";
            }

        }

        if ($id) {
            $whereArr[] = "`id` = {$id}";
        }

        if ($type) {
            $whereArr[] = "`pt_id` = {$type}";
        }

        if ($startDate && $endDate) {
            $whereArr[] = "`create_time` >= '{$startDate}' AND `create_time` < '{$endDate}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `create_time` DESC";
        $list = $this->_dbDao->mget($sql);

        return $list;
    }

    public function getPageList($appId = 0, $platId = 0, $id = 0, $type = 0, $startDate = '', $endDate = '') {

        $sql = "select * from `system_pay`";

        $whereArr = array();
        if ($appId) {
            $whereArr[] = "`app_id` = {$appId}";
        }

        if ($platId) {
            $whereArr[] = "`p_id` = {$platId}";
        }

        if ($id) {
            $whereArr[] = "`id` = {$id}";
        }

        if ($type) {
            $whereArr[] = "`pt_id` = {$type}";
        }

        if ($startDate && $endDate) {
            $whereArr[] = "`create_time` >= '{$startDate}' AND `create_time` < '{$endDate}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `id` DESC";
        $pageList = $this->_dbDao->pget($sql);

        return $pageList;
    }

    public function getCount($appId = 0, $platId = 0, $id = 0, $type = 0, $startDate = '', $endDate = '') {

        $sql = "select count(`id`) as countNum from `system_pay`";

        $whereArr = array();
        if ($appId) {
            $whereArr[] = "`app_id` = {$appId}";
        }

        if ($platId) {
            $whereArr[] = "`p_id` = {$platId}";
        }

        if ($id) {
            $whereArr[] = "`id` = {$id}";
        }

        if ($type) {
            $whereArr[] = "`pt_id` = {$type}";
        }

        if ($endDate) {

            if ($startDate) {
                $whereArr[] = "`create_time` >= '{$startDate}' AND `create_time` < '{$endDate}'";
            } else {
                $whereArr[] = "`create_time` < '{$endDate}'";
            }
        }

        $whereArr[] = "`end_time` IS NOT NULL";
        $sql .= " WHERE" . implode(' AND ', $whereArr);

        $info = $this->_dbDao->get($sql);

        return $info['countNum'];
    }

    public function getSum($appId = 0, $platId = 0, $id = 0, $type = 0, $startDate = '', $endDate = '') {

        $sql = "select sum(`amount`) as sumAmount, sum(`real_amount`) as sumRealAmount, sum(`current_fee`) as sumFee from `system_pay`";

        $whereArr = array();
        if ($appId) {
            $whereArr[] = "`app_id` = {$appId}";
        }

        if ($platId) {
            $whereArr[] = "`p_id` = {$platId}";
        }

        if ($id) {
            $whereArr[] = "`id` = {$id}";
        }

        if ($type) {
            $whereArr[] = "`pt_id` = {$type}";
        }

        if ($endDate) {

            if ($startDate) {
                $whereArr[] = "`create_time` >= '{$startDate}' AND `create_time` < '{$endDate}'";
            } else {
                $whereArr[] = "`create_time` < '{$endDate}'";
            }
        }

        $whereArr[] = "`end_time` IS NOT NULL";
        $sql .= " WHERE" . implode(' AND ', $whereArr);

        $info = $this->_dbDao->get($sql);
        return $info;
    }
    
    public function getCountByAppPayId($appId,$payId) {

        if (!$appId || !$payId) {
            return false;
        }

        $sql = "SELECT count(`id`) as countNum FROM `system_pay` WHERE `app_id` = {$appId} and `pay_id` = {$payId}";
        $info = $this->_dbDao->get($sql);
        return $info['countNum'];
    }

    public function update($id, $updateData) {

        if(!$id || !$updateData){
            return false;
        }

        $rs = $this->_dbDao->set('`system_pay`', $updateData, '`id` = :id', array(':id' => $id));
        return $rs;
    }

    public function insert($insertData) {

        if(!$insertData){
            return false;
        }

        $rs = $this->_dbDao->set('`system_pay`', $insertData);
        return $rs;
    }

}
