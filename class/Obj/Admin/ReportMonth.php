<?php
class Obj_Admin_ReportMonth extends Obj_Admin {

    public function getInfoById($id) {

        if(!$id){
            return false;
        }

        $sql = "SELECT * FROM `system_report_month` WHERE `id` = :id";
        $info = $this->_dbDao->get($sql, array(':id' => $id));
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getInfo($date = '') {

        $sql = "select * from `system_report_month`";

        $whereArr = array();
        if ($date) {
            $whereArr[] = "`report_month` = '{$date}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $info = $this->_dbDao->get($sql);
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getPageList($startTime = '', $endTime = '') {

        $sql = "select * from `system_report_month`";

        $whereArr = array();
        if ($startTime && $endTime) {
            $whereArr[] = "`report_month` >= '{$startTime}' AND `report_month` < '{$endTime}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `report_month` DESC";
        $pageList = $this->_dbDao->pget($sql);

        return $pageList;
    }

    public function getSum($startTime = '', $endTime = '') {

        $sql = "select sum(`order_number`) as orderNum, sum(`real_amount`) as sumRealAmount, sum(`fee`) as sumFee from `system_report_month`";

        $whereArr = array();

        if ($startTime && $endTime) {
            $whereArr[] = "`report_month` >= '{$startTime}' AND `report_month` < '{$endTime}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `report_month` DESC";
        $info = $this->_dbDao->get($sql);

        return $info;
    }

    public function update($id, $updateData) {

        if(!$id || !$updateData){
            return false;
        }

        $rs = $this->_dbDao->set('`system_report_month`', $updateData, '`id` = :id', array(':id' => $id));
        return $rs;
    }

    public function insert($insertData) {

        if(!$insertData){
            return false;
        }

        $rs = $this->_dbDao->set('`system_report_month`', $insertData);
        return $rs;
    }

    public function delete($id) {

        if(!$id){
            return false;
        }

        $rs = $this->_dbDao->del('`system_report_month`', '`id` = :id', array(':id' => $id));
        return $rs;
    }

}
