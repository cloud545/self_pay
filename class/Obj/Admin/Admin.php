<?php
class Obj_Admin extends Obj{
	protected $_dbDao;
	protected $_cacheDao;

    public function __construct() {
        $this->_dbDao = Dao_Mysql_Default::getInstance();
        $this->_cacheDao = Dao_Redis_Default::getInstance();
    }
}