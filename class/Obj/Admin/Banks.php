<?php
class Obj_Admin_Banks extends Obj_Admin {

    public function getInfoById($id) {

        if(!$id){
            return false;
        }

        $sql = "SELECT * FROM `banks` WHERE `id` = :id";
        $info = $this->_dbDao->get($sql, array(':id' => $id));
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getInfo($type, $userId, $cardNo) {

        if(!$type || !$userId || !$cardNo){
            return false;
        }

        $sql = "SELECT * FROM `banks` WHERE `type` = {$type} AND `user_id` = {$userId} AND `card_no` = '{$cardNo}'";
        $info = $this->_dbDao->get($sql);
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getDefaultInfo($type, $userId) {

        if(!$type || !$userId){
            return false;
        }

        $sql = "SELECT * FROM `banks` WHERE `type` = {$type} AND `user_id` = {$userId} AND `is_default` = 1";
        $info = $this->_dbDao->get($sql);
        if (!$info) {
            return false;
        }

        return $info;
    }
    
    public function getDefaultList($type, $userId) {

        if(!$type || !$userId){
            return false;
        }

        $sql = "SELECT * FROM `banks` WHERE `type` = {$type} AND `user_id` = {$userId} AND `is_default` = 1";
        $list = $this->_dbDao->mget($sql);
        if (!$list) {
            return false;
        }
        return $list;
    }

    public function getList($type, $userId, $bankType = 'all') {

        if (!$type || !$userId) {
            return false;
        }

        $sql = "SELECT * FROM `banks`";

        $whereArr = array();
        if ($type) {
            $whereArr[] = "`type` = {$type}";
        }

        if ($userId) {
            $whereArr[] = "`user_id` = {$userId}";
        }

        if ($bankType != 'all') {
            $whereArr[] = "`bank_type` = '{$bankType}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `create_time` DESC";

        $list = $this->_dbDao->mget($sql);
        if (!$list) {
            return array();
        }

        return $list;
    }

    public function getPageList($type = 0, $userId = 0, $bankType = 'all', $cardAccountName = '', $cardNo = '', $startDate = '', $endDate = '') {

        $sql = "select * from `banks`";

        $whereArr = array();
        if ($type) {
            $whereArr[] = "`type` = {$type}";
        }

        if ($userId) {
            $whereArr[] = "`user_id` = {$userId}";
        }

        if ($bankType != 'all') {
            $whereArr[] = "`bank_type` = '{$bankType}'";
        }

        if ($cardAccountName) {
            $whereArr[] = "`card_account_name` LIKE '%{$cardAccountName}%'";
        }

        if ($cardNo) {
            $whereArr[] = "`card_no` LIKE '%{$cardNo}%'";
        }

        if ($startDate && $endDate) {
            $whereArr[] = "`create_time` >= '{$startDate}' AND `create_time` <= '{$endDate}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `create_time` DESC";
        $pageList = $this->_dbDao->pget($sql);

        return $pageList;
    }

    public function update($id, $updateData) {

        if(!$id || !$updateData){
            return false;
        }

        $rs = $this->_dbDao->set('`banks`', $updateData, '`id` = :id', array(':id' => $id));

        return $rs;
    }

    public function insert($insertData) {

        if(!$insertData){
            return false;
        }

        $rs = $this->_dbDao->set('`banks`', $insertData);
        return $rs;
    }

    public function delete($id) {

        if(!$id){
            return false;
        }

        $rs = $this->_dbDao->del('`banks`', '`id` = :id', array(':id' => $id));
        return $rs;
    }

}