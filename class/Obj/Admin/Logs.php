<?php
class Obj_Admin_Logs extends Obj_Admin{
	
	public function getInfoById($id) {
		
		if(!$id){
			return false;
		}
		
		$sql = "SELECT * FROM `admin_action_log` WHERE `id` = :id";
		$info = $this->_dbDao->get($sql, array(':id' => $id));
		if (!$info) {
			return false;
		}
		
		return $info;
	}

	public function getPageList($startTime, $endTime, $controller = '', $actioner = '', $adminId = 0, $ip = '') {

        $where = "`create_time` >= '{$startTime}' AND `create_time` <= '{$endTime}'";
        if ($controller) {
            $where .= " AND `controller` = '{$controller}'";
        }
        if ($actioner) {
            $where .= " AND `actioner` = '{$actioner}'";
        }
        if ($adminId) {
            $where .= " AND `admin_id` = {$adminId}";
        }
        if ($ip) {
            $where .= " AND `ip` = '{$ip}'";
        }

		$sql = "SELECT * FROM `admin_action_log` WHERE {$where} ORDER BY `create_time` DESC ";
		$pageList = $this->_dbDao->pget($sql);
		if (!$pageList) {
			return array();
		}

		return $pageList;
	}
	
	public function insert($insertData) {
		
		if(!$insertData){
			return false;
		}
		
		$rs = $this->_dbDao->set('`admin_action_log`', $insertData);
		return $rs;
	}

}