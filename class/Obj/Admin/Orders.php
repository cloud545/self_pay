<?php
class Obj_Admin_Orders extends Obj_Admin {

    public function getInfoById($id) {

        if(!$id){
            return false;
        }

        $sql = "SELECT * FROM `system_order` WHERE `id` = :id";
        $info = $this->_dbDao->get($sql, array(':id' => $id));
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getInfoByDate($type = 0, $date = 0) {

        $sql = "SELECT * FROM `system_order` WHERE `type` = {$type}  AND `create_date` = {$date} order by `create_date` desc";
        $info = $this->_dbDao->get($sql);
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getLastInfo($type = 0) {

        if ($type) {
            $sql = "SELECT * FROM `system_order` WHERE `type` = {$type} ORDER BY `create_time` DESC LIMIT 1";
        } else {
            $sql = "SELECT * FROM `system_order` ORDER BY `create_time` DESC LIMIT 1";
        }

        $info = $this->_dbDao->get($sql);
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getLimitList($type = 0, $limitNum = 1) {

        if ($type) {
            $sql = "SELECT * FROM `system_order` WHERE `type` = {$type} ORDER BY `create_time` DESC LIMIT {$limitNum}";
        } else {
            $sql = "SELECT * FROM `system_order` ORDER BY `create_time` DESC LIMIT {$limitNum}";
        }

        $list = $this->_dbDao->mget($sql);
        if (!$list) {
            return false;
        }

        return $list;
    }

    public function getList($type = 'all', $startTime = '', $endTime = '') {

        $sql = "select * from `system_order`";

        $whereArr = array();

        if ($type != 'all') {
            $whereArr[] = "`type` = {$type}";
        }

        if ($startTime && $endTime) {
            $whereArr[] = "`create_time` >= '{$startTime}' AND `create_time` < '{$endTime}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `id` DESC";
        $list = $this->_dbDao->mget($sql);

        return $list;
    }

    public function getSum($type = 'all', $startTime = '', $endTime = '') {

        $sql = "select sum(`amount`) as sumAmount from `system_order`";

        $whereArr = array();
        if ($type != 'all') {
            $whereArr[] = "`type` = {$type}";
        }

        if ($startTime && $endTime) {
            $whereArr[] = "`create_time` >= '{$startTime}' AND `create_time` < '{$endTime}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `id` DESC";
        $info = $this->_dbDao->get($sql);

        return $info;
    }

    public function getPageList($type = 'all', $startTime = '', $endTime = '') {

        $sql = "select * from `system_order`";

        $whereArr = array();

        if ($type != 'all') {
            $whereArr[] = "`type` = {$type}";
        }

        if ($startTime && $endTime) {
            $whereArr[] = "`create_time` >= '{$startTime}' AND `create_time` < '{$endTime}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `id` DESC";
        $pageList = $this->_dbDao->pget($sql);

        return $pageList;
    }

    public function update($id, $updateData) {

        if(!$id || !$updateData){
            return false;
        }

        $rs = $this->_dbDao->set('`system_order`', $updateData, '`id` = :id', array(':id' => $id));
        return $rs;
    }

    public function insert($insertData) {

        if(!$insertData){
            return false;
        }

        $rs = $this->_dbDao->set('`system_order`', $insertData);
        return $rs;
    }

    public function delete($id) {

        if(!$id){
            return false;
        }

        $rs = $this->_dbDao->del('`system_order`', '`id` = :id', array(':id' => $id));
        return $rs;
    }

}
