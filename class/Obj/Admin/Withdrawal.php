<?php
class Obj_Admin_Withdrawal extends Obj_Admin {

    public function getInfoById($id) {

        if(!$id){
            return false;
        }

        $sql = "SELECT * FROM `system_withdrawal` WHERE `id` = :id";
        $info = $this->_dbDao->get($sql, array(':id' => $id));
        if (!$info) {
            return false;
        }

        return $info;
    }

    public function getCount($status = 'all', $startTime = '', $endTime = '', $id = 0) {

        $sql = "select count(`id`) as countNum from `system_withdrawal`";

        $whereArr = array();
        if ($id) {
            $whereArr[] = "`id` = {$id}";
        }

        if ($status != 'all') {
            $whereArr[] = "`status` = {$status}";
        }

        if ($startTime && $endTime) {
            $whereArr[] = "`create_time` >= '{$startTime}' AND `create_time` < '{$endTime}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `create_time` DESC";
        $info = $this->_dbDao->get($sql);

        return $info['countNum'];
    }

    public function getSum($status = 'all', $startTime = '', $endTime = '', $id = 0) {

        $sql = "select sum(`amount`) as sumAmount from `system_withdrawal`";

        $whereArr = array();
        if ($id) {
            $whereArr[] = "`id` = {$id}";
        }

        if ($status != 'all') {
            $whereArr[] = "`status` = {$status}";
        }

        if ($startTime && $endTime) {
            $whereArr[] = "`create_time` >= '{$startTime}' AND `create_time` < '{$endTime}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `create_time` DESC";
        $info = $this->_dbDao->get($sql);

        return $info;
    }

    public function getPageList($status = 'all', $startTime = '', $endTime = '', $id = 0) {

        $sql = "select * from `system_withdrawal`";

        $whereArr = array();
        if ($id) {
            $whereArr[] = "`id` = {$id}";
        }

        if ($status != 'all') {
            $whereArr[] = "`status` = {$status}";
        }

        if ($startTime && $endTime) {
            $whereArr[] = "`create_time` >= '{$startTime}' AND `create_time` < '{$endTime}'";
        }

        if ($whereArr) {
            $sql .= " WHERE " . implode(' AND ', $whereArr);
        }

        $sql .= " ORDER BY `create_time` DESC";
        $pageList = $this->_dbDao->pget($sql);

        return $pageList;
    }

    public function update($id, $updateData) {

        if(!$id || !$updateData){
            return false;
        }

        $rs = $this->_dbDao->set('`system_withdrawal`', $updateData, '`id` = :id', array(':id' => $id));
        return $rs;
    }

    public function insert($insertData) {

        if(!$insertData){
            return false;
        }

        $rs = $this->_dbDao->set('`system_withdrawal`', $insertData);
        return $rs;
    }

    public function delete($id) {

        if(!$id){
            return false;
        }

        $rs = $this->_dbDao->del('`system_withdrawal`', '`id` = :id', array(':id' => $id));
        return $rs;
    }

}
