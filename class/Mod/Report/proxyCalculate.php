<?php
class Mod_Report_proxyCalculate extends Mod_Report {

    protected function _do($proxyId = 0, $reportDate = ''){

        if (!$proxyId || !$reportDate) {
            return false;
        }

        $startTime = $reportDate . " 00:00:00";
        $endTime = date('Y-m-d 00:00:00', strtotime($startTime) + 86400);
        $intNowDate = date('Ymd', strtotime($startTime));

        $proxyObj = BooController::get('Obj_Proxy_Info');
        $proxyInfo = $proxyObj->getInfoById($proxyId);

        $yestodayDate = date('Y-m-d', strtotime($startTime) - 86400);
        $nowDateTime = date('Y-m-d H:i:s');
        $payObj = BooController::get('Obj_Proxy_Pay');
        $reportObj = BooController::get('Obj_Proxy_Report');
        $withdrawalObj = BooController::get('Obj_Proxy_Withdrawal');
        $orderObj = BooController::get('Obj_Proxy_Orders');


        // 日报表
        $paySumInfo = $payObj->getSum($proxyId, 0, 0, 0, $startTime, $endTime);
        $orderNumber = $payObj->getCount($proxyId, 0, 0, 0, $startTime, $endTime);
        $withdrawalSumInfo = $withdrawalObj->getSum($proxyId, 2, $startTime, $endTime);

        // 管理员添加资金
        $orderSumInfo4 = $orderObj->getSum($proxyId, 4, $startTime, $endTime);
        // 管理员扣减资金
        $orderSumInfo5 = $orderObj->getSum($proxyId, 5, $startTime, $endTime);
        // 管理员冻结资金
        //$orderSumInfo6 = $orderObj->getSum($tmpInfo['app_id'], 6, $startTime, $endTime);
        // 管理员解冻资金
        $orderSumInfo7 = $orderObj->getSum($proxyId, 7, $startTime, $endTime);

        $delBalance = $orderSumInfo4['sumAmount'] + abs($orderSumInfo7['sumAmount']) + $orderSumInfo5['sumAmount'];// - $orderSumInfo6['sumAmount'];

        $yestodayRsInfo = $reportObj->getInfo($proxyId, $yestodayDate);
        $balance = $yestodayRsInfo['balance'] + $paySumInfo['sumFee'] - $withdrawalSumInfo['sumAmount'] - $withdrawalSumInfo['sumFee'] + $delBalance;

        $tmpRsInfo = $reportObj->getInfo($proxyId, $reportDate);
        if (!$tmpRsInfo) {
            $insertData = array();
            $insertData['proxy_id'] = $proxyId;
            $insertData['proxy_name'] = $proxyInfo['proxy_name'];
            $insertData['order_number'] = $orderNumber;
            $insertData['real_amount'] = $paySumInfo['sumRealAmount'];
            $insertData['fee'] = $paySumInfo['sumFee'];
            $insertData['balance'] = $balance;
            $insertData['report_date'] = $reportDate;
            $insertData['create_time'] = $nowDateTime;
            $reportObj->insert($insertData);
        } else {
            $updateData = array();
            $updateData['order_number'] = $orderNumber;
            $updateData['real_amount'] = $paySumInfo['sumRealAmount'];
            $updateData['fee'] = $paySumInfo['sumFee'];
            $updateData['balance'] = $balance;
            $reportObj->update($tmpRsInfo['id'], $updateData);
        }

        $orderInfo = $orderObj->getInfoByDate($proxyId, 1, $intNowDate);
        if (!$orderInfo) {
            // 计算记录写入帐变表
            $insertData = array();
            $insertData['proxy_id'] = $proxyId;
            $insertData['proxy_name'] = $proxyInfo['proxy_name'];
            $insertData['amount'] = $paySumInfo['sumFee'];
            $insertData['type'] = 1;
            $insertData['start_time'] = $startTime;
            $insertData['end_time'] = $endTime;
            $insertData['create_time'] = $endTime;
            $insertData['create_date'] = $intNowDate;
            $insertData['current_balance'] = $balance;
            $orderObj->insert($insertData);
        } else {
            $updata = array();
            $updata['amount'] = $paySumInfo['sumFee'];
            $updata['end_time'] = $endTime;
            $updata['current_balance'] = $balance;
            $orderObj->update($orderInfo['id'], $updata);
        }

        return true;
    }
    
}