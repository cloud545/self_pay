<?php

class Mod_Admin_getBalance extends Mod_Admin
{
    
    protected function _do()
    {
        
        $startTime    = date('Y-m-d') . " 00:00:00";
        $endTime      = date('Y-m-d H:i:s');
        $yestodayDate = date('Y-m-d', strtotime($startTime) - 86400);
        
        /**
         * @var $balanceObj Obj_Admin_Info
         */
        $balanceObj = BooController::get('Obj_Admin_Info');
        $info       = $balanceObj->getList();
        
        $payObj            = BooController::get('Obj_Admin_Pay');
        $orderObj          = BooController::get('Obj_Admin_Orders');
        $reportObj         = BooController::get('Obj_Admin_Report');
        $withdrawalObj     = BooController::get('Obj_Admin_Withdrawal');
        $paySumInfo        = $payObj->getSum(0, 0, 0, 0, $startTime, $endTime);
        $withdrawalSumInfo = $withdrawalObj->getSum(2, $startTime, $endTime);
        
        $yestodayRsInfo = $reportObj->getInfo($yestodayDate);
        
        // 管理员添加资金
        $orderSumInfo4 = $orderObj->getSum(4, $startTime, $endTime);
        // 管理员扣减资金
        $orderSumInfo5 = $orderObj->getSum(5, $startTime, $endTime);
        // 管理员冻结资金
        //$orderSumInfo6 = $orderObj->getSum(6, $startTime, $endTime);
        // 管理员解冻资金
        $orderSumInfo7 = $orderObj->getSum(7, $startTime, $endTime);
        
        $delBalance = $orderSumInfo4['sumAmount']
                      + abs($orderSumInfo7['sumAmount'])
                      + $orderSumInfo5['sumAmount'];// - $orderSumInfo6['sumAmount'];
        $balance    = $yestodayRsInfo['balance']
                      + $paySumInfo['sumFee']
                      - $withdrawalSumInfo['sumAmount']
                      - $withdrawalSumInfo['sumFee']
                      + $delBalance
                      - $info['frozen_amount']
                      - $info['frozen_balance'];
        
        $info = $balanceObj->getList();
        if (!empty($info)) {
            foreach ($info as $key => $admin_user) {
                $updateData                      = array();
                $updateData['available_balance'] = $balance;
                $updateData['cash_balance']      = $balance
                                                   + $admin_user['frozen_amount']
                                                   + $admin_user['frozen_balance'];
                $balanceObj->update($updateData, $admin_user['admin_id']);
            }
        }
        
        return $balance;
    }
    
}
