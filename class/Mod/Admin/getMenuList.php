<?php
class Mod_Admin_getMenuList extends Mod_Admin{

    protected function _do(){

        $obj = BooController::get('Obj_Admin_Menu');
        $list = $obj->getList();

        // 先提取一级菜单
        $floorMenuList = array();
        foreach ($list as $info) {
            if (!$info['menu_parent_id']) {
                $floorMenuList[$info['menu_id']] = $info;
            }
        }

        // 提取二级菜单
        foreach ($list as $info) {
            if (isset($floorMenuList[$info['menu_parent_id']])) {
                $floorMenuList[$info['menu_parent_id']]['childList'][$info['menu_id']] = $info;
            }
        }

        // 提取三级菜单
        foreach ($list as $info) {

            // 一级和二级菜单跳过
            if (!$info['menu_parent_id'] || isset($floorMenuList[$info['menu_parent_id']])) {
                continue;
            }

            foreach ($floorMenuList as $menuId => $tmpInfo) {
                if (isset($tmpInfo['childList'][$info['menu_parent_id']])) {
                    $floorMenuList[$menuId]['childList'][$info['menu_parent_id']]['childList'][$info['menu_id']] = $info;
                }
            }
        }

        return $floorMenuList;
    }
    
}