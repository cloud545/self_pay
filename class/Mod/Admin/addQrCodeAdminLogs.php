<?php
class Mod_Admin_addQrCodeAdminLogs extends Mod_Admin{

	protected function _do($adminId = 0){

	    if (!$adminId) {
            $adminId = BooSession::get('qrCodeUserId');
        }
        $uri = $_SERVER['REQUEST_URI'];

        /*
        if (strpos($uri, 'list')) {
            return false;
        }
        // */
        $noLogMenu = BooConfig::get('main.noLogMenu');
        if (in_array($uri, $noLogMenu)) {
            return false;
        }

        $obj = BooController::get('Obj_QrPay_Menu');
        $info = $obj->getInfoByTarget($uri);
        if (!$info) {
            return false;
        }

        $logObj = BooController::get('Obj_QrPay_Logs');

        // 隐藏掉密码项
        if ($_REQUEST['oldpass']) {
            $_REQUEST['oldpass'] = '******';
        }
        if ($_REQUEST['newpass']) {
            $_REQUEST['newpass'] = '******';
        }
        if ($_REQUEST['newpass2']) {
            $_REQUEST['newpass2'] = '******';
        }
        if ($_REQUEST['password']) {
            $_REQUEST['password'] = '******';
        }
        if ($_REQUEST['password2']) {
            $_REQUEST['password2'] = '******';
        }
        if ($_REQUEST['oldPassword']) {
            $_REQUEST['oldPassword'] = '******';
        }
        if ($_REQUEST['newPassword']) {
            $_REQUEST['newPassword'] = '******';
        }
        if ($_REQUEST['newPassword2']) {
            $_REQUEST['newPassword2'] = '******';
        }
        if ($_REQUEST['appLoginCaptcha']) {
            unset($_REQUEST['appLoginCaptcha']);
        }
        if ($_REQUEST['PHPSESSID']) {
            unset($_REQUEST['PHPSESSID']);
        }

        foreach ($_REQUEST as $key => $value) {

            $pos = strpos($key, '_pk_');
            if ($pos !== false) {
                unset($_REQUEST[$key]);
            }

            $pos = strpos($key, '__');
            if ($pos !== false) {
                unset($_REQUEST[$key]);
            }

        }

        $insertData = array(
            'admin_id' => $adminId,
            'controller' => $uri,
            'actioner' => $_REQUEST['flag'] ? $_REQUEST['flag'] : '',
            'title' => $info['menu_title'],
            'content' => json_encode($_REQUEST),
            'create_time' => date('Y-m-d H:i:s'),
            'ip' => BooUtil::realIp(),
        );

        $logObj->insert($insertData);

        return true;
	}

}