<?php
class Mod_App_getBalance extends Mod_App {

    protected function _do($appId = 0){

        $startTime = date('Y-m-d') . " 00:00:00";
        $endTime = date('Y-m-d H:i:s');
        $yestodayDate = date('Y-m-d', strtotime($startTime) - 86400);

        $reportObj = BooController::get('Obj_App_Report');
        $payObj = BooController::get('Obj_App_Pay');
        $withdrawalObj = BooController::get('Obj_App_Withdrawal');
        $orderObj = BooController::get('Obj_App_Orders');
        $balanceObj = BooController::get('Obj_App_Balance');

        $yestodayRsInfo = $reportObj->getInfo($appId, $yestodayDate);
        $paySumInfo = $payObj->getSum($appId, 0, 0, 0, $startTime, $endTime);
        $withdrawalSumInfo = $withdrawalObj->getSum($appId, 2, $startTime, $endTime);

        // 管理员添加资金
        $orderSumInfo4 = $orderObj->getSum($appId, 4, $startTime, $endTime);
        // 管理员扣减资金
        $orderSumInfo5 = $orderObj->getSum($appId, 5, $startTime, $endTime);
        // 管理员冻结资金
        //$orderSumInfo6 = $orderObj->getSum($appId, 6, $startTime, $endTime);
        // 管理员解冻资金
        $orderSumInfo7 = $orderObj->getSum($appId, 7, $startTime, $endTime);

        $delBalance = $orderSumInfo4['sumAmount'] + abs($orderSumInfo7['sumAmount']) + $orderSumInfo5['sumAmount'];// - $orderSumInfo6['sumAmount'];

        $appBalanceInfo = $balanceObj->getInfoByAppId($appId);
        $balance = $yestodayRsInfo['balance'] + $paySumInfo['sumFee'] - $withdrawalSumInfo['sumAmount'] - $withdrawalSumInfo['sumFee'] + $delBalance - $appBalanceInfo['frozen_amount'] - $appBalanceInfo['frozen_balance'];

        if ($appBalanceInfo['available_balance'] != $balance) {
            $updateData = array();
            $updateData['available_balance'] = $balance;
            $updateData['cash_balance'] = $balance + $appBalanceInfo['frozen_amount'] + $appBalanceInfo['frozen_balance'];
            $updateData['last_modify_time'] = $endTime;
            $rs = $balanceObj->update($appBalanceInfo['id'], $updateData);
            if (!$rs) {
                return false;
            }
        }

        return $balance;
    }
    
}