<?php
class Mod_ThirdPlatform_XiaobaoPay_payCallback extends Mod_ThirdPlatform_XiaobaoPay {

	protected function _do($platInfo = array()) {
        $data = [
            'status' 				=> $_REQUEST['status'],
            'msg' 					=> $_REQUEST['msg'],
            'tenantOrderNo'	=> $_REQUEST['tenantOrderNo'],
            'amount'		=> $_REQUEST['amount'],
            'oldAmount' => $_REQUEST['oldAmount'],
            'remark'		=> $_REQUEST['remark'],
        ];

        $sign = strtoupper(md5($this->serialization($data)."&key={$platInfo['platInfo']['appKey']}")); // key请在后台管理系统的我的账户中查看

        if ($_REQUEST['sign'] == $sign && $_REQUEST['status'] == 200){
            echo "success";
            return array('platOrderId' => $_REQUEST['outTradeNo'],'realMoney' => $_REQUEST['amount']);
        } else {
            return false;
        }

	}

}
