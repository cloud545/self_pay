<?php
class Mod_ThirdPlatform_XiaobaoPay_saoma extends Mod_ThirdPlatform_XiaobaoPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝wap
            $payType = 'alipay';//$isMobile ? '904' : '903';
        } elseif ($type == 3) { // 微信wap
            $payType = 'wxpay';
        } elseif ($type == 5) { // 银联扫码
            $payType = '926';
        } elseif ($type == 9) { // 支付宝扫码
            $payType = 'alipay';
        } elseif ($type == 10) { // 微信扫码
            $payType = 'wxpay';
        }

        $data = [
            'tenantOrderNo' => $payInfo['pOrderId'],
            'pageUrl'		=> '',//$platInfo['appInfo']['clientUrl']
            'notifyUrl'		=> $platInfo['appInfo']['callbackUrl'],
            'amount'		=> $payInfo['amount'],
            'payType'		=> $payType,
            'remark'		=> '',
        ];

        $data['sign'] = strtoupper(md5($this->serialization($data)."&key={$platInfo['platInfo']['appKey']}")); // key请在后台管理系统的我的账户中查看

        $res = $this->http_post_data($platInfo['platInfo']['p_pay_url'], $data);
        $result = json_decode($res, true);
        if ($result['status'] != 200) {
            echo $res;
            exit;
        }

        BooView::set('payUrl', $result['url']);
        BooView::display('pay/locationPay.html');
        exit;
    }

}
