<?php
class Mod_ThirdPlatform_XiaobaoPay extends Mod_ThirdPlatform {

    protected function http_post_data($url, $data_string) {
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    protected function serialization($array){
        $arr = array();
        foreach ($array as $key=>$value){
            if (!$value) {
                continue;
            }
            $arr[$key] = $key;
        }
        sort($arr);
        $str = "";
        foreach ($arr as $k => $v) {
            if($k ==0){
                $str =$v.'='.$array[$v];
            }else{
                $str =$str.'&'.$v.'='.$array[$v];}
        }
        return $str;
    }

}
