<?php
class Mod_ThirdPlatform_BaiYiPay_saoma extends Mod_ThirdPlatform_BaiYiPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 17) {
            $payType = '5296';
        }

        $data = array(
            'pay_memberid' => $platInfo['platInfo']['appId'],
            'pay_orderid' => $payInfo['pOrderId'],
            'pay_applydate' => date('Y-m-d H:i:s'),
            'pay_bankcode' => $payType,
            'pay_notifyurl' => $platInfo['appInfo']['callbackUrl'] ,
            'pay_amount' => $payInfo['amount'],
        );
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $md5str = $md5str .'key='.$platInfo['platInfo']['appKey'];
        $data['pay_md5sign'] = strtoupper(md5($md5str));
        $data['pay_productname'] = '充值';
        $data['format'] = 'json';
        $data = http_build_query(
            $data
        );
        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => $data
            )
        );
        $context = stream_context_create($opts);
        $result = file_get_contents($platInfo['platInfo']['p_pay_url'], false, $context);
        $result = json_decode($result,1);
        if ($result['status'] == 'ok') {
            header('location:'.$result['data']['pay_url']);
        } else {
            echo $result['msg'];
        }
    }
}
