<?php
class Mod_ThirdPlatform_BaiYiPay_payCallback extends Mod_ThirdPlatform_BaiYiPay {

	protected function _do($platInfo = array()) {
        $data = $_POST;
        unset($data['attach']);

        $sign = $_POST['sign'];
        unset($data['sign']);
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $md5str = $md5str. 'key=' .$platInfo['platInfo']['appKey'];
        $signs = strtoupper(md5($md5str));

        if ($sign == $signs && $data['returncode'] == '00') {
            echo 'OK';
            return array('platOrderId' => $_POST['orderid'],'realMoney' => $_POST['amount']);
        }
        return false;
	}

}
