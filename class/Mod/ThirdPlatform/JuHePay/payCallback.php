<?php
class Mod_ThirdPlatform_JuHePay_payCallback extends Mod_ThirdPlatform_JuHePay {

    protected function _do($platInfo = array()) {
        //商户名称
        $appid  = $_POST['appid'];
        //支付时间戳
        $callbacks  = $_POST['callbacks'];
        //支付状态
        $pay_type  = $_POST['pay_type'];
        //支付金额
        $amount  = $_POST['amount'];
        //支付时提交的订单信息
        $success_url  = $_POST['success_url'];
        //平台订单交易流水号
        $error_url  = $_POST['error_url'];
        //该笔交易手续费用
        $out_trade_no  = $_POST['out_trade_no'];
        //实付金额
        $amount_true  = $_POST['amount_true'];
        //用户请求uid
        $out_uid  = $_POST['out_uid'];
        //回调时间戳
        $sign  = $_POST['sign'];

        $data = [
            'appid'         => $appid,
            'callbacks'     => $callbacks,
            'pay_type'      => $pay_type,
            'amount'        => $amount,
            'success_url'   => $success_url,
            'error_url'     => $error_url,
            'out_trade_no'  => $out_trade_no,
            'amount_true'   => $amount_true,
            'out_uid'       => $out_uid,
            'sign'          => $sign,
        ];

        $md5key = $platInfo['platInfo']['appKey'];
        if (!$this->verifySign($data,$md5key)){
            echo "error";
            return false;
        }else{
            echo 'success';
            return array('platOrderId' => $out_trade_no,'realMoney' => $amount_true);
        }
    }

    /**
     * @Note  生成签名
     * @param $secret   商户密钥
     * @param $data     参与签名的参数
     * @return string
     */
    public function getSign($secret, $data)
    {

        // 去空
        $data = array_filter($data);

        //签名步骤一：按字典序排序参数
        ksort($data);
        $string_a = http_build_query($data);
        $string_a = urldecode($string_a);

        //签名步骤二：在string后加入mch_key
        $string_sign_temp = $string_a . "&key=" . $secret;

        //签名步骤三：MD5加密
        $sign = md5($string_sign_temp);

        // 签名步骤四：所有字符转为大写
        $result = strtoupper($sign);

        return $result;
    }


    /**
     * @Note   验证签名
     * @param $data
     * @param $orderStatus
     * @return bool
     */
    public function verifySign($data, $secret) {
        // 验证参数中是否有签名
        if (!isset($data['sign']) || !$data['sign']) {
            return false;
        }
        // 要验证的签名串
        $sign = $data['sign'];
        unset($data['sign']);
        // 生成新的签名、验证传过来的签名
        $sign2 = $this->getSign($secret, $data);

        if ($sign != $sign2) {
            return false;
        }
        return true;
    }



}
