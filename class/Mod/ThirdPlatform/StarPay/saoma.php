<?php
class Mod_ThirdPlatform_StarPay_saoma extends Mod_ThirdPlatform_StarPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝wap
            $payType = '01';//$isMobile ? '904' : '903';
        } elseif ($type == 3) { // 微信wap
            $payType = '02';
        } elseif ($type == 5) { // 银联扫码
            $payType = '03';
        } elseif ($type == 9) { // 支付宝扫码
            $payType = '01';
        } elseif ($type == 10) { // 微信扫码
            $payType = '02';
        }

        $postData = array();
        $postData['appId'] = $platInfo['platInfo']['appId'];
        $postData['tradeOrderId'] = $payInfo['pOrderId'];
        $postData['amount'] = $payInfo['amount'] * 100;
        $postData['channelId'] = $payType;
        $postData['notifyUrl'] = $platInfo['appInfo']['callbackUrl'];

        ksort($postData);
        $md5Str = '';
        foreach ($postData as $key => $value) {

            if (!$md5Str) {
                $md5Str .= "{$key}={$value}";
            } else {
                $md5Str .= "&{$key}={$value}";
            }
        }

        $sign = md5($md5Str . "&key={$platInfo['platInfo']['appKey']}");
        $postData['sign'] = $sign;

        BooCurl::setData($postData, 'POST');
        BooCurl::setOption(CURLOPT_HTTPHEADER, array(
            "Content-Type : application/x-www-form-urlencoded;charset=utf-8",
        ));
        $response = BooCurl::call($platInfo['platInfo']['p_pay_url']);

        $result = json_decode($response, true);
        if ($result['code'] != 200) {
            BooFile::write('/tmp/starPay.log', $response . "\n{$payInfo['amount']}\n{$platInfo['appInfo']['name']}\n111\n\n", 'a');
            echo "支付异常，请联系客服! code：{$result['code']}，msg: {$result['msg']}";
            exit;
        }

        BooView::set('payUrl', $result['data']);
        BooView::display('pay/locationPay.html');
        exit;
    }

}
