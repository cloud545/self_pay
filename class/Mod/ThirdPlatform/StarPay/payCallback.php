<?php
class Mod_ThirdPlatform_StarPay_payCallback extends Mod_ThirdPlatform_StarPay {

	protected function _do($platInfo = array()) {

	    $post = $_REQUEST;

        $postData = array();
        $postData['success'] = $post['success'];
        $postData['tradeOrderId'] = $post['tradeOrderId'];
        $postData['amount'] = $post['amount'];
        $postData['platOrderId'] = $post['platOrderId'];

        ksort($postData);
        $md5Str = '';
        foreach ($postData as $key => $value) {

            if (!$md5Str) {
                $md5Str .= "{$key}={$value}";
            } else {
                $md5Str .= "&{$key}={$value}";
            }
        }

        $sign = md5($md5Str . "&key={$platInfo['platInfo']['appKey']}");
        if ($post['sign'] == $sign && $postData['success']) {
            echo "success";
            return array('platOrderId' => $postData['platOrderId'], 'realMoney' => $postData['amount'] / 100);
        } else {
            return false;
        }

	}

}
