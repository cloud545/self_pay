<?php
class Mod_ThirdPlatform_YTongPay_payCallback extends Mod_ThirdPlatform_YTongPay {

        protected function _do($platInfo = array()) {
        $native = array(
            'income'            => $_POST['income'],
            'payOrderId'        => $_POST['payOrderId'],
            'mchId'             => $_POST['mchId'],
            'mchOrderNo'        => $_POST['mchOrderNo'],
            'amount'            => $_POST['amount'],
            'paySuccTime'       => $_POST['paySuccTime'],
        );
        ksort($native);
        $md5Str = '';
        foreach ($native as $key => $value) {

            if (!$value) {
                continue;
            }

            if (!$md5Str) {
                $md5Str = "{$key}={$value}";
            } else {
                $md5Str .= "&{$key}={$value}";
            }
        }
        $checkSign = strtoupper(md5($md5Str . "&key={$platInfo['platInfo']['appKey']}"));
        $native['sign'] = $_POST['sign'];
        if ($native['sign'] == $checkSign) {
            echo 'success';
            return array('platOrderId' => $native['payOrderId'],'realMoney' => $native["amount"]);
        }
        return false;
        }

}
