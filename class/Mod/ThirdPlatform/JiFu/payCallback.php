<?php
class Mod_ThirdPlatform_JiFu_payCallback extends Mod_ThirdPlatform_JiFu {

	protected function _do($platInfo = array()) {
        $data = array( // 返回字段
            "memberid" => $_REQUEST["memberid"], // 商户ID
            "orderid" =>  $_REQUEST["orderid"], // 订单号
            "amount" =>  $_REQUEST["amount"], // 交易金额
            "datetime" =>  $_REQUEST["datetime"], // 交易时间
            "transaction_id" =>  $_REQUEST["transaction_id"], // 支付流水号
            "returncode" => $_REQUEST["returncode"],
        );

        $sign = $_POST['sign'];
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $md5str = $md5str. 'key=' .$platInfo['platInfo']['appKey'];
        $signs = strtoupper(md5($md5str));

        if ($sign == $signs && $data['returncode'] == '00') {
            echo 'OK';
            return array('platOrderId' => $_POST['orderid'],'realMoney' => $_POST['amount']);
        }
        return false;
	}

}
