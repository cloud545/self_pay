<?php
class Mod_ThirdPlatform_XinGuangDong_saoma extends Mod_ThirdPlatform_XinGuangDong {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;

        if ($type == 10) {
            $payType = '6015';
        }


        $data = array(
            'merchantNo' => $platInfo['platInfo']['appId'],
            'orderNo' => $payInfo['pOrderId'],
            'total' => $payInfo['amount'] * 100,
            'payMethod' => $payType,
            'notifyUrl' => $platInfo['appInfo']['callbackUrl'],
            'version' => 'v2.0',
            'timestamp' => $this->getMillisecond(),
            'name' => '成人用品',
        );

        ksort($data);

        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key  .'='. $value ;
        }
        $data['sign'] = md5($md5str.$platInfo['platInfo']['appKey']);

        $data = http_build_query(
            $data
        );
        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => $data
            )
        );
        $context = stream_context_create($opts);
        $result = file_get_contents($platInfo['platInfo']['p_pay_url'], false, $context);
        $result = json_decode($result,1);
        if ($result['code'] == '1') {
            header('location:'. $result['result']['payUrl']);
        } else {
            echo $result['msg'];
        }
    }

    public function getMillisecond() {
        list($s1, $s2) = explode(' ', microtime());
        return (float)sprintf('%.0f', (floatval($s1) + floatval($s2)) * 1000);
    }
}

