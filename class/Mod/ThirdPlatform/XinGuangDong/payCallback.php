<?php
class Mod_ThirdPlatform_XinGuangDong_payCallback extends Mod_ThirdPlatform_XinGuangDong {

	protected function _do($platInfo = array()) {

        $data = $_POST;
        $sign = $data['sign'];
        unset($data['sign']);

        ksort($data);

        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key  .'='. $value ;
        }
        $signs = md5($md5str.$platInfo['platInfo']['appKey']);
        if ($sign == $signs && $data['code'] == '1') {
            echo 'success';
            return array('platOrderId' => $data['orderNo'],'realMoney' => $data['total'] / 100);
        }
        return false;
	}

}
