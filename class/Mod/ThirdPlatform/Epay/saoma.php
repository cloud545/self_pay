<?php
class Mod_ThirdPlatform_Epay_saoma extends Mod_ThirdPlatform_Epay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();
        $payType = 0;
        $trade_code = '';

        if ($type == 9) {
            $payType = 'pc';
            $trade_code = 'alipay pc';
        }

        if ($type == 2) {
            $payType = 'wap';
            $trade_code = 'alipay wap';
        }
        $data = array(
            'merchant_id' => $platInfo['platInfo']['appId'],
            'version' => 'V1.1',
            'service' => 'service.ffff.pay',
            'request_time' => date('YmdHis'),
            'nonce_str' => time(),
        );

        $content = [
            'order_no' => $payInfo['pOrderId'],
            'amount' => number_format($payInfo['amount'],'2','.',''),
            'notify_url' => $platInfo['appInfo']['callbackUrl'],
            'currency' => 'CNY',
            'trade_code' => $trade_code,
            'client_ip' => $platInfo['appInfo']['last_ip'],
            'terminal_type' => $payType,
        ];

        $arr = array_merge($data, $content);

        ksort($arr);
        $md5str = '';
        foreach ($arr as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $data['sign'] = strtoupper(md5($md5str.'key='.$platInfo['platInfo']['appKey']));
        $data['data'] = json_encode($content);
        $data['sign_type'] = 'MD5';
        $data = json_encode($data);
        $ch = curl_init($platInfo['platInfo']['p_pay_url']);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data))
        );
        $result = json_decode(curl_exec($ch),1);
        if ($result['resp_code'] == '1000') {
            header('location:'.$result['data']['pay_info']);
        } else {
            var_dump($result);
        }
    }
}
