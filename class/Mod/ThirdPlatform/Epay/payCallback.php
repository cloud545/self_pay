<?php
class Mod_ThirdPlatform_Epay_payCallback extends Mod_ThirdPlatform_Epay {

	protected function _do($platInfo = array()) {
        $data = json_decode(file_get_contents('php://input'), true);
        file_put_contents('test.txt',json_encode($data));
        $sign = $data['sign'];
        unset($data['sign']);
        unset($data['sign_type']);
        $content = $data['data'];
        unset($data['data']);
        $arr = array_merge($data, $content);
        ksort($arr);
        $md5str = '';
        foreach ($arr as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $signs = strtoupper(md5($md5str.'key='.$platInfo['platInfo']['appKey']));

        if ($sign == $signs) {
            echo 'SUCCESS';
            return array('platOrderId' => $content['order_no'],'realMoney' => $content['amount']);
        }
        return false;
	}
}
