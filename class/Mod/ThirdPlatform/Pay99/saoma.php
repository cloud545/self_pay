<?php
class Mod_ThirdPlatform_Pay99_saoma extends Mod_ThirdPlatform_Pay99 {


    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝
            $payType = 3237;
        } elseif ($type == 3) { // 微信
            $payType = 3227;
        } elseif ($type == 5) { // 银联
            $payType = 3257;
        }

        $info = array(
            'agentNo'=>$platInfo['platInfo']['appId'],
            'tradeAmt'=> $payInfo['amount'] * 100,
            'orderNo'=>$payInfo['pOrderId'],
            'callbackUrl'=>$platInfo['appInfo']['callbackUrl'],
            'bizCode'=>$payType
        );

        ksort($info);

        $md5Str = '';
        foreach ($info as $key => $val) {
            $md5Str .= "{$key}{$val}";
        }

        $sign = strtoupper(md5($platInfo['platInfo']['appKey'] . $md5Str . $platInfo['platInfo']['appKey']));

        BooCurl::setData($info, 'POST');
        $response = BooCurl::call($platInfo['platInfo']['p_pay_url'] . "/{$platInfo['platInfo']['appId']}/createOrder/{$sign}");
        $result = json_decode($response, true);
        if ($result['retCode'] != '0000') {
            echo "支付异常，请联系客服! code：{$result['retCode']}，msg: {$result['retMsg']}";
            exit;
        }

        $jsonStrArr = json_decode($result['retData'], true);

        header('Location: ' . $jsonStrArr['scanUrl']);
        exit;


    }

}
