<?php
class Mod_ThirdPlatform_Pay99_payCallback extends Mod_ThirdPlatform_Pay99 {

	protected function _do($platInfo = array()) {

        $info = array(
            'tradeNo' => $_REQUEST['tradeNo'],
            'orderNo'=>$_REQUEST['orderNo'],
            'status'=>$_REQUEST['status'],
            'alipayOrderNo'=>$_REQUEST['alipayOrderNo'],
            'repeatReceipt'=>$_REQUEST['repeatReceipt'],
            'payTime'=>$_REQUEST['payTime'],
            'agent_no'=>$_REQUEST['agent_no'],
            'trade_amt'=>$_REQUEST['trade_amt'],
        );

        ksort($info);

        $md5Str = '';
        foreach ($info as $key => $val) {
            $md5Str .= "{$key}{$val}";
        }

        $checkSign = strtoupper(md5($platInfo['platInfo']['appKey'] . $md5Str . $platInfo['platInfo']['appKey']));
        if ($_REQUEST['sign'] == $checkSign && $_REQUEST['status'] == 2) {
            echo 'success';
            return array('platOrderId' => $_REQUEST['tradeNo'],'realMoney' => $_REQUEST['trade_amt'] / 100);
        }else {
            //签名验证失败
            return false;
        }

	}

}
