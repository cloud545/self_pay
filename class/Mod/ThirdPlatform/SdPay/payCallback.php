<?php
class Mod_ThirdPlatform_SdPay_payCallback extends Mod_ThirdPlatform_SdPay {

	protected function _do($platInfo = array()) {

        $postData = array();
        $postData['payKey'] = $_REQUEST['payKey'];
        $postData['orderNo'] = $_REQUEST['orderNo'];
        $postData['productName'] = $_REQUEST['productName'];
        $postData['orderPrice'] = $_REQUEST['orderPrice'];
        $postData['orderDate'] = $_REQUEST['orderDate'];
        $postData['orderTime'] = $_REQUEST['orderTime'];
        $postData['remark'] = $_REQUEST['remark'];
        $postData['trxNo'] = $_REQUEST['trxNo'];
        $postData['tradeStatus'] = $_REQUEST['tradeStatus'];

        ksort($postData);

        $md5Str = '';
        foreach ($postData as $key => $value) {

            if (!$value) {
                continue;
            }

            if (!$md5Str) {
                $md5Str .= "{$key}={$value}";
            } else {
                $md5Str .= "&{$key}={$value}";
            }
        }

        $sign = strtoupper(md5($md5Str . "&paySecret={$platInfo['platInfo']['appSecret']}"));
        if ($_REQUEST['sign'] == $sign && $_REQUEST['tradeStatus'] == 'SUCCESS') {
            echo 'OK';
            return array('platOrderId' => $_REQUEST['trxNo'], 'realMoney' => $_REQUEST['orderPrice']);
        } else {
            //签名验证失败
            return false;
        }

	}

}
