<?php
class Mod_ThirdPlatform_SdPay_saoma extends Mod_ThirdPlatform_SdPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝wap
            $payType = 'ALIPAY';
        } elseif ($type == 3) { // 微信wap
            $payType = 'WEIXIN';
        } elseif ($type == 5) { // 银联扫码
            $payType = 'YL_QR';
        } elseif ($type == 9) { // 支付宝扫码
            $payType = 'ALIPAY';
        } elseif ($type == 10) { // 微信扫码
            $payType = 'WEIXIN';
        }

        $postData = array();
        $postData['payKey'] = $platInfo['platInfo']['appKey'];
        $postData['orderNo'] = $payInfo['pOrderId'];
        $postData['productName'] = '商品';
        $postData['orderPrice'] = number_format($payInfo['amount'],2,'.','');
        $postData['payWayCode'] = $payType;
        $postData['orderDate'] = date('Ymd');
        $postData['orderTime'] = date('YmdHis');
        $postData['notifyUrl'] = $platInfo['appInfo']['callbackUrl'];

        ksort($postData);

        $md5Str = '';
        foreach ($postData as $key => $value) {

            if (!$value) {
                continue;
            }

            if (!$md5Str) {
                $md5Str .= "{$key}={$value}";
            } else {
                $md5Str .= "&{$key}={$value}";
            }
        }

        $sign = md5($md5Str . "&paySecret={$platInfo['platInfo']['appSecret']}");
        $postData['sign'] = strtoupper($sign);

        BooView::set('payUrl', "{$platInfo['platInfo']['p_pay_url']}?{$md5Str}&sign={$sign}");
        BooView::display('pay/locationPay.html');
        exit;
    }

}
