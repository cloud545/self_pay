<?php
class Mod_ThirdPlatform_Weihui_saoma extends Mod_ThirdPlatform_Weihui {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝
            $payType = '904';
        } elseif ($type == 3) { // 微信
            $payType = '902';
        } elseif ($type == 5) { // 银联扫码
            $payType = '926';
        } elseif ($type == 9) { // 支付宝扫码
            $payType = '903';
        } elseif ($type == 10) { // 微信扫码
            $payType = '902';
        }

        $native = array(
            "pay_memberid" => $platInfo['platInfo']['appId'],
            "pay_orderid" => $payInfo['pOrderId'],
            "pay_amount" => number_format($payInfo['amount'],2,'.',''),
            "pay_applydate" => date("Y-m-d H:i:s"),
            "pay_bankcode" => $payType,
            "pay_notifyurl" => $platInfo['appInfo']['callbackUrl'],
            "pay_callbackurl" => $platInfo['appInfo']['clientUrl'],
        );
        ksort($native);
        $md5str = "";
        foreach ($native as $key => $val) {
            $md5str = $md5str . $key . "=" . $val . "&";
        }

        $sign = strtoupper(md5($md5str . "key=" . $platInfo['platInfo']['appKey']));
        $native["pay_md5sign"] = $sign;
        $native['pay_attach'] = "1234|456";
        $native['pay_productname'] ='团购商品';


        BooView::set('payUrl', $platInfo['platInfo']['p_pay_url']);
        BooView::set('native', $native);
        BooView::display('pay/formPay.html');
        exit;
    }

}
