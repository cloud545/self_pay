<?php
class Mod_ThirdPlatform_XiaoQiangPay extends Mod_ThirdPlatform {

    public function sign($params = [], $secret = '')
    {
        ksort($params);
        $str = '';
        foreach ($params as $k => $v) {
            if ($v) {
                $str = $str . $k .'='. $v.'&';
            }
        }
        $str = $str .'key='. $secret;
        return strtoupper(md5($str));
    }
}
