<?php
class Mod_ThirdPlatform_XiaoQiangPay_payCallback extends Mod_ThirdPlatform_XiaoQiangPay {

    protected function _do($platInfo = array()) {

        $data = $_POST;
        $arr = json_decode($data['body'], 1);
        $sign = $data['sign'];
        $status = $this->getStatus($arr,$platInfo['platInfo']['appId'] ,$platInfo['platInfo']['appKey']);
        $arr = json_decode($data['body'], 1);
		//进行验签
        $checkSign = strtoupper(md5('body='.$data['body'].'&key='.$platInfo['platInfo']['appKey']));
        if ($checkSign == $sign &&  $arr['status'] == 'success' && $status == 'ok'){
            return array('platOrderId' => $arr['outTradeNo'],'realMoney' => $arr['actualAmount']);
        } else {
            return false;
        }
    }

    public function getStatus($arr ,$id ,$appKey) {
        $data['appId'] = $id;
        $data['outTradeNo'] = $arr['outTradeNo'];
        $data['nonceStr'] = time();
        $data['sign'] = $this->sign($data, $appKey);
        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, 'https://xqpay.info/api/v2/orderInfo'); // 要访问的地址
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        $result = json_decode(curl_exec($curl),1);
        if ($result['data']['status'] == 'success') {
            return 'ok';
        } else {
            return false;
        }
    }
}

