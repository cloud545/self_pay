<?php
class Mod_ThirdPlatform_XiaoQiangPay_saoma extends Mod_ThirdPlatform_XiaoQiangPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];

        $payType  = 0;
        
		
		if ($type == 2) {       // 支付宝wap
            $payType = 'CP';
        }

        if ($type == 9) { // 支付宝扫码
            $payType = 'CP';
        }
		
        //参与验签的参数
        $data = array(
            'appId'             => $platInfo['platInfo']['appId'],        //商户id
            'outTradeNo'        => $payInfo['pOrderId'],                  //商户订单号
            'amount'            => number_format($payInfo['amount'], '2', '.', ''),              //支付金额,分位制
            'payType'           => $payType,                            //商品标题
            'ip'              =>  $_SERVER['REMOTE_ADDR'],                        //商品描述信息
            'asyncUrl'         => $platInfo['appInfo']['callbackUrl'],   //通知地址
            'nonceStr' => time(),
        );

        $data['sign'] = $this->sign($data , $platInfo['platInfo']['appKey']);

        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, $platInfo['platInfo']['p_pay_url']); // 要访问的地址
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        $result = json_decode(curl_exec($curl),1);
        curl_close($curl); // 关闭CURL会话
        if ($result['success'] == true) {
            $url = str_replace('http://akalipay.com', 'https://xqpay.info', $result['data']['payUrl']);
            header('location:'.$url);
        } else {
            echo $result['message'];
        }
    }
}
