<?php
class Mod_ThirdPlatform_HBaoPay_saoma extends Mod_ThirdPlatform_HBaoPay {

    protected function _do($platInfo = array(), $payInfo = array()) {
        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        if ($type == 1) {			// 网银
            $typeCode       = '10';
            $payResource    = '0';
        }else{
            exit("通道错误！");
        }

        $postData = [
            'shop_no'  	=> $platInfo['platInfo']['appId'],	                  //商户号-平台提供
            'shop_order_no'  	=> $payInfo['pOrderId'],	                  //商户订单号
            'shop_order_time' => date("Y-m-d H:i:s"),	                  //商户订单时间
            'trans_type_code' => $typeCode,		                  //支付方式
            'money'  	=> number_format($payInfo['amount'] * 100,2,'.',''),    //交易金额
            'order_remark'  	=> 'best1pays',		                  //订单备注
            'order_name'  	=> 'best1pays',		                  //订单名称
            'callback_url'  	=> $platInfo['appInfo']['callbackUrl'],                //异步回调地址
            'pay_resource'  	=> $payResource,		                 //支付的类型
            'timestamp'  	=> time()		                 //时间戳
        ];

        //排序
        ksort($postData);
        $stringToBeSigned = "";
        foreach ($postData as $key =>$value) {
            $stringToBeSigned = "{$key}"."{$value}";
        }
        $stringToBeSigned = $stringToBeSigned.$platInfo['platInfo']['appKey'];

        //echo $stringToBeSigned;
        $postData['sign'] = sha1($stringToBeSigned);
        $postData['auth_code'] = $platInfo['platInfo']['appId'];

        BooCurl::setData(json_encode($postData), 'POST');
        BooCurl::setOption(CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json;charset=UTF-8',
        ));
        $response = BooCurl::call($platInfo['platInfo']['p_pay_url']);
        $resData = json_decode($response, true);
        if($resData['code'] != 'R0001' || $resData['desc'] != '下单完成！')
        {
            echo "支付异常，请联系客服! data: {$resData['desc']}";
            exit;
        }

        BooView::set('payUrl', $resData['data']['payUrl']);
        BooView::display('pay/locationPay.html');
        exit;
    }


    public function makeAuthSort($data = [], $return = false,$secret_key = false)
    {

        $signature = '';
        ksort($data);
        foreach ($data as $key => $item) {
            $signature .= $key . '=' . $item . '&';
        }
        if ($return) {
            return [
                'params' => json_encode($data),
            ];
        }
        else {
            $signature .= 'key=' . $secret_key;

            return strtoupper(
                md5($signature)
            );
        }
    }

	
	public function payurl($url,$params){
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_HTTP_VERSION , CURL_HTTP_VERSION_1_1 );
        //json数据格式请求头
        curl_setopt( $ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json; charset=utf-8']);
        curl_setopt( $ch, CURLOPT_USERAGENT , 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.172 Safari/537.22' );
        curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT , 30 );
        curl_setopt( $ch, CURLOPT_TIMEOUT , 30);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER , true );
        curl_setopt( $ch , CURLOPT_POST , true );
        curl_setopt( $ch , CURLOPT_POSTFIELDS , $params );
        curl_setopt( $ch , CURLOPT_URL , $url );

        $response = curl_exec( $ch );
        if ($response === FALSE) {
            return false;
        }
        $httpCode = curl_getinfo( $ch , CURLINFO_HTTP_CODE );
        curl_close( $ch );
        return $response;
    }

}
