<?php
class Mod_ThirdPlatform_HBaoPay_payCallback extends Mod_ThirdPlatform_HBaoPay {

    protected function _do($platInfo = array()) {
        $postData                   = array();
        //参与验签的参数(空值不参与验签)
        $postData['code']               = $_POST['code'];                 //返回状态码
        $postData['result']             = $_POST['result'];             //商户id
        $postData['shopNo']          = $_POST['shopNo'];                 //商户订单号
        $postData['orderStatus']     = $_POST['orderStatus'];          //支付产品
        $postData['orderNo']         = $_POST['orderNo'];               //支付金额 分位置
        $postData['shopOrderNo']     = $_POST['shopOrderNo'];            //状态，0-订单生成 1-支付中 2-支付成功 3-业务处理完成
        $postData['completeDate']    = $_POST['completeDate'];           //支付时间，精确到毫秒
        $postData['payMoney']        = $_POST['payMoney'];       //通知类型 1-前台通知 2-后台通知

        //进行验签
        //排序
        ksort($postData);
        $stringToBeSigned = "";
        foreach ($postData as $key =>$value) {
            $stringToBeSigned .= "{$key}"."{$value}";
        }
        $stringToBeSigned = $stringToBeSigned.$platInfo['platInfo']['appKey'];
        //echo $stringToBeSigned;
        $checkSign = sha1($stringToBeSigned);
        $postData['sign'] = $_POST['sign'];

        if ($checkSign == $postData['sign'] &&  $postData['orderStatus'] == 1){
            echo "success";
            return array('platOrderId' => $postData['orderNo'],'realMoney' => $postData['payMoney'] / 100);
        } else {
            return false;
        }
    }

}
