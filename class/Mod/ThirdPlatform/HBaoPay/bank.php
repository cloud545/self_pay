<?php
class Mod_ThirdPlatform_HBaoPay_bank extends Mod_ThirdPlatform_HBaoPay {

        protected function _do($platInfo = array(), $payInfo = array()){
            if (!$platInfo || !$payInfo) {
                Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
            }

            $type = $platInfo['platInfo']['pt_id'];
            if ($type == 1) {                   // 网银
                $typeCode       = '10';
                $payResource    = '0';
            }else{
               exit("通道错误！");
            }

            $postData = [
                    'shop_no'   => $platInfo['platInfo']['appId'],                        //商户号-平台提供
                    'shop_order_no'     => $payInfo['pOrderId'],                          //商户订单号
                    'shop_order_time' => date("Y-m-d H:i:s"),                     //商户订单时间
                    'trans_type_code' => $typeCode,                               //支付方式
                    'money'     => $payInfo['amount'] * 100,    //交易金额
                    'order_remark'      => 'best1pays',                           //订单备注
                    'order_name'        => 'best1pays',                           //订单名称
                    'callback_url'      => $platInfo['appInfo']['callbackUrl'],                //异步回调地址
                    'pay_resource'      => $payResource,                                 //支付的类型
                    'timestamp'         => time()                                //时间戳
            ];
			
            //排序
            ksort($postData);
            $stringToBeSigned = "";
            foreach ($postData as $key =>$value) {
                $stringToBeSigned .= "{$key}"."{$value}";
            }
            $stringToBeSigned = $stringToBeSigned.$platInfo['platInfo']['appKey'];

            $postData['sign'] = sha1($stringToBeSigned);
            $postData['auth_code'] = $platInfo['platInfo']['appId'];

            BooCurl::setData(json_encode($postData), 'POST');
            BooCurl::setOption(CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json;charset=UTF-8',
            ));
            $response = BooCurl::call($platInfo['platInfo']['p_pay_url']);
            $resData = json_decode($response, true);
            if($resData['code'] != 'R0001')
            {
                echo "支付异常，请联系客服! data: {$resData['desc']}";
                exit;
            }

            BooView::set('payUrl', $resData['data']['payUrl']);
            BooView::display('pay/locationPay.html');
            exit;

        }
}
