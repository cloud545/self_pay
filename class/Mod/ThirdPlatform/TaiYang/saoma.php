<?php
class Mod_ThirdPlatform_TaiYang_saoma extends Mod_ThirdPlatform_TaiYang {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        if ($type == 16) { // 网银支付
            $payType    = '1';
            $scanType   = '3';
        }

        $params = array(
            "merchantId" => $platInfo['platInfo']['appId'], //商户号
            "orderSn"    => $payInfo['pOrderId'], //商家订单号，保证唯一性
            "currency"   => "1",//货币，人民币: CNY, 美元: USD,DOGE-[1:CNY,2:USD,3:HKD,4:BD]
            "amount"     => number_format($payInfo['amount'] * 100,2,'.',''),//金额 1元，单位分
            "notifyUrl" => $platInfo['appInfo']['callbackUrl'], //异步通知回调地址
            "returnUrl" => $platInfo['appInfo']['clientUrl'], //同步通知回调地址
            "extra"     => '', //额外参数
            "goodsName" => 'best1pay', //商品标题
            "goodsDetail" => 'best1pay', //商品描述
            "scanType"  => $scanType, //
            "type"      => $payType, //支付方式（*）-[1:网银,2:扫码,3:快捷,4:代付]
        );

        //组装签名字段
        $sign = $params['merchantId'].$params['orderSn'].$params['currency'].$params['amount'].$params['notifyUrl'].$params['type'].$platInfo['platInfo']['appKey'];
        //MD5签名
        $params['sign'] = md5($sign);

        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, $platInfo['platInfo']['p_pay_url']); // 要访问的地址
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
        curl_setopt($curl, CURLOPT_POSTFIELDS, $params); // Post提交的数据包
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        $result = json_decode(curl_exec($curl),1); // 执行操作

        curl_close($curl); // 关闭CURL会话
        if($result['code'] == 200){
            header('location:'.$result['data']['param']['url']);
        }else{
            exit;
        }

    }

}
