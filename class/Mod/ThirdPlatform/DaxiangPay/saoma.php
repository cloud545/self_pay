<?php
class Mod_ThirdPlatform_DaxiangPay_saoma extends Mod_ThirdPlatform_DaxiangPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 3;
        if ($type == 2) { // 支付宝
            $payType = 1;
        } elseif ($type == 3) { // 微信
            $payType = 2;
        } elseif ($type == 4) { // qq
            $payType = 3;
        } elseif ($type == 5) { // 银联
            $payType = 3;
        }

        $uid = $platInfo['platInfo']['appId']; //商户id
        $orderno = $payInfo['pOrderId']; //订单号码
        $paytype = $payType; //支付类型 1 支付宝  2 微信 3 QQ钱包
        //金额
        $pmoney = $payInfo['amount'];
        $ptime = date('Y-m-d H:i:s'); //支付时间yyyy-MM-dd HH:mm:ss）
        $token = $platInfo['platInfo']['appKey']; //商户key(MD5密钥)
        $attachfield = $payInfo['pOrderId']; //附属字段会原样返回
        $clientip = BooUtil::realIp(); //IP
        $sign = md5($uid .'&'. $orderno .'&'. $paytype .'&'. $pmoney .'&'. $ptime .'&'. $attachfield .'&'. $token); //sign = MD5(Uid + orderno + paytype + pmoney + ptime + MD5秘钥)+为字符串拼接
        $payUrl = $platInfo['platInfo']['p_pay_url']; //支付地址
        $notify_url = $platInfo['appInfo']['callbackUrl']; //支付通知地址接，收请求结果,自定义
        $postData['uid'] = $uid;
        $postData['orderno'] = $orderno;
        $postData['paytype'] = $paytype;
        $postData['pmoney'] = $pmoney;
        $postData['ptime'] = $ptime;
        $postData['sign'] = $sign;
        $postData['notify_url'] = $notify_url;
        $postData['attachfield'] = $attachfield;
        $postData['clientip'] = $clientip;
        $result = $this->send_post($payUrl, $postData);

        if ($result['code'] != '00') {
            echo "支付异常，请联系客服! error: {$result['message']}";
            exit;
        }

        header('Location: ' . $result['message']);
        exit;
    }

}
