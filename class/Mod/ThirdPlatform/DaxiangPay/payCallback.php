<?php
class Mod_ThirdPlatform_DaxiangPay_payCallback extends Mod_ThirdPlatform_DaxiangPay {

	protected function _do($platInfo = array()) {

        $orderid = $_GET["orderno"];          //订单号
        $oState = $_GET["oState"];            //支付状态（1=支付成功，只有成功的才会回调）
        $attachfield = $_GET["attachfield"];  //请求时候的附属字段
        $pmoney = $_GET["pmoney"];            //值金额(单位：元)
        $sign = $_GET["sign"];                //MD5签名
        //校验传入的参数是否格式正确，略
        $token = $platInfo['platInfo']['appKey'];          //商户MD5密钥
        $temps = md5($orderid .'&'. $oState .'&'. $attachfield .'&'. $pmoney .'&'. $token);
        if ($temps == $sign && $oState == 1) {
            echo 'OK';
            return array('platOrderId' => $orderid,'realMoney' => $pmoney);
        } else {
            //签名验证失败
            return false;
        }

	}

}
