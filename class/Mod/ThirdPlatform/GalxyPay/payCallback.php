<?php
class Mod_ThirdPlatform_GalxyPay_payCallback extends Mod_ThirdPlatform_GalxyPay
{
    protected function _do($platInfo = array()){
        $postData = array();
                $appid                  = $_POST['appid'];//商户名称
                $callbacks      = $_POST['callbacks'];//支付时间戳
                $pay_type               = $_POST['pay_type'];//支付状态
                $amount                 = $_POST['amount'];//支付金额
                $success_url    = $_POST['success_url'];//支付时提交的订单信息
                $error_url      = $_POST['error_url'];//平台订单交易流水号
                $out_trade_no   = $_POST['out_trade_no'];//该笔交易手续费用
                $amount_true    = $_POST['amount_true'];//实付金额
                $out_uid                = $_POST['out_uid'];//用户请求uid
                $sign                   = $_POST['sign'];//回调时间戳

                $postData = [
                        'appid'         => $appid,
                        'callbacks'     => $callbacks,
                        'pay_type'              => $pay_type,
                        'amount'        => $amount,
                        'success_url'   => $success_url,
                        'error_url'     => $error_url,
                        'out_trade_no'  => $out_trade_no,
                        'amount_true'   => $amount_true,
                        'out_uid'       => $out_uid,
                        'sign'          => $sign,
                ];
                if ($this->verifySign($postData,$platInfo['platInfo']['appKey']) && $callbacks == 'CODE_SUCCESS'){
                        echo "success";
                        return array('platOrderId' => $postData['out_trade_no'], 'realMoney' => $postData['amount']);
                }else{
                        exit('error:sign');
                        return false;
                }

    }

}
