<?php
class Mod_ThirdPlatform_GalxyPay_payOrder extends Mod_ThirdPlatform_GalxyPay {

	protected function _do($platInfo = array()){

        if (!$platInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $payType = 'd';
        $post = BooVar::requestx();

        $info = array(
            'command' => 'beginpay',
            'merchantid' => $platInfo['platInfo']['appId'],
            'amount' => $post['amount'],
            'tradeno' => $post['pOrderId'],
            'frontjumpurl' => $platInfo['appInfo']['client_url'],
            'paytype' => $payType,
        );
        ksort($info);

        $httpQueryStr = '';
        foreach ($info as $key => $val) {
            if (!$httpQueryStr) {
                $httpQueryStr = "{$key}={$val}";
            } else {
                $httpQueryStr = "&{$key}={$val}";
            }
        }

        $sign = strtoupper(md5($httpQueryStr . $platInfo['platInfo']['appKey']));
        $info['sign'] = $sign;

        BooCurl::setData($info, 'POST');
        $data = BooCurl::call($platInfo['platInfo']['p_pay_url']);

        $return = json_decode($data,true);
        if(is_array($return) && $return['ret'] == 'success'){
            header('Location: ' . $return['payweblink']);
            exit;
        }

        echo "支付异常，请联系客服! error: {$return['info']}";
        exit;
	}

}
