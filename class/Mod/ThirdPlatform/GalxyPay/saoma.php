<?php
class Mod_ThirdPlatform_GalxyPay_saoma extends Mod_ThirdPlatform_GalxyPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];

        $payType =  0;
        if ($type == 2) {       // 支付宝wap
            $payType  = 'alipay';
        } elseif ($type == 9) { // 支付宝扫码
            $payType  = 'alipay';
        } else {
			return false;
            exit;
        }

		$postData = [
			'appid'        => $platInfo['platInfo']['appId'],     //商户ID
			'pay_type'     => $payType,
			'out_trade_no' => $payInfo['pOrderId'],               //订单号
			'amount'       => number_format($payInfo['amount'],2,'.',''),                 //付款金额
			'callback_url' => $platInfo['appInfo']['callbackUrl'],  					//回调地址
			'success_url'  => 'www.baidu.com',
			'error_url'    => 'www.baidu.com',
			'version'      => 'v1.1',
			'out_uid'      => 'dy'.$payInfo['pOrderId'],
		];
		
		//拿APPKEY与请求参数进行签名
		$postData['sign'] = $this->getSign($platInfo['platInfo']['appKey'], $postData);		
		
        $formstr = '';
        foreach ($postData as $key => $val) {
            $formstr .= '<input type="hidden" name="' . $key . '" value="' . $val . '">';
        }
        echo '
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            </head>
            <body onLoad="document.zlinepay.submit();">
                <form name="zlinepay" method="post" action="' . $platInfo['platInfo']['p_pay_url'] . '" target="_top"/>'.$formstr.'</form>
            </body>
        </html>
        ';
        exit;
    }

}
