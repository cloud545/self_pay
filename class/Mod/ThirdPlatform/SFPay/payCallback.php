<?php
class Mod_ThirdPlatform_SFPay_payCallback extends Mod_ThirdPlatform_SFPay {

	protected function _do($platInfo = array()) {

        $notifyData = file_get_contents('php://input');
        if (!$notifyData) {
            return false;
        }

        $postdata = json_decode($notifyData, true);
        if ($this->verify($postdata, $platInfo['platInfo']['appKey'])) {
            echo 'success';
            return array('platOrderId' => $postdata['orderno'], 'realMoney' => $postdata['realprice']);
        } else {
            return false;
        }

	}

}
