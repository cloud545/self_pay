<?php
class Mod_ThirdPlatform_SFPay_saoma extends Mod_ThirdPlatform_SFPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝wap
            $payType = 13;
        } elseif ($type == 3) { // 微信wap
            $payType = 14;
        } elseif ($type == 5) { // 银联
            $payType = 15;
        } elseif ($type == 9) { // 支付宝扫码
            $payType = 13;
        } elseif ($type == 10) { // 微信扫码
            $payType = 14;
        }

        $param = array();
        $param['uid'] = $platInfo['platInfo']['appId'];
        $param['price'] = sprintf('%.2f',$payInfo['amount']); //保留两位小数
        $param['paytype'] = $payType;
        $param['notify_url'] = $platInfo['appInfo']['callbackUrl'];
        $param['return_url'] = $platInfo['appInfo']['clientUrl'];
        $param['user_order_no'] = $payInfo['pOrderId']; //订单号必须唯一和数字

        //获取sign签名串
        $param['sign'] = $this->getSign($param, $platInfo['platInfo']['appKey']);

        //以下参数不参与加密
        $param['note'] = 'my note';
        $param['cuid'] = 'user|email|identity';//可选参数 一般填写用户名 邮箱 或者需要的主键ID
        $param['tm']	= date('Y-m-d H:i:s');

        BooView::set('payUrl', $platInfo['platInfo']['p_pay_url']);
        BooView::set('native', $param);
        BooView::display('pay/formPay.html');
        exit;
    }

}
