<?php
class Mod_ThirdPlatform_SFPay extends Mod_ThirdPlatform {

    protected function getSign($param, $token) {
        $string = '';
        foreach($param as $value)
        {
            $string .= $value;
        }
        return md5($string.$token);
    }

    protected function verify($postdata, $token) {

        $sign = md5($postdata['user_order_no'] . $postdata['orderno'] . $postdata['tradeno'] . $postdata['price'] . $postdata['realprice'] . $token);
        if($postdata['sign'] == $sign)
        {
            return true;
        }
        return false;
    }

}
