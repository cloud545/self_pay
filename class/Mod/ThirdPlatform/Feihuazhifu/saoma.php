<?php
class Mod_ThirdPlatform_Feihuazhifu_saoma extends Mod_ThirdPlatform_Feihuazhifu {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝wap
            $payType = "alipay";
        } elseif ($type == 3) { // 微信wap
            $payType = 'wx';
        } elseif ($type == 5) { // 银联
            $payType = 4001;
        } elseif ($type == 9) { // 支付宝扫码
            $payType = "alipay";
        } elseif ($type == 10) { // 微信扫码
            $payType = 'wx';
        }

        $appid = $platInfo['platInfo']['appId']; //商户号
        $key = $platInfo['platInfo']['appKey']; //用户key
        $type = $payType; //支付方式
        $note = "test"; //备注

        $orderid = $payInfo['pOrderId']; //订单号
        $fee = $payInfo['amount']; //支付金额
        $pay = new qrpay($appid, $key);
        $pay->p02($fee); //金额
        $pay->p09(time()); //日期时间戳
        $pay->p20($orderid); //用户订单号
        $pay->p21($platInfo['appInfo']['callbackUrl']); //支付成功后通知的地址
        $pay->p22($platInfo['appInfo']['clientUrl']); //支付成功跳转页面
        $pay->p23(""); //手机 设备id(硬件id) 不指定随机抽取
        $pay->p03($type); //支付方式
        $pay->getsign();

        $url = http_build_query($pay->DATA);
        $url = "{$platInfo['platInfo']['p_pay_url']}?{$url}";

        BooView::set('payUrl', $url);
        BooView::display('pay/locationPay.html');
        exit;
    }

}
