<?php
class Mod_ThirdPlatform_Feihuazhifu_payCallback extends Mod_ThirdPlatform_Feihuazhifu {

	protected function _do($platInfo = array()) {

        $content = file_get_contents("php://input");
        if ($content) {
            $_REQUEST = json_decode($content, true);
        }

        $appid = $platInfo['platInfo']['appId']; //商户号
        $key = $platInfo['platInfo']['appKey']; //用户key
        $pay = new qrpay($appid, $key);
        $pay->chksign($_REQUEST);

        if ($pay->chksign($_REQUEST)) {
            echo 'success';
            return array('platOrderId' => $_REQUEST['p20'],'realMoney' => $_REQUEST['p02']);
        } else {
            //签名验证失败
            return false;
        }

	}

}
