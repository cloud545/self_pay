<?php
class qrpay
{
    private $APPID;
    private $KEY;
    public $DATA;
    private $URL = "http://www.feihuazhifu.com:8000/api/v1";
    public function __construct($appid, $key)
    {
        $this->APPID = $appid;
        $this->KEY = $key;
        $this->DATA['p01'] = $appid;
    }

    public function __call($name, $value)
    {
        $this->DATA[$name] = $value[0];
    }
    public function chksign($arr)
    {
        $sign = $arr['sign'];
        $tmp = strtoupper(md5($arr['p01'] . $arr['p02'] . $arr['p03'] . $arr['p09'] . $arr['p20'] . $this->DATA['p21'] . $this->DATA['p22'] . $arr['p23'] . $arr['p30'] . $this->KEY));

        if (strtoupper($sign) === $tmp) {
            return true;
        } else {
            return false;
        }
    }
    //加签
    public function getsign()
    {
        $this->DATA['sign'] = strtoupper(md5($this->DATA['p01'] . $this->DATA['p02'] . $this->DATA['p03'] . $this->DATA['p09'] . $this->DATA['p20'] . $this->DATA['p21'] . $this->DATA['p22'] . $this->DATA['p23'] . $this->DATA['p30'] . $this->KEY));
    }
    public function run()
    {
        $this->getsign();
        return $this->request_post($this->URL . "/order/create", $this->DATA);
    }
    public  function request_post($url = '', $post_data = array())
    {
        if (empty($url) || empty($post_data)) {
            return false;
        }
        $o = "";
        foreach ($post_data as $k => $v) {
            $o .= "$k=" . urlencode($v) . "&";
        }
        $post_data = substr($o, 0, -1);
        $postUrl = $url;
        $curlPost = $post_data;
        $ch = curl_init(); //初始化curl
        curl_setopt($ch, CURLOPT_URL, $postUrl); //抓取指定网页
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HEADER, 0); //设置header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_POST, 1); //post提交方式
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        $data = curl_exec($ch); //运行curl
        curl_close($ch);
        return $data;
    }
}