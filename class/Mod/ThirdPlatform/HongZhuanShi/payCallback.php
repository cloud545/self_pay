<?php
class Mod_ThirdPlatform_HongZhuanShi_payCallback extends Mod_ThirdPlatform_HongZhuanShi {

	protected function _do($platInfo = array()) {

        $data = $_POST;
        $sign = $data['sign'];
        unset($data['sign']);
        $arr = [
            'appKey' => $platInfo['platInfo']['appId'],
            'outOrderId' => $data['outOrderId'],
            'orderFund' => $data['orderFund'],
            'orderId' => $data['orderId'],
            'realOrderFund' => $data['realOrderFund'],
        ];
        $md5str = '';
        foreach ($arr as $key => $value) {
            $md5str = $md5str . $key . '=' . $value;
        }
        $signs = md5($md5str.'key='.$platInfo['platInfo']['appKey']);

        if ($sign == $signs) {
            echo 'success';
            return array('platOrderId' => $data['outOrderId'],'realMoney' => $data['realOrderFund']);
        }
        return false;
	}

}
