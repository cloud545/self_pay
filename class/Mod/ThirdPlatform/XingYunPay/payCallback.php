<?php
class Mod_ThirdPlatform_XingYunPay_payCallback extends Mod_ThirdPlatform_XingYunPay {

	protected function _do($platInfo = array()) {
        $data = $_POST;

        $sign = $_POST['sign'];
        unset($data['sign']);
        $data['key'] = $platInfo['platInfo']['appKey'];
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            if($value) {
                $md5str = $md5str . $key . '=' . $value .'&';
            }
        }


        $signs = md5($md5str);

        if ($sign == $signs) {
            echo 'success';
            return array('platOrderId' => $_POST['out_trade_no'],'realMoney' => $_POST['amount']);
        }
        return false;
	}

}
