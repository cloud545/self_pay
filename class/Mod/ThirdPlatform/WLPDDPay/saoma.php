<?php
class Mod_ThirdPlatform_WLPDDPay_saoma extends Mod_ThirdPlatform_WLPDDPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];

        $payType =  0;
        if ($type == 2) {       // 支付宝wap
            $payType = 'ALIPAY-H5';
        } elseif ($type == 3) { // 微信wap
            $payType = 'WECHAT-H5';
        } elseif ($type == 5) { // 银联扫码
            exit;
        } elseif ($type == 9) { // 支付宝扫码
            $payType        = 'ALIPAY-H5';
        } elseif ($type == 10) { // 微信扫码
            $payType        = 'WECHAT-H5';
        }

        //参与验签的数据
        $postData = [
            'referenceCode' => $platInfo['platInfo']['appId'],                                 //商户号，由我司分配
            'orderNo'       => $payInfo['pOrderId'],                                           //订单号，
            'amount'        => $payInfo['amount'] * 100 ,                  						//交易金额：分。10元表示为1000
            'notifyUrl'     => $platInfo['appInfo']['callbackUrl'],                           //结果通知地址。当交易成功或失败时会调⽤此 URL
            'paymentType'   => $payType,                                                        //付款类型标识符。可在后台查看拥有的付款类型
        ];

        //固定金额，如果不满足要求不用推上游通道去
        if(!is_array($payInfo['amount'],array(300,400,500,600,700,800,900,1000,1500,2000,2500,3000,4000,5000)))
        {
            Common_errorCode::jsonEncode(Common_errorCode::NOT_RIGHT_MONEY);
        }

        //按字段的字⺟顺序从到排序
        ksort($postData);
        //验签加密
        ksort($postData);
        $strTem = "amount=".$postData['amount']."&notifyUrl=".$postData['notifyUrl']."&orderNo=".$postData['orderNo']."&paymentType=".$postData['paymentType']."&referenceCode=".$postData['referenceCode'].$platInfo['platInfo']['appKey'];

        $postData["sign"] = strtoupper(md5($strTem));

        BooCurl::setData(json_encode($postData), 'POST');
        BooCurl::setOption(CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json;charset=UTF-8',
        ));
        $res = BooCurl::call($platInfo['platInfo']['p_pay_url']);
        $data = json_decode($res,true);

        if ($data['ok'] != true) {
            echo $data['msg'];
            exit;
        }
        BooView::set('payUrl', $data['url']);
        BooView::display('pay/locationPay.html');
        exit;
    }
}

