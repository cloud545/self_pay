<?php
class Mod_ThirdPlatform_WanJiaPay_saoma extends Mod_ThirdPlatform_WanJiaPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 9) { // 支付宝扫码或者支付宝wap
            $payType = '1';
        } elseif ($type == 2) {
            $payType = '2';
        }

        $data = array(
            'version' => '2',
            'merchant_number' => $platInfo['platInfo']['appId'],
            'cash' => number_format($payInfo['amount'], '2', '.', '') ,
            'server_url' => $platInfo['appInfo']['callbackUrl'],
            'brower_url' => 'http://www.baidu.com',
            'order_id' => $payInfo['pOrderId'],
            'order_time' => time(),
            'pay_type' => $payType,
        );

        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $md5str = $md5str . 'key='.$platInfo['platInfo']['appKey'];
        $data['sign'] = md5($md5str);
        $str='<form class="form-inline" name="payform" method="post" action="'.$platInfo['platInfo']['p_pay_url'].'">';
        foreach ($data as $key => $val) {
            $str.='<input type="hidden" name="' . $key . '" value="' . $val . '">';
        }
        $str.="</form><script>document.forms['payform'].submit();</script>";
        exit($str);
    }
}
