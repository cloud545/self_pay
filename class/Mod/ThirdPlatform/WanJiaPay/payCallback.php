<?php
class Mod_ThirdPlatform_WanJiaPay_payCallback extends Mod_ThirdPlatform_WanJiaPay {

	protected function _do($platInfo = array()) {
        $data = array(
            'merchant_number' => $_POST['merchant_number'],
            'order_id' => $_POST['order_id'],
            'cash' => $_POST['cash'],
            'order_time' => $_POST['order_time'],
            'status' => $_POST['status'],
            'notify_type' => $_POST['notify_type'],
        );

        $md5key  = $platInfo['platInfo']['appKey'];
        $sign = $_POST['sign'];
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $md5str = $md5str . 'key='.$platInfo['platInfo']['appKey'];
        $signs = md5($md5str);

        if ($sign == $signs) {
            echo 'success';
            return array('platOrderId' => $_POST['order_id'],'realMoney' => $_POST['cash']);
        }
        return false;
	}

}
