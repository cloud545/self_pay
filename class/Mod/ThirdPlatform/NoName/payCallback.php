<?php
class Mod_ThirdPlatform_NoName_payCallback extends Mod_ThirdPlatform_NoName {

    protected function _do($platInfo = array()) {
        $data = json_decode(file_get_contents('php://input'), 1);
        $sign = $data['sign'];
        unset($data['sign']);
        $signs = md5($data['order_id'].$data['amount'].$data['amount_ex'].$data['status'].$platInfo['platInfo']['appKey']);
        if ($data['status'] == '2' && $signs == $sign){
            echo 'success';
            return array('platOrderId' => $data['order_id'],'realMoney' => $data['amount_ex']);
        }
    }
}
