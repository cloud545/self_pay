<?php
class Mod_ThirdPlatform_NoName_saoma extends Mod_ThirdPlatform_NoName {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 3) {
            $payType = 'H5';
        }

        if ($type == 10) {
            $payType = 'PC';
        }

        $data = array(
            "order_id" 	=> $payInfo['pOrderId'],
            "user_name" 	=> $platInfo['platInfo']['appId'],
            "amount" 	=> $payInfo['amount'],
            "pay_type" 	=> $payType,
            "pay_channel" 	=> 'ALIPAY',
            "notify_url" 	=> $platInfo['appInfo']['callbackUrl'],
            "pay_card_name" 	=> '',
            "pay_cardno_r4" 	=> '',
        );

        $str = '';
        foreach ($data as $key => $value) {
            $str = $str . $value;
        }
        $data['sign'] = strtoupper(md5($str.$platInfo['platInfo']['appKey']));
        $data = json_encode($data);
        $ch = curl_init($platInfo['platInfo']['p_pay_url']);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data))
        );
        $result = json_decode(curl_exec($ch),1);
        if ($result['success'] == true) {
            BooView::set('money', $result['args']['amount']);
            BooView::set('type', $type);
            BooView::set('url', $result['args']['pay_url']);
            BooView::display('pay/saoma.html');
        } else {
            echo $result['message'];
        }
    }
}
