<?php
class Mod_ThirdPlatform_YYDPay_saoma extends Mod_ThirdPlatform_YYDPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 10) {
            $payType = '8002';
        }

        if ($type == '2') {
            $payType = '8034';
        }

        $data = array(
            'mch_id'          => $platInfo['platInfo']['appId'],
            'app_id'          => $platInfo['platInfo']['appKey'],
            'product_Id'      => $payType,
            'amount'          => $payInfo['amount'] * 100,
            'subject'         => 'best1pay',
            'body'            => 'best1pay',
            'currency'        => 'cny',
            'spbill_create_ip'=> $_SERVER['REMOTE_ADDR'],
            'notify_url'      => $platInfo['appInfo']['callbackUrl'],
            'return_url'      => $platInfo['appInfo']['clientUrl'],
            'out_trade_no'    => $payInfo['pOrderId']
        );
        
        $params="amount=".$data['amount']."&appId=".$data['app_id']."&body=".$data['body']."&clientIp=".$data['spbill_create_ip']."&currency=".$data['currency']."&mchId=".$data['mch_id']."&mchOrderNo=".$data['out_trade_no']."&notifyUrl=".$data['notify_url']."&productId=".$data['product_Id']."&returnUrl=".$data['return_url']."&subject=".$data['subject'];
        $signstr=$params."&key=".$platInfo['platInfo']['appSecret'];

        $sign = md5($signstr);
        $params=$params."&sign=".$sign;
        $url=$platInfo['platInfo']['p_pay_url'];
        $rdata=$this->http_post_data($url."?".$params,'');
        $res=json_decode($rdata,true);

        if($res['retCode'] == 'SUCCESS'){
            exit($res['payParams']['payUrl']);
        }else{
            exit($res['retMsg']);
        }        
    }

    
    public function http_post_data($url, $data_string) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json; charset=utf-8",
            "Content-Length: " . strlen($data_string))
        );
        ob_start();
        curl_exec($ch);
        $return_content = ob_get_contents();
        ob_end_clean();
        $return_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        return $return_content;
    }
}
