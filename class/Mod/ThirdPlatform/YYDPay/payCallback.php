<?php
class Mod_ThirdPlatform_YYDPay_payCallback extends Mod_ThirdPlatform_YYDPay {
    protected function _do($platInfo = array()) {
		$data['income'] = $_POST['income'];
		$data['payOrderId'] = $_POST['payOrderId'];
		$data['amount'] = $_POST['amount'];
		$data['mchId'] = $_POST['mchId'];
		$data['productId'] = $_POST['productId'];
		$data['mchOrderNo'] = $_POST['mchOrderNo'];
		$data['paySuccTime'] = $_POST['paySuccTime'];
		$data['channelOrderNo'] = $_POST['channelOrderNo'];
		$data['backType'] = $_POST['backType'];
		$data['appId'] = $_POST['appId'];
		$data['status'] = $_POST['status'];
		ksort($data);
        $md5Str = '';
        foreach ($data as $key => $value) {
            if($value==='' || $value===null){
                continue;
            }
            if (!$md5Str) {
                $md5Str .= "{$key}={$value}";
            } else {
                $md5Str .= "&{$key}={$value}";
            }
        }
        $signStr = $md5Str."&key=".$platInfo['platInfo']['appSecret'];
        $sign = strtoupper(md5($signStr));
        if ($sign == $_POST['sign'] && $data['status'] == 2) {
            echo 'success';
            return array('platOrderId' => $data['platform_trade_no'],'realMoney' => $data['money'] / 100);
        }
        return false;
    }
}
