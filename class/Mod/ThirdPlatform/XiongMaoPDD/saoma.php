<?php
class Mod_ThirdPlatform_XiongMaoPDD_saoma extends Mod_ThirdPlatform_XiongMaoPDD {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();
        $payType = 0;
        if ($type == 7 || $type == 2) {       // 话费支付宝H5
            $payType = 'zfbwap';
        }

        if ($type == 3 || $type == 6) {
            $payType = 'wxwap';
        }

        $data = array(
            'fxid' => $platInfo['platInfo']['appId'],
            'fxddh' => $payInfo['pOrderId'],
            'fxdesc' => '充值',
            'fxfee' => $payInfo['amount'] ,
            'fxnotifyurl' => $platInfo['appInfo']['callbackUrl'],
            'fxbackurl' => 'https://www.baidu.com',
            'fxpay' => $payType,
            'fxip' => $_SERVER['REMOTE_ADDR'],
        );
        $data['fxsign'] = md5($data['fxid'].$data['fxddh'].$data['fxfee'].$data['fxnotifyurl'].$platInfo['platInfo']['appKey']);
        $data = http_build_query(
            $data
        );
        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => $data
            )
        );
        $context = stream_context_create($opts);
        $result = file_get_contents($platInfo['platInfo']['p_pay_url'], false, $context);
        $result = json_decode($result,1);
        if ($result['status'] == 1) {
            header('location:'.$result['payurl']);
        } else {
            echo $result['error'];
        }

    }
}
