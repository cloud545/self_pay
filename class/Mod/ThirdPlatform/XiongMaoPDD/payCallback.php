<?php
class Mod_ThirdPlatform_XiongMaoPDD_payCallback extends Mod_ThirdPlatform_XiongMaoPDD {

	protected function _do($platInfo = array()) {
        $data = $_POST;

        $sign = $_POST['fxsign'];

        $signs = md5($data['fxstatus'].$data['fxid'].$data['fxddh'].$data['fxfee']. $platInfo['platInfo']['appKey']);

        if ($sign == $signs && $data['fxstatus'] == '1') {
            echo 'success';
            return array('platOrderId' => $_POST['fxddh'],'realMoney' => $_POST['fxfee']);
        }
        return false;
	}
}
