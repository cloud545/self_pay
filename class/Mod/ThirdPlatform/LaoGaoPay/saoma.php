<?php
class Mod_ThirdPlatform_LaoGaoPay_saoma extends Mod_ThirdPlatform_LaoGaoPay {
    protected function _do($platInfo = array(), $payInfo = array()) {
    if (!$platInfo || !$payInfo) {
        Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
    }
    $type = $platInfo['platInfo']['pt_id'];
    $payType  = 0;
    if ($type == 2 || $type == 9) {
        $payType = '3';
    }elseif ($type == 17) {
        $payType = '1';
    }
    else{
        exit("通道错误");
    }
		
    //参与验签的参数
    $postData = [
        'pay_memberid'  	=> $platInfo['platInfo']['appId'],	                  //商户号
        'pay_orderid'  	=> $payInfo['pOrderId'],	                  //订单号
        'pay_amount'               => number_format($payInfo['amount'] ,2,'.',''),	//订单
        'pay_applydate'           => date("Y-m-d H:i:s"),		//提交时间
        'pay_bankcode'  	=> $payType,                                                 //银行编码
        'pay_notifyurl'  	=> $platInfo['appInfo']['callbackUrl'],                   //服务端通知
        'pay_callbackurl'  	=> $platInfo['appInfo']['clientUrl'],	                   //页面跳转通知
    ];
    ksort($postData);
    $md5str = "";
    foreach ($postData as $key => $val) {
        $md5str = $md5str . $key . "=" . $val . "&";
    }
    $postData['pay_md5sign'] = strtoupper(md5($md5str . "key=" . $platInfo['platInfo']['appKey']));
    $postData['pay_attach']          = "best1pays";
    $postData['pay_productname']    = 'best1pays';
        if ($type == 2 || $type == 9) {
            $data = http_build_query(
                $postData
            );
            $opts = array('http' =>
                array(
                    'method' => 'POST',
                    'header' => 'Content-type: application/x-www-form-urlencoded',
                    'content' => $data
                )
            );
            $context = stream_context_create($opts);
            $res = file_get_contents($platInfo['platInfo']['p_pay_url'], false, $context);
            $result = json_decode($res, 1);
            
            if ($result['status'] == 'success') {
                $log = "LaoGaoPay pOrderId={$payInfo['pOrderId']}\t请求成功\n";
                $log = $log."支付地址：{$result["data"]["payurl"]}";
            } else {
                $log = "LaoGaoPay pOrderId={$payInfo['pOrderId']}\t请求失败\n";
                $log = $log."返回信息：{$res}";
            }
            BooLog::pay($log);
            
            if ($result['status'] == 'success') {
                exit("<script>window.location.href='" . $result["data"]["payurl"] . "'</script>");
            }else{
                echo $result['msg'];
            }
        } elseif ($type == 17) {
            $str='<form class="form-inline" name="payform" method="post" action="'.$platInfo['platInfo']['p_pay_url'].'">';
            foreach ($postData as $key => $val) {
                $str.='<input type="hidden" name="' . $key . '" value="' . $val . '">';
            }
            $str.="</form><script>document.forms['payform'].submit();</script>";
            exit($str);
        }
    }
}
