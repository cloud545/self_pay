<?php
class Mod_ThirdPlatform_GuangDongYinLian_payCallback extends Mod_ThirdPlatform_GuangDongYinLian {

	protected function _do($platInfo = array()) {

        $data = json_decode(file_get_contents('php://input'), 1);
        $sign = $data['sign'];
        unset($data['sign']);
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $signs = strtoupper(md5($md5str.'key='.$platInfo['platInfo']['appKey']));

        if ($sign == $signs && $data['resultCode'] == 'success') {
            echo 'SUCCESS';
            return array('platOrderId' => $data['orderNo'],'realMoney' => $data['payPrice']);
        }
        return false;
	}

}
