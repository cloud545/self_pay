<?php
class Mod_ThirdPlatform_LyfPay extends Mod_ThirdPlatform {

    protected function rsaSign($data, $private_key) {
        $search = [
            "-----BEGIN RSA PRIVATE KEY-----",
            "-----END RSA PRIVATE KEY-----",
            "\n",
            "\r",
            "\r\n"
        ];
        $private_key=str_replace($search,"",$private_key);
        $private_key=$search[0] . PHP_EOL . wordwrap($private_key, 64, "\n", true) . PHP_EOL . $search[1];
        $res=openssl_get_privatekey($private_key);
        if($res)
        {
            openssl_sign($data, $sign, $res, MD5);
            openssl_free_key($res);
        }else {
            exit("私钥格式有误");
        }
        $sign = base64_encode($sign);
        return $sign;
    }

    protected function rsaCheck($data, $public_key, $sign)  {
        $search = [
            "-----BEGIN PUBLIC KEY-----",
            "-----END PUBLIC KEY-----",
            "\n",
            "\r",
            "\r\n"
        ];
        $public_key=str_replace($search,"",$public_key);
        $public_key=$search[0] . PHP_EOL . wordwrap($public_key, 64, "\n", true) . PHP_EOL . $search[1];
        $res=openssl_get_publickey($public_key);
        if($res)
        {
            $result = (bool)openssl_verify($data, base64_decode($sign), $res, MD5);
            openssl_free_key($res);
        }else{
            exit("公钥格式有误!");
        }
        return $result;
    }

}
