<?php
class Mod_ThirdPlatform_LyfPay_payCallback extends Mod_ThirdPlatform_LyfPay {

	protected function _do($platInfo = array()) {


        $postData = $_REQUEST;
        $signStr = "memCode=".$postData['memCode']."&orderNo=".$postData['orderNo']."&state=".$postData['state']."&amount=".$postData['amount'];
        $key = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDg25SJVXLF22XXDN7KTimNEOlu0oum5c3uSL8+C3lUtvQrIHIzF96IOwkwcInumBoqtySWn/oNbKYA5iPEXqtihAVDTmYkiEJ/in1yR2hyK2z8hMtgY6vIz0TEmcl1heuevjYE9XSePEzjKz78/qs45WcxLLBlZ4m4eDthVPlLrQIDAQAB';
        $rs = $this->rsaCheck($signStr, $key, $_REQUEST['sign']);

        if ($rs && $postData['state'] == 1){
            echo "success";
            return array('platOrderId' => $postData['orderNo'], 'realMoney' => $postData['amount']);
        } else {
            return false;
        }

	}

}
