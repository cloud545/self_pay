<?php
class Mod_ThirdPlatform_LyfPay_saoma extends Mod_ThirdPlatform_LyfPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝wap
            $payType = '0005';
        } elseif ($type == 3) { // 微信wap
            $payType = '0006';
        } elseif ($type == 5) { // 银联扫码
            $payType = '0008';
        } elseif ($type == 9) { // 支付宝扫码
            $payType = '0001';
        } elseif ($type == 10) { // 微信扫码
            $payType = '0004';
        }

        $postData = array(
            'memCode' => $platInfo['platInfo']['appId'],
            'channel' => $payType,
            'amount' => $payInfo['amount'],
            'orderNo' => $payInfo['pOrderId'],
            'notifyUrl' => urlencode($platInfo['appInfo']['callbackUrl']),
            'returnUrl' => $platInfo['appInfo']['clientUrl'],
            'remark' => '',
        );

        $signStr = "memCode=".$postData['memCode']."&channel=".$postData['channel']."&amount=".$postData['amount']."&orderNo=".$postData['orderNo']."&notifyUrl=".$postData['notifyUrl'];
        $key = 'MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAJ5Cwd3H9cnomNyHZ17O1LV+2ctulnUN0Y+vbuA42OyF69ruf7Cjkku9NvEs+3M3x+Wubz8IAHB1mLLkxEevo20/e1pu3oApXJjjrDOFHQ8cIXPaADlGUZAQOR/+OqLO0S68cfwdD0C2c+Vm1LN6kgba6Q70nNKg+AWfT2/x81WFAgMBAAECgYEAkX27rkD649DSXsKYNF+eyu/AF7Yl2ZyHkDlui/GW8vI3HsDtYJ0vjDN6BAQfcg/Q1IUlPmQDy19Arw2CqIh+iyAPomd0MmaYCrFYwpx2yDKmB1HFHWAATFOjpklkkrcOKdY6xR9A0xD4BbGyxTsm6uH1Oci1IXoeue9+d/xfmcECQQD8h/qu8g25g3HDhXGxEA5daOBDq26G7wVIHWXsXcwhPgeOyMrspw3vizxrrvBHkEbmugSV4AGg2uicyceyz2ExAkEAoG9HMbS3oiAL9tCbsTpxibr4BtgBeYLO0UTyb/DrR2cwUhOonu3JXit2/gsat4eAMGrm6G2HVpBN62bBlqsElQJBAKuZ43k6NqVXctkANw+w73H+MFcLiWHbzmUAxR/nZS05r30RaQODSW772KT8DX8zxVpIEZGe1LxvxR216CPT1CECQAildqTLJIpsR7jJjVqD19KgbPWRdqLh1duZZ6d4yHVUrln+DTxjfFLC7OrMFmiFuPbjDAbCwGR96nUhWJWSKfECQF+0IJEiZt8iNU+t4SnMqK4aKp6e/3VASc6jrxGww/HgTFqdj6ySvjJLAJuRyM/9GJnVc0Iavsw7hK4TGlKb5NE=';
        $sign = $this->rsaSign($signStr, $key);
        $postData['sign'] = $sign;

        BooCurl::setData($postData, 'POST');
        $res = BooCurl::call($platInfo['platInfo']['p_pay_url']);
        $data = json_decode($res, true);
        if (!$data) {
            echo $res;
            exit;
        }

        BooView::set('payUrl', $data['data']);
        BooView::display('pay/locationPay.html');
        exit;
    }

}
