<?php
class Mod_ThirdPlatform_QRCodePay_payCallback extends Mod_ThirdPlatform_QRCodePay {

	protected function _do($platInfo = array()) {
        $data = $_POST;

        $sign = $_POST['sign'];
        unset($data['sign']);
        ksort($data);

        $signs= md5(http_build_query($data).$platInfo['platInfo']['appKey']);

        if ($sign == $signs) {
            echo 'success';
            return array('platOrderId' => $_POST['merchant_order_sn'],'realMoney' => $_POST['amount']);
        }
        return false;
	}
}
