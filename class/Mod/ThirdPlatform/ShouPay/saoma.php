<?php
class Mod_ThirdPlatform_ShouPay_saoma extends Mod_ThirdPlatform_ShouPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];

        $payType =  0;
        if ($type == 2) {       // 支付宝wap
            $payType = '9007';
        } elseif ($type == 3) { // 微信wap
            $payType = '2';
        } elseif ($type == 5) { // 银联扫码
            $payType    = '926';
        } elseif ($type == 9) { // 支付宝扫码
            $payType        = '9006';
        } elseif ($type == 10) { // 微信扫码
            $payType        = '2';
        }


        //参与验签的参数
        $postData = array(
            'pay_memberid'  	=> $platInfo['platInfo']['appId'],                  //商户ID
            'pay_orderid'  		=> $payInfo['pOrderId'],             				//订单号
            'pay_amount'  		=> $payInfo['amount'],                  			//交易金额
            'pay_applydate'  	=> date("Y-m-d H:i:s"),                 			//订单时间
            'pay_bankcode'  	=> $payType,                 						//银行编码
            'pay_notifyurl'  	=> $platInfo['appInfo']['callbackUrl'],             //服务端返回地址
            'pay_callbackurl'  	=> $platInfo['appInfo']['clientUrl'],               //页面跳转返回地址
        );
		
		//验签加密
		$md5str = "";
		foreach ($postData as $key => $val) {
			$md5str = $md5str . $key . "=" . $val . "&";
		}
		//echo($md5str . "key=" . $Md5key);
		$postData["pay_md5sign"] = strtoupper(md5($md5str . "key=" . $platInfo['platInfo']['appKey']));
		
        //非验签参数
        $postData["pay_attach"]    = '1234|456';                                	//?????
        $postData["pay_productname"]    = 'we are best1pays';         				//商品名称

		$formstr = '';
        foreach ($postData as $key => $val) {
            $formstr .= '<input type="hidden" name="' . $key . '" value="' . $val . '">';
        }
		
		echo '
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            </head>
            <body onLoad="document.zlinepay.submit();">
                <form name="zlinepay" method="post" action="' . $platInfo['platInfo']['p_pay_url'] . '" target="_top"/>'.$formstr.'</form>
            </body>
        </html>
        ';
        exit;
    }

}
