<?php
class Mod_ThirdPlatform_OnePDDPay_payCallback extends Mod_ThirdPlatform_OnePDDPay {

	protected function _do($platInfo = array()) {

        $data = $_POST;
        $sign = $data['sign'];
        unset($data['sign']);
        $signs = $this->sign($data, $platInfo['platInfo']['appKey']);

        if ($sign == $signs && $data['callbacks'] == 'CODE_SUCCESS') {
            echo 'success';
            return array('platOrderId' => $data['api_order_sn'],'realMoney' => $data['total']);
        }
        return false;
	}

    public function sign($params = [], $secret = '')
    {
        unset($params['sign']);
        ksort($params);
        $str = '';
        foreach ($params as $k => $v) {

            $str = $str . $k . $v;
        }
        $str = $secret . $str . $secret;
        return strtoupper(md5($str));
    }
}
