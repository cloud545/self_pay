<?php
class Mod_ThirdPlatform_OnePDDPay_saoma extends Mod_ThirdPlatform_OnePDDPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;

        if ($type == 2 || $type == 7) {
            $payType = 'alipay';
        }

        if($type == 3 || $type == 6) {
            $payType = 'wechat';
        }

        $data = array(
            'client_id' => $platInfo['platInfo']['appId'],
            'api_order_sn' => $payInfo['pOrderId'],
            'total' => $payInfo['amount'],
            'type' => $payType,
            'notify_url' => $platInfo['appInfo']['callbackUrl'],
            'timestamp' => $this->getMillisecond(),
        );
        $data['sign'] = $this->sign($data,$platInfo['platInfo']['appKey']);

        $data=str_ireplace("\/","/",json_encode($data));
        $opts=array(
            'http'=>array(
                'method'=>'POST',
                "timeout"=>10,
                'header'=>"Content-type: application/json\r\n".
                    "Content-Length: ".strlen($data)."\r\n",
                'content'=>$data
            ),
        );
        $context=stream_context_create($opts);
        $html=file_get_contents($platInfo['platInfo']['p_pay_url'],false,$context);
        $result = json_decode($html, true);


        if ($result['status'] == '1') {
            header("location:".$result['data']['h5_url']);
        } else {
            echo $result['msg'];
        }
    }

    public function getMillisecond() {

        list($t1, $t2) = explode(' ', microtime());

        return (float)sprintf('%.0f',(floatval($t1)+floatval($t2))*1000);

    }

    public function sign($params = [], $secret = '')
    {
        unset($params['sign']);
        ksort($params);
        $str = '';
        foreach ($params as $k => $v) {

            $str = $str . $k . $v;
        }
        $str = $secret . $str . $secret;
        return strtoupper(md5($str));
    }
}
