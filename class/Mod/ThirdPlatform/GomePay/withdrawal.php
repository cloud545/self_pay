<?php
require_once 'm2base.php';

class Mod_ThirdPlatform_GomePay_withdrawal extends Mod_ThirdPlatform_GomePay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        $post = BooVar::requestx();
        $appKey = json_decode($platInfo['platInfo']['appKey'], true);
        $appSecret = json_decode($platInfo['platInfo']['appSecret'], true);

        $param = array();
        $param['merchant_number'] = $platInfo['platInfo']['appId'];
        $param['order_number'] = $payInfo['pOrderId'];
        $param['wallet_id'] = $appKey['wallet_id'];
        $param['asset_id'] = $appKey['asset_id'];
        $param['business_type'] = 1;
        $param['money_model'] = 1;
        $param['source'] = 0;
        $param['password_type'] = '02';
        $param['encrypt_type'] = '02';
        $param['pay_password'] = $appKey['pay_password'];
        $param['customer_type'] = '01';
        $param['customer_name'] = $post['accountName'];
        $param['account_number'] = $post['cardNo'];
        $param['currency'] = "CNY";
        $param['amount'] = number_format($post['amount'],2,'.','');
        $param['async_notification_addr'] = $platInfo['appInfo']['callbackUrl'];

        $param['app_code'] = $appSecret['app_code'];			//应用号
        $param['app_version'] = '1.0.0';					//应用版本
        $param['service_code'] = $appSecret['service_code'];		//服务号
        $param['plat_form'] = '01';						//平台
        $param['login_token']='';						//登录令牌
        $param['req_no'] = date('YmdHis',time());			//请求流水号
        $param['payment_channel_list']=array();

        $m2 = new m2base ($appSecret['aid'], $appSecret['key']);
        $data = $m2->url_data ( 'PAYING', $param, "POST" );

        $return = json_decode($data,true);


        if (is_array($return)) {
            if ($return['op_ret_code'] != '000') {
                $status = 2;
            } else {
                $status = 0;
            }
            return array('platOrderId' => $return['orderid'], 'status' => $status, 'msg' => "op_ret_code: {$return['op_ret_code']}，op_err_msg: {$return['op_err_msg']}");
        } else {
            return array('platOrderId' => 0, 'status' => 2, 'msg' => "op_ret_code: {$return['op_ret_code']}，op_err_msg: {$return['op_err_msg']}");
        }

    }

}
