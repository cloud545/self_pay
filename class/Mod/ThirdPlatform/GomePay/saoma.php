<?php
class Mod_ThirdPlatform_GomePay_saoma extends Mod_ThirdPlatform_GomePay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = '';
        if ($type == 2) { // 支付宝
            $payType = 'ALIPAY';
        } elseif ($type == 3) { // 微信
            $payType = 'WECHAT';
        } elseif ($type == 4) { // qq
            $payType = 'QQSCAN';
        } elseif ($type == 5) { // 银联
            $payType = 'UNIONPAY';
        } elseif ($type == 6) { // 京东
            $payType = 'JDSCAN';
        }

        $post = BooVar::requestx();

        $pay = array();
        $pay['version'] = "v1";
        $pay['merchant_no'] = $platInfo['platInfo']['appId'];
        $pay['order_no'] = $post['pOrderId'];
        $pay['goods_name'] = base64_encode("充值");
        $pay['order_amount'] = number_format($post['amount'],2,'.','');
        $pay['backend_url'] = $platInfo['appInfo']['callbackUrl'];
        $pay['frontend_url'] = $platInfo['appInfo']['clientUrl'];
        $pay['reserve'] = '';
        $pay['pay_mode'] = "09";
        $pay['bank_code'] = $payType;
        $pay['card_type'] = "0";

        //MD5签名
        $src = "version={$pay['version']}&merchant_no={$pay['merchant_no']}&order_no={$pay['order_no']}&goods_name={$pay['goods_name']}&order_amount={$pay['order_amount']}&backend_url={$pay['backend_url']}&frontend_url={$pay['frontend_url']}&reserve={$pay['reserve']}&pay_mode={$pay['pay_mode']}&bank_code={$pay['bank_code']}&card_type={$pay['card_type']}&key={$platInfo['platInfo']['appKey']}";
        $pay['sign'] = md5($src);

        $formstr = '';
        foreach ($pay as $key => $val) {
            if (!$formstr) {
                $formstr = "{$key}={$val}";
            } else {
                $formstr = "&{$key}={$val}";
            }
        }

        BooCurl::setData($formstr, 'POST');
        $data = BooCurl::call($platInfo['platInfo']['p_pay_url']);
        $return = json_decode($data,true);

        if(is_array($return) && $return['result_code'] == '00'){
            BooView::set('url', $return['code_url']);
            BooView::display('pay/saoma.html');
            exit;
        }

        echo "支付异常，请联系客服! error: {$data}";
        exit;
    }

}
