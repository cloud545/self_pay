<?php
/**
 * 获取签名数据
 * @param array $params
 * @return string
 */
function get_dstbdatasign($bonuse_key, $params) {
	
	$sign = http_build_query($params);
	
	$des = new DES_JAVA ( $bonuse_key );
	
	$dstbdatasign = $des->encrypt ( $sign );
	
	return $dstbdatasign;
}

/**
 * 获取m2传输需要的数据
 * @param array $params
 * @return json
 */
function get_params($bonuse_key, $params) {
	
	$dstbdatasign = get_dstbdatasign ($bonuse_key, $params );
	
	$params ['dstbdatasign'] = $dstbdatasign;
	
	return json_encode ( $params );
}
/**
 * @param array $params
 * @param str $dstbdatasign
 * @return boolean
 */
function verify_sign($bonuse_key, $params, $return_sign){
	$sign = "";
	if (isset($params ['dstbdata']) && !is_null($params ['dstbdata']) && $params ['dstbdata'] != '') {
		$sign = $params ['dstbdata'];
	}
	
	$des = new DES_JAVA ( $bonuse_key );
	
	$dstbdatasign = $des->encrypt ( $sign );

	if($dstbdatasign == $return_sign){
		return true;
	}else{
		return false;
	}

}


/*
 * des加密类
 */
class DES_JAVA {
	var $key;
	function DES_JAVA($key) {
		$this->key = $key;
	}
	function encrypt($encrypt) {
		$encrypt = $this->pkcs5_pad ( $encrypt );
		$iv = mcrypt_create_iv ( mcrypt_get_iv_size ( MCRYPT_DES, MCRYPT_MODE_ECB ), MCRYPT_RAND );
		$passcrypt = mcrypt_encrypt ( MCRYPT_DES, $this->key, $encrypt, MCRYPT_MODE_ECB, $iv );
		return strtoupper ( bin2hex ( $passcrypt ) );
	}
	function decrypt($decrypt) {
		// $decoded = base64_decode($decrypt);
		$decoded = pack ( "H*", $decrypt );
		$iv = mcrypt_create_iv ( mcrypt_get_iv_size ( MCRYPT_DES, MCRYPT_MODE_ECB ), MCRYPT_RAND );
		$decrypted = mcrypt_decrypt ( MCRYPT_DES, $this->key, $decoded, MCRYPT_MODE_ECB, $iv );
		return $this->pkcs5_unpad ( $decrypted );
	}
	function pkcs5_unpad($text) {
		$pad = ord ( $text {strlen ( $text ) - 1} );
		
		if ($pad > strlen ( $text ))
			return $text;
		if (strspn ( $text, chr ( $pad ), strlen ( $text ) - $pad ) != $pad)
			return $text;
		return substr ( $text, 0, - 1 * $pad );
	}
	function pkcs5_pad($text) {
		$len = strlen ( $text );
		$mod = $len % 8;
		$pad = 8 - $mod;
		return $text . str_repeat ( chr ( $pad ), $pad );
	}
}

?>