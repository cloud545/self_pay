<?php
class Mod_ThirdPlatform_OdPay_saoma extends Mod_ThirdPlatform_OdPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝wap
            $payType = '1379';
        } elseif ($type == 3) { // 微信wap
            $payType = '1399';
        } elseif ($type == 5) { // 银联扫码
            $payType = '';
        } elseif ($type == 9) { // 支付宝扫码
            $payType = '1379';
        } elseif ($type == 10) { // 微信扫码
            $payType = '1399';
        }

        $postData = array();
        $postData['DOMAINNAME'] = 'm.odpay.net';
        $postData['CLOUDID'] = $platInfo['platInfo']['appId'];
        $postData['CLOUDCODESIGN'] = $platInfo['platInfo']['appKey'];
        $postData['ACCOUNTINFO'] = $payInfo['pOrderId'];
        $postData['PAYAMOUNT'] = number_format($payInfo['amount'],2,'.','');
        $postData['CALLBACKURL'] = $platInfo['appInfo']['callbackUrl'];
        $postData['CALLBACKPAGEURL'] = $platInfo['appInfo']['clientUrl'];
        $postData['USERCODE'] = $payType;

        $md5Str = "{$postData['ACCOUNTINFO']}{$postData['DOMAINNAME']}{$postData['CLOUDID']}";
        $sign = md5(md5($md5Str) . "{$platInfo['platInfo']['appKey']}");
        $postData['SIGN'] = substr($sign, 8, 16);

        BooCurl::setData($postData, 'POST');
        $response = BooCurl::call($platInfo['platInfo']['p_pay_url']);

        $result = json_decode($response, true);
        if ($result['success'] != 'true' && $result['code'] != '200') {
            BooFile::write('/tmp/odpay.log', $response . "\n{$payInfo['amount']}\n{$platInfo['appInfo']['name']}\n111\n" . date('Y-m-d H:i:s') . "\n\n", 'a');
            echo "支付异常，请联系客服! success：{$result['ErrorCode']}，code：{$result['code']}，remark: {$result['remark']}";
            exit;
        }

        BooView::set('payUrl', $result['result']['pageUrl']);
        BooView::display('pay/locationPay.html');

        exit;
    }

}
