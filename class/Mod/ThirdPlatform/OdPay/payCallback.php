<?php
class Mod_ThirdPlatform_OdPay_payCallback extends Mod_ThirdPlatform_OdPay {

	protected function _do($platInfo = array()) {

        $content = file_get_contents("php://input");
        BooFile::write('/tmp/odpaycallback.log', json_encode($_REQUEST) . "\n" . date('Y-m-d H:i:s') . "\n{$content}\n\n\n", 'a');

        $postData = array();
        $postData['CLOUDID'] = $_REQUEST['CLOUDID'];
        $postData['CLOUDCODESIGN'] = $_REQUEST['CLOUDCODESIGN'];
        $postData['ACCOUNTINFO'] = $_REQUEST['ACCOUNTINFO'];
        $postData['ADDACCOUNTMONEY'] = $_REQUEST['ADDACCOUNTMONEY'];
        $postData['DOMAINNAME'] = 'm.odpay.net';

        $md5Str = "{$postData['ACCOUNTINFO']}{$postData['ADDACCOUNTMONEY']}{$postData['DOMAINNAME']}{$postData['CLOUDID']}";
        $sign = substr(md5(md5($md5Str) . "{$platInfo['platInfo']['appKey']}"), 8, 16);
        if ($_REQUEST['SIGN'] == $sign) {
            echo 'success';
            return array('platOrderId' => $_REQUEST['ACCOUNTINFO'], 'realMoney' => $_REQUEST['ADDACCOUNTMONEY']);
        } else {
            //签名验证失败
            return false;
        }

	}

}
