<?php
class Mod_ThirdPlatform_PingGuoPay_payCallback extends Mod_ThirdPlatform_PingGuoPay {

	protected function _do($platInfo = array()) {
        $data = $_POST;

        $sign = $_POST['SIGN'];
        unset($data['SIGN']);
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $signs = md5(substr($md5str,0 , strlen($md5str)-1) . $platInfo['platInfo']['appKey']);

        if ($sign == $signs && $data['TRDSTATUS'] == '0') {
            echo 'SUCCESS';
            return array('platOrderId' => $_POST['AGTORDID'],'realMoney' => $_POST['TRDFINMONEY']);
        }
        return false;
	}
}
