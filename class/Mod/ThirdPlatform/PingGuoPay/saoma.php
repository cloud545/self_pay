<?php
class Mod_ThirdPlatform_PingGuoPay_saoma extends Mod_ThirdPlatform_PingGuoPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();
        $payType = 0;
        if ($type == 4 || $type == 2) {       // 话费支付宝H5
            $payType = '6';
        } elseif ($type == 3) { // 微信wap
            $payType = '4';
		}
		
        $data = array(
            'CMDID' => 'A_PAYURL',
            'AGTID' => $platInfo['platInfo']['appId'],
            'AGTORDID' => $payInfo['pOrderId'],
            'PAYTYPE' => $payType,
            'PARVALUE' => $payInfo['amount'] ,
            'CRTTIME' => date('YmdHis'),
            'NTFURL' => $platInfo['appInfo']['callbackUrl'],
            'USERID' => '1111',
        );
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $data['SIGN'] = md5(substr($md5str, 0, strlen($md5str) - 1).$platInfo['platInfo']['appKey']);
        $data = http_build_query(
            $data
        );
        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => $data
            )
        );
        $context = stream_context_create($opts);
        $result = file_get_contents($platInfo['platInfo']['p_pay_url'], false, $context);
        $result = json_decode($result,1);
        if ($result['STATUS'] == '0') {
            header("location:".$result['PAYURL']);
        }
    }
}
