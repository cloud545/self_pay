<?php
class Mod_ThirdPlatform_FuQiQi_saoma extends Mod_ThirdPlatform_FuQiQi {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 9 || $type == 2) {
            $payType = 'alipay';
        } 

        $pay_memberid  = $platInfo['platInfo']['appId'];   //商户后台API管理获取
        $pay_orderid   = $payInfo['pOrderId'];    //订单号
        $pay_amount    = number_format($payInfo['amount'],2,'.','');    //交易金额
        $pay_notifyurl = $platInfo['appInfo']['callbackUrl'];   //服务端返回地址
        $pay_callbackurl = $platInfo['appInfo']['clientUrl'];  //页面跳转返回地址
        $Md5key        = $platInfo['platInfo']['appKey'];   //商户后台API管理获取

        $native = array(
            "appid" 	=> $pay_memberid,
            "pay_type" 	=> $payType,
            "out_trade_no" 	=> $pay_orderid,
            "amount" 	=> $pay_amount,
            "callback_url" 	=> $pay_notifyurl,
            "success_url" 	=> $pay_callbackurl,
            "error_url" 	=> $pay_callbackurl,
            "version" 	=> 'v1.1',
            "out_uid" 	=> 'best1pay',
        );

        $native['sign'] = $this->getSign($Md5key, $native);

        $str='<form class="form-inline" name="payform" method="post" action="'.$platInfo['platInfo']['p_pay_url'].'">';
        foreach ($native as $key => $val) {
            $str.='<input type="hidden" name="' . $key . '" value="' . $val . '">';
        }
        $str.="</form><script>document.forms['payform'].submit();</script>";
        exit($str);
    }

    public function getSign($secret, $data){
        // 去空
        $data = array_filter($data);

        //签名步骤一：按字典序排序参数
        ksort($data);
        $string_a = http_build_query($data);
        $string_a = urldecode($string_a);

        //签名步骤二：在string后加入mch_key
        $string_sign_temp = $string_a . "&key=" . $secret;

        //签名步骤三：MD5加密
        $sign = md5($string_sign_temp);

        // 签名步骤四：所有字符转为大写
        $result = strtoupper($sign);

        return $result;
    }

}
