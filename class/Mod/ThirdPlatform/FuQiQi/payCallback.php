<?php
class Mod_ThirdPlatform_FuQiQi_payCallback extends Mod_ThirdPlatform_FuQiQi {

    protected function _do($platInfo = array()) {
        //商户名称
        $appid  = $_POST['appid'];
        //支付时间戳
        $callbacks  = $_POST['callbacks'];
        //支付状态
        $pay_type  = $_POST['pay_type'];
        //支付金额
        $amount  = $_POST['amount'];
        //支付时提交的订单信息
        $success_url  = $_POST['success_url'];
        //平台订单交易流水号
        $error_url  = $_POST['error_url'];
        //该笔交易手续费用
        $out_trade_no  = $_POST['out_trade_no'];
        //实付金额
        $amount_true  = $_POST['amount_true'];
        //用户请求uid
        $out_uid  = $_POST['out_uid'];
        //回调时间戳
        $sign  = $_POST['sign'];

        $data = [
            'appid'         => $appid,
            'callbacks'     => $callbacks,
            'pay_type'      => $pay_type,
            'amount'        => $amount,
            'success_url'   => $success_url,
            'error_url'     => $error_url,
            'out_trade_no'  => $out_trade_no,
            'amount_true'   => $amount_true,
            'out_uid'       => $out_uid,
            'sign'          => $sign,
        ];

        $md5key = $platInfo['platInfo']['appKey'];

        $status = $this->getStatus($data['appid'], $data['out_trade_no'], $md5key);

        if ($this->verifySign($data,$md5key) && $status == 'ok'){
            echo 'success';
            return array('platOrderId' => $out_trade_no,'realMoney' => $amount_true);
        }
    }

    /**
     * @Note  生成签名
     * @param $secret   商户密钥
     * @param $data     参与签名的参数
     * @return string
     */
    public function getSign($secret, $data)
    {

        // 去空
        $data = array_filter($data);

        //签名步骤一：按字典序排序参数
        ksort($data);
        $string_a = http_build_query($data);
        $string_a = urldecode($string_a);

        //签名步骤二：在string后加入mch_key
        $string_sign_temp = $string_a . "&key=" . $secret;

        //签名步骤三：MD5加密
        $sign = md5($string_sign_temp);

        // 签名步骤四：所有字符转为大写
        $result = strtoupper($sign);

        return $result;
    }


    /**
     * @Note   验证签名
     * @param $data
     * @param $orderStatus
     * @return bool
     */
    public function verifySign($data, $secret) {
        // 验证参数中是否有签名
        if (!isset($data['sign']) || !$data['sign']) {
            return false;
        }
        // 要验证的签名串
        $sign = $data['sign'];
        unset($data['sign']);
        // 生成新的签名、验证传过来的签名
        $sign2 = $this->getSign($secret, $data);

        if ($sign != $sign2) {
            return false;
        }
        return true;
    }


    public function getStatus($appid, $order , $appKey) {
        $data['appid'] = $appid;
        $data['out_trade_no'] = $order;
        $data['sign'] = $this->getSign($appKey, $data);
        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, 'https://api.paykiki.com/index/getorder'); // 要访问的地址
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        $result = json_decode(curl_exec($curl),1);
        file_put_contents('test.txt', json_encode($result));
        curl_close($curl); // 关闭CURL会话
        if ($result['data'][0]['status'] == '4') {
            return 'ok';
        }
    }

}
