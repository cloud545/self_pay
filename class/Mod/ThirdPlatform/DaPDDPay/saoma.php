<?php
class Mod_ThirdPlatform_DaPDDPay_saoma extends Mod_ThirdPlatform_DaPDDPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();
        $payType = 0;

        if ($type == 7 || $type == 2) {
            $payType = '1';
        }
        if ($type == 6 || $type == 3) {
            $payType = '2';
        }


        $data = array(
            'merchno' => $platInfo['platInfo']['appId'],
            'payType' => $payType,
            'traceno' => $payInfo['pOrderId'],
            'amount' => number_format($payInfo['amount'],'2','.',''),
            'notifyUrl' => $platInfo['appInfo']['callbackUrl'],
        );
        ksort($data);
        $src='';
        foreach ($data as $x => $x_value){
            if ($x_value != null){
                $src = $src.$x."=".iconv('UTF-8','GBK//IGNORE',$x_value)."&"; //采用本方法进行GBK编码
            }
        }
        $md5=md5($src.$platInfo['platInfo']['appKey']);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $platInfo['platInfo']['p_pay_url']);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $src.'signature'.'='.$md5);
        $result = curl_exec($curl);
        curl_close($curl);
        $result = json_decode(iconv('GBK//IGNORE', 'UTF-8', $result), true);
        if ($result['respCode'] == '00') {
            header("location:".$result['barCode']);
        } else {
            echo $result['message'];
        }
    }
}
