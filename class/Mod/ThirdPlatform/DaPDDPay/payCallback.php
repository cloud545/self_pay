<?php
class Mod_ThirdPlatform_DaPDDPay_payCallback extends Mod_ThirdPlatform_DaPDDPay {

	protected function _do($platInfo = array()) {
        $data = $_POST;
        $sign = $data['signature'];
        unset($data['signature']);
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            if ($value) {
                $md5str = $md5str . $key . '=' . $value .'&';
            }
        }
        $signs = strtoupper(md5($md5str.$platInfo['platInfo']['appKey']));

        if ($sign == $signs && $data['status'] == '1') {
            echo 'success';
            return array('platOrderId' => $data['traceno'],'realMoney' => $data['amount']);
        }
        return false;
	}
}
