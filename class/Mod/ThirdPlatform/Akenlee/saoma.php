<?php
class Mod_ThirdPlatform_Akenlee_saoma extends Mod_ThirdPlatform_Akenlee {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = '';
        if ($type == 2) { // 支付宝wap

        } elseif ($type == 3) { // 微信wap

        } elseif ($type == 9) { // 支付宝扫码

        } elseif ($type == 10) { // 微信扫码

        }

        $info = array(
            'version'=>'1.0',
            'charset'=> 'UTF-8',
            'sign_type'=>'MD5',
            'mch_id'=>$platInfo['platInfo']['appId'],
            'out_trade_no'=>$payInfo['pOrderId'],
            'body'=> "商品",
            'total_fee'=>$payInfo['amount'] * 100,
            'mch_create_ip'=>BooUtil::realIp(),
            'notify_url'=>$platInfo['appInfo']['callbackUrl'],
            'nonce_str'=>md5(time() . rand(1, 100)),
        );

        ksort($info);
        $md5Str = '';
        foreach ($info as $key => $val) {

            if (!$val) {
                continue;
            }

            if (!$md5Str) {
                $md5Str = "{$key}={$val}";
            } else {
                $md5Str .= "&{$key}={$val}";
            }
        }

        $sign = strtoupper(md5($md5Str . "&key=" . $platInfo['platInfo']['appKey']));
        $info['sign'] = $sign;

        $xml = "<xml>";
        foreach ($info as $key => $val) {
            if (is_numeric($val)) {
                $xml .= "<" . $key . ">" . $val . "</" . $key . ">";
            } else {
                $xml .= "<" . $key . "><![CDATA[" . $val . "]]></" . $key . ">";
            }
        }
        $xml .= "</xml>";

        /*
        $options = array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-type:application/xml',
                'content' => $xml,
                'timeout' => 1 * 60 // 超时时间（单位:s）

            )
        );
        $context = stream_context_create($options);
        $response = file_get_contents($platInfo['platInfo']['p_pay_url'], false, $context);
        // */

        $header[] = "Content-type: application/xml";//定义content-type为xml
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_URL, $platInfo['platInfo']['p_pay_url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        $response = curl_exec($ch);
        curl_close($ch);

        $return = json_decode(json_encode(simplexml_load_string($response)), true);
        if(is_array($return) && $return['status'] == '0' && $return['result_code'] == '0'){
            if ($return['redirect_url']) {
                BooView::set('payUrl', $return['redirect_url']);
                BooView::display('pay/locationPay.html');
            } elseif (in_array($type, array(2, 9))) {
                BooView::set('payUrl', $return['code_url']);
                BooView::display('pay/locationPay.html');

            } elseif ($return['code_url']) {
                BooView::set('payUrl', $return['code_url']);
                BooView::display('pay/locationPay.html');
            }
            exit;
        } else {
            BooFile::write('/tmp/akenlee.log', $platInfo['platInfo']['p_pay_url'] . "\n" . $xml . "\n" . $response . "\n" . json_encode($return) . "\n{$payInfo['amount']}\n{$platInfo['appInfo']['name']}\n" . date('Y-m-d H:i:s') . "\n\n", 'a');
        }

        echo json_encode($return);
        exit;
    }

}
