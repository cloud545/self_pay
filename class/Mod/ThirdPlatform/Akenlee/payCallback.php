<?php
class Mod_ThirdPlatform_Akenlee_payCallback extends Mod_ThirdPlatform_Akenlee {

	protected function _do($platInfo = array()) {

        $return = array();
        $content = file_get_contents("php://input");
        if ($content) {
            $return = json_decode(json_encode(simplexml_load_string($content)), true);
        }

        ksort($return);
        $md5Str = '';
        foreach ($return as $key => $val) {

            if ($key == 'sign') {
                continue;
            }

            if (!$md5Str) {
                $md5Str = "{$key}={$val}";
            } else {
                $md5Str .= "&{$key}={$val}";
            }
        }

        $sign = strtoupper(md5($md5Str . "&key=" . $platInfo['platInfo']['appKey']));

        if (is_array($return) && $sign == $return['sign'] && $return['status'] == '0' && $return['result_code'] == '0' && $return['pay_result'] == '0'){
            echo "SUCCESS";
            return array('platOrderId' => $return['transaction_id'],'realMoney' => $return['total_fee'] / 100);
        } else {
            //签名验证失败
            return false;
        }

	}

}
