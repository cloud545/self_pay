<?php
class Mod_ThirdPlatform_HuoHuoPay_saoma extends Mod_ThirdPlatform_HuoHuoPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) {
            $payType = 'alipaywap';
        }

        $data = array(
            'mch_body' => '支付',
            'mch_id' => $platInfo['platInfo']['appId'],
            'mch_no' => $payInfo['pOrderId'],
            'notify_url' => $platInfo['appInfo']['callbackUrl'],
            'paytype' => $payType,
            'return_url' => 'https://www.baidu.com',
            'total_fee' => $payInfo['amount'] * 100,
        );

        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $md5str = $md5str. 'key=' .$platInfo['platInfo']['appKey'];
        $data['client_ip'] = $_REQUEST['REMOTE_ADDR'];
        $data['sign'] = strtoupper(md5($md5str));
        $str='<form class="form-inline" name="payform" method="post" action="'.$platInfo['platInfo']['p_pay_url'].'">';
        foreach ($data as $key => $val) {
            $str.='<input type="hidden" name="' . $key . '" value="' . $val . '">';
        }
        $str.="</form><script>document.forms['payform'].submit();</script>";
        exit($str);
    }
}
