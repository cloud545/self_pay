<?php
class Mod_ThirdPlatform_HuoHuoPay_payCallback extends Mod_ThirdPlatform_HuoHuoPay {

	protected function _do($platInfo = array()) {
        $data = json_decode(file_get_contents('php://input'), true);
        $arr['status'] = $data['status'];
        $arr['mch_id'] = $data['mch_id'];
        $arr['mch_no'] = $data['mch_no'];
        $arr['total_fee'] = $data['total_fee'];
        $sign = $data['sign'];
        $md5str = '';
        foreach ($arr as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $md5str = $md5str. 'key=' .$platInfo['platInfo']['appKey'];
        $signs = strtoupper(md5($md5str));

        if ($sign == $signs && $data['status'] == '1') {
            echo 'success';
            return array('platOrderId' => $data['mch_no'],'realMoney' => $data['total_fee'] /100);
        }
        return false;
	}
}
