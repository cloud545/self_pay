<?php
class Mod_ThirdPlatform_NdaPay_saoma extends Mod_ThirdPlatform_NdaPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = $joinType = 0;
        $transType = '';
        if ($type == 2) { // 支付宝wap
            $payType = 'ALIPAY';
            $transType = 'G004';
            $joinType = 2;
        } elseif ($type == 3) { // 微信wap
            $payType = 'WEIXIN';
            $transType = 'G004';
            $joinType = 2;
        } elseif ($type == 5) { // 银联扫码
            $payType = '926';
        } elseif ($type == 9) { // 支付宝扫码
            $payType = 'ALIPAY';
            $transType = 'G004';
        } elseif ($type == 10) { // 微信扫码
            $payType = 'WEIXIN';
            $transType = 'G004';
        }

        $postData = array(
            'transType' => $transType,		//用户订单编号ID
            'merchantId' => $platInfo['platInfo']['appId'],			//此处填写商户的id
            'orderNo' => $payInfo['pOrderId'],				//支付渠道
            'money' => $payInfo['amount'] * 100,					//支付金额
            'tradetime' => date('YmdHis'),			//订单号
            'notifyUrl' => $platInfo['appInfo']['callbackUrl'],		//支付成功，用户会跳转到这个地址
            'joinType' => $joinType,
            'payWay' => $payType,
            'requestIp' => BooUtil::realIp(),
        );

        $postData["sign"] = $this->generateSign($postData, $platInfo['platInfo']['appKey']);

        BooCurl::setData($postData, 'POST');
        $res = BooCurl::call($platInfo['platInfo']['p_pay_url']);
        $data = json_decode($res, true);
        if (!$data || !$data['qrCodeUrl']) {
            echo $res;
            exit;
        }

        if ($type == 9 || $type == 10) {
            BooView::set('url', $data['qrCodeUrl']);
            BooView::display('pay/saoma.html');
        } else {
            BooView::set('payUrl', $data['qrCodeUrl']);
            BooView::display('pay/locationPay.html');
        }
        exit;

    }

}
