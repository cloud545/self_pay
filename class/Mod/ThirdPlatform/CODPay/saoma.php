<?php
class Mod_ThirdPlatform_CODPay_saoma extends Mod_ThirdPlatform_CODPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();
        $payType = 0;

        if ($type == 3) {   //个码支付宝wap
            $payType = 'wechatHF';
        }
		
        $data = array(
            'api_code' => $platInfo['platInfo']['appId'],
            'is_type' => $payType,
            'order_id' => $payInfo['pOrderId'],
            'price' => number_format($payInfo['amount'], 2, '.', '') ,
            'notify_url' => $platInfo['appInfo']['callbackUrl'],
            'return_url' => 'http://www.baidu.com',
            'return_type' => 'html',
            'time' => time(),
            'mark' => '购买食杂',
        );
		
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $md5strs = $md5str.'key='.$platInfo['platInfo']['appKey'];
        $data['sign'] = strtoupper(md5($md5strs));

        foreach ($data as $key => $val) {
            $formstr .= '<input type="hidden" name="' . $key . '" value="' . $val . '">';
        }

        $p_pay_url = $platInfo['platInfo']['p_pay_url'];
        echo '
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            </head>
            <body onLoad="document.zlinepay.submit();">
                <form name="zlinepay" method="post" action="' . $p_pay_url . '" target="_top"/>'.$formstr.'</form>
            </body>
        </html>
        ';
        exit;
    }
}
