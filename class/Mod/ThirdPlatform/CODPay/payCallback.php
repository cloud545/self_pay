<?php
class Mod_ThirdPlatform_CODPay_payCallback extends Mod_ThirdPlatform_CODPay {

	protected function _do($platInfo = array()) {

	    $data = json_decode(file_get_contents('php://input'), 1);
	    $sign = $data['sign'];
	    unset($data['sign']);
	    unset($data['message']);
	    $data['api_code'] = $platInfo['platInfo']['appId'];
		ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $md5strs = $md5str.'key='.$platInfo['platInfo']['appKey'];
        $signs = strtoupper(md5($md5strs));

        if ($sign == $signs && $data['code'] == '1') {
            echo 'SUCCESS';
            return array('platOrderId' => $data['order_id'],'realMoney' => $data['real_price']);
        }else{
            return false;
        }
        
    }

}
