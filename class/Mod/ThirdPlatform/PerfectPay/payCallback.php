<?php
class Mod_ThirdPlatform_PerfectPay_payCallback extends Mod_ThirdPlatform_PerfectPay {

	protected function _do($platInfo = array()) {
        $data = $_POST;
        $signs = md5($platInfo['platInfo']['appKey'].'|'.$data['pay_time'].'|'.$data['money'].'|'.$data['pay_money'].'|'.$data['order_sn'].'|'.$data['sys_order_sn'].'|'.$platInfo['platInfo']['appSecret']);

        $result = $this->getStatus($platInfo['platInfo']['appId'],$data['order_sn'],$platInfo['platInfo']['appKey'] ,$platInfo['platInfo']['appSecret']);

        if ($data['sign'] == $signs && $result== 'ok') {
            echo 'success';
            return array('platOrderId' => $data['order_sn'],'realMoney' => $data['money']);
        }
        return false;
	}

	public function getStatus($id , $order, $key1 , $key2) {
	    $data['bid'] = $id;
	    $data['order_sn'] = $order;
	    $data['sign'] = md5($key1 . "|"  . $id . "|"  . $order . "|"  . $key2);
        $data = http_build_query(
            $data
        );
        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => $data
            )
        );
        $context = stream_context_create($opts);
        $result = file_get_contents('https://pay.zsf918.com/api/detail', false, $context);
        $result = json_decode($result,1);
        if ($result['data']['pay_state'] == '1') {
            return 'ok';
        }
    }
}
