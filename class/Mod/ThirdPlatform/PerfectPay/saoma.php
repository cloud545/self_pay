<?php
class Mod_ThirdPlatform_PerfectPay_saoma extends Mod_ThirdPlatform_PerfectPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();
        $payType = 0;

        if ($type == 9 || $type == 2) {
            $payType = '2';
        }
        $data = array(
            'bid' => $platInfo['platInfo']['appId'],
            'pay_type' => $payType,
            'username' => md5(time()),
            'user_ip' =>  $platInfo['appInfo']['last_ip'],
            'order_sn' => $payInfo['pOrderId'],
            'money' => number_format($payInfo['amount'],'2','.',''),
            'notify_url' => $platInfo['appInfo']['callbackUrl'],
        );
        $data['sign'] = md5($platInfo['platInfo']['appKey'].'|'.$data['bid'].'|'.$data['money'].'|'.$data['order_sn'].'|'.$data['notify_url'].'|'.$platInfo['platInfo']['appSecret']);
        $data = http_build_query(
            $data
        );
        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => $data
            )
        );
        $context = stream_context_create($opts);
        $result = file_get_contents($platInfo['platInfo']['p_pay_url'], false, $context);
        $result = json_decode($result,1);
        if ($result['code'] == '100') {
            header('location:'.$result['data']['url']);
        } else {
            echo $result['msg'];
        }
    }
}
