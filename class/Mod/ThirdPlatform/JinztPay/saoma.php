<?php
class Mod_ThirdPlatform_JinztPay_saoma extends Mod_ThirdPlatform_JinztPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];

        $payType =  0;
        if ($type == 2) {       // 支付宝wap
            $payType = '1';
        } elseif ($type == 3) { // 微信wap
            $payType = '2';
        } elseif ($type == 5) { // 银联扫码
            $payType    = '926';
        } elseif ($type == 9) { // 支付宝扫码
            $payType        = '1';
        } elseif ($type == 10) { // 微信扫码
            $payType        = '2';
        }


        //参与验签的参数
        $postData = array(
            'merchantCode'  => $platInfo['platInfo']['appId'],                  //商户账号
            'merchantOrderNo'=> $payInfo['pOrderId'],                           //商户订单号
            'payType'       => $payType,                                        //付款类型
            'payPrice'      => $payInfo['amount'] * 100,                        //付款金额
            'merchantName'  => '第一',                                       //商户名称

        );

        //验签加密

        $postData["sign"] = md5($string = "merchantName=" . $postData['merchantName'] . "&merchantCode=" . $postData['merchantCode'] . "&merchantOrderNo=" . $postData['merchantOrderNo']
            . "&payPrice=" . $postData['payPrice'] . "&payType=" . $postData['payType'] . "&" .$platInfo['platInfo']['appKey']);


        //非验签参数
        $postData["requestType"]    = '1';                                //请求类型
        $postData["callbackUrl"]    = $platInfo['appInfo']['callbackUrl'];         //异步通知地址
        $postData["merchantRemark"] = '';                                          //商户备注


        $res = $this->payurl($platInfo['platInfo']['p_pay_url'],$postData);
        $data = json_decode($res, true);
        if ($data['result'] != 0) {
            echo $data['message'];
            exit;
        }

        BooView::set('payUrl', $data['datas']['payPic']);
        BooView::display('pay/locationPay.html');
        exit;


    }

    public function payurl($url,$params){
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_HTTP_VERSION , CURL_HTTP_VERSION_1_1 );
        //json数据格式请求头
        curl_setopt( $ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json; charset=utf-8']);
        curl_setopt( $ch, CURLOPT_USERAGENT , 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.172 Safari/537.22' );
        curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT , 30 );
        curl_setopt( $ch, CURLOPT_TIMEOUT , 30);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER , true );
        curl_setopt( $ch , CURLOPT_POST , true );
        //数据转成json格式
        curl_setopt( $ch , CURLOPT_POSTFIELDS , json_encode($params) );
        curl_setopt( $ch , CURLOPT_URL , $url );

        $response = curl_exec( $ch );
        if ($response === FALSE) {
            return false;
        }
        $httpCode = curl_getinfo( $ch , CURLINFO_HTTP_CODE );
        curl_close( $ch );
        return $response;
    }

}
