
<?php
class Mod_ThirdPlatform_JinztPay_payCallback extends Mod_ThirdPlatform_JinztPay {

    protected function _do($platInfo = array()) {

        $postData                   = array();

        //参与验签的参数
        $postData['merchantOrderNo']     = $_GET['merchantOrderNo'];                //商户订单号
        $postData['payType']             = $_GET['payType'];                        //付款类型
        $postData['payPrice']            = $_GET['payPrice'];                       //付款金额
        $postData['merchantName']        = $_GET['merchantName'];                   //商户名称
        $postData['noticeType']          = $_GET['noticeType'];                     //通知类型

        //验签加密
        $string = "merchantName=" . urlencode($postData['merchantName']) . "&merchantOrderNo=" . $postData['merchantOrderNo']
            . "&noticeType=" . $postData['noticeType'] . "&payPrice=" . $postData['payPrice'] . "&payType=" . $postData['payType'] .'&'.$platInfo['platInfo']['appKey'];
        $checkSign = md5($string);


        $postData['merchantRemark']     = $_GET['merchantRemark'];      //商户备注
        $postData['sign']               = $_GET['sign'];                //签名


        if ($checkSign == $postData['sign'] &&  $postData['noticeType'] == 1){
            echo "ok";
            return array('platOrderId' => $postData['merchantOrderNo'],'realMoney' => $postData['payMoney'] / 100 );
        } else {
            echo "error";
            return false;
        }

    }

}
