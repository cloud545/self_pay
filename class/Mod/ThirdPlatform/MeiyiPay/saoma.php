<?php
class Mod_ThirdPlatform_MeiyiPay_saoma extends Mod_ThirdPlatform_MeiyiPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];

        $payType  = 0;
        
		
		if ($type == 2) {       // 支付宝wap
            $payType = '8008';
        } elseif ($type == 3) { // 微信wap
            $payType = '8007';
        } elseif ($type == 5) { // 银联扫码
            $payType = '926';
        } elseif ($type == 9) { // 支付宝扫码
            $payType = '8008';
        } elseif ($type == 10) { // 微信扫码
            $payType = '8007';
        }
		
        //参与验签的参数
        $postData = array(
            'mchId'             => $platInfo['platInfo']['appId'],        //商户id
            'mchOrderNo'        => $payInfo['pOrderId'],                  //商户订单号
            'productId'         => $payType,                              //支付产品
            'currency'         => 'cny',                              //支付产品
            'amount'            => $payInfo['amount'] * 100,              //支付金额,分位制
			'clientIp'         	=> BooUtil::realIp(),                              //支付产品
            'subject'           => 'best1pay',                            //商品标题
            'body'              => 'best1pay',                            //商品描述信息
            'notifyUrl'         => $platInfo['appInfo']['callbackUrl'],   //通知地址
        );

        $postData['sign'] = $this->makeAuthSort($postData,false,$platInfo['platInfo']['appKey']);
        $params = $this->makeAuthSort($postData,true,true);
		//备注：这里请求上游URL时需要把参数一起传过去并且做一个urlencode处理
		$url = $platInfo['platInfo']['p_pay_url']."?params=".urlencode($params['params']);
		
        $res = $this->payurl($url,$params);
        $res = json_decode($res,true);

        if($res['retCode'] == SUCCESS && !empty($res))
        {
			BooView::set('payUrl', $res['payUrl']);
			BooView::display('pay/locationPay.html');
        }else{
            echo "支付异常，请联系客服! error: {$res['retMsg']}";

        }
        die;
    }


    public function makeAuthSort($data = [], $return = false,$secret_key = false)
    {

        $signature = '';
        ksort($data);
        foreach ($data as $key => $item) {
            $signature .= $key . '=' . $item . '&';
        }
        if ($return) {
            return [
                'params' => json_encode($data),
            ];
        }
        else {
            $signature .= 'key=' . $secret_key;

            return strtoupper(
                md5($signature)
            );
        }
    }

	
	public function payurl($url,$params){
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_HTTP_VERSION , CURL_HTTP_VERSION_1_1 );
        //json数据格式请求头
        curl_setopt( $ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json; charset=utf-8']);
        curl_setopt( $ch, CURLOPT_USERAGENT , 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.172 Safari/537.22' );
        curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT , 30 );
        curl_setopt( $ch, CURLOPT_TIMEOUT , 30);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER , true );
        curl_setopt( $ch , CURLOPT_POST , true );
        curl_setopt( $ch , CURLOPT_POSTFIELDS , $params );
        curl_setopt( $ch , CURLOPT_URL , $url );

        $response = curl_exec( $ch );
        if ($response === FALSE) {
            return false;
        }
        $httpCode = curl_getinfo( $ch , CURLINFO_HTTP_CODE );
        curl_close( $ch );
        return $response;
    }

}
