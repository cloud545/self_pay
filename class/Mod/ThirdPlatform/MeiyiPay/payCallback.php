<?php
class Mod_ThirdPlatform_MeiyiPay_payCallback extends Mod_ThirdPlatform_MeiyiPay {

    protected function _do($platInfo = array()) {

        $postData                   = array();
		//参与验签的参数(空值不参与验签)
        $postData['payOrderId']     = $_POST['payOrderId'];         //支付订单号
        $postData['mchId']          = $_POST['mchId'];           	//商户id
        $postData['mchOrderNo']     = $_POST['mchOrderNo'];         //商户订单号
        $postData['productId']      = $_POST['productId'];          //支付产品
        $postData['amount']         = $_POST['amount'];           	//支付金额 分位置
        $postData['status']         = $_POST['status'];           	//状态，0-订单生成 1-支付中 2-支付成功 3-业务处理完成
        $postData['paySuccTime']    = $_POST['paySuccTime'];     	//支付时间，精确到毫秒
        $postData['backType']       = $_POST['backType'];     		//通知类型 1-前台通知 2-后台通知
        $postData['channelOrderNo'] = $_POST['channelOrderNo'];     		//

		//进行验签
        $checkSign = $this->makeAuthSort($postData,false,$platInfo['platInfo']['appKey']);

        $postData['sign']           = $_POST['sign'];            //签名  
        if ($checkSign == $postData['sign'] &&  $postData['status'] == 2){
            echo "success";
            return array('platOrderId' => $postData['payOrderId'],'realMoney' => $postData['amount'] / 100);
        } else {
            return false;
        }

    }
	

	
	public function makeAuthSort($data = [], $return = false,$secret_key = false)
    {

        $signature = '';
        ksort($data);
        foreach ($data as $key => $item) {
            if (!$item) {
                continue;
            }
            $signature .= $key . '=' . $item . '&';
        }
        if ($return) {
            return [
                'params' => json_encode($data),
            ];
        }
        else {
            $signature .= 'key=' . $secret_key;

            return strtoupper(
                md5($signature)
            );
        }
    }
	
	

}

