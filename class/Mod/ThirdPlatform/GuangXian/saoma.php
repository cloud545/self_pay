<?php
class Mod_ThirdPlatform_GuangXian_saoma extends Mod_ThirdPlatform_GuangXian {


    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $pay_bankcode = '';
        if ($type == 2) { // 支付宝
            $pay_bankcode = 'mcali';
        } elseif ($type == 3) { // 微信
            $pay_bankcode = 'mcwx';
        } elseif ($type == 5) { // 银联
            $pay_bankcode = 'cup';
        } elseif ($type == 9) { // 支付宝扫码
            $pay_bankcode = 'mcali';
        } elseif ($type == 10) { // 微信扫码
            $pay_bankcode = 'mcwx';
        }

        $merchNo=$platInfo['platInfo']['appId'];
        $Md5key = $platInfo['platInfo']['appSecret'];
        $uid = $payInfo['pOrderId'];

        $info = array(
            'product'=>'game',
            'amount'=>number_format($payInfo['amount'],2,'.',''),
            'orderNo'=>$payInfo['pOrderId'],
            'merchNo'=>$merchNo,
            'memo'=>'coin',
            'notifyUrl'=>$platInfo['appInfo']['callbackUrl'],
            'currency'=>'CNY',
            'reqTime'=>date('YmdHis'),
            'title'=>'pay',
            'returnUrl'=>$platInfo['appInfo']['client_url'],
            'userId'=>$uid,
            'outChannel'=>$pay_bankcode
        );

        $context = json_encode($info);
        $sign = md5($context.$Md5key);
        $context = base64_encode($context);
        $pay = array(
            'sign'=>$sign,
            'encryptType'=>'MD5',
            'context'=>$context
        );

        $p_pay_url = $platInfo['platInfo']['p_pay_url'];
        $return = $this->curl($p_pay_url,$pay,'POST');
        $rdata = json_decode($return,true);
        $recContext = base64_decode($rdata['context']);
        $context = json_decode($recContext,true);
        if(is_array($context) && $context['code_url']){
            header('Location: '.$context['code_url']);
        }else{
            echo '下单失败，原因：'.$rdata['msg'];
        }
        exit;


    }

}
