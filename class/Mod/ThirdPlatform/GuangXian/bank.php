<?php
class Mod_ThirdPlatform_GuangXian_bank extends Mod_ThirdPlatform_GuangXian {

	protected function _do($platInfo = array(), $payInfo = array()){

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $time = time();
        $post = BooVar::requestx();
        $data = array();
        $data['appId'] = $post['appId'];
        $data['platId'] = $post['platId'];
        $data['userId'] = $post['userId'];
        $data['amount'] = $payInfo['amount'];
        $data['orderId'] = $payInfo['orderId'];
        $data['pOrderId'] = $payInfo['pOrderId'];
        $data['time'] = $time;
        $sign = $this->getSign($data, $platInfo['appInfo']['app_key']);

        BooView::set('appId', $post['appId']);
        BooView::set('platId', $post['platId']);
        BooView::set('userId', $post['userId']);
        BooView::set('amount', $payInfo['amount']);
        BooView::set('orderId', $payInfo['orderId']);
        BooView::set('pOrderId', $payInfo['pOrderId']);
        BooView::set('time', $time);
        BooView::set('sign', $sign);
        BooView::set('bankCodeList', $platInfo['platInfo']['bankCode']);
        BooView::display('pay/bank.html');
        exit;
	}


}
