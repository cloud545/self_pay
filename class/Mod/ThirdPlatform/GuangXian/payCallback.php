<?php
class Mod_ThirdPlatform_GuangXian_payCallback extends Mod_ThirdPlatform_GuangXian {

	protected function _do($platInfo = array()) {

        $post_origin = file_get_contents("php://input");
        $post = json_decode($post_origin,true);
        $md5_key = $platInfo['platInfo']['appSecret'];
        if(is_array($post)){
            $context = base64_decode($post['context']);
            $notify = json_decode($context,true);
            $sign = md5($context.$md5_key);
            if($sign == $post['sign'] && $notify["orderState"] == 1){
                echo json_encode(array('result'=>'ok'));
                return array('platOrderId' => $notify["businessNo"],'realMoney'=>$notify['amount']);
            }
        }
        return false;

	}

}
