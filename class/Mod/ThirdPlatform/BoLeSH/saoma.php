<?php
class Mod_ThirdPlatform_BoLeSH_saoma extends Mod_ThirdPlatform_BoLeSH {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();
        $payType = 0;

        if ($type == 2) {
            $payType = '4';
        }

        if ($type == 9) {
            $payType = '5';
        }
        $data = array(
            'customerNo' => $platInfo['platInfo']['appId'],
            'orderNo' => $payInfo['pOrderId'],
            'payTypeId' => $payType,
            'amount' => $payInfo['amount'],
            'customerCallbackUrl' => $platInfo['appInfo']['callbackUrl'],
        );
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $data['sign'] = md5($md5str.'key='.$platInfo['platInfo']['appKey']);
        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, $platInfo['platInfo']['p_pay_url']); // 要访问的地址
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        $res = curl_exec($curl);
        $result = json_decode($res,1);
        curl_close($curl); // 关闭CURL会话
        
        if($result['success'] == true){
            $log = "BoLeSH pOrderId={$payInfo['pOrderId']}\t请求成功\n";
            $log = $log."支付地址：{$result['data']['url']}";
        }else{
            $log = "BoLeSH pOrderId={$payInfo['pOrderId']}\t请求失败\n";
            $log = $log."返回信息：{$res}";
        }
        BooLog::pay($log);
        
        if ($result['success'] == true) {
            header('location:'.$result['data']['url']);
        } else {
            var_dump($result);
        }
    }
}
