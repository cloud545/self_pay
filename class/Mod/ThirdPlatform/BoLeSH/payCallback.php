<?php
class Mod_ThirdPlatform_BoLeSH_payCallback extends Mod_ThirdPlatform_BoLeSH {

	protected function _do($platInfo = array()) {
        $data = json_decode(file_get_contents('php://input'), 1);
        $sign = $data['sign'];
        unset($data['sign']);
        ksort($data);
        $md5str = '';
        foreach ($data['data'] as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }

        $signs = md5($md5str.'key='.$platInfo['platInfo']['appKey']);

        if ($sign == $signs && $data['success'] == true) {
            echo 'ok';
            return array('platOrderId' => $data['data']['orderNo'],'realMoney' => $data['data']['amount']);
        }
        return false;
	}
}
