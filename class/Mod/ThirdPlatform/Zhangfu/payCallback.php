<?php
class Mod_ThirdPlatform_Zhangfu_payCallback extends Mod_ThirdPlatform_Zhangfu {

	protected function _do($platInfo = array()) {


        //异步回调通知
        $arr['status'] = $_POST['status'];
        $arr['mchno'] = $_POST['mchno'];
        $arr['orderno'] = $_POST['orderno'];
        $arr['orderid'] = $_POST['orderid'];
        $arr['payamt'] = $_POST['payamt'];
        $arr['paytime'] = $_POST['paytime'];
        $arr['paytype'] = $_POST['paytype'];
        $arr['attach'] = $_POST['attach'];
        $sign = $_POST['sign'];
        $mkey = $platInfo['platInfo']['appKey']; //后台获取
        $yzsign = md5($this->ASCII($arr).$mkey);
        if($_POST['status'] == '101' && $sign == $yzsign) {
            //逻辑处理
            //注意做好防重复处理
            echo 'ok';
            return array('platOrderId' => $_POST['orderid'], 'realMoney' => $_POST['payamt']);
        } else {
            return false;
        }

	}

}
