<?php
class Mod_ThirdPlatform_Zhangfu extends Mod_ThirdPlatform {

    protected function curl_post($url, $params){
        $curl = curl_init();//设置url
        curl_setopt($curl, CURLOPT_URL,$url);//设置发送方式：post
        curl_setopt($curl, CURLOPT_POST, true);//设置发送数据
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($curl, CURLOPT_TIMEOUT, 8);
        if(stripos($url,'https')!==false){
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        }
        curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        $r =curl_exec($curl);
        curl_close($curl);
        return $r;
    }

    protected function ASCII($params = array()){
        if(!empty($params)){
            $params=array_filter($params);
            $p =  ksort($params);
            if($p){
                $str = '';
                foreach ($params as $k=>$val){
                    $str .= $k .'=' . $val . '&';
                }
                $strs = rtrim($str, '&');
                return $strs;
            }
        }
        return '';
    }
}
