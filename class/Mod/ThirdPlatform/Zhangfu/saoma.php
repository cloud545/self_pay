<?php
class Mod_ThirdPlatform_Zhangfu_saoma extends Mod_ThirdPlatform_Zhangfu {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝wap
            $payType = 'alipay_h5';
        } elseif ($type == 3) { // 微信wap
            $payType = 'wxpay_h5';
        } elseif ($type == 5) { // 银联
            $payType = 'gate_ewm';
        } elseif ($type == 9) { // 支付宝扫码
            $payType = 'alipay_ewm';
        } elseif ($type == 10) { // 微信扫码
            $payType = 'wxpay_ewm';
        }

        $arr = array();
        $arr['mchno'] = $platInfo['platInfo']['appId']; //商户号
        $arr['money'] = $payInfo['amount']; //订单金额,元为单位,整数
        $arr['orderno'] = $payInfo['pOrderId']; //商户订单号
        $arr['paytype'] = $payType;  //支付类型, alipay_ewm/alipay_h5
        $arr['notifyurl'] = $platInfo['appInfo']['callbackUrl']; //异步通知URL
        $arr['attach'] = '';  //透传参数,原样返回
        $mkey = $platInfo['platInfo']['appKey']; //后台获取
        $arr['sign'] = md5($this->ASCII($arr) . $mkey);
        $res = $this->curl_post($platInfo['platInfo']['p_pay_url'], $arr);

        $result = json_decode($res, true);
        if ($result['status'] != 101) {
            BooFile::write('/tmp/zhangfu.log', $res . "\n{$payInfo['amount']}\n{$platInfo['appInfo']['name']}\n111\n\n", 'a');
            echo "支付异常，请联系客服! status：{$result['status']}，message: {$result['message']}";
            exit;
        }

        if (!$result['url']) {
            BooFile::write('/tmp/zhangfu.log', $res . "\n{$payInfo['amount']}\n{$platInfo['appInfo']['name']}\n222\n\n", 'a');
        }

        BooView::set('payUrl', $result['url']);
        BooView::display('pay/locationPay.html');
        exit;
    }

}
