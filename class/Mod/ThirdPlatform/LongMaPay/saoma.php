<?php
class Mod_ThirdPlatform_LongMaPay_saoma extends Mod_ThirdPlatform_LongMaPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];

        $payType  = 0;


        if ($type == 3 || $type == 10) {        // 微信wap1和微信扫码1
            $payType = '902';
        } elseif ($type == 2 || $type == 9) { // 微信wap
            $payType = '904';
        } else {                 // 支付宝wap
            exit;
        }

        //参与验签的参数
        $postData= [
            'user_id'            => $platInfo['platInfo']['appId'],             //商户id
            'out_trade_no'       => $payInfo['pOrderId'],                               //商户订单号    
            'product_id'         => $payType,                                                   //支付产品ID
            'return_url'         => $platInfo['appInfo']['clientUrl'],      //页面通知
            'notify_url'         => $platInfo['appInfo']['callbackUrl'],    //服务器通知
            'subject'            => 'Best1Pay',                                                 //订单标题
            'body'               => 'Best1Pay',                                                 //订单描述      
            'remark'             => 'Best1Pay',                                                 //备注
            'attach'             => '',                                                                 //附加字段
            'pay_amount'         => $payInfo['amount'] * 100,                   //支付金额,分位制
            'applydate'          => date("Y-m-d H:i:s"),                                //提交时间      


        ];
        $postData['sign'] = $this->generateSign($postData,$platInfo['platInfo']['appKey']);
        BooCurl::setData($postData, 'POST');
        $res = BooCurl::call($platInfo['platInfo']['p_pay_url']);
        $data = json_decode($res, true);
        if($data['code'] == '1' && !empty($data) && $data['msg'] == 'success')
        {
			BooView::set('payUrl', $data['data']['pay_extends']['pay_url']);
			BooView::display('pay/locationPay.html');
			exit;
        }else{
            echo "支付异常，请联系客服! error: {$res['msg']}";

        }
    }
}
