<?php
class Mod_ThirdPlatform_LongMaPay_payCallback extends Mod_ThirdPlatform_LongMaPay {

    protected function _do($platInfo = array()) {
        $data = $_POST;
        $signs = $data['sign'];
        unset($data['sign']);
        ksort($data);
        $result = $this->getOrderStatus($data['user_id'],$data['trade_no'],$platInfo['platInfo']['appKey']);

        //进行验签
        $md5str = '';
        foreach ($data as $key=> $value) {
            if ($value) {
                $md5str = $md5str . $key .'=' . $value.'&';
            }
        }
        $sign = strtoupper(md5($md5str.'apikey='.$platInfo['platInfo']['appKey']));
        if ($sign == $signs &&  $data['returncode'] == '00' && $result == 'ok'){

            echo "OK";
            return array('platOrderId' => $data['out_trade_no'],'realMoney' => $data['transaction_money'] / 100);
        } else {
            return false;
        }

    }
    public function getOrderStatus($user_id,$order, $appKey) {
        $data['user_id'] = $user_id;
        $data['trade_no'] = $order;
        ksort($data);
        $md5str = '';
        foreach ($data as $key=> $value) {
            if ($value) {
                $md5str = $md5str . $key .'=' . $value.'&';
            }
        }
        $data['sign'] = strtoupper(md5($md5str.'apikey='.$appKey));
        BooCurl::setData($data, 'POST');
        $res = BooCurl::call('https://longmazhifu.com/index/pay/trade_query');
        $result = json_decode($res, true);
        if ($result['data']['status'] == '1') {
            return 'ok';
        } else {
            return false;
        }
    }
}

