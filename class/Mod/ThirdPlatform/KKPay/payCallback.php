<?php
class Mod_ThirdPlatform_KKPay_payCallback extends Mod_ThirdPlatform_KKPay {

    protected function _do($platInfo = array()) {
        $data = array(
            'body'      => $_POST['body'],
            'mOrderId'  => $_POST['mOrderId'],
            'merchantId'    => $_POST['merchantId'],
            'orderAmount'   => $_POST['orderAmount'],
            'pOrderId'      => $_POST['pOrderId'],
            'payAmount'     => $_POST['payAmount'],
            'payType'       => $_POST['payType'],
            'signType'      => $_POST['signType'],
            'version'       => $_POST['version'],
        );

        $md5key  = $platInfo['platInfo']['appKey'];
        $sign = $_POST['sign'];
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $md5str = substr($md5str, 0, strlen($md5str) - 1 ) . $md5key;
        $signs = strtoupper(md5($md5str));

        if ($sign == $signs) {
            echo 'success';
            return array('platOrderId' => $_POST['mOrderId'],'realMoney' => $_POST['payAmount'] / 100);
        }
        return false;
    }

}

