<?php
class Mod_ThirdPlatform_KKPay_saoma extends Mod_ThirdPlatform_KKPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];

        $payType = $joinType = 0;
        if ($type == 2) {       // 支付宝wap
            $payType = 'alipay_tra';
        } elseif ($type == 3) { // 微信wap
            exit;
        } elseif ($type == 5) { // 银联扫码
            exit;
        } elseif ($type == 9) { // 支付宝扫码
            $payType = 'alipay_tra';
        } elseif ($type == 10) { // 微信扫码
            exit;
        }

        $postData = array(
            'merchantId'    => $platInfo['platInfo']['appId'],               //商户账号
            'mOrderId'      => $payInfo['pOrderId'],                          //商户订单号
            'payType'       => $payType,                                       //支付通道
            'body'          => 'best1pays',                                   //支付模式
            'amount'        => $payInfo['amount'] * 100,                     //充值面额
            'signType'      => 'MD5',                                       //签名方式
            'version'       => '1.0',                                       //接口版本
            'notifyUrl'     => $platInfo['appInfo']['callbackUrl'],       //回调参数
        );

        //验签加密
        ksort($postData);
        $checkString = '';
        foreach ($postData as $key =>$value) {
            if (!$checkString) {
                $checkString = "{$key}={$value}";
            } else {
                $checkString .= "&{$key}={$value}";
            }
        }

        $checkString .= "{$platInfo['platInfo']['appKey']}";
        $postData['sign'] = strtoupper(md5($checkString));
        $resp_json_str = $this->send_post($platInfo['platInfo']['p_pay_url'], $postData);
        $resp_arr = json_decode($resp_json_str,true);
        if($resp_arr['code'] == 0 && !empty($resp_arr))
        {
            BooView::set('payUrl', $resp_arr['data']['url']);
            BooView::display('pay/locationPay.html');
        }else{
            echo "支付异常，请联系客服! error: {$resp_arr['msg']}";
        }
        exit;
    }
}

