<?php
class Mod_ThirdPlatform_XinFTPay_saoma extends Mod_ThirdPlatform_XinFTPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();
        $payType = 0;
        $url = '';

        if ($type == 2) {   //个码支付宝wap
            $payType = '10107';
        }
        if ($type == 9) {   //个码支付宝扫码
            $payType = '10106';
        }
        
        if ($type == 10) {
            $payType = '10104';
        }
        
		
        $data = array(
            'partner' => $platInfo['platInfo']['appId'],
            'service' => $payType,
            'tradeNo' => $payInfo['pOrderId'],
            'amount' => $payInfo['amount'] ,
            'notifyUrl' => $platInfo['appInfo']['callbackUrl'],
            'extra' => 'best1pay' 
        );
		
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $md5strs = $md5str.$platInfo['platInfo']['appKey'];
        $data['sign'] = md5($md5strs);
		
        foreach ($data as $key => $val) {
            $formstr .= '<input type="hidden" name="' . $key . '" value="' . $val . '">';
        }

        $p_pay_url = $platInfo['platInfo']['p_pay_url'];
        echo '
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            </head>
            <body onLoad="document.zlinepay.submit();">
                <form name="zlinepay" method="post" action="' . $p_pay_url . '" target="_top"/>'.$formstr.'</form>
            </body>
        </html>
        ';
        exit;
    }
}
