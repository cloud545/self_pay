<?php
class Mod_ThirdPlatform_XinFTPay_payCallback extends Mod_ThirdPlatform_XinFTPay {

	protected function _do($platInfo = array()) {

	    $data = $_POST;
	    $sign = $data['sign'];
	    unset($data['sign']);
	    unset($data['signType']);
	    unset($data['charset']);
		ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $md5strs = $md5str.$platInfo['platInfo']['appKey'];
        $signs = md5($md5strs);

        if ($sign == $signs && $data['status'] == '1') {
            echo 'success';
            return array('platOrderId' => $data['outTradeNo'],'realMoney' => $data['amount']);
        }else{
            echo "error";
            return false;
        }
        
    }

}
