<?php
class Mod_ThirdPlatform_ZanBeiHK_saoma extends Mod_ThirdPlatform_ZanBeiHK {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if($type == 2) {
            $payType = '2';
        }

        $data = array(
            'uid'      => $platInfo['platInfo']['appId'],
            'paytype'  => $payType,
            'notify_url' => $platInfo['appInfo']['callbackUrl'],
            'money'      => number_format($payInfo['amount'], '2', '.', ''),
            'orderid'    => $payInfo['pOrderId'],
            'orderuid'   => $platInfo['platInfo']['appId'],
            'ip'         => $_SERVER['REMOTE_ADDR']
        );
        
        $md5Str = $data['uid'].$data['paytype'].$data['notify_url'].$data['orderid'].$data['orderuid'].$data['money'].$platInfo['platInfo']['appKey'];
        $data['key'] = md5($md5Str);
        $data = json_encode($data);                                                                                   
        $ch = curl_init($platInfo['platInfo']['p_pay_url']);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data))                                                                       
        ); 
        $result = json_decode(curl_exec($ch),1);

        if ($result['code'] == 1) {
                header('location:'.$result['data']['payurl']);
        } else {
            echo $result['msg'];
            exit;
        }
    }
}
