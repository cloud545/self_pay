<?php
class Mod_ThirdPlatform_ZanBeiHK_payCallback extends Mod_ThirdPlatform_ZanBeiHK {
    protected function _do($platInfo = array()) {

        $data = $_POST;
        $sign = $data['key'];
        $md5Str = $data['uid'].$data['platform_trade_no'].$data['orderid'].$data['money'].$data['orderuid'].$data['status'].$platInfo['platInfo']['appKey'];
        $key = md5($md5Str);
        if ($sign == $key) {
            echo 'OK';
            return array('platOrderId' => $data['platform_trade_no'],'realMoney' => $data['money']);
        }
        return false;
    }
}