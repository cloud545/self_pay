<?php
class Mod_ThirdPlatform_AiNiPay_saoma extends Mod_ThirdPlatform_AiNiPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();
        $payType = 0;

        if ($type == 17) {
            $payType = 'NETBANK';
        }
		
        $data = array(
            'sa' => 'deposit',
            'mchNo' => $platInfo['platInfo']['appId'],
            'payType' => $payType,
            'orderNo' => $payInfo['pOrderId'],
            'amount' => number_format($payInfo['amount'], 2, '.', '') ,
            'callbackUrl' => $platInfo['appInfo']['callbackUrl'],
            'returnUrl' => 'http://www.baidu.com',
            'remark' => '购买食杂',
        );
		
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $md5str = substr($md5str , 0 , strlen($md5str) - 1);
        $md5str = $md5str.$platInfo['platInfo']['appKey'];
        $data['sign'] = strtoupper(md5($md5str));
        $data['userIP'] = $platInfo['appInfo']['last_ip'];
        $str='<form class="form-inline" name="payform" method="post" action="'.$platInfo['platInfo']['p_pay_url'].'">';
        foreach ($data as $key => $val) {
            $str.='<input type="hidden" name="' . $key . '" value="' . $val . '">';
        }
        $str.="</form><script>document.forms['payform'].submit();</script>";
        exit($str);
    }
}
