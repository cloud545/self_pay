<?php
class Mod_ThirdPlatform_AiNiPay_payCallback extends Mod_ThirdPlatform_AiNiPay {

	protected function _do($platInfo = array()) {

	    $data = $_POST;
	    $sign = $data['sign'];
	    unset($data['sign']);
	    unset($data['message']);
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $md5str = substr($md5str , 0 , strlen($md5str) - 1);
        $md5str = $md5str.$platInfo['platInfo']['appKey'];
        $signs = strtoupper(md5($md5str));
        if ($sign == $signs && $data['payResult'] == '8') {
            echo 'success';
            return array('platOrderId' => $data['orderNo'],'realMoney' => $data['amount']);
        }else{
            return false;
        }
    }
}
