<?php
class Mod_ThirdPlatform_JunLinPay_payCallback extends Mod_ThirdPlatform_JunLinPay {

	protected function _do($platInfo = array()) {
        $data = $_POST;

        $sign = $_POST['sign'];
        unset($data['sign']);
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            if($value) {
                $md5str = $md5str . $key . '=' . $value .'&';
            }
        }
        $signs = md5(substr($md5str,0 , strlen($md5str)-1). $platInfo['platInfo']['appKey']);

        if ($sign == $signs && $data['order_state'] == '1') {
            echo 'success';
            return array('platOrderId' => $_POST['out_order_no'],'realMoney' => $_POST['pay_amount']);
        }
        return false;
	}

}