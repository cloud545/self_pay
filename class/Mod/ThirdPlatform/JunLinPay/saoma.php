<?php
class Mod_ThirdPlatform_JunLinPay_saoma extends Mod_ThirdPlatform_JunLinPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();
        $payType = 0;
        $url = '';
        if ($type == 9) {   //话费微信H5
            $payType = 'alipay_qr';
        }

        if ($type == 10) {
            $payType = 'wechat_qr';
        }

        $data = array(
            'mch_id' => $platInfo['platInfo']['appId'],
            'out_trade_no' => $payInfo['pOrderId'],
            'pay_type' => $payType,
            'order_amount' => number_format($payInfo['amount'], '2', '.', ''),
            'notify_url' => $platInfo['appInfo']['callbackUrl'],
            'user_code' => '1',
        );
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $data['sign'] = md5(substr($md5str, 0, strlen($md5str) - 1). $platInfo['platInfo']['appKey']);

        $data = http_build_query(
            $data
        );
        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => $data
            )
        );
        $context = stream_context_create($opts);
        $result = json_decode(file_get_contents($platInfo['platInfo']['p_pay_url'], false, $context), 1);
        if ($result['success'] == '0') {
            header('location:'.$result['pay_url']);
        } else {
            echo $result['msg'];
        }
    }
}
