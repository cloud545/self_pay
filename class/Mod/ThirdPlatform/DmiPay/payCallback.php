<?php
class Mod_ThirdPlatform_DmiPay_payCallback extends Mod_ThirdPlatform_DmiPay {

    protected function _do($platInfo = array()) {

        $postData                   = array();

        //参与验签的参数
        $postData['memberid']           = $_REQUEST['memberid'];                //商户编号
        $postData['orderid']            = $_REQUEST['orderid'];                 //订单号
        $postData['amount']             = $_REQUEST['amount'];                  //订单金额
        $postData['transaction_id'] 	= $_REQUEST['transaction_id'];          //交易流水号
        $postData['datetime']           = $_REQUEST['datetime'];                //交易时间
        $postData['returncode']     	= $_REQUEST['returncode'];              //交易状态 00表示成功，其它表示失败

        //验签加密
        $md5str = "";
        foreach ($postData as $key => $val) {
            $md5str = $md5str . $key . "=" . $val . "&";
        }
        $checkSign = strtoupper(md5($md5str . "key=" . $platInfo['platInfo']['appKey']));
        $postData['sign']               = $_REQUEST['sign'];                //签名

        if ($checkSign == $postData['sign'] &&  $postData['returncode'] == '00'){
            echo "OK";
            return array('platOrderId' => $postData['orderid'],'realMoney' => $postData['amount']);
        } else {
            echo "error";
            return false;
        }

    }
	

	

}

