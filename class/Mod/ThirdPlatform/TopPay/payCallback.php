<?php
class Mod_ThirdPlatform_TopPay_payCallback extends Mod_ThirdPlatform_TopPay {

	protected function _do($platInfo = array()) {

        $back_str = file_get_contents("php://input");
        $back_data = json_decode($back_str,true);

        // 系统公钥
        // MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAweUFPpBJ7JRvCzJv3Zzeme6GZurti6wnPxwvl85jjhApKo58HSBaUqAj7XMHLUkliwjpeQxCZo0t44thOEs0znlXNMXE1eMtN/iOmyw9jZHpRlImHxAvR4pIqPGXdvPnG1oh3z46mR3uuPBdw6GFltuPA7QWCYNZc+Er6NjWvV3yr4FMzD3pz91J8pifE/UBm3o3rU967DPej/XYs83ec4N4Gqz4bIpgf1OSLz1YOz4cjDiikYkeGwWUajzqukA1PJPOR90iK2vmgNL4bUxss4py69ZZLlT+21ISEqxZdxLLnHlpF+hm3K5pSZMHSghAjVAmr3fxy0ZZfO9768OC9wIDAQAB

        // 商户公私钥
        $public_key = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnC8zuAHdQh21B6EezIMHlPgLCIYc+BiKgcpNRTk6Qq7qMTkhrkJC0x3pL4r+8NF3RFN1LA91zgQG5ZzD56JzOg60acWZHqv1E4H8xoUIEoQgiy5wP+LA8HGvdesWg02ieDkiazC2NS6gQaBzTxb+H5Jtk7uiN8aaQNQf8pYFwJsqII1XtLdJSjbVSvvBNSQBK3flD2tgwRVrYfPiPcG0UizRpk8HM50nLCwnW+oO2ZgMIeRUgASQXZUr8FObC+eP7pLHYQho0o6lg1QZjVm22yDQeMLHH1jd0wISu2Ixz9q/DyQwWiOyJGRNOsti7ViCbvAWJa9JFp5u5QlN7XMzhQIDAQAB';
        $private_key = 'MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCcLzO4Ad1CHbUHoR7MgweU+AsIhhz4GIqByk1FOTpCruoxOSGuQkLTHekviv7w0XdEU3UsD3XOBAblnMPnonM6DrRpxZkeq/UTgfzGhQgShCCLLnA/4sDwca916xaDTaJ4OSJrMLY1LqBBoHNPFv4fkm2Tu6I3xppA1B/ylgXAmyogjVe0t0lKNtVK+8E1JAErd+UPa2DBFWth8+I9wbRSLNGmTwcznScsLCdb6g7ZmAwh5FSABJBdlSvwU5sL54/uksdhCGjSjqWDVBmNWbbbINB4wscfWN3TAhK7YjHP2r8PJDBaI7IkZE06y2LtWIJu8BYlr0kWnm7lCU3tczOFAgMBAAECggEAAQinwGJBLN/cQ+Z2he06gdG5spIlfPFRcWLkBT8Uuj5E9JOnomsNdpV7GdwP34sANBnG68ZXORVG8M9YO9J6dGCGimPkpV+HNsLXtOSw7smktLPw/RGq+N+q+EFyTub9QIVFGHQkSjcHZO42W5UP+sHrkcL3/VCGOUM6/U5HtkR0Z+6VjAUm/GgrTythFeCj4SecoqGz6HKohoR6T7m+FeM2CBDq1GouwzEi/N/MVJmJhheWdza5wTVBDtG/zTjLWJimrRyayX0s5tjStLe+8oD/+Ps4h6hjOQeHupTAZGzAcryeEzb4SjNpEWNXhxDAlxtQUjHvdETB1lhsGkgDnQKBgQDaueigvmtXVSPLNZi7vUJWovbPfVTql7P6A82SRCxx7+1QnIr0Ta5eQjDuzmNJ7Jj9d+aYBH4O7nWzYVNRLgLs2B8qjoYq2N6DMgde2ZFOQidREDhvQhloPORLZMbmNDiuHIT7LU1U2Bqb1TBbO6qFO2WAYGX7z0wOgdZv0SydRwKBgQC2zNxCOypsBWN3DvYM3tg/1w5eP9wS7iauZuG4x9yosfom8C8vgC+2hE1C7snsd0iiD+aCh84qmwYOaMGanTeCwQB+ZLI4faoAjHm5rMnnkKA5bfk5A00aV3jNSsGrdalXPUtIoWvXtYIv7CZ4K1FsWQMxJcyEv7PN2xQm/Wbe0wKBgFzZ4OwkCdNYHErOpNjF3hyDL70jWmIAJnHzHViDQk7X6Gl5Gu/c7jGrbbRivvmJy+SF+sj71zuldSfCICltG+bJVGmveTtFNyiAm2pjd/C92ozqEtsAN2LtvohW7qyY7JrVFoq+T36+AoW2ucFxEdFfRpZ51bFQ7duWuJ5dMNBPAoGAGrLpKLNDiutuj6Ebxt11cD2U16c0GX+3HpngNgDa9bio9K739VQfo+TG9BO0Ylk5vpHb9QyozAhAZjJetJpp1SsjS1KhxSZ1ggcCcGAQTUYOYNTzJaSZylMkTbexeb2BskA9kqog+iyEi0NOixaomc6WbMM8j9cAB/0LgHZaZAkCgYEAipqJcbvk/8S4sMW+dVjuAb3+CvsRFzxRRTWcUFPmlnBJTaRQGLrI/Qxbvsvm/LBc4ADVoTrtLxVv7TRQAZQ5CIuimYk4s13euO3lF5HRQEuAyrTj6Go4D5FKriANtRJ5q9/a6fb79qz0ZVhGfUyMawJwPWLcVqZJTND3/+795AM=';

        $signStr = "actualPrice=".$back_data['actualPrice']."&dealTime=".date("YmdHis",strtotime($back_data['dealTime']))."&orderNo=".$back_data['orderNo']."&orderStatus=".$back_data['orderStatus']."&orderTime=".date("YmdHis",strtotime($back_data['orderTime']))."&price=".$back_data['price']."&tradeNo=".$back_data['tradeNo'];
        $str = $signStr;
        $sign = $this->rsaSign($str,$private_key);
        $sign = urlencode($sign);

        //若想确认私钥加密结果是否正常，请把以下两条程序注解掉
        //$sign = $back_data['signature'];
        //$sign = urldecode($sign);
        if($this->rsaCheck($str,$public_key,$sign) && $back_data['orderStatus'] == 1){
            echo 'OK';
            return array('platOrderId' => $back_data['tradeNo'],'realMoney' => $back_data['actualPrice']);
        }else{
            return false;
        }

	}

}
