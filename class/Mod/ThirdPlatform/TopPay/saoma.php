<?php
class Mod_ThirdPlatform_TopPay_saoma extends Mod_ThirdPlatform_TopPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝wap
            $payType = 1101;
        } elseif ($type == 3) { // 微信wap
            $payType = 1102;
        } elseif ($type == 5) { // 银联
            $payType = 1105;
        } elseif ($type == 9) { // 支付宝扫码
            $payType = 1101;
        } elseif ($type == 10) { // 微信扫码
            $payType = 1102;
        }

        $brandNo		= $platInfo['platInfo']['appId'];
        $orderNo		= $payInfo['pOrderId'];
        $price			= number_format($payInfo['amount'],2,'.','');
        $serviceType	= $payType;								// 参考serviceType 列表
        $userName		= "Test User";
        $callbackUrl	= $platInfo['appInfo']['callbackUrl'];	// 记得改成自己的位址
        $frontUrl		= $platInfo['appInfo']['clientUrl'];		// 记得改成自己的位址
        $clientIP		= BooUtil::realIp();
        $signType		= "RSA-S";
        $signature		= "";

        /* serviceType 列表如下
        1001 = 银行卡
        1002 = 网银
        1003 = 快捷支付
        1101 = 支付宝(扫码)
        1102 = 微信支付(扫码)
        1103 = QQ钱包(扫码)
        1104 = 京东支付(扫码)
        1105 = 银联支付(扫码)
        1201 = 支付宝(H5)
        1202 = 微信支付(H5)
        1203 = QQ钱包(H5)
        1204 = 京东支付(H5)
        1205 = 银联支付(H5)	*/

        /////////////////////////////   参数组装   /////////////////////////////////
        /* 参与签名参数都要参与组装，组装顺序是按照a~z的顺序，下划线"_"优先于字母 */
        $signStr = "";
        $signStr = $signStr."brandNo=".$brandNo."&";
        $signStr = $signStr."clientIP=".$clientIP."&";
        $signStr = $signStr."orderNo=".$orderNo."&";
        $signStr = $signStr."price=".$price."&";
        $signStr = $signStr."serviceType=".$serviceType."&";
        $signStr = $signStr."userName=".$userName;

        //Debug时使用
        //echo "<br>".$signStr."<br>";

        /////////////////////////////   获取sign值（RSA-S加密）  /////////////////////////////////
        $merchant_private_key = 'MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCcLzO4Ad1CHbUHoR7MgweU+AsIhhz4GIqByk1FOTpCruoxOSGuQkLTHekviv7w0XdEU3UsD3XOBAblnMPnonM6DrRpxZkeq/UTgfzGhQgShCCLLnA/4sDwca916xaDTaJ4OSJrMLY1LqBBoHNPFv4fkm2Tu6I3xppA1B/ylgXAmyogjVe0t0lKNtVK+8E1JAErd+UPa2DBFWth8+I9wbRSLNGmTwcznScsLCdb6g7ZmAwh5FSABJBdlSvwU5sL54/uksdhCGjSjqWDVBmNWbbbINB4wscfWN3TAhK7YjHP2r8PJDBaI7IkZE06y2LtWIJu8BYlr0kWnm7lCU3tczOFAgMBAAECggEAAQinwGJBLN/cQ+Z2he06gdG5spIlfPFRcWLkBT8Uuj5E9JOnomsNdpV7GdwP34sANBnG68ZXORVG8M9YO9J6dGCGimPkpV+HNsLXtOSw7smktLPw/RGq+N+q+EFyTub9QIVFGHQkSjcHZO42W5UP+sHrkcL3/VCGOUM6/U5HtkR0Z+6VjAUm/GgrTythFeCj4SecoqGz6HKohoR6T7m+FeM2CBDq1GouwzEi/N/MVJmJhheWdza5wTVBDtG/zTjLWJimrRyayX0s5tjStLe+8oD/+Ps4h6hjOQeHupTAZGzAcryeEzb4SjNpEWNXhxDAlxtQUjHvdETB1lhsGkgDnQKBgQDaueigvmtXVSPLNZi7vUJWovbPfVTql7P6A82SRCxx7+1QnIr0Ta5eQjDuzmNJ7Jj9d+aYBH4O7nWzYVNRLgLs2B8qjoYq2N6DMgde2ZFOQidREDhvQhloPORLZMbmNDiuHIT7LU1U2Bqb1TBbO6qFO2WAYGX7z0wOgdZv0SydRwKBgQC2zNxCOypsBWN3DvYM3tg/1w5eP9wS7iauZuG4x9yosfom8C8vgC+2hE1C7snsd0iiD+aCh84qmwYOaMGanTeCwQB+ZLI4faoAjHm5rMnnkKA5bfk5A00aV3jNSsGrdalXPUtIoWvXtYIv7CZ4K1FsWQMxJcyEv7PN2xQm/Wbe0wKBgFzZ4OwkCdNYHErOpNjF3hyDL70jWmIAJnHzHViDQk7X6Gl5Gu/c7jGrbbRivvmJy+SF+sj71zuldSfCICltG+bJVGmveTtFNyiAm2pjd/C92ozqEtsAN2LtvohW7qyY7JrVFoq+T36+AoW2ucFxEdFfRpZ51bFQ7duWuJ5dMNBPAoGAGrLpKLNDiutuj6Ebxt11cD2U16c0GX+3HpngNgDa9bio9K739VQfo+TG9BO0Ylk5vpHb9QyozAhAZjJetJpp1SsjS1KhxSZ1ggcCcGAQTUYOYNTzJaSZylMkTbexeb2BskA9kqog+iyEi0NOixaomc6WbMM8j9cAB/0LgHZaZAkCgYEAipqJcbvk/8S4sMW+dVjuAb3+CvsRFzxRRTWcUFPmlnBJTaRQGLrI/Qxbvsvm/LBc4ADVoTrtLxVv7TRQAZQ5CIuimYk4s13euO3lF5HRQEuAyrTj6Go4D5FKriANtRJ5q9/a6fb79qz0ZVhGfUyMawJwPWLcVqZJTND3/+795AM=';
        $merchant_private_key = "-----BEGIN PRIVATE KEY-----"."\r\n".wordwrap(trim($merchant_private_key),64,"\r\n",true)."\r\n"."-----END PRIVATE KEY-----";
        $merchant_private_key = openssl_get_privatekey($merchant_private_key);
        openssl_sign($signStr, $sign_info, $merchant_private_key, OPENSSL_ALGO_MD5);
        $signature = base64_encode($sign_info);

        //Debug时使用
        //echo "<br>".$signature."<br>";

        /////////////////////////  提交参数  ////////////////////////
        /* curl方法提交支付参数并且获取返回值 */
        $data = array(
            "brandNo" => $brandNo,
            "callbackUrl" => $callbackUrl,
            "frontUrl" => $frontUrl,
            "clientIP" => $clientIP,
            "orderNo" => $orderNo,
            "price" => $price,
            "serviceType" => $serviceType,
            "signType" => $signType,
            "signature" => $signature,
            "userName" => $userName
        );
        $params = json_encode($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $platInfo['platInfo']['p_pay_url']);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: '.strlen($params)));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);

        $data = curl_exec($ch);

        curl_close($ch);

        $result = json_decode($data, true);
        if (!$result['data']['payUrl'] || $result['code'] != 0) {
            BooFile::write('/tmp/toppay.log', $data . "\n{$payInfo['amount']}\n{$platInfo['appInfo']['name']}\n111\n" . date('Y-m-d H:i:s') . "\n\n", 'a');
            echo "支付异常，请联系客服! code：{$result['code']}，message: {$result['message']}";
            exit;
        }

        BooView::set('payUrl', "{$result['data']['payUrl']}");
        BooView::display('pay/locationPay.html');
        exit;
    }

}
