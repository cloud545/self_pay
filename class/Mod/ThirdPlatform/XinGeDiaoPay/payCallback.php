<?php
class Mod_ThirdPlatform_XinGeDiaoPay_payCallback extends Mod_ThirdPlatform_XinGeDiaoPay {

	protected function _do($platInfo = array()) {

        $data = $_POST;
        $sign = $data['fxsign'];
        $signs = md5($data['fxstatus'].$data['fxid'].$data['fxddh'].$data['fxfee'].$platInfo['platInfo']['appKey']);

        if ($sign == $signs && $data['fxstatus'] == '1') {
            echo 'success';
            return array('platOrderId' => $data['fxddh'],'realMoney' => $data['fxfee']);
        }
        return false;
	}

}
