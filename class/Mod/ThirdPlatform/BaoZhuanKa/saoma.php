<?php
class Mod_ThirdPlatform_BaoZhuanKa_saoma extends Mod_ThirdPlatform_BaoZhuanKa {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        // 宝转卡扫码
        if ($type == 9) {
            $payType = '1.0';
        }

        if ($type == 2) {
            $payType = '1.0';
        }

        // 微信转卡扫码
        if ($type == 3 || $type == 10) {
            $payType = '20.20';
        }

        // 银联扫码
        if ($type == 5) {
            $payType = '30.20';
        }

        $data = array(
            'merchantno' => $platInfo['platInfo']['appId'],
            'requestid' => $payInfo['pOrderId'],
            'amount' => number_format($payInfo['amount'], '2', '.',''),
            'servicetype' => $payType,
            'notifyurl' => $platInfo['appInfo']['callbackUrl'],
            );

        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }

        $data['sign'] = md5($md5str.'md5='.$platInfo['platInfo']['appKey']);
        header("Content-type: text/html; charset=utf-8");
        header("Location: " . $platInfo['platInfo']['p_pay_url']. "?" . http_build_query($data));
    }
}
