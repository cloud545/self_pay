<?php
class Mod_ThirdPlatform_BaoZhuanKa_payCallback extends Mod_ThirdPlatform_BaoZhuanKa {

	protected function _do($platInfo = array()) {

        $data = $_POST;
        $sign = $data['sign'];
        unset($data['sign']);
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $signs = md5($md5str.'md5='.$platInfo['platInfo']['appKey']);

        if ($sign == $signs) {
            echo 'OK';
            return array('platOrderId' => $data['requestid'],'realMoney' => $data['requestamount']);
        }
        return false;
	}

}
