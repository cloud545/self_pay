<?php
class Mod_ThirdPlatform_HSPay_payCallback extends Mod_ThirdPlatform_HSPay {
    protected $pri = 'MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDlf6hqfwMqtCQmLpZbzIJtw662akU8wzMZtmYJLIbgpGyNDUfm3ynXwGnFqxoj7IOnOuvgS7IVHlykTgd32JYEElTBX8XKEgya/fYIN6imjUfAsMLI02ynVCwffuEKDbGlmFudCe1Z36ofwgWaenDhmtSYGJxABgAzcwf/zEuUs3nEt+QgGtOt3GqTD00j9I1zN3rO8qNKTZOhIi7Nta1Thc3QcvnRcja7R8HLTBCQuZ0AWiuSc9sN8EppQbfs3h11D+aUDyoI97DQBaM9LSlBGxw3ixJhGmgJ//9/KeIMvNJnc8Q1znFsn/++mBdl4i45WHrNY+88dmBKIt0wYxDfAgMBAAECggEAaiWpV9h3NoErVx2lHcBxcyNW47gzXe9K61GZ0BRwLebe7j5+SyJc0BWG1o5VANib6/UstAlOzMXGQ5hzi9L5T9jKqsYJTXUbr3RXH2Xe82nVDiKzdP+U+a/M2KRuoZ/D0rutY+z7gt4Yaafrv49yNmbOfaqkWlhsL1+9C2sKajPYbkibAtqv4JoCOM+rTA6gQHZm5nLYrbHGyxgu8aFUW+AS+vgRvM/vTaz9BWMyzQjOSDP3ijlhUeIiNJRJ4DhrosJpZvE7PETsE7wAISgx4HeBNOFBnOnOOlv/gx9SMNZNNz5Skknpp5RAKeF+zkWpTcs+WH0vpvuCIAowqbNqwQKBgQD8d2/6WwwwAkekMTuzI7bsYTV9vpGevaTZpIJ+BR5uIRJqdJnFHHs9ZzbmDUubzvgxq/rHoamCGQzzaCJNYyIH408iMVvMKyLV/uYNb5Av2HzO4a++EvqtJnO+DCFsVP+v3kWxBl+guYEh/Ic7/AmnoPz4nJarWoZScJXF/jSNTQKBgQDote3FmALtfWtETDCriT6J51GVlSCi+4LJAy+m+LGubleyUL4BYnR5HLHXMgHfktXuQpWUMlHZ11VadgVqxSvwC2J67pp8FrgRzvXu4GSq9hXxnS78fMgj+7eMYp/2BTsF6NIa4eYq4pF0+2nmkECCpbJcZA2VvCSKvuT8Rc7w2wKBgQC136T/ig+hlkLd+cm0J4pjhzPtvhdRb/UOBQEsn6toUtLdddpAsV/mwDBAUluhhtwtjGNA5Skj28AEqYPsk/m0DQC49S3YS0rxNousKi6JCUAAGX1tRGbpzcWbp0l8rU709v7lpF3Hf1uTjEQv5kVeSQ90WfoAv1n8kbaTx6k8EQKBgGP/mZcpRUjMdkW4/8kWhts3naVLmeSvc3nxdo6nm6K7I9AqRhvONQK+Q+WXRZrPPELQNfqizO1xTOo8gQNqiwDxw3XUBmqAUMTGTIRDxL56VN+ojcJN1KsxxaP71YUgpkZZz79hh0iyZ5L/XniHUFFT5psxG21WldVRRpDpZo21AoGBANbgAb09anEtdjfueRiI34RwyoLpo7eh7kcuL0gRuTfVkd2iTyQ5gBStf6JEax217OCKTYc+ZEQHyj7LcJgVIi5CMkM/N7WkI3jXcVVBsOBEXKCEXc5IBddEB5Xt6FoaRn07IvBDVf02+xJbup5c/4taivLxFNk9rJ6LNSNi9xeU';
	protected function _do($platInfo = array())
    {

        $data = file_get_contents("php://input");
        $resultArr = $this->urlToArray($data);
        $data = urldecode($resultArr["data"]);
        $priKey = $this->privateKeyStr($this->pri);
        $arr = $this->privateDecrypt($data, $priKey);
        $result = json_decode($arr, 1);
        $sign = $result['sign'];
        unset($result['sign']);
        ksort($result);
        $signs = md5(json_encode($result).$platInfo['platInfo']['appKey']);
        if ($sign == $signs && $result['payResult'] == 'SUCCESS') {
            echo 'ok';
            return array('platOrderId' => $resultArr['orderNum'],'realMoney' => $result['amount'] / 100);
        } else {
            return false;
        }
	}

    public function privateDecrypt($data,$pr_key){

        if ($pr_key == false){
            echo "打开密钥出错";
            die;
        }
        $data = base64_decode($data);
        openssl_private_decrypt($data, $decryptData, $pr_key);
        return $decryptData;
    }

    public function urlToArray($str)
    {
        $arr1 = explode('&', $str);
        $arr = [];
        foreach ($arr1 as $key => $value) {
            $arr2 = explode('=', $value);
            $arr[$arr2[0]] = $arr2[1];
        }

        return $arr;
    }

    public function privateKeyStr($privatekey){

        $private_key = "-----BEGIN PRIVATE KEY-----\r\n";
        foreach (str_split($privatekey,64) as $str){
            $private_key .= $str . "\r\n";
        }
        $private_key .="-----END PRIVATE KEY-----";

        return $private_key;
    }
}
