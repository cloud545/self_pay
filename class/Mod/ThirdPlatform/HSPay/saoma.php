<?php
class Mod_ThirdPlatform_HSPay_saoma extends Mod_ThirdPlatform_HSPay {

    protected $pub = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwvISfMCA24hLiTuaqfEFudDE0i10+Mz1dfI381FSya7zjUh+D4f4SqP6o7e6K5EhwZJLEs1dJ/rvqSL8+UeDQq2qpy+9xni9gUKCJXbPTE+WrZwrN7mhgZcXMEiIgC5b/SqJ8mdaxewO4NJg0NfvV0B1kvvjM+VOny0Z3oGkZd9pt5gDU9sAmOElYbDSTrynM9IzhJ5TGGCAwYH446m8dp8XDvSzguw4yLQqt2yyLtsOAYMrXIkHf5nDvck9wfI+78qxqgmxwjKRXj0Wlj0uPBvvHZgF0eTi9DJzWVGcOQ4ISGsl9HHXzwv1pz4T9W0756+avC+3P3wJe3Xp1sCyJQIDAQAB';

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;

        if ($type == 9) {
            $payType = 'alipay_gm';
        }

        if($type == 10) {
            $payType = 'weixin_gm';
        }

        if ($type == 2 || $type == 7) {
            $payType = 'alipaywap';
        }

        if($type == 3 || $type == 6) {
            $payType = 'wxh5';
        }

        $headerKey = 'H191226214730978922036';
        $data = array(
            'apiVersion' => md5("1.0.0".$headerKey),
            'orderType' => $payType,
        );

        $request = [
            'merchNo' => $platInfo['platInfo']['appId'],
            'orderSn' => $payInfo['pOrderId'],
            'amount' => $payInfo['amount']*100 ,
            'callBackUrl' => $platInfo['appInfo']['callbackUrl'],
        ];

        ksort($request);
        $pubKey = $this->publicKeyStr($this->pub);
        $md5str = json_encode($request).$platInfo['platInfo']['appKey'];
        $request['sign'] = md5($md5str);
        $data['requestBody'] = urlencode($this->encrypt($pubKey, json_encode($request)));
        $str = '';
        foreach ($data as $key => $value) {
            $str = $str . $key . '=' .$value . '&';
        }

        $str = substr($str, 0, strlen($str) - 1);
        $result = $this->streamContextCreate($platInfo['platInfo']['p_pay_url'], $str, $headerKey);

        if ($result['stateCode'] == '200') {
            header('Location:'.$result['qrcodeUrl']);
        } else {
            echo $result['message'];
        }
    }

    public function encrypt($key, $data) {

        $original_arr = str_split($data,117);
        foreach($original_arr as $o) {
            $sub_enc = null;
            openssl_public_encrypt($o,$sub_enc,$key);
            $original_enc_arr[] = $sub_enc;
        }

        openssl_free_key($key);
        $original_enc_str = base64_encode(implode('',$original_enc_arr));
        return $original_enc_str;
    }

    public function streamContextCreate($url,$dataStr, $headerKey){
        $httpData["method"] = "POST";
        $httpData["header"] = "Content-type: application/x-www-form-urlencoded\r\n"."securityHeaderKey: ".$headerKey."";
        $httpData["content"] = $dataStr;
        $http["http"]=$httpData;
        $resultData = "";
        try {
            $context = stream_context_create($http);
            $result = file_get_contents($url, false, $context);
            $resultData= json_decode($result,true);
        } catch (Exception $e) {
            throw $e;
        }
        return $resultData;
    }

    public function publicKeyStr($publicStr){
        //公钥
        $public_key = "-----BEGIN PUBLIC KEY-----\r\n";
        foreach (str_split($publicStr,64) as $str){
            $public_key .= $str . "\r\n";
        }
        $public_key .="-----END PUBLIC KEY-----";

        return $public_key;

    }
}
