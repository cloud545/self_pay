<?php
class Mod_ThirdPlatform_ODPPay extends Mod_ThirdPlatform {
    // 私钥;
    public $private_key = '-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQCBU645jkNMHW1gcWGkHlomlVv4fsmpyCB7ORSyJWJA+GYmgFD8
Ps6iNGVg8y/TY/HsiVIeJZNiIuuqoy/0W2hKKZTf/WQ/RoOgwuYSUD08ejIFyUVu
PmN76lvIbm/PDkYt/ndstJGnOlyKB7w05j5KnK2WHdPKTIKlNKhB6GTSCQIDAQAB
AoGAG1PLmZlcUpHYShwDOhr4yTTf14ExCzmA1ZDg9OjN23UxnpqYIkkCQEeqkWMp
vLUeinEU405k14SSAD9V04BaJ5w93F9Chy9WAFU8YRsx1j6BdsUnqC5LAq5JYfP2
/PiTw4UYuljBAqw/m4w2W5kfn3j/c/NZ2tOX2w9O9+7qPkUCQQCNE7IICyXy78DO
ivR7pTQ0T5vtokTaELUNOpverShqd41HDo+3LfL+4ZDx3hFGWdmQYZkoZ1Ootpcz
FzD79D0NAkEA6q2eNCWqwlHabpm7WVt/AecRIIzbTF7/IUSk+mhe5ObNGqgn8lUy
zBUml55ktIhYw6qWRtXc/AJkSXMFkOhB7QJALq4GqXyu2NyV6oG19berN2fkC1Q2
/gSrM65h5uNazSezuoBJ822jysxuPmmhQ6r1Pz3cBK9z0g5gp0oULjoZfQJBAIB7
HSoflSVd+uCiedeqKBa6kr3nYAin2bY1Br2xdoKLea8mcPhTfMQOsFpB9ZCBNVeq
hSuy2pF+Hbm4p0t2XOECQAIsp3GIk7JpAygDhtO6E4mmaEbPsZ2XzCI2ndoWCjL/
3X1Ccynef4kLzWl8tamx+h6GjVKNuNXLW4QcRr9Pm5o=
-----END RSA PRIVATE KEY-----';
    //公钥
    public $public_key = '-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCBU645jkNMHW1gcWGkHlomlVv4
fsmpyCB7ORSyJWJA+GYmgFD8Ps6iNGVg8y/TY/HsiVIeJZNiIuuqoy/0W2hKKZTf
/WQ/RoOgwuYSUD08ejIFyUVuPmN76lvIbm/PDkYt/ndstJGnOlyKB7w05j5KnK2W
HdPKTIKlNKhB6GTSCQIDAQAB
-----END PUBLIC KEY-----';

    //上游公钥
    public $xw_public_key = '-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDDwuGpz1SpqJIU8zLULAriMN0P
gxzajwQI24wdg5t4uso4mJbWwr981cYnlUb/UXmtQeQeKAjY+R5Qe7dBcQEpP/OF
XBfVqJUwBKckgUYUTClXCX59Tl9AG+oBaGPUGMpzJ4vlg7Zd6juKPCis5LZi1oHJ
R0ryjiUr//yKqxLkRwIDAQAB
-----END PUBLIC KEY-----';

	
	public $pi_key;
    public $pu_key;
    public $xw_pu_key;

		//判断公钥和私钥是否可用
		public function __construct()
		{
				$this->pi_key =  openssl_pkey_get_private($this->private_key);//这个函数可用来判断私钥是否是可用的，可用返回资源id Resource id
				$this->pu_key = openssl_pkey_get_public($this->public_key);//这个函数可用来判断公钥是否是可用的
				$this->xw_pu_key = openssl_pkey_get_public($this->xw_public_key);//这个函数可用来判断公钥是否是可用的
		}
		/**
		 * @param $data 待签名字符串
		 * @param $privateKey
		 * @return string 生成的签名
		 */
		public function generateSign($data){
				$signature='';
				openssl_sign($data,$signature,$this->pi_key);
				//openssl_free_key($this->pi_key);
				return bin2hex($signature);
		}

		//公钥加密
		public function PublicEncrypt($data){
			$crypto = '';
			foreach (str_split($data, 117) as $chunk) {
				openssl_public_encrypt($chunk, $encryptData, $this->xw_pu_key, OPENSSL_PKCS1_PADDING);
				$crypto .= $encryptData;
			}
			return base64_encode($crypto);
		}
	
		//私钥解密
		public function PrivateDecrypt($encrypted)
		{
			$crypto = '';
			foreach (str_split(base64_decode($encrypted), 128) as $chunk) {
				openssl_private_decrypt($chunk, $decryptData, $this->pi_key);
				$crypto .= $decryptData;
			}
			return $crypto;
		}

		/**
		 * @param $data 待验签数据
		 * @param $sign 签名字符串
		 * @param $publicKey
		 * @return bool
		 */
		public function veritySign($data,$sign){
			$result = openssl_verify($data,hex2bin($sign),$this->xw_pu_key);
			return (bool)$result;
		}
}
