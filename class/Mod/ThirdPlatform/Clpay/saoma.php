<?php
class Mod_ThirdPlatform_Clpay_saoma extends Mod_ThirdPlatform_Clpay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝wap
            $payType = 'alipay_wap';
        } elseif ($type == 3) { // 微信wap
            $payType = 'wechat_wap';
        } elseif ($type == 4) { // qq
            $payType = 'qq_scan';
        } elseif ($type == 5) { // 银联扫码
            $payType = 'union_scan';
        } elseif ($type == 6) { // 京东
            $payType = 'jd_scan';
        } elseif ($type == 9) { // 支付宝扫码
            $payType = 'alipay_scan';
        } elseif ($type == 10) { // 微信扫码
            $payType = 'wechat_scan';
        }

        $pay = array();
        $pay['orderAmt'] = $payInfo['amount'] * 100;
        $pay['goodsName'] = "商品";
        $pay['mercNo'] = $platInfo['platInfo']['appId'];
        $pay['pageReturnUrl'] = $platInfo['appInfo']['clientUrl'];
        $pay['notifyUrl'] = $platInfo['appInfo']['callbackUrl'];
        $pay['orderNo'] = $payInfo['pOrderId'];

        $md5Str = '';
        foreach ($pay as $key => $value) {

            if (!$value) {
                continue;
            }

            if (!$md5Str) {
                $md5Str .= "{$key}={$value}";
            } else {
                $md5Str .= "&{$key}={$value}";
            }
        }

        $sign = md5($md5Str . "{$platInfo['platInfo']['appKey']}");
        $pay['sign'] = strtoupper($sign);

        $httpParam = base64_encode("{$md5Str}&sign={$sign}");
        $httpParam = str_replace(array('+', '/'), array('=J=', '=X='), $httpParam);

        BooView::set('payUrl', "{$platInfo['platInfo']['p_pay_url']}?data={$httpParam}");
        BooView::display('pay/locationPay.html');
        exit;
    }

}
