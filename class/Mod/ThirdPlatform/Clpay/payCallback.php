<?php
class Mod_ThirdPlatform_Clpay_payCallback extends Mod_ThirdPlatform_Clpay {

	protected function _do($platInfo = array()) {

        $data = array();
        $data['result'] = $_REQUEST['result'];
        $data['code'] = $_REQUEST['code'];
        $data['message'] = $_REQUEST['message'];
        $data['mercNo'] = $_REQUEST['mercNo'];
        $data['channel'] = $_REQUEST['channel'];
        $data['date'] = $_REQUEST['date'];
        $data['orderAmt'] = $_REQUEST['orderAmt'];
        $data['orderNo'] = $_REQUEST['orderNo'];
        $data['orderState'] = $_REQUEST['orderState'];

        $md5Str = '';
        foreach ($data as $key => $value) {

            if (!$value) {
                continue;
            }

            if (!$md5Str) {
                $md5Str .= "{$key}={$value}";
            } else {
                $md5Str .= "&{$key}={$value}";
            }
        }

        $sign = md5($md5Str . "{$platInfo['platInfo']['appKey']}");
        $signature = strtoupper($sign);

        if($_REQUEST['sign'] == $signature && $data['result'] == 'true' && $data['orderState'] == '01'){
            echo '{"success":"true"}';
            return array('platOrderId' => $data['orderNo'], 'realMoney' => $data['orderAmt'] / 100);
        } else {
            return false;
        }

	}

}
