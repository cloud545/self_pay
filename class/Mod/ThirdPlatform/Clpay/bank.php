<?php
class Mod_ThirdPlatform_Clpay_bank extends Mod_ThirdPlatform_Clpay {

	protected function _do($platInfo = array(), $payInfo = array()){

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        if ($type == 1) {// 网银
            $payType = 'bank_pay';
        } elseif ($type == 8) {// 银联快捷
            $payType = 'quick_pay';
        } else {
            $payType = 'bank_pay';
        }

        $pay = array();
        $pay['orderAmt'] = $payInfo['amount'] * 100;
        $pay['goodsName'] = "商品";
        $pay['mercNo'] = $platInfo['platInfo']['appId'];
        $pay['pageReturnUrl'] = $platInfo['appInfo']['clientUrl'];
        $pay['notifyUrl'] = $platInfo['appInfo']['callbackUrl'];
        $pay['orderNo'] = $payInfo['pOrderId'];

        $md5Str = '';
        foreach ($pay as $key => $value) {

            if (!$value) {
                continue;
            }

            if (!$md5Str) {
                $md5Str .= "{$key}={$value}";
            } else {
                $md5Str .= "&{$key}={$value}";
            }
        }

        $sign = md5($md5Str . "{$platInfo['platInfo']['appKey']}");
        $pay['sign'] = strtoupper($sign);

        $httpParam = base64_encode("{$md5Str}&sign={$sign}");
        $httpParam = str_replace(array('+', '/'), array('=J=', '=X='), $httpParam);

        echo '<pre>';
        var_dump($md5Str . "{$platInfo['platInfo']['appKey']}", "{$md5Str}&sign={$sign}", $httpParam, "{$platInfo['platInfo']['p_pay_url']}?data={$httpParam}");
        exit;

        BooView::set('payUrl', "{$platInfo['platInfo']['p_pay_url']}?data={$httpParam}");
        BooView::display('pay/locationPay.html');
        exit;
	}


}
