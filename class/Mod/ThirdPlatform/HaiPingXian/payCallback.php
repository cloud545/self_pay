<?php
class Mod_ThirdPlatform_HaiPingXian_payCallback extends Mod_ThirdPlatform_HaiPingXian {

	protected function _do($platInfo = array()) {

        $data = $_POST;
        $sign = $data['sign'];
        unset($data['sign']);

        ksort($data);
        $md5str = "";
        foreach ($data as $key => $val) {
            $md5str = $md5str . $key . "=" . $val . "&";
        }
        $signs = md5($md5str . "key=" . $platInfo['platInfo']['appKey']);
        $status = $this->getStatus($platInfo['platInfo']['appId'] , $data['sh_order'], $platInfo['platInfo']['appKey']);
        if ($sign == $signs && $data['status'] == 'success' && $status == 'ok') {
            echo 'success';
            return array('platOrderId' => $data["sh_order"],'realMoney' => $data["money"]);
        }

        return false;
	}

	public function getStatus($id , $order , $appKey) {
	    $data['mch_id'] = $id;
	    $data['out_order_sn'] = $order;
	    $data['time'] = time();
        ksort($data);
        $md5str = "";
        foreach ($data as $key => $val) {
            $md5str = $md5str . $key . "=" . $val . "&";
        }
        $data['sign'] = md5($md5str . "key=" . $appKey);
        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, 'http://hp.angeltutu.net/?c=Pay&a=query'); // 要访问的地址
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        $result = json_decode(curl_exec($curl),1);
        curl_close($curl); // 关闭CURL会话
        if ($result['data']['status'] == 9) {
            return 'ok';
        }
    }
}
