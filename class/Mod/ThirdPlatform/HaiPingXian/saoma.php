<?php
class Mod_ThirdPlatform_HaiPingXian_saoma extends Mod_ThirdPlatform_HaiPingXian {

protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];

        $payType  = 0;
        if ($type == 9 || $type == 2) { // 微信wap
            $payType = '1';
        }
		
        //参与验签的参数
        $data = [
            'mch_id'  	    => $platInfo['platInfo']['appId'],	                  //商户号
            'order_sn'  	=> $payInfo['pOrderId'],	                  //订单号
            'money'         => number_format($payInfo['amount'], 2 , '.', ''),	//订单
            'format'        => 'page',		//提交时间
            'ptype'  	    => $payType,                                                 //银行编码
            'notify_url'  	=> $platInfo['appInfo']['callbackUrl'],                   //服务端通知
            'time'  	    =>  time(),	                   //页面跳转通知
            'goods_desc'  	=>  'test',	                   //页面跳转通知
            'client_ip'  	=>  $_SERVER['REMOTE_ADDR'],	                   //页面跳转通知
        ];

        ksort($data);
        $md5str = "";
        foreach ($data as $key => $val) {
            $md5str = $md5str . $key . "=" . $val . "&";
        }
        $data['sign'] = md5($md5str . "key=" . $platInfo['platInfo']['appKey']);
        echo $md5str . "key=" . $platInfo['platInfo']['appKey'];

        $str='<form class="form-inline" name="payform" method="post" action="'.$platInfo['platInfo']['p_pay_url'].'">';
        foreach ($data as $key => $val) {
            $str.='<input type="hidden" name="' . $key . '" value="' . $val . '">';
        }
        $str.="</form><script>document.forms['payform'].submit();</script>";
        exit($str);
    }
}
