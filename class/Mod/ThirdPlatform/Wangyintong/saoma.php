<?php
class Mod_ThirdPlatform_Wangyintong_saoma extends Mod_ThirdPlatform_Wangyintong {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝wap
            $payType = 1002;
        } elseif ($type == 3) { // 微信wap
            $payType = 1001;
        } elseif ($type == 5) { // 银联
            $payType = 1009;
        } elseif ($type == 9) { // 支付宝扫码
            $payType = 1002;
        } elseif ($type == 10) { // 微信扫码
            $payType = 1001;
        }

        $postData = array();
        $postData['merId'] = $platInfo['platInfo']['appId'];
        $postData['ordId'] = $payInfo['pOrderId'];
        $postData['amount'] = $payInfo['amount'] * 100;
        $postData['payType'] = $payType;
        $postData['body'] = $payInfo['pOrderId'];
        $postData['synUrl'] = $platInfo['appInfo']['callbackUrl'];

        $sign = md5("{$postData['merId']}{$postData['ordId']}{$postData['amount']}{$postData['payType']}{$platInfo['platInfo']['appKey']}");
        $postData['sign'] = $sign;

        BooCurl::setData($postData, 'POST');
        $response = BooCurl::call($platInfo['platInfo']['p_pay_url']);

        $result = json_decode($response, true);
        if ($result['responseCode'] != 0) {
            BooFile::write('/tmp/wangyintong.log', $response . "\n{$payInfo['amount']}\n{$platInfo['appInfo']['name']}\n111\n\n", 'a');
            echo "支付异常，请联系客服! responseCode：{$result['responseCode']}，responseMsg: {$result['responseMsg']}";
            exit;
        }

        BooView::set('url', $result['data']['codeurl']);
        BooView::display('pay/saoma.html');
        exit;
    }

}
