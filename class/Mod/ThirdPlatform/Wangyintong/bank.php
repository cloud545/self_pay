<?php
class Mod_ThirdPlatform_Wangyintong_bank extends Mod_ThirdPlatform_Wangyintong {

	protected function _do($platInfo = array(), $payInfo = array()){

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        if ($type == 1) {// 网银
            $payType = '1014';
        } elseif ($type == 8) {// 银联快捷
            $payType = '1016';
        } else {
            $payType = '1014';
        }

        $postData = array();
        $postData['merId'] = $platInfo['platInfo']['appId'];
        $postData['ordId'] = $payInfo['pOrderId'];
        $postData['amount'] = $payInfo['amount'] * 100;
        $postData['payType'] = $payType;
        $postData['body'] = $payInfo['pOrderId'];
        $postData['synUrl'] = $platInfo['appInfo']['callbackUrl'];
        $postData['selfParam'] = $payInfo['pOrderId'];

        $sign = md5("{$postData['merId']}{$postData['ordId']}{$postData['amount']}{$postData['payType']}{$platInfo['platInfo']['appKey']}");
        $postData['sign'] = $sign;

        BooCurl::setData($postData, 'POST');
        $response = BooCurl::call($platInfo['platInfo']['p_pay_url']);
        $result = json_decode($response, true);

        echo $result['data']['codeurl'];

        //BooView::set('payUrl', $result['data']['PayUrl']);
        //BooView::display('pay/locationPay.html');
        exit;
	}


}
