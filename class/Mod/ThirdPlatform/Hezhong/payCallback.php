<?php
class Mod_ThirdPlatform_Hezhong_payCallback extends Mod_ThirdPlatform_Hezhong {

	protected function _do($platInfo = array()) {

        $postStr = file_get_contents("php://input");
        $postStr = substr($postStr, 0, strlen($postStr) - 1);
        $postStr = substr($postStr, 1);
        $post = json_decode($postStr, true);

        //拼接字符串
        $st = 'v_pagecode='.$post['v_pagecode'].'&v_mid='.$post['v_mid'].'&v_oid='.$post['v_oid'].'&v_orderid='.$post['v_orderid'].'&v_result='.$post['v_result'].'&v_value='.$post['v_value'].$platInfo['platInfo']['appKey'];
        //SHA1加密并转换为大写
        $sign = strtoupper(sha1($st));
        if ($sign == $post["v_sign"] && $post['v_result'] == 2000) {
            echo '[{result:"ok"}]';
            return array('platOrderId' => $post["v_orderid"],'realMoney'=>$post['v_realvalue']);
        }else {
            //签名验证失败
            return false;
        }

	}

}
