<?php
class Mod_ThirdPlatform_Hezhong_saoma extends Mod_ThirdPlatform_Hezhong {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝wap
            $payType = 1009;
        } elseif ($type == 3) { // 微信wap
            $payType = 1011;
        } elseif ($type == 5) { // 银联
            $payType = 1081;
        } elseif ($type == 9) { // 支付宝扫码
            $payType = 1080;
        } elseif ($type == 10) { // 微信扫码
            $payType = 1003;
        }

        $pt = array(
            'v_pagecode' => $payType,  //协议号
            'v_mid' => $platInfo['platInfo']['appId'],    //商户号
            'v_oid' => $payInfo['pOrderId'],   //商户订单编号，该参数格式为：订单生日期-商户编号-商户流水号。例如：20100101-888-12345。商流水号为数字，每日内不可重复，并且不能包括除数字、英文字母和“-”外以其它字符。流水号可为一组也可以用“-”间隔成几组。
            'v_rcvname' => $platInfo['platInfo']['appId'],   //收货人姓名，考虑到系统编码可能不统一的问题，建议统一用商户编号的值代替。
            'v_rcvaddr' => $platInfo['platInfo']['appId'],   //收货人地址，建议使用商户编号的值代替
            'v_goodsname' => $platInfo['platInfo']['appId'],  //商品名称
            'v_goodsdescription' => $platInfo['platInfo']['appId'], //商品描述
            'v_rcvtel' => $platInfo['platInfo']['appId'],    //收货人电话，建议使用商户编号的值代替
            'v_rcvpost' => $platInfo['platInfo']['appId'],   //收货人邮政编码
            'v_qq' => $platInfo['platInfo']['appId'],   //收货人QQ
            'v_amount' => number_format($payInfo['amount'],2,'.',''), //订单总金额
            "v_ymd"=>date("YmdHis"),//订单产生日期，格式为yyyymmdd，例如：20100101
            'v_orderstatus' => "1",   //0为未配齐，1为已配齐；一般商户该参数无实际意义，建议统一配置为1（已配齐）
            'v_ordername' => $platInfo['platInfo']['appId'], //订货人姓名，建议统一用商户编号的值代替。
            'v_bankno' => '0000', //银行编码
            'v_moneytype' => "0", //0为人民币，1为美元，2为欧元，3为英镑，4为日元，5为韩元，6为澳大利亚元，7为卢布，8为瑞士法郎，9为港币，10为新加坡元，11为澳门元。
            'v_url' => $platInfo['appInfo']['callbackUrl'], //异步通知接收地址
            'v_noticeurl' => $platInfo['appInfo']['clientUrl'], //同步跳转地址
            'v_app' => $isMobile ? "APP" : "web",  //APP或者web支付
        );

        //拼接字符串
        $st = 'v_pagecode='.$pt['v_pagecode'].'&v_mid='.$pt['v_mid'].'&v_oid='.$pt['v_oid'].'&v_amount='.$pt['v_amount'].'&v_ymd='.$pt['v_ymd'].'&v_bankno='.$pt['v_bankno'].$platInfo['platInfo']['appKey'];
        //SHA1加密并转换为大写
        $sign = strtoupper(sha1($st));
        $pt['v_sign'] = $sign;

        //待发送数据
        $data = '['.json_encode($pt).']';

        //使用方法
        $rt = $this->post($platInfo['platInfo']['p_pay_url'], $data);
        $rarr = json_decode($rt,true);
        $p_pay_url = $rarr[0]["returnMsg"];

        if ($p_pay_url) {
            BooView::set('payUrl', $p_pay_url);
            BooView::display('pay/locationPay.html');
        } else {
            echo $rt;
        }

        exit;
    }

}
