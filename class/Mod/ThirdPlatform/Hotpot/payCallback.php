<?php
class Mod_ThirdPlatform_Hotpot_payCallback extends Mod_ThirdPlatform_Hotpot {

	protected function _do($platInfo = array()) {

        $returnArray = array( // 返回字段
            "memberid" => $_REQUEST["memberid"], // 商户ID
            "orderid" =>  $_REQUEST["orderid"], // 订单号
            "amount" =>  $_REQUEST["amount"], // 交易金额
            "datetime" =>  $_REQUEST["datetime"], // 交易时间
            "transaction_id" =>  $_REQUEST["transaction_id"], // 支付流水号
            "returncode" => $_REQUEST["returncode"],
        );
        $md5key = $platInfo['platInfo']['appKey'];
        ksort($returnArray);
        reset($returnArray);
        $md5str = "";
        foreach ($returnArray as $key => $val) {
            $md5str = $md5str . $key . "=" . $val . "&";
        }
        $sign = strtoupper(md5($md5str . "key=" . $md5key));
        if ($sign == $_REQUEST["sign"]) {
            if ($_REQUEST["returncode"] == "00") {
                echo 'OK';
                return array('platOrderId' => $_REQUEST["transaction_id"],'realMoney' => $_REQUEST["amount"]);
            }
        }

        return false;
	}

}
