<?php
class Mod_ThirdPlatform_YunPay_bank extends Mod_ThirdPlatform_YunPay {

	protected function _do($platInfo = array(), $payInfo = array()){

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $time = time();
        $post = BooVar::requestx();
        $data = array();
        $data['appId'] = $post['appId'];
        $data['payType'] = $post['payType'];
        $data['amount'] = $payInfo['amount'];
        $data['orderId'] = $payInfo['orderId'];
        $data['pAutoOrderId'] = $payInfo['pAutoOrderId'];
        $data['pOrderId'] = $payInfo['pOrderId'];
        $data['clientUrl'] = $platInfo['appInfo']['clientUrl'];
        $data['time'] = $time;
        $sign = $this->getSign($data, $platInfo['appInfo']['app_key']);

        BooView::set('appId', $post['appId']);
        BooView::set('payType', $post['payType']);
        BooView::set('amount', $payInfo['amount']);
        BooView::set('orderId', $payInfo['orderId']);
        BooView::set('pOrderId', $payInfo['pOrderId']);
        BooView::set('pAutoOrderId', $payInfo['pAutoOrderId']);
        BooView::set('clientUrl', $platInfo['appInfo']['clientUrl']);
        BooView::set('time', $time);
        BooView::set('sign', $sign);
        BooView::set('bankCodeList', $platInfo['platInfo']['bankCode']);
        BooView::display('pay/bank.html');
        exit;
	}


}
