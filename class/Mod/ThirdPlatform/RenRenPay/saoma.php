<?php
class Mod_ThirdPlatform_RenRenPay_saoma extends Mod_ThirdPlatform_RenRenPay {

    private $hex_iv = '00000000000000000000000000000000';
    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();
        $payType = 0;

        $aseKey = $platInfo['platInfo']['appSecret'];
        $appKey = $platInfo['platInfo']['appKey'];

        if ($type == 2 || $type == '25' || $type == '21') {
            $payType = 'ALIPAYH5';
        }

        if ($type == '9' || $type == '26' || $type == '20') {
            $payType = 'ALIPAY';
        }

        $data = array(
            'MerchantAccount' => $platInfo['platInfo']['appId'],
            'Action' => 'deposit',
        );

        $arr = [
            'MerchantOrderNo' => $payInfo['pOrderId'],
            'OrderPrice' => $payInfo['amount'],
            'ChannelCode' => $payType,
            'CallBackURL' => $platInfo['appInfo']['callbackUrl'],
            'Message' => 'deposit',
            'PlatFormUserID' => '1',
        ];

        $data['Data'] = strtoupper(bin2hex(openssl_encrypt(json_encode($arr), 'AES-256-ECB', $aseKey, OPENSSL_RAW_DATA, $this->hexToStr($this->hex_iv))));

        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . urlencode($value) .'&';
        }

        $data['Sign'] = md5($md5str . 'Key=' .$appKey);

        $data = json_encode($data);
        $ch = curl_init($platInfo['platInfo']['p_pay_url']);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data))
        );
        $result = json_decode(curl_exec($ch),1);
        if ($result['Code'] == 0) {
            $data = json_decode(openssl_decrypt(hex2bin($result['Result']), 'AES-256-ECB', $aseKey, OPENSSL_RAW_DATA, $this->hexToStr($this->hex_iv)), 1);
            header('location:'.$data['URL']);
        }
    }

    public function hexToStr($hex) {
        $str = '';
        for($i=0; $i<strlen($hex)-1; $i+=2) {
            $str .= chr(hexdec($hex[$i].$hex[$i+1]));
        }
        return $str;
    }
}
