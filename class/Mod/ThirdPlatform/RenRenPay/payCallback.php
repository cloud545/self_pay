<?php
class Mod_ThirdPlatform_RenRenPay_payCallback extends Mod_ThirdPlatform_RenRenPay {

    private $hex_iv = '00000000000000000000000000000000';
	protected function _do($platInfo = array()) {
        $data = json_decode(file_get_contents("php://input"),1);

        $sign = $data['Sign'];
        unset($data['Sign']);
        $appKey = $platInfo['platInfo']['appKey'];
        $aseKey = $platInfo['platInfo']['appSecret'];

        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $signs = md5($md5str . 'Key=' . $appKey);

        $arr = json_decode(openssl_decrypt(hex2bin($data['Result']), 'AES-256-ECB', $aseKey, OPENSSL_RAW_DATA, $this->hexToStr($this->hex_iv)), 1);


        if ($sign == $signs && $arr['OrderState'] == '6') {
            echo 'SUCCESS';
            return array('platOrderId' => $arr['MerchantOrderNo'],'realMoney' => $arr['OrderRealPrice']);
        }
        return false;
	}

    public function hexToStr($hex) {
        $str = '';
        for($i=0; $i<strlen($hex)-1; $i+=2) {
            $str .= chr(hexdec($hex[$i].$hex[$i+1]));
        }
        return $str;
    }
}
