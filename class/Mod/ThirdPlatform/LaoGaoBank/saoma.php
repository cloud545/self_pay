<?php
class Mod_ThirdPlatform_LaoGaoBank_saoma extends Mod_ThirdPlatform_LaoGaoBank {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();
        $payType = 0;

        if ($type == 5) {
            $payType = 'unionpay_qrcode_fix';
        }
		
        $data = array(
            'uid' => $platInfo['platInfo']['appId'],
            'channel' => $payType,
            'outTradeNo' => $payInfo['pOrderId'],
            'money' => number_format($payInfo['amount'], 2, '.', '') ,
            'notifyUrl' => $platInfo['appInfo']['callbackUrl'],
            'returnUrl' => 'http://www.baidu.com',
            'timestamp' => $this->getMillisecond(),
            'token' => $platInfo['platInfo']['appKey'],
        );
		
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $md5str = substr($md5str , 0 , strlen($md5str) - 1);
        $data['sign'] = strtoupper(md5($md5str));
        unset($data['token']);
        $data = http_build_query(
            $data
        );
        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => $data
            )
        );
        $context = stream_context_create($opts);
        $result = file_get_contents($platInfo['platInfo']['p_pay_url'], false, $context);
        $result = json_decode($result,1);
        if ($result['code'] == 0) {
            header('location:'.$result['data']['payUrl']);
        } else {
            echo $result['msg'];
        }
    }

    public function getMillisecond() {
        list($s1, $s2) = explode(' ', microtime());
        return (float)sprintf('%.0f', (floatval($s1) + floatval($s2)) * 1000);
    }
}
