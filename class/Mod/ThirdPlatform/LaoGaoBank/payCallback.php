<?php
class Mod_ThirdPlatform_LaoGaoBank_payCallback extends Mod_ThirdPlatform_LaoGaoBank {

	protected function _do($platInfo = array()) {

	    $data = $_POST;
	    $sign = $data['sign'];
	    unset($data['sign']);
	    $data['token'] = $platInfo['platInfo']['appKey'];
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            if ($value) {
                $md5str = $md5str . $key . '=' . $value .'&';
            }
        }
        $md5str = substr($md5str , 0 , strlen($md5str) - 1);
        $signs = strtoupper(md5($md5str));
        if ($sign == $signs) {
            echo 'SUCCESS';
            return array('platOrderId' => $data['outTradeNo'],'realMoney' => $data['amount']);
        }else{
            return false;
        }
    }
}
