<?php
class Mod_ThirdPlatform_UGCPay_saoma extends Mod_ThirdPlatform_UGCPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 3;
        if ($type == 2) { // 支付宝
            $payType = 1007;
        } elseif ($type == 3) { // 微信
            $payType = 2001;
        } elseif ($type == 4) { // qq
            $payType = $isMobile ? 8010 : 8009;
        } elseif ($type == 5) { // 银联
            $payType = 8014;
        } elseif ($type == 9) { // 支付宝扫码
            $payType = 1007;
        } elseif ($type == 10) { // 微信扫码
            $payType = 2001;
        }

        $amount = $payInfo["amount"] * 1 * 100; //元转换为分
        $paramArray = array(
            "mchId" => $platInfo['platInfo']['appId'], //商户ID
            "appId" => $platInfo['platInfo']['appSecret'], //应用ID 请根据实际配置传参
            "productId" => $payType,  //支付产品ID
            "mchOrderNo" => $payInfo['pOrderId'] ,  // 商户订单号
            "currency" => 'cny',  //币种
            "amount" => $amount . "", // 支付金额
            "clientIp" => BooUtil::realIp(),   //客户端IP
            "device" => '',    //客户端设备
            "returnUrl" => $platInfo['appInfo']['clientUrl'],	 //支付结果前端跳转URL
            "notifyUrl" => $platInfo['appInfo']['callbackUrl'],	 //支付结果后台回调URL
            "subject" => '网络购物',	 //商品主题
            "body" => '网络购物',	 //商品描述信息
            "param1" => '',	 //扩展参数1
            "param2" =>  '',	 //扩展参数2
            "extra" =>  ''	 //附加参数
        );

        $sign = $this->paramArraySign($paramArray, $platInfo['platInfo']['appKey']);  //签名
        $paramArray["sign"] = $sign;

        $paramsStr = json_encode($paramArray); //请求参数str
        $response = $this->httpPost($platInfo['platInfo']['p_pay_url'], "params=" . $paramsStr);
        $result = json_decode($response, true);
        if ($result['retCode'] != 'SUCCESS') {
            BooFile::write('/tmp/ugc.log', $response . "\n{$payInfo['amount']}\n{$platInfo['appInfo']['name']}\n111\n\n", 'a');
            echo "支付异常，请联系客服! errCode：{$result['errCode']}，errDes: {$result['errDes']}";
            exit;
        }

        header('Location: ' . $result['payUrl']);
        exit;
    }

}
