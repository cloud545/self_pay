<?php
class Mod_ThirdPlatform_WanTengPay_saoma extends Mod_ThirdPlatform_WanTengPay {

protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];

        $payType  = 0;
        if ($type == 10) {
            $payType = '919';
        }
		
        //参与验签的参数
        $postData = [
            'pay_memberid'  	=> $platInfo['platInfo']['appId'],	                  //商户号
            'pay_orderid'  	=> $payInfo['pOrderId'],	                  //订单号
            'pay_amount'               => number_format($payInfo['amount'] ,2,'.',''),	//订单
            'pay_applydate'           => date("Y-m-d H:i:s"),		//提交时间
            'pay_bankcode'  	=> $payType,                                                 //银行编码
            'pay_notifyurl'  	=> $platInfo['appInfo']['callbackUrl'],                   //服务端通知
            'pay_callbackurl'  	=> $platInfo['appInfo']['clientUrl'],	                   //页面跳转通知
        ];

        ksort($postData);
        $md5str = "";
        foreach ($postData as $key => $val) {
            $md5str = $md5str . $key . "=" . $val . "&";
        }
        $postData['pay_md5sign'] = strtoupper(md5($md5str . "key=" . $platInfo['platInfo']['appKey']));
        $postData['pay_productname']    = 'best1pays';

        $formstr = '';
        foreach ($postData as $key => $val) {
            $formstr .= '<input type="hidden" name="' . $key . '" value="' . $val . '">';
        }

        echo '
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            </head>
            <body onLoad="document.zlinepay.submit();">
                <form name="zlinepay" method="post" action="' . $platInfo['platInfo']['p_pay_url'] . '" target="_top"/>'.$formstr.'</form>
            </body>
        </html>
        ';
        exit;
    }
}
