<?php
class Mod_ThirdPlatform_ChangZhiFu_payCallback extends Mod_ThirdPlatform_ChangZhiFu {

	protected function _do($platInfo = array()) {
        $data = $_POST;

        $sign = $_POST['sign'];
        unset($data['sign']);
        $data['mch_key'] = $platInfo['platInfo']['appKey'];
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            if($value) {
                $md5str = $md5str . $key . '=' . $value .'&';
            }
        }


        $signs = md5(substr($md5str,0 , strlen($md5str)-1));

        if ($sign == $signs && $data['status'] == '2') {
            echo 'success';
            return array('platOrderId' => $_POST['mch_order'],'realMoney' => $_POST['amt']/1000);
        }
        return false;
	}

}
