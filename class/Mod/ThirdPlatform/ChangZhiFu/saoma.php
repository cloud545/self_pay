<?php
class Mod_ThirdPlatform_ChangZhiFu_saoma extends Mod_ThirdPlatform_ChangZhiFu {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();
        $payType = 0;
        $url = '';
        if ($type == 1 || $type == 3) {   //话费微信H5
            $url = $platInfo['platInfo']['p_pay_url'].'api/v1/wx_h5.api';
        }
        if ($type == 2) {   //个码支付宝wap
            $url = $platInfo['platInfo']['p_pay_url'].'api/v1/ali_h5.api';
        }
        if ($type == 9) {   //个码支付宝扫码
            $url = $platInfo['platInfo']['p_pay_url'].'api/v1/ali_qrcode.api';
        }
        if ($type == 10) {   //个码微信扫码
            $url = $platInfo['platInfo']['p_pay_url'].'api/v1/wx_qrcode.api';
        }

        $data = array(
            'mch_id' => $platInfo['platInfo']['appId'],
            'mch_order' => $payInfo['pOrderId'],
            'amt' => $payInfo['amount'] * 1000,
            'remark' => '充值',
            'created_at' => time() ,
            'client_ip' => $_SERVER["REMOTE_ADDR"],
            'notify_url' => $platInfo['appInfo']['callbackUrl'],
            'sign_type' => 'md5',
            'mch_key' => $platInfo['platInfo']['appKey'],
        );
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $data['sign'] = md5(substr($md5str, 0, strlen($md5str) - 1));
        unset($data['mch_key']);
        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        $result = json_decode(curl_exec($curl), 1); // 执行操作
        curl_close($curl); // 关闭CURL会话
        if ($result['code'] == '1') {
            header('location:'.$result['data']['jump_url']);
        } else {
            echo $result['msg'];
        }
    }
}
