<?php
class Mod_ThirdPlatform_ChengZiPay_payCallback extends Mod_ThirdPlatform_ChengZiPay {

	protected function _do($platInfo = array()) {

        $data = $_POST;
        $sign = $data['fxsign'];
        $signs = md5($data['fxstatus'].$data['fxid'].$data['fxddh'].$data['fxfee'].$platInfo['platInfo']['appKey']);
        $status = $this->getStatus($data, $platInfo['platInfo']['appKey']);
        if ($sign == $signs && $data['fxstatus'] == '1') {
            echo 'success';
            return array('platOrderId' => $data['fxddh'],'realMoney' => $data['fxfee']);
        }
        return false;
	}

	public function getStatus($arr, $appKey) {
	    $data['fxid'] = $arr['fxid'];
	    $data['fxddh'] = $arr['fxddh'];
	    $data['fxaction'] = 'orderquery';
	    $data['sign'] = md5($data['fxid'].$data['fxddh'].$data['fxaction'].$appKey);
        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, $platInfo['platInfo']['p_pay_url']); // 要访问的地址
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        $result = json_decode(curl_exec($curl),1); // 执行操作
        curl_close($curl); // 关闭CURL会话
        if ($result['fxstatus'] == '1') {
            return 'ok';
        } else {
            return false;
        }
    }
}
