<?php
class Mod_ThirdPlatform_WeiBoPay_payCallback extends Mod_ThirdPlatform_WeiBoPay {

	protected function _do($platInfo = array()) {
        $data = $_GET;

        $sign = $_GET['sign'];
        unset($data['sign']);
        unset($data['res']);

        $signs = md5("c_code=".$_GET['c_code']."&pay_type=".$_GET['pay_type']."&pay_amt=".$_GET['pay_amt']."&jpay_bill_id=".$_GET['jpay_bill_id']."&agent_bill_id=".$_GET['agent_bill_id']."&key=".$platInfo['platInfo']['appKey']);

        if ($sign == $signs && $_GET['res'] == '1') {
            return array('platOrderId' => $_GET['agent_bill_id'],'realMoney' => $_GET['pay_amt']);
        }
        return false;
	}
}
