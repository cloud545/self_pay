<?php
class Mod_ThirdPlatform_WeiBoPay_saoma extends Mod_ThirdPlatform_WeiBoPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();
        $payType = 0;

        if ($type == 1 || $type == 3) {
            $payType = '20';
        }

        if ($type == 9) {
            $payType = '31';
        }

        if ($type == 10) {
            $payType = '11';
        }

        if ($type == 2) {
            $payType = '40';
        }

        $data = array(
            'c_code' => $platInfo['platInfo']['appId'],
            'pay_type' => $payType,
            'pay_amt' => $payInfo['amount'],
            'agent_bill_id' => $payInfo['pOrderId'],
        );
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $data['sign'] = md5($md5str . 'key='.$platInfo['platInfo']['appKey']);
        $data['goods_name'] = '支付';
        $data['remark'] = '支付';
        $data['notify_url'] = $platInfo['appInfo']['callbackUrl'];
        $data['callback_url'] ='https://www.baidu.com';
        $ch = curl_init(); // 启动一个CURL会话
        curl_setopt($ch, CURLOPT_URL, $platInfo['platInfo']['p_pay_url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch); // 执行操作
        curl_close($ch); // 关闭CURL会话
        $result = json_decode($result, true);
        if ($result['res'] == '0') {
            header("location:".$result['url']);
        } else {
            var_dump($result);
        }
    }
}
