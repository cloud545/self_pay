<?php
class Mod_ThirdPlatform_Shijia extends Mod_ThirdPlatform {

    /**
     * 生成签名
     * $signdata 签名数据 array
     * $api_key 商户秘钥
     * @return 返回md5签名
     */
    protected function make_sign($signdata,$api_key)
    {
        //签名步骤一：按字典序排序参数
        ksort($signdata);
        $string = $this->to_params($signdata);
        //签名步骤二：在string后加入KEY
        $string = $string . "&key=".$api_key;
        //签名步骤三：MD5加密
        $string = md5($string);
        //签名步骤四：所有字符转为大写
        $result = strtoupper($string);
        return $result;
    }

    /**
     * 格式化参数格式化成url参数
     */
    protected function to_params($signdata)
    {
        $buff = "";
        foreach ($signdata as $k => $v)
        {
            if($k != "sign" && $v != "" && !is_array($v)){
                $buff .= $k . "=" . $v . "&";
            }
        }

        $buff = trim($buff, "&");
        return $buff;
    }

    /**
     * 生成随机字符串，订单号
     * @return 字符串
     */
    protected function order_sn()
    {
        $ycode = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J');
        $ordersn = $ycode[intval(substr(date('Y'),-1))-1] . strtoupper(dechex(date('m'))) . date('d') . substr(time(), -5) . substr(microtime(), 2, 5) . sprintf('%02d', rand(0, 99));
        return $ordersn;
    }

    /**
     * 返回json
     * $message 提示文字
     * $data 返回数据 array
     * $code 返回代码
     * @return json字符串
     */
    protected function json_return($message = '',$data = '',$code = 0)
    {
        $return['msg']  = $message;
        $return['data'] = $data;
        $return['code'] = $code;
        return json_encode($return);
    }

    /**
     * 模拟post进行url请求
     * @param string $url
     * @param array $post_data
     */

    protected function curlPost($url, $param)
    {
        if (empty($url) || empty($param)) {
            return false;
        }

        $ch=curl_init();
        $timeout=5;
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_HEADER, 0);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,2);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($ch,CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
        $data=curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    /**
     * 模拟JSON提交
     * $url 路径
     * $param_string 数据
     */
    protected function curlPostJson($url, $param_string){
        $param_string = json_encode($param_string);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $param_string);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Content-Type: application/json; charset=utf-8",
                "Content-Length: " . strlen($param_string))
        );
        ob_start();
        curl_exec($ch);
        $return_content = ob_get_contents();
        ob_end_clean();
        $return_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        return array($return_code, $return_content);
    }

}
