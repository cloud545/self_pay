<?php
class Mod_ThirdPlatform_Shijia_saoma extends Mod_ThirdPlatform_Shijia {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = '';
        if ($type == 2) { // 支付宝
            $payType = 'alipay_red';
        } elseif ($type == 3) { // 微信
            $payType = 'wechat_rand';
        }


        //判断是否使用测试页面的手动输入商户号
        $return_type = "html";
        $api_code = $platInfo['platInfo']['appId'];	//此处填写商户号;
        $api_key = $platInfo['platInfo']['appKey'];	//此处填写的密钥;

        $price = number_format($payInfo['amount'],2,'.','');	//从网页传入price:交易金额
        $is_type = $payType;	//商户后台[ 账户管理 > 通道列表 ] 上已授权通道类型
        $mark = "商品";	//此处填写产品名称，或充值，消费说明
        $time = time();

        //$order_id = "1234567890";
        $order_id = $payInfo['pOrderId']; //此处就在您服务器生成新订单，订单号不能重复提交。
        $return_url = $platInfo['appInfo']['clientUrl'];//支付成功，用户会跳转到这个地址
        $notify_url = $platInfo['appInfo']['callbackUrl'];//通知异步回调接收地址


        $signdata = array(
            'return_type' => $return_type,		//用户订单编号ID
            'api_code' => $api_code,			//此处填写商户的id
            'is_type' => $is_type,				//支付渠道
            'price' => $price,					//支付金额
            'order_id' => $order_id,			//订单号
            'time' => $time,					//支付时间
            'mark' => $mark,					//此处填写产品名称，或充值，消费说明
            'return_url' => $return_url,		//支付成功，用户会跳转到这个地址
            'notify_url' => $notify_url,		//通知异步回调接收地址
        );

        $returndata = array(
            'return_type' => $return_type,		//用户订单编号ID
            'api_code' => $api_code,			//此处填写商户的id
            'is_type' => $is_type,				//支付渠道
            'price' => $price,					//支付金额
            'order_id' => $order_id,			//订单号
            'time' => $time,					//支付时间
            'mark' => $mark,					//此处填写产品名称，或充值，消费说明
            'return_url' => $return_url,		//支付成功，用户会跳转到这个地址
            'notify_url' => $notify_url,		//通知异步回调接收地址
            'sign' => $this->make_sign($signdata,$api_key),	//base.php里签名md5加密
        );

        BooView::set('payUrl', $platInfo['platInfo']['p_pay_url']);
        BooView::set('native', $returndata);
        BooView::display('pay/formPay.html');
        exit;
    }

}
