<?php
class Mod_ThirdPlatform_MuChenPay_payCallback extends Mod_ThirdPlatform_MuChenPay {

	protected function _do($platInfo = array()) {

	    $data = $_POST;
	    $sign = $data['Sign'];
	    unset($data['Sign']);
	    $signs = md5($data['Data'] . $platInfo['platInfo']['appKey'] );

	    $result = json_decode($this->decrypt($platInfo['platInfo']['appKey'],$platInfo['platInfo']['appKey'] ,$data['Data']),1);
        if ($sign == $signs && $result['respcode'] == '2') {
            echo "{\"message\": \"成功\",\"response\":\"00\"}";
            return array('platOrderId' => $data['Ordernumber'],'realMoney' => $result['actpayamount']/100);
        }else{
            return false;
        }
    }
}
