<?php
class Mod_ThirdPlatform_MuChenPay extends Mod_ThirdPlatform {

    public function encode($str){
        $base64str = str_replace("+", "-",  $str);
        $base64str = str_replace("/","_",  $base64str);
        $base64str = str_replace("=",".",  $base64str);
        return $base64str;
    }

    public function decode($str){
        $str = str_replace("-", "+",  $str);
        $str = str_replace("_","/",  $str);
        $str = str_replace(".","=",  $str);
        $unbase64str=base64_decode($str);
        return $unbase64str;
    }

    public function encrypt($privateKey,$iv,$data){
        $encrypted = openssl_encrypt($data,'aes-128-cbc',$privateKey,false, $iv);
        return $this->encode($encrypted);
    }

    public function decrypt($privateKey,$iv,$data){
        $encryptedData = $this->decode($data);
        $decrypted = openssl_decrypt($encryptedData,'aes-128-cbc', $privateKey,OPENSSL_RAW_DATA|OPENSSL_ZERO_PADDING , $iv);
        return trim($decrypted);
    }
}
