<?php
class Mod_ThirdPlatform_MuChenPay_saoma extends Mod_ThirdPlatform_MuChenPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();
        $payType = 0;

        if ($type == 2) {   //个码支付宝wap
            $payType = '1006';
        }
		
        $data = array(
            'appid' => $platInfo['platInfo']['appId'],
            'method' => 'qdpay.pay.compay.router.pay',
            'format' => 'json',
            'v' => '2.0',
            'timestamp' => date('Y-m-d H:i:s', time()),
            'session' => 'i4nfxkbgzu04adek9iemndnlhm9rcsie',
        );
        $content = [
            'ordernumber' => $payInfo['pOrderId'],
            'body' => '充值',
            'amount' => $payInfo['amount']*100,
            'fronturl' => 'https://www.baidu.com',
            'callbackurl' => $platInfo['appInfo']['callbackUrl'],
            'tunnelcode' => $payType,
        ];

        $data['data'] = $this->encrypt($platInfo['platInfo']['appKey'] ,$platInfo['platInfo']['appKey'], json_encode($content, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT));
        $data['sign'] = md5($platInfo['platInfo']['appKey'].$data['appid'].$data['data'].$data['format'].$data['method'].$data['session'].$data['timestamp'].$data['v'].$platInfo['platInfo']['appKey']);

        $data = http_build_query(
            $data
        );
        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => $data
            )
        );
        $context = stream_context_create($opts);
        $result = file_get_contents($platInfo['platInfo']['p_pay_url'], false, $context);
        $result = json_decode($result,1);
        if ($result['ret'] == '0') {
            header('location:'.$result['data']['qrcode']);
        } else {
            var_dump($result);
        }
    }
}
