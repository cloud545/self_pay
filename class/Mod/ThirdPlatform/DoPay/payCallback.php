<?php
class Mod_ThirdPlatform_DoPay_payCallback extends Mod_ThirdPlatform_DoPay {

	protected function _do($platInfo = array()) {
        $data = json_decode(file_get_contents('php://input'), true);

        $sign = $data['sign'];
        unset($data['sign']);
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $signs = md5($md5str.'api_secret='.$platInfo['platInfo']['appSecret']);

        if ($sign == $signs && $data['result'] == 'success') {
            echo 'success';
            return array('platOrderId' => $data['order_no'],'realMoney' => $data['money']);
        }
        return false;
	}

}
