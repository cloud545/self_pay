<?php
class Mod_ThirdPlatform_DoPay_saoma extends Mod_ThirdPlatform_DoPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();
        $payType = 0;

        if ($type == 9  || $type == 2) {
            $payType = 'alipay4';
        }

        if ($type == 10 || $type == 3) {
            $payType = 'wechat1';
        }


        $data = array(
            'code' => $platInfo['platInfo']['appId'],
            'order_no' => $payInfo['pOrderId'],
            'money' => number_format($payInfo['amount'],'2','.',''),
            'type' => $payType,
            'notify_url' => $platInfo['appInfo']['callbackUrl'],
        );
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $data['sign'] = md5($md5str.'api_token='.$platInfo['platInfo']['appKey']);
        $data = json_encode($data);
        $ch = curl_init($platInfo['platInfo']['p_pay_url']);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data))
        );
        $result = json_decode(curl_exec($ch),1);
        if ($result['code'] == '0') {
            header('location:'.$result['data']['pay_url']);
        } else {
            echo $result['msg'];
        }
    }
}
