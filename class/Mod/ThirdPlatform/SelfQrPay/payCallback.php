<?php
class Mod_ThirdPlatform_SelfQrPay_payCallback extends Mod_ThirdPlatform_SelfQrPay {

	protected function _do($platInfo = array()) {

        $post = array();
        $post['amount'] = $_REQUEST['amount'];
        $post['realMoney'] = $_REQUEST['realMoney'];
        $post['platOrderId'] = $_REQUEST['platOrderId'];
        $post['createTime'] = $_REQUEST['createTime'];
        $post['endTime'] = $_REQUEST['endTime'];

        ksort($post);
        $md5Str = '';
        foreach ($post as $key => $value) {

            if (!$md5Str) {
                $md5Str .= "{$key}={$value}";
            } else {
                $md5Str .= "&{$key}={$value}";
            }
        }

        $checkSign = md5($md5Str . 'rtsj64i543nb$jh*gjkg8368#');
        if ($_REQUEST["sign"] == $checkSign) {

            if (!$platInfo['platInfo']['qrCodeUserId'] || !$platInfo['platInfo']['qrCodeId']) {
                return false;
            }

            $qrCodeUserId = $platInfo['platInfo']['qrCodeUserId'];
            $qrCodeUserObj = BooController::get('Obj_QrPay_CodeUser');
            $qrCodeUserInfo = $qrCodeUserObj->getInfoById($qrCodeUserId);
            if (!$qrCodeUserInfo) {
                return false;
            }

            $qrCodeId = $platInfo['platInfo']['qrCodeId'];
            $codeObj = BooController::get('Obj_QrPay_CodeInfo');
            $qrCodeInfo = $codeObj->getInfoById($qrCodeId);
            if (!$qrCodeInfo) {
                return false;
            }

            $balance = $qrCodeUserInfo['balance'] - $_REQUEST['realMoney'];
            $updateData = array();
            $updateData['balance'] = $balance > 0 ? $balance : 0;
            $qrCodeUserObj->update($qrCodeUserId, $updateData);

            if (date('Y-m-d', strtotime($qrCodeInfo['last_time'])) != date('Y-m-d')) {
                $qrCodeInfo['use_amount'] = 0;
            }

            $updateData = array();
            $updateData['use_amount'] = $qrCodeInfo['use_amount'] + $_REQUEST['realMoney'];
            $codeObj->update($qrCodeId, $updateData);

            echo 'success';
            return array('platOrderId' => $post["platOrderId"],'realMoney' => $post['realMoney']);
        }else {
            //签名验证失败
            return false;
        }

	}

}
