<?php
class Mod_ThirdPlatform_SelfQrPay_saoma extends Mod_ThirdPlatform_SelfQrPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $codeUserObj = BooController::get('Obj_QrPay_CodeUser');
        $codeObj = BooController::get('Obj_QrPay_CodeInfo');

        // 判断码商的余额是否够
        $list = array();
        if ($platInfo['platInfo']['qrCodeUserId']) {
            $codeUserInfo = $codeUserObj->getInfoById($platInfo['platInfo']['qrCodeUserId']);
            if (!$codeUserInfo) {
                Common_errorCode::jsonEncode(Common_errorCode::NOW_NO_CAN_USE_PAY, array(), ' 1');
            }

            if ($codeUserInfo['is_lock']) {
                Common_errorCode::jsonEncode(Common_errorCode::NOW_NO_CAN_USE_PAY, array(), ' 3');
            }

            if ($payInfo['amount'] > $codeUserInfo['balance']) {
                Common_errorCode::jsonEncode(Common_errorCode::MERCHANT_SCORE_LESS);
            }

            $list = $codeObj->getList($type, $platInfo['platInfo']['qrCodeUserId']);
        } else {

            $codeUserIds = array();
            $codeUserList = $codeUserObj->getList();
            foreach ($codeUserList as $tmpInfo) {

                if ($tmpInfo['app_id_list'] || $tmpInfo['is_lock']) {
                    continue;
                }

                if ($payInfo['amount'] < $tmpInfo['balance']) {
                    $codeUserIds[$tmpInfo['id']] = $tmpInfo['id'];
                }
            }

            $list = $codeObj->getListByQrcodeUserIds($type, $codeUserIds);
        }

        if (!$list) {
            Common_errorCode::jsonEncode(Common_errorCode::NOW_NO_CAN_USE_PAY, array(), ' 2');
        }

        // 刨去限额已经不够的
        $nowDate = date('Y-m-d');
        foreach ($list as $key => $tmpInfo) {

            if (!$tmpInfo['img_url']) {
                unset($list[$key]);
                continue;
            }

            if (date('Y-m-d', strtotime($tmpInfo['last_time'])) != $nowDate) {
                $tmpInfo['use_amount'] = 0;
            }

            if ($tmpInfo['day_amount'] > 0 && $tmpInfo['use_amount'] + $payInfo['amount'] > $tmpInfo['day_amount']) {
                unset($list[$key]);
                continue;
            }

            if ($tmpInfo['min_money'] > 0 && $payInfo['amount'] < $tmpInfo['min_money']) {
                unset($list[$key]);
                continue;
            }

            if ($tmpInfo['max_money'] > 0 && $payInfo['amount'] > $tmpInfo['max_money']) {
                unset($list[$key]);
                continue;
            }
        }

        if (!$list) {
            Common_errorCode::jsonEncode(Common_errorCode::NOW_NO_CAN_USE_PAY, array(), ' 3');
        }

        $cacheKey = BooConfig::get('cacheKey.selfQrpayUsingCodeList');
        $cacheDao = Dao_Redis_Default::getInstance();
        $cacheList = $cacheDao->get($cacheKey);
        if ($cacheList) {
            $cacheList = json_decode($cacheList, true);
        } else {
            $cacheList = array();
        }

        // 大于1.5分钟未支付则认为已经放弃支付
        $nowTime = time();
        $diffTimer = 180;
        $haveUseCodeIds = array();
        foreach ($cacheList as $key => $cacheInfo) {
            if ($nowTime - $cacheInfo['time'] >= $diffTimer + 10 * 60) {
                unset($cacheList[$key]);
            } else {
                $haveUseCodeIds[$cacheInfo['qrCodeId']] = $cacheInfo['qrCodeId'];
            }
        }

        // 获取正在收款的二维码
        $nowQrList = $list;
        $amount = $payInfo['amount'];
        foreach ($list as $key => $tmpInfo) {
            if ($cacheList["{$tmpInfo['id']}_{$amount}"]) {
                unset($list[$key]);
                continue;
            }
        }

        if (!$list) {
            //Common_errorCode::jsonEncode(Common_errorCode::NOW_NO_CAN_USE_QRCODE);

            $tmpKey = array_rand($nowQrList);
            $qrCodeInfo = $nowQrList[$tmpKey];

            for ($i = 1; $i <= 100; $i++) {
                $randNumber = rand(1, 10);
                $tmpAmount = $amount + $randNumber;
                if (!$cacheList["{$qrCodeInfo['id']}_{$tmpAmount}"]) {
                    $amount = $tmpAmount;
                    break;
                }
            }

            foreach ($list as $key => $tmpInfo) {
                if ($cacheList["{$tmpInfo['id']}_{$amount}"]) {
                    unset($list[$key]);
                    continue;
                }
            }

            if (!$list) {
                Common_errorCode::jsonEncode(Common_errorCode::NOW_NO_CAN_USE_QRCODE, array(), ' 2');
            }

        } else {

            $nowQrList = $list;
            foreach ($list as $key => $tmpInfo) {
                if ($haveUseCodeIds[$tmpInfo['id']]) {
                    unset($list[$key]);
                }
            }

            if (!$list) {
                $tmpKey = array_rand($nowQrList);
                $qrCodeInfo = $nowQrList[$tmpKey];
            } else {
                $tmpKey = array_rand($list);
                $qrCodeInfo = $list[$tmpKey];
            }
        }

        if (!$qrCodeInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::NOW_NO_CAN_USE_QRCODE, array(), ' 3');
        }

        $nowDateTime = date('Y-m-d H:i:s');
        $amount = number_format($amount,2,'.','');

        $insertData = array();
        $insertData['pt_id'] = $platInfo['platInfo']['payType'];
        $insertData['qrcode_id'] = $qrCodeInfo['id'];
        $insertData['qrcode_user_id'] = $qrCodeInfo['qrcode_user_id'];
        $insertData['qrcode_user_name'] = $qrCodeInfo['qrcode_user_name'];
        $insertData['pay_account'] = $qrCodeInfo['pay_account'];
        $insertData['amount'] = $amount;
        $insertData['app_id'] = $platInfo['appInfo']['app_id'];
        $insertData['app_order_id'] = $payInfo['orderId'];
        $insertData['p_id'] = $platInfo['platInfo']['platId'];
        $insertData['plat_order_id'] = $payInfo['pAutoOrderId'];
        $insertData['create_time'] = $nowDateTime;
        $insertData['create_date'] = date('Y-m-d');
        $insertData['client_ip'] = BooUtil::realIp();
        $insertData['page'] = 1;

        if ($isMobile) {
            $insertData['system_type'] = 'h5';
        } else {
            $insertData['system_type'] = 'pc';
        }

        $qrPayObj = BooController::get('Obj_QrPay_Pay');
        $orderId = $qrPayObj->insert($insertData, $nowDateTime);
        if (!$orderId) {
            Common_errorCode::jsonEncode(Common_errorCode::PAY_FAIL);
        }

        $cacheList["{$qrCodeInfo['id']}_{$amount}"]['qrCodeId'] = $qrCodeInfo['id'];
        $cacheList["{$qrCodeInfo['id']}_{$amount}"]['orderId'] = $orderId;
        $cacheList["{$qrCodeInfo['id']}_{$amount}"]['time'] = $nowTime;
        $cacheDao->set($cacheKey, json_encode($cacheList), 3 * 3600);

        // 记录成功率
        $cacheDao = Dao_Redis_Default::getInstance();
        $payLogListCacheKey = BooConfig::get('cacheKey.qrPayUserPayLogList') . $qrCodeInfo['qrcode_user_id'];
        $payLogListCacheInfo = $cacheDao->get($payLogListCacheKey);
        if ($payLogListCacheInfo) {
            $payLogListCacheInfo = json_decode($payLogListCacheInfo, true);
            if (count($payLogListCacheInfo) >= 1000) {
                for ($i = 1; $i <= count($payLogListCacheInfo) - 998; $i++) {
                    array_shift($payLogListCacheInfo);
                }
            }
        }

        $tmpArr = array();
        $tmpArr['qrCodeId'] = $qrCodeInfo['id'];
        $tmpArr['app_name'] = $platInfo['appInfo']['name'];
        $tmpArr['amount'] = $amount;

        if (in_array($platInfo['platInfo']['pt_id'], array(2, 9))) {
            $tmpArr['payType'] = 2;
        } else {
            $tmpArr['payType'] = 3;
        }
        $tmpArr['result'] = 0;
        $tmpArr['pay_account'] = $qrCodeInfo['pay_account'];
        $tmpArr['create_time'] = time();
        $payLogListCacheInfo[] = $tmpArr;
        $cacheDao->set($payLogListCacheKey, json_encode($payLogListCacheInfo), 3600);

        $webCdnUrl = BooConfig::get("main.webCdnUrl");
        BooView::set('qrCodeId', $qrCodeInfo['id']);
        BooView::set('orderId', $payInfo['orderId']);
        BooView::set('amount', $amount);
        BooView::set('diffTimer', $diffTimer);
        BooView::set('qrCodeUrl', $qrCodeInfo['img_url']);
        BooView::set('payUrl', $qrCodeInfo['code_url']);
        BooView::set('dateTime', $nowDateTime);
        BooView::set('qrOrderId', $orderId);
        BooView::set('payType', $type);
        BooView::set('isMobile', $isMobile);

        // 微信
        if (in_array($type, array(3, 10))) {
            BooView::set('appType', 3);
            BooView::set('qrCodeLogoUrl', "$webCdnUrl/images/weixin_logo.png");
        } else {
            BooView::set('appType', 2);
            BooView::set('qrCodeLogoUrl', "$webCdnUrl/images/alipay_logo.png");
        }

        BooView::display('pay/self_img.html');
        exit;
    }

}
