<?php
class Mod_ThirdPlatform_SelfQrPay_payOrder extends Mod_ThirdPlatform_SelfQrPay {

	protected function _do($platInfo = array()){

        if (!$platInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $post = BooVar::requestx();

        $pay = array();
        $pay['customer'] = $platInfo['platInfo']['appId'];
        $pay['bankcode'] = $post['bankCode'];
        $pay['amount'] = number_format($post['amount'],2,'.','');
        $pay['orderid'] = $post['pOrderId'];
        $pay['asynbackurl'] = $platInfo['appInfo']['callbackUrl'];
        $pay['request_time'] = date('YmdHis');
        $pay['synbackurl'] = $platInfo['appInfo']['clientUrl'];
        $pay['attach'] = '';

        $signStr = "customer={$pay['customer']}&amount={$pay['amount']}&orderid={$pay['orderid']}&asynbackurl={$pay['asynbackurl']}&request_time={$pay['request_time']}&key={$platInfo['platInfo']['appKey']}";
        $pay['sign'] = md5($signStr);

        $formstr = '';
        foreach ($pay as $key => $val) {
            $formstr .= '<input type="hidden" name="' . $key . '" value="' . $val . '">';
        }

        echo '
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            </head>
            <body onLoad="document.zlinepay.submit();">
                <form name="zlinepay" method="post" action="' . $platInfo['platInfo']['p_pay_url'] . '" target="_top"/>'.$formstr.'</form>
            </body>
        </html>
        ';
        exit;
	}

}
