<?php
class Mod_ThirdPlatform_GoodatPay_withdrawal extends Mod_ThirdPlatform_GoodatPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        $post = BooVar::requestx();

        //MD5签名
        $src = "merchant_no={$platInfo['platInfo']['appId']}&key={$platInfo['platInfo']['appKey']}";
        $sign = md5($src);

        BooCurl::setData("merchant_no={$platInfo['platInfo']['appId']}&sign={$sign}", 'POST');
        $data = BooCurl::call("https://pay1.goodatpay.com/withdraw/queryBalance");
        $return = json_decode($data,true);

        if (is_array($return) && $return['result_code'] == '000000') {
            if ($return['balance'] < $payInfo['amount'] + $payInfo['fee']) {
                return array('platOrderId' => 0, 'status' => 2, 'msg' => Common_errorCode::getMsg(Common_errorCode::CASH_IS_NOT_ENOUGH));
            }
        } else {
            return array('platOrderId' => 0, 'status' => 2, 'msg' => $return['result_msg']);
        }

        $merchant_no = $platInfo['platInfo']['appId'];									//商户号
        $order_no = $payInfo['pOrderId'];											//商户订单号
        $card_no = $post['cardNo'];									    //银行卡号
        $account_name = base64_encode($post['accountName']);						//银行开户名,使用base64进行编码（UTF-8编码）
        $bank_branch = "";												//银行支行名称,对公账户需填写，使用base64进行编码（UTF-8编码）
        $cnaps_no = "";													//银行联行号,银行唯一识别编号，对公账户需填写
        $bank_code = $platInfo['platInfo']['bankCode'][$post['bankCode']];											//银行代码,参考银行代码对照表
        $bank_name = base64_encode($post['bankName']);				//银行名称,参考银行代码对照表，使用base64进行编码（UTF-8编码）
        $amount = number_format($post['amount'],2,'.','');													//代付金额,最多两位小数
        $pay_pwd = $platInfo['platInfo']['appSecret'];		//支付密码(需先在商戶後台配置支付密碼,獲取支付密碼)
        $key = $platInfo['platInfo']['appKey'];					//商户接口秘钥

        //MD5签名
        $param = "merchant_no=" . $merchant_no . "&order_no="
            . $order_no . "&card_no=" . $card_no . "&account_name=" . $account_name
            . "&bank_branch=" . $bank_branch . "&cnaps_no="
            . $cnaps_no . "&bank_code=" . $bank_code
            . "&bank_name=" . $bank_name . "&amount=" . $amount;
        $src = $param . "&pay_pwd=" . $pay_pwd . "&key=" . $key;
        $sign = md5($src);

        BooCurl::setData("$param}&sign={$sign}", 'POST');
        $data = BooCurl::call("https://pay1.goodatpay.com/withdraw/singleWithdraw");
        $return = json_decode($data,true);

        if (is_array($return) && $return['result_code'] == '000000') {
            if ($return['result'] == 'S') {
                $status = 1;
            } elseif ($return['result'] == 'F') {
                $status = 2;
            } else {
                $status = 0;
            }
            return array('platOrderId' => $return['order_no'], 'status' => $status, 'msg' => $return['result_msg']);
        } else {
            return array('platOrderId' => 0, 'status' => 2, 'msg' => $return['result_msg']);
        }

    }

}
