<?php
class Mod_ThirdPlatform_GoodatPay_withdrawalCallback extends Mod_ThirdPlatform_GoodatPay {

	protected function _do($platInfo = array()) {

        $_POST = json_decode(file_get_contents("php://input",'r'), true);
        $merchant_no = $_POST['merchant_no'];							//商户号
        $orders = $_POST['orders'];									//代付結果通知
        $key = $platInfo['platInfo']['appKey'];					//商户接口秘钥
        $sign = $_POST['sign'];

        //md5加密串
        //MD5签名
        $post = array(
            "merchant_no" => $merchant_no,
            "orders" => $orders
        );

        $src = json_encode($post);
        $src .= $key;

        if ($sign == md5($src)) {
            $orderList = array();
            foreach($orders as $k => $v) {
                if ($v['result'] == 'S') {
                    $orderList[] = array(
                        'platOrderId' => $v["order_no"],
                        'amount' => $v['amount'],
                        'fee' => $v['withdraw_fee'],
                        'orderId' => $v['mer_order_no'],
                    );
                    echo "S";
                }
            }

            echo "success";
            return $orderList;
        } else {
            echo "MD5签名错误";
            return array();
        }

	}

}
