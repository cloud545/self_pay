<?php
class Mod_ThirdPlatform_GoodatPay_payOrder extends Mod_ThirdPlatform_GoodatPay {

	protected function _do($platInfo = array()){

        if (!$platInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $post = BooVar::requestx();

        $pay = array();
        $pay['version'] = "v1";
        $pay['merchant_no'] = $platInfo['platInfo']['appId'];
        $pay['order_no'] = $post['pOrderId'];
        $pay['goods_name'] = base64_encode("充值");
        $pay['order_amount'] = number_format($post['amount'],2,'.','');
        $pay['backend_url'] = $platInfo['appInfo']['callbackUrl'];
        $pay['frontend_url'] = $platInfo['appInfo']['clientUrl'];
        $pay['reserve'] = '';
        $pay['pay_mode'] = "07";
        $pay['bank_code'] = $post['bankCode'];
        $pay['card_type'] = "0";
        $pay['merchant_user_id'] = "1";

        //MD5签名
        $src = "version={$pay['version']}&merchant_no={$pay['merchant_no']}&order_no={$pay['order_no']}&goods_name={$pay['goods_name']}&order_amount={$pay['order_amount']}&backend_url={$pay['backend_url']}&frontend_url={$pay['frontend_url']}&reserve={$pay['reserve']}&pay_mode={$pay['pay_mode']}&bank_code={$pay['bank_code']}&card_type={$pay['card_type']}&merchant_user_id={$pay['merchant_user_id']}&key={$platInfo['platInfo']['appKey']}";
        $pay['sign'] = md5($src);

        $formstr = '';
        foreach ($pay as $key => $val) {
            $formstr .= '<input type="hidden" name="' . $key . '" value="' . $val . '">';
        }

        echo '
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            </head>
            <body onLoad="document.zlinepay.submit();">
                <form name="zlinepay" method="post" action="' . $platInfo['platInfo']['p_pay_url'] . '" target="_top"/>'.$formstr.'</form>
            </body>
        </html>
        ';
        exit;
	}

}
