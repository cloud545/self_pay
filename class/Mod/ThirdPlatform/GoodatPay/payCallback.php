<?php
class Mod_ThirdPlatform_GoodatPay_payCallback extends Mod_ThirdPlatform_GoodatPay {

	protected function _do($platInfo = array()) {

        $post = $_REQUEST;
        $sign = $post["sign"];

        $md5Str = "merchant_no={$post['merchant_no']}&order_no={$post['order_no']}&order_amount={$post['order_amount']}&original_amount={$post['original_amount']}&upstream_settle={$post['upstream_settle']}&result={$post['result']}&pay_time={$post['pay_time']}&trace_id={$post['trace_id']}&reserve={$post['reserve']}&key={$platInfo['platInfo']['appKey']}";
        $checkSign = md5($md5Str);
        if ($sign == $checkSign && $post['result'] == 'S') {
            return array('platOrderId' => $post["trace_id"], 'realMoney' => $post['order_amount']);
        } else {

            $merchant_no = $platInfo['platInfo']['appId'];                    //商户号
            $order_no = $post['order_no'];                     //商户订单号
            $key = $platInfo['platInfo']['appKey'];  //商户接口秘钥

            //MD5签名
            $src = "merchant_no=" . $merchant_no . "&order_no=" . $order_no . "&key=" . $key;
            $sign = md5($src);

            BooCurl::setData("merchant_no={$merchant_no}&order_no={$order_no}&sign={$sign}", 'POST');
            $data = BooCurl::call('https://pay1.goodatpay.com/gateway/queryOrder.jsp');
            $return = json_decode($data,true);
            if ($return['result_code'] != '000000' || $return['result'] != 'S') {
                return false;
            }

            return array('platOrderId' => $return["trace_id"], 'realMoney' => $return['order_amount']);
        }

	}

}
