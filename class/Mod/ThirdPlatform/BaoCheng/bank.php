<?php
class Mod_ThirdPlatform_BaoCheng_bank extends Mod_ThirdPlatform_BaoCheng {

	protected function _do($platInfo = array(), $payInfo = array()){

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        if ($type == 1) {// 网银
            $payType = '911';
        } elseif ($type == 8) {// 银联快捷
            $payType = 10;
        } else {
            $payType = '911';
        }

        $requestarray = array(
            "orgNo"=>$platInfo['platInfo']['appId'],//机构编号
            "merNo"=>$platInfo['platInfo']['appKey'],//商户编号
            "action"=>'UnionWapOrder',
        );
        $data['linkId'] = $payInfo['pOrderId'];   //三方流水号
        $data['orderType'] = $payType;   //支付类型
        $data['goodsName'] = '商品';    //结算类型
        $data['amount'] = $payInfo['amount'] * 100;  //结算到账金额
        $data['bankCode'] = '';
        $data['notifyUrl'] = $platInfo['appInfo']['callbackUrl'];//支付结果回调地址
        $data['frontUrl'] = $platInfo['appInfo']['clientUrl'];

        $value = json_encode($data);
        $key1 = substr($platInfo['platInfo']['appSecret'],0,16);
        $encry = $this->encrypt($value,$key1);//加密

        echo '<pre>';
        var_dump(222, $requestarray, $data);
        exit;

        $requestarray['data'] = strtoupper($encry);
        $sign = md5($requestarray['orgNo'].$requestarray['merNo'].$requestarray['action'].$requestarray['data'].$platInfo['platInfo']['appSecret']);
        $requestarray["sign"] = $sign;

        echo '<pre>';
        var_dump($requestarray);
        exit;


        BooCurl::setData($requestarray, 'POST');
        $respose = BooCurl::call($platInfo['platInfo']['p_pay_url']);

        echo '<pre>';
        var_dump($respose);
        exit;




        $pay_memberid = $platInfo['platInfo']['appId'];   //商户后台API管理获取
        $pay_orderid = $payInfo['pOrderId'];    //订单号
        $pay_amount = number_format($payInfo['amount'],2,'.','');    //交易金额
        $pay_applydate = date("Y-m-d H:i:s");  //订单时间
        $pay_notifyurl = $platInfo['appInfo']['callbackUrl'];   //服务端返回地址
        $pay_callbackurl = $platInfo['appInfo']['clientUrl'];  //页面跳转返回地址
        $Md5key = $platInfo['platInfo']['appKey'];   //商户后台API管理获取
        $pay_bankcode = "907"; //支付宝扫码  //商户后台通道费率页 获取银行编码
        $native = array(
            "pay_memberid" => $pay_memberid,
            "pay_orderid" => $pay_orderid,
            "pay_amount" => $pay_amount,
            "pay_applydate" => $pay_applydate,
            "pay_bankcode" => $pay_bankcode,
            "pay_notifyurl" => $pay_notifyurl,
            "pay_callbackurl" => $pay_callbackurl,
        );
        ksort($native);
        $md5str = "";
        foreach ($native as $key => $val) {
            $md5str = $md5str . $key . "=" . $val . "&";
        }

        $sign = strtoupper(md5($md5str . "key=" . $Md5key));
        $native["pay_md5sign"] = $sign;
        $native['pay_attach'] = "";
        $native['pay_productname'] ='商品';

        BooView::set('payUrl', $platInfo['platInfo']['p_pay_url']);
        BooView::set('native', $native);
        BooView::display('pay/formPay.html');
        exit;
	}


}
