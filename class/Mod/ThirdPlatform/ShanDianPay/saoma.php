<?php
class Mod_ThirdPlatform_ShanDianPay_saoma extends Mod_ThirdPlatform_ShanDianPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;



        if ($type == 9 || $type == 2 || $type == 20 || $type == 21) { // 支付宝扫码或者支付宝wap
            $payType = '944';
        }

        $data = array(
            'pay_memberid' => $platInfo['platInfo']['appId'],
            'pay_orderid' => $payInfo['pOrderId'],
            'pay_applydate' => date('Y-m-d H:i:s'),
            'pay_bankcode' => $payType,
            'pay_notifyurl' => $platInfo['appInfo']['callbackUrl'] ,
            'pay_callbackurl' => 'http://www.baidu.com',
            'pay_amount' => $payInfo['amount'],
            'pay_clientip' => $_SERVER["REMOTE_ADDR"],
        );
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $md5str = $md5str .'key='.$platInfo['platInfo']['appKey'];
        $data['pay_md5sign'] = strtoupper(md5($md5str));
        $data['pay_productname'] = '充值';
        $str='<form class="form-inline" name="payform" method="post" action="'.$platInfo['platInfo']['p_pay_url'].'">';
        foreach ($data as $key => $val) {
            $str.='<input type="hidden" name="' . $key . '" value="' . $val . '">';
        }
        $str.="</form><script>document.forms['payform'].submit();</script>";
        exit($str);
    }
}
