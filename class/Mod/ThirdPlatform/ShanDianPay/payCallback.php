<?php
class Mod_ThirdPlatform_ShanDianPay_payCallback extends Mod_ThirdPlatform_ShanDianPay {
    protected function _do($platInfo = array()) {
        $data = array( // 返回字段
            "memberid" => $_REQUEST["memberid"], // 商户ID
            "orderid" =>  $_REQUEST["orderid"], // 订单号
            "amount" =>  $_REQUEST["amount"], // 交易金额
            "datetime" =>  $_REQUEST["datetime"], // 交易时间
            "transaction_id" =>  $_REQUEST["transaction_id"], // 支付流水号
            "returncode" => $_REQUEST["returncode"],
        );

        $sign = $_POST['sign'];
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $md5str = $md5str. 'key=' .$platInfo['platInfo']['appKey'];
        $signs = strtoupper(md5($md5str));

        //检测查询接口是否订单状态一致
        $checkData = [
            'pay_memberid' => $data['memberid'],
            'pay_orderid'  => $data['orderid'],
        ];
        $tradeSign = 'pay_memberid='.$checkData['pay_memberid'].'&pay_orderid='.$checkData['pay_orderid']. '&key=' .$platInfo['platInfo']['appKey'];
        $checkData['pay_md5sign'] = strtoupper(md5($tradeSign));
        $questUrl = "http://test.baolv168.com/Pay_Trade_query.html";
        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, $questUrl); // 要访问的地址
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
        curl_setopt($curl, CURLOPT_POSTFIELDS, $checkData); // Post提交的数据包
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        $result = json_decode(curl_exec($curl),1); // 执行操作
        curl_close($curl); // 关闭CURL会话

        //两道验证防御
        if ($sign == $signs && $data['returncode'] == '00' && $result['returncode'] == '00' ) {
            if($result['data'][0]['trade_state'] == '1' || $result['data'][0]['trade_state'] == '2'){
                echo 'OK';
                return array('platOrderId' => $_POST['orderid'],'realMoney' => $_POST['amount']);
            }
            echo "异常订单请注意，:".$checkData['pay_orderid'];
            exit;
        }
        return false;
    }

}
