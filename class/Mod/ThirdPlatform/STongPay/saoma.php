<?php
class Mod_ThirdPlatform_STongPay_saoma extends Mod_ThirdPlatform_STongPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];

        $payType =  0;
        if ($type == 5) {                       // 银联扫码云闪付
            $payType    = 'ysf';
        }else{
                   exit("通道错误！");
                }

        $postData = [
            'version'             => '2.0',                                                               //版本固定
            'merchant_id'         => $platInfo['platInfo']['appId'],//商户号              
            'order_price'         => $payInfo['amount'] * 100,    //1分=1 1元=100 无小数
            'out_trade_no'        => $payInfo['pOrderId'],                        //      商户自己的订单号
            'client_user_ip'      => $this->get_ip(),                                             //必须提供用户IP
            'pay_type'            => $payType,                                                    //支付类型
            'notify_url'          => $platInfo['appInfo']['callbackUrl'],         //通知消息回调页面
            'return_url'          => $platInfo['appInfo']['clientUrl'],           //支付成后跳转页面
            'nonce_str'           => md5(uniqid(microtime(true), true))
        ];

        $postData['sign']=$this->sign($postData,$platInfo['platInfo']['appKey']);
        $res = $this->http_post($platInfo['platInfo']['p_pay_url'],$postData);
        $data = json_decode($res,true);
        if ($data['status'] != 200 || $data['msg'] != 'success') {
            echo $data['msg'];
            exit;
        }

        BooView::set('payUrl', $data['pay_url']);
        BooView::display('pay/locationPay.html');
        exit;

    }


}
