<?php
class Mod_ThirdPlatform_STongPay extends Mod_ThirdPlatform {

    private $charset = 'utf8';

    protected function buildRequestForm($payUrl, $para_temp) {

        $sHtml = "正在跳转至支付页面...<form id='submit' name='submit' action='".$payUrl."' method='POST'>";
        foreach($para_temp as $key=>$val){
            if (false === $this->checkEmpty($val)) {
                $val = str_replace("'","&apos;",$val);
                $sHtml.= "<input type='hidden' name='".$key."' value='".$val."'/>";
            }
        }
        //submit按钮控件请不要含有name属性
        $sHtml = $sHtml."<input type='submit' value='ok' style='display:none;''></form>";
        $sHtml = $sHtml."<script>document.forms['submit'].submit();</script>";

        // echo $sHtml;
        return $sHtml;
    }

    public function generateSign($params, $paySecret) {
        return $this->sign($this->getSignContent($params, $paySecret));
    }

	
    public function sign($data,$key)
    {
        $newSign = "";
        if (count($data) > 0) {
            //签名步骤一：按字典序排序参数
            ksort($data);
            $string = "";
            foreach ($data as $k => $v) {
                if ($v != "" && !is_array($v) && $k!='sign') {
                    $string .= $k . "=" . $v . "&";
                }
            }
            //签名步骤二：在string后加入KEY
            $string = $string . "key=" . $key;
            //Log::info("SIGN1".$string);
            //签名步骤三：MD5加密
            $string = md5($string);
            //签名步骤四：所有字符转为大写
            $newSign = strtoupper($string);
            //Log::info("SIGN2".$newSign);

        }
        //echo $newSign;
        return $newSign;
    }

    public function http_post($url, $post)
    {
        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2); // 从证书中检查SSL加密算法是否存在
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla / 4.0 (compatible; MSIE 7.0; Windows NT 6.0; SLCC1; .NET CLR M.'.$this->mid.')'); // 模拟用户使用的浏览器
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($post)); // Post提交的数据包
        curl_setopt($curl, CURLOPT_TIMEOUT, 10); // 设置超时限制防止死循环
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回

        $res = curl_exec($curl); // 执行操作

        if (curl_errno($curl)) {

            echo 'Errno' . curl_error($curl);//捕抓异常

        }


        curl_close($curl); // 关闭CURL会话

        return $res; // 返回数据

    }

    public function get_ip(){
        if (getenv('HTTP_CLIENT_IP')) {
            $ip = getenv('HTTP_CLIENT_IP');
        }
        if (getenv('HTTP_X_REAL_IP')) {
            $ip = getenv('HTTP_X_REAL_IP');
        } elseif (getenv('HTTP_X_FORWARDED_FOR')) {
            $ip = getenv('HTTP_X_FORWARDED_FOR');
            $ips = explode(',', $ip);
            $ip = $ips[0];
        } elseif (getenv('REMOTE_ADDR')) {
            $ip = getenv('REMOTE_ADDR');
        } else {
            $ip = '0.0.0.0';
        }

        return $ip;
    }

}
