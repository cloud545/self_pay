<?php
class Mod_ThirdPlatform_STongPay_payCallback extends Mod_ThirdPlatform_STongPay {

    protected function _do($platInfo = array()) {
        $postData                   = array();
        //参与验签的参数
        $postData['version']            = $_POST['version'];                //商户订单号
        $postData['status']         = $_POST['status'];                 //付款类型
        $postData['msg']            = $_POST['msg'];                    //状态
        $postData['merchant_id']    = $_POST['merchant_id'];            //商户号
        $postData['paid_fee']       = $_POST['paid_fee'];               //支付金额
        $postData['paid_time']      = $_POST['paid_time'];              //支付时间
        $postData['out_trade_no']   = $_POST['out_trade_no'];           //商户订单号
        $postData['trade_id']       = $_POST['trade_id'];               //平台订单号
        $postData['nonce_str']      = $_POST['nonce_str'];              //随机数
        $postData['user_tag']      = $_POST['user_tag'];              //随机数
                $checkSign=$this->sign($postData,$platInfo['platInfo']['appKey']);

        $postData['sign']  = $_POST['sign'];                //签名

        if ($checkSign == $postData['sign'] &&  $postData['status'] == 200 && $postData['msg'] == 'success'){
            echo "success";
            return array('platOrderId' => $postData['trade_id'],'realMoney' => $postData['paid_fee'] / 100 );
        } else {
            echo "error";
            return false;
        }

    }

}
