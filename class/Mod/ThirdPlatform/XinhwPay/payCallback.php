<?php
class Mod_ThirdPlatform_XinhwPay_payCallback extends Mod_ThirdPlatform_XinhwPay {

    protected function _do($platInfo = array()) {

        $postData                   = array();
		//参与验签的参数(空值不参与验签)
        $postData['mercId']     		= $_POST['mercId'];         		//原订单商户编号
        $postData['mercOrderId']        = $_POST['mercOrderId'];           	//商户原订单编号
        $postData['orderStatus']     	= $_POST['orderStatus'];         	//支付状态
        $postData['createdTime']      	= $_POST['createdTime'];          	//订单创建时间
        $postData['paidTime']         	= $_POST['paidTime'];           	//用户支付时间 
        $postData['orderId']         	= $_POST['orderId'];           		//平台系统流水号
        $postData['txnAmt']         	= $_POST['txnAmt'];           		//订单金额 
        $postData['txnFee']         	= $_POST['txnFee'];           		//交易手续费 
        $postData['paidAmt']         	= $_POST['paidAmt'];           		//用户支付金额
        
		//进行验签
		$dStr = http_build_query($postData);
        $checkSign	= md5($dStr . $platInfo['platInfo']['appKey']);
		
        $postData['sign']           	= $_POST['sign'];            		//签名  
        
		if ($checkSign == $postData['sign']){
			if($postData['orderStatus'] == 'paid' || $postData['orderStatus'] == 'paid'){
				echo "success";
				return array('platOrderId' => $postData['orderId'],'realMoney' => $postData['txnAmt']);
			}else{
				return false;
			}
        }else{
            return false;
        }
    }
}

