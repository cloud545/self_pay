<?php
class Mod_ThirdPlatform_XinhwPay extends Mod_ThirdPlatform {

	
	/**
     *
     * @param $requiredKeys
     * @param $params
     * @throws F4stPayException
     */
    function __checkRequireField($requiredKeys, $params)
    {
        foreach ($requiredKeys as $key) {
            if (!key_exists($key, $params)) {
                throw new F4stPayException("missing required field: {$key}");
            }
        }
    }

    /**
     * 创建支付订单，返回支付二维码
     * @param array $data
     */
    function __addDefaultParam(&$data)
    {
        $data["mercId"] = $this->mercId;
        $data["time"] = time();
        $data["sign"] = $this->__sign($data);
    }

    /**
     * 签名
     * @param $data array
     * @return string
     */
    function __sign($data)
    {
        ksort($data);
        $dStr = http_build_query($data);
        return md5($dStr . $this->mercKey);
    }

	
	
	
	

}
