<?php
class Mod_ThirdPlatform_YinZuoPay_payCallback extends Mod_ThirdPlatform_YinZuoPay {

	protected function _do($platInfo = array()) {
        $data = $_POST;

        $sign = $_POST['sign'];
        unset($data['sign']);
        unset($data['sign_type']);
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            if($value) {
                $md5str = $md5str . $key . '=' . $value .'&';
            }
        }

        $signs = strtoupper(md5($md5str . $platInfo['platInfo']['appKey']));

        if ($sign == $signs && $data['status'] == '成功') {
            echo 'success';
            return array('platOrderId' => $_POST['tbbv1uvi1bto'],'realMoney' => $_POST['amount'] / 100);
        }
        return false;
	}
}