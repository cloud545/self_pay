<?php
class Mod_ThirdPlatform_YinZuoPay_saoma extends Mod_ThirdPlatform_YinZuoPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();
        $payType = 0;

        if ($type == 9) {   //个码支付宝扫码
            $payType = 'ALI_CODE';
        }
        if ($type == 10) {   //个码微信扫码
            $payType = 'WEIXIN_PAY_SCAN';
        }
        if ($type == 2) {
            $payType = 'ALI_PAY_WAP';
        }
        if ($type == 3) {
            $payType = 'WEIXIN_PAY_SCAN';
        }

        if($type == 5) {
            $payType = 'H5';
        }

        $data = array(
            'mer_id' => $platInfo['platInfo']['appId'],
            'terminal' => $payType,
            'timestamp' => date('Y-m-d H:i:s', time()),
            'businessnumber' => $payInfo['pOrderId'],
            'amount' => $payInfo['amount'] * 100,
            'version' => '01',
            'goodsName' => '充值' ,
            'backurl' => 'https://www.baidu.com',
            'failUrl' => 'https://www.baidu.com',
            'ServerUrl' => $platInfo['appInfo']['callbackUrl'],

        );
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }

        $data['sign_type'] = 'md5';
        $data['sign'] = strtoupper(md5($md5str . $platInfo['platInfo']['appKey']));

        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, $platInfo['platInfo']['p_pay_url']); // 要访问的地址
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        $result = json_decode(curl_exec($curl), 1); // 执行操作
        curl_close($curl); // 关闭CURL会话

        if ($result['result'] == 'success') {
            header('location:'.$result['data']['trade_qrcode']);
        } else {
            echo $result['msg'];
        }
    }
}
