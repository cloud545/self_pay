<?php
class Mod_ThirdPlatform_Jiabao_payCallback extends Mod_ThirdPlatform_Jiabao {

	protected function _do($platInfo = array()) {

        $content = file_get_contents("php://input");
        if ($content) {
            $_REQUEST = json_decode($content, true);
        }

        if (isset($_REQUEST['target'])) {
            unset($_REQUEST['target']);
        }

        if (isset($_REQUEST['appPayInfo'])) {
            unset($_REQUEST['appPayInfo']);
        }

        ksort($_REQUEST);
        $md5Str = '';
        foreach ($_REQUEST as $key => $value) {

            if (!$value || $key == 'sign') {
                continue;
            }

            if (!$md5Str) {
                $md5Str .= "{$key}={$value}";
            } else {
                $md5Str .= "&{$key}={$value}";
            }
        }

        $sign = strtoupper(md5($md5Str . "&key={$platInfo['platInfo']['appSecret']}"));
        if ($_REQUEST['sign'] == $sign && $_REQUEST['status'] == 2) {
            echo 'success';
            return array('platOrderId' => $_REQUEST['payOrderId'],'realMoney' => $_REQUEST['amount'] / 100);
        } else {
            //签名验证失败
            return false;
        }

	}

}
