<?php
class Mod_ThirdPlatform_XinLiYing_saoma extends Mod_ThirdPlatform_XinLiYing {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 10) {
            $payType = '10';
        }

        if ($type == 9) {
            $payType = '9';
        }

        if ($type == 2) {
            $payType = '2';
        }

        if ($type == 3) {
            $payType = '3';
        }

        $data = array(
            'appId' => $platInfo['platInfo']['appId'],
            'orderId' => $payInfo['pOrderId'],
            'amount' => $payInfo['amount'],
            'payType' => $payType,
            'remark' => '支付',
            'clientUrl' => 'https://www.baidu.com',
            'notifyUrl' => $platInfo['appInfo']['callbackUrl'],
            'time' => date('YmdHis'),
        );

        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $md5str = substr($md5str, 0 , strlen($md5str)-1);
        $data['sign'] = md5($md5str.$platInfo['platInfo']['appKey']);
        $str='<form class="form-inline" name="payform" method="post" action="'.$platInfo['platInfo']['p_pay_url'].'">';
        foreach ($data as $key => $val) {
            $str.='<input type="hidden" name="' . $key . '" value="' . $val . '">';
        }
        $str.="</form><script>document.forms['payform'].submit();</script>";
        exit($str);
    }
}
