<?php
class Mod_ThirdPlatform_XinLiYing_payCallback extends Mod_ThirdPlatform_XinLiYing {

	protected function _do($platInfo = array()) {

        $data = $_POST;
        $sign = $data['sign'];
        unset($data['sign']);
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $md5str = substr($md5str, 0 , strlen($md5str)-1);
        $signs = md5($md5str.$platInfo['platInfo']['appKey']);

        if ($sign == $signs) {
            echo 'success';
            return array('platOrderId' => $data['orderId'],'realMoney' => $data['amount']);
        }
        return false;
	}

}
