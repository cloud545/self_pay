<?php
class Mod_ThirdPlatform_BoLePay_payCallback extends Mod_ThirdPlatform_BoLePay {

	protected function _do($platInfo = array()) {

        $data = $_POST;
        $sign = $data['sign'];
        unset($data['sign']);
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $signs = md5($md5str.'app_secret='.$platInfo['platInfo']['appKey']);

        if ($sign == $signs) {
            echo 'PAY_SUCCESS';
            return array('platOrderId' => $data['trade_no'],'realMoney' => $data['money']);
        }
        return false;
	}
}