<?php
class Mod_ThirdPlatform_BoLePay_saoma extends Mod_ThirdPlatform_BoLePay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;


        if($type == 2 || $type == 9 ) {
            $payType = '10018';
        }

        if($type == 3 || $type == 10 ) {
            $payType = '10019';
        }

        $data = array(
            'app_id' => $platInfo['platInfo']['appId'],
            'trade_no' => $payInfo['pOrderId'],
            'money' => number_format($payInfo['amount'], '2', '.', ''),
            'channel_id' => $payType,
            'ip' => $_SERVER['REMOTE_ADDR'],
            'notify_url' => $platInfo['appInfo']['callbackUrl'],
        );

        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $data['sign'] = md5($md5str.'app_secret='.$platInfo['platInfo']['appKey']);
        $data = http_build_query(
            $data
        );
        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => $data
            )
        );
        $context = stream_context_create($opts);
        $res = file_get_contents($platInfo['platInfo']['p_pay_url'], false, $context);
        $result = json_decode($res,1);
        
        if($result['status'] == true){
            $log = "BoLePay pOrderId={$payInfo['pOrderId']}\t请求成功\n";
            $log = $log."支付地址：{$result['data']['url']} {$result['data']['qrcode']}";
        }else{
            $log = "BoLePay pOrderId={$payInfo['pOrderId']}\t请求失败\n";
            $log = $log."返回信息：{$res}";
        }
        BooLog::pay($log);

        if ($result['status'] == true) {
            if ($type == '2' || $type = '3') {
                header('location:'.$result['data']['url']);
            }

            if ($type == 9 || $type = 10) {
                header('location:'.$result['data']['qrcode']);
            }
        } else {
            echo $result['msg'];
        }
    }
}
