<?php
class Mod_ThirdPlatform_GeDiaoPay_saoma extends Mod_ThirdPlatform_GeDiaoPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];

        $payType =  0;
        if ($type == 2) {       // 支付宝wap
            $payType = 'alipaywap';
        } elseif ($type == 3) { // 微信wap
            exit;
        } elseif ($type == 5) { // 银联扫码
            $payType    = 'ysf';
        } elseif ($type == 9) { // 支付宝扫码
            exit;
        } elseif ($type == 10) { // 微信扫码
            exit;
        }


        $postData = [
            'version'           => '1.0',                                               //版本号固定
            'customerid'        => $platInfo['platInfo']['appId'],     //商户ID
            'total_fee'     => number_format($payInfo['amount'],2,'.',''),    //付款金额 
            'sdorderno'     => $payInfo['pOrderId'],              //订单号
            'notifyurl'         => $platInfo['appInfo']['callbackUrl'],                         //回调地址
            'returnurl'         => $platInfo['appInfo']['callbackUrl'],                         //回调地址
        ];


        //验签中
        $checkString = 'version='.$postData['version'].'&customerid='.$postData['customerid'].'&total_fee='.$postData['total_fee'].'&sdorderno='.$postData['sdorderno'].'&notifyurl='.$postData['notifyurl'].'&returnurl='.$postData['returnurl'];
        $checkString .= "&{$platInfo['platInfo']['appKey']}";
//echo $checkString;exit;
        $postData['sign'] = md5($checkString);
        $postData['paytype'] = $payType;
        $postData['commodityName'] = 'best1pays';
        $postData['client'] = $_SERVER["REMOTE_ADDR"];


        $formstr = '';
        foreach ($postData as $key => $val) {
            $formstr .= '<input type="hidden" name="' . $key . '" value="' . $val . '">';
        }

        echo '
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            </head>
            <body onLoad="document.zlinepay.submit();">
                <form name="zlinepay" method="post" action="' . $platInfo['platInfo']['p_pay_url'] . '" target="_top"/>'.$formstr.'</form>
            </body>
        </html>
        ';
        exit;
		

    }

}
