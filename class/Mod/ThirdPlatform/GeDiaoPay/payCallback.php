<?php
class Mod_ThirdPlatform_GeDiaoPay_payCallback extends Mod_ThirdPlatform_GeDiaoPay
{

    protected function _do($platInfo = array()){
        $postData = array();
        //参与验签的参数
        $postData['customerid'] 	= $_POST['customerid'];                //商户的订单号
        $postData['status'] 	= $_POST['status'];                        //商户的userId
        $postData['sdpayno'] 		= $_POST['sdpayno'];                       //订单号
        $postData['sdorderno'] 			= $_POST['sdorderno'];                   //订单金额
        $postData['total_fee'] 		= $_POST['total_fee'];                     //支付状态
        $postData['paytype'] 		= $_POST['paytype'];                     //交易支付方式
        
		//验签中

        $checkString = '';
        foreach ($postData as $key =>$value) {
            if (!$checkString) {
                $checkString = "{$key}={$value}";
            } else {
                $checkString .= "&{$key}={$value}";
            }
        }

        $checkString .= "&{$platInfo['platInfo']['appKey']}";
        $checkSign = md5($checkString);		


        if ($checkSign == $_POST['sign'] && $postData['status'] == 1) {
            echo "success";
            return array('platOrderId' => $postData['sdpayno'], 'realMoney' => $postData['total_fee']);
        } else {
            echo "error";
            return false;
        }

    }

}