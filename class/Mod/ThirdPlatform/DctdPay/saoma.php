<?php
class Mod_ThirdPlatform_DctdPay_saoma extends Mod_ThirdPlatform_DctdPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝wap
            $payType = 'alipaywzp';//$isMobile ? '904' : '903';
        } elseif ($type == 3) { // 微信wap
            $payType = 'wechatangelwap';
        } elseif ($type == 5) { // 银联扫码
            $payType = '926';
        } elseif ($type == 9) { // 支付宝扫码
            $payType = 'alipaywzp';
        } elseif ($type == 10) { // 微信扫码
            $payType = 'wechatangel';
        }

        $native = array(
            "outTradeNo" => $payInfo['pOrderId'],
            "amount" => number_format($payInfo['amount'],2,'.',''),
            "notifyUrl" => $platInfo['appInfo']['callbackUrl'],
            "appId" => $platInfo['platInfo']['appKey'],
            "merchantId" => $platInfo['platInfo']['appId'],
        );
        ksort($native);
        $md5str = "";
        foreach ($native as $key => $val) {
            $md5str = $md5str . $key . "=" . $val . "&";
        }

        $sign = md5($md5str . $platInfo['platInfo']['appSecret']);
        $native["sign"] = $sign;

        BooView::set('payUrl', $platInfo['platInfo']['p_pay_url'] . "?" . http_build_query($native));
        BooView::display('pay/locationPay.html');
        exit;
    }

}
