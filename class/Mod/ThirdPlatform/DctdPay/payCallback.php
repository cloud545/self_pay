<?php
class Mod_ThirdPlatform_DctdPay_payCallback extends Mod_ThirdPlatform_DctdPay {

	protected function _do($platInfo = array()) {

        $native = array(
            "outTradeNo" => $_REQUEST['outTradeNo'],
            "amount" => $_REQUEST['amount'],
            "appId" => $_REQUEST['appId'],
            "merchantId" => $_REQUEST['merchantId'],
        );
        ksort($native);
        $md5str = "";
        foreach ($native as $key => $val) {
            $md5str = $md5str . $key . "=" . $val . "&";
        }

        $sign = md5($md5str . $platInfo['platInfo']['appSecret']);

        if ($_REQUEST['sign'] == $sign){
            echo "success";
            return array('platOrderId' => $_REQUEST['outTradeNo'],'realMoney' => $_REQUEST['amount']);
        } else {
            return false;
        }

	}

}
