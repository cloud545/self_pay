<?php
class Mod_ThirdPlatform_NeedPay_payCallback extends Mod_ThirdPlatform_NeedPay {

	protected function _do($platInfo = array()) {

	    $postData = array();
        $postData['merchantId'] = $_GET['merchantId'];
        $postData['orderNo'] = $_GET['orderNo'];
        $postData['money'] = $_GET['money'];
        $postData['tradetime'] = $_GET['tradetime'];
        $postData['respCode'] = $_GET['respCode'];
        $postData['respMsg'] = $_GET['respMsg'];
        $sign = $_GET['sign'];

        $cheeckSign = $this->generateSign($postData, $platInfo['platInfo']['appKey']);
        if ($cheeckSign == $sign && $_GET['respCode'] == '00'){
            echo "true";
            return array('platOrderId' => $postData['orderNo'],'realMoney' => $postData['money'] / 100);
        } else {
            return false;
        }

	}

}
