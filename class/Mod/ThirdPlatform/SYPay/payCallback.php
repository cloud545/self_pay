<?php
class Mod_ThirdPlatform_SYPay_payCallback extends Mod_ThirdPlatform_SYPay{
    protected function _do($platInfo = array()){
        $postData = array();
        //获取支付回调参数
		$money                  = $_GET["money"];
		$mch_order_no           = $_GET["mch_order_no"];                //商户订单号
		$order_no               = $_GET["order_no"];                    //平台订单号
		$time_stamp             = $_GET["time_stamp"];                  //13位的支付时间戳
		$token                  = $_GET["token"];                               //验签参数
		$code                   = $_GET["code"];
		//校验验签参数是否正确
		$token_fields =[
			"money"         => $money,
			"mch_order_no"  => $mch_order_no,
			"order_no"      => $order_no,
			"time_stamp"    => $time_stamp
		];
		//生成token
        $my_token=$this->get_sign($token_fields,'8l475ow4m1z42uh6qcwg8rkks230jply');        
		//做判断了
		if ($my_token == $token && $code == 0) {
            echo "OK";
            return array('platOrderId' => $order_no, 'realMoney' => $money / 100);
        } else {
            echo "error";
            return false;
        }
    }

}
