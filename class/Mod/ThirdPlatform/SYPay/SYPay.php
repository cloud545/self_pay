<?php
class Mod_ThirdPlatform_SYPay extends Mod_ThirdPlatform {

    private $charset = 'utf8';

    protected function buildRequestForm($payUrl, $para_temp) {

        $sHtml = "正在跳转至支付页面...<form id='submit' name='submit' action='".$payUrl."' method='POST'>";
        foreach($para_temp as $key=>$val){
            if (false === $this->checkEmpty($val)) {
                $val = str_replace("'","&apos;",$val);
                $sHtml.= "<input type='hidden' name='".$key."' value='".$val."'/>";
            }
        }
        //submit按钮控件请不要含有name属性
        $sHtml = $sHtml."<input type='submit' value='ok' style='display:none;''></form>";
        $sHtml = $sHtml."<script>document.forms['submit'].submit();</script>";

        // echo $sHtml;
        return $sHtml;
    }

	//签名
	function get_sign($args=array(),$key=''){
		//签名步骤一：按字典序排序参数
		$Parameters=$args;
		ksort($Parameters);
		$String="";
		foreach ($Parameters as $k => $v){
		   $String.=$k."=".$v."&";
		}
		
		//签名步骤二：在string后加入KEY
		$String = $String."key=".$key;

		//签名步骤三：MD5加密
		$String=md5($String);

		return $String;
	}

	//获取13位的时间戳
	function getMillisecond() {
		list($t1, $t2) = explode(' ', microtime());
		return (float)sprintf('%.0f', (floatval($t1) + floatval($t2)) * 1000);
	}
}
