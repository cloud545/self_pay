<?php
class Mod_ThirdPlatform_SYPay_saoma extends Mod_ThirdPlatform_SYPay {

    protected function _do($platInfo = array(), $payInfo = array()) {
        
        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }
		
        $type = $platInfo['platInfo']['pt_id'];
        $payType =  0;
        if ($type == 2) {       // 支付宝wap
            $payType    = 2;
        } elseif ($type == 3) { // 微信wap
            $payType    = 1;
        } elseif ($type == 5) { // 银联扫码
            exit;
        } elseif ($type == 9) { // 支付宝扫码
            $payType    = 2;
        } elseif ($type == 10) { // 微信扫码
            $payType    = 1;
        }

		//获取商户配置
		$partner_no             = $platInfo['platInfo']['appId'];                       //商户编号
		$key_order              = $platInfo['platInfo']['appKey'];                      //商户下单密钥 


		//设置下单参数
        $data                   = $_REQUEST;
        $partner_no             = $partner_no; 								//必填，商户号或者客户号
        $mch_order_no   		= $payInfo['pOrderId']; 					//必填，商户订单号
        $body                   = 'best1pays'; 								//必填，商品名称
        $detail                 = ""; 									//选填，商品详情
        $money                  = $payInfo['amount'] * 100; 			//必填，订单金额, 单位`分`
        $attach                 = ""; 									//选填，透传字段,支付回调里原样返回
        $callback_url   		= $platInfo['appInfo']['callbackUrl']; 		//选填: 动态的支付回调地址, 默认是商户配置的 
        $time_stamp             = $this->getMillisecond(); 					//必填: 13位的时间戳，下单验签时用到
        $code_type              = $payType; 							//选填: 支付方式，1=微信 2=支付宝，默认是1

        //设置验签参数token
        $token_fields=array(
			"partner_no"    => $partner_no,
			"mch_order_no"  => $mch_order_no,
			"time_stamp"    => $time_stamp,
			"money"         => $money
		);

        $token=$this->get_sign($token_fields,$key_order);
        
		//统一下单参数
        $order_data=[
            'partner_no'        => $partner_no,
            'mch_order_no'      => $mch_order_no,
            'body'              => $body,
            'detail'            => $detail,
            'money'             => $money,
            'attach'            => $attach,
            'callback_url'      => $callback_url,
            'time_stamp'        => $time_stamp,
            'token'             => $token,
            'code_type'         => $code_type
        ];

		//开始下单
        $api_url	= $platInfo['platInfo']['p_pay_url']; //请填写实际的下单地址
		$post_url	= $api_url."?action=pay&m=pay_it&";
        foreach ($order_data as $key=>$val){
            $post_url.=$key.'='.$val.'&';
        }
        $post_url	= substr($post_url,0,-1);
		//请求地址带参数过去
		$response 	= BooCurl::call($post_url);
		$result 	= json_decode($response, true);
		
		//判断
		if ($result['code'] != 0) {
			echo "支付异常，请联系客服! 报错内容: {$result['msg']}";
		}else{
			BooView::set('payUrl', $result['code_link']);
			BooView::display('pay/locationPay.html');
			exit;
		}
    }
}
