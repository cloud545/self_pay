<?php
class Mod_ThirdPlatform_FeizaiPay_saoma extends Mod_ThirdPlatform_FeizaiPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];

        $payType = $joinType = 0;
        if ($type == 2) {       // 支付宝wap
            $payType = 'alipay';
            $mode    = 'wap';
        } elseif ($type == 3) { // 微信wap
            $payType = 'wechat';
            $mode    = 'wap';
        } elseif ($type == 5) { // 银联扫码
            $payType = '926';
        } elseif ($type == 9) { // 支付宝扫码
            $payType = 'alipay';
            $mode    = 'qrcode';
        } elseif ($type == 10) { // 微信扫码
            $payType = 'wechat';
            $mode    = 'qrcode';
        }

        $postData = array(
            'appId'         => $platInfo['platInfo']['appId'],                  //商户账号
            'reqStreamId'   => $payInfo['pOrderId'],                        //商户请求流水号
            'payType'       => $payType,                                        //支付通道
            'mode'          => $mode,                                       //支付模式
            'face'          => $payInfo['amount'],                              //充值面额
            'timeStamp'     => date('YmdHis',time()),                           //时间戳
            'clientIp'      => BooUtil::realIp(),                                   //客户端IP
        );

        $postData["sign"] = md5($postData['appId'].$postData['reqStreamId'].$postData['payType'].$postData['mode'].$postData['clientIp'].$postData['face'].$postData['timeStamp'].$platInfo['platInfo']['appKey']);
        BooCurl::setData($postData);
        $res = BooCurl::call($platInfo['platInfo']['p_pay_url']);
        $data = json_decode($res, true);
        if ($data['status'] != 0) {
            echo $data['msg'];
            exit;
        }

        if ($type == 9 ) {
            BooView::set('url', $data['data']['payUrl']);
            BooView::display('pay/saoma.html');
        } else {
            BooView::set('payUrl', $data['data']['payUrl']);
            BooView::display('pay/locationPay.html');
        }
        exit;
    }

}
