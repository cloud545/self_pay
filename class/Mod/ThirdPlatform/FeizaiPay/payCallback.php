<?php
class Mod_ThirdPlatform_FeizaiPay_payCallback extends Mod_ThirdPlatform_FeizaiPay {

    protected function _do($platInfo = array()) {

        $postData                   = array();
        $postData['appId']          = $_GET['appId'];           //appId账号
        $postData['reqStreamId']    = $_GET['reqStreamId'];     //商户下单请求流水号
        $postData['orderNo']        = $_GET['orderNo'];         //我方流水
        $postData['payMoney']       = $_GET['payMoney'];        //实际支付金额
        $postData['face']           = $_GET['face'];            //订单创建金额
        $postData['payState']       = $_GET['payState'];        //支付状态
        $postData['sign']           = $_GET['sign'];            //签名

        $checkSign = md5($postData['appId'].$postData['reqStreamId'].$postData['orderNo'].$postData['payMoney'].$postData['face'].$postData['payState'].$platInfo['platInfo']['appKey']);

        if ($checkSign == $postData['sign'] &&  $postData['payState'] == 0){
            echo "0";
            return array('platOrderId' => $postData['orderNo'],'realMoney' => $postData['payMoney'] );
        } else {
            echo "1";
            return false;
        }

    }

}

