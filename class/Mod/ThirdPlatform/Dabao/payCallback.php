<?php
class Mod_ThirdPlatform_Dabao_payCallback extends Mod_ThirdPlatform_Dabao {

	protected function _do($platInfo = array()) {

        $postData = array();
        $postData['merchant'] = $_REQUEST['merchant'];
        $postData['qrtype'] = $_REQUEST['qrtype'];
        $postData['customno'] = $_REQUEST['customno'];
        $postData['sendtime'] = $_REQUEST['sendtime'];
        $postData['orderno'] = $_REQUEST['orderno'];
        $postData['money'] = $_REQUEST['money'];
        $postData['paytime'] = $_REQUEST['paytime'];
        $postData['state'] = $_REQUEST['state'];

        $sign = md5("merchant={$postData['merchant']}&qrtype={$postData['qrtype']}&customno={$postData['customno']}&sendtime={$postData['sendtime']}&orderno={$postData['orderno']}&money={$postData['money']}&paytime={$postData['paytime']}&state={$postData['state']}{$platInfo['platInfo']['appKey']}");
        if ($_REQUEST['sign'] == $sign && $postData['state'] == 1) {
            echo 'OK';
            return array('platOrderId' => $postData['orderno'],'realMoney' => $postData['money']);
        } else {
            //签名验证失败
            return false;
        }

	}

}
