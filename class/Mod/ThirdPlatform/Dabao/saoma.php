<?php
class Mod_ThirdPlatform_Dabao_saoma extends Mod_ThirdPlatform_Dabao {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝wap
            $payType = 'aph5';
        } elseif ($type == 3) { // 微信wap
            $payType = 'wph5';
        } elseif ($type == 5) { // 银联
            $payType = $isMobile ? 'ysfh5' : 'ysf';
        } elseif ($type == 9) { // 支付宝扫码
            $payType = 'ap';
        } elseif ($type == 10) { // 微信扫码
            $payType = 'wp';
        }

        $postData = array();
        $postData['merchant'] = $platInfo['platInfo']['appId'];
        $postData['qrtype'] = $payType;
        $postData['customno'] = $payInfo['pOrderId'];
        $postData['money'] = $payInfo['amount'];
        $postData['sendtime'] = time();
        $postData['notifyurl'] = $platInfo['appInfo']['callbackUrl'];
        $postData['backurl'] = $platInfo['appInfo']['clientUrl'];
        $postData['risklevel'] = '';

        $sign = md5("merchant={$postData['merchant']}&qrtype={$postData['qrtype']}&customno={$postData['customno']}&money={$postData['money']}&sendtime={$postData['sendtime']}&notifyurl={$postData['notifyurl']}&backurl={$postData['backurl']}&risklevel={$postData['risklevel']}{$platInfo['platInfo']['appKey']}");
        $postData['sign'] = $sign;

        BooView::set('payUrl', $platInfo['platInfo']['p_pay_url']);
        BooView::set('native', $postData);
        BooView::display('pay/formPay.html');
        exit;
    }

}
