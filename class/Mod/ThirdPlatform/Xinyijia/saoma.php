<?php
class Mod_ThirdPlatform_Xinyijia_saoma extends Mod_ThirdPlatform_Xinyijia {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝wap
            $payType = 'alipay';
        } elseif ($type == 3) { // 微信wap
            $payType = 'wxpay';
        } elseif ($type == 5) { // 银联
            $payType = 'uppay';
        } elseif ($type == 9) { // 支付宝扫码
            $payType = 'alipay';
        } elseif ($type == 10) { // 微信扫码
            $payType = 'wxpay';
        }

        $postData = array();
        $postData['MerId'] = $platInfo['platInfo']['appId'];
        $postData['PayType'] = $payType;
        $postData['MerOrderNo'] = $payInfo['pOrderId'];
        $postData['MerOrderTime'] = date('YmdHis');
        $postData['UserId'] = 1;
        $postData['Amount'] = $payInfo['amount'] * 100;
        $postData['NotifyUrl'] = $platInfo['appInfo']['callbackUrl'];

        $sign = strtoupper(md5(strtoupper(md5("{$postData['Amount']}|{$postData['MerOrderNo']}|{$postData['MerOrderTime']}|{$platInfo['platInfo']['appKey']}"))));
        $postData['Sign'] = $sign;

        BooCurl::setData($postData, 'POST');
        $response = BooCurl::call($platInfo['platInfo']['p_pay_url']);

        $result = json_decode($response, true);
        if ($result['code'] != 'success') {
            BooFile::write('/tmp/xinyijia.log', $response . "\n{$payInfo['amount']}\n{$platInfo['appInfo']['name']}\n111\n\n", 'a');
            echo "支付异常，请联系客服! code：{$result['code']}，message: {$result['message']}";
            exit;
        }

        BooView::set('payUrl', $result['data']['PayUrl']);
        BooView::display('pay/locationPay.html');
        exit;
    }

}
