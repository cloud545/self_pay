<?php
class Mod_ThirdPlatform_XinJiabao_saoma extends Mod_ThirdPlatform_XinJiabao {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        $terminalType = '0';
        if ($type == 2) { // 支付宝wap
            $payType = 'Alipay';
            $terminalType = '2';
        }
        if ($type == 9) { // 支付宝扫码
            $payType = 'Alipay';
            $terminalType = '1';
        }


        $data['merchantCode'] = $platInfo['platInfo']['appId'];
        $data['signType'] = 'md5';

        $content['merchantCode'] = $platInfo['platInfo']['appId'];
        $content['channel'] = $payType;
        $content['merchantTradeNo'] = $payInfo['pOrderId'];
        $content['userId'] = '144';
        $content['amount'] = number_format($payInfo['amount'], '2' , '.', '');
        $content['terminalType'] = $terminalType;
        $content['notifyUrl'] = $platInfo['appInfo']['callbackUrl'];

        ksort($content);
        $md5Str = '';
        foreach ($content as $key => $value) {
            if ($value) {
                $md5Str .= $value;
            }
        }

        $sign = md5($md5Str . $platInfo['platInfo']['appKey']);
        $content['sign'] = $sign;
        $data['content'] = json_encode($content);
        $data = json_encode($data);
        $ch = curl_init($platInfo['platInfo']['p_pay_url']);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data))
        );
        $result = json_decode(curl_exec($ch),1);
        if ($result['status'] == 'SUCCESS') {
            $data = json_decode($result['data'], 1);
            $result = json_decode($data['content'], 1);
            header('location:'.$result['payUrl']);
        } else {
            echo $result['msg'];
        }
    }
}
