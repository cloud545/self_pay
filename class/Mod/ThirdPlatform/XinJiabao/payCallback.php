<?php
class Mod_ThirdPlatform_XinJiabao_payCallback extends Mod_ThirdPlatform_XinJiabao {

	protected function _do($platInfo = array()) {

        $data = json_decode(file_get_contents("php://input"), 1);
        $content = json_decode($data['content'], 1);
        $sign = $content['sign'];
        unset($content['sign']);
        $md5str = '';
        foreach ($content as $key => $value) {
            if ($value) {
                $md5str .= $value;
            }
        }

        $signs = md5($md5str.$platInfo['platInfo']['appKey']);
        if ($signs == $sign && $content['tradeStatus'] == 'PAYMENT_SUCCESS') {
            echo 'SUCCESS';
            return array('platOrderId' => $content['merchantTradeNo'],'realMoney' => $content['amount']);
        }
        return false;
	}

}
