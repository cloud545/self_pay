<?php
class Mod_ThirdPlatform_Kuaifu_payCallback extends Mod_ThirdPlatform_Kuaifu {

	protected function _do($platInfo = array()) {

        $fxid = $_REQUEST['fxid']; //商户编号
        $fxddh = $_REQUEST['fxddh']; //商户订单号
        $fxorder = $_REQUEST['fxorder']; //平台订单号
        $fxdesc = $_REQUEST['fxdesc']; //商品名称
        $fxfee = $_REQUEST['fxfee']; //交易金额
        $fxattch = $_REQUEST['fxattch']; //附加信息
        $fxstatus = $_REQUEST['fxstatus']; //订单状态
        $fxtime = $_REQUEST['fxtime']; //支付时间
        $fxsign = $_REQUEST['fxsign']; //md5验证签名串

        $mysign = md5($fxstatus . $fxid . $fxddh . $fxfee . $platInfo['platInfo']['appKey']); //验证签名

        if ($fxsign == $mysign) {
            if ($fxstatus == '1') {//支付成功
                echo 'success';
                return array('platOrderId' => $fxorder ? $fxorder : $fxddh, 'realMoney' => $fxfee);
            } else { //支付失败
                echo 'fail';
                return false;
            }
        } else {
            echo 'sign error';
            return false;
        }

	}

}
