<?php
class Mod_ThirdPlatform_YunPay_payCallback extends Mod_ThirdPlatform_YunPay {

	protected function _do($platInfo = array()) {

        $data = array(
            'appid' => $_POST['appid'],
            'out_no' => $_POST['out_no'],
            'out_trade_no' => $_POST['out_trade_no'],
            'money' => $_POST['money'],
            'type' => $_POST['money'],
            'status' => $_POST['status'],
            'pay_time' => $_POST['pay_time'],
            'body' => $_POST['body'],
        );

        $key  = $platInfo['platInfo']['appKey'];
        $sign_old = $_POST['sign'];
        $sign = $this->createSign($data,$key);
        if($sign == $sign_old){

            $checkOrderData = array(
                'appid' => $_POST['appid'],
                'out_trade_no' => $_POST['out_trade_no'],
            );

            $checkOrderData['sign'] = $this->createSign($checkOrderData, $platInfo['platInfo']['appKey']);
            $result = $this->sendPostRequst('http://www.shguangwen88.com/api/pay/order_query', $checkOrderData);
            $res = json_decode($result,true);
            if ($res['rsp_code'] == 1 && $res['status'] == 1) {
                echo 'success';
                return array('platOrderId' => $res['out_no'], 'realMoney' => $res['realprice']);
            } else {
                return false;
            }

        } else {
            //签名验证失败
            return false;
        }

	}

}
