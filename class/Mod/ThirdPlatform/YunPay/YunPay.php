<?php
class Mod_ThirdPlatform_YunPay extends Mod_ThirdPlatform {

    protected function createSign($data=array(), $apikey = ''){
        if (empty($data) || empty($apikey)) {
            return false;
        }
        ksort($data);
        $md5str = "";
        foreach ($data as $key => $val) {
            $md5str = $md5str . $key . "=" . $val . "&";
        }
        //生成签名
        return strtoupper(md5($md5str . "key=" . $apikey));
    }

    //post请求返回数据
    protected function sendPostRequst($url, $data){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);//设置为post
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);//data为post请求的参数
        $res = curl_exec ($ch);//$return为接收到的返回值
        curl_close($ch);
        return $res;
    }

}
