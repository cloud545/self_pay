<?php
class Mod_ThirdPlatform_YunPay_saoma extends Mod_ThirdPlatform_YunPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝wap
            $payType = 'alipay';
        } elseif ($type == 3) { // 微信wap
            $payType = 'wechat';
        } elseif ($type == 5) { // 银联
            $payType = 4001;
        } elseif ($type == 9) { // 支付宝扫码
            $payType = 'alipay';
        } elseif ($type == 10) { // 微信扫码
            $payType = 'wechat';
        }

        header("Content-type:text/html;charset=utf-8");

        $data = array(
            'appid' => $platInfo['platInfo']['appId'],
            'out_trade_no' => $payInfo['pOrderId'],
            'money' => $payInfo['amount'],
            'type' => $payType,
            'notifyurl' => $platInfo['appInfo']['callbackUrl'],
            'returnurl' => $platInfo['appInfo']['clientUrl'],
            'body' => '支付',
        );

        $data['sign'] = $this->createSign($data, $platInfo['platInfo']['appKey']);
        //$result = $this->sendPostRequst($platInfo['platInfo']['p_pay_url'], $data);
        //$res = json_decode($result,true);

        BooView::set('payUrl', $platInfo['platInfo']['p_pay_url']);
        BooView::set('native', $data);
        BooView::display('pay/formPay.html');
        exit;
    }

}
