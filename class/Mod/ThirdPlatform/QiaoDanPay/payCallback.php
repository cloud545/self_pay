<?php
class Mod_ThirdPlatform_QiaoDanPay_payCallback extends Mod_ThirdPlatform_QiaoDanPay {

	protected function _do($platInfo = array()) {

        $data = json_decode(file_get_contents("php://input"), 1);
        $sign = $data['sign'];
        unset($data['sign']);
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $md5str = substr($md5str, 0 , strlen($md5str)-1);
        $signs = strtoupper(md5($md5str.$platInfo['platInfo']['appKey']));

        if ($sign == $signs || $data['result_code'] == 'success') {
            echo 'SUCCESS';
            return array('platOrderId' => $data['mch_order_no'],'realMoney' => $data['real_amount']);
        }
        return false;
	}

}
