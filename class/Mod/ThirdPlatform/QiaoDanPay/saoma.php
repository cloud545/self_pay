<?php
class Mod_ThirdPlatform_QiaoDanPay_saoma extends Mod_ThirdPlatform_QiaoDanPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;

        if ($type == 9) {
            $payType = 'alipay.qr';
        }

        $data = array(
            'mch_id' => $platInfo['platInfo']['appId'],
            'mch_order_no' => $payInfo['pOrderId'],
            'total_amount' => number_format($payInfo['amount'], '2', '.',''),
            'method' => $payType,
            'version' => '1.0',
            'body' => '可口可乐',
            'cur_code' => 'CNY',
            'nonce_str ' => time(),
            'notify_url' => $platInfo['appInfo']['callbackUrl'],
            'spbill_create_ip' => $_SERVER['REMOTE_ADDR'],
            'mch_req_time' => date('YmdHis'),
        );
        var_dump($data);
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $md5str = substr($md5str, 0 , strlen($md5str)-1);
        $data['sign'] = strtoupper(md5($md5str.$platInfo['platInfo']['appKey']));
        $data=str_ireplace("\/","/",json_encode($data));
        $opts=array(
            'http'=>array(
                'method'=>'POST',
                "timeout"=>10,
                'header'=>"Content-type: application/json\r\n".
                    "Content-Length: ".strlen($data)."\r\n",
                'content'=>$data
            ),
        );
        $context=stream_context_create($opts);
        $html=file_get_contents($platInfo['platInfo']['p_pay_url'],false,$context);
        $result = json_decode($html, true);

        if ($result['result_code'] == 'SUCCESS') {
            header('location:'.$result['code_url']);
        } else {
            echo $result['return_msg'];
        }
    }
}
