<?php
class Mod_ThirdPlatform_AppPay_saoma extends Mod_ThirdPlatform_AppPay {
    protected function _do($platInfo = array(), $payInfo = array()) {
        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }
        $type = $platInfo['platInfo']['pt_id'];
        $payType =  0;
        if ($type == 2) {       // 支付宝wap
            $payType 		= '6';
        } elseif ($type == 3) { // 微信wap
            $payType        = '4';
        } elseif ($type == 5) { // 银联扫码
            exit;
        } elseif ($type == 9) { // 支付宝扫码
            $payType        = '6';
        } elseif ($type == 10) { // 微信扫码
            exit;
        }

        $postData = [
            'CMDID'     => 'A_PAYURL',                             //命令ID
            'AGTID'     => $platInfo['platInfo']['appId'],        //代理商ID
            'AGTORDID'  => $payInfo['pOrderId'],                  //代理商订单ID
            'PAYTYPE'   => $payType,                               //支付类型
            'PARVALUE'  => $payInfo['amount'],                    //面值
            'CRTTIME'   => date("YmdHis"),                        //下单时间
            'NTFURL'    => $platInfo['appInfo']['callbackUrl'],                               //通知地址
            'USERID'    => "best1pays",                           //支付用户ID
        ];

        //验签加密
        ksort($postData);
        $checkString = '';
        foreach ($postData as $key =>$value) {
            if (!$checkString) {
                $checkString = "{$key}={$value}";
            } else {
                $checkString .= "&{$key}={$value}";
            }
        }

        $checkString .= "{$platInfo['platInfo']['appKey']}";
        $postData['SIGN'] = md5($checkString);
        $postData = http_build_query($postData);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $platInfo['platInfo']['p_pay_url']);
        curl_setopt($curl, CURLOPT_USERAGENT,'Opera/9.80 (Windows NT 6.2; Win64; x64) Presto/2.12.388 Version/12.15');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // stop verifying certificate
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        $r = curl_exec($curl);
        curl_close($curl);
        $res = json_decode($r, true);
        //暂时不做验签
        if ($res['STATUS'] != 0) {
            echo $res['STATUS'];
            exit;
        }
        //映射到页面
        BooView::set('payUrl', $res['PAYURL']);
        BooView::display('pay/locationPay.html');
        exit;
    }
}


