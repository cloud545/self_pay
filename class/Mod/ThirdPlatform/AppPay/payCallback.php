<?php
class Mod_ThirdPlatform_AppPay_payCallback extends Mod_ThirdPlatform_AppPay{
    protected function _do($platInfo = array()){
        $postData = array();
        //参与验签的参数
        $postData['CMDID']          = $_POST['CMDID'];                          //命令ID
        $postData['AGTID']          = $_POST['AGTID'];                          //代理商ID
        $postData['AGTORDID']       = $_POST['AGTORDID'];                       //代理商订单ID
        $postData['TRDORDID']       = $_POST['TRDORDID'];                        //我司系统订单号
        $postData['TRDERRCODE']     = $_POST['TRDERRCODE'];                        //我司系统订单号
        $postData['TRDREQTIME']     = $_POST['TRDREQTIME'];                     //我司系统下单时间
        $postData['TRDFINTIME']     = $_POST['TRDFINTIME'];                     //我司系统完成时间
        $postData['TRDFINMONEY']    = $_POST['TRDFINMONEY'];                    //我司系统支付完成金额
        $postData['TRDSTATUS']      = $_POST['TRDSTATUS'];                     //订单状态
        //验签中
        ksort($postData);
        $checkString = '';
        foreach ($postData as $key =>$value) {
            if (!$checkString) {
                $checkString = "{$key}={$value}";
            } else {
                $checkString .= "&{$key}={$value}";
            }
        }
        $checkString    .= "{$platInfo['platInfo']['appKey']}";
        $checkSign      = md5($checkString);
        if ($checkSign == $_POST['SIGN']) {
            echo "success";
            if($postData['TRDSTATUS'] != 0){
                exit;
            }
            return array('platOrderId' => $postData['TRDORDID'], 'realMoney' => $postData['TRDFINMONEY']);
        } else {
            echo "error";
            exit;
            return false;
        }
    }

}

