<?php
class Mod_ThirdPlatform_HuiJieTong_payCallback extends Mod_ThirdPlatform_HuiJieTong {

	protected function _do($platInfo = array()) {
        $data = $_POST;

        $sign = $data['sign'];
        unset($data['sign']);
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }

        $signs = strtoupper(md5($md5str.'key='.$platInfo['platInfo']['appKey']));

        if ($sign == $signs && $data['status'] == '1') {
            echo 'success';
            return array('platOrderId' => $data['out_trade_no'],'realMoney' => $data['total_fee']);
        }
        return false;
	}
}
