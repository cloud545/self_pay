<?php
class Mod_ThirdPlatform_BituexPay_saoma extends Mod_ThirdPlatform_BituexPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        date_default_timezone_set('UTC');

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝wap
            $payType = 'USDT';// $isMobile ? 8007 : 8006;
        } elseif ($type == 3) { // 微信wap
            $payType = 'USDT';
        } elseif ($type == 4) { // qq
            $payType = 'USDT';
        } elseif ($type == 5) { // 银联
            $payType = 'USDT';
        } elseif ($type == 6) { // 京东
            $payType = 'USDT';
        } elseif ($type == 9) { // 支付宝扫码
            $payType = 'USDT';
        } elseif ($type == 10) { // 微信扫码
            $payType = 'USDT';
        } else {
            $payType = 'USDT';
        }

        $postData = array();
        $postData['mid'] = $platInfo['platInfo']['appId'];
        $postData['coin'] = $payType;
        $postData['currency_type'] = 'CNY';
        $postData['quantity'] = $payInfo['amount'];
        $postData['out_trade_no'] = $payInfo['pOrderId'];
        $postData['notify_url'] = $platInfo['appInfo']['callbackUrl'];
        $postData['return_url'] = $platInfo['appInfo']['clientUrl'];
        $postData['user_tag'] = $platInfo['platInfo']['appSecret'];
        $postData['subject'] = '用户充值';
        $postData['body'] = '用户充值';
        $postData['timestamp'] = date('Y-m-d H:i:s');

        ksort($postData);
        $md5Str = '';
        foreach ($postData as $key => $value) {

            if (!$value) {
                continue;
            }

            if (!$md5Str) {
                $md5Str .= "{$key}={$value}";
            } else {
                $md5Str .= "&{$key}={$value}";
            }
        }

        $sign = md5($md5Str . "&access_key_secret={$platInfo['platInfo']['appKey']}");
        $postData['sign'] = $sign;

        BooView::set('payUrl', $platInfo['platInfo']['p_pay_url'] . "?{$md5Str}&sign={$sign}");
        BooView::display('pay/locationPay.html');
        //header('Location: ' . $result['payUrl']);
        exit;
    }

}
