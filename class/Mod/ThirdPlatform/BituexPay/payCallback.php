<?php
class Mod_ThirdPlatform_BituexPay_payCallback extends Mod_ThirdPlatform_BituexPay {

	protected function _do($platInfo = array()) {

        if (!$_REQUEST['mid']) {
            $content = file_get_contents("php://input");
            if ($content) {
                $_REQUEST = json_decode($content, true);
            }
        }

        $postData = array();
        $postData['status'] = $_REQUEST['status'];
        $postData['pay_order_no'] = $_REQUEST['pay_order_no'];
        $postData['out_trade_no'] = $_REQUEST['out_trade_no'];
        $postData['mid'] = $_REQUEST['mid'];
        $postData['coin'] = $_REQUEST['coin'];
        $postData['quantity'] = $_REQUEST['quantity'];
        $postData['currency_type'] = $_REQUEST['currency_type'];
        $postData['amount'] = $_REQUEST['amount'];
        $postData['payer_mobile'] = $_REQUEST['payer_mobile'];
        $postData['payer_email'] = $_REQUEST['payer_email'];
        $postData['paid_time'] = $_REQUEST['paid_time'];
        $postData['unit_price'] = $_REQUEST['unit_price'];
        $postData['currency_symbol'] = $_REQUEST['currency_symbol'];
        $postData['subject'] = $_REQUEST['subject'];
        $postData['body'] = $_REQUEST['body'];

        ksort($postData);
        $md5Str = '';
        foreach ($postData as $key => $value) {

            if (!$md5Str) {
                $md5Str .= "{$key}={$value}";
            } else {
                $md5Str .= "&{$key}={$value}";
            }
        }

        $sign = md5($md5Str . "&access_key_secret={$platInfo['platInfo']['appKey']}");
        if ($_REQUEST['sign'] == $sign && $_REQUEST['status'] == 1) {
            echo json_encode(array('status' => 0, 'msg' => ''));
            return array('platOrderId' => $_REQUEST['pay_order_no'],'realMoney' => $_REQUEST['amount']);
        } else {
            //签名验证失败
            return false;
        }

	}

}
