<?php
class Mod_ThirdPlatform_FunPay_saoma extends Mod_ThirdPlatform_FunPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;

        if ($type == 3) {
            $payType = '30';
        }

        $data = array(
            'merchantId' => $platInfo['platInfo']['appId'],
            'orderId' => $payInfo['pOrderId'],
            'amount' => $payInfo['amount'],
            'payType' => $payType,
            'requestIp' => $_SERVER['REMOTE_ADDR'],
            'goodsName' => 'IPhone11',
            'goodsDesc' => 'IPhone11',
            'returnUrl' => 'https://www.baidu.com',
            'notifyUrl' => $platInfo['appInfo']['callbackUrl'],
            'orderTime' => date('YmdHisHis'),
        );

        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $data['sign'] = strtoupper(md5($md5str.'key='.$platInfo['platInfo']['appKey']));

        $str='<form class="form-inline" name="payform" method="post" action="'.$platInfo['platInfo']['p_pay_url'].'">';
        foreach ($data as $key => $val) {
            $str.='<input type="hidden" name="' . $key . '" value="' . $val . '">';
        }
        $str.="</form><script>document.forms['payform'].submit();</script>";
        exit($str);
    }
}
