<?php
class Mod_ThirdPlatform_FunPay_payCallback extends Mod_ThirdPlatform_FunPay {

	protected function _do($platInfo = array()) {

        $data = json_decode(file_get_contents("php://input"),1);
        $sign = $data['sign'];
        unset($data['sign']);
        unset($data['random']);
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $signs = strtoupper(md5($md5str. 'key=' .$platInfo['platInfo']['appKey']));

        if ($sign == $signs && $data['status'] == '4') {
            echo 'SUCCESS';
            return array('platOrderId' => $data['orderId'],'realMoney' => $data['amount']);
        } else {
            echo 'Fail';
        }
	}

}
