<?php
class Mod_ThirdPlatform_DaZFB_payCallback extends Mod_ThirdPlatform_DaZFB {
    protected function _do($platInfo = array()) {

        if(!isset($_REQUEST["sign"]) ){
            echo "fail(sign not exists)";
            return false;
        }

        $paramArray = [];

        $resSign = $_REQUEST["sign"] ;

        if(isset($_REQUEST["payOrderId"]) ){
            $paramArray["payOrderId"] = $_REQUEST["payOrderId"];
        }

        if(isset($_REQUEST["income"]) ){
            $paramArray["income"] = $_REQUEST["income"];
        }

        if(isset($_REQUEST["mchId"]) ){
            $paramArray["mchId"] = $_REQUEST["mchId"];
        }

        if(isset($_REQUEST["appId"]) ){
            $paramArray["appId"] = $_REQUEST["appId"];
        }

        if(isset($_REQUEST["productId"]) ){
            $paramArray["productId"] = $_REQUEST["productId"];
        }

        if(!isset($_REQUEST["mchOrderNo"]) ){
            echo '参数丢失';
            return false;
        }

        if(isset($_REQUEST["mchOrderNo"]) ){
            $paramArray["mchOrderNo"] = $_REQUEST["mchOrderNo"];
        }

        if(isset($_REQUEST["amount"]) ){
            $paramArray["amount"] = $_REQUEST["amount"];
        }

        if(isset($_REQUEST["status"]) ){
            $paramArray["status"] = $_REQUEST["status"];
        }

        if(isset($_REQUEST["channelOrderNo"]) ){
            $paramArray["channelOrderNo"] = $_REQUEST["channelOrderNo"];
        }

        if(isset($_REQUEST["channelAttach"]) ){
            $paramArray["channelAttach"] = $_REQUEST["channelAttach"];
        }

        if(isset($_REQUEST["param1"]) ){
            $paramArray["param1"] = $_REQUEST["param1"];
        }

        if(isset($_REQUEST["param2"]) ){
            $paramArray["param2"] = $_REQUEST["param2"];
        }

        if(isset($_REQUEST["paySuccTime"]) ){
            $paramArray["paySuccTime"] = $_REQUEST["paySuccTime"];
        }

        if(isset($_REQUEST["backType"]) ){
            $paramArray["backType"] = $_REQUEST["backType"];
        }

        $sign = $this->paramArraySign($paramArray, $platInfo['platInfo']['appSecret']);  //签名

        if($resSign != $sign){
            echo "fail(sign not exists)";
            return false;
        }

        if ($_REQUEST['status'] != 2) {
            echo "fail(status = {$_REQUEST['status']})";
            return false;
        }
        echo "success";
        return array('platOrderId' => $_REQUEST['payOrderId'],'realMoney' => $_REQUEST['amount'] / 100);
    }

    public  function paramArraySign($paramArray, $mchKey){

        ksort($paramArray);  //字典排序
        reset($paramArray);

        $md5str = "";
        foreach ($paramArray as $key => $val) {
            if( !empty($key) && !empty($val) ){
                $md5str = $md5str . $key . "=" . $val . "&";
            }
        }
        $sign = strtoupper(md5($md5str . "key=" . $mchKey));  //签名

        return $sign;

    }

    public function httpPost($url, $paramStr){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $paramStr,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return $err;
        }
        return $response;
    }

}
