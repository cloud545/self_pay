<?php
class Mod_ThirdPlatform_Yida_payCallback extends Mod_ThirdPlatform_Yida {

	protected function _do($platInfo = array()) {

        $content = file_get_contents("php://input");
        if ($content) {
            $_REQUEST = json_decode($content, true);
        }

        $postData = array();
        $postData['code'] = $_REQUEST['code'];
        $postData['msg'] = $_REQUEST['msg'];
        $postData['appId'] = $_REQUEST['appId'];
        $postData['tradeNo'] = $_REQUEST['tradeNo'];
        $postData['outTradeNo'] = $_REQUEST['outTradeNo'];
        $postData['amount'] = $_REQUEST['amount'];
        $postData['payMethod'] = $_REQUEST['payMethod'];
        $postData['realAmount'] = $_REQUEST['realAmount'];

        ksort($postData);
        $md5Str = '';
        foreach ($postData as $key => $value) {

            if (!$md5Str) {
                $md5Str .= "{$key}={$value}";
            } else {
                $md5Str .= "&{$key}={$value}";
            }
        }

        $sign = strtoupper(md5($md5Str . "&key={$platInfo['platInfo']['appKey']}"));
        if ($_REQUEST['sign'] == $sign && $postData['code'] == "10000") {
            echo 'success';
            return array('platOrderId' => $postData['tradeNo'],'realMoney' => $postData['realAmount']);
        } else {
            //签名验证失败
            return false;
        }

	}

}
