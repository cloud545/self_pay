<?php
class Mod_ThirdPlatform_Yida_saoma extends Mod_ThirdPlatform_Yida {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝wap
            $payType = 2001;
        } elseif ($type == 3) { // 微信wap
            $payType = 1001;
        } elseif ($type == 5) { // 银联
            $payType = 4001;
        } elseif ($type == 9) { // 支付宝扫码
            $payType = 2001;
        } elseif ($type == 10) { // 微信扫码
            $payType = 1001;
        }

        $postData = array();
        $postData['appId'] = $platInfo['platInfo']['appId'];
        $postData['payMethod'] = $payType;
        $postData['notifyUrl'] = $platInfo['appInfo']['callbackUrl'];
        $postData['returnUrl'] = $platInfo['appInfo']['clientUrl'];
        $postData['outTradeNo'] = $payInfo['pOrderId'];
        $postData['amount'] = number_format($payInfo['amount'],2,'.','');
        $postData['timestamp'] = time();

        ksort($postData);
        $md5Str = '';
        foreach ($postData as $key => $value) {

            if (!$md5Str) {
                $md5Str .= "{$key}={$value}";
            } else {
                $md5Str .= "&{$key}={$value}";
            }
        }

        $sign = md5($md5Str . "&key={$platInfo['platInfo']['appKey']}");
        $postData['sign'] = strtoupper($sign);

        BooCurl::setData(json_encode($postData), 'POST');
        BooCurl::setOption(CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
        ));
        $response = BooCurl::call($platInfo['platInfo']['p_pay_url']);

        $result = json_decode($response, true);
        if ($result['code'] != 10000) {
            BooFile::write('/tmp/yida.log', $response . "\n{$payInfo['amount']}\n{$platInfo['appInfo']['name']}\n111\n\n", 'a');
            echo "支付异常，请联系客服! code：{$result['code']}，msg: {$result['msg']}";
            exit;
        }

        header('Location: ' . $result['payUrl']);
        exit;
    }

}
