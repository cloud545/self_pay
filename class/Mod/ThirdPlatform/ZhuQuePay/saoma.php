<?php
class Mod_ThirdPlatform_ZhuQuePay_saoma extends Mod_ThirdPlatform_ZhuQuePay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }
        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();
        $payType = 0;

        if ($type == '2') {
            $payType = 'ALIH5';
        }

        if ($type == '9') {
            $payType = 'ALICODE';
        }

        if ($type == '3') {
            $payType = 'WXH5SMALL';
        }

        if ($type == '10') {
            $payType = 'WXCODESMALL';
        }

        $data = array(
            'mch_id' => $platInfo['platInfo']['appId'],
            'type' => $payType,
            'body' => '付款',
            'client_ip' =>  $_SERVER["REMOTE_ADDR"],
            'out_trade_no' => $payInfo['pOrderId'],
            'total_fee' => $payInfo['amount'],
            'notify_url' => $platInfo['appInfo']['callbackUrl'],
        );

        if ($type == 2 || $type == 3) {
            $data['card_type'] = '2';
            $data['back_url'] = 'https://www.baidu.com';
        }
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $md5str = substr($md5str, 0 , strlen($md5str)-1);

        $data['sign'] = md5($md5str.htmlspecialchars_decode($platInfo['platInfo']['appKey']));

        $data = json_encode($data);
        $ch = curl_init($platInfo['platInfo']['p_pay_url']);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data))
        );
        $result = json_decode(curl_exec($ch),1);
        var_dump($result);
        if ($result['error_code'] == '0') {
            if ($type == '2' || $type == '3') {
                header('location:'.$result['pay_url']);
            }

            if ($type == '9' || $type == '10') {
                header('location:'.$result['qr_code']);
            }
        } else {
            echo $result['error_msg'];
        }
    }
}
