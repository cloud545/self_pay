<?php
class Mod_ThirdPlatform_Longbao_saoma extends Mod_ThirdPlatform_Longbao {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝wap
            $payType = 8007;// $isMobile ? 8007 : 8006;
        } elseif ($type == 3) { // 微信wap
            $payType = 8003;
        } elseif ($type == 4) { // qq
            $payType = $isMobile ? 8010 : 8009;
        } elseif ($type == 5) { // 银联
            $payType = 8014;
        } elseif ($type == 6) { // 京东
            $payType = $isMobile ? 8012 : 8011;
        } elseif ($type == 9) { // 支付宝扫码
            $payType = 8006;
        } elseif ($type == 10) { // 微信扫码
            $payType = 8002;
        }

        $postData = array();
        $postData['mchId'] = $platInfo['platInfo']['appId'];
        $postData['appId'] = $platInfo['platInfo']['appKey'];
        $postData['productId'] = $payType;
        $postData['mchOrderNo'] = $payInfo['pOrderId'];
        $postData['currency'] = 'cny';
        $postData['amount'] = $payInfo['amount'] * 100;
        $postData['clientIp'] = BooUtil::realIp();
        $postData['returnUrl'] = $platInfo['appInfo']['clientUrl'];
        $postData['notifyUrl'] = $platInfo['appInfo']['callbackUrl'];
        $postData['subject'] = '商品';
        $postData['body'] = '商品';
        $postData['device'] = '';
        $postData['param2'] = '';
        $postData['param1'] = '';
        $postData['extra'] = '';

        ksort($postData);
        $md5Str = '';
        foreach ($postData as $key => $value) {

            if (!$value) {
                continue;
            }

            if (!$md5Str) {
                $md5Str .= "{$key}={$value}";
            } else {
                $md5Str .= "&{$key}={$value}";
            }
        }

        $sign = md5($md5Str . "&key={$platInfo['platInfo']['appSecret']}");
        $postData['sign'] = strtoupper($sign);

        BooCurl::setData('', 'GET');
        $response = BooCurl::call($platInfo['platInfo']['p_pay_url'] . "?params=" . urlencode(json_encode($postData)));

        $result = json_decode($response, true);
        if ($result['retCode'] != 'SUCCESS') {
            BooFile::write('/tmp/longbao.log', $response . "\n{$payInfo['amount']}\n{$platInfo['appInfo']['name']}\n111\n" . date('Y-m-d H:i:s') . "\n\n", 'a');
            echo "支付异常，请联系客服! retCode：{$result['retCode']}，retMsg: {$result['retMsg']}";
            exit;
        }

        BooView::set('payUrl', $result['payUrl']);
        BooView::display('pay/locationPay.html');
        //header('Location: ' . $result['payUrl']);
        exit;
    }

}
