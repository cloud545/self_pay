<?php
class Mod_ThirdPlatform_ShenZhiFu_saoma extends Mod_ThirdPlatform_ShenZhiFu {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝wap
            $payType = 'alipay';//$isMobile ? '904' : '903';
        } elseif ($type == 3) { // 微信wap
            $payType = 'wxpay';
        } elseif ($type == 5) { // 银联扫码
            $payType = '926';
        } elseif ($type == 9) { // 支付宝扫码
            $payType = 'alipay';
        } elseif ($type == 10) { // 微信扫码
            $payType = 'wxpay';
        }

        $data = [
            'tenantNo' => $platInfo['platInfo']['appId'],
            'tenantOrderNo' => $payInfo['pOrderId'],
            'notifyUrl'		=> $platInfo['appInfo']['callbackUrl'],
            'amount'		=> $payInfo['amount'],
            'payType'		=> $payType,
        ];

        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value. '&';
        }
        $data['sign'] = strtoupper(md5($md5str . 'key='.$platInfo['platInfo']['appKey']));
        $res = $this->http_post_data($platInfo['platInfo']['p_pay_url'], $data);
        $result = json_decode($res, true);
        if ($result['status'] != 200) {
            echo $res;
            exit;
        }

        BooView::set('payUrl', $result['url']);
        BooView::display('pay/locationPay.html');
        exit;
    }

}
