<?php
class Mod_ThirdPlatform_ShenZhiFu_payCallback extends Mod_ThirdPlatform_ShenZhiFu {

	protected function _do($platInfo = array()) {
        $data = $_POST;
        $sign = $data['sign'];
        unset($data['sign']);
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value. '&';
        }
        $signs = strtoupper(md5($md5str . 'key='.$platInfo['platInfo']['appKey']));

        if ($signs == $sign && $data['status'] == 200){
            echo "success";
            return array('platOrderId' => $data['outTradeNo'],'realMoney' => $data['amount']);
        } else {
            return false;
        }

	}

}
