<?php
class Mod_ThirdPlatform_Gembowl_payCallback extends Mod_ThirdPlatform_Gembowl {

	protected function _do($platInfo = array()) {

        $content = file_get_contents("php://input");
        if ($content) {
            $data = json_decode($content, true);
        }

        $checkSign = $data["sign"];
        unset($data["sign"]);
        ksort($data);
        $sign = md5(urldecode(http_build_query($data)).'&'.$platInfo['platInfo']['appKey']);

        if ($sign == $checkSign) {
            if ($data["status"] == "PAID" || $data["status"] == 'SUCCESS') {
                echo 'ok';
                return array('platOrderId' => $data["transaction_id"],'realMoney' => $data["amount"]);
            }
        }

        return false;
	}

}
