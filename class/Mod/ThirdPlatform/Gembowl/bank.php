<?php
class Mod_ThirdPlatform_Gembowl_bank extends Mod_ThirdPlatform_Gembowl {

	protected function _do($platInfo = array(), $payInfo = array()){

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $pay_memberid = $platInfo['platInfo']['appId'];   //商户后台API管理获取
        $pay_orderid = $payInfo['pOrderId'];    //订单号
        $pay_amount = number_format($payInfo['amount'],2,'.','');    //交易金额
        $pay_applydate = date("Y-m-d H:i:s");  //订单时间
        $pay_notifyurl = $platInfo['appInfo']['callbackUrl'];   //服务端返回地址
        $pay_callbackurl = $platInfo['appInfo']['clientUrl'];  //页面跳转返回地址
        $Md5key = $platInfo['platInfo']['appKey'];   //商户后台API管理获取
        $pay_bankcode = "907"; //支付宝扫码  //商户后台通道费率页 获取银行编码
        $native = array(
            "pay_memberid" => $pay_memberid,
            "pay_orderid" => $pay_orderid,
            "pay_amount" => $pay_amount,
            "pay_applydate" => $pay_applydate,
            "pay_bankcode" => $pay_bankcode,
            "pay_notifyurl" => $pay_notifyurl,
            "pay_callbackurl" => $pay_callbackurl,
        );
        ksort($native);
        $md5str = "";
        foreach ($native as $key => $val) {
            $md5str = $md5str . $key . "=" . $val . "&";
        }

        $sign = strtoupper(md5($md5str . "key=" . $Md5key));
        $native["pay_md5sign"] = $sign;
        $native['pay_attach'] = "";
        $native['pay_productname'] ='商品';

        BooView::set('payUrl', $platInfo['platInfo']['p_pay_url']);
        BooView::set('native', $native);
        BooView::display('pay/formPay.html');
        exit;

        $formstr = '';
        foreach ($native as $key => $val) {
            $formstr .= '<input type="hidden" name="' . $key . '" value="' . $val . '">';
        }

        $p_pay_url = $platInfo['platInfo']['p_pay_url'];

        echo '
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            </head>
            <body onLoad="document.zlinepay.submit();">
                <form name="zlinepay" method="post" action="' . $p_pay_url . '" target="_top"/>'.$formstr.'</form>
            </body>
        </html>
        ';

        exit;
	}


}
