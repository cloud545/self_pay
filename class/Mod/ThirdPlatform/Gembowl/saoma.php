<?php
class Mod_ThirdPlatform_Gembowl_saoma extends Mod_ThirdPlatform_Gembowl {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝
            $payType = 2;
        } elseif ($type == 3) { // 微信
            $payType = 4;
        } elseif ($type == 16) { // 银联扫码
            $payType = 11;
        } elseif ($type == 9) { // 支付宝扫码
            $payType = 1;
        } elseif ($type == 10) { // 微信扫码
            $payType = 3;
        }

        $key= $platInfo['platInfo']['appKey'];
        $data= array(
            "merchant_code" => $platInfo['platInfo']['appId'],
            "orderid"=> $payInfo['pOrderId'],
            "channel"=> $payType,
            "amount" => $payInfo['amount'],
            "timestamp" => time(),
            "reference" => '',
            "notifyurl" => $platInfo['appInfo']['callbackUrl'],
            "httpurl" =>  $platInfo['appInfo']['clientUrl'],
        );

        ksort($data);
        $data["sign"]=md5(urldecode(http_build_query($data)).'&'.$key);
        $request = json_encode($data);

        BooCurl::setData($request, 'POST');
        BooCurl::setOption(CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
        ));
        $data = BooCurl::call($platInfo['platInfo']['p_pay_url']);
        $res = json_decode($data, true);
        if (!$data || $res['status'] != true || !$res['data']['return']) {
            echo "支付异常，请联系客服! error: {$data}";
            exit;
        }

        BooView::set('payUrl', $res['data']['return']);
        BooView::display('pay/locationPay.html');
        exit;
    }

}
