<?php
class Mod_ThirdPlatform_HangKongMa_payCallback extends Mod_ThirdPlatform_HangKongMa {

    protected function _do($platInfo = array()) {

        $data = $_POST;

        $sign = $_POST['sign'];
        unset($data['sign']);
        ksort($data);

        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str. $key . '=' . $value . '&';
        }
        $md5str = substr($md5str, '0', strlen($md5str) - 1);
        $signs = md5($md5str . $platInfo['platInfo']['appKey']);

        if ($sign == $signs && $data['orderStatus'] == 'paid') {
            echo 'success';
            return array('platOrderId' => $data['mercOrderId'],'realMoney' => $data['txnAmt']);
        }
        return false;
    }
}

