<?php
class Mod_ThirdPlatform_JanePay_payCallback extends Mod_ThirdPlatform_JanePay {

	protected function _do($platInfo = array()) {

        $data = json_decode(file_get_contents('php://input'), true);
        $sign = $data['signValue'];
        unset($data['signValue']);
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $md5str = substr($md5str, 0 , strlen($md5str)-1);
        $signs = md5($md5str.$platInfo['platInfo']['appKey']);

        if ($sign == $signs) {
            echo 'SUCCESS';
            return array('platOrderId' => $data['outTradeNo'],'realMoney' => $data['amount']);
        }
        return false;
	}

}
