<?php
class Mod_ThirdPlatform_JanePay_saoma extends Mod_ThirdPlatform_JanePay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;

        if ($type == 7 || $type == 9) {
           $payType = 'aqc';
        }

        if($type == 2 ) {
            $payType = 'ah5';
        }


        if($type == 3 || $type == 6) {
            $payType = 'wh5';
        }


        $data = array(
            'merchantNo' => $platInfo['platInfo']['appId'],
            'outTradeNo' => $payInfo['pOrderId'],
            'amount' => $payInfo['amount'],
            'payment' => $payType,
            'fontUrl' => 'https://www.baidu.com',
            'notifyUrl' => $platInfo['appInfo']['callbackUrl'],
            'member' => '1',
            'sence' => 'wap',
            'signType' => 'md5',
        );

        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $md5str = substr($md5str, 0 , strlen($md5str)-1);
        $data['sign'] = md5($md5str.$platInfo['platInfo']['appKey']);

        $data['clientIp'] =  $_SERVER['REMOTE_ADDR'];
        $data = http_build_query(
            $data
        );
        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => $data
            )
        );
        $context = stream_context_create($opts);
        $result = file_get_contents($platInfo['platInfo']['p_pay_url'], false, $context);
        $result = json_decode($result,1);
        if ($result['code'] == 1000) {
            header('location:'.$result['data']['result']);
        } else {
            echo $result['message'];
        }
    }
}
