<?php
class Mod_ThirdPlatform_XuanWuPay_payCallback extends Mod_ThirdPlatform_XuanWuPay {

	protected function _do($platInfo = array()) {
        $data = json_decode(file_get_contents('php://input'), true);

        $sign = $data['sign'];
        unset($data['sign']);
        unset($data['error_code']);
        unset($data['error_msg']);
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $md5str = substr($md5str, 0 , strlen($md5str)-1);
        $signs = md5($md5str.htmlspecialchars_decode($platInfo['platInfo']['appKey']));

        if ($sign == $signs && $data['pay_status'] == '1') {
            echo 'SUCCESS';
            return array('platOrderId' => $data['out_trade_no'],'realMoney' => $data['total_fee']);
        } else {
            echo 'fail';
        }
	}
}
