<?php
class Mod_ThirdPlatform_JiZhiPay extends Mod_ThirdPlatform {
    // 私钥;
    public $private_key = '-----BEGIN RSA PRIVATE KEY-----
MIICXQIBAAKBgQCh7HfKnmdimJKt0uWKEc1l99znBAgzjXikamu9Y4ER3qoLdgPV
AtHOgWEPiw1d2pfekTIpfAkdZwzHW0WdPz7J/ig/KNYhGrxMX2xK27QaWXzhhkWj
LmVFpRp+slOKD33PNcuWo9a+eAklLNGO4UAP72Af9SHDQPa+lDo268eR3QIDAQAB
AoGAER2tVfIjjIUDMDOURuF13Dr0k0jWUxi3sW18YQMVIB3wzJBXfFs1cUY0y89P
hOW32vNvIXp2fjCn5aTMuX/3c3d8+BG0f/j0xNIB+frylQbN2mkkLCZg97o/AUvN
a671GLHCR/0xx3AbQDiBS6nT0mXN34xANqM5raGJWByTzpECQQD953FC8E7ZuT84
tmgcD6c248FTr2O0nY3tDFec13Rd43KhFJf4Q+v5omrR9TRJqVlbR9XtNaUc/PAp
9yGgy3/FAkEAo0KmXn6/46zdMWXTMZaXx4dvbXOqpBHheCJrpBnvW7rIh4XpFwID
L2LkH0WE6yk/I1G4r4Sv6JQrT1eDa1KTOQJBAJq0drKOsby/vuHTYsQWVZCAXb+b
0miux1d0eb27GdBLUgEMBzziRdopOGUNvvpTEXso3hRW53AuPjeTuQ28cPECQGHT
gClDPexJGsmMwD4IhK40w1oMYwWpCTeZOHK+b6TBiYNbhE4bEJmGYjcyyvC2aHTb
dRUHJvjwP/ts7vySIFECQQCucVHEcwzp+OLaW0WLY5RgYklSFraL6fsneXfI2JJy
91um3yuK+jcr1ZfKum5BHpbCspLL53/shSRdq4wT6/qM
-----END RSA PRIVATE KEY-----';


    //上游公钥
    public $public_key = '-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApawwfAOl/i9T9fNpphPI
21QIoZHiCkOnk1AwrSu3rCMdXyT/B7UOAxJGzuSZnQyJ3PDc6rV0gX5PvNBjI9Os
ZlJ8l/b1yIqwNs0FPZk10kIjUaPdl/pKBiJcEmcgThJtPpOh7grzpmIEqf083Frb
iSPri1l9JI7i6++hojEz7Meb2UinHuSrPyW+aHbTwBpsu/sE1ez1i34nLF1zo7uR
uwdoTxFkcOBHJa73E89rmF3YH7ls99Ukp9ANxvqZeM0TQjkG5yqQ5+w9YRWezc/3
2jdJEINs4FbIOxvetau8RwqphJO4WlolGvmpZrRAcCpeWD11k98+/qZodMEo6iAF
DwIDAQAB
-----END PUBLIC KEY-----';

    /**
     * 生成签名
     *
     * @param $data
     * @return string
     */
    function sign($data)
    {
        $str = '';
        foreach ($data as $key => $value) {
            $str = $str . $key . '=' . $value . '&' ;
        }
        $signature = '';
        openssl_sign($str, $signature, $this->private_key,'SHA256');
        return base64_encode($signature);
    }

    /**
     * 验证签名
     *
     * @param $signStr
     * @param $sign
     * @return int
     */
    function verifySign($signStr,$sign)
    {
        return openssl_verify($signStr,base64_decode($sign),$this->public_key,'SHA256');
    }

    function doPost($url,$postData,$key,$headers=array())
    {
        //初始化
        $curl = curl_init();
        //设置抓取的url
        curl_setopt($curl, CURLOPT_URL, $url);
        //设置头文件的信息作为数据流输出
        curl_setopt($curl, CURLOPT_HEADER, 0);
        //设置获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS,$postData);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'app-token:'.$key,
                'Content-Type: application/json',
                'Content-Length: ' . strlen($postData))
        );

        //执行命令
        $data = curl_exec($curl);
        //关闭URL请求
        curl_close($curl);
        return json_decode($data, 1);
    }
}
