<?php
class Mod_ThirdPlatform_JiZhiPay_saoma extends Mod_ThirdPlatform_JiZhiPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 9) {
            $payType = 'AliPayCashier';
        }

        if ($type == 10) {
            $payType = 'WeChatPayCashier';
        }

        $data = array(
            'merchant_id' => $platInfo['platInfo']['appId'],
            'order_no' => $payInfo['pOrderId'],
            'amount' => $payInfo['amount'],
            'user_id' => '4225',
            'user_name' => '逍遥',
            'goods_name' => '可口可乐',
        );

        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $data['sign']= $this->sign($data);
        $data['pay_type'] = $payType;
        $data['notify_url'] = $platInfo['appInfo']['callbackUrl'];
        $result = $this->doPost($platInfo['platInfo']['p_pay_url'], json_encode($data), $platInfo['platInfo']['appKey']);
        if ($result['success'] == 1) {
            header('location:'.$result['data']['pay_url']);
        } else {
            echo $result['status_msg'];
        }
    }
}
