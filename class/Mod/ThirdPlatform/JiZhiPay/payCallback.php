<?php
class Mod_ThirdPlatform_JiZhiPay_payCallback extends Mod_ThirdPlatform_JiZhiPay {

	protected function _do($platInfo = array()) {

        $data = json_decode(file_get_contents("php://input"),1);

        $md5str = 'merchant_id='.$data['merchant_id'].'&'.'serial_no='.$data['serial_no'].'&'.'order_no='.$data['order_no'].'&'.'amount='.$data['amount'].'&'.'status='.$data['status'].'&'.'create_time='.$data['create_time'].'&'.'pay_completed_time='.$data['pay_completed_time'].'&'.'actual_amount='.$data['actual_amount'].'&';

        if ($this->verifySign($md5str, $data['sign']) && $data['status'] == '3') {
            echo 'SUCCESS';
            return array('platOrderId' => $data['order_no'],'realMoney' => $data['actual_amount']);
        }
        return false;
	}

}
