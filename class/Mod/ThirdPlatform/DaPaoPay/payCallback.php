<?php
class Mod_ThirdPlatform_DaPaoPay_payCallback extends Mod_ThirdPlatform_DaPaoPay {

	protected function _do($platInfo = array()) {
        $data = $_POST;

        $sign = $data['sign'];
        unset($data['sign']);
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            if ($value) {
                $md5str = $md5str . $key .'='.  $value . '&';
            }
        }
        $signs = strtoupper(md5($md5str. 'key=' .$platInfo['platInfo']['appKey']));

        if ($sign == $signs && $data['status'] == '2') {
            echo 'success';
            return array('platOrderId' => $data['mchOrderNo'],'realMoney' => $data['amount'] / 100);
        }
        return false;
	}
}
