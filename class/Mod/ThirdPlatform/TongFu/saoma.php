<?php
class Mod_ThirdPlatform_TongFu_saoma extends Mod_ThirdPlatform_TongFu {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 10) {
            $payType = 'WECHATTOPHONE';
        }

        $data = array(
            'merchant_id' => $platInfo['platInfo']['appId'],
            'order_no' => $payInfo['pOrderId'],
            'amount' => number_format($payInfo['amount'], '2', '.'),
            'type' => $payType,
            'user_identifier' => time(),
            'callback_url' => $platInfo['appInfo']['callbackUrl'],
            'time' => date('YmdHis'),
        );

        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $data['sign'] = strtoupper(md5($md5str.'key='.$platInfo['platInfo']['appKey']));
        $str='<form class="form-inline" name="payform" method="post" action="'.$platInfo['platInfo']['p_pay_url'].'">';
        foreach ($data as $key => $val) {
            $str.='<input type="hidden" name="' . $key . '" value="' . $val . '">';
        }
        $str.="</form><script>document.forms['payform'].submit();</script>";
        exit($str);
    }
}
