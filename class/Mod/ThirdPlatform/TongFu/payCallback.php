<?php
class Mod_ThirdPlatform_TongFu_payCallback extends Mod_ThirdPlatform_TongFu {

	protected function _do($platInfo = array()) {

        $data = $_POST;
        $sign = $data['sign'];
        unset($data['sign']);
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $signs = strtoupper(md5($md5str. 'key=' .$platInfo['platInfo']['appKey']));

        if ($sign == $signs || $data['status'] == '1') {
            echo 'success';
            return array('platOrderId' => $data['order_no'],'realMoney' => $data['amount']);
        }
        return false;
	}

}
