<?php
class Mod_ThirdPlatform_ZnpPay_payCallback extends Mod_ThirdPlatform_ZnpPay {

	protected function _do($platInfo = array()) {

        if($_SERVER['REQUEST_METHOD'] == "POST"){

            $json = file_get_contents("php://input");
            $json_arr = json_decode(stripslashes($json),true);

            $paysapi_id = $json_arr["paysapi_id"]; 	//服务器API接口返回的唯一支付编码ID
            $order_id = $json_arr["order_id"];			//用户订单编号ID
            $is_type = $json_arr["is_type"];				//支付类型
            $price = $json_arr["price"];				//订单金额
            $real_price = $json_arr["real_price"];		//实际支付金额
            $mark = $json_arr["mark"];					//此处填写产品名称，或充值，消费说明
            $code = $json_arr["code"];					//订单状态
            $sign = $json_arr["sign"];					//服务器API接口返回的唯一key

            //请检查传入的参数是否格式正确
            $api_code = $platInfo['platInfo']['appId'];								//此处填写商户的id;
            $api_key = $platInfo['platInfo']['appKey']; 	//此处填写的密钥

            $signdata = array(
                'api_code' => $api_code,			//商户的id;
                'paysapi_id' => $paysapi_id,		//服务器API接口返回的唯一支付编码ID
                'order_id' => $order_id,			//用户订单编号ID
                'is_type' => $is_type,				//支付类型
                'price' => $price,					//订单金额
                'real_price' => $real_price,		//实际支付金额
                'mark' => $mark,					//此处填写产品名称，或充值，消费说明
                'code' => $code						//订单状态
            );

            $temp_sign = $this->make_sign($signdata,$api_key);

            if ($temp_sign == $sign && $code == 1){
                echo "SUCCESS";
                return array('platOrderId' => $paysapi_id,'realMoney' => $real_price);
            } else {
                return false;
            }

        }

        return false;
	}

}
