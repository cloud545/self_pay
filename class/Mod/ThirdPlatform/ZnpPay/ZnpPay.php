<?php
class Mod_ThirdPlatform_ZnpPay extends Mod_ThirdPlatform {

    protected function make_sign($signdata,$api_key)
    {
        //签名步骤一：按字典序排序参数
        ksort($signdata);
        $string = $this->to_params($signdata);
        //签名步骤二：在string后加入KEY
        $string = $string . "&key=".$api_key;
        //$file = "make_sign1";
        //$path ="/data/wwwroot/";
        //$content = $string;
        //F($file,$content,$path);
        //签名步骤三：MD5加密
        $string = md5($string);
        //签名步骤四：所有字符转为大写
        $result = strtoupper($string);
        return $result;
    }

    protected function to_params($signdata)
    {
        $buff = "";
        foreach ($signdata as $k => $v)
        {
            if($k != "sign" && $v != "" && !is_array($v)){
                $buff .= $k . "=" . $v . "&";
            }
        }

        $buff = trim($buff, "&");
        return $buff;
    }

    protected function order_sn()
    {
        $ycode = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J');
        $ordersn = $ycode[intval(substr(date('Y'),-1))-1] . strtoupper(dechex(date('m'))) . date('d') . substr(time(), -5) . substr(microtime(), 2, 5) . sprintf('%02d', rand(0, 99));
        return $ordersn;
    }

    protected function json_return($message = '',$data = '',$code = 0)
    {
        $return['msg']  = $message;
        $return['data'] = $data;
        $return['code'] = $code;
        return json_encode($return);
    }

    protected function F($name,$data='',$dir = ''){
        $exc = '.log';

        if (!file_exists($dir)){
            mkdir ($dir,0777,true);
        }
        if(!is_writable($dir.$name.$exc)){
            fopen($dir.$name.$exc, "wd");
        }
        if($data){
            $data = json_encode($data);
            file_put_contents($dir.$name.$exc,date("Y-m-d H:i:s").' '.$data."\r\n", FILE_APPEND);
        }else{
            $data = file_get_contents($dir.$name.$exc);
            $data = str_replace("\r\n","|",$data);
            $data = explode('|',$data);
            $array =  array();
            foreach ($data as $v){
                $array[] = json_decode($v,true);
            }
            return array_filter($array);
        }
    }

    protected function curlPost($url, $param)
    {
        if (empty($url) || empty($param)) {
            return false;
        }

        $ch=curl_init();
        $timeout=5;
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_HEADER, 0);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,2);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($ch,CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
        $data=curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    protected function curlPostJson($url, $param_string){
        $param_string = json_encode($param_string);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $param_string);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Content-Type: application/json; charset=utf-8",
                "Content-Length: " . strlen($param_string))
        );
        ob_start();
        curl_exec($ch);
        $return_content = ob_get_contents();
        ob_end_clean();
        $return_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        return array($return_code, $return_content);
    }

}
