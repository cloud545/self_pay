<?php
class Mod_ThirdPlatform_RHPay_payCallback extends Mod_ThirdPlatform_RHPay {

	protected function _do($platInfo = array()) {

        $returnArray = array(
            "merchant_no" => $_REQUEST["merchant_no"],
            "order_num" =>  $_REQUEST["order_num"],
            "pay_num" =>  $_REQUEST["pay_num"],
            "pay_amount" =>  $_REQUEST["pay_amount"],
            "pay_date" =>  $_REQUEST["pay_date"],
            "pay_result" => $_REQUEST["pay_result"],
        );
        $md5key = $platInfo['platInfo']['appKey'];

        $md5str = "{$returnArray['merchant_no']}{$returnArray['pay_num']}{$returnArray['pay_amount']}" . date('Ymd');
        $sign = strtoupper(md5($md5str . $platInfo['platInfo']['appKey']));
        if ($sign == $_REQUEST["sign"]) {
            if ($_REQUEST["pay_result"] == 1) {
                echo 'success';
                return array('platOrderId' => $_REQUEST["order_num"],'realMoney' => $_REQUEST["pay_amount"]);
            }
        }

        return false;
	}

}
