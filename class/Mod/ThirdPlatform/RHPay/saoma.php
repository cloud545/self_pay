<?php
class Mod_ThirdPlatform_RHPay_saoma extends Mod_ThirdPlatform_RHPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝
            $payType = 0;
        } elseif ($type == 3) { // 微信
            $payType = '902';
        } elseif ($type == 5) { // 银联扫码
            $payType = '926';
        } elseif ($type == 9) { // 支付宝扫码
            $payType = 0;
        } elseif ($type == 10) { // 微信扫码
            $payType = '902';
        }

        $native = array(
            'merchant_no'=>$platInfo['platInfo']['appId'],
            'pay_num'=>$payInfo['pOrderId'],
            'total_fee'=>$payInfo['amount'],
            'pay_method'=>$payType,
            'merchant_userid' => "1",
            'notify_url'=>$platInfo['appInfo']['callbackUrl'],
            'return_type'=>0,
        );

        $md5str = "{$native['merchant_no']}{$native['pay_num']}{$native['total_fee']}" . date('Ymd');
        $sign = strtoupper(md5($md5str . $platInfo['platInfo']['appKey']));
        $native["sign"] = $sign;

        BooView::set('payUrl', $platInfo['platInfo']['p_pay_url']);
        BooView::set('native', $native);
        BooView::display('pay/formPay.html');
        exit;
    }

}
