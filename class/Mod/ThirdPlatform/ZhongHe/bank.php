<?php
class Mod_ThirdPlatform_ZhongHe_bank extends Mod_ThirdPlatform_ZhongHe {

	protected function _do($platInfo = array(), $payInfo = array()){

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $post = BooVar::requestx();
        $postData = array();
        $postData['appId'] = $platInfo['platInfo']['appId'];
        $postData['orderId'] = $post['orderId'];
        $postData['amount'] = $post['amount'];
        $postData['payType'] = $platInfo['platInfo']['pt_id'];
        $postData['remark'] = '';
        $postData['clientUrl'] = $platInfo['appInfo']['client_url'];
        $postData['notifyUrl'] = $platInfo['appInfo']['callbackUrl'];
        $postData['time'] = date('Y-m-d H:i:s');

        ksort($postData);

        $queryString = '';
        foreach ($postData as $key =>$value) {
            if (!$queryString) {
                $queryString = "{$key}={$value}";
            } else {
                $queryString .= "&{$key}={$value}";
            }
        }

        $sign = md5("{$queryString}{$platInfo['platInfo']['appKey']}");

        header("Location: {$platInfo['platInfo']['p_pay_url']}?{$queryString}&sign={$sign}");
        exit;
	}


}
