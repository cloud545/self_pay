<?php
class Mod_ThirdPlatform_ZhongHe_payCallback extends Mod_ThirdPlatform_ZhongHe {

	protected function _do($platInfo = array()) {

        if (isset($_REQUEST['target'])) {
            unset($_REQUEST['target']);
        }
        if (isset($_REQUEST['appId'])) {
            unset($_REQUEST['appId']);
        }
        if (isset($_REQUEST['appPayInfo'])) {
            unset($_REQUEST['appPayInfo']);
        }
        if (isset($_REQUEST['platId'])) {
            unset($_REQUEST['platId']);
        }

        $post = $_REQUEST;
        $sign = $post["sign"];
        unset($post["sign"]);
        ksort($post);
        $md5Str = '';
        foreach ($post as $key => $value) {

            if (!$md5Str) {
                $md5Str .= "{$key}={$value}";
            } else {
                $md5Str .= "&{$key}={$value}";
            }
        }

        $checkSign = md5($md5Str . $platInfo['platInfo']['appKey']);
        if ($sign == $checkSign) {
            echo 'success';
            return array('platOrderId' => $post["platOrderId"],'realMoney' => $post['realAmount']);
        }else {
            //签名验证失败
            return false;
        }

	}

}
