<?php
class Mod_ThirdPlatform_ZhongHe_payOrder extends Mod_ThirdPlatform_ZhongHe {

	protected function _do($platInfo = array()){

        if (!$platInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $post = BooVar::requestx();

        $Md5key = $platInfo['platInfo']['appKey'];

        $requestarray = array(
            "MERCHANT_ID" => $platInfo['platInfo']['appId'],
            "TRAN_CODE" => $post['orderId'],
            "TRAN_AMT" => $post['amount'] * 100,
            "REMARK" => "REMARK",
            "SUBMIT_TIME" => date("YmdHis"),
            "BANK_ID" => $post['bankCode'],
            "VERSION" => "1",
            "NO_URL" => $platInfo['appInfo']['callbackUrl'],
            "RET_URL" => $platInfo['appInfo']['client_url'],
        );

        ksort($requestarray);
        reset($requestarray);
        $md5str = "";
        foreach ($requestarray as $key => $val) {
            $md5str = $md5str . $key . "=" . $val . "&";
        }
        //echo($md5str . "key=" . $Md5key."<br>");
        $md5str = rtrim($md5str,"&");
        $sign = md5($md5str . $Md5key);

        $paramSrc = http_build_query($requestarray);

        $p_pay_url = $platInfo['platInfo']['p_pay_url']."?".$paramSrc.'&SIGNED_MSG='.$sign;
        header('Location: '.$p_pay_url);
        exit;
	}

}
