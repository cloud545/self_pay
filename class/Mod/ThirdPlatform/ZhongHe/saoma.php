<?php
class Mod_ThirdPlatform_ZhongHe_saoma extends Mod_ThirdPlatform_ZhongHe {


    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $payType = 2;
        if (in_array($platInfo['platInfo']['pt_id'], array(2, 9))) {
            $payType = 2;
        } else {
            $payType = $platInfo['platInfo']['pt_id'];
        }

        $post = BooVar::requestx();
        $postData = array();
        $postData['appId'] = $platInfo['platInfo']['appId'];
        $postData['orderId'] = $post['pOrderId'];
        $postData['amount'] = $post['amount'];
        $postData['payType'] = $payType;
        $postData['remark'] = '';
        $postData['clientUrl'] = $platInfo['appInfo']['client_url'];
        $postData['notifyUrl'] = $platInfo['appInfo']['callbackUrl'];
        $postData['time'] = date('Y-m-d H:i:s');

        ksort($postData);

        $queryString = '';
        foreach ($postData as $key =>$value) {
            if (!$queryString) {
                $queryString = "{$key}={$value}";
            } else {
                $queryString .= "&{$key}={$value}";
            }
        }

        $sign = md5("{$queryString}{$platInfo['platInfo']['appKey']}");

        BooView::set('payUrl', "{$platInfo['platInfo']['p_pay_url']}?{$queryString}&sign={$sign}");
        BooView::display('pay/locationPay.html');
        exit;
    }

}
