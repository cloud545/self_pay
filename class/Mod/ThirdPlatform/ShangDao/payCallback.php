<?php
class Mod_ThirdPlatform_ShangDao_payCallback extends Mod_ThirdPlatform_ShangDao {

	protected function _do($platInfo = array()) {
        $data = $_POST;

        $sign = $data['sign'];
        unset($data['sign']);
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $signs = md5($md5str.'key='.$platInfo['platInfo']['appKey']);
        if ($sign == $signs && $data['status'] == 'success') {
            echo 'success';
            return array('platOrderId' => $data['sh_order'],'realMoney' => $data['money']);
        }
        return false;
	}
}
