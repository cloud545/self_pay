<?php
class Mod_ThirdPlatform_ShangDao_saoma extends Mod_ThirdPlatform_ShangDao {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();
        $payType = 0;

        if ($type == 9) {
            $payType = '1';
        }
        $data = array(
            'mch_id' => $platInfo['platInfo']['appId'],
            'ptype' => $payType,
            'order_sn' => $payInfo['pOrderId'],
            'money' => number_format($payInfo['amount'],'2','.',''),
            'format' => 'page',
            'goods_desc' => 'buy',
            'client_ip' => $platInfo['appInfo']['last_ip'],
            'notify_url' => $platInfo['appInfo']['callbackUrl'],
            'return_url' => 'https://www.baidu.com',
            'time' => time(),
        );
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $data['sign'] = md5($md5str.'key='.$platInfo['platInfo']['appKey']);
        $str='<form class="form-inline" name="payform" method="post" action="'.$platInfo['platInfo']['p_pay_url'].'">';
        foreach ($data as $key => $val) {
            $str.='<input type="hidden" name="' . $key . '" value="' . $val . '">';
        }
        $str.="</form><script>document.forms['payform'].submit();</script>";
        exit($str);
    }
}
