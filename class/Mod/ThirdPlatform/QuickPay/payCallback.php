<?php
class Mod_ThirdPlatform_QuickPay_payCallback extends Mod_ThirdPlatform_QuickPay {

    protected function _do($platInfo = array()) {
        $returnArray = array(   // 返回字段
            "ORDER_ID"      =>  $_REQUEST["ORDER_ID"],
            "ORDER_AMT"     =>  $_REQUEST["ORDER_AMT"],
            "ORDER_TIME"    =>  $_REQUEST["ORDER_TIME"],
            "USER_ID"       =>  $_REQUEST["USER_ID"],
            "BUS_CODE"      =>  $_REQUEST["BUS_CODE"],
            "RESP_CODE"     =>  $_REQUEST["RESP_CODE"],
            "RESP_DESC"     =>  $_REQUEST["RESP_DESC"],
        );

        $md5key = $platInfo['platInfo']['appKey'];
        $md5str = "";
        foreach ($returnArray as $key => $val) {
            $md5str = $md5str.$val;
        }

        $str    = strtoupper(md5($md5str)).$md5key;
        $str    = strtoupper(md5($str));
        $sign   = substr($str, 8, 16);
        if ($sign == $_REQUEST["SIGN"]) {
            if ($_REQUEST["RESP_DESC"] == "success") {
                echo 'SUCCESS';
                return array('platOrderId' => $_REQUEST["ORDER_ID"] , 'realMoney' => $_REQUEST["ORDER_AMT"]);
            }
        }
        return false;
    }

}
