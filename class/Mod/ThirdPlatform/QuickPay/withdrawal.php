<?php
class Mod_ThirdPlatform_QuickPay_withdrawal extends Mod_ThirdPlatform_QuickPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        $post = BooVar::requestx();
        $appKey = json_decode($platInfo['platInfo']['appKey'], true);
        $appSecret = json_decode($platInfo['platInfo']['appSecret'], true);

        $param = array();
        $param['ORDER_ID']           = $payInfo['pOrderId'];                        //订单号
        $param['ORDER_AMT']          = number_format($post['amount'],2,'.','');     //订单金额
        $param['ORDER_TIME']         = date('YmdHis',time());                      //订单提交时间
        $param['USER_TYPE']          = '02';                                       //用户类型
        $param['USER_ID']            = $platInfo['platInfo']['appId'];            //用户编号
        $param['SIGN_TYPE']          = '03';                                       //签名类型
        $param['BUS_CODE']           = '1006';                                      //业务代码
        $param['BANK_CODE']          = "?????????";                                 //银行代码
        $param['ACCOUNT_NO']         = $post['cardNo'];                             //账号
        $param['BANK_NAME']          = $platInfo['platInfo']['appId'];              //银行名称
        $param['ACCOUNT_NAME']       = $platInfo['platInfo']['appId'];              //账号名
        			//请求流水号
        $param['payment_channel_list']=array();

        $m2 = new m2base ($appSecret['aid'], $appSecret['key']);
        $data = $m2->url_data ( 'PAYING', $param, "POST" );

        $return = json_decode($data,true);


        if (is_array($return)) {
            if ($return['op_ret_code'] != '000') {
                $status = 2;
            } else {
                $status = 0;
            }
            return array('platOrderId' => $return['orderid'], 'status' => $status, 'msg' => "op_ret_code: {$return['op_ret_code']}，op_err_msg: {$return['op_err_msg']}");
        } else {
            return array('platOrderId' => 0, 'status' => 2, 'msg' => "op_ret_code: {$return['op_ret_code']}，op_err_msg: {$return['op_err_msg']}");
        }

    }

    function bankinfo($bankName){

         $bankInfo  =[
            '01000000' => '邮储银行',
            '01020000' => '工商银行',
            '01030000' => '农业银行',
            '01040000' => '中国银行',
            '01050000' => '建设银行',
            '03010000' => '交通银行',
            '03020000' => '中信银行',
            '03030000' => '光大银行',
            '03040000' => '华夏银行',
            '03050000' => '民生银行',
            '03060000' => '广发银行股份有限公司',
            '03070000' => '平安银行',
            '03080000' => '招商银行',
            '03090000' => '兴业银行',
            '03100000' => '浦东发展银行',
            '04010000' => '上海银行',
            '04030000' => '北京银行'
        ];
        foreach ($bankInfo as $item => $value) {
             if($bankName == $item['']){

             }
        }
    }

}
