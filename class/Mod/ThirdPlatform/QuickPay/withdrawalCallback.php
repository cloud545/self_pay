<?php
class Mod_ThirdPlatform_QuickPay_withdrawalCallback extends Mod_ThirdPlatform_QuickPay {

    protected function _do($platInfo = array()) {

        $appSecret = json_decode($platInfo['platInfo']['appSecret'], true);

        if (isset($_REQUEST['target'])) {
            unset($_REQUEST['target']);
        }

        if (isset($_REQUEST['appWithdrawalInfo'])) {
            unset($_REQUEST['appWithdrawalInfo']);
        }

        $return_data = $_REQUEST;

        if(verify_sign($appSecret['bonuse_key'], $return_data, $return_data['dstbdatasign'])){
            echo "00";//交易成功，在此处写后续的订单操作方法。
            $orderList = array();
            $orderList[] = array(
                'platOrderId' => $return_data["orderid"],
                'amount' => $return_data['amount'],
                'fee' => 0,
                'orderId' => $return_data['dsorderid'],
            );
            return $orderList;
        } else {
            //交易失败，在此处写后续的订单操作方法。
            echo "99";
            return array();
        }

	}

}
