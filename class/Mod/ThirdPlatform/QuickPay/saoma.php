<?php
class Mod_ThirdPlatform_QuickPay_saoma extends Mod_ThirdPlatform_QuickPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 5) { // 银联扫码
            $payType = '3303';
        }

        $pay_memberid   = $platInfo['platInfo']['appId'];
        $pay_orderid    = $payInfo['pOrderId'];
        $pay_amount     = number_format($payInfo['amount'],2,'.','');
        $pay_applydate  = date("YmdHis");
        $pay_notifyurl  = $platInfo['appInfo']['callbackUrl'];
        $pay_callbackurl= $platInfo['appInfo']['clientUrl'];
        $Md5key         = $platInfo['platInfo']['appKey'];
        $pay_bankcode   = "{$payType}";

        $native = array(
            "ORDER_ID"      => $pay_orderid,
            "ORDER_AMT"     => $pay_amount,
            "ORDER_TIME"    => $pay_applydate,
            "USER_TYPE"     => '02',
            "USER_ID"       => $pay_memberid,
            "SIGN_TYPE"     => '03',
            "BUS_CODE"      => $pay_bankcode,
            "CCT"           => "CNY",
        );

        $md5str = "";
        foreach ($native as $key => $val) {
            $md5str = $md5str . $key . "=" . $val . "&";
        }

        $md5str = $md5str . $Md5key;
        $md5str = strtoupper(md5($md5str));
        $md5str = $md5str.$Md5key;
        $md5str = strtoupper(md5($md5str));
        $native['SIGN']     = substr($md5str, 8, 16);
        $native['BG_URL']   = $pay_notifyurl;
        $native['PAGE_URL'] = $pay_callbackurl;
        $native['GOODS_NAME'] = '支付';
        $native['GOODS_DESC'] = '支付';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $platInfo['platInfo']['p_pay_url']);            //设置访问的url地址
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);                  //设置超时
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);              //跟踪301
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);            //返回结果
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_POST, 1);                      //设置为POST方式
        curl_setopt($ch, CURLOPT_POSTFIELDS, $native);          //设置参数
        $r = curl_exec($ch);
        curl_close($ch);
        $obj = simplexml_load_string($r, 'SimpleXMLElement', LIBXML_NOCDATA);
        $json = json_encode($obj);
        $arr = json_decode($json, true);
        BooView::set('url', $arr['CODE_URL']);
        BooView::display('pay/saoma.html');
        exit;
    }
}
