<?php
class Mod_ThirdPlatform_GiojfxogbPay_payCallback extends Mod_ThirdPlatform_GiojfxogbPay {

	protected function _do($platInfo = array()) {

        $order_id = $_POST["order_id"];//1平台订单号
        $mid = $_POST["mid"];//商户号
        $oid = $_POST["oid"];//商户订单号
        $amt = $_POST["amt"];//交易金额
        $tamt = $_POST["tamt"];//实际支付金额
        $way = $_POST["way"]; //1微信扫码
        $code = $_POST["code"];//状态码
        $sign = $_POST["sign"];//签名

        $checkSign = md5($order_id.$mid.$oid.$amt.$tamt.$way.$code.$platInfo['platInfo']['appKey']);
        if($checkSign == $sign && $code == "1"){
            //必须返回ok
            echo "ok";
            return array('platOrderId' => $order_id,'realMoney' => $tamt);
        } else {
            return false;
        }

	}

}
