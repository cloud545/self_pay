<?php
class Mod_ThirdPlatform_GiojfxogbPay_saoma extends Mod_ThirdPlatform_GiojfxogbPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝wap
            $payType = 1;
        } elseif ($type == 3) { // 微信wap
            $payType = 3;
        } elseif ($type == 5) { // 银联扫码
            $payType = 0;
        } elseif ($type == 9) { // 支付宝扫码
            $payType = 2;
        } elseif ($type == 10) { // 微信扫码
            $payType = 1;
        }

        $returndata = array(
            'mid' => $platInfo['platInfo']['appId'],
            'oid' => $payInfo['pOrderId'],
            'amt' => number_format($payInfo['amount'],2,'.',''),
            'way' => $payType,
            'body' => '商品',
            'back' => $platInfo['appInfo']['clientUrl'],
            'notify' => $platInfo['appInfo']['callbackUrl'],
        );
        $returndata['sign'] = md5($returndata['mid'].$returndata['oid'].$returndata['amt'].$returndata['way'].$returndata['body'].$returndata['back'].$returndata['notify'].$platInfo['platInfo']['appKey']);
        $res = $this->buildRequestForm($platInfo['platInfo']['p_pay_url'],$returndata);
		echo $res;
		exit;


    }
	
	
	//表单提交
	public function buildRequestForm($payUrl, $para_temp) {
        $sHtml = "<form id='submit' name='submit' action='".$payUrl."' method='POST'>";
        foreach($para_temp as $key=>$val){
                $sHtml.= "<input type='hidden' name='".$key."' value='".$val."'/>";
        }
        //submit按钮控件请不要含有name属性
        $sHtml = $sHtml."<input type='submit' value='ok' style='display:none;''></form>";
        $sHtml = $sHtml."<script>document.forms['submit'].submit();</script>";
        return $sHtml;
    }

	


}
