<?php
class Mod_ThirdPlatform_KaLaZan_saoma extends Mod_ThirdPlatform_KaLaZan {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝
            $payType = 'alipay';
        } elseif ($type == 3) { // 微信
            $payType = 'wechat';
        } elseif ($type == 5) { // 银联扫码
            $payType = 'ysf';
        } elseif ($type == 9) { // 支付宝扫码
            $payType = 'alipay';
        } elseif ($type == 10) { // 微信扫码
            $payType = 'wechat';
        }

        $native = array(
            'version' => 1,
            'type' => 'text',
            'orderType' => 0,
            'merchant'=>$platInfo['platInfo']['appId'],
            'payType'=>$payType,
            'orderNo'=>$payInfo['pOrderId'],
            'amount'=>number_format(floatval($payInfo['amount']),2, '.', ''),
            'remark' => "",
            'currentTime' => time(),
            'refer'=>$platInfo['appInfo']['clientUrl'],
            'notifyUrl'=>$platInfo['appInfo']['callbackUrl'],
        );

        $sign = "amount={$native['amount']}&currentTime={$native['currentTime']}&merchant={$native['merchant']}&notifyUrl={$native['notifyUrl']}&orderNo={$native['orderNo']}&payType={$native['payType']}&refer={$native['refer']}";
        $sign = $sign . "#" . $platInfo['platInfo']['appKey'];
        $sign = md5($sign);
        $native["sign"] = $sign;

        BooView::set('payUrl', $platInfo['platInfo']['p_pay_url']);
        BooView::set('native', $native);
        BooView::display('pay/formPay.html');
        exit;
    }

}
