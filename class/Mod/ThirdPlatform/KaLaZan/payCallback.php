<?php
class Mod_ThirdPlatform_KaLaZan_payCallback extends Mod_ThirdPlatform_KaLaZan {

	protected function _do($platInfo = array()) {

        $merchant=$_REQUEST['merchant'];//商户ID
        $accName = $_REQUEST['accName'];//收款账号
        $amount=$_REQUEST['amount'];//订单金额（元，两位小数）
        $orderNo=$_REQUEST['orderNo'];//商户订单号
        $remark=$_REQUEST['remark'];//商户备注信息
        $systemNo=$_REQUEST['systemNo'];//平台订单号
        $money=$_REQUEST['money'];//实付金额（元，两位小数）
        $payType=$_REQUEST['payType'];//支付类型
        $payFlag = $_REQUEST['payFlag'];//支付状态 ( 1未支付，2已支付，3已关闭 )
        $createTime = $_REQUEST['createTime'];//创建时间 ( 10位时间戳 )
        $payTime = $_REQUEST['payTime'];//支付时间 ( 10位时间戳 )
        $currentTime=$_REQUEST['currentTime'];//当前时间 ( 10位时间戳 )
        $sign=$_REQUEST['sign'];//md5密钥（KEY）


//步骤1、必填参数按照ascii码表顺序拼接
        $mySign = "accName=".$accName."&amount=".$amount."&createTime=".$createTime."&currentTime=".$currentTime."&merchant=".$merchant."&money=".$money."&orderNo=".$orderNo."&payFlag=".$payFlag."&payTime=".$payTime."&payType=".$payType;
//步骤2、选填参数判断拼接
        if($remark != ""){
            $mySign = $mySign."&remark=".$remark;
        }
        $mySign = $mySign."&systemNo=".$systemNo;
//步骤3、把字段字符串通过“#"号与商户密钥拼接，得到最终的加密字符串
        $mySign = $mySign."#".$platInfo['platInfo']['appKey'];
//步骤4、把最终的加密字符串进行md5加密
        $mySign = md5($mySign);

        if($sign == $mySign){
            if($payFlag==2){
                echo 'success';
                return array('platOrderId' => $_REQUEST["systemNo"],'realMoney' => $_REQUEST["money"]);
            }
        }

        return false;
	}

}
