<?php
class Mod_ThirdPlatform_JiabaoTrade_payCallback extends Mod_ThirdPlatform_JiabaoTrade {

	protected function _do($platInfo = array()) {

        $md5Str = "merchantId={$_REQUEST['merchantId']}&merchantOrderId={$_REQUEST['merchantOrderId']}&orderAmount={$_REQUEST['orderAmount']}&systemOrderId={$_REQUEST['systemOrderId']}&channelType={$_REQUEST['channelType']}&remark={$_REQUEST['remark']}&ip={$_REQUEST['ip']}";
        $sign = md5($md5Str . "{$platInfo['platInfo']['appKey']}");
        if ($_REQUEST['sign'] == $sign) {
            echo 'OK';
            return array('platOrderId' => $_REQUEST['systemOrderId'], 'realMoney' => $_REQUEST['orderAmount']);
        } else {
            //签名验证失败
            return false;
        }

	}

}
