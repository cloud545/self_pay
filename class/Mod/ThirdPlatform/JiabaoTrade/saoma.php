<?php
class Mod_ThirdPlatform_JiabaoTrade_saoma extends Mod_ThirdPlatform_JiabaoTrade {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝wap
            $payType = 'C201';
        } elseif ($type == 3) { // 微信wap
            $payType = 'C101';
        } elseif ($type == 5) { // 银联扫码
            $payType = '926';
        } elseif ($type == 9) { // 支付宝扫码
            $payType = 'C200';
        } elseif ($type == 10) { // 微信扫码
            $payType = 'C100';
        }

        $postData = array();
        $postData['merchantId'] = $platInfo['platInfo']['appId'];
        $postData['merchantOrderId'] = $payInfo['pOrderId'];
        $postData['orderAmount'] = $payInfo['amount'];
        $postData['channelType'] = $payType;
        $postData['notifyUrl'] = $platInfo['appInfo']['callbackUrl'];
        $postData['returnUrl'] = $platInfo['appInfo']['clientUrl'];
        $postData['ip'] = BooUtil::realIp();
        $postData['remark'] = '';
        $postData['jsonResult'] = 1;

        $md5Str = "merchantId={$postData['merchantId']}&merchantOrderId={$postData['merchantOrderId']}&orderAmount={$postData['orderAmount']}&notifyUrl={$postData['notifyUrl']}&channelType={$postData['channelType']}&remark={$postData['remark']}&ip={$postData['ip']}";

        $sign = md5($md5Str . "{$platInfo['platInfo']['appKey']}");
        $postData['sign'] = $sign;

        BooCurl::setData($postData, 'POST');
        $response = BooCurl::call($platInfo['platInfo']['p_pay_url']);

        $result = json_decode($response, true);
        if ($result['Success'] != 'true') {
            BooFile::write('/tmp/jiabaoTrade.log', $response . "\n{$payInfo['amount']}\n{$platInfo['appInfo']['name']}\n111\n" . date('Y-m-d H:i:s') . "\n\n", 'a');
            echo "支付异常，请联系客服! ErrorCode：{$result['ErrorCode']}，ErrorMessage: {$result['ErrorMessage']}";
            exit;
        }

        if (in_array($type, array(2, 3))) {
            BooView::set('payUrl', $result['Qrcode']);
            BooView::display('pay/locationPay.html');
        } else {
            BooView::set('url', $result['Qrcode']);
            BooView::display('pay/saoma.html');
        }

        exit;
    }

}
