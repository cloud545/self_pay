<?php
class Mod_ThirdPlatform_Duobao_saoma extends Mod_ThirdPlatform_Duobao {


    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $pay_bankcode = '1001';
        if ($type == 2) { // 支付宝
            $pay_bankcode = $isMobile ? '1006' : '992';
        } elseif ($type == 3) { // 微信
            $pay_bankcode = $isMobile ? '1007' : '1004';
        } elseif ($type == 4) { // qq
            $pay_bankcode = $isMobile ? '1102' : '993';
        } elseif ($type == 5) { // 银联
            $pay_bankcode = '1001';
        }

        $post = BooVar::requestx();
        $params = array(
            'parter' => $platInfo['platInfo']['appId'],
            'type' => $pay_bankcode, //银行编码
            'value' => number_format($post['amount'],2,'.',''),
            'orderid' => $payInfo['pOrderId'], //商户网站订单
            'callbackurl' => $platInfo['appInfo']['callbackUrl'], //后台异步通知地址
        );

        $md5str = "parter={$params['parter']}&type={$params['type']}&value={$params['value']}&orderid={$params['orderid']}&callbackurl={$params['callbackurl']}";
        $sign = md5($md5str . $platInfo['platInfo']['appKey']);
        $params['sign'] = $sign;

        $p_pay_url = $platInfo['platInfo']['p_pay_url'] . "?{$md5str}&sign={$sign}";


        header('Location: '.$p_pay_url);
    }

}
