<?php
class Mod_ThirdPlatform_Duobao_payOrder extends Mod_ThirdPlatform_Duobao {

	protected function _do($platInfo = array()){

        if (!$platInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $post = BooVar::requestx();
        $params = array(
            'parter' => $platInfo['platInfo']['appId'],
            'type' => $post['bankCode'], //银行编码
            'value' => number_format($post['amount'],2,'.',''),
            'orderid' => $post['pOrderId'], //商户网站订单
            'callbackurl' => $platInfo['appInfo']['callbackUrl'], //后台异步通知地址
        );

        $md5str = "parter={$params['parter']}&type={$params['type']}&value={$params['value']}&orderid={$params['orderid']}&callbackurl={$params['callbackurl']}";
        $sign = md5($md5str . $platInfo['platInfo']['appKey']);
        $params['sign'] = $sign;

        $p_pay_url = $platInfo['platInfo']['p_pay_url'] . "?{$md5str}&sign={$sign}";
        header('Location: '.$p_pay_url);
	}

}
