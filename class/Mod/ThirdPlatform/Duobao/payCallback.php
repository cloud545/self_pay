<?php
class Mod_ThirdPlatform_Duobao_payCallback extends Mod_ThirdPlatform_Duobao {

	protected function _do($platInfo = array()) {

        $post = $_REQUEST;
        if ($post['opstate'] != 0) {
            echo "opstate=1";
            return false;
        }

        $sign = md5("orderid={$post['orderid']}&opstate={$post['opstate']}&ovalue={$post['ovalue']}" . $platInfo['platInfo']['appKey']);
        if ($sign == $post["sign"]) {
            echo "opstate=0";
            return array('platOrderId' => $post["sysorderid"], 'realMoney'=>$post['ovalue']);
        }else {
            //签名验证失败
            echo "opstate=2";
            return false;
        }

	}

}
