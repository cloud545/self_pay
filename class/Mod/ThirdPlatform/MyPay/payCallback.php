<?php
class Mod_ThirdPlatform_MyPay_payCallback extends Mod_ThirdPlatform_MyPay
{

    protected function _do($platInfo = array())
    {

        $back_str = file_get_contents("php://input");
        $back_data = json_decode($back_str, true);

        $postData = array();
        //参与验签的参数
        //参与验签的参数
        $postData['thirdOrderId'] = $back_data['thirdOrderId'];                //商户的订单号
        $postData['thirdUserId'] = $back_data['thirdUserId'];                        //商户的userId
        $postData['tradeNo'] = $back_data['tradeNo'];                       //订单号
        $postData['total'] = $back_data['total'];                   //订单金额
        $postData['status'] = $back_data['status'];                     //支付状态
        $postData['method'] = $back_data['method'];                     //交易支付方式
        $postData['payDate'] = $back_data['payDate'];                     //支付时间
        //验签加密
        $secretKey = '1568273202844rrqkbqjfj03p';
        ksort($postData);
        $md5str = "";
        foreach ($postData as $key => $val) {
            $md5str = $md5str . $key . "=" . $val . "&";
        }

        $checkSign = md5($md5str . "secret=" . $secretKey);
        $postData["sign"] = $back_data['sign'];

        if ($checkSign == $postData['sign'] && $postData['status'] == 1) {
            echo "success";
            return array('platOrderId' => $postData['thirdOrderId'], 'realMoney' => $postData['payMoney']);
        } else {
            echo "error";
            return false;
        }

    }

}