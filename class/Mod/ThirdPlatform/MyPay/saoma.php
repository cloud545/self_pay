<?php
class Mod_ThirdPlatform_MyPay_saoma extends Mod_ThirdPlatform_MyPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];

        $payType =  0;
        if ($type == 20 || $type == 21 || $type == 9 || $type == 2) {       // 24H支付宝wap 和 24H支付宝扫码
            $payType = '900';
        } elseif ($type == 3 || $type == 10) { // 微信wap
            $payType = '200';
        } else {
            exit;
        }

        $postData = [
            'thirdOrderId'  =>      $payInfo['pOrderId'],               //订单号
            'thirdUserId'   =>      $platInfo['platInfo']['appId'],     //商户ID
            'total'         =>      number_format($payInfo['amount'],2,'.',''),                 //付款金额
            'tradeType'     =>      1,                                  //1-代收,充值
            'remark'        =>      '第一',                               //备注
            'callBackUrl'   =>      $platInfo['appInfo']['callbackUrl'],  //回调地址
            'payMethodCode' =>      $payType,                           //支付方式
            'currentStamp'  =>      time(),                             //发起时间
        ];


        //验签加密
        $secretKey = '1568273202844rrqkbqjfj03p';
        ksort($postData);
        $md5str = "";
        foreach ($postData as $key => $val) {
            $md5str = $md5str . $key . "=" . $val . "&";
        }

        $sign = md5($md5str."secret=".$secretKey);
        $postData["sign"] = $sign;

        //非验签参数
        $postData['key'] = $platInfo['platInfo']['appKey'];

        BooCurl::setData(json_encode($postData), 'POST');
        BooCurl::setOption(CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json;charset=UTF-8',
        ));
        $res = BooCurl::call($platInfo['platInfo']['p_pay_url']);
        $data = json_decode($res,true);

        if ($data['code'] != 200 || $data['success'] != true) {
            echo $data['msg'];
            exit;
        }

        BooView::set('payUrl', $data['data']['pay']['payUrl']);
        BooView::display('pay/locationPay.html');
        exit;
    }

}
