<?php
class Mod_ThirdPlatform_DingDangPay_payCallback extends Mod_ThirdPlatform_DingDangPay {

	protected function _do($platInfo = array()) {
        $data = $_POST;
        $sign = $data['sign'];
        $status = $data['status'];
        if ($this->sign($platInfo['platInfo']['appKey'], $data) == $sign || $status == 'success')
        {
            echo 'success';
            return array('platOrderId' => $_POST['out_trade_no'],'realMoney' => $_POST['amount']);
        }
        return false;
	}

    function sign($key_id, $array)
    {
        //计算订单号和金额的拼接字符串的长度
        $str = strlen($array['out_trade_no'].$array['amount']);
        //MD5（密钥+MD5（拼接字符串长度+MD5（订单号+金额）））
        $str_a =  $key_id.md5($str.md5($array['out_trade_no'].$array['amount']));
        return md5($str_a);
    }
}
