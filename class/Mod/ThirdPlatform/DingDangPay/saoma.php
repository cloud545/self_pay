<?php
class Mod_ThirdPlatform_DingDangPay_saoma extends Mod_ThirdPlatform_DingDangPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 5) {
            $payType = '2';
        }

        $data = array(
            'account_id' => $platInfo['platInfo']['appId'],
            'content_type' => 'json',
            'thoroughfare' => 'service_auto',
            'type' => $payType,
            'out_trade_no' => $payInfo['pOrderId'],
            'robin' => '2' ,
            'amount' => number_format($payInfo['amount'], '2', '.', ''),
            'callback_url' => $platInfo['appInfo']['callbackUrl'],
            'success_url' => 'http://www.baidu.com',
            'error_url' => 'http://www.baidu.com',
        );

        $data['sign'] = $this->sign($platInfo['platInfo']['appKey'] ,$data);
        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, $platInfo['platInfo']['p_pay_url']); // 要访问的地址
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        $result = json_decode(curl_exec($curl),1); // 执行操作

        curl_close($curl); // 关闭CURL会话
        if ($result['code'] == 200) {
            header("location:".$result['data']['order_url']);
        }
    }

    function sign($key_id, $array)
    {
        //计算订单号和金额的拼接字符串的长度
        $str = strlen($array['out_trade_no'].$array['amount']);
        //MD5（密钥+MD5（拼接字符串长度+MD5（订单号+金额）））
        $str_a =  $key_id.md5($str.md5($array['out_trade_no'].$array['amount']));
        return md5($str_a);
    }
}
