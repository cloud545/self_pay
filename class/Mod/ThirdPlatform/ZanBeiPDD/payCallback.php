<?php
class Mod_ThirdPlatform_ZanBeiPDD_payCallback extends Mod_ThirdPlatform_ZanBeiPDD {

	protected function _do($platInfo = array()) {
        $data = $_POST;

        $sign = $data['sign'];
        unset($data['sign']);
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key .  $value;
        }
        $signs = strtoupper(md5($platInfo['platInfo']['appKey'].$md5str.$platInfo['platInfo']['appKey']));

        if ($sign == $signs && $data['callbacks'] == 'CODE_SUCCESS') {
            return array('platOrderId' => $data['api_order_sn'],'realMoney' => $data['total']);
        }
        return false;
	}
}
