<?php
class Mod_ThirdPlatform_ZanBeiPDD_saoma extends Mod_ThirdPlatform_ZanBeiPDD {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();
        $payType = 0;

        if ($type == 2 || $type == 7) {
            $payType = 'alipay';
        }

        if ($type == 3 || $type == 6) {
            $payType = 'wechat';
        }

        $data = array(
            'client_id' => $platInfo['platInfo']['appId'],
            'type' => $payType,
            'timestamp' => time() * 1000,
            'api_order_sn' => $payInfo['pOrderId'],
            'total' => $payInfo['amount'],
            'notify_url' => $platInfo['appInfo']['callbackUrl'],
        );
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key .  $value;
        }
        $data['sign'] = strtoupper(md5($platInfo['platInfo']['appKey'].$md5str.$platInfo['platInfo']['appKey']));

        $data = http_build_query(
            $data
        );
        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => $data
            )
        );
        $context = stream_context_create($opts);
        $result = file_get_contents($platInfo['platInfo']['p_pay_url'], false, $context);
        $result = json_decode($result,1);

        if ($result['status'] == '1') {
            header('location:'.$result['data']['h5_url']);
        } else {
            echo $result['msg'];
        }
    }
}
