<?php
class Mod_ThirdPlatform_AZFPay_payCallback extends Mod_ThirdPlatform_AZFPay {

    protected function _do($platInfo = array()) {

        $postData = $_POST;
        $sign = $postData['sign'];
        unset($postData['sign']);
        $status = $this->getStatus($postData, $platInfo['platInfo']['appKey']);
		//进行验签
        $checkSign = $this->sign($postData,$platInfo['platInfo']['appKey']);

        if ($checkSign == $sign &&  $postData['status'] == 2 && $status == 'ok'){
            echo "success";
            return array('platOrderId' => $postData['payOrderId'],'realMoney' => $postData['amount'] / 100);
        } else {
            return false;
        }

    }

    public function getStatus($arr , $appKey) {
        $data['mchId'] = $arr['mchId'];
        $data['appId'] = $arr['appId'];
        $data['payOrderId'] = $arr['payOrderId'];
        $data['mchOrderNo'] = $arr['mchOrderNo'];
        $data['sign'] = $this->sign($data, $appKey);
        $result = $this->payurl('http://pay.supermayun.com/api/pay/query_order', $data);
        if ($result['status'] == '2') {
            return 'ok';
        } else {
            return false;
        }
    }

    public function payurl($url,$data){
        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        $result = json_decode(curl_exec($curl),1);
        curl_close($curl); // 关闭CURL会话
        return $result;
    }
	
	public function sign($params = [], $secret = '')
    {
        ksort($params);
        $str = '';
        foreach ($params as $k => $v) {
            if ($v) {
                $str = $str . $k .'='. $v.'&';
            }
        }
        $str = $str .'key='. $secret;
        return strtoupper(md5($str));
    }
}

