<?php
class Mod_ThirdPlatform_HTPay_saoma extends Mod_ThirdPlatform_HTPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = '';
        if ($type == 2) { // 支付宝
            $payType = $isMobile ? 1004 : 1003;
        } elseif ($type == 3) { // 微信
            $payType = 1002;
        } elseif ($type == 4) { // qq
            $payType = $isMobile ? 1005 : 1006;
        } elseif ($type == 5) { // 银联
            $payType = 1009;
        } elseif ($type == 6) { // 京东
            $payType = 1007;
        }

        $post = BooVar::requestx();

        $pay = array();
        $pay['customer'] = $platInfo['platInfo']['appId'];
        $pay['banktype'] = $payType;
        $pay['amount'] = number_format($post['amount'],2,'.','');
        $pay['orderid'] = $payInfo['pOrderId'];
        $pay['asynbackurl'] = $platInfo['appInfo']['callbackUrl'];
        $pay['request_time'] = date('YmdHis');
        $pay['synbackurl'] = $platInfo['appInfo']['clientUrl'];
        $pay['attach'] = '';

        $signStr = "customer={$pay['customer']}&banktype={$pay['banktype']}&amount={$pay['amount']}&orderid={$pay['orderid']}&asynbackurl={$pay['asynbackurl']}&request_time={$pay['request_time']}&key={$platInfo['platInfo']['appKey']}";
        $pay['sign'] = md5($signStr);

        $formstr = '';
        foreach ($pay as $key => $val) {
            $formstr .= '<input type="hidden" name="' . $key . '" value="' . $val . '">';
        }

        echo '
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            </head>
            <body onLoad="document.zlinepay.submit();">
                <form name="zlinepay" method="post" action="' . $platInfo['platInfo']['p_pay_url'] . '" target="_top"/>'.$formstr.'</form>
            </body>
        </html>
        ';
        exit;
    }

}
