<?php
class Mod_ThirdPlatform_ChangYouFu_payCallback extends Mod_ThirdPlatform_ChangYouFu {

	protected function _do($platInfo = array()) {
        $data = json_decode(file_get_contents('php://input'), true);

        $sign = $data['sign'];
        unset($data['sign']);
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $md5str = substr($md5str, 0 , strlen($md5str)-1);
        $signs = strtoupper(md5($md5str.$platInfo['platInfo']['appKey']));

        if ($sign == $signs && $data['status'] == 'success') {
            echo 'success';
            return array('platOrderId' => $data['out_trade_no'],'realMoney' => $data['money']);
        }
        return false;
	}
}
