<?php
class Mod_ThirdPlatform_ChangYouFu_saoma extends Mod_ThirdPlatform_ChangYouFu {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();
        $payType = 0;

        if ($type == 9) {
            $payType = '8001030';
        }
        $data = array(
            'merchant_id' => $platInfo['platInfo']['appId'],
            'version' => 'V2.0',
            'pay_type' => $payType,
            'device_type' => 'pc',
            'request_time' => date('YmdHis'),
            'nonce_str' => time(),
            'pay_ip' =>  $_SERVER["REMOTE_ADDR"],
            'out_trade_no' => $payInfo['pOrderId'],
            'amount' => number_format($payInfo['amount'],'2','.',''),
            'notify_url' => $platInfo['appInfo']['callbackUrl'],
        );
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $md5str = substr($md5str, 0 , strlen($md5str)-1);
        $data['sign'] = strtoupper(md5($md5str.$platInfo['platInfo']['appKey']));
        $data = json_encode($data);
        $ch = curl_init($platInfo['platInfo']['p_pay_url']);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data))
        );
        $result = json_decode(curl_exec($ch),1);
        if ($result['status'] == 'success') {
            header('location:'.$result['pay_url']);
        } else {
            echo $result['message'];
        }
    }
}
