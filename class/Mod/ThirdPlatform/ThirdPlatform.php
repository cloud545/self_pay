<?php
class Mod_ThirdPlatform extends Mod{

    protected function getSign($data, $appKey) {

        ksort($data);

        $checkString = '';
        foreach ($data as $key =>$value) {
            if (!$checkString) {
                $checkString = "{$key}={$value}";
            } else {
                $checkString .= "&{$key}={$value}";
            }
        }

        $checkString .= "{$appKey}";
        $sign = md5($checkString);

        return $sign;
    }

}