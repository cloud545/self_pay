<?php
class Mod_ThirdPlatform_RShengPay_payCallback extends Mod_ThirdPlatform_RShengPay{
    protected function _do($platInfo = array())
    {


        $postData = [
            "merchant_id"   => $_REQUEST["merchant_id"],
            "pay_time"      => $_REQUEST["pay_time"],
            "status"        => $_REQUEST["status"],
            "amount"        => $_REQUEST["amount"],
            "out_trade_no"  => $_REQUEST["out_trade_no"],
            "trade_no"      => $_REQUEST["trade_no"],
            "callback_time" => $_REQUEST["callback_time"],
            "type"          => $_REQUEST["type"]
        ];

        //验签加密
        $checkSign = $this->sign($platInfo['platInfo']['appKey'], $postData);
        //参与验签的参数
        $postData["sign"] = $_REQUEST['sign'];
        if ($checkSign == $postData['sign'] && $postData['status'] == 'success') {
            echo "success";
            return array('platOrderId' => $postData['trade_no'], 'realMoney' => $postData['amount']);
        } else {
            echo "error";
            return false;
        }

    }
}