<?php
class Mod_ThirdPlatform_RShengPay_saoma extends Mod_ThirdPlatform_RShengPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];

        $payType =  0;
        if ($type == 2) {       // 支付宝wap
            $payType = '7';
        } elseif ($type == 3) { // 微信wap
            $payType = '200';
        } elseif ($type == 5) { // 银联扫码
            $payType    = '300';
        } elseif ($type == 9) { // 支付宝扫码
            $payType        = '7';
        } elseif ($type == 10) { // 微信扫码
            $payType        = '200';
        }

        //参与验签的数据
        $postData = [
            'merchant_id'      =>  $platInfo['platInfo']['appId'],     // 商户ID
            'amount'           =>  $payInfo['amount'],                            // 支付金额
            'out_trade_no'     =>  $payInfo['pOrderId'],               //订单号，
            'callback_url'     =>  $platInfo['appInfo']['callbackUrl'],//异步通知地址，在支付完成时，本平台服务器系统会自动向该地址发起一条支付成功的回调请求, 对接方接收到回调后，必须返回 success ,否则默认为回调失败,回调信息会补发3次
            'success_url'      =>  $platInfo['appInfo']['clientUrl'],  // 支付成功跳转地址，支付成功后自动跳转到该地址
            'fail_url'         =>  $platInfo['appInfo']['clientUrl'],  // 异步通知地址
            'type'             =>  $payType,                          // 支付类型，目前可用取值为 1（支付宝AA） 2（拼多多微信） 3（拼多多支付宝） 4（支付宝当面付） 5（云闪付） 6（支付宝转卡） 7（淘代付）
            'timestamp'        =>  time()                         // 时间戳，10位数字的Unix时间戳格式

        ];

        //验签加密
        $postData["sign"] = $this->sign($platInfo['platInfo']['appKey'], $postData);
        BooView::set('payUrl', $platInfo['platInfo']['p_pay_url']);
        BooView::set('native', $postData);
        BooView::display('pay/formPay.html');
        exit;

    }
}

