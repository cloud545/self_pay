<?php
class Mod_ThirdPlatform_RShengPay extends Mod_ThirdPlatform {

    /**
     * 签名算法
     * @param unknown $key_id S_KEY（商户KEY）
     * @param unknown $array 除sign外其他全部参数
     * @return string
     */
    public function sign($key_id, $array) {
        if (is_null($key_id)) {
            return null;
        }
        ksort($array);
        $buff = '';
        foreach ($array as $k => $v) {
            $buff .= ($k != 'sign' && $v != '' && !is_array($v)) ? $k.'='.$v.'&' : '';
        }
        $content = trim($buff, '&') .'&key=' . $key_id;
        $string = md5($content);
        return strtoupper($string);
    }


}
