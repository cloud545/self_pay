<?php
class Mod_ThirdPlatform_Xinyin_saoma extends Mod_ThirdPlatform_Xinyin {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝wap
            $payType = 9;
        } elseif ($type == 3) { // 微信wap
            $payType = 8;
        } elseif ($type == 5) { // 银联
            $payType = 11;
        } elseif ($type == 9) { // 支付宝扫码
            $payType = 3;
        } elseif ($type == 10) { // 微信扫码
            $payType = 2;
        }

        //呼叫充值所需要参数
        $bankCode = '';
        $serviceType = "{$payType}";
        $amount = number_format($payInfo['amount'],2,'.','');
        $merchantUser = '1';
        $merchantOrderNo = $payInfo['pOrderId'];
        $note = $payInfo['pOrderId'];

        //呼叫充值申请
        $this->depositUrl = $platInfo['platInfo']['p_pay_url'];  //payment充值API接口
        $this->merchantCode = $platInfo['platInfo']['appId'];              //payment代理商号
        $this->depositCallbackUrl = $platInfo['appInfo']['callbackUrl'];     //Callback URL 这部份需要贵司更换为贵司的充值回调连接

        $receive_data = $this->depositRequest($bankCode, $serviceType, $amount, $merchantUser, $merchantOrderNo, $note);
        if (!$receive_data) {
            BooFile::write('/tmp/xinyin.log', $receive_data . "\n{$payInfo['amount']}\n{$platInfo['appInfo']['name']}\n111\n\n", 'a');
            echo "支付异常，请联系客服! 返回数据：{$receive_data}";
            exit;
        }

        BooView::set('payUrl', $receive_data);
        BooView::display('pay/locationPay.html');
        exit;
    }

}
