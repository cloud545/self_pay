<?php
class Mod_ThirdPlatform_Xinyin_payCallback extends Mod_ThirdPlatform_Xinyin {

	protected function _do($platInfo = array()) {

        //接收回调信息
        $m_sign = $_POST['sign'];
        $m_merchant_code = $_POST['merchant_code'];
        $m_data = $_POST['data'];

        $this->depositUrl = $platInfo['platInfo']['p_pay_url'];  //payment充值API接口
        $this->merchantCode = $platInfo['platInfo']['appId'];              //payment代理商号
        $this->depositCallbackUrl = $platInfo['appInfo']['callbackUrl'];     //Callback URL 这部份需要贵司更换为贵司的充值回调连接

        $tmp_verifySign = $this->verifySign($m_data,$m_sign);

        if ($tmp_verifySign == '1') {
            echo "{\"error_msg\":\" \",\"status\":\"1\"}";
            //$tmp_decrypt= $payment_class->depositCallback($m_data);
            return array('platOrderId' => $_POST['trans_id'],'realMoney' => $_POST['amount']);
        } else {
            //签名验证失败
            return false;
        }

	}

}
