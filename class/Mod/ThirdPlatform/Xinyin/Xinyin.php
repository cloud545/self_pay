<?php
class Mod_ThirdPlatform_Xinyin extends Mod_ThirdPlatform {

    protected $withdrawUrl;
    protected $depositUrl;
    protected $merchantCode;
    protected $depositCallbackUrl;
    protected $withdrawCallbackUrl;
    protected $ps_public_key = "-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkTVMv53LgkX/WHbiIsTd6Nk2KHADrkA8iYXHG726IVMbmSPCn3dJqYAweuHqa+BTcCP1IhPYc8ltRX6ZP75GHJaD/6EvgZdAJKMMZ3LahXruusDjJkPPeCPsPI89P1Y62SjKs72+I+A3BlYWse0F/lu3BJDiCE88AbBcc86rEOT2n0B3dagqpTzUeqwgprFPVl3UGDmYAvkMtwtzjJ3etYpoDCenakcPEvAlcxqU224zvDMolDYClf/tuXFlQSEn3BTlxDtPV1Xu6mFr1h2QeQ6ggVYazTrw3tr2/cLqt7lDn9Gq8xp4gQgUQNSlxDmsRqsmW00UlhjJdb5u3Sb0+QIDAQAB
-----END PUBLIC KEY-----";
    protected $merchantPrivateKey = "-----BEGIN RSA PRIVATE KEY-----
MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDtk0QCXttDpgUAmblptwzy5Kvfw6O16fxuP/41iS4eJ2j7+QejQij1rZ7TQpuGDwcJdQaTXB23nYo0QWIqBbauKmXqQql21u55/+TZzsvnQGUGKgR260oo8DlguvMt3bZDGqVvLWccjdAlQUVZk2e7Fo6IzmLt2reOR80amJFWznqVLYeeAHXzKGyqoQCOXOGvBnUdSbwK8lJk8KkNggB3mrO38nAfD62t0ZyDmiiZjLNeExty9WzlgIUDtAmoUHRY/2I8kz/URxgU0ELfCngKk2Z8nLNL6yDJUf//em9IVx1SfAPyvT5XOKuDDt7B9KscZmo6dIdt+Bdi7s/xAMozAgMBAAECggEBALouzv2MmQlK8mCmGEhgzCUOUCPsjj/iP5GnVPFfsGSxPfU6FGI9VxwyFGY7rOZCoKEotR6G8kMTk1ugYe3MzH1xfK050NTxW7MAd7i599IjBQjvpNANVPEAUfcci7dGKbAtEbkvO7+E47pOjKrGemSEKO/NznnZDkDSxEozhrMfWv/ZSSvkdAPISBX60B29XAR/w8+WqGVFJjDBVWw5YQLJ7lSwOEfzHxUyfsGNMeEo7GsvBFCTPu0cp2M/BMcwxMaGks/dh6cJ41FoC3/DlV3F9oPviUjXBI8xM76+lLCvZ8aeXnonojVpfUZglHR+tgkNJV+Ufm5WSUCNI0fvowECgYEA/a1XLUoOFDW9vW46t5We8+p5PU9gRY4fnE53OBv/CbM4eyYiHp6yGQHHQ1JC4t2uzH6xQnqAFh0OvCINvha0KL5eRFS/oPWEWuqM/aBq4M/LcK7iZxJoeXbV34JCsoKVMm3fq5Jw54BLagoc2SPSatqlKU7om6/VDCPxuCsq/TUCgYEA78AuCKMmsuKZW/wbISIlKEurpDBaHGO2vctPx83ec60KupH7AAkAZZPeOf1xfkkNNMTEqUgXeY1zb+FMdLBySWoVlg7StLL4R6LKEJH3sJVmuPvz+K8PvuXBftWmVaiMXNOsRR0eGhWNdjA5L3YpsfFZqkmGB3tcqnlqA2k63scCgYEA0AeGpi1Evm4T23S50jNw1rZCFnUxjdivM49lVS4gR+QhPVxYPqQFiVxb/GlJUKJa5ng/BTVQNNI/cyi5/FjCBWeYBpE5VNYCZExUsukKvLGrNA02vS/rXTCG03QPHjTYBTL1ac39qNsc/Sit36Pwuo/zQZzkREg9fmc7orxD1MECgYATaSHQ/TSS4xm7gsvHgPY1Lvs0jd2d0szmzOEmshpCj04BEPSbah8oGjKxyz03Fj8FeLDba8TKyJMCCeGgyPBX1UjuH0Eb9Cl500o80TSWjWIFtBBqn/HMaNmjpTSUVtMWaOTXq6XWQsxzNzBKEW0KxRmXWeCKFojIHJBiulBT3QKBgQDaF6QwPEymHT3VmA7ekSMJJGXbUfMiswcwvNdBGcz0ar6POjfrPsz9Cwy1QC53a3eFBR7iTsLj0QGDt51WAcVY3M42KQFZjBO/DiMBWIVedXUhv2mQ3khvUI70CrwCfKbDeOj30xTW+enJkCoWUTwxvDP0smSfjY1sk55Lxe+3FA==
-----END RSA PRIVATE KEY-----";
    protected $RISK_LVL	= '1';
    protected $PLATFORM	= 'PC';

    /**
     * 共用参数区
     *
     * payment_class constructor.
     * @param $merchantCode
     * @param $depositUrl
     * @param $depositCallbackUrl
     */
    function setInfo($merchantCode, $depositUrl, $depositCallbackUrl){

        /**需调整参数*/
        $this->withdrawUrl = 'https://api.XXXX.com/rsa/withdraw';  //payment提现API接口
        $this->depositUrl = $depositUrl;  //payment充值API接口
        $this->merchantCode = $merchantCode;              //payment代理商号
        $this->depositCallbackUrl = $depositCallbackUrl;     //Callback URL 这部份需要贵司更换为贵司的充值回调连接
        $this->withdrawCallbackUrl = 'http://www.XXXX.com/callback.php';    //Callback URL 这部份需要贵司更换为贵司的提现回调连接

        //支付平台公钥
        $this->ps_public_key = "-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkTVMv53LgkX/WHbiIsTd6Nk2KHADrkA8iYXHG726IVMbmSPCn3dJqYAweuHqa+BTcCP1IhPYc8ltRX6ZP75GHJaD/6EvgZdAJKMMZ3LahXruusDjJkPPeCPsPI89P1Y62SjKs72+I+A3BlYWse0F/lu3BJDiCE88AbBcc86rEOT2n0B3dagqpTzUeqwgprFPVl3UGDmYAvkMtwtzjJ3etYpoDCenakcPEvAlcxqU224zvDMolDYClf/tuXFlQSEn3BTlxDtPV1Xu6mFr1h2QeQ6ggVYazTrw3tr2/cLqt7lDn9Gq8xp4gQgUQNSlxDmsRqsmW00UlhjJdb5u3Sb0+QIDAQAB
-----END PUBLIC KEY-----";

        // 商戶的RSA私钥，贵司可以透过支付平台的后台产生，但记得储存贵司的公私钥
        $this->merchantPrivateKey = "-----BEGIN RSA PRIVATE KEY-----
MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDtk0QCXttDpgUAmblptwzy5Kvfw6O16fxuP/41iS4eJ2j7+QejQij1rZ7TQpuGDwcJdQaTXB23nYo0QWIqBbauKmXqQql21u55/+TZzsvnQGUGKgR260oo8DlguvMt3bZDGqVvLWccjdAlQUVZk2e7Fo6IzmLt2reOR80amJFWznqVLYeeAHXzKGyqoQCOXOGvBnUdSbwK8lJk8KkNggB3mrO38nAfD62t0ZyDmiiZjLNeExty9WzlgIUDtAmoUHRY/2I8kz/URxgU0ELfCngKk2Z8nLNL6yDJUf//em9IVx1SfAPyvT5XOKuDDt7B9KscZmo6dIdt+Bdi7s/xAMozAgMBAAECggEBALouzv2MmQlK8mCmGEhgzCUOUCPsjj/iP5GnVPFfsGSxPfU6FGI9VxwyFGY7rOZCoKEotR6G8kMTk1ugYe3MzH1xfK050NTxW7MAd7i599IjBQjvpNANVPEAUfcci7dGKbAtEbkvO7+E47pOjKrGemSEKO/NznnZDkDSxEozhrMfWv/ZSSvkdAPISBX60B29XAR/w8+WqGVFJjDBVWw5YQLJ7lSwOEfzHxUyfsGNMeEo7GsvBFCTPu0cp2M/BMcwxMaGks/dh6cJ41FoC3/DlV3F9oPviUjXBI8xM76+lLCvZ8aeXnonojVpfUZglHR+tgkNJV+Ufm5WSUCNI0fvowECgYEA/a1XLUoOFDW9vW46t5We8+p5PU9gRY4fnE53OBv/CbM4eyYiHp6yGQHHQ1JC4t2uzH6xQnqAFh0OvCINvha0KL5eRFS/oPWEWuqM/aBq4M/LcK7iZxJoeXbV34JCsoKVMm3fq5Jw54BLagoc2SPSatqlKU7om6/VDCPxuCsq/TUCgYEA78AuCKMmsuKZW/wbISIlKEurpDBaHGO2vctPx83ec60KupH7AAkAZZPeOf1xfkkNNMTEqUgXeY1zb+FMdLBySWoVlg7StLL4R6LKEJH3sJVmuPvz+K8PvuXBftWmVaiMXNOsRR0eGhWNdjA5L3YpsfFZqkmGB3tcqnlqA2k63scCgYEA0AeGpi1Evm4T23S50jNw1rZCFnUxjdivM49lVS4gR+QhPVxYPqQFiVxb/GlJUKJa5ng/BTVQNNI/cyi5/FjCBWeYBpE5VNYCZExUsukKvLGrNA02vS/rXTCG03QPHjTYBTL1ac39qNsc/Sit36Pwuo/zQZzkREg9fmc7orxD1MECgYATaSHQ/TSS4xm7gsvHgPY1Lvs0jd2d0szmzOEmshpCj04BEPSbah8oGjKxyz03Fj8FeLDba8TKyJMCCeGgyPBX1UjuH0Eb9Cl500o80TSWjWIFtBBqn/HMaNmjpTSUVtMWaOTXq6XWQsxzNzBKEW0KxRmXWeCKFojIHJBiulBT3QKBgQDaF6QwPEymHT3VmA7ekSMJJGXbUfMiswcwvNdBGcz0ar6POjfrPsz9Cwy1QC53a3eFBR7iTsLj0QGDt51WAcVY3M42KQFZjBO/DiMBWIVedXUhv2mQ3khvUI70CrwCfKbDeOj30xTW+enJkCoWUTwxvDP0smSfjY1sk55Lxe+3FA==
-----END RSA PRIVATE KEY-----";


        // Common 固定常数
        $this->RISK_LVL	= '1';
        $this->PLATFORM	= 'PC';
    }

    /*
    * 充值申请
    * $bankCode(string)         银行代码(只有网关使用到)
    * $serviceType(string)      服务类型
    * $amount(string)           金额(支持到小数点俩位)
    * $merchantUser(string)     商户用户名称
    * $merchantOrderNo(string)  商户订单代码(不能重复)
    * $note(string)             备注(可为空字段)
    * $return                   回传值
    */
    public function depositRequest($bankCode, $serviceType, $amount, $merchantUser, $merchantOrderNo, $note){

//网关service type:1
//需要使用bankcode，其馀都不需要使用到bankcode
//参数也需要一并拿掉
        if  ( $serviceType === "1"  ) {
            $depositParameters = array(
                'amount' => $amount,
                'platform' => $this->PLATFORM,
                'note' => $note ,
                'bank_code' =>$bankCode,
                'service_type' => $serviceType,
                'merchant_user' => $merchantUser,
                'merchant_order_no' => $merchantOrderNo,
                'risk_level' => $this->RISK_LVL,
                'callback_url' => $this->depositCallbackUrl,
            );
        }
        else
        {
            $depositParameters = array(
                'amount' => $amount,
                'platform' => $this->PLATFORM,
                'note' => $note ,
                'bank_code' => '',
                'service_type' => $serviceType,
                'merchant_user' => $merchantUser,
                'merchant_order_no' => $merchantOrderNo,
                'risk_level' => $this->RISK_LVL,
                'callback_url' => $this->depositCallbackUrl,
            );
        }

        //json格式化后在去掉反斜线
        $ex =  stripslashes(json_encode($depositParameters));

        //使用支付后台公钥 加密json data
        $ret_e = $this->encrypt($ex,$this->ps_public_key);

        //制作簽名
        $tmp_sign = $this->sign($ret_e);

        //组合post字段转换特殊字符
        $tmpSend="merchant_code=".$this->merchantCode."&data=".rawurlencode($ret_e)."&sign=".rawurlencode($tmp_sign);

        $opts = array(
            'http' => array(
                'method'  => 'POST',
                'header'  => "Content-type:application/x-www-form-urlencoded",
                'content' => $tmpSend,
                'timeout' => 1 * 60 // 超时时间（单位:s）
            )
        );

        //传送请求
        $context = stream_context_create($opts);

        //接收返回信息
        $result = file_get_contents($this->depositUrl, false, $context);



        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->depositUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // post数据
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 信任任何证书
        // post的变量
        curl_setopt($ch, CURLOPT_POSTFIELDS, $tmpSend);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-type: application/x-www-form-urlencoded',
                'Content-Length: ' . strlen($tmpSend))
        );
        $data = curl_exec($ch);
        curl_close($ch);

        $J=json_decode($result);
        $tmp_str=$J->data;
        $tmp_error=$J->error_code;

        $decrypted = "";
        $decodeStr =($tmp_str);

        //解密信息
        $decrypted =$this->decrypt($decodeStr );
        //判断是否有错误,有错误输出错误代码，没错误输出充值连结
        if (empty($transaction_url)==1) {
            $J2=json_decode($decrypted);
            $result=$J2->transaction_url;
        }

        if (!$tmp_error=="") {
            //$result= $tmp_error;
        }

        return  $result;
    }


    /**
     * 提现申请
     * $bankCode(string)         银行代码
     * $card_num(string)         银行卡卡号
     * $amount(string)           金额(支持到小数点俩位)
     * $merchantUser(string)     商户用户名称
     * $merchantOrderNo(string)  商户订单代码(不能重复)
     * $card_name(string)        银行卡姓名
     * $bank_branch(string)      银行卡分行
     * $bank_province(string)    银行卡省份
     * $bank_city(string)        银行卡城市
     * $return                   回传值
     */
    public function withdrawRequest($bankCode, $card_num, $amount, $merchantUser, $merchantOrderNo, $card_name , $bank_branch, $bank_province, $bank_city)
    {

        $withdrawParameters = array(
            'amount' => $amount,
            'platform'  =>  $this-> PLATFORM,
            'bank_code'  =>  $bankCode,
            'merchant_user'  =>  $merchantUser,
            'merchant_order_no'  =>  $merchantOrderNo,
            'card_num'  =>  $card_num,
            'card_name'  =>  $card_name,
            'bank_branch'  =>  $bank_branch,
            'bank_province'  =>  $bank_province,
            'bank_city'  =>  $bank_city,
            'callback_url'  =>  $this->withdrawCallbackUrl,
        );

        //json格式化后在去掉反斜线
        $ex =  urldecode(json_encode(array_map('urlencode', $withdrawParameters)));

        //使用捷付支付后台公钥 加密json data
        $ret_e = $this->encrypt($ex,$this->ps_public_key);

        //制作簽名
        $x = $this->sign($ret_e);

        //组合post字段转换特殊字符
        $tmpSend="merchant_code=".$this->merchantCode."&data=".rawurlencode($ret_e)."&sign=".rawurlencode($x);

        $opts = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'content' => $tmpSend
            )
        );

        //传送请求
        $context = stream_context_create($opts);

        //接收返回信息
        $result = file_get_contents($this->withdrawUrl, false, $context);

        $J=json_decode($result);
        $tmp_str=$J->data;
        $tmp_error=$J->error_code;

        $decrypted = "";
        $decodeStr =  ($tmp_str);

        //解密信息
        $decrypted =$this->decrypt($decodeStr );

        //判断是否有错误,有错误输出错误代码，没错误输出 交易ID
        if ($tmp_error=='') {
            $J2=json_decode($decrypted);
            $result=$J2->trans_id;
        } else {
            $result=$tmp_error;
        }

        return  $result;
    }



    //call back 验签
    public function verifySign($data,$sign )
    {
        $tmp_sign=openssl_verify( ($data), base64_decode(($sign)), $this->ps_public_key  );   //平台公钥验签
        $result=$tmp_sign;
        return  $result;
    }




    //提现callback解密
    public function depositCallback($data){
        $decrypted = '';
        $decodeStr = base64_decode($data);
        $enArray = str_split($decodeStr, 256);
        foreach ($enArray as $va) {
            openssl_private_decrypt($va,$decryptedTemp,$this->merchantPrivateKey );//私钥解密
            $decrypted .= $decryptedTemp;
        }
        $result=$decrypted;
        return  $result;
    }


//=================================================================================================================================


    /*
     *自定义错误处理方式
     */
    private function _error($msg)
    {
        die('RSA Error:' . $msg); //TODO
    }

    /**
     * 检查填充类型
     * 加密只支持PKCS1_PADDING
     * 解密支持PKCS1_PADDING和NO_PADDING
     *
     * $padding int 填充模式(OPENSSL_PKCS1_PADDING,OPENSSL_NO_PADDING ...etc.)
     * $type string 加密en/解密de
     * $ret bool
     */
    private function _checkPadding($padding, $type)
    {
        if ($type == 'en') {
            switch ($padding) {
                case OPENSSL_PKCS1_PADDING:
                    $ret = true;
                    break;
                default:
                    $ret = false;
            }
        } else {
            switch ($padding) {
                case OPENSSL_PKCS1_PADDING:
                case OPENSSL_NO_PADDING:
                    $ret = true;
                    break;
                default:
                    $ret = false;
            }
        }
        return $ret;
    }

    private function _encode($data, $code)
    {
        switch (strtolower($code)) {
            case 'base64':
                $data = base64_encode('' . $data);
                break;
            case 'hex':
                $data = bin2hex($data);
                break;
            case 'bin':
            default:
        }
        return $data;
    }

    private function _decode($data, $code)
    {
        switch (strtolower($code)) {
            case 'base64':
                $data = base64_decode($data);
                break;
            case 'hex':
                $data = $this->_hex2bin($data);
                break;
            case 'bin':
            default:
        }
        return $data;
    }

    private function _hex2bin($hex = false)
    {
        $ret = $hex !== false && preg_match('/^[0-9a-fA-F]+$/i', $hex) ? pack("H*", $hex) : false;
        return $ret;
    }


    /**
     * 生成签名
     * $data string 签名材料
     * $code string 签名编码（base64/hex/bin）
     * $ret 签名值
     */
    public function sign($data, $code = 'base64')
    {
        $ret = false;
        if (openssl_sign($data, $ret, $this->merchantPrivateKey ,OPENSSL_ALGO_SHA1 )) {
            $ret = $this->_encode($ret, $code);
        }
        return $ret;
    }

    /**
     * 驗證簽名
     *
     * @param string 簽名材料
     * @param string 簽名值
     * @param string 簽名編碼（base64/hex/bin）
     * @return bool
     */
    public function verify($data, $sign, $code = 'base64')
    {
        $ret = false;
        $sign = $this->_decode($sign, $code);
        if ($sign !== false) {
            switch (openssl_verify($data, $sign, $this->ps_public_key )) {
                case 1:
                    $ret = true;
                    break;
                case 0:
                case -1:
                default:
                    $ret = false;
            }
        }
        return $ret;
    }

    /**
     * 加密
     *
     * @param string 明文
     * @param string 密文編碼（base64/hex/bin）
     * @param int 填充方式(所以目前僅支持OPENSSL_PKCS1_PADDING)
     * @return string 密文
     */
    public function encrypt($data , $code = 'base64', $padding = OPENSSL_PKCS1_PADDING )
    {
        $ret = false;
        if (!$this->_checkPadding($padding, 'en')) $this->_error('padding error');
        $tmpCode="";


        //明文过长分段加密
        foreach (str_split($data, 117) as $chunk) {
            openssl_public_encrypt($chunk, $encryptData, $this->ps_public_key, $padding);
            $tmpCode .=$encryptData ;
            $ret = base64_encode($tmpCode);

        }
        return $ret;
    }

    /**
     * 解密
     *
     * @param string 密文
     * @param string 密文編碼（base64/hex/bin）
     * @param int 填充方式（OPENSSL_PKCS1_PADDING / OPENSSL_NO_PADDING）
     * @param bool 是否翻轉明文（When passing Microsoft CryptoAPI-generated RSA cyphertext, revert the bytes in the block）
     * @return string 明文
     */
    public function decrypt($data, $code = 'base64', $padding = OPENSSL_PKCS1_PADDING, $rev = false)
    {
        $ret = false;
        $data = $this->_decode($data, $code);
        if (!$this->_checkPadding($padding, 'de')) $this->_error('padding error');
        if ($data !== false) {

            $enArray = str_split($data, 256);
            foreach ($enArray as $va) {
                openssl_private_decrypt($va,$decryptedTemp,$this->merchantPrivateKey);//私钥解密
                $ret .= $decryptedTemp;
            }
        }
        else
        {
            echo "<br>解密失敗<br>".$data;
        }




        return $ret;
    }

}
