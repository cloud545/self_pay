<?php
class Mod_ThirdPlatform_XWPay_saoma extends Mod_ThirdPlatform_XWPay {

    protected function _do($platInfo = array(), $payInfo = array())
    {
    if (!$platInfo || !$payInfo) {
        Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
    }

    $type = $platInfo['platInfo']['pt_id'];
    $isMobile = BooMobileDetect::getInstance()->isMobile();

    $payType = 0;
    if ($type == 2 || $type == 9) {
        $payType = 'AlipayBank';
    }elseif ($type == 3 || $type == 10) {
        $payType = 'WechatPay';
    }elseif ($type == 5) { // 银联扫码
        $payType = 'UnionCloudPay';
    } elseif($type == '17') {
        $payType = 'AB2B';
    }elseif($type == '14') {
        $payType = 'AWB2B';
    } else{
        exit;
    }

    $native = [
        'userId' 	                    => 4,
        'terminalType' 	=> $isMobile ? 2 : 1,
        'merchantCode'	=>$platInfo['platInfo']['appId'],
        'channel'	                    =>$payType,
        'merchantTradeNo'	=>$payInfo['pOrderId'],
        'amount'	                     =>number_format(floatval($payInfo['amount']),2, '.', ''),
        'extendedAttrData'            => "",
        'returnUrl'	=>$platInfo['appInfo']['clientUrl'],
        'notifyUrl'	=>$platInfo['appInfo']['callbackUrl'],
    ];

	ksort($native);
	//申请如下
	//1.签名：用商户私钥
	      $signData = '';
	  foreach ($native as $value) {
		  if (!$value) {
			  continue;
		  }
		  $signData .= $value;
	  }
	$native["sign"] =  $this->generateSign($signData);
		//2.加密：用平台公钥 得到加密 $encrypted
	$enc = 	$this->PublicEncrypt(json_encode($native));
	//组合成请求参数
	 $arrData = [
		 'merchantCode'  => $platInfo['platInfo']['appId'],
		 'content'               => $enc,
	 ];

	 BooCurl::setData(json_encode($arrData), 'POST');
	 BooCurl::setOption(CURLOPT_HTTPHEADER, array(
		 'Content-Type: application/json;charset=UTF-8',
	 ));
	 $response = BooCurl::call($platInfo['platInfo']['p_pay_url']);
	 $responseData = json_decode($response, true);

	 //验证参数1
	 if (!$responseData || $responseData['status'] != 'SUCCESS') {
		BooFile::write('/tmp/XWPay.log', $response . "\n{$payInfo['amount']}\n{$platInfo['appInfo']['name']}\n111\n\n", 'a');
		echo $response;
		exit;
	}

	 //验证参数2
	 $responseData2 = json_decode($responseData['data'], true);
	 if (!$responseData2 ) {
		echo "支付异常，请联系客服! data: {$responseData['data']}";
		exit;
	 }

	//申请响应结果如下…
	//1.解密：用商私钥 得到解密的数组 $decrypted
	$decrypted = $this->PrivateDecrypt($responseData2['content']);
	 //2.验签：用平台公钥 得到 验签结果
	 $arrDecrypt = json_decode($decrypted, true);
	  $natiVerify= [
		'merchantCode'	=>	$arrDecrypt['merchantCode'],
		'merchantTradeNo'=>	$arrDecrypt['merchantTradeNo'],
		'tradeNo'	=>	$arrDecrypt['tradeNo'],
		'payUrl'	=>	$arrDecrypt['payUrl'],
	  ];

	ksort($natiVerify);
	  $strVer = '';
	  foreach ($natiVerify as $value) {
		  if (!$value) {
			  continue;
		  }
		  $strVer .= $value;
	  }

	 $verify =  $this->veritySign($strVer,$arrDecrypt['sign']);
	 $result = ($verify==1?'success':'false');
	 if ($result!= 'success') {
		echo "验签失败";
		exit;
	 }
	BooView::set('payUrl', $arrDecrypt['payUrl']);
	BooView::display('pay/locationPay.html');
	exit;
    }
}