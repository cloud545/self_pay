<?php
class Mod_ThirdPlatform_XWPay_payCallback extends Mod_ThirdPlatform_XWPay
{

	protected function _do($platInfo = array())
	{
		$content = file_get_contents("php://input");
		if ($content) {
			$_REQUEST = json_decode($content, true);
		}

		//异步处理如下… 
		//组合成请求参数
		$arrData = [
			'merchantCode' => $_REQUEST['merchantCode'],
			'content' => $_REQUEST['content'],
		];

		//申请响应结果如下…
		//1.解密：用商私钥 得到解密的数组 $decrypted
		$decrypted = $this->PrivateDecrypt($arrData['content']);

		//2.验签：用平台公钥 得到 验签结果
		$arrDecrypt = json_decode($decrypted, true);
		$natiVerify = [
			'merchantCode' => $arrDecrypt['merchantCode'],
			'merchantTradeNo' => $arrDecrypt['merchantTradeNo'],
			'tradeNo' => $arrDecrypt['tradeNo'],
			'amount' => $arrDecrypt['amount'],
			'tradeStatus' => $arrDecrypt['tradeStatus'],
			'notifyTime' => $arrDecrypt['notifyTime'],
		];

		ksort($natiVerify);
		$strVer = '';
		foreach ($natiVerify as $value) {
			if (!$value) {
				continue;
			}
			$strVer .= $value;
		}
		$verify = $this->veritySign($strVer, $arrDecrypt['sign']);
		$result = ($verify == 1 ? 'success' : 'false');
		if ($result != 'success' || $natiVerify['tradeStatus'] != 'PAYMENT_SUCCESS') {
			echo "验签失败";
			exit;
		} else {
			echo "SUCCESS";
			return array('platOrderId' => $natiVerify['tradeNo'], 'realMoney' => $natiVerify['amount']);
		}
	}
}