<?php 
class Mod_ThirdPlatform_FFlyPay_saoma extends Mod_ThirdPlatform_FFlyPay {
    
    protected function _do($platInfo = array(), $payInfo = array()) {
        
        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }
        $type = $platInfo['platInfo']['pt_id'];
        $payType =  0;
        if ($type == 2) {       // 支付宝wap
            $payType    = 1;
        } elseif ($type == 3) { // 微信wap
            $payType    = 2;
        } elseif ($type == 5) { // 银联扫码
            exit;
        } elseif ($type == 9) { // 支付宝扫码
            $payType    = 1;
        } elseif ($type == 10) { // 微信扫码
            $payType    = 2;
        }
		
        //参与token的参数集合
		$postData = [
		'merchno'    	=> $platInfo['platInfo']['appId'],
		'amount'     	=> $payInfo['amount'],
		'traceno'    	=> $payInfo['pOrderId'],
		'payType'    	=> $payType,
		'notifyUrl'  	=> $platInfo['appInfo']['callbackUrl'] ,
		];
		
		//拼接字符根据提供demo
		$src='';
		ksort($postData);
		foreach ($postData as $x => $x_value){
			if ($x_value != null){
				$src = $src.$x."=".iconv('UTF-8','GBK//IGNORE',$x_value)."&"; //采用本方法进行GBK编码
			}
		}
		//转成MD5
		$md5	= md5($src.$platInfo['platInfo']['appKey']);
		//传值
		$curl 	= curl_init();
		curl_setopt($curl, CURLOPT_URL, $platInfo['platInfo']['p_pay_url']);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $src.'signature'.'='.$md5);
		$res = curl_exec($curl);
		curl_close($curl);
		//获取响应参数
		$data = json_decode(iconv('GBK//IGNORE', 'UTF-8', $res),true);
        //判断
		if ($data['respCode'] != '00' ) {
            echo $data['message'];
            exit;
        }
		//跳转
		header('Location: ' . $data['barCode']);
    }
}
