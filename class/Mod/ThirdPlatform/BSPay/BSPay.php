<?php
class Mod_ThirdPlatform_BSPay extends Mod_ThirdPlatform {

    //获取accessToken
    protected function AcceToken($merchantNo, $key) {
        $url="http://api.huiyuan1688.com.cn/api/v1/getAccessToken/merchant";
        $commonData = array(
            'merchantNo'=>"{$merchantNo}",//商户号
            'nonce' => $this->create_noncestr(16),//随机字符串
            'timestamp' => date("YmdHis"),
            'key' => "{$key}",//密钥

        );

        $params = "merchantNo=".$commonData['merchantNo']."&nonce=".$commonData['nonce']."&timestamp=".$commonData['timestamp']."&key=".$commonData['key'];

        $commonData['sign'] = strtoupper(md5($params));
        $ansData = $this->curl_post($url,$commonData);

        return   $ansData['value']['accessToken'];
    }

    //微信
    protected function submitwx($merchantNo, $key, $amount,$order,$notifyUrl, $payUrl) {
        $url="http://api.huiyuan1688.com.cn/api/v1/order/wechatScan";
        $token=$this->AcceToken($merchantNo, $key);

        $commonData = array(
            "accessToken"=>$token,
            "param"=>array("outTradeNo"=>$order,//订单号
                "money" => $amount,//金额(分)
                "type" => "T1",//付款类型
                "body" => "充值",//商品描述
                "detail"=>"Uline2WX",//商品详情 微信和支付宝用得到通一个借口为好区分
                "notifyUrl"=>"{$notifyUrl}",//后台通知地址
                "productId"=>"1",//商品ID
            ));

        $ansData = $this->curl_post($payUrl,$commonData);
        return $ansData;
    }

    //支付宝
    protected function submitzfb($merchantNo, $key, $amount,$order,$notifyUrl, $payUrl) {
        $url="http://api.huiyuan1688.com.cn/api/v1/order/alipayScan";
        $token=$this->AcceToken($merchantNo, $key);

        $commonData = array(
            "accessToken"=>$token,
            "param"=>array("outTradeNo"=>$order,//订单号
                "money" => $amount,//金额(分)
                "type" => "T1",//付款类型
                "body" => "充值",//商品描述
                "detail"=>"Uline2ZFB",//商品详情 微信和支付宝用得到通一个借口为好区分
                "notifyUrl"=>"{$notifyUrl}",//后台通知地址
                "productId"=>"1",//商品ID
            ));

        $ansData = $this->curl_post($payUrl,$commonData);
        return $ansData;
    }

    //生成随机字符串
    private function create_noncestr($length = 16) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $str = "";
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
            //$str .= $chars[ mt_rand(0, strlen($chars) - 1) ];
        }
        return $str;
    }

    private function curl_post($url,$post_data){
        $headers = array(
            "Content-type: application/json;charset='utf-8'",
            "Accept: application/json",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
        );
        $post_data= json_encode($post_data);
        //var_dump($post_data);
        $curl = curl_init();
        //设置提交的url
        curl_setopt($curl, CURLOPT_URL, $url);
        //设置头文件的信息作为数据流输出
        curl_setopt($curl, CURLOPT_HEADER, 0);
        //设置获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        //设置post方式提交
        curl_setopt($curl, CURLOPT_POST, 1);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //设置post数据
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
        //执行命令
        $data = curl_exec($curl);

        //关闭URL请求
        curl_close($curl);
        //获得数据并返回
        return json_decode($data,true);
    }

    //获取客户端IP地址
    private function get_client_ip($type = 0,$adv=false) {
        $type       =  $type ? 1 : 0;
        static $ip  =   NULL;
        if ($ip !== NULL) return $ip[$type];
        if($adv){
            if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $arr    =   explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
                $pos    =   array_search('unknown',$arr);
                if(false !== $pos) unset($arr[$pos]);
                $ip     =   trim($arr[0]);
            }elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
                $ip     =   $_SERVER['HTTP_CLIENT_IP'];
            }elseif (isset($_SERVER['REMOTE_ADDR'])) {
                $ip     =   $_SERVER['REMOTE_ADDR'];
            }
        }elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ip     =   $_SERVER['REMOTE_ADDR'];
        }
        // IP地址合法验证
        $long = sprintf("%u",ip2long($ip));
        $ip   = $long ? array($ip, $long) : array('0.0.0.0', 0);
        return $ip[$type];
    }

}
