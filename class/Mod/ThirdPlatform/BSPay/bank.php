<?php
class Mod_ThirdPlatform_BSPay_bank extends Mod_ThirdPlatform_BSPay {

	protected function _do($platInfo = array(), $payInfo = array()){

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        if ($type == 1) {// 网银
            $payType = 'bank_pay';
        } elseif ($type == 8) {// 银联快捷
            $payType = 'quick_pay';
        } else {
            $payType = 'bank_pay';
        }

        //*
        $data = array(
            "fxid" => $platInfo['platInfo']['appId'], //商户号
            "fxddh" => $payInfo['pOrderId'], //商户订单号
            "fxdesc" => "商品", //商品名
            "fxfee" => $payInfo['amount'], //支付金额 单位元
            "fxattch" => '', //附加信息
            "fxnotifyurl" => $platInfo['appInfo']['callbackUrl'], //异步回调 , 支付结果以异步为准
            "fxbackurl" => $platInfo['appInfo']['clientUrl'], //同步回调 不作为最终支付结果为准，请以异步回调为准
            "fxpay" => $payType, //支付类型 此处可选项以网站对接文档为准 微信公众号：wxgzh   微信H5网页：wxwap  微信扫码：wxsm   支付宝H5网页：zfbwap  支付宝扫码：zfbsm 等参考API
            "fxip" => $this->getClientIP(0, true), //支付端ip地址
            'fxbankcode'=>'',
            'fxfs'=>'',
        );
        $data["XX"] = md5($data["fxid"] . $data["fxddh"] . $data["fxfee"] . $data["fxnotifyurl"] . $platInfo['platInfo']['appKey']); //加密
        $r = $this->getHttpContent($platInfo['platInfo']['p_pay_url'], "POST", $data);
        $backr = $r;
        $r = json_decode($r, true); //json转数组

        //如果转换错误，原样输出返回
        if (empty($r)) {
            BooFile::write('/tmp/kuaifu.log', $backr . "\n\n", 'a');
            exit(print_r($backr));
        }

        //验证返回信息
        if ($r["status"] == 1) {
            BooView::set('payUrl', $r["payurl"]);
            BooView::display('pay/locationPay.html');
            exit();
        } else {
            //echo $r['error'].print_r($backr); //输出详细信息
            echo $r['error']; //输出错误信息
            exit();
        }
        // */

        $time = time();
        $post = BooVar::requestx();
        $data = array();
        $data['appId'] = $post['appId'];
        $data['payType'] = $post['payType'];
        $data['amount'] = $payInfo['amount'];
        $data['orderId'] = $payInfo['orderId'];
        $data['pAutoOrderId'] = $payInfo['pAutoOrderId'];
        $data['pOrderId'] = $payInfo['pOrderId'];
        $data['clientUrl'] = $platInfo['appInfo']['clientUrl'];
        $data['time'] = $time;
        $sign = $this->getSign($data, $platInfo['appInfo']['app_key']);

        BooView::set('appId', $post['appId']);
        BooView::set('payType', $post['payType']);
        BooView::set('amount', $payInfo['amount']);
        BooView::set('orderId', $payInfo['orderId']);
        BooView::set('pOrderId', $payInfo['pOrderId']);
        BooView::set('pAutoOrderId', $payInfo['pAutoOrderId']);
        BooView::set('clientUrl', $platInfo['appInfo']['clientUrl']);
        BooView::set('time', $time);
        BooView::set('sign', $sign);
        BooView::set('bankCodeList', $platInfo['platInfo']['bankCode']);
        BooView::display('pay/bank.html');
        exit;
	}


}
