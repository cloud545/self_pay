<?php
class Mod_ThirdPlatform_BSPay_payCallback extends Mod_ThirdPlatform_BSPay {

	protected function _do($platInfo = array()) {

        $data=file_get_contents('php://input','r');
        $data=json_decode($data,true);

        if($data['success'] == true){
            //返回成功
            $dexcrypt=$platInfo['platInfo']['appKey'];
            $params = "merchantNo=".$data['merchantNo']."&no=".$data['no']."&nonce=".$data['nonce']."&timestamp=".$data['timestamp'];
            $signature = strtoupper(md5($params.'&key='.$dexcrypt));
            if($data['sign'] == $signature){
                echo 'SUCCESS';
                return array('platOrderId' => $data['no'], 'realMoney' => $data['money'] / 100);
            } else {
                //验签失败
                echo "FAIL";
                return false;
            }

        }else{

            echo "FAIL1111";
            return false;
        }

	}

}
