<?php
class Mod_ThirdPlatform_BSPay_saoma extends Mod_ThirdPlatform_BSPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝wap
            $payType = 'alipay_wap';
        } elseif ($type == 3) { // 微信wap
            $payType = 'wechat_wap';
        } elseif ($type == 4) { // qq
            $payType = 'qq_scan';
        } elseif ($type == 5) { // 银联扫码
            $payType = 'union_scan';
        } elseif ($type == 6) { // 京东
            $payType = 'jd_scan';
        } elseif ($type == 9) { // 支付宝扫码
            $payType = 'alipay_scan';
        } elseif ($type == 10) { // 微信扫码
            $payType = 'wechat_scan';
        }

        if($type == 3 || $type == 10){
            $req = $this->submitwx($platInfo['platInfo']['appId'], $platInfo['platInfo']['appKey'], $payInfo['amount'] * 100, $payInfo['pOrderId'], $platInfo['appInfo']['callbackUrl'], $platInfo['platInfo']['p_pay_url']);
            if ($req['success']== true) {
                $codeUrl = $req['value'];
                if ($codeUrl != '') {

                    if ($type == 10) {
                        BooView::set('url', $codeUrl);
                        BooView::display('pay/saoma.html');
                    } else {
                        BooView::set('payUrl', $codeUrl);
                        BooView::display('pay/locationPay.html');
                    }
                } else {
                    echo '无法获得返回的URL';
                }
            } else {
                echo '90000,请求充值失败，请重试，返回：' . json_encode($req);
            }
        } elseif ($type == 2 || $type == 9){

            $req = $this->submitzfb($platInfo['platInfo']['appId'], $platInfo['platInfo']['appKey'], $payInfo['amount'] * 100, $payInfo['pOrderId'], $platInfo['appInfo']['callbackUrl'], $platInfo['platInfo']['p_pay_url']);
            if ($req['success']== true) {
                $codeUrl = $req['value']; //获取二维码链接
                if ($codeUrl != '') {
                    if ($type == 9) {
                        BooView::set('url', $codeUrl);
                        BooView::display('pay/saoma.html');
                    } else {
                        BooView::set('payUrl', $codeUrl);
                        BooView::display('pay/locationPay.html');
                    }

                } else {
                    echo '无法获得返回的URL';
                }
            } else {
                echo '90000,请求充值失败，请重试，返回：' . json_encode($req);
            }
        }

        exit;
    }

}
