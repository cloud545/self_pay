<?php
class Mod_ThirdPlatform_TonghbPay extends Mod_ThirdPlatform {

    private $charset = 'utf8';

    protected function buildRequestForm($payUrl, $para_temp) {

        $sHtml = "正在跳转至支付页面...<form id='submit' name='submit' action='".$payUrl."' method='POST'>";
        foreach($para_temp as $key=>$val){
            if (false === $this->checkEmpty($val)) {
                $val = str_replace("'","&apos;",$val);
                $sHtml.= "<input type='hidden' name='".$key."' value='".$val."'/>";
            }
        }
        //submit按钮控件请不要含有name属性
        $sHtml = $sHtml."<input type='submit' value='ok' style='display:none;''></form>";
        $sHtml = $sHtml."<script>document.forms['submit'].submit();</script>";

        // echo $sHtml;
        return $sHtml;
    }

    /**
     * 生成签名
     * @param  array $params 待签参数
     * @param  string $apikey API秘钥
     * @return string         签名
     */
    public static function generateSign($params,$apikey)
    {
        ksort($params);
        $sign = '';
        foreach ($params as $k => $v) {
            if($v==='' || $v===null){
                continue;
            }
            $sign = $sign . $k . '=' . $v . '&';
        }
        $sign = strtoupper(md5($sign . 'apikey=' . $apikey));
        return $sign;
    }

}
