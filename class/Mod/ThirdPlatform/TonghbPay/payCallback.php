<?php
class Mod_ThirdPlatform_TonghbPay_payCallback extends Mod_ThirdPlatform_TonghbPay {

    protected function _do($platInfo = array()) {
        $postData                   = array();
                //参与验签的参数(空值不参与验签)
        $postData['user_id']               = $_REQUEST['user_id'];              //商户ID
        $postData['trade_no']          = $_REQUEST['trade_no'];                 //订单号
        $postData['out_trade_no']      = $_REQUEST['out_trade_no'];         //商户订单号
        $postData['transaction_money'] = $_REQUEST['transaction_money'];    //订单到账金额
        $postData['datetime']       = $_REQUEST['datetime'];            //交易时间
        $postData['returncode']     = $_REQUEST['returncode'];                  //交易状态      "00"为成功
        $postData['attach']         = $_REQUEST['attach'];                      //扩展返回      商户附加数据返回

        //进行验签
        $checkSign  = $this->generateSign($postData,$platInfo['platInfo']['appKey']);



        $postData['sign']           = $_REQUEST['sign'];                        //签名 
        if ($checkSign == $postData['sign'] &&  $postData['returncode'] == '00'){

            echo "OK";
            return array('platOrderId' => $postData['trade_no'],'realMoney' => $postData['transaction_money'] / 100);
        } else {
            return false;
        }

    }


}

