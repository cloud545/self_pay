<?php
class Mod_ThirdPlatform_WinleePay_payCallback extends Mod_ThirdPlatform_WinleePay
{
    protected function _do($platInfo = array())
    {
        $back_str = file_get_contents("php://input");
        $back_data = json_decode($back_str, true);

        //验签加密
        $postData = array();
        //参与验签的参数
        $postData['amount']         = $back_data['amount'];                         //商户的订单号
        $postData['datePaid']       = $back_data['datePaid'];                         //商户的userId
        if($back_data['ok'] == true)
        {
            $postData['ok'] = 'true';
        }else{
            $postData['ok'] = 'false';
        }
        $postData['orderNo']        = $back_data['orderNo'];                     //订单金额
        $postData['payStatus']      = $back_data['payStatus'];                      //交易支付方式
        $postData['referenceCode']  = $back_data['referenceCode'];                  //支付时间
        $postData['sn']             = $back_data['sn'];                     //支付时间
        $postData['type']           = $back_data['type'];                     //支付时间


        $strTem = "amount=".$postData['amount']."&datePaid=".$postData['datePaid']."&ok=".$postData['ok']."&orderNo=".$postData['orderNo']."&payStatus=".$postData['payStatus']."&referenceCode=".$postData['referenceCode']."&sn=".$postData['sn']."&type=".$postData['type'].$platInfo['platInfo']['appKey'];
        $checkSign = strtoupper(md5($strTem));

        $postData["sign"] = $back_data['sign'];

        if ($checkSign == $postData['sign'] && $back_data['ok'] == true) {
            echo "success";
            return array('platOrderId' => $postData['sn'], 'realMoney' => $postData['amount'] / 100 );
        } else {
            echo "error";
            return false;
        }

    }
}