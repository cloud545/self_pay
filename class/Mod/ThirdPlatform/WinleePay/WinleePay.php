<?php
class Mod_ThirdPlatform_WinleePay extends Mod_ThirdPlatform {

    public function fixFormat(&$body) {
        // Date Format Issues
        if(isset($body["datePaid"]) && !strpos($body["datePaid"],"T") ) {
            $body["datePaid"] = str_replace(" ","T",$body["datePaid"]);
            $body["datePaid"] =$body["datePaid"]."Z";
        }
    }

    public function wlfSign($body,$secret) {
        $keys = array_keys($body);
        ksort($keys);
        $map = function ($key) use ($body) {
            if(!isset($body[$key]) || $body[$key] == NULL || $body[$key] == "") {
                return "";
            }
            if(is_object($body[$key])) {
                return $key."=".json_encode($body[$key]);
            }
            return $key."=".$body[$key];
        };
        $str = implode(array_map($map,$keys),"&").$secret;
        //print_r($str.'<br/>');
        return md5($str);

    }


}
