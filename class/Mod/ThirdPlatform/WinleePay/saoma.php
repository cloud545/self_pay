<?php
class Mod_ThirdPlatform_WinleePay_saoma extends Mod_ThirdPlatform_WinleePay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];

        $payType =  0;
        if ($type == 2) {       // 支付宝wap
            $payType = 'ALIPAY-QR';
        } elseif ($type == 3) { // 微信wap
            $payType = 'WECHATPAY-QR';
        } elseif ($type == 5) { // 银联扫码
            $payType    = '300';
        } elseif ($type == 9) { // 支付宝扫码
            $payType        = 'ALIPAY-QR';
        } elseif ($type == 10) { // 微信扫码
            $payType        = 'WECHATPAY-QR';
        }

        //参与验签的数据
        $postData = [
            'referenceCode' => $platInfo['platInfo']['appId'],                                 //商户号，由我司分配
            'orderNo'       => $payInfo['pOrderId'],                                           //订单号，
            'amount'        => $payInfo['amount'] * 100 ,                  						//交易金额：分。10元表示为1000
            'notifyUrl'     => $platInfo['appInfo']['callbackUrl'],                                  //结果通知地址。当交易成功或失败时会调⽤此 URL
            'paymentType'   => $payType,                                                    //付款类型标识符。可在后台查看拥有的付款类型
        ];

        //按字段的字⺟顺序从到排序
        ksort($postData);
        //验签加密
        ksort($postData);
        $strTem = "amount=".$postData['amount']."&notifyUrl=".$postData['notifyUrl']."&orderNo=".$postData['orderNo']."&paymentType=".$postData['paymentType']."&referenceCode=".$postData['referenceCode'].$platInfo['platInfo']['appKey'];

        $postData["sign"] = strtoupper(md5($strTem));

        BooCurl::setData(json_encode($postData), 'POST');
        BooCurl::setOption(CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json;charset=UTF-8',
        ));
        $res = BooCurl::call($platInfo['platInfo']['p_pay_url']);
        $data = json_decode($res,true);

        if ($data['ok'] != true) {
            echo $data['msg'];
            exit;
        }

		if($platInfo['platInfo']['platId'] == 159 || $platInfo['platInfo']['platId'] == 160 || $platInfo['platInfo']['platId'] == 163 || $platInfo['platInfo']['platId'] == 164){
		   BooView::set('payUrl', $data['url']);
		   BooView::display('pay/locationPay.html');
		}else{
		   BooView::set('qrCode', $data['qrCode']);
		   BooView::set('url', $data['url']);
		   BooView::set('type', $type);
		   BooView::display('pay/saomaOrUrl.html');
		}
	   exit;


    }
}

