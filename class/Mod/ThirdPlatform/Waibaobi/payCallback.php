<?php
class Mod_ThirdPlatform_Waibaobi_payCallback extends Mod_ThirdPlatform_Waibaobi {

	protected function _do($platInfo = array()) {

        $native = array(
            'income' => $_REQUEST['income'],
            'payOrderId' => $_REQUEST['payOrderId'],
            'mchId' => $_REQUEST['mchId'],
            'appId' => $_REQUEST['appId'],
            'productId' => $_REQUEST['productId'],
            'mchOrderNo'=>$_REQUEST['mchOrderNo'],
            'amount' => $_REQUEST['amount'],
            'status' => $_REQUEST['status'],
            'channelOrderNo' => $_REQUEST['channelOrderNo'],
            'channelAttach' => $_REQUEST['channelAttach'],
            'param1' => $_REQUEST['param1'],
            'param2' => $_REQUEST['param2'],
            'paySuccTime' => $_REQUEST['paySuccTime'],
            'backType' => $_REQUEST['backType'],
        );

        ksort($native);

        $md5Str = '';
        foreach ($native as $key => $value) {

            if (!$value) {
                continue;
            }

            if (!$md5Str) {
                $md5Str = "{$key}={$value}";
            } else {
                $md5Str .= "&{$key}={$value}";
            }
        }

        $checkSign = strtoupper(md5($md5Str . "&key={$platInfo['platInfo']['appSecret']}"));
        if ($_REQUEST['sign'] == $checkSign && $_REQUEST['status'] == 2) {
            echo 'success';
            return array('platOrderId' => $_REQUEST['payOrderId'],'realMoney' => $_REQUEST["amount"] / 100);
        }

        return false;
	}

}
