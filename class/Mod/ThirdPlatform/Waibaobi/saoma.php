<?php
class Mod_ThirdPlatform_Waibaobi_saoma extends Mod_ThirdPlatform_Waibaobi {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = 0;
        if ($type == 2) { // 支付宝
            $payType = 8023;
        } elseif ($type == 3) { // 微信
            $payType = 'wechat';
        } elseif ($type == 5) { // 银联扫码
            $payType = 'UnionCloudPay';
        } elseif ($type == 9) { // 支付宝扫码
            $payType = 8023;
        } elseif ($type == 10) { // 微信扫码
            $payType = 'wechat';
        }

        $native = array(
            'mchId'=>$platInfo['platInfo']['appId'],
            'appId' => $platInfo['platInfo']['appKey'],
            'productId' => $payType,
            'mchOrderNo'=>$payInfo['pOrderId'],
            'currency' => 'cny',
            'amount' => $payInfo['amount'] * 100,
            'notifyUrl' => $platInfo['appInfo']['callbackUrl'],
            'subject'=> '商品',
            'body'=> '商品',
        );

        ksort($native);

        $md5Str = '';
        foreach ($native as $key => $value) {

            if (!$value) {
                continue;
            }

            if (!$md5Str) {
                $md5Str = "{$key}={$value}";
            } else {
                $md5Str .= "&{$key}={$value}";
            }
        }

        $sign = strtoupper(md5($md5Str . "&key={$platInfo['platInfo']['appSecret']}"));
        $native["sign"] = $sign;

        BooCurl::setData($md5Str . "&sign={$sign}", 'POST');
        $response = BooCurl::call($platInfo['platInfo']['p_pay_url']);
        $responseData = json_decode($response, true);
        if (!$responseData || $responseData['retCode'] != 'SUCCESS') {
            BooFile::write('/tmp/odppay.log', $response . "\n{$payInfo['amount']}\n{$platInfo['appInfo']['name']}\n111\n\n", 'a');
            echo $response;
            exit;
        }

        BooView::set('payUrl', $responseData['payParams']['payUrl']);
        BooView::display('pay/locationPay.html');
        exit;
    }

}
