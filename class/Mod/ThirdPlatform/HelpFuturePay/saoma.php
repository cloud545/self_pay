<?php
class Mod_ThirdPlatform_HelpFuturePay_saoma extends Mod_ThirdPlatform_HelpFuturePay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();

        $payType = '';
        if ($type == 2) { // 支付宝
            $payType = 'b';
        } elseif ($type == 3) { // 微信
            $payType = 'c';
        } elseif ($type == 4) { // qq
            $payType = '';
        } elseif ($type == 5) { // 银联
            $payType = 'd';
        }

        $post = BooVar::requestx();

        $info = array(
            'command' => 'beginpay',
            'merchantid' => $platInfo['platInfo']['appId'],
            'amount' => $payInfo['amount'],
            'tradeno' => $payInfo['pOrderId'],
            'frontjumpurl' => $post['clientUrl'],
            'paytype' => $payType,
        );
        ksort($info);

        $httpQueryStr = '';
        foreach ($info as $key => $val) {
            if (!$httpQueryStr) {
                $httpQueryStr = "{$key}={$val}";
            } else {
                $httpQueryStr = "&{$key}={$val}";
            }
        }

        $sign = strtoupper(md5($httpQueryStr . $platInfo['platInfo']['appKey']));
        $info['sign'] = $sign;

        BooCurl::setData($info, 'POST');
        $data = BooCurl::call($platInfo['platInfo']['p_pay_url']);
        $return = json_decode($data,true);

        if(is_array($return) && $return['ret'] == 'success'){
            header('Location: ' . $return['payweblink']);
            exit;
        }
        
        echo "支付异常，请联系客服! error: {$return['info']}";
        exit;
    }

}
