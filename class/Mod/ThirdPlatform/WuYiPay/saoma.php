<?php
class Mod_ThirdPlatform_WuYiPay_saoma extends Mod_ThirdPlatform_WuYiPay {

    protected function _do($platInfo = array(), $payInfo = array()) {

        if (!$platInfo || !$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $type = $platInfo['platInfo']['pt_id'];
        $isMobile = BooMobileDetect::getInstance()->isMobile();
        $payType = 0;

        if ($type == 9) {
            $payType = 'ALIPAY';
        }
        $data = array(
            'partner_id' => $platInfo['platInfo']['appId'],
            'out_trade_no' => $payInfo['pOrderId'],
            'goods_name' => 'IPhone手机',
            'attach' => 'IPhone手机',
            'scenetype' => '103',
            'pay_method' => $payType,
            'order_time' => date('Y-m-d H:i:s', time()),
            'order_ip' =>  $platInfo['appInfo']['last_ip'],
            'total_fee' => number_format($payInfo['amount'],'2','.',''),
            'notifyurl' => $platInfo['appInfo']['callbackUrl'],
        );
        ksort($data);
        $md5str = '';
        foreach ($data as $key => $value) {
            $md5str = $md5str . $key . '=' . $value .'&';
        }
        $data['sign'] = md5($md5str.'key='.$platInfo['platInfo']['appKey']);
        $data = http_build_query(
            $data
        );
        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => $data
            )
        );
        $context = stream_context_create($opts);
        $result = file_get_contents($platInfo['platInfo']['p_pay_url'], false, $context);
        $result = json_decode($result,1);
        if ($result['Code'] == 1) {
            header('location:'.$result['Data']);
        } else {
            echo $result['ErrorMsg'];
        }
    }
}
