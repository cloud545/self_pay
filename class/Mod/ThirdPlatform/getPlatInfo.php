<?php
class Mod_ThirdPlatform_getPlatInfo extends Mod_ThirdPlatform {

	protected function _do($appId = 0, $payType = 0, $platId = 0, $payInfo = array()) {

        if (!$appId || !$payType) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $appObj = BooController::get('Obj_App_Info');
        $appInfo = $appObj->getInfoByAppId($appId);
        if (!$appInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::APP_NOT_EXIST);
        }

        if (!$appInfo['plat_app_info']) {
            Common_errorCode::jsonEncode(Common_errorCode::NO_SETTING_PAY_PLAT);
        }

        // 判断是否使用平台独有信息
        $platInfo = array();
        if ($platId) {

            $platAppInfo = json_decode($appInfo['plat_app_info'], true);
            $platInfo = $platAppInfo[$platId];

            $platObj = BooController::get('Obj_Plat_Info');
            $tmpPlatInfo = $platObj->getInfoById($platId);
            if (!$tmpPlatInfo) {
                Common_errorCode::jsonEncode(Common_errorCode::NO_SETTING_PAY_PLAT);
            }

            if (!$tmpPlatInfo['p_app_id'] && !$tmpPlatInfo['p_app_key'] && !$tmpPlatInfo['p_app_secret']) {
                Common_errorCode::jsonEncode(Common_errorCode::NO_SETTING_PAY_PLAT);
            }

            $platInfo['platTag'] = $tmpPlatInfo['p_tag'];
            $platInfo['payType'] = $tmpPlatInfo['pt_id'];
            $platInfo['point'] = $payInfo['current_point'];

            $platInfo = array_merge($platInfo, $tmpPlatInfo);
            if ($tmpPlatInfo['p_bank_code']) {
                $platInfo['bankCode'] = json_decode($tmpPlatInfo['p_bank_code'], true);
            } else {
                $platInfo['bankCode'] = '';
            }

            $platInfo['platId'] = $platId;
            $platInfo['appId'] = $tmpPlatInfo['p_app_id'];
            $platInfo['appKey'] = $tmpPlatInfo['p_app_key'];
            $platInfo['appSecret'] = $tmpPlatInfo['p_app_secret'];

        } else {

            $platAppInfo = json_decode($appInfo['plat_app_info'], true);
            foreach ($platAppInfo as $tmpPlatId => $info) {
                if ($info['payType'] == $payType) {
                    $platInfo = $info;

                    $platObj = BooController::get('Obj_Plat_Info');
                    $tmpPlatInfo = $platObj->getInfoById($tmpPlatId);
                    if (!$tmpPlatInfo) {
                        Common_errorCode::jsonEncode(Common_errorCode::NO_SETTING_PAY_PLAT);
                    }

                    if (!$tmpPlatInfo['p_app_id'] && !$tmpPlatInfo['p_app_key'] && !$tmpPlatInfo['p_app_secret']) {
                        Common_errorCode::jsonEncode(Common_errorCode::NO_SETTING_PAY_PLAT);
                    }

                    $platInfo = array_merge($platInfo, $tmpPlatInfo);
                    if ($tmpPlatInfo['p_bank_code']) {
                        $platInfo['bankCode'] = json_decode($tmpPlatInfo['p_bank_code'], true);
                    } else {
                        $platInfo['bankCode'] = '';
                    }

                    $platInfo['platId'] = $tmpPlatId;
                    $platInfo['appId'] = $tmpPlatInfo['p_app_id'];
                    $platInfo['appKey'] = $tmpPlatInfo['p_app_key'];
                    $platInfo['appSecret'] = $tmpPlatInfo['p_app_secret'];
                }
            }
        }

        if (!$platInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PAY_TYPE_NO_OPEN);
        }

        return array('appInfo' => $appInfo, 'platInfo' => $platInfo);
	}
}