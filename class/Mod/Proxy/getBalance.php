<?php
class Mod_Proxy_getBalance extends Mod_Proxy {

    protected function _do($proxyId = 0){

        $startTime = date('Y-m-d') . " 00:00:00";
        $endTime = date('Y-m-d H:i:s');
        $yestodayDate = date('Y-m-d', strtotime($startTime) - 86400);

        $orderObj = BooController::get('Obj_Proxy_Orders');
        $payObj = BooController::get('Obj_Proxy_Pay');
        $withdrawalObj = BooController::get('Obj_Proxy_Withdrawal');
        $reportObj = BooController::get('Obj_Proxy_Report');
        $balanceObj = BooController::get('Obj_Proxy_Balance');

        $yestodayRsInfo = $reportObj->getInfo($proxyId, $yestodayDate);
        if (!$yestodayRsInfo) {
            //continue;
        }

        $paySumInfo = $payObj->getSum($proxyId, 0, 0, 0, $startTime, $endTime);
        $withdrawalSumInfo = $withdrawalObj->getSum($proxyId, 2, $startTime, $endTime);

        // 管理员添加资金
        $orderSumInfo4 = $orderObj->getSum($proxyId, 4, $startTime, $endTime);
        // 管理员扣减资金
        $orderSumInfo5 = $orderObj->getSum($proxyId, 5, $startTime, $endTime);
        // 管理员冻结资金
        //$orderSumInfo6 = $orderObj->getSum($proxyId, 6, $startTime, $endTime);
        // 管理员解冻资金
        $orderSumInfo7 = $orderObj->getSum($proxyId, 7, $startTime, $endTime);

        $delBalance = $orderSumInfo4['sumAmount'] + abs($orderSumInfo7['sumAmount']) + $orderSumInfo5['sumAmount'];// - $orderSumInfo6['sumAmount'];

        $proxyBalanceInfo = $balanceObj->getInfoByProxyId($proxyId);
        $balance = $yestodayRsInfo['balance'] + $paySumInfo['sumFee'] - $withdrawalSumInfo['sumAmount'] - $withdrawalSumInfo['sumFee'] + $delBalance - $proxyBalanceInfo['frozen_amount'] - $proxyBalanceInfo['frozen_balance'];

        if ($balance != $proxyBalanceInfo['available_balance']) {
            $updateData = array();
            $updateData['available_balance'] = $balance;
            $updateData['cash_balance'] = $balance + $proxyBalanceInfo['frozen_amount'] + $proxyBalanceInfo['frozen_balance'];
            $updateData['last_modify_time'] = $endTime;
            $rs = $balanceObj->update($proxyBalanceInfo['id'], $updateData);
            if (!$rs) {
                return false;
            }
        }

        return $balance;
    }
    
}