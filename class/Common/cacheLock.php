<?php
class Common_cacheLock extends Common {

    public static function set($key, $value, $time = 0) {

        if (!$key) {
            return false;
        }

        $cacheDao = Dao_Redis_Default::getInstance();

        $incrKey = "{$key}_incr";
        $incrValue = $cacheDao->get($incrKey);
        $keyValue = $cacheDao->get($key);
        if ($keyValue) {
            return false;
        }

        if ($time) {
            $cacheDao->set($key, $value, $time);
        } else {
            $cacheDao->set($key, $value);
        }

        $incrValue2 = $cacheDao->incr($incrKey);

        if ($incrValue2 == $incrValue + 1) {
            if ($incrValue2 >= 1000000) {
                $cacheDao->set($incrKey, 0);
            }
            return true;
        }

        return false;
    }

    public static function get($key) {

        if (!$key) {
            return false;
        }

        $cacheDao = Dao_Redis_Default::getInstance();
        $keyValue = $cacheDao->get($key);

        return $keyValue;
    }

    public static function del($key) {

        if (!$key) {
            return false;
        }

        $incrKey = "{$key}_incr";

        $cacheDao = Dao_Redis_Default::getInstance();
        // 删除锁
        $cacheDao->del($key);
        $cacheDao->del($incrKey);

        return true;
    }

}