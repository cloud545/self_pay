<?php
class Common extends BooObj{
	
	public function __construct() {
	}
	
	public function run() {
		$args = func_get_args();
		return call_user_func_array(array($this, "_do"), $args);
	}
	
	//子类要重载这个方法
	protected function _do() {
		return array();
	}
}