<?php
class Common_numberAndStrSwap extends Common {

    /**
     * 数字和字符串互转
     *
     * @param string $mFrom
     * @param string $sType  数字转字符串：encode，字符串转数字：decode
     * @return string
     */
	protected function _do($mFrom = '', $sType = 'encode') {

        $aStr = array(
            '0' => 'o',
            '1' => 'p',
            '2' => 'b',
            '3' => 'd',
            '4' => 't',
            '5' => 'v',
            '6' => 'a',
            '7' => 'u',
            '8' => 'i',
            '9' => 'w',
        );

        if($sType == 'decode'){
            $aStr = array_flip($aStr);
        }

        $sStr = '';
        $aData = str_split($mFrom);
        foreach ($aData as $val) {
            $sStr .= $aStr["{$val}"];
        }

        return $sStr;
	}

}