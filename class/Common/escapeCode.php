<?php
class Common_escapeCode extends Common {
	
	//子类要重载这个方法
	protected function _do($sString, $type = 'encode') {

		if ($type == 'decode') {
            $aData = explode("Q", $sString);

            if (!isset($aData[1])) {
                return 0;
            }

            for ($i = 0; $i < strlen($aData[1]); $i++) {
                $ascii = ord($aData[1][$i]);
                if ( $ascii > 74 ) {
                    $ascii = 42;
                } else {
                    $ascii -= 17;
                }

                $aData[1][$i] = chr($ascii);
            }
            $aData[1] = str_replace("*", "", $aData[1]);

            return is_numeric($aData[1]) ? $aData[1] : 0;
		} else {

            $tmp = explode("-", $sString);
            $tmp2 = array_pop($tmp);
            $aData = array(implode('-', $tmp), $tmp2);

            if (!isset($aData[1])) {
                return $aData;
            }

            for ($i = strlen($aData[1]); $i <= 8; $i++) {
                $aData[1] .= "*";
            }

            $zero = 0;
            for($i = 0; $i < strlen($aData[1]); $i++) {
                $ascii = ord($aData[1][$i]);
                if ( $ascii == 42 ) {
                    $ascii += $zero + 33;
                    $zero++;
                } else {
                    $ascii += 17;
                }
                $aData[1][$i] = chr($ascii);
            }

            return $aData[0] . "Q" . $aData[1];
        }
	}

}