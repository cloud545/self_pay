<?php

class Common_errorCode extends Common {

    const SUCCESS = 200;//成功
    const FAIL = 1001;//失败
    const NO_OPEN = 1002;//功能暂未开放
    const NO_DATA = 1003;//没有数据
    const PARAM_ERR = 1004;//参数错误
    const SIGN_ERR = 1005;//sign验证失败
    const USERNAME_ERR = 1006;//用户名错误
    const PASSWORD_ERR = 1007;//密码错误
    const VERIFY_CODE_ERR = 1008;//验证码错误
    const USER_NOT_EXIST = 1009;//用户不存在
    const USER_LOCKED = 1010;//用户被锁定
    const USER_IS_EXIST = 1011;//用户已存在
    const PASSWORD_DIFFERENT = 1012;//两次密码不相同
    const USERNAME_EMPTY = 1013;//用户名不能为空
    const PASSWORD_EMPTY = 1014;//密码不能为空
    const VERIFY_CODE_EMPTY = 1015;//验证码不能为空
    const PARAM_LOST = 1016;//缺少参数
    const MENU_NOT_EXIST = 1017;//菜单不存在
    const MENU_HAVE_CHILDREN_MENU = 1018;//该菜单有下级子菜单
    const MENU_CHILDREN_IS_MAX = 1019;//目前最大支持三级菜单
    const SEARCH_MAX_DAY_30 = 1020;//最多只能查询30天的数据
    const SEARCH_MAX_DAY_7 = 1021;//最多只能查询7天的数据
    const APP_NOT_EXIST = 1022;// 该应用不存在
    const INSERT_USER_FAIL = 1023;// 注册用户失败
    const USER_NO_INSERT = 1024;// 该用户未注册
    const INSERT_ERR = 1025;// 添加失败
    const UPDATE_ERR = 1026;// 更新失败
    const DELETE_ERR = 1027;// 删除失败
    const NO_DATA_UPDATE = 1028;// 没有数据需要更新
    const OLD_NEW_PASSWORD_SAME = 1029;//新密码与旧密码相同
    const PASSWORD_NO_MATCH_RULE = 1030;//密码不符合规则
    const OLD_PASSWORD_IS_ERR = 1031;//旧密码错误
    const TRANSFER_FAIL = 1032;//转账失败
    const APP_IS_LOCK = 1033;//应用已被限制访问
    const THIRD_PLAT_KEY_EMPTY = 1034;//未配置平台key等信息，请联系工作人员
    const API_REQUEST_OFTEN = 1035;//请求过于频繁
    const CASH_IS_NOT_ENOUGH = 1036;//余额不足
    const FROZEN_BALANCE_IS_NOT_ENOUGH = 1136;//解冻金额不能超过冻结金额
    const TRANSFER_CASH_IS_POWER = 1037;//转账金额超过限定金额
    const WITHDRAW_LESS_LIMIT = 1038;//提现金额不能少于最低限制
    const CASH_LOCK_FAIL = 1039;//资金锁定失败
    const CASH_UNLOCK_FAIL = 1040;//资金解锁失败
    const CASH_LOCKED = 1041;//资金已经被锁定
    const HAVE_CASH_IN_WITHDRAW = 1042;//当前有资金正在提款中
    const SEARCH_MAX_DAY_3 = 1043;//最多只能查询3天的数据
    const USER_UNLOCK_SELF = 1044;//不能锁定自己
    const USERNAME_IS_ADMIN_UNUPDATE = 1045; //不能修改admin账号信息
    const ONLY_SELECT_SAME_MONTH = 1046; // 每次只能查同一月份的数据
    const ORDER_NOT_EXIST = 1047;//订单不存在
    const ACTIVE_USER_FAIL = 1048;// 激活用户失败
    const OPEN_GAME_FAIL = 1049;// 打开游戏失败
    const NAME_IS_EXIST = 1050;//账号已存在
    const TOKEN_IS_NULL = 1051;//获取TOKEN失败
    const ORDER_ING = 1052;//当前有未处理完的订单
    const LONG_NO_OPERATE = 1053;//长时间未操作请刷新重新登录
    const NO_SETTING_PAY_PLAT = 1054;//该应用未设定支付渠道，请联系客服
    const PAY_FAIL = 1055;//支付失败
    const PAY_IS_END = 1056;//订单已完成
    const NAME_NOT_MATCH_THE_RULE = 1057;//账号不符合规则，账号由字母数字(6-16位构成)
    const PASSWORD_NOT_MATCH_THE_RULE = 1058;//密码不符合规则，密码由字母数字混合构成，且连续三位不能为相同的字符，长度为6~16位
    const POINT_IS_LOW = 1059;//点数不能比通道的点数低
    const POINT_IS_ZERO = 1060;//点数不能小于等于0
    const POINT_IS_WRONG = 1160;//点数不符合规则
    const GOOGLE_CODE_IS_ERROR = 1061;//谷歌验证码错误
    const CARD_NO_INFO_IS_LESS = 1062;//请先完善银行卡信息
    const SAFE_ANSWER_IS_ERROR = 1063;//安全问题答案错误
    const HAVE_SET_PWD = 1064;//已设置密码，不能再设置
    const BANK_CARD_IS_EXIST = 1065;//该银行卡已存在
    const DEFAULT_BANK_CARD_UNDEL = 1066;//默认银行卡不能删除
    const DEFAULT_BANK_NOT_EXIST = 1067;//银行卡不存在
    const DEFAULT_BANK_NOT_CANCEL = 1068;//不能把默认银行卡关闭
    const CASH_PASSWORD_IS_LESS = 1069;//请先设置资金密码
    const SAFE_ANSWER_IS_LESS = 1070;//安全问题错误
    const NO_POWER = 1071;//无权访问
    const QRCODE_IS_EXIST = 1072;//该二维码已经存在
    const NOW_NO_CAN_USE_PAY = 1073;//当前无可用的支付渠道
    const APP_ORDER_ID_IS_EXIST = 1074;// 订单号已经存在，不能重复下单
    const FA_IS_WITHDRAWAL_BUSY = 1075;// 结算中，请稍等5秒再进行
    const QRCODE_IS_INVALID = 1076;//二维码已经失效，请重新下单
    const ALIPAY_LIMIT_NUMBER = 1077;//支付金额不付，单笔支付金额范围 100-4999 元
    const WECHAT_LIMIT_NUMBER = 1078;//支付金额不付，单笔支付金额范围 100-10000 元
    const MERCHANT_SCORE_LESS = 1079;//收款码方余额不足
    const PAY_TIME_OUT = 1080;//订单已失效，请重新下单
    const ORDER_NO_PAY = 1081;//该订单未支付
    const ONLY_ONE_PAY_TYPE = 1082;//每种支付方式只能选择一个支付渠道
    const WITHDRAWAL_IS_END = 1083;//该笔提款已经处理过
    const PAY_TYPE_NO_OPEN = 1084;//该支付通道未分配
    const NETWORK_FLUCTUATION = 1085;//网络出现波动，请重新请求
    const ACCOUNT_NOT_EXIST = 1086;//收款账号不存在
    const SYSTEM_DAY_SETTLE_ACCOUNTS = 1087;//每日 00:00:00 到 00:00:10 系统在结算，无法进行提款
    const MERCHANT_CASH_IS_NOT_ENOUGH = 1088;//商户余额不足
    const PROXY_CASH_IS_NOT_ENOUGH = 1089;//代理余额不足
    const ALREADY_DOING = 1090;//对不起宝贝们，这单已被其他操作员截获，请刷新等待..
    const AMOUNT_IS_WRONG = 1100;//提现金额范围是10000~49999
    const WITHDRAWAL_IS_PROCESSING = 1101;//银行受理中
    const WITHDRAWAL_IS_REJECTION = 1102;//订单被驳回
    const NOT_RIGHT_MONEY = 1103;//请按指定金额进行充值
    const AMOUNT_IS_POWER = 1104;//提现金额超过限定金额
    const NOW_NO_CAN_USE_QRCODE = 9997;//当前所有的收款码正在使用中，请等待
    const REQUEST_ERR = 9998;//非法请求
    const ERR_CODE_UNDEFINED = 9999;//错误码未定义
    const PASSWORD_SAFE_ERR = 2001;//资金安全密码错误
    const ACCOUNT_LOGIN_OTHER_DEVICE = 2002;//该账号在其他地方登录,请重新登录
    const AGENT_HAS_NOT_THIS_PLAT = 2003;//该商户的代理未配置这个通道
    const AGENT_POINT_IS_LOW = 2004;//该商户的点数不能比代理的点数低

    public static $msg = array(
        self::ACCOUNT_LOGIN_OTHER_DEVICE=>'该账号在其他地方登录,请重新登录',
        self::SUCCESS => '成功',
        self::FAIL => '失败',
        self::NO_OPEN => '功能暂未开放',
        self::NO_DATA => '没有数据',
        self::USERNAME_EMPTY => '用户名不能为空',
        self::PASSWORD_EMPTY => '密码不能为空',
        self::VERIFY_CODE_EMPTY => '验证码不能为空',
        self::USERNAME_ERR => '用户名错误',
        self::PASSWORD_ERR => '密码错误',
        self::PASSWORD_SAFE_ERR => '资金安全密码错误',
        self::VERIFY_CODE_ERR => '验证码错误',
        self::USER_NOT_EXIST => '用户不存在',
        self::USER_LOCKED => '用户被锁定',
        self::USER_IS_EXIST => '用户已存在',
        self::PASSWORD_DIFFERENT => '两次密码不相同',
        self::PARAM_LOST => '缺少参数',
        self::MENU_NOT_EXIST => '菜单不存在',
        self::MENU_HAVE_CHILDREN_MENU => '该菜单有下级子菜单',
        self::MENU_CHILDREN_IS_MAX => '目前最大支持三级菜单',
        self::SEARCH_MAX_DAY_30 => '最多只能查询30天的数据',
        self::SEARCH_MAX_DAY_7 => '最多只能查询7天的数据',
        self::PARAM_ERR => '参数错误',
        self::SIGN_ERR => 'sign验证失败',
        self::APP_NOT_EXIST => '该应用不存在',
        self::INSERT_USER_FAIL => '注册用户失败',
        self::USER_NO_INSERT => '该用户未注册',
        self::INSERT_ERR => '添加失败',
        self::UPDATE_ERR => '更新失败',
        self::DELETE_ERR => '删除失败',
        self::NO_DATA_UPDATE => '没有数据需要更新',
        self::OLD_NEW_PASSWORD_SAME => '新密码与旧密码相同',
        self::PASSWORD_NO_MATCH_RULE => '密码不符合规则',
        self::OLD_PASSWORD_IS_ERR => '旧密码错误',
        self::TRANSFER_FAIL => '转账失败',
        self::APP_IS_LOCK => '应用已被限制访问',
        self::THIRD_PLAT_KEY_EMPTY => '未配置平台key等信息，请联系工作人员',
        self::API_REQUEST_OFTEN => '请求过于频繁',
        self::CASH_IS_NOT_ENOUGH => '余额不足',
        self::FROZEN_BALANCE_IS_NOT_ENOUGH => '解冻金额不能超过冻结金额',
        self::TRANSFER_CASH_IS_POWER => '转账金额超过限定金额',
        self::WITHDRAW_LESS_LIMIT => '提现金额不能少于最低限制',
        self::CASH_LOCK_FAIL => '资金锁定失败',
        self::CASH_UNLOCK_FAIL => '资金解锁失败',
        self::CASH_LOCKED => '资金已经被锁定',
        self::HAVE_CASH_IN_WITHDRAW => '当前有资金正在提款中',
        self::SEARCH_MAX_DAY_3 => '最多只能查询3天的数据',
        self::USER_UNLOCK_SELF => '不能锁定自己',
        self::USERNAME_IS_ADMIN_UNUPDATE => '不能修改admin账号信息',
        self::ONLY_SELECT_SAME_MONTH => '每次只能查同一月份的数据',
        self::ORDER_NOT_EXIST => '订单不存在',
        self::ACTIVE_USER_FAIL => '激活用户失败',
        self::OPEN_GAME_FAIL => '打开游戏失败',
        self::NAME_IS_EXIST => '账号已存在',
        self::TOKEN_IS_NULL => '获取TOKEN失败',
        self::ORDER_ING => '当前有未处理完的订单',
        self::LONG_NO_OPERATE => '长时间未操作请刷新重新登录',
        self::NO_SETTING_PAY_PLAT => '该应用未设定支付渠道，请联系客服',
        self::PAY_FAIL => '支付失败',
        self::PAY_IS_END => '订单已完成',
        self::NAME_NOT_MATCH_THE_RULE => '账号不符合规则，账号由字母数字(6-16位构成)',
        self::PASSWORD_NOT_MATCH_THE_RULE => '密码不符合规则，密码由字母数字混合构成，且连续三位不能为相同的字符，长度为6~16位',
        self::POINT_IS_LOW => '点数不能比通道的点数低',
        self::POINT_IS_ZERO => '点数不能小于等于0',
        self::AGENT_HAS_NOT_THIS_PLAT => '该商户的代理未配置这个通道',
        self::AGENT_POINT_IS_LOW => '该商户的点数不能比代理的点数低',
        self::POINT_IS_WRONG => '点数不符合规则',
        self::GOOGLE_CODE_IS_ERROR => '谷歌验证码错误',
        self::CARD_NO_INFO_IS_LESS => '请先完善银行卡信息',
        self::SAFE_ANSWER_IS_ERROR => '安全问题答案错误',
        self::HAVE_SET_PWD => '已设置密码，不能再设置',
        self::BANK_CARD_IS_EXIST => '该银行卡已存在',
        self::DEFAULT_BANK_CARD_UNDEL => '默认银行卡不能删除',
        self::DEFAULT_BANK_NOT_EXIST => '银行卡不存在',
        self::DEFAULT_BANK_NOT_CANCEL => '不能把默认银行卡关闭',
        self::CASH_PASSWORD_IS_LESS => '请先设置资金密码',
        self::SAFE_ANSWER_IS_LESS => '安全问题错误',
        self::NO_POWER => '无权访问',
        self::QRCODE_IS_EXIST => '该二维码已经存在',
        self::NOW_NO_CAN_USE_PAY => '当前无可用的支付渠道',
        self::APP_ORDER_ID_IS_EXIST => '订单号已经存在，不能重复下单',
        self::FA_IS_WITHDRAWAL_BUSY => '结算中，请稍等5秒再进行',
        self::QRCODE_IS_INVALID => '二维码已经失效，请重新下单',
        self::ALIPAY_LIMIT_NUMBER => '支付金额不符合规定，单笔支付金额范围 100-4999 元',
        self::WECHAT_LIMIT_NUMBER => '支付金额不符合规定，单笔支付金额范围 100-10000 元',
        self::MERCHANT_SCORE_LESS => '收款码方余额不足',
        self::PAY_TIME_OUT => '订单已失效，请重新下单',
        self::ORDER_NO_PAY => '该订单未支付',
        self::ONLY_ONE_PAY_TYPE => '每种支付方式只能选择一个支付渠道',
        self::WITHDRAWAL_IS_END => '该笔提款已经处理过',
        self::PAY_TYPE_NO_OPEN => '该支付通道未分配',
        self::NETWORK_FLUCTUATION => '网络出现波动，请重新请求',
        self::ACCOUNT_NOT_EXIST => '收款账号不存在',
        self::SYSTEM_DAY_SETTLE_ACCOUNTS => '每日 00:00:00 到 00:00:10 系统在结算，无法进行提款',
        self::MERCHANT_CASH_IS_NOT_ENOUGH => '商户余额不足',
        self::PROXY_CASH_IS_NOT_ENOUGH => '代理余额不足',
        self::ALREADY_DOING => '对不起宝贝们，这单已被其他操作员截获，请刷新等待',
        self::AMOUNT_IS_WRONG => '提现金额范围是10000~49999',
        self::AMOUNT_IS_POWER => '提现金额超过限定金额',
        self::WITHDRAWAL_IS_PROCESSING => '银行受理中',
        self::WITHDRAWAL_IS_REJECTION => '订单被驳回',
        self::NOT_RIGHT_MONEY => '请按指定金额进行充值',
        self::NOW_NO_CAN_USE_QRCODE => '当前所有的收款码正在使用中，请等待',
        self::REQUEST_ERR => '非法请求',
        self::ERR_CODE_UNDEFINED => '错误码未定义',
    );

    /**
     * 通过code获取错误信息
     * @param $code
     * @return mixed
     */
    public static function getMsg($code){

        if (!self::$msg[$code]) {
            $code = self::ERR_CODE_UNDEFINED;
        }

        return self::$msg[$code];
    }


    /**
     * 生成错误信息
     * @param string $code
     * @param array $returnData
     * @param string $msg
     */
    public static function jsonEncode($code, $returnData = array(), $msg = ''){
        header("Access-Control-Allow-Origin: *");
        header("Content-type: application/json;charset=utf-8");

        if (!self::$msg[$code]) {
            $code = self::ERR_CODE_UNDEFINED;
        }

        $returnData = array('code' => $code, 'msg' => urlencode(self::$msg[$code]) . ' ' . urlencode($msg), 'data' => $returnData ? $returnData : new stdClass());

        echo urldecode(json_encode($returnData));
        exit;
    }

}
