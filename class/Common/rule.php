<?php
class Common_rule extends Common {

    public static function checkStrSafe($str) {

        if (preg_match("/^[0-9a-zA-Z]+$/i", $str)) {
            return true;
        } else {
            return false;
        }
    }

    public static function checkMoneyNumberSafe($number) {

        if (preg_match("/^[0-9.]+$/i", $number)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 检测用户名是否合法
     *
     * @param $sUserName
     * @return bool
     */
    public static function checkUserName($sUserName) {

        if (preg_match("/(.)\\1{3,}/i", $sUserName) != 0 ) {
            return false;
        }

        if (preg_match( "/^[^0oO].*$/iUs", $sUserName) == 0 ) {
            return false;
        }

        if (preg_match( "/^[0-9a-zA-Z]{6,16}$/iUs", $sUserName)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 数字转字符串，或字符串转数字
     *
     * @param $mFrom 转换前的原始值
     * @param bool $mType true为数字转字符串，false为字符串转数字
     * @return string
     */
    public static function numberAndStrSwap($mFrom, $mType = true){
        $aStr = array(
            '0' => 'o',
            '1' => 'p',
            '2' => 'b',
            '3' => 'd',
            '4' => 't',
            '5' => 'v',
            '6' => 'a',
            '7' => 'u',
            '8' => 'i',
            '9' => 'w',
        );

        if(!$mType){
            $aStr = array_flip($aStr);
        }

        $sStr = '';
        $aData = str_split($mFrom);
        foreach ($aData as $val) {
            $sStr .= $aStr["{$val}"];
        }

        return $sStr;
    }

    /**
     * 检测登陆密码是否合法
     *
     * @param $sUserPass
     * @return bool
     */
    public static function checkUserPassword($sUserPass) {

        if (!preg_match("/^[0-9a-zA-Z]{6,32}$/i", $sUserPass) || preg_match("/^[0-9]+$/", $sUserPass) || preg_match("/^[a-zA-Z]+$/i", $sUserPass)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 检测呢称是否合法[2-6个任意字符，中文和全角算一个]
     *
     * @param $sName
     * @return bool
     */
    public static function checkNickname($sName) {
        if (mb_strlen($sName, "UTF-8") >= 2 && mb_strlen($sName, "UTF-8") <= 6) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 检查邮箱是否合法
     *
     * @param $sMail
     * @return bool
     */
    public static function checkEmail($sMail) {
        return (preg_match('/^[_.0-9a-z-a-z-]+@([0-9a-z][0-9a-z-]+.)+[a-z]{2,4}$/', $sMail)) ? true : false;
    }

    /**
     * 检测用户预留信息是否合法[2-16个任意字符，中文和全角算一个]
     *
     * @param $string
     * @return bool
     */
    public static function checkVerifyMessage($string) {
        if(mb_strlen($string, "UTF-8") >= 2 && mb_strlen($string, "UTF-8") <= 16) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 获取一个密码加密盐值随机字符串
     * @param int      $length    字符串长度  默认 6
     * @return string  随机字符串
     */
    public static function getRandSaltStr($length = 6){
        $aRand = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'), array('!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '+', '-', '_', '='));
        shuffle($aRand);
        $aData = array_flip($aRand);
        $sRandStr = implode('', array_rand($aData, $length));
        return $sRandStr;
    }

    /**
     * 加密资金密码
     *
     * @param $pwd 密码
     * @param $salt 盐
     * @return bool
     */
    public static function encodeSecurityPassword($pwd, $salt) {

        if (!$pwd || !$salt) {
            return false;
        }

        return self::passwordEncode("{$pwd}ghnv{$salt}");
    }

    /**
     * 加密代理登录密码
     *
     * @param string $pwd 密码
     * @param string $salt 盐
     * @return bool
     */
    public static function encodeLoginPassword($pwd, $salt) {

        if (!$pwd || !$salt) {
            return false;
        }

        return self::passwordEncode("{$pwd}jyhg{$salt}");
    }

    /**
     * 加密管理员登录密码
     *
     * @param $pwd 密码
     * @param $salt 盐
     * @return bool
     */
    public static function encodeAdminLoginPassword($pwd, $salt) {

        if (!$pwd || !$salt) {
            return false;
        }

        return self::passwordEncode("{$pwd}dfsf{$salt}");
    }

    /**
     * 加密游戏私钥
     *
     * @param $key 私钥
     * @return bool
     */
    public static function encodeGameSecretKey($key) {

        if (!$key) {
            return false;
        }

        return self::passwordEncode("{$key}Gamefhffy");
    }

    /**
     * 密码加密
     * @param string $sLaws  明文密码
     * @return string        加密后的密文字符串
     */
    private static function passwordEncode($sLaws){
        return md5(hash("SHA256", $sLaws));
    }

}