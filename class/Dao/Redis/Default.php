<?php
class Dao_Redis_Default extends Dao_Redis{
    protected $_sDaoName = "default";
    protected static $_oSingle = null;

    public static function getInstance(){
        if(!self::$_oSingle) self::$_oSingle = new self;
        return self::$_oSingle;
    }
}