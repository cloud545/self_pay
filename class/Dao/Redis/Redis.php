<?php
class Dao_Redis extends BooRedis{
    protected $_sDaoName;

    public function __construct(){
        if(!$this->_sDaoName) throw new ErrorException('Dao name is null.', 20001);

        $aServer = $this->_getServer();
        $this->_connect($aServer);
    }

    protected function _getServer(){
        $sAppEvn = BooVar::server("APP_ENV");
        $aConfig = BooConfig::get($sAppEvn . "/redis." . $this->_sDaoName);
        return $aConfig;
    }
}