<?php
class Dao_Mysql_Default extends Dao_Mysql {
    private static $_oSingle = null;

    public static function getInstance() {
        if (!self::$_oSingle) self::$_oSingle = new self;
        return self::$_oSingle;
    }
    
    protected function _getServer($sMastSlave) {

        $this->_dbName = BooConfig::get('dbName.default');

        $sAppEnv = BooVar::server("APP_ENV");
        $aServer = BooConfig::get("{$sAppEnv}/mysql." . $this->_dbName . ".{$sMastSlave}");
    	if ($sMastSlave == "slave") {
    		$iId = array_rand($aServer);
            $aServer = $aServer[$iId];
    	}
    	return $aServer;
    }
}