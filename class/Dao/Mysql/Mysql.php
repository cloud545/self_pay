<?php
class Dao_Mysql extends BooPdo {
    protected $_dbName = '';

    protected function _getServer($sMastSlave) {
        $sAppEnv = BooVar::server("APP_ENV");
        $aServer = BooConfig::get("{$sAppEnv}/mysql." . $this->_dbName . ".{$sMastSlave}");
        return $aServer;
    }
}