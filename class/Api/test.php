<?php
class Api_test extends Api {

    protected function _do() {

        header("Content-type: text/html;charset=utf-8");

        $orderObj = BooController::get('Obj_App_Orders');
        $obj = BooController::get('Obj_App_Info');
        $payObj = BooController::get('Obj_App_Pay');
        $withdrawalObj = BooController::get('Obj_App_Withdrawal');
        $reportObj = BooController::get('Obj_App_Report');
        $balanceObj = BooController::get('Obj_App_Balance');

        echo "商户&nbsp;&nbsp;&nbsp;&nbsp;根据历史计算得到余额&nbsp;&nbsp;&nbsp;&nbsp;现有显示余额<br/>";

        $endTime = date('Y-m-d H:i:s');
        $appList = $obj->getList();
        foreach ($appList as $appInfo) {

            $appId = $appInfo['app_id'];

            $paySumInfo = $payObj->getSum($appId, 0, 0, 0, '2019-05-02 00:00:00', $endTime);
            $withdrawalSumInfo = $withdrawalObj->getSum($appId, 2, '2019-05-02 00:00:00', $endTime);

            // 管理员添加资金
            $orderSumInfo4 = $orderObj->getSum($appId, 4, '2019-05-02 00:00:00', $endTime);
            // 管理员扣减资金
            $orderSumInfo5 = $orderObj->getSum($appId, 5, '2019-05-02 00:00:00', $endTime);
            // 管理员冻结资金
            $orderSumInfo6 = $orderObj->getSum($appId, 6, '2019-05-02 00:00:00', $endTime);
            // 管理员解冻资金
            $orderSumInfo7 = $orderObj->getSum($appId, 7, '2019-05-02 00:00:00', $endTime);

            $delBalance = $orderSumInfo4['sumAmount'] + abs($orderSumInfo7['sumAmount']) + $orderSumInfo5['sumAmount'] - $orderSumInfo6['sumAmount'];

            $appBalanceInfo = $balanceObj->getInfoByAppId($appId);
            $balance = $paySumInfo['sumFee'] - $withdrawalSumInfo['sumAmount'] - $withdrawalSumInfo['sumFee'] + $delBalance - $appBalanceInfo['frozen_amount'];

            echo "{$appInfo['name']}&nbsp;&nbsp;&nbsp;&nbsp;{$balance}&nbsp;&nbsp;&nbsp;&nbsp;{$appBalanceInfo['available_balance']}<br/>";

        }

        
        exit;

        $params = array(
            'appId' => '7606',
            'platId' => $_REQUEST['platId'],
            'userId' => '2',
            'time' => time(),
        );

        if ($_REQUEST['userName']) {
            $params['userName'] = $_REQUEST['userName'];
        }

        if ($_REQUEST['amount']) {
            $params['amount'] = $_REQUEST['amount'];
            $params['orderId'] = time();
        }

        ksort($params);

        $checkString = '';
        foreach ($params as $key =>$value) {
            if (!$checkString) {
                $checkString = "{$key}={$value}";
            } else {
                $checkString .= "&{$key}={$value}";
            }
        }

        $checkString .= "8484f5239dfd5cca03794e4458263abd";
        $sign = md5($checkString);
        $params['sign'] = $sign;

        echo '<pre>';
        var_dump('pay', 'http://' . $_SERVER['SERVER_NAME'] . '/app/pay?' . http_build_query($params));

        exit;
	}
}