<?php
class Api_App_checkPayCallback extends Api_App {

	protected function _do() {

        $post = BooVar::requestx();

        $appPayInfo = array();
        if (!$post['appPayInfo']) {
            if ($post['tradeno']) {
                $appPayInfo = explode('1', $post['tradeno']);
            } elseif ($post['reqStreamId']) {
                $appPayInfo = explode('1', $post['reqStreamId']);
            } else {
                Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
            }
        } else {
            $appPayInfo = explode('1', $post['appPayInfo']);
        }

        $appId = BooController::get('Common')->numberAndStrSwap($appPayInfo[0], 'decode');
        $payId = BooController::get('Common')->numberAndStrSwap($appPayInfo[1], 'decode');
        if (!$appId || !$payId) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $platId = 0;
        if ($appPayInfo[2]) {
            $platId = BooController::get('Common')->numberAndStrSwap($appPayInfo[2], 'decode');
        }

        $qrCodeUserId = 0;
        if ($appPayInfo[3]) {
            $qrCodeUserId = BooController::get('Common')->numberAndStrSwap($appPayInfo[3], 'decode');
        }

        $qrCodeId = 0;
        if ($appPayInfo[4]) {
            $qrCodeId = BooController::get('Common')->numberAndStrSwap($appPayInfo[4], 'decode');
        }

        $appPayObj = BooController::get('Obj_App_Pay');

        $payInfo = $appPayObj->getInfoById($appId, $payId);

        if (!$payInfo) {
            echo 'fail';
            exit();
        }

//这里为了简便直接赋值处理，省去验证
 //查询所有这几天的没有处理的数据
        $arrRsesult = $appPayObj->getInfoByAll();
		$i  = 0;
//开始执行文件处理
        foreach($arrRsesult as $payInfo)
        {
            $arrRes = json_decode($payInfo['content'],true);
			
//这里是循环执行未处理的
            $payCallbackRs['platOrderId']       = $arrRes['trade_no'];
            $payCallbackRs['realMoney']         = $arrRes['transaction_money'] /100;

            $updateData['plat_order_id'] = $payCallbackRs['platOrderId'];

            $realMoney = 0;
            if ($payCallbackRs['realMoney']) {
                $realMoney = $payCallbackRs['realMoney'];
            } else {
                $realMoney = $payInfo['amount'];
            }

            if ($realMoney - $payInfo['amount'] >= 1000) {
                echo 'money error';
                exit();
            }

            if ($realMoney <= 0) {
                Common_errorCode::jsonEncode(Common_errorCode::PAY_FAIL);
            }

            // 记录成功率
            $cacheDao = Dao_Redis_Default::getInstance();
            $payLogListCacheKey = BooConfig::get('cacheKey.payLogList');
            $payLogListCacheInfo = $cacheDao->get($payLogListCacheKey);
            if ($payLogListCacheInfo) {
                $payLogListCacheInfo = json_decode($payLogListCacheInfo, true);

                $isChange = false;
                foreach ($payLogListCacheInfo as $key => $tmpInfo) {
                    if ($tmpInfo['id'] == $payId && $tmpInfo['app_id'] == $appId) {
                        $payLogListCacheInfo[$key]['real_amount'] = $realMoney;
                        $payLogListCacheInfo[$key]['result'] = 1;
                        $payLogListCacheInfo[$key]['end_time'] = time();
                        $isChange = true;
                        break;
                    }
                }

                if ($isChange) {
                    $cacheDao->set($payLogListCacheKey, json_encode($payLogListCacheInfo), 3600);
                }
            }

            $appRealMoney = $realMoney * (1 - $payInfo['current_fee_rate']);

            $updateData['real_amount'] = $realMoney;
            $updateData['result'] = 1;

            $data = array();
            $data['platOrderId'] = 'P' . date('Ymd', strtotime($payInfo['create_time'])) . $payInfo['id'];
            $data['orderId'] =  $payInfo['order_id'];
            $data['amount'] =  $payInfo['amount'];
            $data['realAmount'] =  $realMoney;
            $data['remark'] =  $payInfo['remark'];
            $data['time'] =  date('Y-m-d H:i:s');
            //$data['sign'] = $this->getSign($data, $platInfo['appInfo']['app_key']);

            $updateData['notify_data'] = json_encode($data);
            $updateData['last_notify_time'] = date('Y-m-d H:i:s');
            $updateData['current_fee'] = $appRealMoney;

            $cacheDao = Dao_Redis_Default::getInstance();
            $cacheKey = BooConfig::get('cacheKey.payPlatOrderId') . $appId . '_' . $payId;
            $cacheInfo = $cacheDao->get($cacheKey);
            if (!$cacheInfo) {
                $payInfo['real_amount'] = $realMoney;
                $payInfo['result'] = 1;
                $payInfo['current_fee'] = $appRealMoney;
                $cacheDao->set($cacheKey, json_encode($payInfo), 5);
            }
			$i++;
echo $i;
            $this->sendMoney($payInfo, $realMoney);
        }
        exit();
	}

	private function sendMoney($payInfo, $realMoney) {

        $obj = BooController::get('Obj_Proxy_Info');
        $info = $obj->getInfoById($payInfo['proxy_id']);
        $platAppInfo = json_decode($info['plat_app_info'], true);
        $pointInfo = $platAppInfo[$payInfo['p_id']];
        foreach ($platAppInfo as $tmpInfo) {
            if ($tmpInfo['payType'] == $payInfo['pt_id']) {
                $pointInfo = $tmpInfo;
                break;
            }
        }

        $proxyMoney = $realMoney * ($payInfo['current_point'] - $pointInfo['point']);

        if ($payInfo['current_point'] - $pointInfo['point'] < 0) {
            return true;
        }

        $nowDateTime = date('Y-m-d H:i:s');
        $insertData = array();
        $insertData['app_id'] = $payInfo['app_id'];
        $insertData['app_name'] = $payInfo['app_name'];
        $insertData['proxy_id'] = $info['proxy_id'];
        $insertData['proxy_name'] = $info['proxy_name'];
        $insertData['p_id'] = $payInfo['p_id'];
        $insertData['order_id'] = $payInfo['order_id'];
        $insertData['amount'] = $payInfo['amount'];
        $insertData['real_amount'] = $realMoney;
        $insertData['pt_id'] = $payInfo['pt_id'];
        $insertData['pay_id'] = $payInfo['id'];
        $insertData['create_time'] = $payInfo['create_time'];
        $insertData['end_time'] = $nowDateTime;
        $insertData['create_date'] = $payInfo['create_date'];
        $insertData['current_point'] = $pointInfo['point'];
        $insertData['current_fee_rate'] = $payInfo['current_point'] - $pointInfo['point'];
        $insertData['current_fee'] = $proxyMoney;

        $proxyPayObj = BooController::get('Obj_Proxy_Pay');

        $proxyPayObj->insert($payInfo['proxy_id'], $insertData);

        $lastLevelPoint = $pointInfo['point'];

        $point = 0;
        $obj = BooController::get('Obj_Plat_Info');
        $list = $obj->getList();
        foreach ($list as $key => $tmpInfo) {
            if ($tmpInfo['p_id'] == $payInfo['p_id']) {
                $point = $tmpInfo['p_point'];
                break;
            }
        }

        if (!$point) {
            return true;
        }

        if ($lastLevelPoint - $point < 0) {
            return true;
        }

        $systemMoney = $realMoney * ($lastLevelPoint - $point);
        $insertData = array();
        $insertData['app_id'] = $payInfo['app_id'];
        $insertData['app_name'] = $payInfo['app_name'];
        $insertData['proxy_id'] = $payInfo['proxy_id'];
        $insertData['proxy_name'] = $payInfo['proxy_name'];
        $insertData['p_id'] = $payInfo['p_id'];
        $insertData['order_id'] = $payInfo['order_id'];
        $insertData['pay_id'] = $payInfo['id'];
        $insertData['amount'] = $payInfo['amount'];
        $insertData['real_amount'] = $realMoney;
        $insertData['pt_id'] = $payInfo['pt_id'];
        $insertData['create_time'] = $payInfo['create_time'];
        $insertData['end_time'] = $nowDateTime;
        $insertData['create_date'] = $payInfo['create_date'];
        $insertData['current_point'] = $point;
        $insertData['current_fee_rate'] = $lastLevelPoint - $point;
        $insertData['current_fee'] = $systemMoney;

        $systemPayObj = BooController::get('Obj_Admin_Pay');

        $systemPayObj->insert($insertData);

        return true;
    }

}
