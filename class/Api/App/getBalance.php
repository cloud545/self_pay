<?php
class Api_App_getBalance extends Api_App {

	protected function _do(){

        $post = BooVar::requestx();
        if (!$post['appId']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }
        $appId = $post['appId'];

        $balanceObj = BooController::get('Obj_App_Balance');
        $appBalanceInfo = $balanceObj->getInfoByAppId($appId);

        return array('balance' => $appBalanceInfo['available_balance']);
	}
}
