<?php
class Api_App_checkOrder extends Api_App {

	protected function _do(){

        $post = BooVar::requestx();
        if (!$post['appId'] || !$post['platOrderId']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $payId = $post['platOrderId'] ? substr($post['platOrderId'],9) : 0;

        $cacheDao = Dao_Redis_Default::getInstance();
        $cacheKey = BooConfig::get('cacheKey.payPlatOrderId') . $post['appId'] . '_' . $payId;
        $cacheInfo = $cacheDao->get($cacheKey);
        if ($cacheInfo) {
            $payInfo = json_decode($cacheInfo, true);
        } else {
            $appPayObj = BooController::get('Obj_App_Pay');
            $payInfo = $appPayObj->getInfoById($post['appId'], $payId);
        }

        if (!$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::ORDER_NOT_EXIST);
        }

        if ($payInfo['result'] != 1) {
            Common_errorCode::jsonEncode(Common_errorCode::ORDER_NO_PAY);
        }

        $data = array();
        $data['platOrderId'] = 'P' . date('Ymd', strtotime($payInfo['create_time'])) . $payInfo['id'];
        $data['orderId'] =  $payInfo['order_id'];
        $data['amount'] =  $payInfo['amount'];
        $data['realAmount'] =  $payInfo['real_amount'];
        $data['remark'] =  $payInfo['remark'];
        $data['time'] =  $payInfo['last_notify_time'];

        return $data;
	}

}
