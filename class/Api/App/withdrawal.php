<?php
class Api_App_withdrawal extends Api_App {
    protected function _do(){
        $post = BooVar::requestx();
        return Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
		exit;
		if (!$post['appId'] || !$post['payType'] || $post['amount'] <= 0 || !$post['cardNo'] || !$post['accountName'] || !$post['bankCode'] || !$post['bankName'] || !$post['bankBranch'] || !$post['orderId'] || !$post['time'] ) {
            return Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }
        // 缓存锁，避免出现并发
        $cacheKey = BooConfig::get('cacheKey.appWithdrawalLock') . $post['appId'];
        if (Common_cacheLock::get($cacheKey)) {
            return Common_errorCode::jsonEncode(Common_errorCode::FA_IS_WITHDRAWAL_BUSY);
        }
        $rs = Common_cacheLock::set($cacheKey, 1, 3);
        if (!$rs) {
            return Common_errorCode::jsonEncode(Common_errorCode::FA_IS_WITHDRAWAL_BUSY);
        }

        //判断是否是数字
        if(!is_numeric($post['amount'])){
            Common_errorCode::jsonEncode(Common_errorCode::AMOUNT_IS_NOT_NUMBER);
        }

        //提现金额限制在10000以上49999以下
        if ($post['amount']  >49999   || $post['amount'] < 10000) {
            Common_errorCode::jsonEncode(Common_errorCode::AMOUNT_IS_WRONG);
        }
		
        //判断商户是否存在
        $appObj = BooController::get('Obj_App_Info');
        $info = $appObj->getInfoByAppId($post['appId']);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::APP_NOT_EXIST);
        }

        $balanceObj     = BooController::get('Obj_App_Balance');
        $balanceInfo    = $balanceObj->getInfoByAppId($post['appId']);

        //判断商户可用余额是否足够
        if ($post['amount'] + $info['withdrawal_fee']  > $balanceInfo['available_balance']) {
            Common_errorCode::jsonEncode(Common_errorCode::CASH_IS_NOT_ENOUGH);
        }

        //判断是否是重复订单
        $obj = BooController::get('Obj_App_Withdrawal');
        $orderInfo = $obj->getInfoByOrderId($post['appId'], $post['orderId']);  
        if ($orderInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::APP_ORDER_ID_IS_EXIST);
        }

        //时间戳转化成Y-m-d H:i:s
        $timeToDate = date("Y-m-d H:i:s",$post['time']);
        $inserData = array(
            'app_id' 	=> $info['app_id'],
            'app_name' 	=> $info['name'],
            'amount' 	=> $post['amount'],
            'fee' 		=> $info['withdrawal_fee'] ,
            'order_id' 	=> $post['orderId'],
            'pt_id' 	=> $post['payType'],
            'p_id' 		=> 0,
            'bank_type'                => $post['bankName'],
            'card_no' 	=> $post['cardNo'],
            'bank_name'                => $post['bankBranch'],
            'card_account_name'       => $post['accountName'],
            'create_time' 	=> $timeToDate,
            'type' 		=> 1,
            'status' 	=> 0,
            'notify_url' 	=> $post['notifyUrl'],
            'apply_user' 	=> '接口提现',
        );
        //$orderId = $obj->insert($inserData);
        //if (!$orderId) {
        //    Common_errorCode::jsonEncode(Common_errorCode::INSERT_ERR);
        //    exit;
        //}

        //更新商户金额表信息
        $updateData = array();
        $updateData['available_balance']= $balanceInfo['available_balance'] - $post['amount'] - $info['withdrawal_fee'];
        $updateData['frozen_amount'] 	= $balanceInfo['frozen_amount'] + $post['amount'] + $info['withdrawal_fee'];
        $updateData['last_modify_time'] = date('Y-m-d H:i:s');
//var_dump($updateData);	exit;	
        $rs = $balanceObj->update($balanceInfo['id'], $updateData);
        if (!$rs) {
              Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }
        // 删除锁
        Common_cacheLock::del($cacheKey);	
            $data = [
                'msg' => '代付订单成功生成',
                'platOrderId' => 'W' . date('Ymd') . $orderId,
            ];
        return $data;
    }
}
