<?php
class Api_App_payCallback extends Api_App {

    protected function _do() {

        $post = BooVar::requestx();

        $appPayInfo = array();
        if (!$post['appPayInfo']) {
            if ($post['tradeno']) {
                $appPayInfo = explode('1', $post['tradeno']);
            } elseif ($post['reqStreamId']) {
                $appPayInfo = explode('1', $post['reqStreamId']);
            } else {
                Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
            }
        } else {
            $appPayInfo = explode('1', $post['appPayInfo']);
        }

        $appId = BooController::get('Common')->numberAndStrSwap($appPayInfo[0], 'decode');
        $payId = BooController::get('Common')->numberAndStrSwap($appPayInfo[1], 'decode');
        if (!$appId || !$payId) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $platId = 0;
        if ($appPayInfo[2]) {
            $platId = BooController::get('Common')->numberAndStrSwap($appPayInfo[2], 'decode');
        }

        $qrCodeUserId = 0;
        if ($appPayInfo[3]) {
            $qrCodeUserId = BooController::get('Common')->numberAndStrSwap($appPayInfo[3], 'decode');
        }

        $qrCodeId = 0;
        if ($appPayInfo[4]) {
            $qrCodeId = BooController::get('Common')->numberAndStrSwap($appPayInfo[4], 'decode');
        }

        $appPayObj = BooController::get('Obj_App_Pay');
        $payInfo = $appPayObj->getInfoById($appId, $payId);
        if (!$payInfo) {
            echo 'fail';
            exit();
        }

        $content = $_REQUEST;
        if (isset($content['target'])) {
            unset($content['target']);
        }

        if (isset($content['appPayInfo'])) {
            unset($content['appPayInfo']);
        }

        if (isset($content['PHPSESSID'])) {
            unset($content['PHPSESSID']);
        }

        if (!$content) {
            $content = file_get_contents("php://input");
        }

        if (is_array($content)) {
            $content = json_encode($content);
        }

        $updateData = array(
            'content' => $content,
            'end_time' => date('Y-m-d H:i:s'),
        );

        // 调用第三方平台接口
        $platInfo = BooController::get('Mod_ThirdPlatform')->getPlatInfo($appId, $payInfo['pt_id'], $platId, $payInfo);

        if ($qrCodeUserId) {
            $platInfo['platInfo']['qrCodeUserId'] = $qrCodeUserId;
        }

        if ($qrCodeId) {
            $platInfo['platInfo']['qrCodeId'] = $qrCodeId;
        }

        $payCallbackRs = BooController::get("Mod_ThirdPlatform_{$platInfo['platInfo']['platTag']}")->payCallback($platInfo);
        if ($payInfo['result'] == 1) {
            if (!$payCallbackRs) {
                echo 'sign error';
            }
            exit;
        }

        if (!$payCallbackRs) {
            $appPayObj->update($appId, $payInfo['id'], $updateData);
            echo 'sign error';
            exit();
        }
        $updateData['plat_order_id'] = $payCallbackRs['platOrderId'];

        $realMoney = 0;
        if ($payCallbackRs['realMoney']) {
            $realMoney = $payCallbackRs['realMoney'];
        } else {
            $realMoney = $payInfo['amount'];
        }

		//订单金额跟实际支付金额不一致差太多则不给回调
        if ($realMoney - $payInfo['amount'] >= 1 || $payInfo['amount'] - $realMoney >= 1) {
            echo '此订单有风险，订单金额跟实际支付金额差太多';
            exit();
        }
		
        if ($realMoney <= 0) {
            Common_errorCode::jsonEncode(Common_errorCode::PAY_FAIL);
        }
		
		//为了满足浮动金额差异正常回调，最后直接取订单金额作为实际支付金额回调
        $realMoney = $payInfo['amount'];


        // 记录成功率
        $cacheDao = Dao_Redis_Default::getInstance();
        $payLogListCacheKey = BooConfig::get('cacheKey.payLogList');
        $payLogListCacheInfo = $cacheDao->get($payLogListCacheKey);
        if ($payLogListCacheInfo) {
            $payLogListCacheInfo = json_decode($payLogListCacheInfo, true);

            $isChange = false;
            foreach ($payLogListCacheInfo as $key => $tmpInfo) {
                if ($tmpInfo['id'] == $payId && $tmpInfo['app_id'] == $appId) {
                    $payLogListCacheInfo[$key]['real_amount'] = $realMoney;
                    $payLogListCacheInfo[$key]['result'] = 1;
                    $payLogListCacheInfo[$key]['end_time'] = time();
                    $isChange = true;
                    break;
                }
            }

            if ($isChange) {
                $cacheDao->set($payLogListCacheKey, json_encode($payLogListCacheInfo), 3600);
            }
        }

        $appRealMoney = $realMoney * (1 - $payInfo['current_fee_rate']);

        $updateData['real_amount'] = $realMoney;
        $updateData['result'] = 1;

        $data = array();
        $data['platOrderId'] = 'P' . date('Ymd', strtotime($payInfo['create_time'])) . $payInfo['id'];
        $data['orderId'] =  $payInfo['order_id'];
        $data['amount'] =  $payInfo['amount'];
        $data['realAmount'] =  $realMoney;
        $data['remark'] =  $payInfo['remark'];
        $data['time'] =  date('Y-m-d H:i:s');
        $data['sign'] = $this->getSign($data, $platInfo['appInfo']['app_key']);

        $updateData['notify_data'] = json_encode($data);
        $updateData['last_notify_time'] = date('Y-m-d H:i:s');
        $updateData['current_fee'] = $appRealMoney;
        //$appPayObj->update($appId, $payInfo['id'], $updateData);

        $cacheDao = Dao_Redis_Default::getInstance();
        $cacheKey = BooConfig::get('cacheKey.payPlatOrderId') . $appId . '_' . $payId;
        $cacheInfo = $cacheDao->get($cacheKey);
        if (!$cacheInfo) {
            $payInfo['real_amount'] = $realMoney;
            $payInfo['result'] = 1;
            $payInfo['current_fee'] = $appRealMoney;
            $cacheDao->set($cacheKey, json_encode($payInfo), 5);
        }

        for ($i = 1; $i <= 3; $i++) {
            $updateData['notify_times'] += 1;

            BooCurl::setData($data, 'POST');
            $result = BooCurl::call($payInfo['notify_url']);
            $updateData['notify_res_data'] = $result;
            if (trim($result) == 'success') {
                $updateData['notify_status'] = 1;
                break;
            }
        }

        $appPayObj->update($appId, $payInfo['id'], $updateData);

        $this->sendMoney($payInfo, $realMoney);
        exit();
	}

    private function sendMoney($payInfo, $realMoney) {
       if (!$payInfo['proxy_id']) {

            $lastLevelPoint = $payInfo['current_point'];
            $point = 0;
            $obj = BooController::get('Obj_Plat_Info');
            $list = $obj->getList();
            foreach ($list as $key => $tmpInfo) {
                if ($tmpInfo['p_id'] == $payInfo['p_id']) {
                    $point = $tmpInfo['p_point'];
                    break;
                }
            }

            if (!$point) {
                //return true;  //找不到费率
            }
            
            $dxpoint = $lastLevelPoint - $point;

            if ($dxpoint < 0) {
                $dxpoint = 0;
            }
            
            $systemPayObj = BooController::get('Obj_Admin_Pay');
            
            $reCnt = $systemPayObj->getCountByAppPayId($payInfo['app_id'],$payInfo['id']);
            if($reCnt>0){
                return true;
            }

            $systemMoney = $realMoney * $dxpoint;
            $insertData = array();
            $insertData['app_id'] = $payInfo['app_id'];
            $insertData['app_name'] = $payInfo['app_name'];
            $insertData['p_id'] = $payInfo['p_id'];
            $insertData['order_id'] = $payInfo['order_id'];
            $insertData['pay_id'] = $payInfo['id'];
            $insertData['amount'] = $payInfo['amount'];
            $insertData['real_amount'] = $realMoney;
            $insertData['pt_id'] = $payInfo['pt_id'];
            $insertData['create_time'] = $payInfo['create_time'];
            $insertData['end_time'] = date('Y-m-d H:i:s');
            $insertData['create_date'] = $payInfo['create_date'];
            $insertData['current_point'] = $point;
            $insertData['current_fee_rate'] = $dxpoint;
            $insertData['current_fee'] = $systemMoney;

            $systemPayObj->insert($insertData);

            return true;
        }

        $obj = BooController::get('Obj_Proxy_Info');
        $info = $obj->getInfoById($payInfo['proxy_id']);
        $platAppInfo = json_decode($info['plat_app_info'], true);
        $pointInfo = $platAppInfo[$payInfo['p_id']];
        foreach ($platAppInfo as $tmpInfo) {
            if ($tmpInfo['payType'] == $payInfo['pt_id']) {
                $pointInfo = $tmpInfo;
                break;
            }
        }

        //代理通道查找不到，这里当作不返点处理
        if($pointInfo['point']>0){
            $dxpoint = $payInfo['current_point'] - $pointInfo['point'];
            if ($dxpoint < 0) {
                $dxpoint = 0;
            }
            $proxyMoney = $realMoney * $dxpoint;

            $nowDateTime = date('Y-m-d H:i:s');
            $insertData = array();
            $insertData['app_id'] = $payInfo['app_id'];
            $insertData['app_name'] = $payInfo['app_name'];
            $insertData['proxy_id'] = $info['proxy_id'];
            $insertData['proxy_name'] = $info['proxy_name'];
            $insertData['p_id'] = $payInfo['p_id'];
            $insertData['order_id'] = $payInfo['order_id'];
            $insertData['amount'] = $payInfo['amount'];
            $insertData['real_amount'] = $realMoney;
            $insertData['pt_id'] = $payInfo['pt_id'];
            $insertData['pay_id'] = $payInfo['id'];
            $insertData['create_time'] = $payInfo['create_time'];
            $insertData['end_time'] = $nowDateTime;
            $insertData['create_date'] = $payInfo['create_date'];
            $insertData['current_point'] = $pointInfo['point'];
            $insertData['current_fee_rate'] = $dxpoint;
            $insertData['current_fee'] = $proxyMoney;

            $proxyPayObj = BooController::get('Obj_Proxy_Pay');
            $reCnt = $proxyPayObj->getCountByAppPayId($info['proxy_id'],$payInfo['app_id'],$payInfo['id']);
            if($reCnt==0){
                $proxyPayObj->insert($payInfo['proxy_id'], $insertData);
            }
            
            $lastLevelPoint = min($payInfo['current_point'],$pointInfo['point']);//$dxpoint<0异常,应取最小值
            
            if ($info['user_tree']) {
                $treeInfo = explode(',', $info['user_tree']);
                krsort($treeInfo);

                foreach ($treeInfo as $tmpProxyId) {
                    $info = $obj->getInfoById($tmpProxyId);
                    $platAppInfo = json_decode($info['plat_app_info'], true);
                    $pointInfo = $platAppInfo[$payInfo['p_id']];
                    foreach ($platAppInfo as $tmpInfo) {
                        if ($tmpInfo['payType'] == $payInfo['pt_id']) {
                            $pointInfo = $tmpInfo;
                            break;
                        }
                    }
                    
                    $dxpoint = $lastLevelPoint - $pointInfo['point'];
                    if ($dxpoint < 0) {
                        $dxpoint = 0;
                    }
                    $proxyMoney = $realMoney * $dxpoint;
                    
                    $lastLevelPoint = min($lastLevelPoint,$pointInfo['point']);

                    $insertData = array();
                    $insertData['app_id'] = $payInfo['app_id'];
                    $insertData['app_name'] = $payInfo['app_name'];
                    $insertData['proxy_id'] = $info['proxy_id'];
                    $insertData['proxy_name'] = $info['proxy_name'];
                    $insertData['p_id'] = $payInfo['p_id'];
                    $insertData['order_id'] = $payInfo['order_id'];
                    $insertData['amount'] = $payInfo['amount'];
                    $insertData['real_amount'] = $realMoney;
                    $insertData['pt_id'] = $payInfo['pt_id'];
                    $insertData['pay_id'] = $payInfo['id'];
                    $insertData['create_time'] = $payInfo['create_time'];
                    $insertData['end_time'] = $nowDateTime;
                    $insertData['create_date'] = $payInfo['create_date'];
                    $insertData['current_point'] = $pointInfo['point'];
                    $insertData['current_fee_rate'] = $dxpoint;
                    $insertData['current_fee'] = $proxyMoney;

                    $reCnt = $proxyPayObj->getCountByAppPayId($tmpProxyId,$payInfo['app_id'],$payInfo['id']);
                    if($reCnt==0){
                        $proxyPayObj->insert($tmpProxyId, $insertData);
                    }
                }

            }
        }

        $point = 0;
        $obj = BooController::get('Obj_Plat_Info');
        $list = $obj->getList();
        foreach ($list as $key => $tmpInfo) {
            if ($tmpInfo['p_id'] == $payInfo['p_id']) {
                $point = $tmpInfo['p_point'];
                break;
            }
        }

        if (!$point) {
            //return true;
        }

        $dxpoint = $lastLevelPoint - $point;

        if ($dxpoint < 0) {
            $dxpoint = 0;
        }
        
        $systemPayObj = BooController::get('Obj_Admin_Pay');
        $reCnt = $systemPayObj->getCountByAppPayId($payInfo['app_id'],$payInfo['id']);
        if($reCnt>0){
            return true;
        }
            
        $systemMoney = $realMoney * $dxpoint;
        $insertData = array();
        $insertData['app_id'] = $payInfo['app_id'];
        $insertData['app_name'] = $payInfo['app_name'];
        $insertData['proxy_id'] = $payInfo['proxy_id'];
        $insertData['proxy_name'] = $payInfo['proxy_name'];
        $insertData['p_id'] = $payInfo['p_id'];
        $insertData['order_id'] = $payInfo['order_id'];
        $insertData['pay_id'] = $payInfo['id'];
        $insertData['amount'] = $payInfo['amount'];
        $insertData['real_amount'] = $realMoney;
        $insertData['pt_id'] = $payInfo['pt_id'];
        $insertData['create_time'] = $payInfo['create_time'];
        $insertData['end_time'] = $nowDateTime;
        $insertData['create_date'] = $payInfo['create_date'];
        $insertData['current_point'] = $point;
        $insertData['current_fee_rate'] = $dxpoint;
        $insertData['current_fee'] = $systemMoney;

        $systemPayObj->insert($insertData);

        return true;
    }

}
