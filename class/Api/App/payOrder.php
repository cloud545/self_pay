<?php
class Api_App_payOrder extends Api_App {

	protected function _do(){

        $post = BooVar::requestx();
        if (!$post['appId'] || !$post['payType'] || !$post['orderId'] || !$post['pOrderId'] || $post['amount'] <= 0) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $platInfo = BooController::get('Mod_ThirdPlatform')->getPlatInfo($post['appId'], $post['payType']);
        $pOrderId = $post['pOrderId'];

        if (BooUtil::isHttps()) {
            $platInfo['appInfo']['callbackUrl'] = 'https://' . $_SERVER['HTTP_HOST'] . '/payCallback/' . "{$pOrderId}";
        } else {
            $platInfo['appInfo']['callbackUrl'] = 'http://' . $_SERVER['HTTP_HOST'] . '/payCallback/' . "{$pOrderId}";
        }

        $platInfo['appInfo']['clientUrl'] = $post['clientUrl'];

        BooController::get("Mod_ThirdPlatform_{$platInfo['platInfo']['platTag']}")->payOrder($platInfo);
	}
}
