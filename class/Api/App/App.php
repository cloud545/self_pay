<?php
class Api_App extends Api{

    public function run() {
        header("Access-Control-Allow-Origin: *");
        header("Content-type: text/html;charset=utf-8");

        // 设置模板配置
        $this->setSmartyTpl();

        if (!in_array(BooVar::get('target'), array('app/payCallback'))) {
            $this->checkSign();
        }

        // 每个请求逻辑处理，一般在子类中处理
        $data = $this->_do();
        if ($data === true || $data === false) {
            $data = array();
        }

        Common_errorCode::jsonEncode(Common_errorCode::SUCCESS, $data);
    }

    //子类要重载这个方法
	protected function _do() {

	}

    private function setSmartyTpl() {

        BooView::setOptions(array(
            "PATH_TPL" => PATH_TPL,
            "PATH_COMPILE" => PATH_DATA . "/smarty/compile",
            "DELIMITER" => array("<%", "%>"),
        ));

        BooView::set("webCdnUrl", BooConfig::get("main.webCdnUrl"));
    }

    private function checkSign() {

        $post = BooVar::requestx();

        if (!$post['appId'] || !$post['sign'] || !$post['time']) {
            $msg = '';
            if (!$post['appId']) {
                $msg .= "，appId 不能为空";
            }
            if (!$post['sign']) {
                $msg .= "，sign 不能为空";
            }
            if (!$post['time']) {
                $msg .= "，time 不能为空";
            }
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST, array(), $msg);
        }

        $appObj = BooController::get('Obj_App_Info');
        $appInfo = $appObj->getInfoByAppId($post['appId']);
        if (!$appInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::APP_NOT_EXIST);
        }

        if ($appInfo['is_lock']) {
            Common_errorCode::jsonEncode(Common_errorCode::APP_IS_LOCK);
        }

        $sign = $post['sign'];
        unset($post['sign']);

        $checkArr = array();
        if ($post['target'] == 'app/pay') {
            $checkArr['amount'] = $post['amount'];
            $checkArr['appId'] = $post['appId'];
            $checkArr['clientUrl'] = $post['clientUrl'];
            $checkArr['notifyUrl'] = $post['notifyUrl'];
            $checkArr['orderId'] = $post['orderId'];
            $checkArr['payType'] = $post['payType'];
            $checkArr['remark'] = $post['remark'];
            $checkArr['time'] = $post['time'];
        } elseif ($post['target'] == 'app/checkOrder') {
            $checkArr['appId'] = $post['appId'];
            $checkArr['platOrderId'] = $post['platOrderId'];
            $checkArr['time'] = $post['time'];
        } elseif ($post['target'] == 'app/getBalance') {
            $checkArr['appId'] = $post['appId'];
            $checkArr['time'] = $post['time'];
        } elseif ($post['target'] == 'app/getWithdrawalList') {
            $checkArr['appId'] = $post['appId'];
            $checkArr['time'] = $post['time'];
            $checkArr['startTime'] = $post['startTime'];
            $checkArr['endTime'] = $post['endTime'];
        } elseif($post['target'] == 'app/withdrawal'){
			$checkArr['appId'] 		= $post['appId'];
            $checkArr['amount'] 	= $post['amount'];
            $checkArr['payType'] 	= $post['payType'];
            $checkArr['bankName'] 	= $post['bankName'];
            $checkArr['bankBranch'] = $post['bankBranch'];
            $checkArr['cardNo'] 	= $post['cardNo'];
            $checkArr['accountName']= $post['accountName'];
            $checkArr['bankCode'] 	= $post['bankCode'];
            $checkArr['orderId'] 	= $post['orderId'];
			$checkArr['time'] 		= $post['time'];
		} elseif($post['target'] == 'app/checkWithdrawal'){
			$checkArr['appId'] 		= $post['appId'];
			$checkArr['orderId'] 	= $post['orderId'];
			$checkArr['time'] 		= $post['time'];
		}

		//验签中
        ksort($checkArr);
        $checkString = '';
        foreach ($checkArr as $key =>$value) {
            if (!$checkString) {
                $checkString = "{$key}={$value}";
            } else {
                $checkString .= "&{$key}={$value}";
            }
        }

        $checkString .= "{$appInfo['app_key']}";
        $checkSign = md5($checkString);

//echo $checkSign;exit;
        if ($sign != $checkSign) {
            Common_errorCode::jsonEncode(Common_errorCode::SIGN_ERR);
        }

        return true;
    }

    protected function getSign($data, $appKey) {

        ksort($data);

        $checkString = '';
        foreach ($data as $key =>$value) {
            if (!$checkString) {
                $checkString = "{$key}={$value}";
            } else {
                $checkString .= "&{$key}={$value}";
            }
        }

        $checkString .= "{$appKey}";
        $sign = md5($checkString);

        return $sign;
    }

}