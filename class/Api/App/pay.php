<?php
class Api_App_pay extends Api_App {

	protected function _do(){

        $post = BooVar::requestx();
        if (!$post['appId'] || !$post['payType'] || $post['amount'] <= 0) {
            $msg = '';
            if (!$post['payType']) {
                $msg .= "，payType 不能为空";
            }
            if (!$post['amount']) {
                $msg .= "，amount 不能小于等于 0";
            }

            if (isset($post['target'])) {
                unset($post['target']);
            }

            if (isset($post['appPayInfo'])) {
                unset($post['appPayInfo']);
            }

            $msg .= "，请求的参数为：" . json_encode($post);

            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST, array(), $msg);
        }

        $appPayObj = BooController::get('Obj_App_Pay');
        $orderInfo = $appPayObj->getInfoByOrderId($post['appId'], $post['orderId']);
        if ($orderInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::APP_ORDER_ID_IS_EXIST);
        }

        $platInfo = BooController::get('Mod_ThirdPlatform')->getPlatInfo($post['appId'], $post['payType']);
        if ($platInfo['platInfo']['platTag'] == 'SelfQrPay') {
            // 支付宝单笔100-4999 微信单笔100-10000
            if (in_array($platInfo['platInfo']['payType'], array(2, 9))) {
                if (100 > $post['amount'] || $post['amount'] > 5000) {
                    Common_errorCode::jsonEncode(Common_errorCode::ALIPAY_LIMIT_NUMBER);
                }

            } elseif (in_array($platInfo['platInfo']['payType'], array(3, 10))) {
                if (100 > $post['amount'] || $post['amount'] > 10000) {
                    Common_errorCode::jsonEncode(Common_errorCode::WECHAT_LIMIT_NUMBER);
                }
            }
        }

        $insertData = array();
        $insertData['app_id'] = $post['appId'];
        $insertData['app_name'] = $platInfo['appInfo']['name'];
        $insertData['proxy_id'] = $platInfo['appInfo']['proxy_id'];
        $insertData['proxy_name'] = $platInfo['appInfo']['proxy_name'];
        $insertData['p_id'] = $platInfo['platInfo']['p_id'];
        $insertData['order_id'] = $post['orderId'];
        $insertData['amount'] = $post['amount'];
        $insertData['pt_id'] = $platInfo['platInfo']['payType'];
        $insertData['remark'] = $post['remark'];
        $insertData['create_date'] = date('Y-m-d');
        $insertData['client_url'] = $post['clientUrl'];
        $insertData['notify_url'] = $post['notifyUrl'];
        $insertData['referer_ip'] = BooUtil::realIp();
        $insertData['current_point'] = $platInfo['platInfo']['point'];
        $insertData['current_fee_rate'] = $platInfo['platInfo']['point'];

        $autoOrderId = $appPayObj->insert($post['appId'], $insertData);
        if (!$autoOrderId) {
            Common_errorCode::jsonEncode(Common_errorCode::PAY_FAIL);
        }

        // 记录成功率
        $cacheDao = Dao_Redis_Default::getInstance();
        $payLogListCacheKey = BooConfig::get('cacheKey.payLogList');
        $payLogListCacheInfo = $cacheDao->get($payLogListCacheKey);
        if ($payLogListCacheInfo) {
            $payLogListCacheInfo = json_decode($payLogListCacheInfo, true);
            if (count($payLogListCacheInfo) >= 1000) {
                for ($i = 1; $i <= count($payLogListCacheInfo) - 998; $i++) {
                    array_shift($payLogListCacheInfo);
                }
            }
        }

        $tmpArr = array();
        $tmpArr['id'] = $autoOrderId;
        $tmpArr['app_id'] = $post['appId'];
        $tmpArr['app_name'] = $platInfo['appInfo']['name'];
        $tmpArr['amount'] = $post['amount'];
        $tmpArr['real_amount'] = 0;
        $tmpArr['pt_id'] = $platInfo['platInfo']['payType'];
        $tmpArr['p_id'] = $platInfo['platInfo']['p_id'];
        $tmpArr['p_name'] = $platInfo['platInfo']['p_name'];
        $tmpArr['real_amount'] = 0;
        $tmpArr['result'] = 0;
        $tmpArr['create_time'] = time();
        $payLogListCacheInfo[] = $tmpArr;
        $cacheDao->set($payLogListCacheKey, json_encode($payLogListCacheInfo), 3600);

        $appId = BooController::get('Common')->numberAndStrSwap($post['appId']);
        $orderId = BooController::get('Common')->numberAndStrSwap($autoOrderId);
        $platId = BooController::get('Common')->numberAndStrSwap($platInfo['platInfo']['p_id']);

        $platInfo['appInfo']['clientUrl'] = $post['clientUrl'];

        $payInfo = array();
        $payInfo['amount'] = $post['amount'];
        $payInfo['pAutoOrderId'] = $autoOrderId;
        $payInfo['pOrderId'] = "{$appId}1{$orderId}1{$platId}";
        $payInfo['orderId'] = $post['orderId'];

        if (BooUtil::isHttps()) {
            $platInfo['appInfo']['callbackUrl'] = 'https://' . $_SERVER['HTTP_HOST'] . '/payCallback/' . "{$payInfo['pOrderId']}";
        } else {
            $platInfo['appInfo']['callbackUrl'] = 'http://' . $_SERVER['HTTP_HOST'] . '/payCallback/' . "{$payInfo['pOrderId']}";
        }

        // 东方龙支付、聚宝盆只能用http
        if (in_array($platInfo['platInfo']['p_id'], array(41, 42, 71, 105))) {
            $platInfo['appInfo']['callbackUrl'] = 'http://' . $_SERVER['HTTP_HOST'] . '/payCallback/' . "{$payInfo['pOrderId']}";
        }

        if (in_array($platInfo['platInfo']['pt_id'], array(1))) {

            $bankCodeList = array();
            if ($platInfo['platInfo']['bankCode']) {
                if ($platInfo['platInfo']['bankCode']) {
                    foreach ($platInfo['platInfo']['bankCode'] as $code => $value) {
                        $bankCodeList[] = array('key' => $code, 'value' => $value);
                    }
                }
            }

            $platInfo['platInfo']['bankCode'] = $bankCodeList;
            BooController::get("Mod_ThirdPlatform_{$platInfo['platInfo']['platTag']}")->bank($platInfo, $payInfo);
        } else {
            BooController::get("Mod_ThirdPlatform_{$platInfo['platInfo']['platTag']}")->saoma($platInfo, $payInfo);
        }

	}
}
