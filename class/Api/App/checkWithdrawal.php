<?php
class Api_App_checkWithdrawal extends Api_App {
	protected function _do(){
        $post = BooVar::requestx();
        if (!$post['appId'] || !$post['orderId']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        // 缓存锁，避免出现并发
        $cacheKey = BooConfig::get('cacheKey.appCWithdrawalLock') . $post['appId'];
        if (Common_cacheLock::get($cacheKey)) {
            return Common_errorCode::jsonEncode(Common_errorCode::FA_IS_WITHDRAWAL_BUSY);
        }
        $rs = Common_cacheLock::set($cacheKey, 1, 3);
        if (!$rs) {
            return Common_errorCode::jsonEncode(Common_errorCode::FA_IS_WITHDRAWAL_BUSY);
        }		

		$appId 		= $post['appId'];
		$orderId 	= $post['orderId'];
		$obj 		= BooController::get('Obj_App_Withdrawal');
		$orderInfo 	= $obj->getInfoByOrderId($appId, $orderId);
		//判断是否订单存在
		if (!$orderInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::ORDER_NOT_EXIST);
			exit;
        }
		
		
		if($orderInfo['status'] == 2){
			//返回成功
			$data = [
			'refCode' 	=> 0,
			'status' 	=> 'success',
			'appId' 	=> $appId,
			'amount' 	=> $orderInfo['amount'],
			'orderId' 	=> $orderInfo['order_id'],
			'successTime' => strtotime($orderInfo['operate_time']),
			]; 			
		}elseif($orderInfo['status'] == 0 || $orderInfo['status'] == 1){
			//订单受理中
			Common_errorCode::jsonEncode(Common_errorCode::WITHDRAWAL_IS_PROCESSING);
			exit;
		}else{
			//提现失败
			Common_errorCode::jsonEncode(Common_errorCode::WITHDRAWAL_IS_REJECTION);
			exit;
		}
		return $data;
	}
}
