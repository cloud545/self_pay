<?php
class Api_App_getWithdrawalList extends Api_App {

	protected function _do(){

        $post = BooVar::requestx();
        if (!$post['appId'] || !$post['startTime'] || !$post['endTime']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }
        $appId = intval($post['appId']);

        $cacheDao = Dao_Redis_Default::getInstance();
        $cacheKey = BooConfig::get('cacheKey.getWithdrawalList') . $appId;
        $cacheInfo = $cacheDao->get($cacheKey);
        if ($cacheInfo) {
            if (!$post['unLock']) {
                Common_errorCode::jsonEncode(Common_errorCode::API_REQUEST_OFTEN);
            }
        }
        $cacheDao->set($cacheKey, 1, 60);

        $startDate = date('Y-m-d H:i:s', $post['startTime']);
        $endDate = date('Y-m-d H:i:s', $post['endTime']);

        $list = array(
            "list" => array(),
        );

        $obj = BooController::get('Obj_App_Withdrawal');
        $dataList = $obj->getList($appId, 2, $startDate, $endDate);
        if (!$dataList) {
            return $list;
        }

        foreach ($dataList as $info) {

            $tmpArr = array();
            $tmpArr['appId'] = $info['app_id'];
            $tmpArr['amount'] = $info['amount'];
            $tmpArr['fee'] = $info['fee'];
            $tmpArr['bankType'] = $info['bank_type'];
            $tmpArr['bankName'] = $info['bank_name'];
            $tmpArr['cardNo'] = $info['card_no'];
            $tmpArr['cardAccountName'] = $info['card_account_name'];
            $tmpArr['createTime'] = $info['create_time'];
            $list['list'][] = $tmpArr;
        }

        return $list;
	}
}