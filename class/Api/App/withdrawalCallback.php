<?php
class Api_App_withdrawalCallback extends Api_App {

	protected function _do(){

        $post = BooVar::requestx();

        $appWithdrawalInfo = array();
        if (!$post['appWithdrawalInfo']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        } else {
            $appWithdrawalInfo = explode('1', $post['appWithdrawalInfo']);
        }

        $appId = BooController::get('Common')->numberAndStrSwap($appWithdrawalInfo[0], 'decode');
        $payType = BooController::get('Common')->numberAndStrSwap($appWithdrawalInfo[1], 'decode');
        if (!$appId || !$payType) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }

        $content = $_REQUEST;
        if (isset($content['target'])) {
            unset($content['target']);
        }

        if (isset($content['appWithdrawalInfo'])) {
            unset($content['appWithdrawalInfo']);
        }

        if (!$content) {
            $content = file_get_contents("php://input");
        }

        if (is_array($content)) {
            $content = json_encode($content);
        }

        $platInfo = BooController::get('Mod_ThirdPlatform')->getPlatInfo($appId, $payType);
        $withdrawalList = BooController::get("Mod_ThirdPlatform_{$platInfo['p_tag']}")->withdrawalCallback($platInfo);
        if ($withdrawalList) {
            $obj = BooController::get('Obj_App_Withdrawal');

            $nowDateTime = date('Y-m-d H:i:s');
            foreach ($withdrawalList as $withdrawalInfo) {

                $tmpAppWithdrawalInfo = explode('1', $withdrawalInfo['orderId']);
                $tmpAppId = BooController::get('Common')->numberAndStrSwap($tmpAppWithdrawalInfo[0], 'decode');
                $tmpPayType = BooController::get('Common')->numberAndStrSwap($tmpAppWithdrawalInfo[1], 'decode');
                $withdrawalId = BooController::get('Common')->numberAndStrSwap($tmpAppWithdrawalInfo[2], 'decode');

                $info = $obj->getInfoById($withdrawalId);
                if (!$info) {
                    continue;
                }

                $tmpPlatInfo = BooController::get('Mod_ThirdPlatform')->getPlatInfo($tmpAppId, $tmpPayType);

                $updateData = array();
                $updateData['status'] = 2;
                $updateData['remark'] = '';
                $updateData['plat_order_id'] = $withdrawalInfo['platOrderId'];
                $updateData['content'] = $content;
                $updateData['end_time'] = $nowDateTime;

                $data = array();
                $data['platOrderId'] = 'W' . date('Ymd', strtotime($info['create_time'])) . $info['id'];
                $data['orderId'] =  $info['order_id'];
                $data['amount'] =  $info['amount'];
                $data['fee'] =  $info['fee'];
                $data['time'] =  $nowDateTime;
                $data['sign'] = $this->getSign($data, $tmpPlatInfo['appInfo']['app_key']);

                $updateData['notify_data'] = json_encode($data);

                for ($i = 1; $i <= 3; $i++) {
                    $updateData['notify_times'] += 1;

                    BooCurl::setData($data, 'POST');
                    $result = BooCurl::call($info['notify_url']);
                    if ($result == 'success') {
                        $updateData['notify_status'] = 1;
                        break;
                    }
                }

                $updateData['last_notify_time'] = $nowDateTime;
                $obj->update($info['id'], $updateData);
            }
        } else {
            BooFile::write('/tmp/withdrawalCallback.log', $content . "\n\n", 'a');
        }

        exit();
	}
}
