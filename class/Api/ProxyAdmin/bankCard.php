<?php
class Api_ProxyAdmin_bankCard extends Api_ProxyAdmin {

    protected $_tpl = "bankCard.html";

	protected function _do(){

        if ($_POST && in_array($_POST['flag'], array('add', 'update', 'del'))) {

            switch ($_POST['flag']) {
                case 'add':
                    return $this->add();
                case 'update':
                    return $this->update();
                case 'del':
                    return $this->del();
                default:
                    Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
                    return false;
            }
        } else {

            $proxyId = BooSession::get("proxyId");

            $post = BooVar::postx();
            $bankType = $post['bankType'] ? $post['bankType'] : 'all';
            $cardAccountName = $post['cardAccountName'];
            $cardNo = $post['cardNo'];
            $startTime = $post['startTime'];
            $endTime = $post['endTime'];
            $_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数

            $bankNameList = BooConfig::get('main.bankList');
            $obj = BooController::get('Obj_Admin_Banks');
            $pageList = $obj->getPageList(2, $proxyId, $bankType, $cardAccountName, $cardNo, $startTime, $endTime);
            foreach ($pageList['data'] as $key => $info) {

                $info['bankName'] = $bankNameList[$info['bank_type']];
                $pageList['data'][$key] = $info;
            }

            if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
                return $pageList;
            } else {
                BooView::set('bankNameList', $bankNameList);
                BooView::set('pageList', $pageList);
            }
        }

	}

    private function add() {

        $post = BooVar::postx();
        if (!$post['bankType'] || !$post['cardNo'] || !$post['cityId'] || !$post['bankName'] || !$post['cardAccountName'] || !$post['password']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }

        $proxyId = BooSession::get("proxyId");
        $userObj = BooController::get('Obj_Proxy_Info');
        $userInfo = $userObj->getInfoById($proxyId);
        if (!$userInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }

        if (!$userInfo['security_pwd']) {
            Common_errorCode::jsonEncode(Common_errorCode::CASH_PASSWORD_IS_LESS);
        }

        // 判断密码是否正确
        if ($userInfo['security_pwd'] != Common_rule::encodeSecurityPassword($post['password'], $userInfo['security_salt'])) {
            Common_errorCode::jsonEncode(Common_errorCode::PASSWORD_ERR);
        }

        $obj = BooController::get('Obj_Admin_Banks');
        $info = $obj->getInfo(2, $proxyId, $post['cardNo']);
        if ($info) {
            Common_errorCode::jsonEncode(Common_errorCode::BANK_CARD_IS_EXIST);
        }

        $inserData = array(
            'type' => 2,
            'user_id' => $proxyId,
            'bank_type' => $post['bankType'],
            'card_no' => $post['cardNo'],
            'city_id' => $post['cityId'],
            'city_name' => $post['cityName'],
            'bank_name' => $post['bankName'],
            'card_account_name' => $post['cardAccountName'],
            'create_time' => date('Y-m-d H:i:s'),
        );

        $defaultInfo = $obj->getDefaultInfo(2, $proxyId);
        if (!$defaultInfo) {
            $inserData['is_default'] = 1;
        }

        $rs = $obj->insert($inserData);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::INSERT_ERR);
        }

        if ($inserData['is_default']) {
            $bankNameList = BooConfig::get('main.bankList');

            // 数据入库
            $updateData = array();
            $updateData['bank_type'] = $bankNameList[$post['bankType']];
            $updateData['card_no'] = $post['cardNo'];
            $updateData['bank_name'] = $post['bankName'];
            $updateData['card_account_name'] = $post['cardAccountName'];
            $updateData['last_update_time'] = date('Y-m-d H:i:s');
            $rs = $userObj->update($proxyId, $updateData);
            if (!$rs) {
                Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
            }
        }

        return true;
    }

    private function update() {

        $post = BooVar::postx();
        $proxyId = BooSession::get("proxyId");

        if (!$post['id']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        $id = intval($post['id']);

        $obj = BooController::get('Obj_Admin_Banks');
        $info = $obj->getInfoById($id);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::DEFAULT_BANK_NOT_EXIST);
        }

        $updateData = array();
        // 单纯操作是否锁定只更新这个值就行
        if ($post['onOffLock']) {
            if ($info['is_default'] && $post['isLock']) {
                Common_errorCode::jsonEncode(Common_errorCode::DEFAULT_BANK_NOT_CANCEL);
            }
            $updateData['is_lock'] = intval($post['isLock']);
        } elseif ($post['setDefault']) {

            if ($info['is_default'] && !$post['isDefault']) {
                Common_errorCode::jsonEncode(Common_errorCode::DEFAULT_BANK_NOT_CANCEL);
            }

            if (!$info['is_default'] && $post['isDefault']) {
                $defaultInfo = $obj->getDefaultInfo(2, $proxyId);
                $obj->update($defaultInfo['id'], array('is_default' => 0));
            }

            $updateData['is_default'] = intval($post['isDefault']);
        }

        $rs = $obj->update($id, $updateData);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }

        if ($post['isDefault']) {
            $bankNameList = BooConfig::get('main.bankList');
            $userObj = BooController::get('Obj_Proxy_Info');

            // 数据入库
            $updateData = array();
            $updateData['bank_type'] = $bankNameList[$info['bank_type']];
            $updateData['card_no'] = $info['card_no'];
            $updateData['bank_name'] = $info['bank_name'];
            $updateData['card_account_name'] = $info['card_account_name'];
            $updateData['last_update_time'] = date('Y-m-d H:i:s');
            $rs = $userObj->update($proxyId, $updateData);
            if (!$rs) {
                Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
            }
        }

        return true;
    }

    private function del() {
        $id = intval(BooVar::post('id'));
        if (!$id) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }

        $obj = BooController::get('Obj_Admin_Banks');
        $info = $obj->getInfoById($id);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::DEFAULT_BANK_NOT_EXIST);
        }

        if ($info['is_default']) {
            Common_errorCode::jsonEncode(Common_errorCode::DEFAULT_BANK_CARD_UNDEL);
        }

        $rs = $obj->delete($id);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::DELETE_ERR);
        }

        return true;
    }

}