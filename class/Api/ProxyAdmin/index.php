<?php
class Api_ProxyAdmin_index extends Api_ProxyAdmin {

    protected $_tpl = "index.html";

	protected function _do() {

        if ($_POST && in_array($_POST['flag'], array('getInfo', 'withdrawal', 'getBalance'))) {

            switch ($_POST['flag']) {
                case 'getInfo':
                    return $this->getInfo();
                case 'update':
                    return $this->update();
                case 'withdrawal':
                    return $this->withdrawal();
                case 'getBalance':
                    return $this->getBalance();
                default:
                    Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
                    return false;
            }

        } else {

            $proxyId = BooSession::get("proxyId");
            $obj = BooController::get('Obj_Proxy_Info');
            $info = $obj->getInfoById($proxyId);
            $proxyPlatAppInfo = json_decode($info['plat_app_info'], true);

            $obj = BooController::get('Obj_Plat_PayType');
            $tmpPayTypeList = $obj->getList();

            $payTypeList = array();
            foreach ($tmpPayTypeList as $tmpInfo) {
                $payTypeList[$tmpInfo['pt_id']] = $tmpInfo['pt_name'];
            }

            $platList = array();
            foreach ($proxyPlatAppInfo as $platId => $tmpInfo) {
                $platList[$platId] = array(
                    'p_name' => $payTypeList[$tmpInfo['payType']],
                    'point' => $tmpInfo['point'],
                );
            }

            $startTime = date('Y-m-d') . " 00:00:00";
            $endTime = date('Y-m-d H:i:s');
            $payObj = BooController::get('Obj_Proxy_Pay');
            $payCount = $payObj->getCount($proxyId, 0, 0, 0, $startTime, $endTime);
            $paySumInfo = $payObj->getSum($proxyId, 0, 0, 0, $startTime, $endTime);

            // 计算从上次结算到目前总充值有多少钱
            $balanceObj = BooController::get('Obj_Proxy_Balance');
            $proxyBalanceInfo = $balanceObj->getInfoByProxyId($proxyId);

            // 读取代理未提款笔数
            $yesterday = date('Y-m-d', strtotime($startTime) - 86400) . " 00:00:00";
            $proxyWithdrawalObj = BooController::get('Obj_Proxy_Withdrawal');
            $applyWithdrawalTimes = $proxyWithdrawalObj->getCount($proxyId, array(0, 1), $yesterday, $endTime);
            $successWithdrawalTimes = $proxyWithdrawalObj->getCount($proxyId, 2, $yesterday, $endTime);
            $sumWithdrawalInfo = $proxyWithdrawalObj->getSum($proxyId, 2, $yesterday, $endTime);

            BooView::set('balance', $proxyBalanceInfo['available_balance']);
            BooView::set('frozenBalance', $proxyBalanceInfo['frozen_balance']);
            BooView::set('payTimes', $payCount ? $payCount : 0);
            BooView::set('paySum', $paySumInfo['sumAmount'] ? $paySumInfo['sumAmount'] : 0);
            BooView::set('payFee', $paySumInfo['sumFee'] ? $paySumInfo['sumFee'] : 0);
            BooView::set('applyWithdrawalTimes', $applyWithdrawalTimes);
            BooView::set('successWithdrawalTimes', $successWithdrawalTimes);
            BooView::set('withdrawalSum', $sumWithdrawalInfo['sumAmount'] ? $sumWithdrawalInfo['sumAmount'] : 0);
            BooView::set('withdrawalFee', $sumWithdrawalInfo['sumFee'] ? $sumWithdrawalInfo['sumFee'] : 0);
            BooView::set('platList', $platList);
        }

	}
        
        private function getBalance() {

        $appId = BooSession::get("proxyId");
	    if (!$appId) {
            return false;
        }

        $balanceObj = BooController::get('Obj_Proxy_Balance');
        $appBalanceInfo = $balanceObj->getInfoByProxyId($appId);

        return array('balance' => $appBalanceInfo['available_balance'], 'frozenBalance' => $appBalanceInfo['frozen_balance']);
    }

	private function getInfo() {

        $proxyId = BooSession::get("proxyId");
        $obj = BooController::get('Obj_Proxy_Info');
        $info = $obj->getInfoById($proxyId);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }

        // 00 点暂不结算，等日汇总结算后再进行结算，否则会导致商户余额在5分钟内全部显示为0
        $startTime = date('Y-m-d') . " 00:00:00";
        if (time() < (strtotime($startTime) + 600)) {
            Common_errorCode::jsonEncode(Common_errorCode::SYSTEM_DAY_SETTLE_ACCOUNTS);
        }

        $balance = BooController::get('Mod_Proxy')->getBalance($proxyId);
        if ($balance === false) {
            Common_errorCode::jsonEncode(Common_errorCode::NETWORK_FLUCTUATION);
        }

        $returnData = array();
        $returnData['bank_type'] = $info['bank_type'] ? $info['bank_type'] : '未设置';
        $returnData['card_no'] = $info['card_no'] ? $info['card_no'] : '未设置';
        $returnData['bank_name'] = $info['bank_name'] ? $info['bank_name'] : '未设置';
        $returnData['card_account_name'] = $info['card_account_name'] ? $info['card_account_name'] : '未设置';
        $returnData['available_balance'] = $balance ? $balance : 0;
        $returnData['google_secret_isopen'] = $info['google_secret_isopen'];

        return $returnData;
    }

    private function withdrawal() {

        $post = BooVar::postx();

        if (!Common_rule::checkMoneyNumberSafe($post['money'])) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }

        if ($post['money'] <= 0 || !$post['password']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }

        // 00 点暂不结算，等日汇总结算后再进行结算，否则会导致商户余额在5分钟内全部显示为0
        $startTime = date('Y-m-d') . " 00:00:00";
        if (time() < (strtotime($startTime) + 600)) {
            Common_errorCode::jsonEncode(Common_errorCode::SYSTEM_DAY_SETTLE_ACCOUNTS);
        }

        $proxyId = BooSession::get("proxyId");
        $obj = BooController::get('Obj_Proxy_Info');
        $info = $obj->getInfoById($proxyId);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }

        $balance = BooController::get('Mod_Proxy')->getBalance($proxyId);
        if ($balance === false) {
            Common_errorCode::jsonEncode(Common_errorCode::NETWORK_FLUCTUATION);
        }

        $balanceObj = BooController::get('Obj_Proxy_Balance');
        $proxyBalanceInfo = $balanceObj->getInfoByProxyId($proxyId);

        if (!$info['card_no'] || !$info['bank_name'] || !$info['card_account_name']) {
            Common_errorCode::jsonEncode(Common_errorCode::CARD_NO_INFO_IS_LESS);
        }

        if ($balance < 0) {
            Common_errorCode::jsonEncode(Common_errorCode::CASH_IS_NOT_ENOUGH);
        }

        if ($post['money'] + $info['withdrawal_fee'] > $balance) {
            Common_errorCode::jsonEncode(Common_errorCode::CASH_IS_NOT_ENOUGH);
        }

        if (!$info['security_pwd']) {
            Common_errorCode::jsonEncode(Common_errorCode::CASH_PASSWORD_IS_LESS);
        }

        if ($info['security_pwd'] != Common_rule::encodeSecurityPassword($post['password'], $info['security_salt'])) {
            Common_errorCode::jsonEncode(Common_errorCode::PASSWORD_ERR);
        }

        if ($info['google_secret_isopen']) {
            if (!$post['googleCode']) {
                Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
            }

            $checkCodeRs = BooGoogleAuthor::getInstance()->verifyCode($info['google_secret_code'], $_POST['googleCode']);
            if (!$checkCodeRs) {
                Common_errorCode::jsonEncode(Common_errorCode::GOOGLE_CODE_IS_ERROR);
            }
        }

        $cacheKey = BooConfig::get('cacheKey.proxyWithdrawalLock') . $proxyId;
        if (Common_cacheLock::get($cacheKey)) {
            return Common_errorCode::jsonEncode(Common_errorCode::FA_IS_WITHDRAWAL_BUSY);
        }
        $rs = Common_cacheLock::set($cacheKey, 1, 3);
        if (!$rs) {
            return Common_errorCode::jsonEncode(Common_errorCode::FA_IS_WITHDRAWAL_BUSY);
        }

        $inserData = array(
            'proxy_id' => $info['proxy_id'],
            'proxy_name' => $info['proxy_name'],
            'amount' => $post['money'],
            'fee' => $info['withdrawal_fee'],
            'bank_type' => $info['bank_type'],
            'card_no' => $info['card_no'],
            'bank_name' => $info['bank_name'],
            'card_account_name' => $info['card_account_name'],
            'create_time' => date('Y-m-d H:i:s'),
            'status' => 0,
            'apply_user' => BooSession::get('proxyName'),

        );

        $obj = BooController::get('Obj_Proxy_Withdrawal');
        $rs = $obj->insert($inserData);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::INSERT_ERR);
        }

        $updateData = array();
        $updateData['available_balance'] = $balance - $post['money'] - $info['withdrawal_fee'];
        $updateData['frozen_amount'] = $proxyBalanceInfo['frozen_amount'] + $post['money'] + $info['withdrawal_fee'];
        $updateData['last_modify_time'] = date('Y-m-d H:i:s');
        $rs = $balanceObj->update($proxyBalanceInfo['id'], $updateData);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }

        // 删除锁
        Common_cacheLock::del($cacheKey);

        return true;
    }

}