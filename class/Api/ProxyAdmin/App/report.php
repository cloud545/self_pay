<?php
class Api_ProxyAdmin_App_report extends Api_ProxyAdmin_App {

    protected $_tpl = "appReport.html";

	protected function _do() {

        if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {

            $post = BooVar::postx();
            if ($post['appId']) {
                $appId = intval($post['appId']);
                $startTime = $post['startTime'];
                $endTime = $post['endTime'];
                $_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数

                $obj = BooController::get('Obj_App_Report');
                $pageList = $obj->getPageList($appId, $startTime, $endTime);
                $sumInfo = $obj->getSum($appId, $startTime, $endTime);
                $pageList = array_merge($pageList, $sumInfo);

                return $pageList;
            }

            return true;
        } else {

            $proxyId = BooSession::get("proxyId");

            $appObj = BooController::get('Obj_App_Info');
            $tmpAppList = $appObj->getListByProxyId($proxyId);

            $appList = array();
            foreach ($tmpAppList as $info) {
                $appList[$info['app_id']] = $info['name'];
            }

            BooView::set('appList', $appList);
        }
	}

}