<?php
class Api_ProxyAdmin_App_list extends Api_ProxyAdmin_App {

    protected $_tpl = "appList.html";

	protected function _do() {

        if ($_POST && in_array($_POST['flag'], array('add', 'update', 'del', 'changeKey'))) {

            switch ($_POST['flag']) {
                case 'add':
                    return $this->add();
                case 'update':
                    return $this->update();
                case 'del':
                    return $this->del();
                case 'changeKey':
                    return $this->changeKey();
                default:
                    Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
                    return false;
            }

        } else {
            $proxyId = BooSession::get("proxyId");
            $post = BooVar::postx();
            $_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数
            $obj = BooController::get('Obj_Plat_PayType');
            $tmpPayTypeList = $obj->getList();

            $payTypeList = array();
            foreach ($tmpPayTypeList as $info) {
                $payTypeList[$info['pt_id']] = $info['pt_name'];
            }

            $obj = BooController::get('Obj_Proxy_Info');
            $info = $obj->getInfoById($proxyId);
            $proxyPlatAppInfo = json_decode($info['plat_app_info'], true);

            $platList = array();
            foreach ($proxyPlatAppInfo as $platId => $tmpInfo) {
                $platList[$platId] = array(
                    'pt_id' => $tmpInfo['payType'],
                    'p_id' => $platId,
                    'p_name' => $payTypeList[$tmpInfo['payType']],
                );
            }

            $obj = BooController::get('Obj_App_Info');
            $pageList = $obj->getPageList(0, '', $proxyId);

            foreach ($pageList['data'] as $key => $info) {

                $tmpArr = array();
                $tmpArr['id'] = $info['id'];
                $tmpArr['name'] = $info['name'];
                $tmpArr['app_id'] = $info['app_id'];
                $tmpArr['create_time'] = $info['create_time'];
                $tmpArr['last_time'] = $info['last_update_time'];
                $tmpArr['is_lock'] = $info['is_lock'];

                $platNameList = '';
                if ($info['plat_app_info']) {
                    $info['plat_app_info'] = json_decode($info['plat_app_info'], true);

                    foreach ($info['plat_app_info'] as $platId => $platInfo) {
                        if ($platNameList) {
                            $platNameList .= '<br/>' . $payTypeList[$platInfo['payType']] . "({$platInfo['point']})";
                        } else {
                            $platNameList = $payTypeList[$platInfo['payType']] . "({$platInfo['point']})";
                        }
                    }
                    $tmpArr['openPlatList'] = $platNameList;

                }
                $tmpArr['plat_app_info'] = $info['plat_app_info'];
                $pageList['data'][$key] = $tmpArr;
            }

            if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
                return $pageList;
            } else {
                BooView::set('platList', $platList);
                BooView::set('pageList', $pageList);
            }
        }
	}

    private function add() {

        $post = BooVar::postx();
        if (!$post['name'] || !$post['password']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }

        $proxyName = BooSession::get("proxyName");

        $appObj = BooController::get('Obj_App_Info');
        $userInfo = $appObj->getInfoByName($post['name']);
        if ($userInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_IS_EXIST);
        }

        $salt = Common_rule::getRandSaltStr();
        $password = Common_rule::encodeLoginPassword($post['password'], $salt);

        $appId = 0;
        while (1) {
            $appId = rand(1000, 9999);
            $rs = $appObj->getInfoByAppId($appId);
            if (!$rs) {
                break;
            }
        }

        $proxyId = BooSession::get("proxyId");
        $obj = BooController::get('Obj_Proxy_Info');
        $info = $obj->getInfoById($proxyId);
        $proxyPlatAppInfo = json_decode($info['plat_app_info'], true);

        $platAppInfo = array();
        if ($post['platId']) {

            $platObj = BooController::get('Obj_Plat_Info');

            $platTypeList = array();
            foreach ($post['platId'] as $key => $value) {

                $platInfo = $platObj->getInfoById($value);
                if (!$platInfo) {
                    continue;
                }

                if ($post['appPoint'][$key] <= 0) {
                    Common_errorCode::jsonEncode(Common_errorCode::POINT_IS_ZERO);
                }
                //点数分配不合理提示
                if ($post['appPoint'][$key] >= 1) {
                    Common_errorCode::jsonEncode(Common_errorCode::POINT_IS_WRONG);
                }
                if ($post['appPoint'][$key] < $proxyPlatAppInfo[$key]['point']) {
                    Common_errorCode::jsonEncode(Common_errorCode::POINT_IS_LOW);
                }

                $platAppInfo[$value] = array(
                    'platTag' => $platInfo['p_tag'],
                    'payType' => $platInfo['pt_id'],
                    'point' => $post['appPoint'][$key],
                );

                $platTypeList[$platInfo['pt_id']] += 1;

                if ($platTypeList[$platInfo['pt_id']] > 1) {
                    Common_errorCode::jsonEncode(Common_errorCode::ONLY_ONE_PAY_TYPE);
                }
            }
        }

        $inserData = array(
            'name' => $post['name'],
            'password' => $password,
            'proxy_id' => $proxyId,
            'proxy_name' => $proxyName,
            'salt' => $salt,
            'app_id' => $appId,
            'app_key' => md5($appId . time() . rand(1, 99)),
            'plat_app_info' => $platAppInfo ? json_encode($platAppInfo) : '',
            'create_time' => date('Y-m-d H:i:s'),
            'withdrawal_fee' => $info['withdrawal_fee'],
        );

        $rs = $appObj->insert($inserData);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::INSERT_ERR);
        }

        $balanceObj = BooController::get('Obj_App_Balance');
        $inserData = array(
            'app_id' => $appId,
            'app_name' => $post['name'],
        );
        $balanceObj->insert($inserData);

        return true;
    }

    private function update() {
        $post = BooVar::postx();

        if (!$post['id']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        $id = intval($post['id']);

        $appObj = BooController::get('Obj_App_Info');
        $info = $appObj->getInfoById($id);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }

        $updateData = array();
        // 单纯操作是否锁定只更新这个值就行
        if ($post['onOffLock']) {
            $updateData['is_lock'] = intval($post['isLock']);
        }

        $proxyId = BooSession::get("proxyId");
        $obj = BooController::get('Obj_Proxy_Info');
        $info = $obj->getInfoById($proxyId);
        $proxyPlatAppInfo = json_decode($info['plat_app_info'], true);

        if ($post['platId']) {

            $platObj = BooController::get('Obj_Plat_Info');

            $platTypeList = array();
            $platAppInfo = array();
            foreach ($post['platId'] as $key => $platId) {

                $platInfo = $platObj->getInfoById($platId);
                if (!$platInfo) {
                    continue;
                }

                if ($post['appPoint'][$key] <= 0) {
                    Common_errorCode::jsonEncode(Common_errorCode::POINT_IS_ZERO);
                }

                //点数分配不合理提示
                if ($post['appPoint'][$key] >= 1) {
                    Common_errorCode::jsonEncode(Common_errorCode::POINT_IS_WRONG);
                }

                if ($post['appPoint'][$key] < $proxyPlatAppInfo[$platId]['point']) {
                    Common_errorCode::jsonEncode(Common_errorCode::POINT_IS_LOW);
                }

                $platAppInfo[$platId] = array(
                    'platTag' => $platInfo['p_tag'],
                    'payType' => $platInfo['pt_id'],
                    'point' => $post['appPoint'][$key],
                );

                $platTypeList[$platInfo['pt_id']] += 1;

                if ($platTypeList[$platInfo['pt_id']] > 1) {
                    Common_errorCode::jsonEncode(Common_errorCode::ONLY_ONE_PAY_TYPE);
                }
            }

            $updateData['plat_app_info'] = $platAppInfo ? json_encode($platAppInfo) : '';
        } else {
            $updateData['plat_app_info'] = '';
        }

        $rs = $appObj->update($id, $updateData);
        /*
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }
        // */

        return true;
    }

    private function del() {
        $id = intval(BooVar::post('id'));
        if (!$id) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }

        $obj = BooController::get('Obj_App_Info');
        $userInfo = $obj->getInfoById($id);
        if (!$userInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }

        $rs = $obj->delete($id);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::DELETE_ERR);
        }

        return true;
    }

    private function changeKey() {
        $key = md5(time() . rand(1, 99));
        return array('key' => $key);
    }

}