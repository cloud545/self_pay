<?php
class Api_ProxyAdmin_App_payList extends Api_ProxyAdmin_App {

    protected $_tpl = "appPayList.html";

	protected function _do() {

        $proxyId = BooSession::get("proxyId");
        $post = BooVar::postx();
        $appId = intval($post['appId']);
        $platId = intval($post['platId']);
        $id = $post['tradeNo'] ? substr($post['tradeNo'],9) : 0;
        $type = $post['type'];
        $startTime = $post['startTime'];
        $endTime = $post['endTime'];
        $_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数

        $get = BooVar::getx();
        if ($get['appId']) {
            $appId = $get['appId'];
            BooView::set('searchAppId', $get['appId']);
        }

        if ($get['tradeNo']) {
            $id = substr($get['tradeNo'],9);
            BooView::set('tradeNo', $get['tradeNo']);
        }

        if ($get['startTime']) {
            $startTime = $get['startTime'];
            $endTime = $get['endTime'];
            BooView::set('startTime', $get['startTime']);
            BooView::set('endTime', $get['endTime']);
        }

        $obj = BooController::get('Obj_Proxy_Pay');
        $pageList = $obj->getPageList($proxyId, $platId, $id, $type, $startTime, $endTime, $appId);
        $sumInfo = $obj->getSum($proxyId, $platId, $id, $type, $startTime, $endTime, $appId);

        if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {

            return array_merge($pageList, $sumInfo);
        } else {

            $proxyId = BooSession::get("proxyId");
            $obj = BooController::get('Obj_Plat_PayType');
            $tmpPayTypeList = $obj->getList();

            $payTypeList = array();
            foreach ($tmpPayTypeList as $info) {
                $payTypeList[$info['pt_id']] = $info['pt_name'];
            }

            $obj = BooController::get('Obj_Proxy_Info');
            $info = $obj->getInfoById($proxyId);
            $proxyPlatAppInfo = json_decode($info['plat_app_info'], true);

            $platList = array();
            foreach ($proxyPlatAppInfo as $platId => $tmpInfo) {
                $platList[$platId] = $payTypeList[$tmpInfo['payType']];
            }

            $appObj = BooController::get('Obj_App_Info');
            $tmpAppList = $appObj->getListByProxyId($proxyId);

            $appList = array();
            foreach ($tmpAppList as $info) {
                $appList[$info['app_id']] = $info['name'];
            }

            BooView::set('appList', $appList);
            BooView::set('platList', $platList);
            BooView::set('pageList', $pageList);
            BooView::set('sumInfo', $sumInfo);
        }
	}
}