<?php
class Api_ProxyAdmin_Proxy_withdrawalList extends Api_ProxyAdmin_Proxy {

    protected $_tpl = "withdrawalList.html";

	protected function _do() {

        $proxyId = BooSession::get("proxyId");
        $post = BooVar::postx();
        $status = $post['status'] ? intval($post['status']) : 'all';
        $startTime = $post['startTime'];
        $endTime = $post['endTime'];

        $get = BooVar::getx();
        if ($get['tradeNo']) {
            $post['tradeNo'] = $get['tradeNo'];
            BooView::set('tradeNo', $get['tradeNo']);
        }

        if ($post['tradeNo']) {
            if (!Common_rule::checkStrSafe($post['tradeNo'])) {
                $id = 0;
            } else {
                $id = substr($post['tradeNo'], 10);
            }
        } else {
            $id = 0;
        }

        $_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数

        $obj = BooController::get('Obj_Proxy_Withdrawal');
        $pageList = $obj->getPageList($proxyId, $status, $startTime, $endTime, $id);
        foreach ($pageList['data'] as $key => $info) {
            $info['tradeNo'] = 'PW' . date('Ymd', strtotime($info['create_time'])) . $info['id'];

            if ($info['status'] == 1) {
                $info['statusName'] = '银行处理中';
            } else if ($info['status'] == 2) {
                $info['statusName'] = '提款成功';
            } else if ($info['status'] == 3) {
                $info['statusName'] = '提款失败';
            } else {
                $info['statusName'] = '系统处理中';
            }

            $pageList['data'][$key] = $info;
        }

        if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
            return $pageList;
        } else {
            BooView::set('pageList', $pageList);
        }
	}

}