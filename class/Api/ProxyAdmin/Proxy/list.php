<?php
class Api_ProxyAdmin_Proxy_list extends Api_ProxyAdmin_Proxy {

    protected $_tpl = "proxyList.html";

	protected function _do() {

        if ($_POST && in_array($_POST['flag'], array('add', 'update', 'del'))) {

            switch ($_POST['flag']) {
                case 'add':
                    return $this->add();
                case 'update':
                    return $this->update();
                case 'del':
                    return $this->del();
                default:
                    Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
                    return false;
            }

        } else {

            $proxyId = BooSession::get("proxyId");
            $obj = BooController::get('Obj_Plat_PayType');
            $tmpPayTypeList = $obj->getList();

            $payTypeList = array();
            foreach ($tmpPayTypeList as $info) {
                $payTypeList[$info['pt_id']] = $info['pt_name'];
            }

            $obj = BooController::get('Obj_Proxy_Info');
            $proxyInfo = $obj->getInfoById($proxyId);
            $proxyPlatAppInfo = json_decode($proxyInfo['plat_app_info'], true);

            $platList = array();
            foreach ($proxyPlatAppInfo as $platId => $tmpInfo) {
                $platList[$platId] = array(
                    'pt_id' => $tmpInfo['payType'],
                    'p_id' => $platId,
                    'p_name' => $payTypeList[$tmpInfo['payType']],
                );
            }

            $obj = BooController::get('Obj_Proxy_Info');
            $pageList = $obj->getPageList(0, '', $proxyId);

            foreach ($pageList['data'] as $key => $info) {

                $tmpArr = array();
                $tmpArr['proxy_id'] = $info['proxy_id'];
                $tmpArr['proxy_name'] = $info['proxy_name'];
                $tmpArr['create_time'] = $info['create_time'];
                $tmpArr['last_time'] = $info['last_update_time'];
                $tmpArr['is_lock'] = $info['is_lock'];

                $platNameList = '';
                if ($info['plat_app_info']) {
                    $info['plat_app_info'] = json_decode($info['plat_app_info'], true);

                    foreach ($info['plat_app_info'] as $platId => $platInfo) {
                        if ($platNameList) {
                            $platNameList .= '<br/>' . $payTypeList[$platInfo['payType']] . "({$platInfo['point']})";
                        } else {
                            $platNameList = $payTypeList[$platInfo['payType']] . "({$platInfo['point']})";
                        }
                    }
                    $tmpArr['openPlatList'] = $platNameList;

                }
                $tmpArr['isOpenTree'] = $info['is_open_tree'] ? '是' : '否';
                $tmpArr['plat_app_info'] = $info['plat_app_info'];
                $tmpArr['is_open_tree'] = $info['is_open_tree'];
                $pageList['data'][$key] = $tmpArr;
            }

            if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
                return $pageList;
            } else {
                BooView::set('platList', $platList);
                BooView::set('pageList', $pageList);
            }
        }
	}

    private function add() {

        $post = BooVar::postx();
        if (!$post['name'] || !$post['password']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }

        $obj = BooController::get('Obj_Proxy_Info');
        $userInfo = $obj->getInfoByName($post['name']);
        if ($userInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_IS_EXIST);
        }

        $salt = Common_rule::getRandSaltStr();
        $password = Common_rule::encodeLoginPassword($post['password'], $salt);

        $proxyId = BooSession::get("proxyId");
        $obj = BooController::get('Obj_Proxy_Info');
        $info = $obj->getInfoById($proxyId);
        $proxyPlatAppInfo = json_decode($info['plat_app_info'], true);

        $platAppInfo = array();
        if ($post['platId']) {

            $platObj = BooController::get('Obj_Plat_Info');

            $platTypeList = array();
            foreach ($post['platId'] as $key => $value) {

                $platInfo = $platObj->getInfoById($value);
                if (!$platInfo) {
                    continue;
                }

                if ($post['appPoint'][$key] <= 0) {
                    Common_errorCode::jsonEncode(Common_errorCode::POINT_IS_ZERO);
                }

                if ($post['appPoint'][$key] < $proxyPlatAppInfo[$key]['point']) {
                    Common_errorCode::jsonEncode(Common_errorCode::POINT_IS_LOW);
                }

                $platAppInfo[$value] = array(
                    'platTag' => $platInfo['p_tag'],
                    'payType' => $platInfo['pt_id'],
                    'point' => $post['appPoint'][$key],
                );

                $platTypeList[$platInfo['pt_id']] += 1;

                if ($platTypeList[$platInfo['pt_id']] > 1) {
                    Common_errorCode::jsonEncode(Common_errorCode::ONLY_ONE_PAY_TYPE);
                }
            }
        }

        $userTree = '';
        if ($info['user_tree']) {
            $userTree = "{$info['user_tree']},{$proxyId}";
        } else {
            $userTree = "{$proxyId}";
        }

        $inserData = array(
            'proxy_name' => $post['name'],
            'proxy_pwd' => $password,
            'salt' => $salt,
            'plat_app_info' => $platAppInfo ? json_encode($platAppInfo) : '',
            'create_time' => date('Y-m-d H:i:s'),
            'parent_id' => $proxyId,
            'parent_name' => BooSession::get('proxyName'),
            'user_tree' => $userTree,
            'is_open_tree' => intval($post['isOpenTree']),
        );

        $proxyId = $obj->insert($inserData);
        if (!$proxyId) {
            Common_errorCode::jsonEncode(Common_errorCode::INSERT_ERR);
        }

        $balanceObj = BooController::get('Obj_Proxy_Balance');
        $inserData = array(
            'proxy_id' => $proxyId,
            'proxy_name' => $post['name'],
        );
        $balanceObj->insert($inserData);

        return true;
    }

    private function update() {
        $post = BooVar::postx();

        if (!$post['id']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        $id = intval($post['id']);

        $obj = BooController::get('Obj_Proxy_Info');
        $info = $obj->getInfoById($id);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }

        $updateData = array();
        // 单纯操作是否锁定只更新这个值就行
        if ($post['onOffLock']) {
            $updateData['is_lock'] = intval($post['isLock']);
        }

        $proxyId = BooSession::get("proxyId");
        $obj = BooController::get('Obj_Proxy_Info');
        $info = $obj->getInfoById($proxyId);
        $proxyPlatAppInfo = json_decode($info['plat_app_info'], true);

        if ($post['platId']) {

            $platObj = BooController::get('Obj_Plat_Info');

            $platTypeList = array();
            $platAppInfo = array();
            foreach ($post['platId'] as $key => $platId) {

                $platInfo = $platObj->getInfoById($platId);
                if (!$platInfo) {
                    continue;
                }

                if ($post['appPoint'][$key] <= 0) {
                    Common_errorCode::jsonEncode(Common_errorCode::POINT_IS_ZERO);
                }

                if ($post['appPoint'][$key] < $proxyPlatAppInfo[$platId]['point']) {
                    Common_errorCode::jsonEncode(Common_errorCode::POINT_IS_LOW);
                }

                $platAppInfo[$platId] = array(
                    'platTag' => $platInfo['p_tag'],
                    'payType' => $platInfo['pt_id'],
                    'point' => $post['appPoint'][$key],
                );

                $platTypeList[$platInfo['pt_id']] += 1;

                if ($platTypeList[$platInfo['pt_id']] > 1) {
                    Common_errorCode::jsonEncode(Common_errorCode::ONLY_ONE_PAY_TYPE);
                }
            }

            $updateData['plat_app_info'] = $platAppInfo ? json_encode($platAppInfo) : '';
        } else {
            $updateData['plat_app_info'] = '';
        }

        $rs = $obj->update($id, $updateData);
        /*
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }
        // */

        return true;
    }

    private function del() {
        $id = intval(BooVar::post('id'));
        if (!$id) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }

        $obj = BooController::get('Obj_Proxy_Info');
        $userInfo = $obj->getInfoById($id);
        if (!$userInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }

        $rs = $obj->delete($id);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::DELETE_ERR);
        }

        return true;
    }

}