<?php
class Api_ProxyAdmin_Proxy_payList extends Api_ProxyAdmin_Proxy {

    protected $_tpl = "payList.html";

	protected function _do() {

        $post = BooVar::postx();
        $proxyId = BooSession::get("proxyId");
        $platId = intval($post['platId']);

        if ($post['tradeNo']) {
            if (!Common_rule::checkStrSafe($post['tradeNo'])) {
                $id = 0;
            } else {
                $id = substr($post['tradeNo'], 10);
            }
        } else {
            $id = 0;
        }

        $type = intval($post['type']);
        $startTime = $post['startTime'];
        $endTime = $post['endTime'];
        $_GET['pageId'] = $post['page'] ? intval($post['page']) : 1;// 查询的页数

        $get = BooVar::getx();
        if ($get['endTime']) {

            $startTime = $get['startTime'];
            $endTime = $get['endTime'];

            BooView::set('startTime', $get['startTime']);
            BooView::set('endTime', $get['endTime']);
        }

        $obj = BooController::get('Obj_Proxy_Pay');
        $pageList = $obj->getPageList($proxyId, $platId, $id, $type, $startTime, $endTime);

        foreach ($pageList['data'] as $key => $info) {

            $info['tradeNo'] = 'P' . date('Ymd', strtotime($info['create_time'])) . $info['pay_id'];
            $pageList['data'][$key] = $info;
        }

        $sumInfo = $obj->getSum($proxyId, $platId, $id, $type, $startTime, $endTime);

        if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {

            return array_merge($pageList, $sumInfo);
        } else {

            $payTypeObj = BooController::get('Obj_Plat_PayType');
            $tmpPayTypeList = $payTypeObj->getList();

            $payTypeList = array();
            foreach ($tmpPayTypeList as $info) {
                $payTypeList[$info['pt_id']] = $info['pt_name'];
            }

            $obj = BooController::get('Obj_Proxy_Info');
            $info = $obj->getInfoById($proxyId);
            $proxyPlatAppInfo = json_decode($info['plat_app_info'], true);

            $platList = array();
            foreach ($proxyPlatAppInfo as $platId => $tmpInfo) {
                $platList[$platId] = $payTypeList[$tmpInfo['payType']];
            }

            BooView::set('platList', $platList);
            BooView::set('pageList', $pageList);
            BooView::set('sumInfo', $sumInfo);
        }
	}
}