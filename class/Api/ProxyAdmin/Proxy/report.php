<?php
class Api_ProxyAdmin_Proxy_report extends Api_ProxyAdmin_Proxy {

    protected $_tpl = "proxyReport.html";

	protected function _do() {

        if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {

            $post = BooVar::postx();
            if ($post['proxyId']) {

                $proxyId = $post['proxyId'];
                $startTime = $post['startTime'];
                $endTime = $post['endTime'];
                $_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数

                $obj = BooController::get('Obj_Proxy_Report');
                $pageList = $obj->getPageList($proxyId, $startTime, $endTime);

                return $pageList;
            }
            return true;
        } else {

            $proxyId = BooSession::get("proxyId");

            $obj = BooController::get('Obj_Proxy_Info');
            $tmpProxyList = $obj->getList($proxyId);

            $proxyList = array();
            foreach ($tmpProxyList as $info) {
                $proxyList[$info['proxy_id']] = $info['proxy_name'];
            }

            BooView::set('proxyList', $proxyList);
        }

	}

}