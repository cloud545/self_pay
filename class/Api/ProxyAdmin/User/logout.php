<?php
class Api_ProxyAdmin_User_logout extends Api_ProxyAdmin_User{

	protected function _do(){

        BooSession::destroy();
        header('Location: /proxyAdmin/user/login');
	    return true;
	}
}