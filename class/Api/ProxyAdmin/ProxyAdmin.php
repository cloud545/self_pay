<?php
class Api_ProxyAdmin extends Api{

    protected $_tpl = "";

    public function run() {

        // 设置模板配置
        $this->setSmartyTpl();

        // 安全验证
        if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
            $this->checkToken();
        }

        // 验证用户是否已经登录和是否锁定，登录页不需要验证是否已经登录
        if (!in_array(BooVar::get('target'), array('proxyAdmin/user/login'))) {
            $this->checkLogin();

            // 设置导航按钮
            $this->setMenuList();

            // 记录管理员行为
            BooController::get('Mod_Admin')->addProxyAdminLogs();
        }

        // 每个请求逻辑处理，一般在子类中处理
        $data = $this->_do();

        $this->setToken();

        // 区分ajax和表单请求数据
        if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
            if ($data === true || $data === false) {
                $data = array();
            }

            Common_errorCode::jsonEncode(Common_errorCode::SUCCESS, $data);
        } else {
            header("Access-Control-Allow-Origin: *");
            header("Content-type: text/html;charset=utf-8");

            if ($this->_tpl) {
                BooView::set("webCdnUrl", BooConfig::get("main.webCdnUrl"));
                BooView::set("adminName", BooSession::get('proxyNickname'));
                BooView::display($this->_tpl);
            } else {
                header('Location: /proxyAdmin/index');
            }
        }
    }

    private function setSmartyTpl() {

        BooView::setOptions(array(
            "PATH_TPL" => PATH_TPL . "/proxyAdmin",
            "PATH_COMPILE" => PATH_DATA . "/proxyAdmin/smarty/compile",
            "DELIMITER" => array("<%", "%>"),
        ));
    }

    //子类要重载这个方法
	protected function _do() {

	}

    /**
     * 更新http请求安全公钥
     *
     * @throws ErrorException
     */
    private function setToken() {

        $microtime = microtime(true);
        $randomKey = $microtime . rand(1, 1000);
        $key = md5($randomKey);

        BooSession::set('proxyHttpRequestCsrfToken', $key);
        header("X-CSRF-TOKEN: {$key}");
        BooView::set('csrfToken', $key);
    }

    private function checkToken() {
        $adminHttpRequestSign = BooSession::get('proxyHttpRequestCsrfToken');
        if (!$adminHttpRequestSign) {
            Common_errorCode::jsonEncode(Common_errorCode::LONG_NO_OPERATE);
        }

        if ($_SERVER['HTTP_X_CSRF_TOKEN'] != $adminHttpRequestSign) {
            Common_errorCode::jsonEncode(Common_errorCode::SIGN_ERR);
        }
    }

    private function checkLogin() {

        if (!BooSession::get('proxyId')) {
            header('Location: /proxyAdmin/user/login');
            exit;
        }

        if (BooSession::get('proxyIsLock')) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_LOCKED);
        }
    }

    private function setMenuList() {

        $floorMenuList = BooController::get('Mod_Admin')->getProxyMenuList();
        $powerTargetArr = array();

        $isOpenTree = BooSession::get('proxyIsOpenTree');

        // 去掉一级菜单里面的key
        $floorMenuList = array_values($floorMenuList);
        $menuList = array();
        foreach ($floorMenuList as $key => $info) {

            // 看不到下级代理列 $info['menu_id'] == 2
            if (!$isOpenTree && $key == 2) {
                continue;
            }

            $menuList[$key] = $info;

            if (isset($info['childList']) && $info['childList']) {
                // 去掉一级菜单里面childList 字段数组的key
                $tmpArr = array_values($info['childList']);
                $menuList[$key]['childList'] = $tmpArr;

                foreach ($tmpArr as $key1 => $info1) {

                    $powerTargetArr[] = $info1['menu_target'];

                    $menuList[$key]['childList'][$key1] = $info1;
                    if (isset($info1['childList']) && $info['childList']) {
                        // 去掉二级菜单里面childList 字段数组的key
                        $tmpArr1 = array_values($info1['childList']);
                        $menuList[$key]['childList'][$key1]['childList'] = $tmpArr1;
                    }
                }
            }
        }

        BooView::set('navMenuList', $menuList);
        BooView::set('menuList', json_encode($menuList));
    }

}