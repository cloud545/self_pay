<?php
class Api_ProxyAdmin_report extends Api_ProxyAdmin {

    protected $_tpl = "report.html";

	protected function _do() {

        $post = BooVar::postx();
        $proxyId = BooSession::get("proxyId");
        $startTime = $post['startTime'];
        $endTime = $post['endTime'];
        $_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数

        $obj = BooController::get('Obj_Proxy_Report');
        $pageList = $obj->getPageList($proxyId, $startTime, $endTime);
        $sumInfo = $obj->getSum($proxyId, $startTime, $endTime);
        $pageList = array_merge($pageList, $sumInfo);

        if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
            return $pageList;
        } else {

            $isOpenTree = BooSession::get('proxyIsOpenTree');

            BooView::set('isOpenTree', $isOpenTree);
            BooView::set('pageList', $pageList);
        }

	}

}