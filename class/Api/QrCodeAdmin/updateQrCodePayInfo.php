<?php
class Api_QrCodeAdmin_updateQrCodePayInfo extends Api_QrCodeAdmin {

	protected function _do() {

        $post = BooVar::requestx();
        if (!$post['createTime'] || !$post['id'] || $post['realAmount'] <= 0 || !$post['payNo']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }
        $orderId = intval($post['id']);

        $qrPayObj = BooController::get('Obj_QrPay_Pay');
        $orderInfo = $qrPayObj->getInfoById($post['createTime'], $orderId);
        if (!$orderInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::ORDER_NOT_EXIST);
        }
        if ($orderInfo['result'] == 1) {
            Common_errorCode::jsonEncode(Common_errorCode::ORDER_NOT_EXIST);
        }

        $updateData = array();
        $updateData['real_amount'] = $post['realAmount'];
        $updateData['result'] = $post['result'];
        $updateData['pay_no'] = $post['payNo'];
        $updateData['end_time'] = date('Y-m-d H:i:s');
        $qrPayObj->update($post['createTime'], $orderInfo['id'], $updateData);

        return true;
	}

}
