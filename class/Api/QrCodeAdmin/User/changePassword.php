<?php
class Api_QrCodeAdmin_User_changePassword extends Api_QrCodeAdmin_User {

    protected $_tpl = "changePassword.html";

	protected function _do(){

	    if ($_POST) {

	        $post = BooVar::postx('oldPassword', 'newPassword', 'newPassword2');
	        if (!$post['oldPassword'] || !$post['newPassword'] || !$post['newPassword2']) {
                Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
            }

            if ($post['oldPassword'] == $post['newPassword']) {
                Common_errorCode::jsonEncode(Common_errorCode::OLD_NEW_PASSWORD_SAME);
            }

            if ($post['newPassword'] != $post['newPassword2']) {
                Common_errorCode::jsonEncode(Common_errorCode::PASSWORD_DIFFERENT);
            }

            //检查密码合法性
            if( false == Common_rule::checkUserPassword($post['newPassword'])) {
                Common_errorCode::jsonEncode(Common_errorCode::PASSWORD_NO_MATCH_RULE);
            }

            $qrCodeUserAutoId = BooSession::get("qrCodeUserId");
            $adminUserObj = BooController::get('Obj_QrPay_CodeUser');
            $adminUserInfo = $adminUserObj->getInfoById($qrCodeUserAutoId);
            if (!$adminUserInfo) {
                Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
            }

            if ($adminUserInfo['password'] != Common_rule::encodeLoginPassword($post['oldPassword'], $adminUserInfo['salt'])) {
                Common_errorCode::jsonEncode(Common_errorCode::OLD_PASSWORD_IS_ERR);
            }

            $newSalt = Common_rule::getRandSaltStr();
            $newPassword = Common_rule::encodeLoginPassword($post['newPassword'], $newSalt);
            $rs = $adminUserObj->update($qrCodeUserAutoId, array('password' => $newPassword, 'salt' => $newSalt));
            if (!$rs) {
                Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
            }

            return true;
        } else {
            BooView::set('userNickname', BooSession::get('qrCodeUserNickname'));
        }

	}
}