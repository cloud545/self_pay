<?php
class Api_QrCodeAdmin_User_logout extends Api_QrCodeAdmin_User {

	protected function _do(){

        BooSession::destroy();
        header('Location: /qrCodeAdmin/user/login');
	    return true;
	}
}