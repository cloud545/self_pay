<?php
class Api_QrCodeAdmin_index extends Api_QrCodeAdmin {

    protected $_tpl = "index.html";

	protected function _do() {

        if ($_POST && in_array($_POST['flag'], array('getAccountList'))) {

            switch ($_POST['flag']) {
                case 'getAccountList':
                    return $this->getAccountList();
                default:
                    Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
                    return false;
            }

        } else {

            $qrCodeUserAutoId = BooSession::get("qrCodeUserId");
            $obj = BooController::get('Obj_QrPay_CodeUser');
            $info = $obj->getInfoById($qrCodeUserAutoId);

            $post = BooVar::requestx();
            $payTypeId = $post['payTypeId'];
            $qrCodeId = $post['qrCodeId'];

            // 记录成功率
            $cacheDao = Dao_Redis_Default::getInstance();
            $payLogListCacheKey = BooConfig::get('cacheKey.qrPayUserPayLogList') . $qrCodeUserAutoId;
            $payLogListCacheInfo = $cacheDao->get($payLogListCacheKey);
            if ($payLogListCacheInfo) {
                $payLogListCacheInfo = json_decode($payLogListCacheInfo, true);
                krsort($payLogListCacheInfo);
            } else {
                $payLogListCacheInfo = array();
            }

            $payLogList = array(
                'list' => array(),
                'successRate' => 0,
                'allNum' => 0,
                'successNum' => 0,
                'successRate25' => 0,
                'allNum25' => 0,
                'successNum25' => 0,
                'balance' => $info['balance'],
                'point' => $info['point'],
            );
            $listNum = $allNum = $successNum = $allNum25 = $successNum25 = 0;
            foreach ($payLogListCacheInfo as $tmpInfo) {

                if ($payTypeId && $tmpInfo['payType'] != $payTypeId) {
                    continue;
                }

                if ($qrCodeId && $tmpInfo['qrCodeId'] != $qrCodeId) {
                    continue;
                }

                if ($tmpInfo['payType'] == 2) {
                    $tmpInfo['payTypeName'] = '支付宝';
                } elseif ($tmpInfo['payType'] == 3) {
                    $tmpInfo['payTypeName'] = '微信';
                }

                $allNum++;
                if ($tmpInfo['result']) {
                    $successNum++;
                }

                if ($listNum >= 25) {
                    continue;
                }

                $allNum25++;
                if ($tmpInfo['result']) {
                    $successNum25++;
                }

                $tmpInfo['create_time'] = date('Y-m-d H:i:s', $tmpInfo['create_time']);
                $tmpInfo['end_time'] = date('Y-m-d H:i:s', $tmpInfo['end_time']);

                $payLogList['list'][] = $tmpInfo;
                $listNum++;
            }

            if ($successNum > 0) {
                $payLogList['successRate'] = number_format(($successNum / $allNum) * 100, 2, '.', '');
                $payLogList['successNum'] = $successNum;
                $payLogList['successRate25'] = number_format(($successNum25 / $allNum25) * 100, 2, '.', '');
                $payLogList['successNum25'] = $successNum25;
            }
            $payLogList['allNum'] = $allNum;
            $payLogList['allNum25'] = $allNum25;

            if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
                return $payLogList;
            } else {

                $payTypeObj = BooController::get('Obj_Plat_PayType');
                $tmpPayTypeList = $payTypeObj->getList();

                $payTypeList = array();
                foreach ($tmpPayTypeList as $info) {
                    $payTypeList[$info['pt_id']] = $info['pt_name'];
                }

                $qrCodeUserId = BooController::get('Common')->numberAndStrSwap($qrCodeUserAutoId);

                if (BooUtil::isHttps()) {
                    $monitorLoginUrl = 'https://' . $_SERVER['HTTP_HOST'] . "/qrCode/monitor/{$qrCodeUserId}";
                } else {
                    $monitorLoginUrl = 'http://' . $_SERVER['HTTP_HOST'] . "/qrCode/monitor/{$qrCodeUserId}";
                }

                BooView::set('monitorLoginUrl', $monitorLoginUrl);
                BooView::set('payTypeList', $payTypeList);
                BooView::set('payLogList', $payLogList);
            }
        }

	}

    private function getAccountList() {

        $qrCodeUserId = BooSession::get("qrCodeUserId");

        $post = BooVar::postx();
        $payTypeId = $post['payTypeId'];

        $codeObj = BooController::get('Obj_QrPay_CodeInfo');
        $codeList = $codeObj->getList($payTypeId, $qrCodeUserId);

        $list = array();
        foreach ($codeList as $info) {
            $list[$info['id']] = $info['pay_account'];
        }

        return $list;
    }

}