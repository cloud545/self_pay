<?php
class Api_QrCodeAdmin_qrCodeList extends Api_QrCodeAdmin {

    protected $_tpl = "qrCodeList.html";

	protected function _do(){

        if ($_POST && in_array($_POST['flag'], array('add', 'update', 'del'))) {

            switch ($_POST['flag']) {
                case 'add':
                    return $this->add();
                case 'update':
                    return $this->update();
                case 'del':
                    return $this->del();
                default:
                    Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
                    return false;
            }
        } else {

            $qrCodeUserAutoId = BooSession::get("qrCodeUserId");

            $post = BooVar::postx();
            $payType = $post['payType'] ? $post['payType'] : 'all';
            $account = $post['account'] ? $post['account'] : 'all';
            $isOpen = is_numeric($post['isOpen']) ? $post['isOpen'] : 'all';
            $_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数

            $obj = BooController::get('Obj_QrPay_CodeInfo');
            $pageList = $obj->getPageList($payType, $qrCodeUserAutoId, $isOpen, $account);
            foreach ($pageList['data'] as $key => $info) {
                if ($info['pt_id'] == 2) {
                    $info['payTypeName'] = '支付宝';
                } elseif ($info['pt_id'] == 3) {
                    $info['payTypeName'] = '微信';
                }

                $pageList['data'][$key] = $info;
            }

            if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
                return $pageList;
            } else {

                BooView::set('pageList', $pageList);
            }
        }

	}

    private function add() {

        $post = BooVar::postx();
        if (!in_array($post['payType'], array(2, 3)) || !$post['account']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }

        $qrCodeUserAutoId = BooSession::get("qrCodeUserId");
        $obj = BooController::get('Obj_QrPay_CodeInfo');
        $info = $obj->getInfo($post['payType'], $qrCodeUserAutoId, $post['account']);
        if ($info) {
            Common_errorCode::jsonEncode(Common_errorCode::QRCODE_IS_EXIST);
        }

        $inserData = array(
            'pt_id' => $post['payType'],
            'pay_account' => $post['account'],
            'min_money' => $post['minMoney'],
            'max_money' => $post['maxMoney'],
            'day_amount' => $post['dayAmount'],
            'code' => $post['code'],
            'img_url' => $post['imgUrl'],
            'remark' => $post['remark'],
            'qrcode_user_id' => $qrCodeUserAutoId,
            'qrcode_user_name' => BooSession::get("qrCodeUserName"),
            'create_time' => date('Y-m-d H:i:s'),
            'last_time' => date('Y-m-d H:i:s'),
        );

        $rs = $obj->insert($inserData);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::INSERT_ERR);
        }

        return true;
    }

    private function update() {

        $post = BooVar::postx();
        $id = intval($post['id']);

        $qrCodeUserAutoId = BooSession::get("qrCodeUserId");
        $obj = BooController::get('Obj_QrPay_CodeInfo');
        $info = $obj->getInfoById($id);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::NO_DATA);
        }

        if ($info['qrcode_user_id'] != $qrCodeUserAutoId) {
            Common_errorCode::jsonEncode(Common_errorCode::NO_POWER);
        }

        $updateData = array();

        // 单纯操作是否锁定只更新这个值就行
        if ($post['setOpen']) {
            $updateData['is_open'] = intval($post['isOpen']);
        } else {
            $updateData['min_money'] = $post['minMoney'];
            $updateData['max_money'] = $post['maxMoney'];
            $updateData['day_amount'] = $post['dayAmount'];
            $updateData['code'] = $post['code'];
            $updateData['img_url'] = $post['imgUrl'];
            $updateData['remark'] = $post['remark'];
        }

        $updateData['last_time'] = date('Y-m-d H:i:s');
        $rs = $obj->update($id, $updateData);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }

        return true;
    }

    private function del() {
        $id = intval(BooVar::post('id'));
        if (!$id) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }

        $qrCodeUserAutoId = BooSession::get("qrCodeUserId");
        $obj = BooController::get('Obj_QrPay_CodeInfo');
        $info = $obj->getInfoById($id);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::NO_DATA);
        }

        if ($info['qrcode_user_id'] != $qrCodeUserAutoId) {
            Common_errorCode::jsonEncode(Common_errorCode::NO_POWER);
        }

        $rs = $obj->delete($id);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::DELETE_ERR);
        }

        return true;
    }

}