<?php
class Api_QrCode extends Api{

    public function run() {
        header("Access-Control-Allow-Origin: *");
        header("Content-type: text/html;charset=utf-8");

        // 设置模板配置
        $this->setSmartyTpl();

        // 每个请求逻辑处理，一般在子类中处理
        $data = $this->_do();
        if ($data === true || $data === false) {
            $data = array();
        }

        Common_errorCode::jsonEncode(Common_errorCode::SUCCESS, $data);
    }

    //子类要重载这个方法
    protected function _do() {

    }

    private function setSmartyTpl() {

        BooView::setOptions(array(
            "PATH_TPL" => PATH_TPL,
            "PATH_COMPILE" => PATH_DATA . "/smarty/compile",
            "DELIMITER" => array("<%", "%>"),
        ));

        BooView::set("webCdnUrl", BooConfig::get("main.webCdnUrl"));
    }

}