<?php
class Api_QrCode_payNotice extends Api_QrCode {

	protected function _do() {

        BooFile::write('/tmp/payNotice.log', json_encode($_REQUEST) . "\n\n", 'a');

        $post = BooVar::requestx();
        if (!$post['type'] || !$post['no'] || $post['money'] <= 0 || !$post['dt'] || !$post['sign'] || !$post['qrCodeUserId'] || !$post['payType'] || !$post['qrCodeId']) {
            Common_errorCode::jsonEncode(Common_errorCode::REQUEST_ERR);
        }

        $amount = $post['money'];
        $payType = $post['payType'];
        $qrCodeUserId = BooController::get('Common')->numberAndStrSwap($post['qrCodeUserId'], 'decode');
        $qrCodeId = BooController::get('Common')->numberAndStrSwap($post['qrCodeId'], 'decode');

        $qrCodeUserObj = BooController::get('Obj_QrPay_CodeUser');
        $qrCodeUserInfo = $qrCodeUserObj->getInfoById($qrCodeUserId);
        if (!$qrCodeUserInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }

        $qrCodeObj = BooController::get('Obj_QrPay_CodeInfo');
        $qrCodeInfo = $qrCodeObj->getInfoById($qrCodeId);
        if (!$qrCodeInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::ACCOUNT_NOT_EXIST);
        }

        $md5Key = $qrCodeUserInfo['sign_key'];
        $md5Str = "{$post['dt']}{$post['mark']}{$post['money']}{$post['no']}{$post['type']}{$md5Key}";
        $checkSign = md5($md5Str);
        if ($checkSign != $post['sign']) {
            $this->insertData($qrCodeInfo, $post, $payType, 2);
            Common_errorCode::jsonEncode(Common_errorCode::SIGN_ERR);
        }


        // 记录成功率
        $cacheDao = Dao_Redis_Default::getInstance();
        $payLogListCacheKey = BooConfig::get('cacheKey.qrPayUserPayLogList') . $qrCodeUserId;
        $payLogListCacheInfo = $cacheDao->get($payLogListCacheKey);
        if ($payLogListCacheInfo) {
            $payLogListCacheInfo = json_decode($payLogListCacheInfo, true);

            $isChange = false;
            foreach ($payLogListCacheInfo as $key => $tmpInfo) {
                if ($tmpInfo['qrCodeId'] == $qrCodeId && $tmpInfo['payType'] == $payType) {
                    $payLogListCacheInfo[$key]['real_amount'] = $post['money'];
                    $payLogListCacheInfo[$key]['result'] = 1;
                    $payLogListCacheInfo[$key]['end_time'] = time();
                    $isChange = true;
                    break;
                }
            }

            if ($isChange) {
                $cacheDao->set($payLogListCacheKey, json_encode($payLogListCacheInfo), 3600);
            }
        }

        $cacheKey = BooConfig::get('cacheKey.selfQrpayUsingCodeList');
        $cacheDao = Dao_Redis_Default::getInstance();
        $cacheList = $cacheDao->get($cacheKey);
        if ($cacheList) {
            $cacheList = json_decode($cacheList, true);
        } else {
            $cacheList = array();
        }

        $nowDateTime = date('Y-m-d H:i:s');
        $qrPayObj = BooController::get('Obj_QrPay_Pay');

        if (!$cacheList["{$qrCodeId}_{$amount}"]) {
            $this->insertData($qrCodeInfo, $post, $payType, 3);
        } else {
            $date = date('Y-m-d H:i:s', $cacheList["{$qrCodeId}_{$amount}"]['time']);
            $orderId = $cacheList["{$qrCodeId}_{$amount}"]['orderId'];
            $orderInfo = $qrPayObj->getInfoById($date, $orderId);

            unset($cacheList["{$qrCodeId}_{$amount}"]);
            $cacheDao->set($cacheKey, json_encode($cacheList), 3 * 3600);

            if ($orderInfo['result'] == 1) {
                echo 'success';
                exit;
            } else {

                $updateData = array();
                $updateData['account'] = $post['account'];
                $updateData['real_amount'] = $post['money'];
                $updateData['pay_no'] = $post['no'];
                $updateData['result'] = 1;
                $updateData['content'] = urldecode(json_encode($post));
                $updateData['content_ip'] = BooUtil::realIp();
                $updateData['end_time'] = $nowDateTime;

                $appId = BooController::get('Common')->numberAndStrSwap($orderInfo['app_id']);
                $pOrderId = BooController::get('Common')->numberAndStrSwap($orderInfo['plat_order_id']);
                $platId = BooController::get('Common')->numberAndStrSwap($orderInfo['p_id']);
                $qrCodeUserIdStr = BooController::get('Common')->numberAndStrSwap($qrCodeUserId);
                $qrCodeIdStr = BooController::get('Common')->numberAndStrSwap($qrCodeId);

                if (BooUtil::isHttps()) {
                    $noticeUrl = 'https://' . $_SERVER['HTTP_HOST'] . '/payCallback/' . "{$appId}1{$pOrderId}1{$platId}1{$qrCodeUserIdStr}1{$qrCodeIdStr}";
                } else {
                    $noticeUrl = 'http://' . $_SERVER['HTTP_HOST'] . '/payCallback/' . "{$appId}1{$pOrderId}1{$platId}1{$qrCodeUserIdStr}1{$qrCodeIdStr}";
                }

                $noticeData = array();
                $noticeData['amount'] = $orderInfo['amount'];
                $noticeData['realMoney'] = $post['money'];
                $noticeData['platOrderId'] = $orderInfo['id'];
                $noticeData['createTime'] = $orderInfo['create_time'];
                $noticeData['endTime'] = $nowDateTime;

                ksort($noticeData);
                $md5Str = '';
                foreach ($noticeData as $key => $value) {

                    if (!$md5Str) {
                        $md5Str .= "{$key}={$value}";
                    } else {
                        $md5Str .= "&{$key}={$value}";
                    }
                }

                $sign = md5($md5Str . 'rtsj64i543nb$jh*gjkg8368#');
                $noticeData['sign'] = $sign;

                BooCurl::setData($noticeData, 'POST');
                $data = BooCurl::call($noticeUrl);
                if ($data == 'success') {
                    $updateData['notify_status'] = 1;
                } else {
                    $updateData['notify_status'] = 2;
                }

                $updateData['notify_time'] = $nowDateTime;
                $rs = $qrPayObj->update($date, $orderInfo['id'], $updateData);
                if (!$rs) {
                    $this->insertData($qrCodeInfo, $post, $payType, 4);
                }
            }
        }

        echo 'success';
        exit;
	}

	private function insertData($qrCodeInfo, $post, $payType, $status = 0) {

        $nowDateTime = date('Y-m-d H:i:s');
        $qrPayObj = BooController::get('Obj_QrPay_Pay');

	    if ($post['no']) {
            $orderInfo = $qrPayObj->getInfoByDateAndPayTypeAndPayNo(date('Y-m-d', strtotime($nowDateTime)), $payType, $post['no']);
            if ($orderInfo) {
                return true;
            }
        }

        $insertData = array();
        $insertData['pt_id'] = $payType;
        $insertData['qrcode_id'] = $qrCodeInfo['id'];
        $insertData['qrcode_user_id'] = $qrCodeInfo['qrcode_user_id'];
        $insertData['qrcode_user_name'] = $qrCodeInfo['qrcode_user_name'];
        $insertData['pay_account'] = $qrCodeInfo['pay_account'];
        $insertData['account'] = $post['account'];
        $insertData['amount'] = $post['money'];
        $insertData['real_amount'] = $post['money'];
        $insertData['app_id'] = 0;
        $insertData['app_order_id'] = '';
        $insertData['pay_no'] = $post['no'];
        $insertData['result'] = 1;
        $insertData['create_time'] = $nowDateTime;
        $insertData['client_ip'] = BooUtil::realIp();
        $insertData['content'] = urldecode(json_encode($post));
        $insertData['content_ip'] = BooUtil::realIp();
        $insertData['client_db_id'] = $post['id'];
        $insertData['end_time'] = $nowDateTime;
        $insertData['status'] = $status;

        $qrPayObj->insert($insertData, $nowDateTime);

        return true;
    }

}
