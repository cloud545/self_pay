<?php
class Api_QrCode_payQrCode extends Api_QrCode {

    protected function _do() {

        //if(strpos($_SERVER['HTTP_USER_AGENT'],'UCBrowser') !== false|| strpos($_SERVER['HTTP_USER_AGENT'],'UCWEB') !== false) {
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'AlipayClient') != true) {
            Common_errorCode::jsonEncode(Common_errorCode::ORDER_NOT_EXIST);
        }

        BooView::setOptions(array(
            "PATH_TPL" => PATH_TPL,
            "PATH_COMPILE" => PATH_DATA . "/smarty/compile",
            "DELIMITER" => array("<%", "%>"),
        ));

        BooView::set("webCdnUrl", BooConfig::get("main.webCdnUrl"));

        $post = BooVar::requestx();
        if (!$post['appPayInfo']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST, array(), ' 1');
        }

        $appPayInfo = explode('1', $post['appPayInfo']);
        $appId = BooController::get('Common')->numberAndStrSwap($appPayInfo[0], 'decode');
        $payType = BooController::get('Common')->numberAndStrSwap($appPayInfo[1], 'decode');
        $alipayUserId = BooController::get('Common')->numberAndStrSwap($appPayInfo[2], 'decode');
        $orderId = BooController::get('Common')->numberAndStrSwap($appPayInfo[3], 'decode');

        $cacheKey = BooConfig::get('cacheKey.selfQrpayUsingCodeList');
        $cacheDao = Dao_Redis_Default::getInstance();
        $cacheList = $cacheDao->get($cacheKey);
        if ($cacheList) {
            $cacheList = json_decode($cacheList, true);
        } else {
            $cacheList = array();
        }

        if (!$cacheList) {
            Common_errorCode::jsonEncode(Common_errorCode::QRCODE_IS_INVALID, array(), ' 3');
        }

        $qrcodeCacheInfo = array();
        foreach ($cacheList as $key => $cacheInfo) {
            if ($cacheInfo['payType'] == $payType && $cacheInfo['appId'] == $appId && $cacheInfo['orderId'] == $orderId) {
                $qrcodeCacheInfo = $cacheInfo;
                break;
            }
        }

        if (!$qrcodeCacheInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::QRCODE_IS_INVALID, array(), ' 4');
        }

        $date = date('Y-m-d H:i:s', $qrcodeCacheInfo['time']);
        $orderId = $qrcodeCacheInfo['orderId'];
        $qrPayObj = BooController::get('Obj_QrPay_Pay');
        $orderInfo = $qrPayObj->getInfoById($date, $orderId);

        if ($orderInfo && $orderInfo['page'] < 2) {
            $updateData = array();
            $updateData['page'] = 2;
            $qrPayObj->update($date, $orderInfo['id'], $updateData);
        }

        BooView::set('dateTime', date('Y-m-d H:i:s', $qrcodeCacheInfo['time']));
        BooView::set('qrOrderId', $qrcodeCacheInfo['orderId']);
        BooView::set('payType', $qrcodeCacheInfo['payType']);
        BooView::set('phoneId', $qrcodeCacheInfo['phoneId']);
        BooView::set('orderId', $qrcodeCacheInfo['appOrderId']);
        BooView::set('amount', $qrcodeCacheInfo['amount']);
        BooView::set('alipayUserId', $alipayUserId);
        BooView::set('alipayUserName', $qrcodeCacheInfo['remark']);
        BooView::display('pay/alipay.html');
        exit;
    }

}
