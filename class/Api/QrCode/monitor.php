<?php
class Api_QrCode_monitor extends Api_QrCode {

	protected function _do() {

        $post = BooVar::requestx();
        if (!$post['flag']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }

        $_SERVER['REQUEST_URI'] = "/qrCode/monitor";

        switch ($post['flag']) {
            case 'login':
                return $this->login();
            case 'checkLogin':
                return $this->checkLogin();
            case 'getQrCodeId':
                return $this->getQrCodeId();
            case 'heartbeat':
                return $this->heartbeat();
            default:
                Common_errorCode::jsonEncode(Common_errorCode::FAIL);
                break;
        }

        return true;
	}

	private function login() {

        $post = BooVar::requestx();
        if (!$post['password'] || !$post['userName'] || !$post['qrCodeInfo']) {
            Common_errorCode::jsonEncode(Common_errorCode::REQUEST_ERR);
        }

        $obj = BooController::get('Obj_QrPay_CodeUser');
        $adminInfo = $obj->getInfoByName($post['userName']);
        if (!$adminInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }

        if ($adminInfo['password'] != Common_rule::encodeLoginPassword($post['password'], $adminInfo['salt'])){
            Common_errorCode::jsonEncode(Common_errorCode::PASSWORD_ERR);
        }

        if ($adminInfo['is_lock']) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_LOCKED);
        }

        $qrCodeInfo = explode('1', $post['qrCodeInfo']);
        $qrCodeUserId = BooController::get('Common')->numberAndStrSwap($qrCodeInfo[0], 'decode');
        if ($qrCodeUserId != $adminInfo['id']) {
            Common_errorCode::jsonEncode(Common_errorCode::REQUEST_ERR);
        }

        $qrCodeUserIdStr = BooController::get('Common')->numberAndStrSwap($qrCodeUserId);
        if (BooUtil::isHttps()) {
            $notifyUrl = 'https://' . $_SERVER['HTTP_HOST'] . "/qrCode/payNotice";
        } else {
            $notifyUrl = 'http://' . $_SERVER['HTTP_HOST'] . "/qrCode/payNotice";
        }

        BooSession::set('qrCodeUserId', $adminInfo['id']);
        BooSession::set('qrCodeUserName', $adminInfo['name']);
        BooSession::set('qrCodeUserNickname', $adminInfo['name']);
        BooSession::set('qrCodeUserIsLock', $adminInfo['is_lock']);

        // 记录管理员行为
        BooController::get('Mod_Admin')->addQrCodeAdminLogs($adminInfo['id']);

        $sessionId = BooSession::getSessionId();
        $returnData = array(
            'sessionId' => $sessionId,
            'qrCodeUserId' => $qrCodeUserIdStr,
            'notifyUrl' => $notifyUrl,
            'signKey' => $adminInfo['sign_key'],
        );

        return $returnData;
    }

    private function checkLogin() {

        if (!BooSession::get('qrCodeUserId')) {
            Common_errorCode::jsonEncode(Common_errorCode::LONG_NO_OPERATE);
        }

        if (BooSession::get('qrCodeUserIsLock')) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_LOCKED);
        }

        // 记录管理员行为
        BooController::get('Mod_Admin')->addQrCodeAdminLogs();

        return true;
    }

    private function getQrCodeId() {

        $post = BooVar::requestx();
        if ((!$post['alipayAccount'] && !$post['wechatAccount']) || !$post['qrCodeInfo']) {
            Common_errorCode::jsonEncode(Common_errorCode::REQUEST_ERR);
        }

        $qrCodeUserId = BooSession::get("qrCodeUserId");
        $obj = BooController::get('Obj_QrPay_CodeUser');
        $qrCodeUserInfo = $obj->getInfoById($qrCodeUserId);
        if (!$qrCodeUserInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }


        if ($qrCodeUserInfo['is_lock']) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_LOCKED);
        }

        $qrCodeInfo = explode('1', $post['qrCodeInfo']);
        $qrCodeUserAutoId = BooController::get('Common')->numberAndStrSwap($qrCodeInfo[0], 'decode');
        if ($qrCodeUserId != $qrCodeUserInfo['id'] || $qrCodeUserAutoId != $qrCodeUserId) {
            Common_errorCode::jsonEncode(Common_errorCode::REQUEST_ERR);
        }

        $returnData = array(
            'alipayAccount' => '',
            'alipayQrCodeId' => '',
            'wechatAccount' => '',
            'wechatQrCodeId' => '',
        );

        $codeObj = BooController::get('Obj_QrPay_CodeInfo');

        if ($post['alipayAccount']) {
            $info = $codeObj->getInfo(2, $qrCodeUserId, $post['alipayAccount']);
            if (!$info) {
                Common_errorCode::jsonEncode(Common_errorCode::ACCOUNT_NOT_EXIST, array(), " 该支付宝账号未在后台设置");
            }

            $returnData['alipayAccount'] = $post['alipayAccount'];
            $returnData['alipayQrCodeId'] = BooController::get('Common')->numberAndStrSwap($info['id']);
        }

        if ($post['wechatAccount']) {
            $info = $codeObj->getInfo(3, $qrCodeUserId, $post['wechatAccount']);
            if (!$info) {
                Common_errorCode::jsonEncode(Common_errorCode::ACCOUNT_NOT_EXIST, array(), " 该微信账号未在后台设置");
            }

            $returnData['wechatAccount'] = $post['wechatAccount'];
            $returnData['wechatQrCodeId'] = BooController::get('Common')->numberAndStrSwap($info['id']);
        }

        // 记录管理员行为
        BooController::get('Mod_Admin')->addQrCodeAdminLogs();
        return $returnData;
    }

    private function heartbeat() {

        return true;
    }

}
