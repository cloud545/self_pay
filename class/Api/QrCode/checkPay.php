<?php
class Api_QrCode_checkPay extends Api_QrCode {

	protected function _do() {

        $post = BooVar::postx();
        if (!$post['dateTime'] || !$post['qrCodeId'] || $post['money'] <= 0) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }

        if (time() - strtotime($post['dateTime']) > 5 * 60) {
            Common_errorCode::jsonEncode(Common_errorCode::PAY_TIME_OUT);
        }

        $qrCodeId = $post['qrCodeId'];
        $money = $post['money'];

        $cacheKey = BooConfig::get('cacheKey.selfQrpayUsingCodeList');
        $cacheDao = Dao_Redis_Default::getInstance();
        $cacheList = $cacheDao->get($cacheKey);
        if ($cacheList) {
            $cacheList = json_decode($cacheList, true);
        } else {
            $cacheList = array();
        }

        if (!$cacheList["{$qrCodeId}_{$money}"]) {
            Common_errorCode::jsonEncode(Common_errorCode::PAY_TIME_OUT, array(), ' 1');
        }

        $qrPayObj = BooController::get('Obj_QrPay_Pay');
        $date = date('Y-m-d H:i:s', $cacheList["{$qrCodeId}_{$money}"]['time']);
        $orderId = $cacheList["{$qrCodeId}_{$money}"]['orderId'];
        $orderInfo = $qrPayObj->getInfoById($date, $orderId);
        if (!$orderInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::PAY_TIME_OUT, array(), ' 2');
        }

        if ($orderInfo['result'] == 1) {
            return array('result' => 1);
        }

        return array('result' => 0);
	}

}
