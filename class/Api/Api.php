<?php
class Api extends BooObj {

    public function __construct() {

        if ($_REQUEST['target'] == 'favicon') {
            exit(200);
        }

        $sessionCacheDao = Dao_Redis_Session::getInstance();
        BooSession::start('redis', $sessionCacheDao);
    }

    public function run() {
        $aData = $this->_do();
        if ($aData === true) {
            $aData = array();
        }
        Common_errorCode::jsonEncode(Common_errorCode::SUCCESS, $aData);
    }

    // 子类要重载这个方法
    protected function _do() {
        return array();
    }

}