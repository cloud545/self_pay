<?php
class Api_Admin_withdrawalList extends Api_Admin {

    protected $_tpl = "withdrawalList.html";

	protected function _do() {

        if (in_array($_POST['flag'], array('getInfo', 'update'))) {
            switch ($_POST['flag']) {
                case 'getInfo':
                    return $this->getInfo();
                case 'update':
                    return $this->update();
                default:
                    return false;
            }
        } else {

            $post = BooVar::postx();
            $status = $post['status'] ? $post['status'] : 'all';
            $startTime = $post['startTime'];
            $endTime = $post['endTime'];

            $get = BooVar::getx();
            if ($get['tradeNo']) {
                $post['tradeNo'] = $get['tradeNo'];
                BooView::set('tradeNo', $get['tradeNo']);
            }

            $id = $post['tradeNo'] ? substr($post['tradeNo'], 9) : 0;
            $_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数

            $obj = BooController::get('Obj_Admin_Withdrawal');
            $pageList = $obj->getPageList($status, $startTime, $endTime, $id);
            foreach ($pageList['data'] as $key => $info) {
                $info['tradeNo'] = 'W' . date('Ymd', strtotime($info['create_time'])) . $info['id'];

                if ($info['status'] == 1) {
                    $info['statusName'] = '银行处理中';
                } else if ($info['status'] == 2) {
                    $info['statusName'] = '提款成功';
                } else if ($info['status'] == 3) {
                    $info['statusName'] = '提款失败';
                } else {
                    $info['statusName'] = '系统处理中';
                }

                $pageList['data'][$key] = $info;
            }

            if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
                return $pageList;
            } else {
                BooView::set('pageList', $pageList);
            }
        }

	}

    private function getInfo() {

        $post = BooVar::postx();

        if (!$post['id']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        $id = intval($post['id']);

        // 00 点暂不结算，等日汇总结算后再进行结算，否则会导致商户余额在5分钟内全部显示为0
        $startTime = date('Y-m-d') . " 00:00:00";
        if (time() < (strtotime($startTime) + 600)) {
            Common_errorCode::jsonEncode(Common_errorCode::SYSTEM_DAY_SETTLE_ACCOUNTS);
        }

        $obj = BooController::get('Obj_Admin_Withdrawal');
        $info = $obj->getInfoById($id);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::NO_DATA);
        }

        $adminObj = BooController::get('Obj_Admin_Info');
        $balanceInfo = $adminObj->getInfo(BooSession::get('adminId'));
        if (!$balanceInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::NO_DATA);
        }

        $returnData = array();
        $returnData['id'] = $id;
        $returnData['cash_balance'] = $balanceInfo['cash_balance'];
        $returnData['amount'] = $info['amount'];

        return $returnData;
    }

    private function update() {
        $post = BooVar::postx();

        if (!$post['id'] || !$post['status']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        $id = intval($post['id']);

        // 00 点暂不结算，等日汇总结算后再进行结算，否则会导致商户余额在5分钟内全部显示为0
        $startTime = date('Y-m-d') . " 00:00:00";
        if (time() < (strtotime($startTime) + 600)) {
            Common_errorCode::jsonEncode(Common_errorCode::SYSTEM_DAY_SETTLE_ACCOUNTS);
        }

        $obj = BooController::get('Obj_Admin_Withdrawal');
        $info = $obj->getInfoById($id);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::NO_DATA);
        }

        $updateData = array();
        $updateData['status'] = $post['status'];
        $updateData['remark'] = $post['remark'];
        $updateData['operator_id'] = BooSession::get('adminId');
        $updateData['operate_time'] = date('Y-m-d H:i:s');
        $rs = $obj->update($id, $updateData);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }

        if ($post['status'] == 2) {

            $balanceObj = BooController::get('Obj_Admin_Info');
            $balanceInfo = $balanceObj->getInfo(BooSession::get('adminId'));
            if (!$balanceInfo) {
                Common_errorCode::jsonEncode(Common_errorCode::NO_DATA);
            }

            $updateBalanceData = array();
            $updateBalanceData['frozen_amount'] = $balanceInfo['frozen_amount'] - $info['amount'] - $info['fee'];
            $updateBalanceData['cash_balance'] = $balanceInfo['cash_balance'] - $info['amount'] - $info['fee'];
            $rs = $balanceObj->update($updateBalanceData, BooSession::get('adminId'));
            if (!$rs) {
                Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
            }

            $orderObj = BooController::get('Obj_Admin_Orders');

            // 计算记录写入帐变表
            $insertData = array();
            $insertData['amount'] = $info['amount'];
            $insertData['type'] = 2;
            $insertData['trade_no'] = $info['id'];
            $insertData['create_time'] = date('Y-m-d H:i:s');
            $insertData['create_date'] = date('Ymd');
            $insertData['current_balance'] = $balanceInfo['cash_balance'] - $info['amount'];
            $orderObj->insert($insertData);

            // 计算记录写入帐变表
            $insertData = array();
            $insertData['amount'] = $info['fee'];
            $insertData['type'] = 3;
            $insertData['trade_no'] = $info['id'];
            $insertData['create_time'] = date('Y-m-d H:i:s');
            $insertData['create_date'] = date('Ymd');
            $insertData['current_balance'] = $balanceInfo['cash_balance'] - $info['amount'] - $info['fee'];
            $orderObj->insert($insertData);

            // 如果审核的提款订单的时间跟提交订单的时间不在同一天，则需要重新结算提交订单那天的数据
            if (date('Y-m-d', strtotime($info['create_time'])) != date('Y-m-d')) {

                $createDateTime = strtotime(date('Y-m-d', strtotime($info['create_time'])));
                $nowDateTime = strtotime(date('Y-m-d'));
                for ($i = $createDateTime; $i < $nowDateTime; $i += 86400) {
                    BooController::get('Mod_Report')->systemCalculate(date('Y-m-d', $i));
                }

            }

        } elseif ($post['status'] == 3) {

            $balanceObj = BooController::get('Obj_Admin_Info');
            $balanceInfo = $balanceObj->getInfo(BooSession::get('adminId'));
            if (!$balanceInfo) {
                Common_errorCode::jsonEncode(Common_errorCode::NO_DATA);
            }

            $updateBalanceData = array();
            $updateBalanceData['frozen_amount'] = $balanceInfo['frozen_amount'] - $info['amount'] - $info['fee'];
            $updateBalanceData['available_balance'] = $balanceInfo['available_balance'] + $info['amount'] + $info['fee'];
            $rs = $balanceObj->update($updateBalanceData, BooSession::get('adminId'));
            if (!$rs) {
                Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
            }

        }

        return true;
    }

}
