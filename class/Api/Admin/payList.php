<?php
class Api_Admin_payList extends Api_Admin {

    protected $_tpl = "payList.html";

	protected function _do() {

        $post = BooVar::requestx();
        $appId = $post['appId'] ? intval($post['appId']) : 0;
        $payTypeId = $post['payTypeId'];
        $platId = intval($post['platId']);
        $id = $post['tradeNo'] ? substr($post['tradeNo'],9) : 0;
        $startTime = $post['startTime'];
        $endTime = $post['endTime'];
        $_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数

        $obj = BooController::get('Obj_Admin_Pay');
        $pageList = $obj->getPageList($appId, $platId, $id, $payTypeId, $startTime, $endTime);
        foreach ($pageList['data'] as $key => $info) {
    
            /**
             * 获取订单信息
             */
            $tmpAppId    = BooController::get('Common')->numberAndStrSwap($info['app_id']);
            $orderId     = BooController::get('Common')->numberAndStrSwap($info['pay_id']);
            $orderPlatId = BooController::get('Common')->numberAndStrSwap($info['p_id']);
            $info['plat_order_id'] = "{$tmpAppId}1{$orderId}1{$orderPlatId}";
            
            $info['tradeNo'] = 'P' . date('Ymd', strtotime($info['create_time'])) . $info['pay_id'];
            $pageList['data'][$key] = $info;
        }

        $sumInfo = $obj->getSum($appId, $platId, $id, $payTypeId, $startTime, $endTime);
        $pageList = array_merge($pageList, $sumInfo);

        if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
            return $pageList;
        } else {

            $appObj = BooController::get('Obj_App_Info');
            $tmpAppList = $appObj->getList();

            $appList = array();
            foreach ($tmpAppList as $info) {
                if ($info['is_lock']) {
                    continue;
                }
                $appList[$info['app_id']] = $info['name'];
            }

            $payTypeObj = BooController::get('Obj_Plat_PayType');
            $tmpPayTypeList = $payTypeObj->getList();

            $payTypeList = array();
            foreach ($tmpPayTypeList as $info) {
                $payTypeList[$info['pt_id']] = $info['pt_name'];
            }

            $platObj = BooController::get('Obj_Plat_Info');
            $tmpPlatList = $platObj->getList();

            $platList = array();
            foreach ($tmpPlatList as $info) {
                $platList[$info['p_id']] = $info['p_name'];
            }

            $get = BooVar::getx();
            if ($get['tradeNo']) {
                BooView::set('tradeNo', $get['tradeNo']);
            }

            if ($get['startTime']) {
                BooView::set('startTime', $get['startTime']);
                BooView::set('endTime', $get['endTime']);
            }

            BooView::set('appList', $appList);
            BooView::set('payTypeList', $payTypeList);
            BooView::set('platList', $platList);
            BooView::set('pageList', $pageList);
        }

	}
}
