<?php
class Api_Admin_Plat_list extends Api_Admin_Plat {

    protected $_tpl = "plat/list.html";

	protected function _do() {

        // 区分ajax和表单请求数据
        if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
            switch ($_POST['flag']) {
                case 'add':
                    return $this->add();
                case 'update':
                    return $this->update();
                case 'del':
                    return $this->del();
                default:
                    Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
                    return false;
            }
        } else {
            $obj = BooController::get('Obj_Plat_Info');
            $list = $obj->getList();

            foreach ($list as $key => $info) {
                if ($info['p_bank_code']) {
                    $list[$key]['p_bank_code'] = json_decode($info['p_bank_code'], true);
                } else {
                    $list[$key]['p_bank_code'] = array();
                }
            }

            $obj = BooController::get('Obj_Plat_PayType');
            $tmpPayTypeList = $obj->getList();

            $payTypeList = array();
            foreach ($tmpPayTypeList as $info) {
                $payTypeList[$info['pt_id']] = $info;
            }

            BooView::set('list', $list);
            BooView::set('payTypeList', $payTypeList);
            BooView::set('bankList', BooConfig::get('main.bankList'));
        }
	}

    private function add() {

        $post = BooVar::postx();
        if (!$post['name'] || !$post['tag'] || !$post['payType'] || $post['appPoint'] < 0) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }

        $bankCodeList = array();
        if ($post['payType'] == 1 || $post['payType'] == 7) {
            $bankList = BooConfig::get('main.bankList');
            foreach ($bankList as $code => $name) {
                if ($post[$code]) {
                    $bankCodeList[$code] = $post[$code];
                }
            }
        }

        $obj = BooController::get('Obj_Plat_Info');
        $inserData = array(
            'pt_id' => intval($post['payType']),
            'p_tag' => $post['tag'],
            'p_name' => $post['name'],
            'p_app_id' => $post['appId'],
            'p_app_key' => is_array($post['appKey']) ? json_encode($post['appKey']) : $post['appKey'],
            'p_app_secret' => is_array($post['appSecret']) ? json_encode($post['appSecret']) : $post['appSecret'],
            'p_pay_url' => $post['payUrl'],
            'p_bank_code' => $bankCodeList ? json_encode($bankCodeList) : '',
            'p_point' => $post['appPoint'],
        );

        $rs = $obj->insert($inserData);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::INSERT_ERR);
        }

        return true;
    }

    private function update() {
        $post = BooVar::postx();

        if (!$post['id'] || !$post['name'] || !$post['tag'] || !$post['payType'] || $post['appPoint'] < 0) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        $id = intval($post['id']);

        $obj = BooController::get('Obj_Plat_Info');
        $info = $obj->getInfoById($id);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::NO_DATA);
        }

        $bankCodeList = array();
        if ($post['payType'] == 1 || $post['payType'] == 7) {
            $bankList = BooConfig::get('main.bankList');
            foreach ($bankList as $code => $name) {
                if ($post[$code]) {
                    $bankCodeList[$code] = $post[$code];
                }
            }
        }

        $updateData = array(
            'pt_id' => intval($post['payType']),
            'p_tag' => $post['tag'],
            'p_name' => $post['name'],
            'p_app_id' => $post['appId'],
            'p_app_key' => is_array($post['appKey']) ? json_encode($post['appKey']) : $post['appKey'],
            'p_app_secret' => is_array($post['appSecret']) ? json_encode($post['appSecret']) : $post['appSecret'],
            'p_pay_url' => $post['payUrl'],
            'p_bank_code' => $bankCodeList ? json_encode($bankCodeList) : '',
            'p_point' => $post['appPoint'],
        );
        $rs = $obj->update($id, $updateData);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }

        return true;
    }

    private function del() {
        $id = intval(BooVar::post('id'));
        if (!$id) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }

        $obj = BooController::get('Obj_Plat_Info');
        $info = $obj->getInfoById($id);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::NO_DATA);
        }

        $rs = $obj->delete($id);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::DELETE_ERR);
        }

        return true;
    }

}