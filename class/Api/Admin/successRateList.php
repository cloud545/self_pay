<?php
class Api_Admin_successRateList extends Api_Admin {

    protected $_tpl = "successRateList.html";

	protected function _do() {

        $post = BooVar::requestx();
        $appId = $post['appId'];
        $payTypeId = $post['payTypeId'];
        $platId = intval($post['platId']);

        // 记录成功率
        $cacheDao = Dao_Redis_Default::getInstance();
        $payLogListCacheKey = BooConfig::get('cacheKey.payLogList');
        $payLogListCacheInfo = $cacheDao->get($payLogListCacheKey);
        if ($payLogListCacheInfo) {
            $payLogListCacheInfo = json_decode($payLogListCacheInfo, true);
            krsort($payLogListCacheInfo);
        }

        $payLogList = array(
            'list' => array(),
            'successRate' => 0,
            'allNum' => 0,
            'successNum' => 0,
            'successRate25' => 0,
            'allNum25' => 0,
            'successNum25' => 0,
            'successRate100' => 0,
            'allNum100' => 0,
            'successNum100' => 0,
        );
        $listNum = $allNum = $successNum = $allNum25 = $successNum25 = 0;
         $allNum100 = $successNum100 = 0;
        foreach ($payLogListCacheInfo as $tmpInfo) {

            if ($appId && $tmpInfo['app_id'] != $appId) {
                continue;
            }

            if ($payTypeId && $tmpInfo['pt_id'] != $payTypeId) {
                continue;
            }

            if ($platId && $tmpInfo['p_id'] != $platId) {
                continue;
            }

            $allNum++;
            if ($tmpInfo['result']) {
                $successNum++;
            }

            if ($listNum >= 100) {
                continue;
            }
            $allNum100++;
            if ($tmpInfo['result']) {
                $successNum100++;
            }

            $tmpInfo['create_time'] = date('Y-m-d H:i:s', $tmpInfo['create_time']);
            $tmpInfo['end_time'] = date('Y-m-d H:i:s', $tmpInfo['end_time']);

            $payLogList['list'][] = $tmpInfo;
            $listNum++;
        }

        if ($successNum > 0) {
            $payLogList['successRate'] = number_format(($successNum / $allNum) * 100,2,'.','');
            $payLogList['successNum'] = $successNum;
            $payLogList['successRate25'] = number_format(($successNum25 / $allNum25) * 100,2,'.','');
            $payLogList['successNum25'] = $successNum25;
            $payLogList['successRate100'] = number_format(($successNum100 / $allNum100) * 100,2,'.','');
            $payLogList['successNum100'] = $successNum100;
        }
        $payLogList['allNum'] = $allNum;
        $payLogList['allNum25'] = $allNum25;
        $payLogList['allNum100'] = $allNum100;
        if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
            return $payLogList;
        } else {

            $appObj = BooController::get('Obj_App_Info');
            $tmpAppList = $appObj->getList();

            $appList = array();
            foreach ($tmpAppList as $info) {
                if ($info['is_lock']) {
                    continue;
                }
                $appList[$info['app_id']] = $info['name'];
            }

            $payTypeObj = BooController::get('Obj_Plat_PayType');
            $tmpPayTypeList = $payTypeObj->getList();

            $payTypeList = array();
            foreach ($tmpPayTypeList as $info) {
                $payTypeList[$info['pt_id']] = $info['pt_name'];
            }

            $platObj = BooController::get('Obj_Plat_Info');
            $tmpPlatList = $platObj->getList();

            $platList = array();
            foreach ($tmpPlatList as $info) {
                $platList[$info['p_id']] = $info['p_name'];
            }

            BooView::set('appList', $appList);
            BooView::set('payTypeList', $payTypeList);
            BooView::set('platList', $platList);
            BooView::set('payLogList', $payLogList);
        }

	}
}