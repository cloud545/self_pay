<?php
class Api_admin_reportMonth extends Api_admin {

    protected $_tpl = "reportMonth.html";

	protected function _do() {

        $post = BooVar::postx();
        $startTime = $post['startTime'];
        $endTime = $post['endTime'];
        $_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数
        $_GET['pageSize'] = 20;// 每页20条

        if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
            $obj = BooController::get('Obj_Admin_ReportMonth');
            $pageList = $obj->getPageList($startTime, $endTime);
            $sumInfo = $obj->getSum($startTime, $endTime);
            $pageList = array_merge($pageList, $sumInfo);
            return $pageList;
        } else {
            //BooView::set('pageList', $pageList);
        }

	}

}