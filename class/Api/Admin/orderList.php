<?php
class Api_Admin_orderList extends Api_Admin {

    protected $_tpl = "orderList.html";

	protected function _do() {

        $post = BooVar::postx();
        $type = $post['type'] ? $post['type'] : 'all';
        $startTime = $post['startTime'];
        $endTime = $post['endTime'];
        $_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数

        $obj = BooController::get('Obj_Admin_Orders');
        $pageList = $obj->getPageList($type, $startTime, $endTime);
        foreach ($pageList['data'] as $key => $info) {

            if ($info['type'] == 1) {
                $info['tradeInfo'] = "{$info['start_time']} 到 {$info['end_time']} 充值结算";
            } else {
                $info['tradeInfo'] = 'W' . date('Ymd', strtotime($info['create_time'])) . $info['trade_no'];
            }

            if ($info['type'] == 1) {
                $info['typeName'] = '充值结算';
            } elseif ($info['type'] == 2) {
                $info['typeName'] = '提款';
            } else {
                $info['typeName'] = '提款手续费';
            }

            $pageList['data'][$key] = $info;
        }

        if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
            return $pageList;
        } else {
            BooView::set('pageList', $pageList);
        }

	}
}