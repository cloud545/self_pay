<?php
class Api_Admin_Logs_info extends Api_Admin_Logs{

	protected function _do(){

	    if ($_POST) {

            $post = BooVar::postx();

            if (!$post['id']) {
                Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
            }
            $id = intval($post['id']);
            $type = intval($post['type']);

            if ($type == 2) {
                $obj = BooController::get('Obj_App_Logs');
            } elseif ($type == 3) {
                $obj = BooController::get('Obj_Proxy_Logs');
            } else {
                $obj = BooController::get('Obj_Admin_Logs');
            }

            $info = $obj->getInfoById($id);
            if (!$info) {
                Common_errorCode::jsonEncode(Common_errorCode::NO_DATA);
            }

            $content = json_decode($info['content'], true);
            $content = var_export($content, true);
            $content = $this->echoArray($content);
            $info['content'] = $content;

            return $info;
        }

	}

    private function echoArray($vars) {
        if (ini_get('html_errors')) {
            $content = "<pre>\n";
            $content .= htmlspecialchars(print_r($vars, true));
            $content .= "\n</pre>\n";
        } else {
            $content = " :\n" . print_r($vars, true);
        }

        return $content;
    }

}