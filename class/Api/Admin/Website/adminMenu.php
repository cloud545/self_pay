<?php
class Api_Admin_Website_adminMenu extends Api_Admin_Website{

    protected $_tpl = "website/adminMenu.html";

	protected function _do(){

        if ($_POST) {

            switch ($_POST['flag']) {
                case 'add':
                    return $this->add();
                case 'update':
                    return $this->update();
                case 'del':
                    return $this->del();
                default:
                    Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
                    return false;
            }

        }

	}

    private function add() {

        $post = BooVar::postx('menuName', 'menuIcon', 'targetUrl', 'isMenu', 'isLink', 'menuId');
        if (!$post['menuName'] || (!$post['isMenu'] && !$post['isLink'])) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }

        $insertArr = array();
        $insertArr['menu_parent_id'] = $post['menuId'] ? $post['menuId'] : 0;
        $insertArr['menu_title'] = $post['menuName'];
        $insertArr['menu_icon'] = $post['menuIcon'];
        $insertArr['menu_target'] = $post['targetUrl'];
        $insertArr['menu_is_menu'] = $post['isMenu'] ? 1 : 0;
        $insertArr['menu_is_link'] = $post['isLink'] ? 1 : 0;

        $obj = BooController::get('Obj_Admin_Menu');
        $rs = $obj->insert($insertArr);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::INSERT_ERR);
        }

        return true;
    }

    private function update() {
        $post = BooVar::postx('menuId', 'menuName', 'menuIcon', 'targetUrl', 'isMenu', 'isLink', 'isShowHidden', 'isChangeRank', 'rankList');

        // 修改菜单排序
        if ($post['isChangeRank']) {
            return $this->changeRank($post['rankList']);
        }

        if (!$post['menuId']) {
            Common_errorCode::jsonEncode(Common_errorCode::MENU_NOT_EXIST);
        }

        $obj = BooController::get('Obj_Admin_Menu');
        $info = $obj->getInfo($post['menuId']);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::MENU_NOT_EXIST);
        }

        $updateData = array();
        if ($post['isShowHidden']) {
            if (intval($info['menu_is_disabled'])) {
                $updateData['menu_is_disabled'] = 0;
            } else {
                $updateData['menu_is_disabled'] = 1;
            }
        } else {
            $updateData['menu_title'] = $post['menuName'];
            $updateData['menu_icon'] = $post['menuIcon'];
            $updateData['menu_target'] = $post['targetUrl'];
            $updateData['menu_is_menu'] = $post['isMenu'] ? 1 : 0;
            $updateData['menu_is_link'] = $post['isLink'] ? 1 : 0;
        }

        $rs = $obj->update($post['menuId'], $updateData);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }

        return true;
    }

    private function del() {
        $id = BooVar::post('menuId');
        if (!$id) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }

        $obj = BooController::get('Obj_Admin_Menu');
        $info = $obj->getInfo($id);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::MENU_NOT_EXIST);
        }

        $conditionStr = "`menu_parent_id` = {$id}";
        $childrenList = $obj->getList('*', $conditionStr);
        if ($childrenList) {
            Common_errorCode::jsonEncode(Common_errorCode::MENU_HAVE_CHILDREN_MENU);
        }

        $rs = $obj->delete($id);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::DELETE_ERR);
        }

        return true;
    }

    private function changeRank($rankList) {
        if (!$rankList) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }

        $parentNodes = $sortNodes = array();
        foreach ($rankList as $rankId => $info) {
            $sortNodes[$info['id']] = $rankId;
            $parentNodes[$info['id']] = 0;
            if ($info['children']) {
                foreach ($info['children'] as $rank1Id => $info1) {
                    $sortNodes[$info1['id']] = $rank1Id;
                    $parentNodes[$info1['id']] = $info['id'];
                    if ($info1['children']) {
                        foreach ($info1['children'] as $rank2Id => $info2) {
                            $sortNodes[$info2['id']] = $rank2Id;
                            $parentNodes[$info2['id']] = $info1['id'];
                            if ($info2['children']) {
                                Common_errorCode::jsonEncode(Common_errorCode::MENU_CHILDREN_IS_MAX);
                            }
                        }
                    }
                }
            }
        }

        // 取出旧的菜单列表
        $oldParentNodes = $oldSortNodes = array();
        $obj = BooController::get('Obj_Admin_Menu');
        $list = $obj->getList();
        foreach ($list as $info) {
            $oldParentNodes[$info['id']] = $info['menu_parent_id'];
            $oldSortNodes[$info['id']] = $info['menu_sort'];
        }

        // 获得需要更新的菜单
        foreach ($parentNodes as $id => $parentId) {
            if ($oldParentNodes[$id] == $parentId) {
                unset($parentNodes[$id]);
            }
        }
        foreach ($sortNodes as $id => $index) {
            if ($oldSortNodes[$id] == $index) {
                unset($sortNodes[$id]);
            }
        }

        // 判断是否有更新的数据
        if (!$parentNodes && !$sortNodes) {
            Common_errorCode::jsonEncode(Common_errorCode::NO_DATA_UPDATE);
        }

        $rs = $obj->updateParentAndSort($parentNodes, $sortNodes);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }

        return true;
    }
}
