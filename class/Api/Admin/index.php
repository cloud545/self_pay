<?php

class Api_Admin_index extends Api_Admin
{
    
    protected $_tpl = "index.html";
    
    protected function _do()
    {
        
        if ($_REQUEST
            && in_array(
                $_REQUEST['flag'],
                array(
                    'getInfo',
                    'withdrawal',
                    'tbody',
                    'api_charts',
                    'theader',
                    'getAppWithdrawal',
                    'getProxyAppWithdrawal',
                )
            )) {
            
            switch ($_REQUEST['flag']) {
                case 'tbody':
                    return $this->tbody();
                    break;
                case 'api_charts':
                    return $this->apiCharts();
                    break;
                case 'theader':
                    return $this->theader();
                    break;
                case 'getInfo':
                    return $this->getInfo();
                case 'withdrawal':
                    return $this->withdrawal();
                case 'getAppWithdrawal':
                    return $this->getAppWithdrawal();
                case 'getProxyAppWithdrawal':
                    return $this->getProxyAppWithdrawal();
                default:
                    Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
                    
                    return false;
            }
            
        }
        else {
            
            $obj  = BooController::get('Obj_Admin_Info');
            $info = $obj->getInfo(BooSession::get('adminId'));
            
            $startTime  = date('Y-m-d') . " 00:00:00";
            $endTime    = date('Y-m-d H:i:s');
            $payObj     = BooController::get('Obj_Admin_Pay');
            $payCount   = $payObj->getCount(0, 0, 0, 0, $startTime, $endTime);
            $paySumInfo = $payObj->getSum(0, 0, 0, 0, $startTime, $endTime);
            
            $withdrawalObj     = BooController::get('Obj_Admin_Withdrawal');
            $withdrawalSumInfo = $withdrawalObj->getSum(2, $startTime, $endTime);
            
            // 读取商户未提款笔数
            $appWithdrawalObj   = BooController::get('Obj_App_Withdrawal');
            $appWithdrawalTimes = $appWithdrawalObj->getCount(0, array(0, 1), $startTime, $endTime);
            
            // 读取代理未提款笔数
            $proxyWithdrawalObj   = BooController::get('Obj_Proxy_Withdrawal');
            $proxyWithdrawalTimes = $proxyWithdrawalObj->getCount(0, 0, $startTime, $endTime);
            
            BooView::set('balance', $info['available_balance']);
            BooView::set('payTimes', $payCount ? $payCount : 0);
            BooView::set('paySum', $paySumInfo['sumAmount'] ? $paySumInfo['sumAmount'] : 0);
            BooView::set('sumFee', $paySumInfo['sumFee'] ? $paySumInfo['sumFee'] : 0);
            BooView::set('withdrawalTimes', $appWithdrawalTimes + $proxyWithdrawalTimes);
            BooView::set('appWithdrawalTimes', $appWithdrawalTimes);
            BooView::set('proxyWithdrawalTimes', $proxyWithdrawalTimes);
            BooView::set('withdrawalSum', $withdrawalSumInfo['sumAmount'] ? $withdrawalSumInfo['sumAmount'] : 0);
            
            /**
             * 新增转化率展示
             */
            
            //商户列表
            $appObj     = BooController::get('Obj_App_Info');
            $tmpAppList = $appObj->getList();
            $appList    = array();
            foreach ($tmpAppList as $info) {
                if ($info['is_lock']) {
                    continue;
                }
                $appList[$info['app_id']] = $info['name'];
            }
            //支付类型列表
            $payTypeObj     = BooController::get('Obj_Plat_PayType');
            $tmpPayTypeList = $payTypeObj->getList();
            $payTypeList    = array();
            foreach ($tmpPayTypeList as $info) {
                $payTypeList[$info['pt_id']] = $info['pt_name'];
            }
            //支付渠道列表
            $platObj     = BooController::get('Obj_Plat_Info');
            $tmpPlatList = $platObj->getList();
            $platList    = array();
            foreach ($tmpPlatList as $info) {
                $platList[$info['p_id']] = $info['p_name'];
            }
            BooView::set('appList', $appList);
            BooView::set('payTypeList', $payTypeList);
            BooView::set('platList', $platList);
            
        }
        
    }
    
    public function apiCharts()
    {
        
        $post        = BooVar::requestx();
        $merchant_id = $post['appId'] ? intval($post['appId']) : '';
        $pay_type_id = $post['payTypeId'];
        $channel_id  = intval($post['platId']);
        $start_time  = $post['startTime'];
        $end_time    = $post['endTime'];
        $min_amount  = intval($post['min_amount']);
        $max_amount  = intval($post['max_amount']);
        
        $page_size      = $post['pageSize'] ? : 20;
        $page_number    = $post['pageNumber'] ? : 1;
        $_GET['pageId'] = $page_number;
        $limit_start    = $page_number == 1 ? 0 : $page_size * ($page_number - 1);
        
        /**
         * @var $obj Obj_App_ConvertRate
         */
        $obj      = BooController::get('Obj_App_ConvertRate');
        $pageList = $obj->getConvertRateList(
            $merchant_id,
            $pay_type_id,
            $channel_id,
            $start_time,
            $end_time,
            $min_amount,
            $max_amount,
            $limit_start,
            $page_size
        );
        $data     = [
            'category' => [],
            'dataset'  => [
                'placed'      => [
                    'seriesname' => '下单金额',
                    "renderAs"   => "bar",
                    "showValues" => "0",
                    'data'       => [],
                ],
                'amount'      => [
                    'seriesname' => '充值金额',
                    "renderAs"   => "bar",
                    "showValues" => "0",
                    'data'       => [],
                ],
                'placed_nums' => [
                    'seriesname' => '下单笔数',
                    "renderAs"   => "bar",
                    "showValues" => "0",
                    'data'       => [],
                ],
                'amount_nums' => [
                    'seriesname' => '成单笔数',
                    "renderAs"   => "bar",
                    "showValues" => "0",
                    'data'       => [],
                ],
                'percent'     => [
                    'seriesname'  => '成单转化率',
                    "parentYAxis" => "S",
                    "renderAs"    => "spline",
                    "showValues"  => "1",
                    'data'        => [],
                ],
            ],
        ];
        if (!empty($pageList['data'])) {
            
            //商户列表
            $appObj     = BooController::get('Obj_App_Info');
            $tmpAppList = $appObj->getList();
            $appList    = array();
            foreach ($tmpAppList as $info) {
                if ($info['is_lock']) {
                    continue;
                }
                $appList[$info['app_id']] = $info['name'];
            }
            
            //支付类型列表
            $payTypeObj     = BooController::get('Obj_Plat_PayType');
            $tmpPayTypeList = $payTypeObj->getList();
            $payTypeList    = array();
            foreach ($tmpPayTypeList as $info) {
                $payTypeList[$info['pt_id']] = $info['pt_name'];
            }
            //支付渠道列表
            $platObj     = BooController::get('Obj_Plat_Info');
            $tmpPlatList = $platObj->getList();
            $platList    = array();
            foreach ($tmpPlatList as $info) {
                $platList[$info['p_id']] = $info['p_name'];
            }
            
            foreach ($pageList['data'] as $key => $item) {
                $data['category'][]                       = ['label' => $item['create_date']];
                $data['dataset']['placed']['data'][]      = [
                    'value' => $this->formatNum($item['total_amount']),
                ];
                $data['dataset']['amount']['data'][]      = [
                    'value' => $this->formatNum($item['amount']),
                ];
                $data['dataset']['amount_nums']['data'][] = [
                    'value' => $this->formatNum($item['num']),
                ];
                $data['dataset']['placed_nums']['data'][] = [
                    'value' => $this->formatNum($item['total_num']),
                ];
                
                if ($item['total_num']) {
                    $data['dataset']['percent']['data'][] = [
                        'value' => number_format((int)$item['num'] / $item['total_num'] * 100, 2),
                    ];
                }
                else {
                    $data['dataset']['percent']['data'][] = [
                        'value' => 0,
                    ];
                }
            }
            
        }
        $data['dataset'] = array_values($data['dataset']);
        die(json_encode($data));
    }
    
    public function formatNum($num)
    {
        
        if ($num > 0) {
            $num = number_format($num / 10000, 2, '.', '');
        }
        
        return $num;
        
    }
    
    public function theader()
    {
        
        return [
            [
                'field' => 'create_date',
                'title' => '统计日期',
                'align' => 'center',
            ],
            [
                'field' => 'app_id',
                'title' => '商户名称',
                'align' => 'center',
            ],
            [
                'field' => 'pt_id',
                'title' => '支付类型',
                'align' => 'center',
            ],
            [
                'field' => 'p_id',
                'title' => '支付渠道',
                'align' => 'center',
            ],
            [
                'field' => 'total_amount',
                'title' => '下单总金额',
                'align' => 'center',
            ],
            [
                'field' => 'amount',
                'title' => '充值总金额',
                'align' => 'center',
            ],
            [
                'field' => 'total_num',
                'title' => '下单总笔数',
                'align' => 'center',
            ],
            [
                'field' => 'num',
                'title' => '成交总笔数',
                'align' => 'center',
            ],
            [
                'field' => 'convert',
                'title' => '成功率',
                'align' => 'center',
            ],
        ];
    }
    
    public function tbody()
    {
        
        $post        = BooVar::requestx();
        $merchant_id = $post['appId'] ? intval($post['appId']) : '';
        $pay_type_id = $post['payTypeId'];
        $channel_id  = intval($post['platId']);
        $start_time  = $post['startTime'];
        $end_time    = $post['endTime'];
        $min_amount  = intval($post['min_amount']);
        $max_amount  = intval($post['max_amount']);
        
        $page_size      = $post['pageSize'] ? : 10;
        $page_number    = $post['pageNumber'] ? : 1;
        $_GET['pageId'] = $page_number;
        $limit_start    = $page_number == 1 ? 0 : $page_size * ($page_number - 1);
        
        /**
         * @var $obj Obj_App_ConvertRate
         */
        $obj      = BooController::get('Obj_App_ConvertRate');
        $pageList = $obj->getConvertRateList(
            $merchant_id,
            $pay_type_id,
            $channel_id,
            $start_time,
            $end_time,
            $min_amount,
            $max_amount,
            $limit_start,
            $page_size
        );
        
        if (!empty($pageList['data'])) {
            
            //商户列表
            $appObj     = BooController::get('Obj_App_Info');
            $tmpAppList = $appObj->getList();
            $appList    = array();
            foreach ($tmpAppList as $info) {
                if ($info['is_lock']) {
                    continue;
                }
                $appList[$info['app_id']] = $info['name'];
            }
            
            //支付类型列表
            $payTypeObj     = BooController::get('Obj_Plat_PayType');
            $tmpPayTypeList = $payTypeObj->getList();
            $payTypeList    = array();
            foreach ($tmpPayTypeList as $info) {
                $payTypeList[$info['pt_id']] = $info['pt_name'];
            }
            //支付渠道列表
            $platObj     = BooController::get('Obj_Plat_Info');
            $tmpPlatList = $platObj->getList();
            $platList    = array();
            foreach ($tmpPlatList as $info) {
                $platList[$info['p_id']] = $info['p_name'];
            }
            
            foreach ($pageList['data'] as $key => $item) {
                
                if (!$item['pt_id']) {
                    $item['pt_id'] = '-';
                }
                if (!$item['p_id']) {
                    $item['p_id'] = '-';
                }
                if ($item['total_num']) {
                    $item['convert'] = number_format((int)$item['num'] / $item['total_num'] * 100, 2) . '%';
                }
                else {
                    $item['convert'] = '0.00%';
                }
                foreach ($item as $k => $v) {
                    if ($k == 'app_id' && !empty($v)) {
                        $item[$k] = $appList[$v];
                    }
                    if ($k == 'pt_id' & $v > 0) {
                        $item[$k] = $payTypeList[$v];
                    }
                    if ($k == 'p_id' & $v > 0) {
                        $item[$k] = $platList[$v];
                    }
                }
                $pageList['data'][$key] = $item;
            }
            
        }
        $sum = '';
        if ($pageList['totalSum']) {
            if ($pageList['totalSum']['total_nums']) {
                $convert_rate = number_format(
                                    (int)$pageList['totalSum']['nums']
                                    / $pageList['totalSum']['total_nums'] * 100,
                                    2
                                ) . '%';
            }
            else {
                $convert_rate = '0.0%';
            }
            $sum = " 下单总金额: <b class='text-danger'>{total_placed_amount}</b> ;";
            $sum .= " 充值总金额: <b class='text-danger'>{total_amount}</b> ;";
            $sum .= " 下单总笔数: <b class='text-info'>{total_placed_nums}</b> ;";
            $sum .= " 成功总笔数:<b class='text-info'>{total_nums}</b> ;";
            $sum .= " 转化率: <b class='text-success'>{convert_rate}</b> ";
            $sum = str_replace('{total_placed_amount}', $pageList['totalSum']['total_amount'], $sum);
            $sum = str_replace('{total_amount}', $pageList['totalSum']['amount'], $sum);
            $sum = str_replace('{total_placed_nums}', $pageList['totalSum']['total_nums'], $sum);
            $sum = str_replace('{total_nums}', $pageList['totalSum']['nums'], $sum);
            $sum = str_replace('{convert_rate}', $convert_rate, $sum);
        }
        
        $total       = "<td colspan='9' style='padding-left: 20px'>" . $sum . '</td>';
        $total_pages = ceil($pageList['totalRows'] / $page_size);
        $url         = '/admin/index?flag=tbody&';
        $data        = [
            'current_page'   => $page_number,
            'first_page_url' => "{$url}page=1",
            'from'           => $page_number,
            'last_page'      => $total_pages,
            'last_page_url'  => "{$url}page=" . $total_pages,
            'next_page_url'  => "{$url}page=" . ($pageList['currentPage'] + 1),
            'path'           => "{$url}",
            'per_page'       => $page_size,
            'prev_page_url'  => null,
            'to'             => $page_size,
            'total'          => $pageList['totalRows'],
            'totalSum'       => $total,
        ];
        
        $data['data'] = $pageList['data'];
        
        die(json_encode($data));
    }
    
    private function getInfo()
    {
        
        $obj  = BooController::get('Obj_Admin_Info');
        $info = $obj->getInfo(BooSession::get('adminId'));
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }
        
        // 00 点暂不结算，等日汇总结算后再进行结算，否则会导致商户余额在5分钟内全部显示为0
        $startTime = date('Y-m-d') . " 00:00:00";
        if (time() < (strtotime($startTime) + 600)) {
            Common_errorCode::jsonEncode(Common_errorCode::SYSTEM_DAY_SETTLE_ACCOUNTS);
        }
        
        $balance = BooController::get('Mod_Admin')->getBalance();
        if ($balance === false) {
            Common_errorCode::jsonEncode(Common_errorCode::NETWORK_FLUCTUATION);
        }
        
        $returnData                         = array();
        $returnData['bank_type']            = $info['bank_type'] ? $info['bank_type'] : '未设置';
        $returnData['card_no']              = $info['card_no'] ? $info['card_no'] : '未设置';
        $returnData['bank_name']            = $info['bank_name'] ? $info['bank_name'] : '未设置';
        $returnData['card_account_name']    = $info['card_account_name'] ? $info['card_account_name'] : '未设置';
        $returnData['available_balance']    = $balance ? $balance : 0;
        $returnData['google_secret_isopen'] = $info['google_secret_isopen'];
        $returnData['withdrawal_fee']       = $info['withdrawal_fee'];
        
        return $returnData;
    }
    
    private function withdrawal()
    {
        
        $post = BooVar::postx();
        
        if (!Common_rule::checkMoneyNumberSafe($post['money'])) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        
        if ($post['money'] <= 0 || !$post['password']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        
        // 00 点暂不结算，等日汇总结算后再进行结算，否则会导致商户余额在5分钟内全部显示为0
        $startTime = date('Y-m-d') . " 00:00:00";
        if (time() < (strtotime($startTime) + 600)) {
            Common_errorCode::jsonEncode(Common_errorCode::SYSTEM_DAY_SETTLE_ACCOUNTS);
        }
        
        $balance = BooController::get('Mod_Admin')->getBalance();
        if ($balance === false) {
            Common_errorCode::jsonEncode(Common_errorCode::NETWORK_FLUCTUATION);
        }
        
        $adminObj = BooController::get('Obj_Admin_Info');
        $info     = $adminObj->getInfo(BooSession::get('adminId'));
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }
        
        if (!$info['card_no'] || !$info['bank_name'] || !$info['card_account_name']) {
            Common_errorCode::jsonEncode(Common_errorCode::CARD_NO_INFO_IS_LESS);
        }
        
        if ($post['money'] + $info['withdrawal_fee'] > $balance) {
            Common_errorCode::jsonEncode(Common_errorCode::CASH_IS_NOT_ENOUGH);
        }
        
        if (!$info['security_pwd']) {
            Common_errorCode::jsonEncode(Common_errorCode::CASH_PASSWORD_IS_LESS);
        }
        
        if ($info['security_pwd'] != Common_rule::encodeSecurityPassword($post['password'], $info['security_salt'])) {
            Common_errorCode::jsonEncode(Common_errorCode::PASSWORD_ERR);
        }
        
        if ($info['google_secret_isopen']) {
            if (!$post['googleCode']) {
                Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
            }
            
            $checkCodeRs = BooGoogleAuthor::getInstance()->verifyCode(
                $info['google_secret_code'],
                $_POST['googleCode']
            );
            if (!$checkCodeRs) {
                Common_errorCode::jsonEncode(Common_errorCode::GOOGLE_CODE_IS_ERROR);
            }
        }
        
        $inserData = array(
            'amount'            => $post['money'],
            'fee'               => $info['withdrawal_fee'],
            'bank_type'         => $info['bank_type'],
            'card_no'           => $info['card_no'],
            'bank_name'         => $info['bank_name'],
            'card_account_name' => $info['card_account_name'],
            'create_time'       => date('Y-m-d H:i:s'),
            'status'            => 1,
            'apply_user'        => BooSession::get('adminName'),
        
        );
        
        $obj = BooController::get('Obj_Admin_Withdrawal');
        $rs  = $obj->insert($inserData);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::INSERT_ERR);
        }
        
        $updateData                      = array();
        $updateData['available_balance'] = $balance - $post['money'] - $info['withdrawal_fee'];
        $updateData['frozen_amount']     = $info['frozen_amount'] + $post['money'] + $info['withdrawal_fee'];
        $updateData['last_modify_time']  = date('Y-m-d H:i:s');
        $rs                              = $adminObj->update($updateData);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }
        
        return true;
    }
    
    private function getAppWithdrawal()
    {
        
        /**
         * 添加商户是否锁定
         */
        $obj      = BooController::get('Obj_Admin_User');
        $userInfo = $obj->getInfoById(BooSession::get('adminId'));
        if ($userInfo['islocked'] == 1) {
            BooSession::destroy();
            Common_errorCode::jsonEncode(Common_errorCode::USER_LOCKED);
        }
        
        /**
         * 添加唯一登录验证
         */
        if (BooSession::get('ACCOUNT_LOGIN_OTHER') == 'END') {
            BooSession::destroy();
            Common_errorCode::jsonEncode(Common_errorCode::ACCOUNT_LOGIN_OTHER_DEVICE);
        }
        if ($userInfo['login_session_id'] != BooSession::getSessionId()) {
            BooSession::set('ACCOUNT_LOGIN_OTHER', 'SUCCESS');
        }
        
        $startTime = date('Y-m-d') . " 00:00:00";
        $endTime   = date('Y-m-d H:i:s');
        
        // 读取商户未提款笔数
        $appWithdrawalObj   = BooController::get('Obj_App_Withdrawal');
        $appWithdrawalTimes = $appWithdrawalObj->getCount(0, array(0), $startTime, $endTime);
        
        $returnData          = array();
        $returnData['times'] = $appWithdrawalTimes;
        
        return $returnData;
    }
    
    public function getProxyAppWithdrawal()
    {
        $startTime = date('Y-m-d') . " 00:00:00";
        $endTime   = date('Y-m-d H:i:s');
        // 读取代理未提款笔数
        $appWithdrawalObj        = BooController::get('Obj_Proxy_Withdrawal');
        $appProxyWithdrawalTimes = $appWithdrawalObj->getCount(0, array(0), $startTime, $endTime);
        
        $returnData          = array();
        $returnData['times'] = $appProxyWithdrawalTimes;
        
        return $returnData;
    }
}
