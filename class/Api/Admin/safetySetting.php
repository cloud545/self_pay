<?php

class Api_Admin_safetySetting extends Api_Admin
{
    
    protected $_tpl = "safetySetting.html";
    
    protected function _do()
    {
        
        if ($_REQUEST
            && in_array(
                $_REQUEST['flag'],
                array(
                    'securityPwd',
                    'safeQuestion',
                    'make_google_secret',
                    'getGoogleCodeQr',
                    'googleSecret',
                    'setWithdrawalFee',
                )
            )) {
            
            switch ($_REQUEST['flag']) {
                case 'securityPwd':
                    return $this->securityPwdSetting();
                case 'safeQuestion':
                    return $this->safeQuestionSetting();
                case 'getGoogleCodeQr':
                    return $this->getGoogleCodeQr();
                case 'googleSecret':
                    return $this->googleSecret();
                case 'setWithdrawalFee':
                    return $this->setWithdrawalFee();
                case 'make_google_secret':
                    return $this->makeGoogleCode();
                default:
                    Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
                    
                    return false;
            }
            
        }
        else {
            
            /**
             * @var $obj Obj_Admin_Info
             */
            $obj      = BooController::get('Obj_Admin_Info');
            $userInfo = $obj->getInfo(BooSession::get('adminId'));
            
            $info                       = array();
            $info['safeLevel']          = 1;
            $info['loginPwdIsSet']      = 1;
            $info['securityPwdIsSet']   = 0;
            $info['safeQuestionIsSet']  = 0;
            $info['safeQuestionId']     = 0;
            $info['googleSecretIsSet']  = 0;
            $info['googleSecretIsopen'] = 0;
            
            if ($userInfo['security_pwd']) {
                $info['securityPwdIsSet'] = 1;
                $info['safeLevel']        += 1;
            }
            
            if ($userInfo['safe_question']) {
                $info['safeQuestionId']    = $userInfo['safe_question'];
                $info['safeQuestionIsSet'] = 1;
                $info['safeLevel']         += 1;
            }
            
            if ($userInfo['google_secret_isopen']) {
                $info['googleSecretIsSet']  = 1;
                $info['googleSecretIsopen'] = 1;
                $info['safeLevel']          += 1;
            }
            
            // 创建问题模型
            $questionObj  = BooController::get('Obj_Admin_Question');
            $list         = $questionObj->getList();
            $questionList = array();
            foreach ($list as $tmpInfo) {
                $questionList[$tmpInfo['id']] = $tmpInfo['question'];
            }
            $info['questionList'] = $questionList;
            
            BooView::set('withdrawalFee', $userInfo['withdrawal_fee']);
            BooView::set('info', $info);
        }
        
    }
    
    private function setWithdrawalFee()
    {
        $post = BooVar::postx();
        if ($post['withdrawalFee'] <= 0) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        
        $adminObj = BooController::get('Obj_Admin_Info');
        $info     = $adminObj->getInfo(BooSession::get('adminId'));
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }
        
        $updateData                     = array();
        $updateData['withdrawal_fee']   = $post['withdrawalFee'];
        $updateData['last_modify_time'] = date('Y-m-d H:i:s');
        $rs                             = $adminObj->update($updateData, BooSession::get('adminId'));
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }
        
        return true;
    }
    
    private function securityPwdSetting()
    {
        
        $post = BooVar::postx();
        $obj  = BooController::get('Obj_Admin_Info');
        $info = $obj->getInfo(BooSession::get('adminId'));
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }
        
        if (!$info['safe_answer'] || $post['answer'] != $info['safe_answer']) {
            Common_errorCode::jsonEncode(Common_errorCode::SAFE_ANSWER_IS_ERROR);
        }
        
        // 区分添加资金密码和修改密码，如果已经设置过资金密码，则更新资金密码
        if ($post['isSet']) {
            $oldSecurityPwd = $post['oldSecurityPwd'];
            $securityPwd    = $post['newSecurityPwd'];
            $securityPwd2   = $post['newSecurityPwd2'];
            
            if (!$info['security_pwd']) {
                Common_errorCode::jsonEncode(Common_errorCode::CASH_PASSWORD_IS_LESS);
            }
            
            // 判断旧密码是否正确
            if ($info['security_pwd'] != Common_rule::encodeSecurityPassword($oldSecurityPwd, $info['security_salt'])) {
                Common_errorCode::jsonEncode(Common_errorCode::PASSWORD_ERR);
            }
            
        }
        else {
            
            if ($info['security_pwd']) {
                Common_errorCode::jsonEncode(Common_errorCode::HAVE_SET_PWD);
            }
            
            // 添加资金密码
            $securityPwd  = $post['securityPwd'];
            $securityPwd2 = $post['securityPwd2'];
        }
        
        if ($securityPwd != $securityPwd2) {
            Common_errorCode::jsonEncode(Common_errorCode::PASSWORD_DIFFERENT);
        }
        
        // 数据入库
        $updateData                     = array();
        $updateData['security_salt']    = Common_rule::getRandSaltStr();
        $updateData['security_pwd']     = Common_rule::encodeSecurityPassword(
            $securityPwd,
            $updateData['security_salt']
        );
        $updateData['last_modify_time'] = date('Y-m-d H:i:s');
        $rs                             = $obj->update($updateData, BooSession::get('adminId'));
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }
        
        return true;
    }
    
    private function safeQuestionSetting()
    {
        
        $post         = BooVar::postx();
        $safeQuestion = $post['safeQuestion'];
        $answer       = $post['answer'];
        
        /**
         * @var $obj Obj_Admin_Info
         */
        $obj  = BooController::get('Obj_Admin_Info');
        $info = $obj->getInfo(BooSession::get('adminId'));
        
        if (!$info) {
            $updateData                     = array();
            $updateData['safe_question']    = intval($safeQuestion);
            $updateData['safe_answer']      = $answer;
            $updateData['last_modify_time'] = date('Y-m-d H:i:s');
            $updateData['admin_id']         = BooSession::get('adminId');
            $rs                             = $obj->insert($updateData);
            if (!$rs) {
                Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
            }
            
            return array('safeQuestionId' => intval($safeQuestion));
        }
        
        if (!$safeQuestion || !$answer) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        
        // 区分添加安全问题和修改安全问题，如果已经设置过安全问题，则更新安全问题
        if ($post['isSet']) {
            $oldAnswer = $post['oldAnswer'];
            
            // 判断旧答案是否正确
            if ($oldAnswer != $info['safe_answer']) {
                Common_errorCode::jsonEncode(Common_errorCode::SAFE_ANSWER_IS_ERROR);
            }
        }
        else {
            if ($info['safe_question']) {
                Common_errorCode::jsonEncode(Common_errorCode::SAFE_ANSWER_IS_LESS);
            }
        }
        
        $updateData                     = array();
        $updateData['safe_question']    = intval($safeQuestion);
        $updateData['safe_answer']      = $answer;
        $updateData['last_modify_time'] = date('Y-m-d H:i:s');
        $rs                             = $obj->update($updateData, BooSession::get('adminId'));
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }
        
        return array('safeQuestionId' => intval($safeQuestion));
    }
    
    private function googleSecret()
    {
        
        $post = BooVar::postx();
        $obj  = BooController::get('Obj_Admin_Info');
        $info = $obj->getInfo(BooSession::get('adminId'));
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }
        
        if (!$post['answer'] || $post['answer'] != $info['safe_answer']) {
            Common_errorCode::jsonEncode(Common_errorCode::SAFE_ANSWER_IS_ERROR);
        }
        
        if (!$info['security_pwd']) {
            Common_errorCode::jsonEncode(Common_errorCode::CASH_PASSWORD_IS_LESS);
        }
        
        if ($info['security_pwd'] != Common_rule::encodeSecurityPassword($post['password'], $info['security_salt'])) {
            Common_errorCode::jsonEncode(Common_errorCode::PASSWORD_ERR);
        }
        
        if (!$post['isAddGoogleCode']) {
            
            $checkCodeRs = BooGoogleAuthor::getInstance()->verifyCode($info['google_secret_code'], $post['googleCode']);
            if (!$checkCodeRs) {
                Common_errorCode::jsonEncode(Common_errorCode::GOOGLE_CODE_IS_ERROR);
            }
            
            // 数据入库
            $updateData                         = array();
            $updateData['google_secret_isopen'] = $post['isLockGoogleCode'];
            $updateData['last_modify_time']     = date('Y-m-d H:i:s');
            
            if (!$post['isLockGoogleCode']) {
                $updateData['google_secret_code'] = '';
            }
            
            $rs = $obj->update($updateData, BooSession::get('adminId'));
            if (!$rs) {
                Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
            }
            
        }
        else {
            $checkCodeRs = BooGoogleAuthor::getInstance()->verifyCode(
                BooSession::get('adminGauthorSecret'),
                $post['googleCode']
            );
            if (!$checkCodeRs) {
                Common_errorCode::jsonEncode(Common_errorCode::GOOGLE_CODE_IS_ERROR);
            }
            
            $updateData                         = array();
            $updateData['google_secret_code']   = BooSession::get('adminGauthorSecret');
            $updateData['google_secret_isopen'] = 1;
            $updateData['last_modify_time']     = date('Y-m-d H:i:s');
            $rs                                 = $obj->update($updateData, BooSession::get('adminId'));
            if (!$rs) {
                Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
            }
        }
        
        return true;
    }
    
    private function getGoogleCodeQr()
    {
        
        $obj  = BooController::get('Obj_Admin_Info');
        $info = $obj->getInfo(BooSession::get('adminId'));
        if (!$info['google_secret_code']) {
            $secret = BooGoogleAuthor::getInstance()->createSecret();
        }
        else {
            $secret = $info['google_secret_code'];
        }
        
        BooSession::set('adminGauthorSecret', $secret);
        $adminId                 = BooSession::get('adminId');
        $url                     = BooGoogleAuthor::getInstance()->getQRCodeGoogleUrl("pay_admin_{$adminId}", $secret);
        $returnData              = array();
        $returnData['codeQRUrl'] = $url;
        
        return $returnData;
    }
    
    public function makeGoogleCode()
    {
        
        /**
         * @var $admins Obj_Admin_User
         */
        $admins = BooController::get('Obj_Admin_User');
        $info   = $admins->getList();
        /**
         * @var $obj Obj_Admin_Info
         */
        $obj = BooController::get('Obj_Admin_Info');
        foreach ($info as $key => $item) {
            //重新生成
            //if ($item['admin_id'] <= 35) {
            //    continue;
            //}
            $updateData                         = array();
            $updateData['google_secret_code']   = BooGoogleAuthor::getInstance()->createSecret();
            $updateData['google_secret_isopen'] = 1;
            $updateData['last_modify_time']     = date('Y-m-d H:i:s');
            $updateData['admin_id']             = $item['admin_id'];
            
            $is_exist = $obj->getInfo($item['admin_id']);
            if (!empty($is_exist)) {
                
                /**
                 * 防止一个星期内更改多次
                 */
                $tm = strtotime($is_exist['last_modify_time']);
                if (time() - $tm < 7 * 86400) {
                    echo $item['admin_id']
                         . '---'
                         . $item['admin_name']
                         . '---'
                         . 'Google秘钥每星期只能更改一次！'
                         . '<br>';
                    continue;
                }
                
                /**
                 * 更新新的Google key
                 */
                $rs = $obj->update($updateData, $item['admin_id']);
                if ($rs) {
                    echo $item['admin_id']
                         . '---'
                         . $item['admin_name']
                         . '---'
                         . $updateData['google_secret_code']
                         . '<br>';
                }
                else {
                    echo '更新失败!' . $item['admin_id'] . '<br>';
                }
            }
            else {
                $rs = $obj->insert($updateData);
                if ($rs) {
                    echo $item['admin_id']
                         . '---'
                         . $item['admin_name']
                         . '---'
                         . $updateData['google_secret_code']
                         . '<br>';
                }
                else {
                    echo '已经存在!' . $item['admin_id'] . '<br>';
                }
            }
        }
        die('make google finish!');
    }
}
