<?php
class Api_Admin_exportExcel extends Api_Admin {

	protected function _do() {
        $post = $_GET;
        $app_id = $post['app_id'];
        $startTime = $post['startTime'];
        $endTime = $post['endTime'];
        $p_id = $post['p_id'];
        $type_id = $post['type_id'];

        $obj = BooController::get('Obj_Admin_Pay');
        $data = $obj->getList($app_id, $p_id, '', $type_id, $startTime, $endTime);
        if (!$data) {
            return false;
        }

        $typeObj = BooController::get('Obj_Plat_PayType');
        $typeList = $typeObj->getList();
        $typeArr = [];
        foreach ($typeList as $key => $value) {
            $typeArr[$value['pt_id']] = $value['pt_name'];
        }


        $platObj = BooController::get('Obj_Plat_Info');
        $platList = $platObj->getList();
        $platArr = [];
        foreach ($platList as $key => $value) {
            $platArr[$value['p_id']] = $value['p_name'];
        }
        
        $list = array();
        foreach ($data as $key => $info) {
            /**
             * 获取订单信息
             */
            $tmpAppId = BooController::get('Common')->numberAndStrSwap($info['app_id']);
            $orderId = BooController::get('Common')->numberAndStrSwap($info['pay_id']);
            $orderPlatId = BooController::get('Common')->numberAndStrSwap($info['p_id']);
            $app_order= "{$tmpAppId}1{$orderId}1{$orderPlatId}";
    
            $tmpArr = array(
                $info['id'],                // id
                $info['app_name'],          // 商户
                $platArr[$info['p_id']],    // 渠道
                $typeArr[$info['pt_id']],   // 支付类型
                $info['order_id']. ' ',          // 充值订单号
                $app_order, // 商户订单号
                $info['amount'],            // 充值金额
                $info['real_amount'],       // 到账金额
                $info['create_time'],       // 充值时间
                $info['end_time'],          // 到账时间
                $info['current_fee_rate'],  // 点数
                $info['current_fee'],       // 返点金额
            );

            $list[] = $tmpArr;
        }

        $excelObj = new BooExcel();
        //$excelObj = $objPHPExcel = PHPExcel_IOFactory::createReader();

        //横向单元格标识
        $cellName = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ');
        $title = array('序号', '商户', '渠道', '支付类型', '充值订单号', '商户订单号', '充值金额', '真实到账金额', '充值时间', '完成时间', '点数', '返点金额');

        $excelObj->getActiveSheet(0)->setTitle('sheet1');   //设置sheet名称
        $_row = 1;   //设置纵向单元格标识
        $_cnt = count($title);
        $excelObj->getActiveSheet(0)->mergeCells('A'.$_row.':'.$cellName[$_cnt-1].$_row);   //合并单元格
        $excelObj->setActiveSheetIndex(0)->setCellValue('A'.$_row, '数据导出：'.date('Y-m-d H:i:s'));  //设置合并后的单元格内容
        $_row++;
        $i = 0;

        foreach($title as $v){   //设置列标题
            $excelObj->setActiveSheetIndex(0)->setCellValue($cellName[$i].$_row, $v);
            $i++;
        }
        $_row++;


        //填写数据
        if($list){
            $i = 0;
            foreach($list AS $_v){
                $j = 0;
                foreach($_v AS $_cell){
                    $excelObj->getActiveSheet(0)->setCellValue($cellName[$j] . ($i+$_row), $_cell);
                    $j++;
                }

                $i++;
            }


        }

        $objWrite = PHPExcel_IOFactory::createWriter($excelObj,'Excel2007');

        header('pragma:public');
        header("Content-Disposition:attachment;filename=充值订单列表" . $startTime .'到'.$endTime .".xls");

        $objWrite->save('php://output');
        exit();
    }
}
