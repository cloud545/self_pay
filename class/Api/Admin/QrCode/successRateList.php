<?php
class Api_Admin_QrCode_successRateList extends Api_Admin_QrCode {

    protected $_tpl = "qrCode/successRateList.html";

	protected function _do() {

        if ($_POST && in_array($_POST['flag'], array('getAccountList'))) {

            switch ($_POST['flag']) {
                case 'getAccountList':
                    return $this->getAccountList();
                default:
                    Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
                    return false;
            }

        } else {

            $post = BooVar::requestx();
            $qrCodeUserId = $post['qrCodeUserId'];
            $qrCodeId = $post['qrCodeId'];
            $payTypeId = $post['payTypeId'];

            // 记录成功率
            $cacheDao = Dao_Redis_Default::getInstance();

            $payLogListCacheInfo = array();
            if ($qrCodeUserId) {
                $payLogListCacheKey = BooConfig::get('cacheKey.qrPayUserPayLogList') . $qrCodeUserId;
                $payLogListCacheInfo = $cacheDao->get($payLogListCacheKey);
                if ($payLogListCacheInfo) {
                    $payLogListCacheInfo = json_decode($payLogListCacheInfo, true);
                } else {
                    $payLogListCacheInfo = array();
                }
            }

            if ($payLogListCacheInfo) {
                krsort($payLogListCacheInfo);
            }

            $payLogList = array(
                'list' => array(),
                'successRate' => 0,
                'allNum' => 0,
                'successNum' => 0,
                'successRate25' => 0,
                'allNum25' => 0,
                'successNum25' => 0,
            );
            $listNum = $allNum = $successNum = $allNum25 = $successNum25 = 0;
            foreach ($payLogListCacheInfo as $tmpInfo) {

                if ($qrCodeId && $tmpInfo['qrCodeId'] != $qrCodeId) {
                    continue;
                }

                if ($payTypeId && $tmpInfo['payType'] != $payTypeId) {
                    continue;
                }

                if ($tmpInfo['payType'] == 2) {
                    $tmpInfo['payTypeName'] = '支付宝';
                } elseif ($tmpInfo['payType'] == 3) {
                    $tmpInfo['payTypeName'] = '微信';
                }

                $allNum++;
                if ($tmpInfo['result']) {
                    $successNum++;
                }

                if ($listNum >= 25) {
                    continue;
                }

                $allNum25++;
                if ($tmpInfo['result']) {
                    $successNum25++;
                }

                $tmpInfo['create_time'] = date('Y-m-d H:i:s', $tmpInfo['create_time']);
                $tmpInfo['end_time'] = date('Y-m-d H:i:s', $tmpInfo['end_time']);

                $payLogList['list'][] = $tmpInfo;
                $listNum++;
            }

            if ($successNum > 0) {
                $payLogList['successRate'] = number_format(($successNum / $allNum) * 100, 2, '.', '');
                $payLogList['successNum'] = $successNum;
                $payLogList['successRate25'] = number_format(($successNum25 / $allNum25) * 100, 2, '.', '');
                $payLogList['successNum25'] = $successNum25;
            }
            $payLogList['allNum'] = $allNum;
            $payLogList['allNum25'] = $allNum25;

            if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
                return $payLogList;
            } else {

                $qrCodeUserList = array();
                $obj = BooController::get('Obj_QrPay_CodeUser');
                $tmpList = $obj->getList();
                foreach ($tmpList as $info) {
                    $qrCodeUserList[$info['id']] = $info['name'];
                }

                BooView::set('qrCodeUserList', $qrCodeUserList);
                BooView::set('payLogList', $payLogList);
            }
        }

	}

    private function getAccountList() {

        $post = BooVar::postx();
        $payTypeId = $post['payTypeId'];
        $qrCodeUserId = $post['qrCodeUserId'] ? $post['qrCodeUserId'] : 0;

        $codeObj = BooController::get('Obj_QrPay_CodeInfo');
        $codeList = $codeObj->getList($payTypeId, $qrCodeUserId);

        $list = array();
        foreach ($codeList as $info) {
            $list[$info['id']] = $info['pay_account'];
        }

        return $list;
    }

}