<?php
class Api_Admin_QrCode_payList extends Api_Admin_QrCode {

    protected $_tpl = "qrCode/payList.html";

	protected function _do(){

        $post = BooVar::postx();
        $obj = BooController::get('Obj_QrPay_Pay');

        // 如果搜索时间段，最多只能搜索7天的数据
        $startTime = $post['startTime'];
        $endTime = $post['endTime'];
        if ($startTime) {
            $timeGrap = strtotime($endTime) - strtotime($startTime);
            if ($timeGrap < 0 || $timeGrap > 30 * 86400) {
                Common_errorCode::jsonEncode(Common_errorCode::SEARCH_MAX_DAY_30);
            }
        }

        $sm = date('m', strtotime($startTime));
        $em = date('m', strtotime($endTime));
        if ($sm != $em) {
            Common_errorCode::jsonEncode(Common_errorCode::ONLY_SELECT_SAME_MONTH);
        }

        $result = $post['result'] ? $post['result'] : 1;
        if ($post['result'] == 'all') {
            $result = '';
        }

        $payType = $post['payType'];
        if ($post['payType'] == 'all') {
            $payType = 0;
        }

        $qrCodeUserId = $post['qrCodeUserId'] ? $post['qrCodeUserId'] : 'all';
        $appId = $post['appId'] ? $post['appId'] : 0;
        $payNo = $post['payNo '] ? $post['payNo '] : '';
        $appOrderId = $post['orderId'] ? $post['orderId'] : '';
        $_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数

        // 没有传入查询日期，默认取当天的
        if (!($startTime && $endTime)) {
            $startTime = date('Y-m-d') .  ' 00:00:00';
            $endTime = date('Y-m-d') .  ' 23:59:59';
        }

        // 获取分页数据
        $noOrderList = array();
        $mergeOrderId = 0;
        $dataList = $obj->getPageList($startTime, $endTime, $result, $payType, $qrCodeUserId, $appOrderId, $appId, $payNo);
        foreach ($dataList['data'] as $key => $info) {
            $dataList['data'][$key]['payTypeName'] = $info['pt_id'] == 2 ? '支付宝' : '微信';
            $dataList['data'][$key]['resultName'] = $info['result'] == 1 ? '已支付' : '支付中';
            $dataList['data'][$key]['content'] = $info['content'] ? $info['content'] : '';

            if ($info['notify_status'] == 1) {
                $dataList['data'][$key]['notifyStatusName'] = '通知成功';
            } elseif ($info['notify_status'] == 2) {
                $dataList['data'][$key]['notifyStatusName'] = '通知失败';
            } else {
                $dataList['data'][$key]['notifyStatusName'] = '未通知';
            }

            if ($appOrderId && $info['result'] == 0) {
                $noOrderList = $obj->getHavePayAndNoOrderList($startTime, $endTime, $payType, $qrCodeUserId, $info['amount']);
                if ($noOrderList) {
                    $mergeOrderId = $info['id'];
                    $dataList['data'][$key]['haveMerge'] = 1;
                }
            }
        }

        if ($noOrderList) {

            foreach ($noOrderList as $info) {

                $info['mergeOrderId'] = $mergeOrderId;
                $info['payTypeName'] = $info['pt_id'] == 2 ? '支付宝' : '微信';
                $info['resultName'] = $info['result'] == 1 ? '已支付' : '支付中';

                if ($info['notify_status'] == 1) {
                    $info['notifyStatusName'] = '通知成功';
                } elseif ($info['notify_status'] == 2) {
                    $info['notifyStatusName'] = '通知失败';
                } else {
                    $info['notifyStatusName'] = '未通知';
                }

                $dataList['data'][] = $info;
            }
        }

        $tallMoney = 0;
        if ($dataList['data']) {
            $tallMoney = $obj->getSum($startTime, $endTime, $payType, $qrCodeUserId);
            $tallMoney = $tallMoney ? $tallMoney : 0;
        }

        // 如果是快捷搜索，需要返回起始和结束时间
        $dataList['startTime'] = $startTime;
        $dataList['endTime'] = $endTime;
        $dataList['tallMoney'] = $tallMoney;

        if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
            return $dataList;
        } else {

            $appObj = BooController::get('Obj_App_Info');
            $tmpAppList = $appObj->getList();

            $appList = array();
            foreach ($tmpAppList as $info) {
                if ($info['is_lock']) {
                    continue;
                }
                $appList[$info['app_id']] = $info['name'];
            }

            $qrCodeUserList = array();
            $obj = BooController::get('Obj_QrPay_CodeUser');
            $tmpList = $obj->getList();
            foreach ($tmpList as $info) {
                $qrCodeUserList[$info['id']] = $info['name'];
            }

            BooView::set('appList', $appList);
            BooView::set('qrCodeUserList', $qrCodeUserList);
            BooView::set('pageList', $dataList);
        }

	}

}