<?php
class Api_Admin_QrCode_list extends Api_Admin_QrCode {

    protected $_tpl = "qrCode/list.html";

	protected function _do(){

        if ($_POST && in_array($_POST['flag'], array('update'))) {

            switch ($_POST['flag']) {
                case 'add':
                    return $this->add();
                case 'update':
                    return $this->update();
                case 'del':
                    return $this->del();
                default:
                    Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
                    return false;
            }
        } else {

            $post = BooVar::postx();
            $qrCodeUserId = $post['qrCodeUserId'] ? $post['qrCodeUserId'] : 'all';
            $payType = $post['payType'] ? $post['payType'] : 'all';
            $account = $post['account'] ? $post['account'] : 'all';
            $isOpen = is_numeric($post['isOpen']) ? $post['isOpen'] : 'all';
            $_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数

            $obj = BooController::get('Obj_QrPay_CodeInfo');
            $pageList = $obj->getPageList($payType, $qrCodeUserId, $isOpen, $account);
            foreach ($pageList['data'] as $key => $info) {
                if ($info['pt_id'] == 2) {
                    $info['payTypeName'] = '支付宝';
                } elseif ($info['pt_id'] == 3) {
                    $info['payTypeName'] = '微信';
                }
                $pageList['data'][$key] = $info;
            }

            if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
                return $pageList;
            } else {

                $qrCodeUserList = array();
                $obj = BooController::get('Obj_QrPay_CodeUser');
                $tmpList = $obj->getList();
                foreach ($tmpList as $info) {
                    $qrCodeUserList[$info['id']] = $info['name'];
                }

                BooView::set('qrCodeUserList', $qrCodeUserList);
                BooView::set('pageList', $pageList);
            }
        }

	}

    private function update() {

        $post = BooVar::postx();
        $id = intval($post['id']);

        $obj = BooController::get('Obj_QrPay_CodeInfo');
        $info = $obj->getInfoById($id);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::NO_DATA);
        }

        $updateData = array();

        // 单纯操作是否锁定只更新这个值就行
        if ($post['setOpen']) {
            $updateData['is_open'] = intval($post['isOpen']);
            $updateData['last_time'] = date('Y-m-d H:i:s');
            $rs = $obj->update($id, $updateData);
            if (!$rs) {
                Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
            }
        }

        return true;
    }

}
