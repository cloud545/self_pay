<?php
class Api_Admin_QrCode_pushOrder extends Api_Admin_QrCode {

	protected function _do() {

        $post = BooVar::requestx();
        if (!$post['createTime'] || !$post['id']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }
        $orderId = intval($post['id']);

        $qrPayObj = BooController::get('Obj_QrPay_Pay');
        $orderInfo = $qrPayObj->getInfoById($post['createTime'], $orderId);
        if (!$orderInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::ORDER_NOT_EXIST);
        }
        if ($orderInfo['result'] != 1) {
            Common_errorCode::jsonEncode(Common_errorCode::ORDER_NOT_EXIST);
        }

        if ($orderInfo['notify_status'] == 1 || !$orderInfo['app_id'] || !$orderInfo['plat_order_id']) {
            Common_errorCode::jsonEncode(Common_errorCode::PAY_IS_END);
        }

        $appId = BooController::get('Common')->numberAndStrSwap($orderInfo['app_id']);
        $pOrderId = BooController::get('Common')->numberAndStrSwap($orderInfo['plat_order_id']);
        $platId = BooController::get('Common')->numberAndStrSwap($orderInfo['p_id']);
        $qrCodeUserIdStr = BooController::get('Common')->numberAndStrSwap($orderInfo['qrcode_user_id']);
        $qrCodeIdStr = BooController::get('Common')->numberAndStrSwap($orderInfo['qrcode_id']);

        if (BooUtil::isHttps()) {
            $noticeUrl = 'https://' . $_SERVER['HTTP_HOST'] . '/payCallback/' . "{$appId}1{$pOrderId}1{$platId}1{$qrCodeUserIdStr}1{$qrCodeIdStr}";
        } else {
            $noticeUrl = 'http://' . $_SERVER['HTTP_HOST'] . '/payCallback/' . "{$appId}1{$pOrderId}1{$platId}1{$qrCodeUserIdStr}1{$qrCodeIdStr}";
        }

        $nowDateTime = date('Y-m-d H:i:s');

        $noticeData = array();
        $noticeData['amount'] = $orderInfo['amount'];
        $noticeData['realMoney'] = $orderInfo['real_amount'];
        $noticeData['platOrderId'] = $orderInfo['id'];
        $noticeData['createTime'] = $orderInfo['create_time'];
        $noticeData['endTime'] = $orderInfo['end_time'];

        ksort($noticeData);
        $md5Str = '';
        foreach ($noticeData as $key => $value) {

            if (!$md5Str) {
                $md5Str .= "{$key}={$value}";
            } else {
                $md5Str .= "&{$key}={$value}";
            }
        }

        $sign = md5($md5Str . 'rtsj64i543nb$jh*gjkg8368#');
        $noticeData['sign'] = $sign;

        $updateData = array();
        BooCurl::setData($noticeData, 'POST');
        $result = BooCurl::call($noticeUrl);

        if ($result == 'success') {
            $updateData['notify_status'] = 1;
        } else {
            $updateData['notify_status'] = 2;
        }

        $updateData['notify_time'] = $nowDateTime;
        $qrPayObj->update($post['createTime'], $orderInfo['id'], $updateData);

        if ($result != 'success') {
            Common_errorCode::jsonEncode(Common_errorCode::FAIL, array(), $result);
        }

        return true;
	}

}
