<?php
class Api_Admin_QrCode_mergePayInfo extends Api_Admin_QrCode {

	protected function _do() {

        $post = BooVar::requestx();
        if (!$post['createTime'] || !$post['id'] || !$post['mergeOrderId']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }
        $orderId = intval($post['id']);
        $mergeOrderId = intval($post['mergeOrderId']);

        $qrPayObj = BooController::get('Obj_QrPay_Pay');
        $orderInfo = $qrPayObj->getInfoById($post['createTime'], $orderId);
        if (!$orderInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::ORDER_NOT_EXIST);
        }

        $mergeOrderInfo = $qrPayObj->getInfoById($post['createTime'], $mergeOrderId);
        if (!$mergeOrderInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::ORDER_NOT_EXIST);
        }
        if ($mergeOrderInfo['result'] == 1) {
            Common_errorCode::jsonEncode(Common_errorCode::PAY_IS_END);
        }

        $updateData = array();
        $updateData['account'] = $orderInfo['account'];
        $updateData['real_amount'] = $orderInfo['real_amount'];
        $updateData['result'] = 1;
        $updateData['pay_no'] = $orderInfo['pay_no'];
        $updateData['content'] = $orderInfo['content'];
        $updateData['end_time'] = date('Y-m-d H:i:s');
        $qrPayObj->update($post['createTime'], $mergeOrderInfo['id'], $updateData);

        $qrPayObj->del($post['createTime'], $orderInfo['id']);

        return true;
	}

}
