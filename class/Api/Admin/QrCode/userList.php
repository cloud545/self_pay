<?php
class Api_Admin_QrCode_userList extends Api_Admin_QrCode {

    protected $_tpl = "qrCode/userList.html";

	protected function _do() {

        if ($_POST && in_array($_POST['flag'], array('add', 'update', 'del', 'changeKey', 'resetLoginPassword', 'getAppList'))) {

            switch ($_POST['flag']) {
                case 'add':
                    return $this->add();
                case 'update':
                    return $this->update();
                case 'del':
                    return $this->del();
                case 'changeKey':
                    return $this->changeKey();
                case 'resetLoginPassword':
                    return $this->resetLoginPassword();
                default:
                    Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
                    return false;
            }

        } else {

            $post = BooVar::postx();
            $name = $post['name'];
            $_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数
            $obj = BooController::get('Obj_QrPay_CodeUser');
            $pageList = $obj->getPageList($name);

            foreach ($pageList['data'] as $key => $info) {

                $tmpArr = array();
                $tmpArr['id'] = $info['id'];
                $tmpArr['name'] = $info['name'];
                $tmpArr['adminName'] = $info['add_admin'];
                $tmpArr['allow_login_ip'] = $info['allow_login_ip'];
                $tmpArr['last_ip'] = $info['last_ip'];
                $tmpArr['create_time'] = $info['create_time'];
                $tmpArr['last_time'] = $info['last_update_time'];
                $tmpArr['is_lock'] = $info['is_lock'];
                $tmpArr['point'] = $info['point'];
                $tmpArr['balance'] = $info['balance'];

                $pageList['data'][$key] = $tmpArr;
            }

            if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
                return $pageList;
            } else {
                BooView::set('pageList', $pageList);
            }
        }
	}

    private function update() {
        $post = BooVar::postx();

        if (!$post['id']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        $id = intval($post['id']);

        $obj = BooController::get('Obj_QrPay_CodeUser');
        $info = $obj->getInfoById($id);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }

        $updateData = array();
        // 单纯操作是否锁定只更新这个值就行
        if ($post['onOffLock']) {
            $updateData['is_lock'] = intval($post['isLock']);
        } else {
            $updateData['allow_login_ip'] = $post['allowLoginIp'];
            $updateData['balance'] = $post['balance'];
            $updateData['point'] = $post['point'] ? $post['point'] : 0.005;
        }

        $rs = $obj->update($id, $updateData);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }

        return true;
    }

    private function add() {

        $post = BooVar::postx();
        if (!$post['name'] || !$post['password']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }

        $obj = BooController::get('Obj_QrPay_CodeUser');
        $userInfo = $obj->getInfoByName($post['name']);
        if ($userInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_IS_EXIST);
        }

        $salt = Common_rule::getRandSaltStr();
        $password = Common_rule::encodeLoginPassword($post['password'], $salt);

        $inserData = array(
            'name' => $post['name'],
            'password' => $password,
            'salt' => $salt,
            'sign_key' => md5(time() . rand(1, 99)),
            'create_time' => date('Y-m-d H:i:s'),
            'add_admin' => BooSession::get('adminName'),
            'balance' => $post['balance'] ? $post['balance'] : 0,
            'point' => $post['point'] ? $post['point'] : 0.005,
        );

        $rs = $obj->insert($inserData);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::INSERT_ERR);
        }

        return true;
    }

    private function resetLoginPassword() {

        $post = BooVar::postx();

        if (!$post['id']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        $id = intval($post['id']);

        $obj = BooController::get('Obj_QrPay_CodeUser');
        $info = $obj->getInfoById($id);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }

        $salt = Common_rule::getRandSaltStr();
        $password = Common_rule::encodeLoginPassword('123qwe', $salt);

        $updateData = array();
        $updateData['password'] = $password;
        $updateData['salt'] = $salt;
        $rs = $obj->update($id, $updateData);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }

        return array('password' => '123qwe');
    }

    private function del() {
        $id = intval(BooVar::post('id'));
        if (!$id) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }

        $obj = BooController::get('Obj_QrPay_CodeUser');
        $userInfo = $obj->getInfoById($id);
        if (!$userInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }

        $rs = $obj->delete($id);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::DELETE_ERR);
        }

        return true;
    }

    private function changeKey() {
        $key = md5(time() . rand(1, 99));
        return array('key' => $key);
    }

}