<?php

class Api_Admin_User_login extends Api_Admin_User
{
    
    protected $_tpl = "user/login.html";
    
    protected function _do()
    {
        
        if ($_POST) {
            
            $post = BooVar::postx('password', 'userName', 'captcha');
            if (!$post['password'] || !$post['userName'] || !$post['captcha']) {
                Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
            }
            
            $obj       = BooController::get('Obj_Admin_User');
            $adminInfo = $obj->getInfoByName($post['userName']);
            if (!$adminInfo) {
                Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
            }
            
            if ($adminInfo['admin_pwd'] != Common_rule::encodeAdminLoginPassword(
                    $post['password'],
                    $adminInfo['salt']
                )) {
                Common_errorCode::jsonEncode(Common_errorCode::PASSWORD_ERR);
            }
            
            if ($adminInfo['islocked']) {
                Common_errorCode::jsonEncode(Common_errorCode::USER_LOCKED);
            }
            
            if ($adminInfo['allow_login_ip']
                && !in_array(
                    BooUtil::realIp(),
                    explode(',', $adminInfo['allow_login_ip'])
                )) {
                Common_errorCode::jsonEncode(Common_errorCode::NO_POWER);
            }
            
            /**
             * 随机验证
             */
            //$adminLoginCaptcha = BooVar::cookie("adminLoginCaptcha");
            //if (strtoupper($post['captcha']) != strtoupper($adminLoginCaptcha)) {
            //    Common_errorCode::jsonEncode(Common_errorCode::VERIFY_CODE_ERR);
            //}
            
            /**
             * Google 二维码验证
             *
             * @var $system_info Obj_Admin_Info
             */
            $system_info = BooController::get('Obj_Admin_Info');
            $info        = $system_info->getInfo($adminInfo['admin_id']);
            $checkCodeRs = BooGoogleAuthor::getInstance()->verifyCode($info['google_secret_code'], $post['captcha']);
            if (!$checkCodeRs) {
                Common_errorCode::jsonEncode(Common_errorCode::GOOGLE_CODE_IS_ERROR);
            }
            
            $updateData = array(
                'last_update_time' => date('Y-m-d H:i:s'),
                'login_session_id' => BooSession::getSessionId(),
                'last_ip'          => BooUtil::realIp(),
            );
            $obj->update($adminInfo['admin_id'], $updateData);
            
            BooSession::set('adminId', $adminInfo['admin_id']);
            BooSession::set('adminName', $adminInfo['admin_name']);
            BooSession::set('adminNickname', $adminInfo['admin_nickname']);
            BooSession::set('adminIsLock', $adminInfo['islocked']);
            
            // 记录管理员行为
            BooController::get('Mod_Admin')->addLogs();
        }
        
        return true;
    }
}
