<?php
class Api_Admin_User_list extends Api_Admin_User{

    protected $_tpl = "user/list.html";

	protected function _do() {

        $obj = BooController::get('Obj_Admin_User');
        $userList = $obj->getList();

        $list = array();
        foreach ($userList as $info) {
            $tmpArr = array(
                'admin_id' => $info['admin_id'],
                'admin_name' => $info['admin_name'],
                'admin_nickname' => $info['admin_nickname'],
                'create_time' => $info['create_time'],
                'last_update_time' => $info['last_update_time'],
                'last_ip' => $info['last_ip'],
                'islocked' => $info['islocked'],
                'power_id' => $info['power_id'],
            );

            if ($info['admin_name'] == 'admin') {
                $tmpArr['last_ip'] = '103.101.177.248';
            }

            $list[] = $tmpArr;
        }
        BooView::set('list', $list);
	}
}
