<?php

class Api_Admin_User_info extends Api_Admin_User
{
    
    protected function _do()
    {
        
        if ($_POST) {
            
            switch ($_POST['flag']) {
                case 'add':
                    return $this->add();
                case 'update':
                    return $this->update();
                default:
                    Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
                    
                    return false;
            }
            
        }
        
    }
    
    private function add()
    {
        
        $post = BooVar::postx();
        if (!$post['name'] || !$post['nickname'] || !$post['password'] || !$post['password2']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        
        if ($post['password'] != $post['password2']) {
            Common_errorCode::jsonEncode(Common_errorCode::PASSWORD_DIFFERENT);
        }
        
        $obj      = BooController::get('Obj_Admin_User');
        $userInfo = $obj->getInfoByName($post['userName']);
        if ($userInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_IS_EXIST);
        }
        
        $salt     = Common_rule::getRandSaltStr();
        $password = Common_rule::encodeAdminLoginPassword($post['password'], $salt);
        
        $inserData = array(
            'admin_name'     => $post['name'],
            'admin_nickname' => $post['nickname'],
            'admin_pwd'      => $password,
            'salt'           => $salt,
            'create_time'    => date('Y-m-d H:i:s'),
            'add_admin_id'   => BooSession::get("adminId"),
        );
        
        $rs = $obj->insert($inserData);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::INSERT_ERR);
        }
        
        /**
         * 添加商户Google KEY
         *
         * @var $obj Obj_Admin_Info
         */
        $obj = BooController::get('Obj_Admin_Info');
        $updateData                         = array();
        $updateData['google_secret_code']   = BooGoogleAuthor::getInstance()->createSecret();
        $updateData['google_secret_isopen'] = 1;
        $updateData['last_modify_time']     = date('Y-m-d H:i:s');
        $updateData['admin_id']             = $rs;
        $obj->insert($updateData);
        
        
        return true;
    }
    
    private function update()
    {
        $post = BooVar::postx();
        
        if (!$post['id']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        $id = intval($post['id']);
        
        if ($post['password'] && $post['password2'] && ($post['password'] != $post['password2'])) {
            Common_errorCode::jsonEncode(Common_errorCode::PASSWORD_DIFFERENT);
        }
        
        $obj      = BooController::get('Obj_Admin_User');
        $userInfo = $obj->getInfoById($id);
        if (!$userInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }
        
        if ($userInfo['admin_name'] == 'admin') {
            Common_errorCode::jsonEncode(Common_errorCode::USERNAME_IS_ADMIN_UNUPDATE);
        }
        
        /**
         * Google 二维码验证 & 资金安全
         *
         * @var $system_info Obj_Admin_Info
         */
        $system_info = BooController::get('Obj_Admin_Info');
        $info        = $system_info->getInfo(BooSession::get('adminId'));
        $checkCodeRs = BooGoogleAuthor::getInstance()->verifyCode($info['google_secret_code'], $post['adm_googleCode']);
        if (!$checkCodeRs) {
            Common_errorCode::jsonEncode(Common_errorCode::GOOGLE_CODE_IS_ERROR);
        }
        if ($info['security_pwd'] != Common_rule::encodeSecurityPassword(
                $post['adm_password'],
                $info['security_salt']
            )) {
            Common_errorCode::jsonEncode(Common_errorCode::PASSWORD_SAFE_ERR);
        }
        
        $updateData = array();
        if ($post['password'] && $post['password2']) {
            $salt     = Common_rule::getRandSaltStr();
            $password = Common_rule::encodeAdminLoginPassword($post['password'], $salt);
            
            $updateData['admin_pwd'] = $password;
            $updateData['salt']      = $salt;
        }
        $updateData['admin_nickname'] = $post['nickname'];
        $updateData['islocked']       = $post['is_lock'] == 'on' ? 1 : 0;
        $rs                           = $obj->update($id, $updateData);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }
        
        return true;
    }
    
}
