<?php
class Api_Admin_User_logout extends Api_Admin_User{

	protected function _do(){

        BooSession::destroy();
        header('Location: /admin/user/login');
	    return true;
	}
}