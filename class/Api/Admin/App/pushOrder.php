<?php
class Api_Admin_App_pushOrder extends Api_Admin_App {

	protected function _do() {

        $post = BooVar::requestx();
        if (!$post['appId'] || !$post['id']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }
        $appId = intval($post['appId']);
        $payId = intval($post['id']);

        $appPayObj = BooController::get('Obj_App_Pay');
        $payInfo = $appPayObj->getInfoById($appId, $payId);
        if (!$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::ORDER_NOT_EXIST);
        }
        if ($payInfo['result'] != 1) {
            Common_errorCode::jsonEncode(Common_errorCode::ORDER_NOT_EXIST);
        }
        if ($payInfo['notify_status'] == 1) {
            Common_errorCode::jsonEncode(Common_errorCode::PAY_IS_END);
        }

        $platInfo = BooController::get('Mod_ThirdPlatform')->getPlatInfo($appId, $payInfo['pt_id']);

        $data = array();
        $data['platOrderId'] = 'P' . date('Ymd', strtotime($payInfo['create_time'])) . $payInfo['id'];
        $data['orderId'] =  $payInfo['order_id'];
        $data['amount'] =  $payInfo['amount'];
        $data['realAmount'] =  $payInfo['real_amount'];
        $data['remark'] =  $payInfo['remark'];
        $data['time'] =  date('Y-m-d H:i:s');
        $data['sign'] = $this->getSign($data, $platInfo['appInfo']['app_key']);

        if ($payInfo['real_amount'] - $payInfo['amount'] > 1000) {
            Common_errorCode::jsonEncode(Common_errorCode::FAIL, array(), "，原因：真实支付金额比用户提交金额大1000，数据异常");
        }

        BooCurl::setData($data, 'POST');
        $result = BooCurl::call($payInfo['notify_url']);
        if (trim($result) != 'success') {

            ksort($data);
            $checkString = '';
            foreach ($data as $key =>$value) {
                if (!$checkString) {
                    $checkString = "{$key}={$value}";
                } else {
                    $checkString .= "&{$key}={$value}";
                }
            }

            $updateData = array();
            $updateData['notify_res_data'] = $result;
            $appPayObj->update($appId, $payInfo['id'], $updateData);
            Common_errorCode::jsonEncode(Common_errorCode::FAIL, array(), "，原因：已通知到商户，商户未返回 success。通知url：{$payInfo['notify_url']}，通知参数：{$checkString}，商户返回的是：" . $result);
        }

        $updateData = array();
        $updateData['notify_data'] = json_encode($data);
        $updateData['notify_times'] = $payInfo['notify_times'] + 1;
        $updateData['notify_status'] = 1;
        $updateData['last_notify_time'] = date('Y-m-d H:i:s');
        $updateData['notify_res_data'] = $result;
        $appPayObj->update($appId, $payInfo['id'], $updateData);

        return true;
	}

    private function getSign($data, $appKey) {

        ksort($data);

        $checkString = '';
        foreach ($data as $key =>$value) {
            if (!$checkString) {
                $checkString = "{$key}={$value}";
            } else {
                $checkString .= "&{$key}={$value}";
            }
        }

        $checkString .= "{$appKey}";
        $sign = md5($checkString);

        return $sign;
    }

}
