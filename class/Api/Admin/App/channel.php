<?php

class Api_Admin_App_Channel extends Api_Admin_App
{
    
    protected $_tpl = "app/channelAssign.html";
    
    protected function _do()
    {
        if ($_REQUEST
            && in_array(
                $_REQUEST['flag'],
                array('tbody', 'theader', 'detail', 'update')
            )) {
            
            switch ($_REQUEST['flag']) {
                case 'tbody':
                    return $this->tbody();
                    break;
                case 'update':
                    return $this->update();
                    break;
                case 'detail':
                    return $this->detail();
                    break;
                case 'theader':
                    return $this->theader();
                    break;
                default:
                    Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
                    
                    return false;
            }
            
        }
        else {
            
            //商户列表
            $appObj     = BooController::get('Obj_App_Info');
            $tmpAppList = $appObj->getList();
            $appList    = array();
            foreach ($tmpAppList as $info) {
                if ($info['is_lock']) {
                    continue;
                }
                $appList[$info['app_id']] = $info['name'];
            }
            
            //支付类型列表
            $payTypeObj     = BooController::get('Obj_Plat_PayType');
            $tmpPayTypeList = $payTypeObj->getList();
            $payTypeList    = array();
            foreach ($tmpPayTypeList as $info) {
                $payTypeList[$info['pt_id']] = $info['pt_name'];
            }
            //支付渠道列表
            $platObj     = BooController::get('Obj_Plat_Info');
            $tmpPlatList = $platObj->getList();
            $platList    = array();
            foreach ($tmpPlatList as $info) {
                $platList[$info['p_id']]['name']  = $info['p_name'];
                $platList[$info['p_id']]['point'] = $info['p_point'];
            }
            BooView::set('app_id', $_REQUEST['id']);
            BooView::set('appList', $appList);
            BooView::set('payTypeList', $payTypeList);
            BooView::set('platList', $platList);
            
        }
        
    }
    
    public function theader()
    {
        
        return [
            [
                'field' => 'app_id',
                'title' => '商户名称',
                'align' => 'center',
            ],
            [
                'field' => 'pt_id',
                'title' => '支付类型',
                'align' => 'center',
            ],
            [
                'field' => 'p_id',
                'title' => '支付渠道',
                'align' => 'center',
            ],
            [
                'field' => 'p_point',
                'title' => '渠道费率',
                'align' => 'center',
            ],
            [
                'field' => 'fee',
                'title' => '商户费率',
                'align' => 'center',
            ],
            [
                'field' => 'status',
                'title' => '启用状态',
                'align' => 'center',
            ],
            [
                'field' => 'updated_time',
                'title' => '更新时间',
                'align' => 'center',
            ],
            [
                'field' => 'created_time',
                'title' => '创建时间',
                'align' => 'center',
            ],
            [
                'field' => 'operator',
                'title' => '操作人',
                'align' => 'center',
            ],
            [
                'field' => 'operation',
                'title' => '操作',
                'align' => 'center',
            ],
        ];
    }
    
    public function tbody()
    {
        
        $post           = BooVar::requestx();
        
        //var_dump($post);die;
        $merchant_id    = $post['appId'] ? intval($post['appId']) : '';
        $pay_type_id    = $post['payTypeId'];
        $channel_id     = intval($post['platId']);
        $channel_status = $post['status'];
        $start_time     = $post['startTime'];
        $end_time       = $post['endTime'];
        
        $page_size      = $post['pageSize'] ? : 10;
        $page_number    = $post['pageNumber'] ? : 1;
        $_GET['pageId'] = $page_number;
        $limit_start    = $page_number == 1 ? 0 : $page_size * ($page_number - 1);
        
        /**
         * @var $obj Obj_App_ChannelAssign
         */
        $obj      = BooController::get('Obj_App_ChannelAssign');
        $pageList = $obj->getPageList(
            $merchant_id,
            $pay_type_id,
            $channel_id,
            $start_time,
            $end_time,
            $limit_start,
            $page_size,
            $channel_status
        );
        
        if (!empty($pageList['data'])) {
            
            //商户列表
            $appObj     = BooController::get('Obj_App_Info');
            $tmpAppList = $appObj->getList();
            $appList    = array();
            foreach ($tmpAppList as $info) {
                if ($info['is_lock']) {
                    continue;
                }
                $appList[$info['app_id']] = $info['name'];
            }
            
            //支付类型列表
            $payTypeObj     = BooController::get('Obj_Plat_PayType');
            $tmpPayTypeList = $payTypeObj->getList();
            $payTypeList    = array();
            foreach ($tmpPayTypeList as $info) {
                $payTypeList[$info['pt_id']] = $info['pt_name'];
            }
            //支付渠道列表
            $platObj     = BooController::get('Obj_Plat_Info');
            $tmpPlatList = $platObj->getList();
            $platList    = array();
            foreach ($tmpPlatList as $info) {
                $platList[$info['p_id']]['name']  = $info['p_name'];
                $platList[$info['p_id']]['point'] = $info['p_point'];
            }
            
            foreach ($pageList['data'] as $key => $item) {
                
                if (!$item['pt_id']) {
                    $item['pt_id'] = '-';
                }
                if (!$item['p_id']) {
                    $item['p_id'] = '-';
                }
                $item['operation'] = '<a class="green" href="javascript:void(0);" onclick="showInfoModal('
                                     . $item['id']
                                     . ');"><i class="ace-icon fa fa-pencil bigger-130"></i> 编辑</a>';
                foreach ($item as $k => $v) {
                    if ($k == 'app_id' && !empty($v)) {
                        $item[$k] = $appList[$v];
                    }
                    if ($k == 'pt_id' & $v > 0) {
                        $item[$k] = $payTypeList[$v];
                    }
                    if ($k == 'p_id' & $v > 0) {
                        $item[$k]        = $platList[$v]['name'];
                        $item['p_point'] = $platList[$v]['point'];
                    }
                    
                    if (in_array($k, ['updated_time', 'created_time'])) {
                        $item[$k] = date('Y-m-d H:i:s', $v);
                    }
                    
                    if (in_array($k, ['status'])) {
                        if ($v == 'N') {
                            $item[$k] = '<span style="color:red">尚未启用</span>';
                        }
                        else {
                            $item[$k] = '<span style="color:green">已经启用</span>';;
                        }
                    }
                }
                $pageList['data'][$key] = $item;
            }
            
        }
        
        $total_pages = ceil($pageList['totalRows'] / $page_size);
        $url         = '/admin/app/channel?flag=tbody&';
        $data        = [
            'current_page'   => $page_number,
            'first_page_url' => "{$url}page=1",
            'from'           => $page_number,
            'last_page'      => $total_pages,
            'last_page_url'  => "{$url}page=" . $total_pages,
            'next_page_url'  => "{$url}page=" . ($pageList['currentPage'] + 1),
            'path'           => "{$url}",
            'per_page'       => $page_size,
            'prev_page_url'  => null,
            'to'             => $page_size,
            'total'          => $pageList['totalRows'],
        ];
        
        $data['data'] = $pageList['data'];
        
        die(json_encode($data));
    }
    
    public function detail($id = '')
    {
        
        $post = BooVar::requestx();
        if (!$id) {
            $id = $post['id'] ? intval($post['id']) : '';
        }
        
        /**
         * @var $obj Obj_App_ChannelAssign
         */
        $obj = BooController::get('Obj_App_ChannelAssign');
        if (!empty($id)) {
            
            $channel = $obj->getInfoById($id);
            if (empty($channel)) {
                die();
            }
            
            /**
             * 商户列表
             *
             * @var $appObj Obj_App_Info
             */
            $appObj            = BooController::get('Obj_App_Info');
            $tmpApp            = $appObj->getInfoByAppId($channel['app_id']);
            $channel['app_id'] = $tmpApp['name'];
            
            /**
             * 支付类型列表
             *
             * @var $payTypeObj Obj_Plat_PayType
             */
            $payTypeObj       = BooController::get('Obj_Plat_PayType');
            $tmpPayType       = $payTypeObj->getInfoById($channel['pt_id']);
            $channel['pt_id'] = $tmpPayType['pt_name'];
            
            /**
             * 支付渠道列表
             *
             * @var $platObj Obj_Plat_Info
             */
            $platObj            = BooController::get('Obj_Plat_Info');
            $tmpPlat            = $platObj->getInfoById($channel['p_id']);
            $channel['p_id']    = $tmpPlat['p_name'];
            $channel['p_point'] = $tmpPlat['p_point'];
            
            die(json_encode($channel));
        }
        else {
            die('');
        }
        
    }
    
    public function update()
    {
        
        $post   = BooVar::requestx();
        $id     = $post['id'] ? intval($post['id']) : '';
        $status = $post['status'] == 'Y' ? 'Y' : 'N';
        if (empty($id)) {
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }
        
        /**
         * 更新渠道信息
         *
         * @var $obj Obj_App_ChannelAssign
         */
        $obj = BooController::get('Obj_App_ChannelAssign');
        $rs  = $obj->update(
            $id,
            [
                'fee'          => $post['fee'],
                'status'       => $status,
                'updated_time' => time(),
                'operator'     => BooSession::get('adminName'),
            ]
        );
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }
        
        return true;
        
    }
}
