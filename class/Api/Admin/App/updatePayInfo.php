<?php
class Api_Admin_App_updatePayInfo extends Api_Admin_App {

	protected function _do() {

        $post = BooVar::requestx();
        if (!$post['appId'] || !$post['id'] || $post['realAmount'] <= 0 || $post['result'] != 1) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_LOST);
        }
        $appId = intval($post['appId']);
        $payId = intval($post['id']);
        $realMoney = $post['realAmount'];

        $appPayObj = BooController::get('Obj_App_Pay');
        $payInfo = $appPayObj->getInfoById($appId, $payId);
        if (!$payInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::ORDER_NOT_EXIST);
        }
        if ($payInfo['result'] == 1) {
            Common_errorCode::jsonEncode(Common_errorCode::PAY_IS_END);
        }

        $appRealMoney = $realMoney * (1 - $payInfo['current_fee_rate']);
        $platInfo = BooController::get('Mod_ThirdPlatform')->getPlatInfo($appId, $payInfo['pt_id']);

        $updateData = array();
        $updateData['real_amount'] = $realMoney;
        $updateData['result'] = 1;

        $data = array();
        $data['platOrderId'] = 'P' . date('Ymd', strtotime($payInfo['create_time'])) . $payInfo['id'];
        $data['orderId'] =  $payInfo['order_id'];
        $data['amount'] =  $payInfo['amount'];
        $data['realAmount'] =  $realMoney;
        $data['remark'] =  $payInfo['remark'];
        $data['time'] =  date('Y-m-d H:i:s');
        $data['sign'] = $this->getSign($data, $platInfo['appInfo']['app_key']);

        $updateData['notify_data'] = json_encode($data);

        $result = '';
        for ($i = 1; $i <= 3; $i++) {
            $updateData['notify_times'] += 1;

            BooCurl::setData($data, 'POST');
            $result = BooCurl::call($payInfo['notify_url']);
            if ($result == 'success') {
                $updateData['notify_status'] = 1;
                break;
            }
        }

        $updateData['notify_res_data'] = $result;
        $updateData['last_notify_time'] = date('Y-m-d H:i:s');
        $updateData['current_fee'] = $appRealMoney;
        $updateData['end_time'] = date('Y-m-d H:i:s');

        $appPayObj->update($appId, $payInfo['id'], $updateData);

        $this->sendMoney($payInfo, $realMoney);
        return true;
	}

    private function getSign($data, $appKey) {

        ksort($data);

        $checkString = '';
        foreach ($data as $key =>$value) {
            if (!$checkString) {
                $checkString = "{$key}={$value}";
            } else {
                $checkString .= "&{$key}={$value}";
            }
        }

        $checkString .= "{$appKey}";
        $sign = md5($checkString);

        return $sign;
    }

    private function sendMoney($payInfo, $realMoney) {

        if (!$payInfo['proxy_id']) {

            $lastLevelPoint = $payInfo['current_point'];
            $point = 0;
            $obj = BooController::get('Obj_Plat_Info');
            $list = $obj->getList();
            foreach ($list as $key => $tmpInfo) {
                if ($tmpInfo['p_id'] == $payInfo['p_id']) {
                    $point = $tmpInfo['p_point'];
                    break;
                }
            }

            if (!$point) {
                //return true;  //找不到费率
            }
            
            $dxpoint = $lastLevelPoint - $point;

            if ($dxpoint < 0) {
                $dxpoint = 0;
            }
            
            $systemPayObj = BooController::get('Obj_Admin_Pay');
            
            $reCnt = $systemPayObj->getCountByAppPayId($payInfo['app_id'],$payInfo['id']);
            if($reCnt>0){
                return true;
            }

            $systemMoney = $realMoney * $dxpoint;
            $insertData = array();
            $insertData['app_id'] = $payInfo['app_id'];
            $insertData['app_name'] = $payInfo['app_name'];
            $insertData['p_id'] = $payInfo['p_id'];
            $insertData['order_id'] = $payInfo['order_id'];
            $insertData['pay_id'] = $payInfo['id'];
            $insertData['amount'] = $payInfo['amount'];
            $insertData['real_amount'] = $realMoney;
            $insertData['pt_id'] = $payInfo['pt_id'];
            $insertData['create_time'] = $payInfo['create_time'];
            $insertData['end_time'] = date('Y-m-d H:i:s');
            $insertData['create_date'] = $payInfo['create_date'];
            $insertData['current_point'] = $point;
            $insertData['current_fee_rate'] = $dxpoint;
            $insertData['current_fee'] = $systemMoney;

            $systemPayObj->insert($insertData);

            return true;
        }

        $obj = BooController::get('Obj_Proxy_Info');
        $info = $obj->getInfoById($payInfo['proxy_id']);
        $platAppInfo = json_decode($info['plat_app_info'], true);
        $pointInfo = $platAppInfo[$payInfo['p_id']];
        foreach ($platAppInfo as $tmpInfo) {
            if ($tmpInfo['payType'] == $payInfo['pt_id']) {
                $pointInfo = $tmpInfo;
                break;
            }
        }

        //代理通道查找不到，这里当作不返点处理
        if($pointInfo['point']>0){
            $dxpoint = $payInfo['current_point'] - $pointInfo['point'];
            if ($dxpoint < 0) {
                $dxpoint = 0;
            }
            $proxyMoney = $realMoney * $dxpoint;

            $nowDateTime = date('Y-m-d H:i:s');
            $insertData = array();
            $insertData['app_id'] = $payInfo['app_id'];
            $insertData['app_name'] = $payInfo['app_name'];
            $insertData['proxy_id'] = $info['proxy_id'];
            $insertData['proxy_name'] = $info['proxy_name'];
            $insertData['p_id'] = $payInfo['p_id'];
            $insertData['order_id'] = $payInfo['order_id'];
            $insertData['amount'] = $payInfo['amount'];
            $insertData['real_amount'] = $realMoney;
            $insertData['pt_id'] = $payInfo['pt_id'];
            $insertData['pay_id'] = $payInfo['id'];
            $insertData['create_time'] = $payInfo['create_time'];
            $insertData['end_time'] = $nowDateTime;
            $insertData['create_date'] = $payInfo['create_date'];
            $insertData['current_point'] = $pointInfo['point'];
            $insertData['current_fee_rate'] = $dxpoint;
            $insertData['current_fee'] = $proxyMoney;

            $proxyPayObj = BooController::get('Obj_Proxy_Pay');
            $reCnt = $proxyPayObj->getCountByAppPayId($info['proxy_id'],$payInfo['app_id'],$payInfo['id']);
            if($reCnt==0){
                $proxyPayObj->insert($payInfo['proxy_id'], $insertData);
            }
            
            $lastLevelPoint = min($payInfo['current_point'],$pointInfo['point']);//$dxpoint<0异常,应取最小值
            
            if ($info['user_tree']) {
                $treeInfo = explode(',', $info['user_tree']);
                krsort($treeInfo);

                foreach ($treeInfo as $tmpProxyId) {
                    $info = $obj->getInfoById($tmpProxyId);
                    $platAppInfo = json_decode($info['plat_app_info'], true);
                    $pointInfo = $platAppInfo[$payInfo['p_id']];
                    foreach ($platAppInfo as $tmpInfo) {
                        if ($tmpInfo['payType'] == $payInfo['pt_id']) {
                            $pointInfo = $tmpInfo;
                            break;
                        }
                    }
                    
                    $dxpoint = $lastLevelPoint - $pointInfo['point'];
                    if ($dxpoint < 0) {
                        $dxpoint = 0;
                    }
                    $proxyMoney = $realMoney * $dxpoint;
                    
                    $lastLevelPoint = min($lastLevelPoint,$pointInfo['point']);

                    $insertData = array();
                    $insertData['app_id'] = $payInfo['app_id'];
                    $insertData['app_name'] = $payInfo['app_name'];
                    $insertData['proxy_id'] = $info['proxy_id'];
                    $insertData['proxy_name'] = $info['proxy_name'];
                    $insertData['p_id'] = $payInfo['p_id'];
                    $insertData['order_id'] = $payInfo['order_id'];
                    $insertData['amount'] = $payInfo['amount'];
                    $insertData['real_amount'] = $realMoney;
                    $insertData['pt_id'] = $payInfo['pt_id'];
                    $insertData['pay_id'] = $payInfo['id'];
                    $insertData['create_time'] = $payInfo['create_time'];
                    $insertData['end_time'] = $nowDateTime;
                    $insertData['create_date'] = $payInfo['create_date'];
                    $insertData['current_point'] = $pointInfo['point'];
                    $insertData['current_fee_rate'] = $dxpoint;
                    $insertData['current_fee'] = $proxyMoney;

                    $reCnt = $proxyPayObj->getCountByAppPayId($tmpProxyId,$payInfo['app_id'],$payInfo['id']);
                    if($reCnt==0){
                        $proxyPayObj->insert($tmpProxyId, $insertData);
                    }
                }

            }
        }

        $point = 0;
        $obj = BooController::get('Obj_Plat_Info');
        $list = $obj->getList();
        foreach ($list as $key => $tmpInfo) {
            if ($tmpInfo['p_id'] == $payInfo['p_id']) {
                $point = $tmpInfo['p_point'];
                break;
            }
        }

        if (!$point) {
            //return true;
        }

        $dxpoint = $lastLevelPoint - $point;

        if ($dxpoint < 0) {
            $dxpoint = 0;
        }
        
        $systemPayObj = BooController::get('Obj_Admin_Pay');
        $reCnt = $systemPayObj->getCountByAppPayId($payInfo['app_id'],$payInfo['id']);
        if($reCnt>0){
            return true;
        }
            
        $systemMoney = $realMoney * $dxpoint;
        $insertData = array();
        $insertData['app_id'] = $payInfo['app_id'];
        $insertData['app_name'] = $payInfo['app_name'];
        $insertData['proxy_id'] = $payInfo['proxy_id'];
        $insertData['proxy_name'] = $payInfo['proxy_name'];
        $insertData['p_id'] = $payInfo['p_id'];
        $insertData['order_id'] = $payInfo['order_id'];
        $insertData['pay_id'] = $payInfo['id'];
        $insertData['amount'] = $payInfo['amount'];
        $insertData['real_amount'] = $realMoney;
        $insertData['pt_id'] = $payInfo['pt_id'];
        $insertData['create_time'] = $payInfo['create_time'];
        $insertData['end_time'] = $nowDateTime;
        $insertData['create_date'] = $payInfo['create_date'];
        $insertData['current_point'] = $point;
        $insertData['current_fee_rate'] = $dxpoint;
        $insertData['current_fee'] = $systemMoney;

        $systemPayObj->insert($insertData);

        return true;
    }

}
