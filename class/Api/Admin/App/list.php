<?php

class Api_Admin_App_list extends Api_Admin_App
{
    
    protected $_tpl = "app/list.html";
    
    protected function _do()
    {
        
        if ($_POST
            && in_array(
                $_POST['flag'],
                array(
                    'add',
                    'update',
                    'updateLock',
                    'del',
                    'changeKey',
                    'resetLoginPassword',
                    'resetSecurityPassword',
                    'getAppList',
                    'onekeySetting',
                )
            )) {
            
            switch ($_POST['flag']) {
                case 'add':
                    return $this->add();
                case 'update':
                    return $this->update();
                case 'updateLock':
                    return $this->updateLock();
                case 'del':
                    return $this->del();
                case 'changeKey':
                    return $this->changeKey();
                case 'resetLoginPassword':
                    return $this->resetLoginPassword();
                case 'resetSecurityPassword':
                    return $this->resetSecurityPassword();
                case 'getAppList':
                    return $this->getAppList();
                case 'onekeySetting':
                    return $this->onekeySetting();
                default:
                    Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
                    
                    return false;
            }
            
        }
        else {
            
            $obj            = BooController::get('Obj_Plat_PayType');
            $tmpPayTypeList = $obj->getList();
            
            $payTypeList = array();
            foreach ($tmpPayTypeList as $info) {
                $payTypeList[$info['pt_id']] = $info['pt_name'];
            }
            
            $obj         = BooController::get('Obj_Plat_Info');
            $tmpPlatList = $obj->getList();
            
            $platList = array();
            foreach ($tmpPlatList as $info) {
                $platList[$info['p_id']] = array(
                    'pt_id'  => $info['pt_id'],
                    'p_id'   => $info['p_id'],
                    'p_name' => $info['p_name'],
                );
            }
            
            $post           = BooVar::postx();
            $name           = $post['name'];
            $_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数
            $obj            = BooController::get('Obj_App_Info');
            $pageList       = $obj->getPageList(0, $name);
            
            foreach ($pageList['data'] as $key => $info) {
                
                $tmpArr                   = array();
                $tmpArr['id']             = $info['id'];
                $tmpArr['name']           = $info['name'];
                $tmpArr['app_id']         = $info['app_id'];
                $tmpArr['app_key']        = $info['app_key'];
                $tmpArr['proxyName']      = $info['proxy_name'];
                $tmpArr['adminName']      = $info['add_admin'];
                $tmpArr['allow_login_ip'] = $info['allow_login_ip'];
                $tmpArr['last_ip']        = $info['last_ip'];
                $tmpArr['create_time']    = $info['create_time'];
                $tmpArr['last_time']      = $info['last_update_time'];
                $tmpArr['is_lock']        = $info['is_lock'];
                $tmpArr['withdrawal_fee'] = $info['withdrawal_fee'];
                
                $platNameList = '';
                if ($info['plat_app_info']) {
                    $info['plat_app_info'] = json_decode($info['plat_app_info'], true);
                    
                    foreach ($info['plat_app_info'] as $platId => $platInfo) {
                        if ($platNameList) {
                            $platNameList .= '<br/>'
                                             . $payTypeList[$platInfo['payType']]
                                             . '：'
                                             . $platList[$platId]['p_name']
                                             . "({$platInfo['point']})";
                        }
                        else {
                            $platNameList = $payTypeList[$platInfo['payType']]
                                            . '：'
                                            . $platList[$platId]['p_name']
                                            . "({$platInfo['point']})";
                        }
                    }
                    $tmpArr['openPlatList'] = $platNameList;
                    
                }
                $tmpArr['plat_app_info'] = $info['plat_app_info'];
                $pageList['data'][$key]  = $tmpArr;
            }
            
            if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
                return $pageList;
            }
            else {
                $adminObj  = BooController::get('Obj_Admin_Info');
                $adminInfo = $adminObj->getInfo(BooSession::get('adminId'));
                
                BooView::set('withdrawal_fee', $adminInfo['withdrawal_fee']);
                BooView::set('platList', $platList);
                BooView::set('pageList', $pageList);
            }
        }
    }
    
    private function getAppList()
    {
        
        $obj        = BooController::get('Obj_App_Info');
        $tmpAppList = $obj->getList();
        $appList    = array();
        foreach ($tmpAppList as $info) {
            if ($info['is_lock']) {
                continue;
            }
            $appList[$info['app_id']] = $info['name'];
        }
        
        return array('list' => $appList);
    }
    
    private function updateLock()
    {
        
        $post = BooVar::postx();
        
        if (!$post['id'] && !isset($post['isLock'])) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        $id  = intval($post['id']);
        $obj = BooController::get('Obj_App_Info');
        $rs  = $obj->update(
            $id,
            [
                'is_lock' => $post['isLock'] == 1 ? 1 : 0,
            ]
        );
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }
        
        return true;
        
    }
    
    private function update()
    {
        $post = BooVar::postx();
        
        if (!$post['id']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        $id = intval($post['id']);
        
        $obj  = BooController::get('Obj_App_Info');
        $info = $obj->getInfoById($id);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }
        
        $updateData                   = array();
        $updateData['app_key']        = $post['app_key'];
        $updateData['allow_login_ip'] = $post['allowLoginIp'];
        $updateData['withdrawal_fee'] = $post['withdrawalFee'] ? $post['withdrawalFee'] : 10;
        if ($post['platId']) {
            
            $platObj      = BooController::get('Obj_Plat_Info');
            $platTypeList = array();
            $platAppInfo  = array();
            foreach ($post['platId'] as $key => $platId) {
                
                $platInfo = $platObj->getInfoById($platId);
                if (!$platInfo) {
                    continue;
                }
                
                if ($post['appPoint'][$key] < 0) {
                    Common_errorCode::jsonEncode(Common_errorCode::POINT_IS_ZERO);
                }
                //点数分配不合理提示
                if ($post['appPoint'][$key] >= 1) {
                    Common_errorCode::jsonEncode(Common_errorCode::POINT_IS_WRONG);
                }
                if ($post['appPoint'][$key] < $platInfo['p_point']) {
                    Common_errorCode::jsonEncode(Common_errorCode::POINT_IS_LOW);
                }
                
                $platAppInfo[$platId] = array(
                    'platTag' => $platInfo['p_tag'],
                    'payType' => $platInfo['pt_id'],
                    'point'   => $post['appPoint'][$key],
                );
                
                /**
                 * 只允许一个支付类型分配一个渠道
                 */
                $platTypeList[$platInfo['pt_id']] += 1;
                if ($platTypeList[$platInfo['pt_id']] > 1) {
                    Common_errorCode::jsonEncode(Common_errorCode::ONLY_ONE_PAY_TYPE);
                }
            }

            // 判定代理的通道费率得小于商户分配的通道费率
            $proxy_id = $info['proxy_id'];
            if($proxy_id != 0) {
                $agentObj = BooController::get('Obj_Proxy_Info');
                $agentInfo = $agentObj->getInfoById($proxy_id);
                $agentPlatInfo = json_decode($agentInfo['plat_app_info'], 1);

                // 判定代理的通道费率是否小于商户的通道费率
                foreach ($post['platId'] as $key2 => $value2) {
                    if($agentPlatInfo[$value2]['point'] > $post['appPoint'][$key2]) {
                        Common_errorCode::jsonEncode(Common_errorCode::AGENT_POINT_IS_LOW);
                    }
                }
            }

            $updateData['plat_app_info'] = $platAppInfo ? json_encode($platAppInfo) : '';
        }
        else {
            $updateData['plat_app_info'] = '';
        }
        
        /**
         * @var $channel Obj_App_ChannelAssign
         */
        $channel = BooController::get('Obj_App_ChannelAssign');
        $channel->updatePlatAppInfo($updateData['plat_app_info'], $info['app_id']);
        if (is_array($updateData['plat_app_info'])) {
            $updateData['plat_app_info'] = json_encode($updateData['plat_app_info']);
        }
        
        $rs = $obj->update($id, $updateData);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }
        
        return true;
    }

    
    private function add()
    {
        
        $post = BooVar::postx();
        if (!$post['name'] || !$post['password']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        
        $obj      = BooController::get('Obj_App_Info');
        $userInfo = $obj->getInfoByName($post['name']);
        if ($userInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_IS_EXIST);
        }
        
        $salt     = Common_rule::getRandSaltStr();
        $password = Common_rule::encodeLoginPassword($post['password'], $salt);
        
        $appId = 0;
        while (1) {
            $appId = rand(1000, 9999);
            $rs    = $obj->getInfoByAppId($appId);
            if (!$rs) {
                break;
            }
        }
        
        $platAppInfo = array();
        if ($post['platId']) {
            
            $platObj = BooController::get('Obj_Plat_Info');
            
            $platTypeList = array();
            foreach ($post['platId'] as $key => $value) {
                
                $platInfo = $platObj->getInfoById($value);
                if (!$platInfo) {
                    continue;
                }
                
                if ($post['appPoint'][$key] <= 0) {
                    Common_errorCode::jsonEncode(Common_errorCode::POINT_IS_ZERO);
                }
                //点数分配不合理提示
                if ($post['appPoint'][$key] >= 1) {
                    Common_errorCode::jsonEncode(Common_errorCode::POINT_IS_WRONG);
                }
                if ($post['appPoint'][$key] < $platInfo['p_point']) {
                    Common_errorCode::jsonEncode(Common_errorCode::POINT_IS_LOW);
                }
                
                $platAppInfo[$value] = array(
                    'platTag' => $platInfo['p_tag'],
                    'payType' => $platInfo['pt_id'],
                    'point'   => $post['appPoint'][$key],
                );
                
                $platTypeList[$platInfo['pt_id']] += 1;
                
                if ($platTypeList[$platInfo['pt_id']] > 1) {
                    Common_errorCode::jsonEncode(Common_errorCode::ONLY_ONE_PAY_TYPE);
                }
            }
        }
        $app_json = $platAppInfo;
        
        /**
         * @var $channel Obj_App_ChannelAssign
         */
        $channel = BooController::get('Obj_App_ChannelAssign');
        $channel->updatePlatAppInfo($app_json, $appId);
        
        $inserData = array(
            'name'           => $post['name'],
            'password'       => $password,
            'salt'           => $salt,
            'app_id'         => $appId,
            'app_key'        => md5($appId . time() . rand(1, 99)),
            'plat_app_info'  => json_encode($app_json),
            'create_time'    => date('Y-m-d H:i:s'),
            'add_admin'      => BooSession::get('adminName'),
            'withdrawal_fee' => $post['withdrawalFee'] ? $post['withdrawalFee'] : 10,
        );
        
        $rs = $obj->insert($inserData);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::INSERT_ERR);
        }
        
        $balanceObj = BooController::get('Obj_App_Balance');
        $inserData  = array(
            'app_id'   => $appId,
            'app_name' => $post['name'],
        );
        $balanceObj->insert($inserData);
        
        return true;
    }
    
    private function onekeySetting()
    {
        $post = BooVar::postx();
        
        if (!$post['appIds']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        $appIds = $post['appIds'];
        if (!is_array($appIds)) {
            $appIds = [
                0 => $appIds,
            ];
        }
        $objApp = BooController::get('Obj_App_Info');
        //$post['platId']
        $arrAppAllPoint = [];
        foreach ($appIds as $val) {
            $getPlatAppInfo       = $objApp->getPlatAppInfoByAppId($val);
            $arrAppAllPoint[$val] = json_decode($getPlatAppInfo['plat_app_info'], true);
        }
        if ($post['platId']) {
            
            $platObj = BooController::get('Obj_Plat_Info');
            
            $platTypeList = array();
            //要修改的通道信息 $post['platId']
            foreach ($post['platId'] as $key => $platId) {
                
                //$platInfo 是要修改的通道的名字，类型
                $platInfo = $platObj->getInfoById($platId);
                
                if (!$platInfo) {
                    continue;
                }
                //这里 需要三个循环嵌套出要替换的商户对应的通道的信息（点位，名字，类型），点位不变
                foreach ($arrAppAllPoint as $app_id => $plat_app_info) {
                    foreach ($plat_app_info as $pt_id => $val) {
                        if ($platInfo['pt_id'] == $val['payType']) {
                            $plat_app_info[$platId] = [
                                'platTag' => $platInfo['p_tag'],
                                'payType' => $platInfo['pt_id'],
                                'point'   => $val["point"],
                            ];
                            unset($plat_app_info[$pt_id]);
                        }
                    }
                    $arrAppAllPoint[$app_id] = $plat_app_info;
                }
                //检查是否含有同类型支付需要做验证
                $platTypeList[$platInfo['pt_id']] += 1;
                if ($platTypeList[$platInfo['pt_id']] > 1) {
                    Common_errorCode::jsonEncode(Common_errorCode::ONLY_ONE_PAY_TYPE);
                }
            }
            /**
             * @var $channel Obj_App_ChannelAssign
             */
            $channel = BooController::get('Obj_App_ChannelAssign');
            $obj     = BooController::get('Obj_App_Info');
            foreach ($arrAppAllPoint as $app_id => $temAppPoint) {
                if (empty($temAppPoint)) {
                    continue;
                }
                $channel->updatePlatAppInfo($temAppPoint, $app_id, false);
                $updateData = [
                    'plat_app_info' => json_encode($temAppPoint),
                ];
                $rs         = $obj->updateByAppId($app_id, $updateData);
                if (!$rs) {
                    Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
                }
            }
            
        }
        else {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        
        return true;
    }
    
    private function resetLoginPassword()
    {
        
        $post = BooVar::postx();
        
        if (!$post['id']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        $id = intval($post['id']);
        
        $obj  = BooController::get('Obj_App_Info');
        $info = $obj->getInfoById($id);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }
        
        $salt     = Common_rule::getRandSaltStr();
        $password = Common_rule::encodeLoginPassword('123qwe', $salt);
        
        $updateData             = array();
        $updateData['password'] = $password;
        $updateData['salt']     = $salt;
        $rs                     = $obj->update($id, $updateData);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }
        
        return array('password' => '123qwe');
    }
    
    private function resetSecurityPassword()
    {
        
        $post = BooVar::postx();
        
        if (!$post['id']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        $id = intval($post['id']);
        
        $obj  = BooController::get('Obj_App_Info');
        $info = $obj->getInfoById($id);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }
        
        $updateData                         = array();
        $updateData['security_pwd']         = '';
        $updateData['security_salt']        = '';
        $updateData['safe_question']        = 0;
        $updateData['safe_answer']          = '';
        $updateData['google_secret_code']   = '';
        $updateData['google_secret_isopen'] = 0;
        $rs                                 = $obj->update($id, $updateData);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }
        
        return true;
    }
    
    private function del()
    {
        $id = intval(BooVar::post('id'));
        if (!$id) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        
        $obj      = BooController::get('Obj_App_Info');
        $userInfo = $obj->getInfoById($id);
        if (!$userInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }
        
        $rs = $obj->delete($id);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::DELETE_ERR);
        }
        
        $obj = BooController::get('Obj_App_Balance');
        $obj->delete($id);
        
        return true;
    }
    
    private function changeKey()
    {
        $key = md5(time() . rand(1, 99));
        
        return array('key' => $key);
    }
    
}
