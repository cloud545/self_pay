<?php
class Api_Admin_App_exportExcel extends Api_Admin_App {

	protected function _do() {
        $post = $_GET;
        $app_id = $post['platId'];
        $startTime = $post['startTime'];
        $endTime = $post['endTime'];

        $obj = BooController::get('Obj_App_Report');
        $data = $obj->getList($app_id, $startTime, $endTime);
        $sumInfo = $obj->getSum($app_id, $startTime, $endTime);
        if (!$data) {
            return false;
        }

        $data2 = array_merge($data, $sumInfo);
        if ($app_id) {
            $name = $data['0']['app_name'];
        } else {
            $name = '';
        }        
        $list = array();
        foreach ($data as $key => $info) {
            $tmpArr = array(
                $info['report_date'],        // 日期
                $info['app_name'],           // 商户
                $info['all_order_number'],   // 总下单笔数
                $info['order_number'],       // 下单笔数
                $info['real_amount'],        // 总充值金额
                $info['fee'],                // 总结算金额
                $info['withdrawal_amount'],  // 总提款金额
                $info['withdrawal_fee'],     // 总手续费
                $info['balance'],            // 余额
            );

            $list[] = $tmpArr;
        }

        $excelObj = new BooExcel();
        //$excelObj = $objPHPExcel = PHPExcel_IOFactory::createReader();

        //横向单元格标识
        $cellName = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ');
        $title = array('日期', '商户', '总下单笔数', '下单笔数', '总充值金额', '总结算金额', '总提款金额', '总手续费', '余额');

        $excelObj->getActiveSheet(0)->setTitle('sheet1');   //设置sheet名称
        $_row = 1;   //设置纵向单元格标识
        $_cnt = count($title);
        $excelObj->getActiveSheet(0)->mergeCells('A'.$_row.':'.$cellName[$_cnt-1].$_row);   //合并单元格
        $excelObj->setActiveSheetIndex(0)->setCellValue('A'.$_row, '数据导出：'.date('Y-m-d H:i:s'));  //设置合并后的单元格内容
        $_row++;
        $i = 0;

        foreach($title as $v){   //设置列标题
            $excelObj->setActiveSheetIndex(0)->setCellValue($cellName[$i].$_row, $v);
            $i++;
        }
        $_row++;


        //填写数据
        if($list){
            $i = 0;
            foreach($list AS $_v){
                $j = 0;
                foreach($_v AS $_cell){
                    $excelObj->getActiveSheet(0)->setCellValue($cellName[$j] . ($i+$_row), $_cell);
                    $j++;
                }

                $i++;
            }

            $sumArr = array(
                '所有商户汇总',
                '',
                $sumInfo['allOrderNum'],          // 总下单笔数
                $sumInfo['orderNum'],             // 总成功充值笔数
                $sumInfo['sumRealAmount'],        // 总充值金额
                $sumInfo['sumFee'],               // 总结算金额
                $sumInfo['sumWithdrawal'],        // 总提款金额
                $sumInfo['sumWithdrawalFee'],        // 总提款手续费
                $sumInfo['sumBalance'],           // 余额
            );

            $j = 0;
            foreach($sumArr AS $_cell){
                $excelObj->getActiveSheet(0)->setCellValue($cellName[$j] . ($i+$_row), $_cell);
                $j++;
            }

        }

        $objWrite = PHPExcel_IOFactory::createWriter($excelObj,'Excel2007');

        header('pragma:public');
        header("Content-Disposition:attachment;filename=" .$name .' '. date('Y-m-d') .".xls");

        $objWrite->save('php://output');
        exit();
    }
}