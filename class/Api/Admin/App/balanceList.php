<?php
class Api_Admin_App_balanceList extends Api_Admin_App {

    protected $_tpl = "app/balanceList.html";

	protected function _do() {

        if ($_POST && in_array($_POST['flag'], array('update', 'updateBalance', 'frozenBalance'))) {

            switch ($_POST['flag']) {
                case 'update':
                    return $this->update();
                case 'updateBalance':
                    return $this->updateBalance();
                case 'frozenBalance':
                    return $this->frozenBalance();
                default:
                    Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
                    return false;
            }

        } else {

            $post = BooVar::postx();
            $appId = intval($post['appId']);
            $status = $post['status'] != 'all' ? intval($post['status']) : 'all';
            $_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数

            $obj = BooController::get('Obj_App_Balance');
            $pageList = $obj->getPageList($appId, $status);

            if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
                return $pageList;
            } else {

                $appObj = BooController::get('Obj_App_Info');
                $tmpAppList = $appObj->getList();

                $appList = array();
                foreach ($tmpAppList as $info) {
                    $appList[$info['app_id']] = $info['name'];
                }

                BooView::set('appList', $appList);
                BooView::set('pageList', $pageList);
            }
        }
	}

    private function update() {
        $post = BooVar::postx();

        if (!$post['id']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        $id = intval($post['id']);

        $obj = BooController::get('Obj_App_Balance');
        $info = $obj->getInfoById($id);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }

        $updateData = array();
        $updateData['lock_status'] = intval($post['isLock']);
        $rs = $obj->update($id, $updateData);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }

        return true;
    }

    private function updateBalance() {

        $adminName = BooSession::get('adminName');
        if (!in_array($adminName, array('admin', 'best1pay','xiaotan','马克','yunli','ningmeng','kele','weide','haley'))) {
            Common_errorCode::jsonEncode(Common_errorCode::NO_POWER);
        }

        $post = BooVar::postx();

        if (!$post['id'] || !$post['amount'] || !$post['remark']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        $id = intval($post['id']);

        $obj = BooController::get('Obj_App_Balance');
        $info = $obj->getInfoById($id);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }

        $cacheKey = BooConfig::get('cacheKey.appWithdrawalLock') . $info['app_id'];
        if (Common_cacheLock::get($cacheKey)) {
            return Common_errorCode::jsonEncode(Common_errorCode::FA_IS_WITHDRAWAL_BUSY);
        }
        $rs = Common_cacheLock::set($cacheKey, 1, 3);
        if (!$rs) {
            return Common_errorCode::jsonEncode(Common_errorCode::FA_IS_WITHDRAWAL_BUSY);
        }

        if ($post['amount'] < 0 && $info['available_balance'] < abs($post['amount'])) {
            // 删除锁
            Common_cacheLock::del($cacheKey);
            return Common_errorCode::jsonEncode(Common_errorCode::CASH_IS_NOT_ENOUGH);
        }

        $updateData = array();
        $updateData['available_balance'] = $info['available_balance'] + $post['amount'];
        $updateData['cash_balance'] = $info['cash_balance'] + $post['amount'];
        $rs = $obj->update($id, $updateData);
        if (!$rs) {
            // 删除锁
            Common_cacheLock::del($cacheKey);
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }

        // 计算记录写入帐变表
        $insertData = array();
        $insertData['app_id'] = $info['app_id'];
        $insertData['app_name'] = $info['app_name'];
        $insertData['amount'] = $post['amount'];
        $insertData['type'] = $post['amount'] > 0 ? 4 : 5;
        $insertData['create_time'] = date('Y-m-d H:i:s');
        $insertData['create_date'] = date('Ymd');
        $insertData['current_balance'] = $updateData['cash_balance'];
        $insertData['remark'] = $post['remark'];
        $orderObj = BooController::get('Obj_App_Orders');
        $orderObj->insert($insertData);

        // 删除锁
        Common_cacheLock::del($cacheKey);

        return array('available_balance' => $updateData['available_balance'], 'cash_balance' => $updateData['cash_balance']);
    }

    private function frozenBalance() {

        $adminName = BooSession::get('adminName');
        if (!in_array($adminName, array('admin', 'best1pay','xiaotan','乔伊','ningmeng','kele','weide','haley'))) {
            Common_errorCode::jsonEncode(Common_errorCode::NO_POWER);
        }

        $post = BooVar::postx();

        if (!$post['id'] || !$post['remark']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        $id = intval($post['id']);

        $obj = BooController::get('Obj_App_Balance');
        $info = $obj->getInfoById($id);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }

        $cacheKey = BooConfig::get('cacheKey.appWithdrawalLock') . $info['app_id'];
        if (Common_cacheLock::get($cacheKey)) {
            return Common_errorCode::jsonEncode(Common_errorCode::FA_IS_WITHDRAWAL_BUSY);
        }
        $rs = Common_cacheLock::set($cacheKey, 1, 3);
        if (!$rs) {
            return Common_errorCode::jsonEncode(Common_errorCode::FA_IS_WITHDRAWAL_BUSY);
        }

        if ($post['amount'] < 0 && $info['available_balance'] < abs($post['amount'])) {
            // 删除锁
            Common_cacheLock::del($cacheKey);
            return Common_errorCode::jsonEncode(Common_errorCode::CASH_IS_NOT_ENOUGH);
        }

        $updateData = array();
        $amount = $type = 0;
        $cashBalance = $info['cash_balance'];
        $availableBalance = $info['available_balance'];
        if ($post['amount'] > 0) {
            $updateData['available_balance'] = $info['available_balance'] - $post['amount'];
            $updateData['frozen_balance'] = $info['frozen_balance'] + $post['amount'];
            $availableBalance = $updateData['available_balance'];

            $amount = $post['amount'];
            $type = 6;
        } elseif ($post['amount'] < 0) {
            $updateData['available_balance'] = $info['available_balance'] + abs($post['amount']);
            $updateData['frozen_balance'] = $info['frozen_balance'] - abs($post['amount']);
            $availableBalance = $updateData['available_balance'];

            $amount = $post['amount'];
            $type = 7;
        } else {
            $updateData['frozen_balance'] = 0;
            $updateData['cash_balance'] = $info['cash_balance'] - $info['frozen_balance'];
            $cashBalance = $updateData['cash_balance'];

            $amount = $info['frozen_balance'];
            $type = 8;
        }

        $rs = $obj->update($id, $updateData);
        if (!$rs) {
            // 删除锁
            Common_cacheLock::del($cacheKey);
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }

        // 计算记录写入帐变表
        $insertData = array();
        $insertData['app_id'] = $info['app_id'];
        $insertData['app_name'] = $info['app_name'];
        $insertData['amount'] = $amount;
        $insertData['type'] = $type;
        $insertData['create_time'] = date('Y-m-d H:i:s');
        $insertData['create_date'] = date('Ymd');
        $insertData['current_balance'] = $availableBalance;
        $insertData['remark'] = $post['remark'];
        $orderObj = BooController::get('Obj_App_Orders');
        $orderObj->insert($insertData);

        // 删除锁
        Common_cacheLock::del($cacheKey);

        return array('available_balance' => $availableBalance, 'cash_balance' => $cashBalance, 'frozen_balance' => $updateData['frozen_balance']);
    }

}
