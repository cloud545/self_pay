<?php
class Api_Admin_App_payList extends Api_Admin_App {

    protected $_tpl = "app/payList.html";

	protected function _do() {

        if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {

            $post = BooVar::postx();

            $payId = 0;
            if ($post['pOrderId']) {
                $appPayInfo = explode('1', trim($post['pOrderId']));
                $post['appId'] = BooController::get('Common')->numberAndStrSwap($appPayInfo[0], 'decode');
                $payId = BooController::get('Common')->numberAndStrSwap($appPayInfo[1], 'decode');
            }

            if ($post['appId']) {
                $appId = intval($post['appId']);
                $platId = intval($post['platId']);
                $result = $post['result'];
                $id = $post['tradeNo'] ? substr($post['tradeNo'],9) : 0;
                $orderId = $post['orderId'] ? trim($post['orderId']) : '';
                $type = $post['type'];
                $startTime = $post['startTime'];
                $endTime = $post['endTime'];
                $_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数

                if ($orderId || $payId) {
                    $result = 'all';
                }

                $obj = BooController::get('Obj_App_Pay');
                $pageList = $obj->getPageList($appId, $platId, $id, $type, $startTime, $endTime, $result, $orderId, $payId);
                foreach ($pageList['data'] as $key => $info) {

                    $tmpAppId = BooController::get('Common')->numberAndStrSwap($info['app_id']);
                    $orderId = BooController::get('Common')->numberAndStrSwap($info['id']);
                    $orderPlatId = BooController::get('Common')->numberAndStrSwap($info['p_id']);

                    $pageList['data'][$key]['pOrderId'] = "{$tmpAppId}1{$orderId}1{$orderPlatId}";
                }

                $sumInfo = $obj->getSum($appId, $platId, $id, $type, $startTime, $endTime);

                return array_merge($pageList, $sumInfo);
            }

            return true;
        } else {

            $appObj = BooController::get('Obj_App_Info');
            $tmpAppList = $appObj->getList();

            $appList = array();
            foreach ($tmpAppList as $info) {
                if ($info['is_lock']) {
                    continue;
                }
                $appList[$info['app_id']] = $info['name'];
            }

            $platObj = BooController::get('Obj_Plat_Info');
            $tmpPlatList = $platObj->getList();

            $platList = array();
            foreach ($tmpPlatList as $info) {
                $platList[$info['p_id']] = $info['p_name'];
            }

            $payTypeObj = BooController::get('Obj_Plat_PayType');
            $tmpPayTypeList = $payTypeObj->getList();

            $payTypeList = array();
            foreach ($tmpPayTypeList as $info) {
                $payTypeList[$info['pt_id']] = $info['pt_name'];
            }

            $get = BooVar::getx();
            if ($get['appId']) {
                BooView::set('searchAppId', $get['appId']);
            }

            if ($get['tradeNo']) {
                BooView::set('tradeNo', $get['tradeNo']);
            }

            if ($get['startTime']) {
                BooView::set('startTime', $get['startTime']);
                BooView::set('endTime', $get['endTime']);
            }

            BooView::set('appList', $appList);
            BooView::set('platList', $platList);
            BooView::set('payTypeList', $payTypeList);
        }
	}
}