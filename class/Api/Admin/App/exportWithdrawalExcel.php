<?php
class Api_Admin_App_exportWithdrawalExcel extends Api_Admin_App {

	protected function _do() {

        $post = BooVar::requestx();
        $appId = intval($post['appId']);
        $status = isset($post['status']) ? $post['status'] : 'all';
        $startTime = $post['startTime'];
        $endTime = $post['endTime'];

        if ($post['tradeNo']) {
            if (!Common_rule::checkStrSafe($post['tradeNo'])) {
                $id = 0;
            } else {
                $id = substr($post['tradeNo'], 9);
            }
        } else {
            $id = 0;
        }
        
        $obj = BooController::get('Obj_App_Withdrawal');
        $list = $obj->getList($appId, $status, $startTime, $endTime, $id);
        foreach ($pageList['data'] as $key => $info) {
            $info['tradeNo'] = 'W' . date('Ymd', strtotime($info['create_time'])) . $info['id'];
            $pageList['data'][$key] = $info;
        }

        $sumInfo = $obj->getSum($appId, 2, $startTime, $endTime,$id);

        $excelObj = new BooExcel();
        //$excelObj = $objPHPExcel = PHPExcel_IOFactory::createReader();

        //横向单元格标识
        $cellName = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ');
        $title = array('序号','商户', '提款金额', '提款手续费', '提款银行', '提款银行卡号', '银行卡对应的支行', '持卡人', '交易流水号', '申请时间', '备注', '状态','提款申请人','审核管理员id','提款审核时间');
        $fieldArr = array(
                'id',               // 序号
                'app_name',         // 商户
                'amount',           // 提款金额
                'fee',              // 提款手续费
                'bank_type',        // 提款银行
                'card_no',          // 提款银行卡号
                'bank_name',        // 银行卡对应的支行
                'card_account_name',// 持卡人
                'tradeNo',          // 交易流水号
                'create_time',      // 申请时间
                'remark',           // 备注
                'status',           // 状态
                'apply_user',       // 提款申请人
                'operator_id',      // 审核管理员id
                'operate_time'      // 提款审核时间
            );

        $excelObj->getActiveSheet(0)->setTitle('sheet1');   //设置sheet名称
        $_row = 1;   //设置纵向单元格标识
        $_cnt = count($title);
        $excelObj->getActiveSheet(0)->mergeCells('A'.$_row.':'.$cellName[$_cnt-1].$_row);   //合并单元格
        $excelObj->setActiveSheetIndex(0)->setCellValue('A'.$_row, '数据导出：'.date('Y-m-d H:i:s'));  //设置合并后的单元格内容
        $_row++;
        $i = 0;

        foreach($title as $v){   //设置列标题
            $excelObj->setActiveSheetIndex(0)->setCellValue($cellName[$i].$_row, $v);
            $i++;
        }
        $_row++;


        //填写数据
        if($list){
            $i = 0;
            foreach($list AS $_v){
                $j = 0;
                foreach($fieldArr AS $field){
                    $value = $_v[$field];
                    
                    if($field=="status"){
                        if($value==1){
                            $value = '银行处理中';
                        }elseif($value==2){
                            $value = '提款成功';
                        }elseif($value==3){
                            $value = '提款失败';
                        }else{
                            $value = '系统处理中';
                        }
                    }elseif($field=="tradeNo"){
                        $value = 'W' . date('Ymd', strtotime($_v['create_time'])) . $_v['id'];
                    }
                    
                    $excelObj->getActiveSheet(0)->setCellValue($cellName[$j] . ($i+$_row), $value);
                    $j++;
                }

                $i++;
            }

            $sumArr = array(
                '提款总计',
                $sumInfo['sumAmount'],
                $sumInfo['sumFee'],
            );

            $j = 0;
            foreach($sumArr AS $_cell){
                $excelObj->getActiveSheet(0)->setCellValue($cellName[$j] . ($i+$_row), $_cell);
                $j++;
            }
            
        }
        

        $objWrite = PHPExcel_IOFactory::createWriter($excelObj, 'Excel2007');

        header('pragma:public');
        header("Content-Disposition:attachment;filename=" . date('Y-m-d') .".xlsx");

        $objWrite->save('php://output');exit;

        return true;
    }
}