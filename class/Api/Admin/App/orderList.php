<?php
class Api_Admin_App_orderList extends Api_Admin_App {

    protected $_tpl = "app/orderList.html";

	protected function _do() {

        $post = BooVar::postx();
        $appId = intval($post['appId']);
        $type = $post['type'] ? $post['type'] : 'all';
        $startTime = $post['startTime'];
        $endTime = $post['endTime'];
        $_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数

        $obj = BooController::get('Obj_App_Orders');
        $pageList = $obj->getPageList($appId, $type, $startTime, $endTime);
        $sumData = $obj->getSum($appId, $type, $startTime, $endTime);
        $pageList = array_merge($pageList, $sumData);
        foreach ($pageList['data'] as $key => $info) {

            if ($info['type'] == 1) {
                $info['tradeInfo'] = "{$info['start_time']} 到 {$info['end_time']} 充值结算";
                if ($info['remark']) {
                    $info['tradeInfo'] .= "，" . $info['remark'];
                }
            } elseif (in_array($info['type'], array(2, 3))) {
                $info['tradeInfo'] = 'W' . date('Ymd', strtotime($info['create_time'])) . $info['trade_no'];
            } elseif (in_array($info['type'], array(4, 5))) {
                $info['tradeInfo'] = '管理员变更资金，变更缘由：' . $info['remark'];
            } else {
                $info['tradeInfo'] = '管理员变更冻结资金，变更缘由：' . $info['remark'];
            }

            if ($info['type'] == 1) {
                $info['typeName'] = '充值结算';
            } elseif ($info['type'] == 2) {
                $info['typeName'] = '提款';
            } elseif ($info['type'] == 3) {
                $info['typeName'] = '提款手续费';
            } elseif ($info['type'] == 4) {
                $info['typeName'] = '管理员添加资金';
            } elseif ($info['type'] == 5) {
                $info['typeName'] = '管理员扣减资金';
            } elseif ($info['type'] == 6) {
                $info['typeName'] = '管理员冻结资金';
            } elseif ($info['type'] == 7) {
                $info['typeName'] = '管理员解冻资金';
            } else {
                $info['typeName'] = '管理员清除冻结资金';
            }

            $pageList['data'][$key] = $info;
        }

        if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
            return $pageList;
        } else {

            $appObj = BooController::get('Obj_App_Info');
            $tmpAppList = $appObj->getList();
            $appList = array();
            foreach ($tmpAppList as $info) {
                if ($info['is_lock']) {
                    continue;
                }
                $appList[$info['app_id']] = $info['name'];
            }

            BooView::set('appList', $appList);
            BooView::set('pageList', $pageList);
        }
	}

}