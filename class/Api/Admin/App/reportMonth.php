<?php
class Api_Admin_App_reportMonth extends Api_Admin_App {

    protected $_tpl = "app/reportMonth.html";

	protected function _do() {

        $post = BooVar::postx();
        $appId = intval($post['appId']);
        $startTime = date('Y-m', strtotime($post['startTime'])) ?? '1970-01';
        $_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数
        $obj = BooController::get('Obj_App_ReportMonth');
        $pageList = $obj->getPageList($appId, $startTime);
        $sumInfo = $obj->getSum($appId, $startTime);
        $pageList = array_merge($pageList, $sumInfo);

        if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
            return $pageList;
        } else {

            $appObj = BooController::get('Obj_App_Info');
            $tmpAppList = $appObj->getList();

            $appList = array();
            foreach ($tmpAppList as $info) {
                if ($info['is_lock']) {
                    continue;
                }
                $appList[$info['app_id']] = $info['name'];
            }

            BooView::set('appList', $appList);
            BooView::set('pageList', $pageList);
        }
	}

}