<?php
class Api_Admin_App_report extends Api_Admin_App {

    protected $_tpl = "app/report.html";

	protected function _do() {

        $post = BooVar::postx();
        $appId = intval($post['appId']);
        $startTime = $post['startTime'];
        $endTime = $post['endTime'];
        $_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数

        $obj = BooController::get('Obj_App_Report');
        $pageList = $obj->getPageList($appId, $startTime, $endTime);
        $sumInfo = $obj->getSum($appId, $startTime, $endTime);
        $pageList = array_merge($pageList, $sumInfo);

        if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
            return $pageList;
        } else {

            $appObj = BooController::get('Obj_App_Info');
            $tmpAppList = $appObj->getList();

            $appList = array();
            foreach ($tmpAppList as $info) {
                if ($info['is_lock']) {
                    continue;
                }
                $appList[$info['app_id']] = $info['name'];
            }

            BooView::set('appList', $appList);
            BooView::set('pageList', $pageList);
        }
	}

}