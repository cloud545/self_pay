<?php
class Api_Admin_App_withdrawalList extends Api_Admin_App {

    protected $_tpl = "app/withdrawalList.html";

	protected function _do() {

        if (in_array($_POST['flag'], array('getInfo', 'update'))) {

            switch ($_POST['flag']) {
                case 'getInfo':
                    return $this->getInfo();
                case 'update':
                    return $this->update();
                default:
                    return false;
            }
        } else {

            $post = BooVar::postx();
            $appId = intval($post['appId']);
            $status = isset($post['status']) ? $post['status'] : 'all';
            $startTime = $post['startTime'];
            $endTime = $post['endTime'];

            if ($post['tradeNo']) {
                if (!Common_rule::checkStrSafe($post['tradeNo'])) {
                    $id = 0;
                } else {
                    $id = substr($post['tradeNo'], 9);
                }
            } else {
                $id = 0;
            }

            $_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数

            $obj = BooController::get('Obj_App_Withdrawal');
            $pageList = $obj->getPageList($appId, $status, $startTime, $endTime, $id);
            foreach ($pageList['data'] as $key => $info) {
                $info['tradeNo'] = 'W' . date('Ymd', strtotime($info['create_time'])) . $info['id'];
                $pageList['data'][$key] = $info;
            }

            $sumInfo = $obj->getSum($appId, 2, $startTime, $endTime);
            $pageList = array_merge($pageList, $sumInfo);

            if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
                return $pageList;
            } else {
                $appObj = BooController::get('Obj_App_Info');
                $tmpAppList = $appObj->getList();

                $appList = array();
                foreach ($tmpAppList as $info) {
                    if ($info['is_lock']) {
                        continue;
                    }
                    $appList[$info['app_id']] = $info['name'];
                }

                $get = BooVar::getx();
                if ($get['appId']) {
                    BooView::set('searchAppId', $get['appId']);
                }

                if ($get['tradeNo']) {
                    BooView::set('tradeNo', $get['tradeNo']);
                }

                //BooView::set('adminId', BooSession::get('adminId'));
                BooView::set('appList', $appList);
                BooView::set('pageList', $pageList);
            }
        }
	}

	private function getInfo() {

        $post = BooVar::postx();

        if (!$post['id']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        $id = intval($post['id']);

        $obj = BooController::get('Obj_App_Withdrawal');
        $info = $obj->getInfoById($id);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::NO_DATA);
        }

        $balanceObj = BooController::get('Obj_App_Balance');
        $balanceInfo = $balanceObj->getInfoByAppId($info['app_id']);
        if (!$balanceInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::NO_DATA);
        }

        $returnData = array();
        $returnData['id'] = $id;
        $returnData['app_name'] = $info['app_name'];
        $returnData['app_id'] = $info['app_id'];
        $returnData['cash_balance'] = $balanceInfo['cash_balance'];
        $returnData['amount'] = $info['amount'];
        $returnData['balanceStatus'] = $balanceInfo['lock_status'] ? '是' : '否';

        return $returnData;
    }

    private function update() {
        $post = BooVar::postx();

        if (!$post['id'] || !$post['status'] || ($post['status'] == 3 && !$post['remark'])) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        $id = intval($post['id']);

        // 00 点暂不结算，等日汇总结算后再进行结算，否则会导致商户余额在5分钟内全部显示为0
        $startTime = date('Y-m-d') . " 00:00:00";
        if (time() < (strtotime($startTime) + 600)) {
            Common_errorCode::jsonEncode(Common_errorCode::SYSTEM_DAY_SETTLE_ACCOUNTS);
        }

        $obj = BooController::get('Obj_App_Withdrawal');
        $info = $obj->getInfoById($id);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::NO_DATA);
        }
		
		//if($info['operator_id'] != 0 && $info['operator_id'] != BooSession::get('adminId')){
		//	Common_errorCode::jsonEncode(Common_errorCode::ALREADY_DOING);
		//}

        if (in_array($info['status'], array(2, 3))) {
            Common_errorCode::jsonEncode(Common_errorCode::WITHDRAWAL_IS_END);
        }

        $cacheKey = BooConfig::get('cacheKey.appWithdrawalLock') . $info['app_id'];
        if (Common_cacheLock::get($cacheKey)) {
            Common_errorCode::jsonEncode(Common_errorCode::FA_IS_WITHDRAWAL_BUSY);
        }
        $rs = Common_cacheLock::set($cacheKey, 1, 3);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::FA_IS_WITHDRAWAL_BUSY);
        }

        $updateData = array();
        $updateData['status'] = $post['status'];
        $updateData['remark'] = $post['remark'];
        $updateData['operator_id'] = BooSession::get('adminId');
        $updateData['operate_time'] = date('Y-m-d H:i:s');
        $rs = $obj->update($id, $updateData);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }

        // 审核提款
        if ($post['status'] == 2) {

            $balanceObj = BooController::get('Obj_App_Balance');
            $balanceInfo = $balanceObj->getInfoByAppId($info['app_id']);
            if (!$balanceInfo) {
                Common_errorCode::jsonEncode(Common_errorCode::NO_DATA);
            }

            $updateBalanceData = array();
            $updateBalanceData['frozen_amount'] = $balanceInfo['frozen_amount'] - $info['amount'] - $info['fee'];
            $updateBalanceData['cash_balance'] = $balanceInfo['cash_balance'] - $info['amount'] - $info['fee'];
            $rs = $balanceObj->update($balanceInfo['id'], $updateBalanceData);
            if (!$rs) {
                Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
            }

            $orderObj = BooController::get('Obj_App_Orders');

            // 计算记录写入帐变表
            $insertData = array();
            $insertData['app_id'] = $info['app_id'];
            $insertData['app_name'] = $info['app_name'];
            $insertData['amount'] = $info['amount'];
            $insertData['type'] = 2;
            $insertData['trade_no'] = $info['id'];
            $insertData['create_time'] = date('Y-m-d H:i:s');
            $insertData['create_date'] = date('Ymd');
            $insertData['current_balance'] = $balanceInfo['cash_balance'] - $info['amount'];
            $rs = $orderObj->insert($insertData);
            if (!$rs) {
                Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
            }

            // 计算记录写入帐变表
            $insertData = array();
            $insertData['app_id'] = $info['app_id'];
            $insertData['app_name'] = $info['app_name'];
            $insertData['amount'] = $info['fee'];
            $insertData['type'] = 3;
            $insertData['trade_no'] = $info['id'];
            $insertData['create_time'] = date('Y-m-d H:i:s');
            $insertData['create_date'] = date('Ymd');
            $insertData['current_balance'] = $balanceInfo['cash_balance'] - $info['amount'] - $info['fee'];
            $rs = $orderObj->insert($insertData);
            if (!$rs) {
                Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
            }

            // 如果审核的提款订单的时间跟提交订单的时间不在同一天，则需要重新结算提交订单那天的数据
            if (date('Y-m-d', strtotime($info['create_time'])) != date('Y-m-d')) {

                $createDateTime = strtotime(date('Y-m-d', strtotime($info['create_time'])));
                $nowDateTime = strtotime(date('Y-m-d'));
                for ($i = $createDateTime; $i < $nowDateTime; $i += 86400) {
                    BooController::get('Mod_Report')->appCalculate($info['app_id'], date('Y-m-d', $i));
                }

            }

        } elseif ($post['status'] == 3) {

            $balanceObj = BooController::get('Obj_App_Balance');
            $balanceInfo = $balanceObj->getInfoByAppId($info['app_id']);
            if (!$balanceInfo) {
                Common_errorCode::jsonEncode(Common_errorCode::NO_DATA);
            }

            $updateBalanceData = array();
            $updateBalanceData['frozen_amount'] = $balanceInfo['frozen_amount'] - $info['amount'] - $info['fee'];
            $updateBalanceData['available_balance'] = $balanceInfo['available_balance'] + $info['amount'] + $info['fee'];
            $rs = $balanceObj->update($balanceInfo['id'], $updateBalanceData);
            if (!$rs) {
                Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
            }

        }

        // 删除锁
        Common_cacheLock::del($cacheKey);

        return true;
    }

}