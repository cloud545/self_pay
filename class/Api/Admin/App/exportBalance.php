<?php
class Api_Admin_App_exportBalance extends Api_Admin_App {

	protected function _do() {
        $post = $_GET;
        $app_id = $post['app_id'];
        $type = $post['type'];

        $obj = BooController::get('Obj_App_Balance');
        $data = $obj->getList($app_id , $type);
        if (!$data) {
            return null;
        }

        $list = array();
        foreach ($data as $key => $info) {

            if ($info['lock_status'] == 0) {
                $status = '否';
            } else {
                $status = '是';
            }
            $tmpArr = array(
                $info['id'],
                $info['app_id'],
                $info['app_name'],
                $info['available_balance'],
                $info['cash_balance'],
                $info['frozen_amount'],
                $info['frozen_balance'],
                $info['last_modify_time'],
                $status,
            );

            $list[] = $tmpArr;
        }

        $excelObj = new BooExcel();
        //$excelObj = $objPHPExcel = PHPExcel_IOFactory::createReader();

        //横向单元格标识
        $cellName = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ');
        $title = array('序号', '商户ID', '商户', '可用余额', '现金余额', '提现冻结金额', '投诉冻结金额', '上次修改时间', '是否锁定');

        $excelObj->getActiveSheet(0)->setTitle('sheet1');   //设置sheet名称
        $_row = 1;   //设置纵向单元格标识
        $_cnt = count($title);
        $excelObj->getActiveSheet(0)->mergeCells('A'.$_row.':'.$cellName[$_cnt-1].$_row);   //合并单元格
        $excelObj->setActiveSheetIndex(0)->setCellValue('A'.$_row, '商户资金列表');  //设置合并后的单元格内容
        $_row++;
        $i = 0;

        foreach($title as $v){   //设置列标题
            $excelObj->setActiveSheetIndex(0)->setCellValue($cellName[$i].$_row, $v);
            $i++;
        }
        $_row++;


        //填写数据
        if($list){
            $i = 0;
            foreach($list AS $_v){
                $j = 0;
                foreach($_v AS $_cell){
                    $excelObj->getActiveSheet(0)->setCellValue($cellName[$j] . ($i+$_row), $_cell);
                    $j++;
                }

                $i++;
            }


        }

        $objWrite = PHPExcel_IOFactory::createWriter($excelObj,'Excel2007');

        header('pragma:public');
        header("Content-Disposition:attachment;filename=商户资金列表.xls");

        $objWrite->save('php://output');
        exit();
    }
}
