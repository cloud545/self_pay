<?php
class Api_Admin_Proxy_list extends Api_Admin_Proxy {

    protected $_tpl = "proxy/list.html";

	protected function _do() {

        if ($_POST && in_array($_POST['flag'], array('add', 'update', 'del', 'resetLoginPassword', 'resetSecurityPassword'))) {

            switch ($_POST['flag']) {
                case 'add':
                    return $this->add();
                case 'update':
                    return $this->update();
                case 'del':
                    return $this->del();
                case 'resetLoginPassword':
                    return $this->resetLoginPassword();
                case 'resetSecurityPassword':
                    return $this->resetSecurityPassword();
                default:
                    Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
                    return false;
            }

        } else {

            $post = BooVar::postx();
            $proxyId = intval($post['proxyId']);
            $name = $post['name'];
            $status = $post['status'] != 'all' ? intval($post['status']) : 'all';
            $_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数
            $obj = BooController::get('Obj_Plat_PayType');
            $tmpPayTypeList = $obj->getList($proxyId, $status);
            $payTypeList = array();
            foreach ($tmpPayTypeList as $info) {
                $payTypeList[$info['pt_id']] = $info['pt_name'];
            }

            $obj = BooController::get('Obj_Plat_Info');
            $tmpPlatList = $obj->getList();

            $platList = array();
            foreach ($tmpPlatList as $info) {
                $platList[$info['p_id']] = array(
                    'pt_id' => $info['pt_id'],
                    'p_id' => $info['p_id'],
                    'p_name' => $info['p_name'],
                );
            }

            $obj = BooController::get('Obj_Proxy_Info');
            $pageList = $obj->getPageList(0,$name);

            foreach ($pageList['data'] as $key => $info) {

                $tmpArr = array();
                $tmpArr['proxy_id'] = $info['proxy_id'];
                $tmpArr['proxy_name'] = $info['proxy_name'];
                $tmpArr['proxyName'] = $info['parent_name'] ? $info['parent_name'] : '';
                $tmpArr['adminName'] = $info['add_admin'];
                $tmpArr['allow_login_ip'] = $info['allow_login_ip'];
                $tmpArr['last_ip'] = $info['last_ip'];
                $tmpArr['create_time'] = $info['create_time'];
                $tmpArr['last_time'] = $info['last_update_time'];
                $tmpArr['is_lock'] = $info['is_lock'];
                $tmpArr['withdrawal_fee'] = $info['withdrawal_fee'];

                $platNameList = '';
                if ($info['plat_app_info']) {
                    $info['plat_app_info'] = json_decode($info['plat_app_info'], true);

                    foreach ($info['plat_app_info'] as $platId => $platInfo) {
                        if ($platNameList) {
                            $platNameList .= '<br/>' . $payTypeList[$platInfo['payType']] . '：' . $platList[$platId]['p_name'] . "({$platInfo['point']})";
                        } else {
                            $platNameList = $payTypeList[$platInfo['payType']] . '：' . $platList[$platId]['p_name'] . "({$platInfo['point']})";
                        }
                    }
                    $tmpArr['openPlatList'] = $platNameList;

                }
                $tmpArr['plat_app_info'] = $info['plat_app_info'];
                $tmpArr['is_open_tree'] = $info['is_open_tree'];
                $tmpArr['isOpenTree'] = $info['is_open_tree'] ? '是' : '否';
                $pageList['data'][$key] = $tmpArr;
            }

            if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
                return $pageList;
            } else {
                $adminObj = BooController::get('Obj_Admin_Info');
                $adminInfo = $adminObj->getInfo(BooSession::get('adminId'));

                BooView::set('withdrawal_fee', $adminInfo['withdrawal_fee']);
                BooView::set('platList', $platList);
                BooView::set('pageList', $pageList);
            }
        }
	}

    private function add() {

        $post = BooVar::postx();
        if (!$post['name'] || !$post['password']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }

        $obj = BooController::get('Obj_Proxy_Info');
        $userInfo = $obj->getInfoByName($post['name']);
        if ($userInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_IS_EXIST);
        }

        $salt = Common_rule::getRandSaltStr();
        $password = Common_rule::encodeLoginPassword($post['password'], $salt);

        $platAppInfo = array();
        if ($post['platId']) {

            $platObj = BooController::get('Obj_Plat_Info');

            $platTypeList = array();
            foreach ($post['platId'] as $key => $value) {

                $platInfo = $platObj->getInfoById($value);
                if (!$platInfo) {
                    continue;
                }

                if ($post['appPoint'][$key] <= 0) {
                    Common_errorCode::jsonEncode(Common_errorCode::POINT_IS_ZERO);
                }

                if ($post['appPoint'][$key] < $platInfo['p_point']) {
                    Common_errorCode::jsonEncode(Common_errorCode::POINT_IS_LOW);
                }

                $platAppInfo[$value] = array(
                    'platTag' => $platInfo['p_tag'],
                    'payType' => $platInfo['pt_id'],
                    'point' => $post['appPoint'][$key],
                );

                $platTypeList[$platInfo['pt_id']] += 1;

                if ($platTypeList[$platInfo['pt_id']] > 1) {
                    Common_errorCode::jsonEncode(Common_errorCode::ONLY_ONE_PAY_TYPE);
                }
            }
        }

        $inserData = array(
            'proxy_name' => $post['name'],
            'proxy_pwd' => $password,
            'salt' => $salt,
            'plat_app_info' => $platAppInfo ? json_encode($platAppInfo) : '',
            'create_time' => date('Y-m-d H:i:s'),
            'add_admin' => BooSession::get('adminName'),
            'is_open_tree' => intval($post['isOpenTree']),
            'withdrawal_fee' => $post['withdrawalFee'] ? $post['withdrawalFee'] : 10,
        );

        $proxyId = $obj->insert($inserData);
        if (!$proxyId) {
            Common_errorCode::jsonEncode(Common_errorCode::INSERT_ERR);
        }

        $balanceObj = BooController::get('Obj_Proxy_Balance');
        $inserData = array(
            'proxy_id' => $proxyId,
            'proxy_name' => $post['name'],
        );
        $balanceObj->insert($inserData);

        return true;
    }

    private function update() {
        $post = BooVar::postx();

        if (!$post['id']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        $id = intval($post['id']);

        $obj = BooController::get('Obj_Proxy_Info');
        $info = $obj->getInfoById($id);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }

        $updateData = array();
        // 单纯操作是否锁定只更新这个值就行
        if ($post['onOffLock']) {
            $updateData['is_lock'] = intval($post['isLock']);
        } else {
            $updateData['allow_login_ip'] = $post['allowLoginIp'];
            $updateData['is_open_tree'] = intval($post['isOpenTree']);
            $updateData['withdrawal_fee'] = $post['withdrawalFee'] ? $post['withdrawalFee'] : 10;
        }

        if ($post['platId']) {

            $platObj = BooController::get('Obj_Plat_Info');

            $platTypeList = array();
            $platAppInfo = array();
            foreach ($post['platId'] as $key => $platId) {

                $platInfo = $platObj->getInfoById($platId);
                if (!$platInfo) {
                    continue;
                }

                if ($post['appPoint'][$key] <= 0) {
                    Common_errorCode::jsonEncode(Common_errorCode::POINT_IS_ZERO);
                }

                if ($post['appPoint'][$key] < $platInfo['p_point']) {
                    Common_errorCode::jsonEncode(Common_errorCode::POINT_IS_LOW);
                }

                $platAppInfo[$platId] = array(
                    'platTag' => $platInfo['p_tag'],
                    'payType' => $platInfo['pt_id'],
                    'point' => $post['appPoint'][$key],
                );

                $platTypeList[$platInfo['pt_id']] += 1;

                if ($platTypeList[$platInfo['pt_id']] > 1) {
                    Common_errorCode::jsonEncode(Common_errorCode::ONLY_ONE_PAY_TYPE);
                }
            }

            $updateData['plat_app_info'] = $platAppInfo ? json_encode($platAppInfo) : '';
        } else {
            $updateData['plat_app_info'] = '';
        }

        $rs = $obj->update($id, $updateData);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }

        return true;
    }


    private function del() {
        $id = intval(BooVar::post('id'));
        if (!$id) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }

        $obj = BooController::get('Obj_Proxy_Info');
        $userInfo = $obj->getInfoById($id);
        if (!$userInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }

        $rs = $obj->delete($id);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::DELETE_ERR);
        }

        return true;
    }

    private function resetLoginPassword() {

        $post = BooVar::postx();

        if (!$post['id']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        $id = intval($post['id']);

        $obj = BooController::get('Obj_Proxy_Info');
        $info = $obj->getInfoById($id);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }

        $salt = Common_rule::getRandSaltStr();
        $password = Common_rule::encodeLoginPassword('123qwe', $salt);

        $updateData = array();
        $updateData['proxy_pwd'] = $password;
        $updateData['salt'] = $salt;
        $rs = $obj->update($id, $updateData);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }

        return array('password' => '123qwe');
    }

    private function resetSecurityPassword()
    {

        $post = BooVar::postx();

        if (!$post['id']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        $id = intval($post['id']);

        $obj  = BooController::get('Obj_Proxy_Info');
        $info = $obj->getInfoById($id);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }

        $updateData                         = array();
        $updateData['security_pwd']         = '';
        $updateData['security_salt']        = '';
        $updateData['safe_question']        = 0;
        $updateData['safe_answer']          = '';
        $updateData['google_secret_code']   = '';
        $updateData['google_secret_isopen'] = 0;
        $rs                                 = $obj->update($id, $updateData);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }

        return true;
    }
}

