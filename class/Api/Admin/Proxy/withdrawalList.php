<?php
class Api_Admin_Proxy_withdrawalList extends Api_Admin_Proxy {

    protected $_tpl = "proxy/withdrawalList.html";

	protected function _do() {

        if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {

            if (in_array($_POST['flag'], array('getInfo', 'update'))) {
                switch ($_POST['flag']) {
                    case 'getInfo':
                        return $this->getInfo();
                    case 'update':
                        return $this->update();
                    default:
                        return false;
                }
            } else {
                $post = BooVar::postx();
				$proxyId = intval($post['proxyId']);
				$status = $post['status'];
				$startTime = $post['startTime'];
				$endTime = $post['endTime'];
				$id = $post['tradeNo'] ? substr($post['tradeNo'], 10) : 0;
				$_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数

				$obj = BooController::get('Obj_Proxy_Withdrawal');
				$pageList = $obj->getPageList($proxyId, $status, $startTime, $endTime, $id);
				foreach ($pageList['data'] as $key => $info) {
					$info['tradeNo'] = 'PW' . date('Ymd', strtotime($info['create_time'])) . $info['id'];
					$pageList['data'][$key] = $info;
				}

				return $pageList;
                
            }

            return true;
        } else {

            $obj = BooController::get('Obj_Proxy_Info');
            $tmpProxyList = $obj->getList();

            $proxyList = array();
            foreach ($tmpProxyList as $info) {
                if ($info['is_lock']) {
                    continue;
                }
                $proxyList[$info['proxy_id']] = $info['proxy_name'];
            }

            $get = BooVar::getx();
            if ($get['proxyId']) {
                BooView::set('searchProxyId', $get['proxyId']);
            }

            if ($get['tradeNo']) {
                BooView::set('tradeNo', $get['tradeNo']);
            }

            BooView::set('proxyList', $proxyList);
        }
	}

    private function getInfo() {

        $post = BooVar::postx();

        if (!$post['id']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        $id = intval($post['id']);

        $obj = BooController::get('Obj_Proxy_Withdrawal');
        $info = $obj->getInfoById($id);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::NO_DATA);
        }

        $balanceObj = BooController::get('Obj_Proxy_Balance');
        $balanceInfo = $balanceObj->getInfoByProxyId($info['proxy_id']);
        if (!$balanceInfo) {
            Common_errorCode::jsonEncode(Common_errorCode::NO_DATA);
        }

        $returnData = array();
        $returnData['id'] = $id;
        $returnData['proxy_name'] = $info['proxy_name'];
        $returnData['proxy_id'] = $info['proxy_id'];
        $returnData['cash_balance'] = $balanceInfo['cash_balance'];
        $returnData['amount'] = $info['amount'];
        $returnData['balanceStatus'] = $balanceInfo['lock_status'] ? '是' : '否';

        return $returnData;
    }

    private function update() {
        $post = BooVar::postx();

        if (!$post['id'] || !$post['status'] || ($post['status'] == 3 && !$post['remark'])) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        $id = intval($post['id']);

        // 00 点暂不结算，等日汇总结算后再进行结算，否则会导致商户余额在5分钟内全部显示为0
        $startTime = date('Y-m-d') . " 00:00:00";
        if (time() < (strtotime($startTime) + 600)) {
            Common_errorCode::jsonEncode(Common_errorCode::SYSTEM_DAY_SETTLE_ACCOUNTS);
        }

        $obj = BooController::get('Obj_Proxy_Withdrawal');
        $info = $obj->getInfoById($id);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::NO_DATA);
        }

        $updateData = array();
        $updateData['status'] = $post['status'];
        $updateData['remark'] = $post['remark'];
        $updateData['operator_id'] = BooSession::get('adminId');
        $updateData['operate_time'] = date('Y-m-d H:i:s');
        $rs = $obj->update($id, $updateData);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }

        if ($post['status'] == 2) {

            $balanceObj = BooController::get('Obj_Proxy_Balance');
            $balanceInfo = $balanceObj->getInfoByProxyId($info['proxy_id']);
            if (!$balanceInfo) {
                Common_errorCode::jsonEncode(Common_errorCode::NO_DATA);
            }

            if ($info['amount'] + $info['fee'] > $balanceInfo['frozen_amount']) {
                Common_errorCode::jsonEncode(Common_errorCode::PROXY_CASH_IS_NOT_ENOUGH);
            }

            $updateBalanceData = array();
            $updateBalanceData['frozen_amount'] = $balanceInfo['frozen_amount'] - $info['amount'] - $info['fee'];
            $updateBalanceData['cash_balance'] = $balanceInfo['cash_balance'] - $info['amount'] - $info['fee'];
            $rs = $balanceObj->update($balanceInfo['id'], $updateBalanceData);
            if (!$rs) {
                Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
            }

            $orderObj = BooController::get('Obj_Proxy_Orders');

            // 计算记录写入帐变表
            $insertData = array();
            $insertData['proxy_id'] = $info['proxy_id'];
            $insertData['proxy_name'] = $info['proxy_name'];
            $insertData['amount'] = $info['amount'];
            $insertData['type'] = 2;
            $insertData['trade_no'] = $info['id'];
            $insertData['create_time'] = date('Y-m-d H:i:s');
            $insertData['create_date'] = date('Ymd');
            $insertData['current_balance'] = $balanceInfo['cash_balance'] - $info['amount'];
            $orderObj->insert($insertData);

            // 计算记录写入帐变表
            $insertData = array();
            $insertData['proxy_id'] = $info['proxy_id'];
            $insertData['proxy_name'] = $info['proxy_name'];
            $insertData['amount'] = $info['fee'];
            $insertData['type'] = 3;
            $insertData['trade_no'] = $info['id'];
            $insertData['create_time'] = date('Y-m-d H:i:s');
            $insertData['create_date'] = date('Ymd');
            $insertData['current_balance'] = $balanceInfo['cash_balance'] - $info['amount'] - $info['fee'];
            $orderObj->insert($insertData);

            // 如果审核的提款订单的时间跟提交订单的时间不在同一天，则需要重新结算提交订单那天的数据
            if (date('Y-m-d', strtotime($info['create_time'])) != date('Y-m-d')) {

                $createDateTime = strtotime(date('Y-m-d', strtotime($info['create_time'])));
                $nowDateTime = strtotime(date('Y-m-d'));
                for ($i = $createDateTime; $i < $nowDateTime; $i += 86400) {
                    BooController::get('Mod_Report')->proxyCalculate($info['proxy_id'], date('Y-m-d', $i));
                }

            }

        } elseif ($post['status'] == 3) {

            $balanceObj = BooController::get('Obj_Proxy_Balance');
            $balanceInfo = $balanceObj->getInfoByProxyId($info['proxy_id']);
            if (!$balanceInfo) {
                Common_errorCode::jsonEncode(Common_errorCode::NO_DATA);
            }

            $updateBalanceData = array();
            $updateBalanceData['frozen_amount'] = $balanceInfo['frozen_amount'] - $info['amount'] - $info['fee'];
            $updateBalanceData['available_balance'] = $balanceInfo['available_balance'] + $info['amount'] + $info['fee'];
            $rs = $balanceObj->update($balanceInfo['id'], $updateBalanceData);
            if (!$rs) {
                Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
            }

        }

        return true;
    }

}