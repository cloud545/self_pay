<?php
class Api_Admin_Proxy_payList extends Api_Admin_Proxy {

    protected $_tpl = "proxy/payList.html";

	protected function _do() {

        if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {

            $post = BooVar::postx();
            if ($post['proxyId']) {
                $proxyId = intval($post['proxyId']);
                $platId = intval($post['platId']);
                $id = $post['tradeNo'] ? substr($post['tradeNo'],10) : 0;
                $type = $post['type'];
                $startTime = $post['startTime'];
                $endTime = $post['endTime'];
                $_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数

                $obj = BooController::get('Obj_Proxy_Pay');
                $pageList = $obj->getPageList($proxyId, $platId, $id, $type, $startTime, $endTime);

                foreach ($pageList['data'] as $key => $info) {

                    $info['tradeNo'] = 'P' . date('Ymd', strtotime($info['create_time'])) . $info['pay_id'];
                    $pageList['data'][$key] = $info;
                }

                $sumInfo = $obj->getSum($proxyId, $platId, $id, $type, $startTime, $endTime);

                return array_merge($pageList, $sumInfo);
            }

            return true;
        } else {

            $obj = BooController::get('Obj_Proxy_Info');
            $tmpProxyList = $obj->getList();

            $proxyList = array();
            foreach ($tmpProxyList as $info) {
                if ($info['is_lock']) {
                    continue;
                }
                $proxyList[$info['proxy_id']] = $info['proxy_name'];
            }

            $platObj = BooController::get('Obj_Plat_Info');
            $tmpPlatList = $platObj->getList();

            $platList = array();
            foreach ($tmpPlatList as $info) {
                $platList[$info['p_id']] = $info['p_name'];
            }

            $payTypeObj = BooController::get('Obj_Plat_PayType');
            $tmpPayTypeList = $payTypeObj->getList();

            $payTypeList = array();
            foreach ($tmpPayTypeList as $info) {
                $payTypeList[$info['pt_id']] = $info['pt_name'];
            }

            $get = BooVar::getx();
            if ($get['proxyId']) {
                BooView::set('searchProxyId', $get['proxyId']);
                BooView::set('startTime', $get['startTime']);
                BooView::set('endTime', $get['endTime']);
            }

            BooView::set('proxyList', $proxyList);
            BooView::set('platList', $platList);
            BooView::set('payTypeList', $payTypeList);
        }
	}
}