<?php
class Api_Admin_Proxy_reportMonth extends Api_Admin_Proxy {

    protected $_tpl = "proxy/reportMonth.html";

	protected function _do() {

        $post = BooVar::postx();
        $proxyId = intval($post['proxyId']);
        $startTime = $post['startTime'];
        $endTime = $post['endTime'];
        $_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数

        $obj = BooController::get('Obj_Proxy_ReportMonth');
        $pageList = $obj->getPageList($proxyId, $startTime, $endTime);
        $sumInfo = $obj->getSum($proxyId, $startTime, $endTime);
        $pageList = array_merge($pageList, $sumInfo);

        if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
            return $pageList;
        } else {

            $obj = BooController::get('Obj_Proxy_Info');
            $tmpProxyList = $obj->getList();

            $proxyList = array();
            foreach ($tmpProxyList as $info) {
                if ($info['is_lock']) {
                    continue;
                }
                $proxyList[$info['proxy_id']] = $info['proxy_name'];
            }

            BooView::set('proxyList', $proxyList);
            BooView::set('pageList', $pageList);
        }
	}

}