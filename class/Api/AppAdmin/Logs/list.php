<?php
class Api_AppAdmin_Logs_list extends Api_AppAdmin_Logs {

    protected $_tpl = "logsList.html";

	protected function _do(){

        $post = BooVar::postx();
        $adminId = BooSession::get("appAutoId");

        // 获取管理员列表
        $obj = BooController::get('Obj_App_Info');
        $info = $obj->getInfoById($adminId);
        $adminList = array();
        $adminList[$info['id']] = array('id' => $info['id'], 'name' => $info['name']);
        BooView::set('adminList', $adminList);

        // 获取菜单列表
        $obj = BooController::get('Obj_App_Menu');
        $list = $obj->getList('menu_target, menu_title', "menu_target != ''");

        $noLogMenu = BooConfig::get('main.noLogMenu');
        $menuList = array();
        foreach ($list as $info) {
            if (in_array($info['menu_target'], $noLogMenu)) {
                continue;
            }
            $menuList[] = $info;
        }
        BooView::set('menuList', $menuList);

        $obj = BooController::get('Obj_App_Logs');

        // 如果搜索时间段，最多只能搜索7天的数据
        $startTime = $post['startTime'];
        $endTime = $post['endTime'];
        if ($startTime) {
            $timeGrap = strtotime($endTime) - strtotime($startTime);
            if ($timeGrap < 0 || $timeGrap > 7 * 86400) {
                Common_errorCode::jsonEncode(Common_errorCode::SEARCH_MAX_DAY_7);
            }
        }

        $controller = $post['logType'] == 'all' ? '' : $post['logType'];// 控制器行为
        $actioner = $post['actioner'] == 'all' ? '' : $post['actioner'];// 动作行为
        $ip = $post['ip'];// ip

        $_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数

        // 没有传入查询日期，默认取当天的
        if (!($startTime && $endTime)) {
            $startTime = date('Y-m-d') .  ' 00:00:00';
            $endTime = date('Y-m-d H:i:s');
        }

        // 获取分页数据
        $dataList = $obj->getPageList($startTime, $endTime, $controller, $actioner, $adminId, $ip);

        // 如果是快捷搜索，需要返回起始和结束时间
        $dataList['startTime'] = $startTime;
        $dataList['endTime'] = $endTime;

        if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
            return $dataList;
        } else {
            BooView::set('pageList', $dataList);
        }
	}

}