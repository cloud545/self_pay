<?php
class Api_AppAdmin_index extends Api_AppAdmin {

    protected $_tpl = "index.html";

    protected function _do() {

        if ($_POST && in_array($_POST['flag'], array('getInfo', 'withdrawal', 'showSecretKey', 'resetSecretKey', 'getBalance'))) {

            switch ($_POST['flag']) {
                case 'getInfo':
                    return $this->getInfo();
                case 'withdrawal':
                    return $this->withdrawal();
                case 'showSecretKey':
                    return $this->showSecretKey();
                case 'resetSecretKey':
                    return $this->resetSecretKey();
                case 'getBalance':
                    return $this->getBalance();
                default:
                    Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
                    return false;
            }

        } else {

            $appAutoId = BooSession::get("appAutoId");
            $appId = BooSession::get("appId");
            $obj = BooController::get('Obj_App_Info');
            $info = $obj->getInfoById($appAutoId);
            $appPlatAppInfo = json_decode($info['plat_app_info'], true);

            $obj = BooController::get('Obj_Plat_PayType');
            $tmpPayTypeList = $obj->getList();

            $payTypeList = array();
            foreach ($tmpPayTypeList as $tmpInfo) {
                $payTypeList[$tmpInfo['pt_id']] = $tmpInfo['pt_name'];
            }

            $platList = array();
            foreach ($appPlatAppInfo as $platId => $tmpInfo) {
                $platList[$platId] = array(
                    'p_name' => $payTypeList[$tmpInfo['payType']],
                    'point' => $tmpInfo['point'],
                );
            }

            $startTime = date('Y-m-d') . " 00:00:00";
            $endTime = date('Y-m-d H:i:s');
            $payObj = BooController::get('Obj_App_Pay');
            $payCount = $payObj->getCount($appId, 0, 0, 0, $startTime, $endTime);
            $paySumInfo = $payObj->getSum($appId, 0, 0, 0, $startTime, $endTime);

            $balanceObj = BooController::get('Obj_App_Balance');
            $appBalanceInfo = $balanceObj->getInfoByAppId($appId);

            // 读取代理未提款笔数
            $yesterday = date('Y-m-d', strtotime($startTime) - 86400) . " 00:00:00";
            $appWithdrawalObj = BooController::get('Obj_App_Withdrawal');
            $applyWithdrawalTimes = $appWithdrawalObj->getCount($appId, array(0, 1), $yesterday, $endTime);
            $successWithdrawalTimes = $appWithdrawalObj->getCount($appId, 2, $yesterday, $endTime);
            $sumWithdrawalInfo = $appWithdrawalObj->getSum($appId, 2, $yesterday, $endTime);

            // 获取私钥
            $secretKey = $info['app_key'];
            $secretKey = mb_substr($secretKey, 0, 3) . '******' . mb_substr($secretKey, -3);

            BooView::set('appId', $info['app_id']);
            BooView::set('appKey', $secretKey);
            BooView::set('securityPwdIsSet', $info['security_pwd'] ? 1 : 0);
            BooView::set('balance', $appBalanceInfo['available_balance']);
            BooView::set('frozenBalance', $appBalanceInfo['frozen_balance']);
            BooView::set('payTimes', $payCount ? $payCount : 0);
            BooView::set('paySum', $paySumInfo['sumAmount'] ? $paySumInfo['sumAmount'] : 0);
            BooView::set('payFee', $paySumInfo['sumFee'] ? $paySumInfo['sumFee'] : 0);
            BooView::set('applyWithdrawalTimes', $applyWithdrawalTimes);
            BooView::set('successWithdrawalTimes', $successWithdrawalTimes);
            BooView::set('withdrawalSum', $sumWithdrawalInfo['sumAmount'] ? $sumWithdrawalInfo['sumAmount'] : 0);
            BooView::set('withdrawalFee', $sumWithdrawalInfo['sumFee'] ? $sumWithdrawalInfo['sumFee'] : 0);
            BooView::set('platList', $platList);
        }

	}

	private function getBalance() {

        $appId = BooSession::get("appId");
	    if (!$appId) {
            return false;
        }

        $balanceObj = BooController::get('Obj_App_Balance');
        $appBalanceInfo = $balanceObj->getInfoByAppId($appId);

        return array('balance' => $appBalanceInfo['available_balance'], 'frozenBalance' => $appBalanceInfo['frozen_balance']);
    }

	private function getInfo() {

        $appAutoId = BooSession::get("appAutoId");
        $appId = BooSession::get("appId");
        $obj = BooController::get('Obj_App_Info');
        $info = $obj->getInfoById($appAutoId);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }

        // 00 点暂不结算，等日汇总结算后再进行结算，否则会导致商户余额在5分钟内全部显示为0
        $startTime = date('Y-m-d') . " 00:00:00";
        if (time() < (strtotime($startTime) + 600)) {
            Common_errorCode::jsonEncode(Common_errorCode::SYSTEM_DAY_SETTLE_ACCOUNTS);
        }

        $balance = BooController::get('Mod_App')->getBalance($appId);
        if ($balance === false) {
            Common_errorCode::jsonEncode(Common_errorCode::NETWORK_FLUCTUATION);
        }

        $returnData = array();
        $returnData['bank_type'] = $info['bank_type'] ? $info['bank_type'] : '未设置';
        $returnData['card_no'] = $info['card_no'] ? $info['card_no'] : '未设置';
        $returnData['bank_name'] = $info['bank_name'] ? $info['bank_name'] : '未设置';
        $returnData['card_account_name'] = $info['card_account_name'] ? $info['card_account_name'] : '未设置';
        $returnData['available_balance'] = $balance ? $balance : 0;
        $returnData['google_secret_isopen'] = $info['google_secret_isopen'];
        $returnData['withdrawal_fee'] = $info['withdrawal_fee'];

        return $returnData;
    }

    private function withdrawal() {

        $post = BooVar::postx();

        if (!Common_rule::checkMoneyNumberSafe($post['money'])) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }

        if ($post['money'] <= 0 || !$post['password']) {
            Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
        }
        
        if ($post['money'] > 50000) {
            Common_errorCode::jsonEncode(Common_errorCode::AMOUNT_IS_POWER);
        }

        // 00 点暂不结算，等日汇总结算后再进行结算，否则会导致商户余额在5分钟内全部显示为0
        $startTime = date('Y-m-d') . " 00:00:00";
        if (time() < (strtotime($startTime) + 600)) {
            Common_errorCode::jsonEncode(Common_errorCode::SYSTEM_DAY_SETTLE_ACCOUNTS);
        }

        $appAutoId = BooSession::get("appAutoId");
        $appId = BooSession::get("appId");
        $obj = BooController::get('Obj_App_Info');
        $info = $obj->getInfoById($appAutoId);
        if (!$info) {
            Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
        }

        $balance = BooController::get('Mod_App')->getBalance($appId);
        if ($balance === false) {
            Common_errorCode::jsonEncode(Common_errorCode::NETWORK_FLUCTUATION);
        }

        $balanceObj = BooController::get('Obj_App_Balance');
        $appBalanceInfo = $balanceObj->getInfoByAppId($appId);

        if ($appBalanceInfo['lock_status']) {
            Common_errorCode::jsonEncode(Common_errorCode::CASH_LOCKED);
        }

        if ($balance < 0) {
            Common_errorCode::jsonEncode(Common_errorCode::CASH_IS_NOT_ENOUGH);
        }

        if (!$info['card_no'] || !$info['bank_name'] || !$info['card_account_name']) {
            Common_errorCode::jsonEncode(Common_errorCode::CARD_NO_INFO_IS_LESS);
        }

        if ($post['money'] + $info['withdrawal_fee'] > $balance) {
            Common_errorCode::jsonEncode(Common_errorCode::CASH_IS_NOT_ENOUGH);
        }

        if (!$info['security_pwd']) {
            Common_errorCode::jsonEncode(Common_errorCode::CASH_PASSWORD_IS_LESS);
        }

        if ($info['security_pwd'] != Common_rule::encodeSecurityPassword($post['password'], $info['security_salt'])) {
            Common_errorCode::jsonEncode(Common_errorCode::PASSWORD_ERR);
        }

        if(!$this->isSpecialApp){
            if ($info['google_secret_isopen']) {
                if (!$post['googleCode']) {
                    Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
                }

                $checkCodeRs = BooGoogleAuthor::getInstance()->verifyCode($info['google_secret_code'], $_POST['googleCode']);
                if (!$checkCodeRs) {
                    Common_errorCode::jsonEncode(Common_errorCode::GOOGLE_CODE_IS_ERROR);
                }
            }
        }

        $cacheKey = BooConfig::get('cacheKey.appWithdrawalLock') . $info['app_id'];
        if (Common_cacheLock::get($cacheKey)) {
            return Common_errorCode::jsonEncode(Common_errorCode::FA_IS_WITHDRAWAL_BUSY);
        }
        $rs = Common_cacheLock::set($cacheKey, 1, 3);
        if (!$rs) {
            return Common_errorCode::jsonEncode(Common_errorCode::FA_IS_WITHDRAWAL_BUSY);
        }

        $platId = $pointFee = 0;
        $platTag = '';
        $tmpPlatInfo = json_decode($info['plat_app_info'], true);

        /*
         foreach ($tmpPlatInfo as $tmpPlatId => $tmpInfo) {
            if ($tmpInfo['payType'] == 7) {
                $platId = $tmpPlatId;
                $pointFee = $post['money'] * $tmpInfo['point'];
                $platTag = $tmpInfo['platTag'];
                break;
            }
        }
        */

        $inserData = array(
            'app_id' => $info['app_id'],
            'app_name' => $info['name'],
            'amount' => $post['money'],
            'fee' => $info['withdrawal_fee'] + $pointFee,
            'pt_id' => 7,
            'p_id' => $platId,
            'bank_type' => $info['bank_type'],
            'card_no' => $info['card_no'],
            'bank_name' => $info['bank_name'],
            'card_account_name' => $info['card_account_name'],
            'create_time' => date('Y-m-d H:i:s'),
            'status' => $platId ? 1 : 0,
            'apply_user' => BooSession::get('appName'),

        );

        $obj = BooController::get('Obj_App_Withdrawal');
        $orderId = $obj->insert($inserData);
        if (!$orderId) {
            Common_errorCode::jsonEncode(Common_errorCode::INSERT_ERR);
        }

        if ($platTag) {
            $platInfo = BooController::get('Mod_ThirdPlatform')->getPlatInfo($info['app_id'], 7);
            $appId = BooController::get('Common')->numberAndStrSwap($info['app_id']);
            $orderId = BooController::get('Common')->numberAndStrSwap($orderId);
            $payType = BooController::get('Common')->numberAndStrSwap(7);
            if (BooUtil::isHttps()) {
                $platInfo['appInfo']['callbackUrl'] = 'https://' . $_SERVER['HTTP_HOST'] . '/withdrawalCallback/' . "{$appId}1{$payType}1{$orderId}";
            } else {
                $platInfo['appInfo']['callbackUrl'] = 'http://' . $_SERVER['HTTP_HOST'] . '/withdrawalCallback/' . "{$appId}1{$payType}1{$orderId}";
            }

            $payInfo = array();
            $payInfo['amount'] = $post['amount'];
            $payInfo['fee'] = $info['withdrawal_fee'] + $pointFee;
            $payInfo['pAutoOrderId'] = $orderId;
            $payInfo['pOrderId'] = "{$appId}1{$payType}1{$orderId}";
            $payInfo['orderId'] = $post['orderId'];
            $rs = BooController::get("Mod_ThirdPlatform_{$platTag}")->withdrawal($platInfo, $payInfo);

            // 提款失败处理，status：1提款成功，2提款失败，0处理中
            $updateData = array();
            if ($rs['status'] != 2) {
                $updateBalanceData = array();
                $updateBalanceData['available_balance'] = $balance - $post['amount'] - $info['withdrawal_fee'] - $pointFee;
                $updateBalanceData['frozen_amount'] = $appBalanceInfo['frozen_amount'] + $post['amount'] + $info['withdrawal_fee'] + $pointFee;
                $updateBalanceData['last_modify_time'] = date('Y-m-d H:i:s');
                $upRs = $balanceObj->update($appBalanceInfo['id'], $updateBalanceData);
                if (!$upRs) {
                    Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
                }
            } else {
                $updateData['status'] = 3;
                $updateData['remark'] = $rs['msg'];
            }

            $updateData['plat_order_id'] = $rs['platOrderId'];
            $obj->update($orderId, $updateData);
        } else {
            $updateData = array();
            $updateData['available_balance'] = $balance - $post['money'] - $info['withdrawal_fee'];
            $updateData['frozen_amount'] = $appBalanceInfo['frozen_amount'] + $post['money'] + $info['withdrawal_fee'];
            $updateData['last_modify_time'] = date('Y-m-d H:i:s');
            $rs = $balanceObj->update($appBalanceInfo['id'], $updateData);
            if (!$rs) {
                Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
            }
        }

        // 删除锁
        Common_cacheLock::del($cacheKey);

        return true;
    }

    /**
     * 显示私钥
     */
    public function showSecretKey() {

        $returnData = array();

        $post = BooVar::postx();
        $securityPwd = $post['password'];// 安全密码

        $appAutoId = BooSession::get("appAutoId");
        $obj = BooController::get('Obj_App_Info');
        $info = $obj->getInfoById($appAutoId);

        // 判断是否进行安全验证，如果没有进行安全验证显示安全验证项
        if (!$securityPwd) {

            // 没有设置安全密码先设置安全密码
            if ($info['security_pwd']) {
                $returnData['showSecurityPwdVerify'] = 1;
                return $returnData;
            } else {
                Common_errorCode::jsonEncode(Common_errorCode::SAFE_ANSWER_IS_ERROR);
            }
        }

        if (!$info['security_pwd']) {
            Common_errorCode::jsonEncode(Common_errorCode::CASH_PASSWORD_IS_LESS);
        }

        if ($info['security_pwd'] != Common_rule::encodeSecurityPassword($securityPwd, $info['security_salt'])) {
            Common_errorCode::jsonEncode(Common_errorCode::PASSWORD_ERR);
        }

        // 返回私钥
        $returnData['secretKey'] = $info['app_key'];
        return $returnData;
    }

    /**
     * 重置私钥
     */
    public function resetSecretKey() {

        $returnData = array();

        //Common_errorCode::jsonEncode(Common_errorCode::NO_POWER);

        $post = BooVar::postx();
        $securityPwd = $post['password'];// 安全密码
        //if (!$securityPwd) {
        //    Common_errorCode::jsonEncode(Common_errorCode::PASSWORD_ERR);
        //}

        $appAutoId = BooSession::get("appAutoId");
        $obj = BooController::get('Obj_App_Info');
        $info = $obj->getInfoById($appAutoId);

        // 判断是否进行安全验证，如果没有进行安全验证显示安全验证项
        if (!$securityPwd) {
            // 没有设置安全密码先设置安全密码
            if ($info['security_pwd']) {
                $returnData['showSecurityPwdVerify'] = 1;
                return $returnData;
            } else {
                Common_errorCode::jsonEncode(Common_errorCode::SAFE_ANSWER_IS_ERROR);
            }
        }

        if (!$info['security_pwd']) {
            Common_errorCode::jsonEncode(Common_errorCode::CASH_PASSWORD_IS_LESS);
        }

        if ($info['security_pwd'] != Common_rule::encodeSecurityPassword($securityPwd, $info['security_salt'])) {
            Common_errorCode::jsonEncode(Common_errorCode::PASSWORD_ERR);
        }

        // 生成新的私钥
        $key = md5(time() . rand(1, 99));

        $updateData = array();
        $updateData['app_key'] = $key;
        $rs = $obj->update($info['id'], $updateData);
        if (!$rs) {
            Common_errorCode::jsonEncode(Common_errorCode::UPDATE_ERR);
        }

        // 返回私钥
        $returnData['secretKey'] = $key;
        return $returnData;
    }

}