<?php
class Api_AppAdmin_App_withdrawalList extends Api_AppAdmin_App {

    protected $_tpl = "withdrawalList.html";

	protected function _do() {

        $appAutoId = BooSession::get("appAutoId");
        $appId = BooSession::get("appId");
        $post = BooVar::postx();
        $status = $post['status'] ? $post['status'] : 'all';
        $startTime = $post['startTime'];
        $endTime = $post['endTime'];

        $get = BooVar::getx();
        if ($get['tradeNo']) {
            $post['tradeNo'] = $get['tradeNo'];
            BooView::set('tradeNo', $get['tradeNo']);
        }

        if ($post['tradeNo']) {
            if (!Common_rule::checkStrSafe($post['tradeNo'])) {
                $id = 0;
            } else {
                $id = substr($post['tradeNo'], 10);
            }
        } else {
            $id = 0;
        }

        $_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数

        $obj = BooController::get('Obj_App_Withdrawal');
        $pageList = $obj->getPageList($appId, $status, $startTime, $endTime, $id);
        foreach ($pageList['data'] as $key => $info) {
            $info['tradeNo'] = 'AW' . date('Ymd', strtotime($info['create_time'])) . $info['id'];

            if ($info['status'] == 1) {
                $info['statusName'] = '银行处理中';
            } else if ($info['status'] == 2) {
                $info['statusName'] = '提款成功';
            } else if ($info['status'] == 3) {
                $info['statusName'] = '提款失败';
            } else {
                $info['statusName'] = '系统处理中';
            }

            $pageList['data'][$key] = $info;
        }

        $sumInfo = $obj->getSum($appId, 2, $startTime, $endTime);
        $pageList = array_merge($pageList, $sumInfo);

        if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
            return $pageList;
        } else {
            BooView::set('pageList', $pageList);
            BooView::set('sumAmount', $sumInfo['sumAmount']);
        }
	}
}