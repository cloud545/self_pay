<?php
class Api_AppAdmin_App_exportWithdrawalExcel extends Api_AppAdmin_App {

	protected function _do() {

        $appAutoId = BooSession::get("appAutoId");
        $appId = BooSession::get("appId");
        $post = BooVar::requestx();
        $status = $post['status'] ? $post['status'] : 'all';
        $startTime = $post['startTime'];
        $endTime = $post['endTime'];

        $obj = BooController::get('Obj_App_Withdrawal');
        $tmpList = $obj->getList($appId, $status, $startTime, $endTime);
        $sumInfo = $obj->getSum($appId, $status, $startTime, $endTime);

        $list = array();
        foreach ($tmpList as $key => $info) {

            $info['tradeNo'] = 'AW' . date('Ymd', strtotime($info['create_time'])) . $info['id'];

            if ($info['status'] == 1) {
                $info['statusName'] = '银行处理中';
            } else if ($info['status'] == 2) {
                $info['statusName'] = '提款成功';
            } else if ($info['status'] == 3) {
                $info['statusName'] = '提款失败';
            } else {
                $info['statusName'] = '系统处理中';
            }

            $pageList['data'][$key] = $info;
            $tmpArr = array(
                $info['id'],
                $info['amount'],
                $info['fee'],
                $info['bank_type'],
                " {$info['card_no']} ",
                $info['bank_name'],
                $info['card_account_name'],
                $info['tradeNo'],
                $info['create_time'],
                $info['remark'],
                $info['statusName'],
            );

            $list[] = $tmpArr;
        }

        $excelObj = new BooExcel();
        //$excelObj = $objPHPExcel = PHPExcel_IOFactory::createReader();

        //横向单元格标识
        $cellName = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ');
        $title = array('序号', '提款金额', '提款手续费', '提款银行', '提款银行卡号', '银行卡对应的支行', '持卡人', '交易流水号', '申请时间', '备注', '状态');

        $excelObj->getActiveSheet(0)->setTitle('sheet1');   //设置sheet名称
        $_row = 1;   //设置纵向单元格标识
        $_cnt = count($title);
        $excelObj->getActiveSheet(0)->mergeCells('A'.$_row.':'.$cellName[$_cnt-1].$_row);   //合并单元格
        $excelObj->setActiveSheetIndex(0)->setCellValue('A'.$_row, '数据导出：'.date('Y-m-d H:i:s'));  //设置合并后的单元格内容
        $_row++;
        $i = 0;

        foreach($title as $v){   //设置列标题
            $excelObj->setActiveSheetIndex(0)->setCellValue($cellName[$i].$_row, $v);
            $i++;
        }
        $_row++;


        //填写数据
        if($list){
            $i = 0;
            foreach($list AS $_v){
                $j = 0;
                foreach($_v AS $_cell){
                    $excelObj->getActiveSheet(0)->setCellValue($cellName[$j] . ($i+$_row), $_cell);
                    $j++;
                }

                $i++;
            }

            $sumArr = array(
                '总计',
                $sumInfo['sumAmount'],
                $sumInfo['sumFee'],
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                ''
            );

            $j = 0;
            foreach($sumArr AS $_cell){
                $excelObj->getActiveSheet(0)->setCellValue($cellName[$j] . ($i+$_row), $_cell);
                $j++;
            }

        }

        $objWrite = PHPExcel_IOFactory::createWriter($excelObj, 'Excel2007');

        header('pragma:public');
        header("Content-Disposition:attachment;filename=" . date('Y-m-d') .".xls");

        $objWrite->save('php://output');exit;

        return true;
    }
}