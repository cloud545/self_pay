<?php
class Api_AppAdmin_App_exportOrderExcel extends Api_AppAdmin_App {

	protected function _do() {

        $appAutoId = BooSession::get("appAutoId");
        $appId = BooSession::get("appId");
        $post = BooVar::requestx();
        $platId = 0;
        $result = isset($post['result']) ? $post['result'] : 1;
        $id = 0;
        $type = $post['type'];
        $startTime = $post['startTime'];
        $endTime = $post['endTime'];
        $obj = BooController::get('Obj_App_Orders');
        $tmpList = $obj->getList($appId, $type, $startTime, $endTime);
        $data = [];
        foreach ($tmpList as $key => $info) {
            if ($info['type'] == 1) {
                $info['tradeInfo'] = "{$info['start_time']} 到 {$info['end_time']} 充值结算";
                if ($info['remark']) {
                    $info['tradeInfo'] .= "，" . $info['remark'];
                }
            } elseif (in_array($info['type'], array(2, 3))) {
                $info['tradeInfo'] = 'AW' . date('Ymd', strtotime($info['create_time'])) . $info['trade_no'];
            } elseif (in_array($info['type'], array(4, 5))) {
                $info['tradeInfo'] = '管理员变更资金，变更缘由：' . $info['remark'];
            } else {
                $info['tradeInfo'] = '管理员变更冻结资金，变更缘由：' . $info['remark'];
            }

            if ($info['type'] == 1) {
                $info['typeName'] = '充值结算';
            } elseif ($info['type'] == 2) {
                $info['typeName'] = '提款';
            } elseif ($info['type'] == 3) {
                $info['typeName'] = '提款手续费';
            } elseif ($info['type'] == 4) {
                $info['typeName'] = '管理员添加资金';
            } elseif ($info['type'] == 5) {
                $info['typeName'] = '管理员扣减资金';
            } elseif ($info['type'] == 6) {
                $info['typeName'] = '管理员冻结资金';
            } elseif ($info['type'] == 7) {
                $info['typeName'] = '管理员解冻资金';
            } else {
                $info['typeName'] = '管理员清除冻结资金';
            }

            $list = array(
                $info['id'],
                $info['amount'],
                $info['typeName'],
                $info['tradeInfo'],
                $info['create_time'],
                $info['current_balance'],
            );
            $data[$key] = $list;
        }

        $excelObj = new BooExcel();
        //$excelObj = $objPHPExcel = PHPExcel_IOFactory::createReader();

        //横向单元格标识
        $cellName = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ');
        $title = array('序号', '金额', '类型', '交易流水号', '帐变时间' , '帐变后金额');

        $excelObj->getActiveSheet(0)->setTitle('sheet1');   //设置sheet名称
        $_row = 1;   //设置纵向单元格标识
        $_cnt = count($title);
        $excelObj->getActiveSheet(0)->mergeCells('A'.$_row.':'.$cellName[$_cnt-1].$_row);   //合并单元格
        $excelObj->setActiveSheetIndex(0)->setCellValue('A'.$_row, '数据导出：');  //设置合并后的单元格内容
        $_row++;
        $i = 0;

        foreach($title as $v){   //设置列标题
            $excelObj->setActiveSheetIndex(0)->setCellValue($cellName[$i].$_row, $v);
            $i++;
        }
        $_row++;


        //填写数据
        if($data){
            $i = 0;
            foreach($data AS $_v){
                $j = 0;
                foreach($_v AS $_cell){
                    $excelObj->getActiveSheet(0)->setCellValue($cellName[$j] . ($i+$_row), $_cell);
                    $j++;
                }

                $i++;
            }
        }

        $objWrite = PHPExcel_IOFactory::createWriter($excelObj, 'Excel2007');

        header('pragma:public');
        header("Content-Disposition:attachment;filename=" . date('Y-m-d') .".xls");

        $objWrite->save('php://output');exit;

        return true;
    }
}