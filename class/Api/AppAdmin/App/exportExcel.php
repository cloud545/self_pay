<?php
class Api_AppAdmin_App_exportExcel extends Api_AppAdmin_App {

	protected function _do() {

        $appAutoId = BooSession::get("appAutoId");
        $appId = BooSession::get("appId");
        $post = BooVar::requestx();
        $platId = 0;
        $result = isset($post['result']) ? $post['result'] : 1;
        $id = 0;
        $type = $post['type'];
        $startTime = $post['startTime'];
        $endTime = $post['endTime'];

        $obj = BooController::get('Obj_App_Pay');
        $tmpList = $obj->getList($appId, $platId, $id, $type, $startTime, $endTime, $result);
        $sumInfo = $obj->getSum($appId, $platId, $id, $type, $startTime, $endTime);

        $typeArr = [];
        $typeObj = BooController::get('Obj_Plat_PayType');
        $typeList = $typeObj->getAll();
        foreach ($typeList as $key => $value) {
            $typeArr[$value['pt_id']] = $value['pt_name'];
        }
        $list = array();
        foreach ($tmpList as $key => $info) {
            $info['tradeNo'] = 'P' . date('Ymd', strtotime($info['create_time'])) . $info['id'];
            $tmpArr = array(
                $info['id'],
                $info['app_name'],
                $typeArr[$info['pt_id']],
                $info['order_id'],
                $info['amount'],
                $info['real_amount'],
                $info['tradeNo'],
                $info['create_time'],
                $info['end_time'],
                $info['current_fee_rate'],
                $info['current_fee'],
            );

            if ($info['result'] == 1) {
                $tmpArr[] = '已支付';
            } else {
                $tmpArr[] = '未支付';
            }

            $list[] = $tmpArr;
        }

        $excelObj = new BooExcel();
        //$excelObj = $objPHPExcel = PHPExcel_IOFactory::createReader();

        //横向单元格标识
        $cellName = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ');
        $title = array('序号', '商户', '通道类型' ,'充值订单号', '充值金额', '真实到账金额', '渠道充值订单号', '充值时间', '完成时间', '点数', '扣掉点数后金额', '支付状态');

        $excelObj->getActiveSheet(0)->setTitle('sheet1');   //设置sheet名称
        $_row = 1;   //设置纵向单元格标识
        $_cnt = count($title);
        $excelObj->getActiveSheet(0)->mergeCells('A'.$_row.':'.$cellName[$_cnt-1].$_row);   //合并单元格
        $excelObj->setActiveSheetIndex(0)->setCellValue('A'.$_row, '数据导出：'.date('Y-m-d H:i:s'));  //设置合并后的单元格内容
        $_row++;
        $i = 0;

        foreach($title as $v){   //设置列标题
            $excelObj->setActiveSheetIndex(0)->setCellValue($cellName[$i].$_row, $v);
            $i++;
        }
        $_row++;


        //填写数据
        if($list){
            $i = 0;
            foreach($list AS $_v){
                $j = 0;
                foreach($_v AS $_cell){
                    $excelObj->getActiveSheet(0)->setCellValue($cellName[$j] . ($i+$_row), $_cell);
                    $j++;
                }

                $i++;
            }

            $sumArr = array(
                '总计',
                '',
                '',
                $sumInfo['sumAmount'],
                $sumInfo['sumRealAmount'],
                '',
                '',
                '',
                '',
                $sumInfo['sumFee'],
                ''
            );

            $j = 0;
            foreach($sumArr AS $_cell){
                $excelObj->getActiveSheet(0)->setCellValue($cellName[$j] . ($i+$_row), $_cell);
                $j++;
            }

        }

        $objWrite = PHPExcel_IOFactory::createWriter($excelObj, 'Excel2007');

        header('pragma:public');
        header("Content-Disposition:attachment;filename=" . date('Y-m-d') .".xls");

        $objWrite->save('php://output');exit;

        return true;
    }
}