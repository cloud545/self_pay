<?php
class Api_AppAdmin_App_payList extends Api_AppAdmin_App {

    protected $_tpl = "payList.html";

	protected function _do() {

        $appAutoId 	= BooSession::get("appAutoId");
        $appId 		= BooSession::get("appId");
        $post 		= BooVar::postx();
		
        $platId 	= intval($post['platId']);
        $result 	= isset($post['result']) ? $post['result'] : 1;
        $id 		= $post['tradeNo'] ? substr($post['tradeNo'],9) : 0;
        $orderId 	= $post['orderId'] ? $post['orderId'] : 0;
        $type 		= $post['type'];
        $startTime 	= $post['startTime'];
        $endTime	= $post['endTime'];
		
        $_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数

        $get = BooVar::getx();
        if ($get['tradeNo']) {
            $id = substr($get['tradeNo'],9);
            BooView::set('tradeNo', $get['tradeNo']);
        }

		if ($get['orderId']) {
			$orderId = $get['orderId'];
			BooView::set('orderId', $get['orderId']);
		}


        if ($get['endTime']) {
            $startTime = $get['startTime'];
            $endTime = $get['endTime'];
            BooView::set('startTime', $get['startTime']);
            BooView::set('endTime', $get['endTime']);
        }

        $obj = BooController::get('Obj_App_Pay');
        $pageList = $obj->getPageList($appId, $platId, $id, $type, $startTime, $endTime, $result,$orderId);
        $sumInfo = $obj->getSum($appId, $platId , $id, $type, $startTime, $endTime);
        foreach ($pageList['data'] as $key => $info) {
            $info['tradeNo'] = 'P' . date('Ymd', strtotime($info['create_time'])) . $info['id'];
            $pageList['data'][$key] = $info;
        }

        if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
            return array_merge($pageList, $sumInfo);
        } else {
			//支付类型，只能显示给分配过的支付类型
			$obj = BooController::get('Obj_Plat_PayType');
			$tmpPayTypeList = $obj->getList();
			$payTypeList = array();
			foreach ($tmpPayTypeList as $info) {
					$payTypeList[$info['pt_id']] = $info['pt_name'];
			}

            $obj = BooController::get('Obj_Plat_PayType');
            $tmpPayTypeList = $obj->getList();

            $payTypeList = array();
            foreach ($tmpPayTypeList as $info) {
                $payTypeList[$info['pt_id']] = $info['pt_name'];
            }
//var_dump($payTypeList);
            $obj = BooController::get('Obj_App_Info');
            $info = $obj->getInfoById($appAutoId);
            $proxyPlatAppInfo = json_decode($info['plat_app_info'], true);
//var_dump($proxyPlatAppInfo);
            $platList = array();
            foreach ($proxyPlatAppInfo as $platId => $tmpInfo) {
//                $platList[$platId] = $payTypeList[$tmpInfo['payType']];
                $platList[$tmpInfo['payType']]  = $payTypeList[$tmpInfo['payType']];
            }

            BooView::set('platList', $platList);
            BooView::set('pageList', $pageList);
            BooView::set('sumInfo', $sumInfo);
            BooView::set('payTypeList', $payTypeList);
         }
    }
}
