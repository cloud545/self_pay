<?php
class Api_AppAdmin_User_logout extends Api_AppAdmin_User {

	protected function _do(){

        BooSession::destroy();
        header('Location: /appAdmin/user/login');
	    return true;
	}
}