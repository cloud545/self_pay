<?php
class Api_AppAdmin_User_login extends Api_AppAdmin_User {

    protected $_tpl = "login.html";

	protected function _do(){

	    if ($_POST) {

	        $post = BooVar::postx('password', 'userName', 'captcha');
	        if (!$post['password'] || !$post['userName'] || !$post['captcha']) {
                Common_errorCode::jsonEncode(Common_errorCode::PARAM_ERR);
            }

            $adminLoginCaptcha = BooVar::cookie("appLoginCaptcha");
            if (strtoupper($post['captcha']) != strtoupper($adminLoginCaptcha)) {
                Common_errorCode::jsonEncode(Common_errorCode::VERIFY_CODE_ERR);
            }

            $obj = BooController::get('Obj_App_Info');
            $adminInfo = $obj->getInfoByName(trim($post['userName']));
            if (!$adminInfo) {
                Common_errorCode::jsonEncode(Common_errorCode::USER_NOT_EXIST);
            }

            if ($adminInfo['password'] != Common_rule::encodeLoginPassword($post['password'], $adminInfo['salt'])){
                Common_errorCode::jsonEncode(Common_errorCode::PASSWORD_ERR);
            }

            if ($adminInfo['is_lock']) {
                Common_errorCode::jsonEncode(Common_errorCode::USER_LOCKED);
            }

            if ($adminInfo['allow_login_ip'] && !in_array(BooUtil::realIp(), explode(',', $adminInfo['allow_login_ip']))) {
                Common_errorCode::jsonEncode(Common_errorCode::NO_POWER);
            }

            $updateData = array(
                'last_update_time' => date('Y-m-d H:i:s'),
                'last_ip' => BooUtil::realIp(),
            );
            $obj->update($adminInfo['id'], $updateData);

            BooSession::set('appAutoId', $adminInfo['id']);
            BooSession::set('appId', $adminInfo['app_id']);
            BooSession::set('appName', $adminInfo['name']);
            BooSession::set('appNickname', $adminInfo['name']);
            BooSession::set('appIsLock', $adminInfo['is_lock']);

            // 记录管理员行为
            BooController::get('Mod_Admin')->addAppAdminLogs();
        }

        return true;
	}
}