<?php
class Api_AppAdmin_checkSign extends Api_AppAdmin {

    protected $_tpl = "checkSign.html";

	protected function _do(){

        if ($_POST && in_array($_POST['flag'], array('check'))) {

            $appAutoId = BooSession::get("appAutoId");
            $obj = BooController::get('Obj_App_Info');
            $info = $obj->getInfoById($appAutoId);

            $post = BooVar::postx();
            $param = htmlspecialchars_decode($post['param']);
            $appKey = $info['app_key'];

            $data = array();
            $paramArr = explode('&', $param);
            foreach ($paramArr as $tmpStr) {
                $tmpArr = explode('=', $tmpStr);
                $data[$tmpArr[0]] = $tmpArr[1];
            }

            ksort($data);

            $checkString = '';
            foreach ($data as $key =>$value) {
                if (!$checkString) {
                    $checkString = "{$key}={$value}";
                } else {
                    $checkString .= "&{$key}={$value}";
                }
            }

            $sign = md5($checkString . $appKey);

            $returnData = array();
            $returnData['noKeyMd5Str'] = $checkString;
            $returnData['md5Str'] = $checkString . $appKey;
            $returnData['sign'] = $sign;

            return $returnData;
        }

	}

}