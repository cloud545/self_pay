<?php
class Api_AppAdmin_report extends Api_AppAdmin {

    protected $_tpl = "report.html";

	protected function _do() {

        $post = BooVar::postx();
        $appId = BooSession::get("appId");
        $startTime = $post['startTime'];
        $endTime = $post['endTime'];
        $_GET['pageId'] = $post['page'] ? $post['page'] : 1;// 查询的页数

        $obj = BooController::get('Obj_App_Report');
        $pageList = $obj->getPageList($appId, $startTime, $endTime);
        $sumInfo = $obj->getSum($appId, $startTime, $endTime);
        $pageList = array_merge($pageList, $sumInfo);

        if (BooVar::server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest') {
            return $pageList;
        } else {
            BooView::set('pageList', $pageList);
        }

	}

}