<?php
class Api_AppAdmin_download extends Api_AppAdmin {

    protected $_tpl = "download.html";

	protected function _do(){

        if (BooUtil::isHttps()) {
            $downloadUrl = 'www.' . $_SERVER['SERVER_NAME'];
        } else {
            $downloadUrl = $_SERVER['SERVER_NAME'];
        }

	    BooView::set('apiDomain', $downloadUrl);

	}

}